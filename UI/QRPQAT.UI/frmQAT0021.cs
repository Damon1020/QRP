﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : R&R관리                                               */
/* 프로그램ID   : frmQAT0021.cs                                         */
/* 프로그램명   : 계측기Data 등록                                       */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-27 : 기능구현 추가 (이종호)                   */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPQAT.UI
{
    public partial class frmQAT0021 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        // 전역변수
        private string strPlantCode;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public frmQAT0021()
        {
            InitializeComponent();
        }

        private void frmQAT0021_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQAT0021_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitTab();
            InitText();
            InitLabel();
            InitComboBox();
            InitGrid();

            //uGroupBoxContentsArea숨기기
            uGroupBoxContentsArea.Expanded = false;

            // CheckBox 편집불가
            this.uCheckStabilityFlag.Enabled = false;
            this.uCheckAccuracyFlag.Enabled = false;
            this.uCheckStabilityFlag.Appearance.BackColorDisabled = Color.White;
            this.uCheckStabilityFlag.Appearance.ForeColorDisabled = Color.Black;
            this.uCheckAccuracyFlag.Appearance.BackColorDisabled = Color.White;
            this.uCheckAccuracyFlag.Appearance.ForeColorDisabled = Color.Black;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmQAT0021_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 탭 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabData, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None, m_resSys.GetString("SYS_LANG"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //타이틀지정
                titleArea.mfSetLabelText("MSA Data등록", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchProject, "프로젝트명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelProjectCode, "프로젝트코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProjectName, "프로젝트명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlantCode, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcessCode, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMeasureTool, "계측기", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInspectDesc, "검사설명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRepeatCount, "측정반복수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPartCount, "측정부품수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMeasureCount, "측정계측기수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelTolerance, "Tolerance", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelAccuracyValue, "정확성값", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRRValueChangeValue, "R&&R값 변경값", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlantCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                //--정보
                grd.mfInitGeneralGrid(this.uGridMSADataList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--정보
                grd.mfSetGridColumn(this.uGridMSADataList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMSADataList, 0, "ProjectCode", "프로젝트코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMSADataList, 0, "ProjectName", "프로젝트명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMSADataList, 0, "InspectDate", "검사일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGridMSADataList, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMSADataList, 0, "MeasureToolName", "계측기명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMSADataList, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMSADataList, 0, "InspectItemDesc", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////--계측기(측정자)명1
                ////grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                ////   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                ////   , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                ////   , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //////--계측기(측정자)명1
                ////grd.mfSetGridColumn(this.uGrid2, 0, "Machine", "Machine", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGrid2, 0, "1회", "1회", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 15
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGrid2, 0, "2회", "2회", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////--계측기(측정자)명2
                ////grd.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                ////   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                ////   , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                ////   , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //////--계측기(측정자)명2
                ////grd.mfSetGridColumn(this.uGrid3, 0, "Machine", "Machine", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGrid3, 0, "1회", "1회", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 15
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGrid3, 0, "2회", "2회", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridMSADataList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMSADataList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                ////uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                ////uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                ////uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                ////uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바기능
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 매개변수 설정
                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                string strProjectName = this.uTextSearchProjectName.Text;
                string strInspectDateFrom = Convert.ToDateTime(this.uDateSearchInspectDateFrom.Value).ToString("yyyy-MM-dd");
                string strInspectDateTo = Convert.ToDateTime(this.uDateSearchInspectDateTo.Value).ToString("yyyy-MM-dd");

                // BL연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                QRPQAT.BL.QATMSA.MSAH clsHeader = new QRPQAT.BL.QATMSA.MSAH();
                brwChannel.mfCredentials(clsHeader);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtSearch = clsHeader.mfReadQATMSAH(strPlantCode, strProjectName, strInspectDateFrom, strInspectDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGridMSADataList.DataSource = dtSearch;
                this.uGridMSADataList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMSADataList, 0);
                }
                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000881", "M001041", Infragistics.Win.HAlign.Center);
                    return;
                }
                else
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAData), "MSAData");
                        QRPQAT.BL.QATMSA.MSAData clsData = new QRPQAT.BL.QATMSA.MSAData();
                        brwChannel.mfCredentials(clsData);


                        DataTable dtData = clsData.mfSetDataInfo();

                        for (int i = 1; i <= Convert.ToInt32(this.uTextMeasureCount.Text); i++)
                        {
                            foreach (Control ctrl in this.uTabData.Tabs[i.ToString()].TabPage.Controls)
                            {
                                if (ctrl.Name == "uGridTab" + i.ToString())
                                {
                                    for (int j = 1; j <= ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows.Count; j++)
                                    {
                                        for (int k = 1; k <= Convert.ToInt32(this.uTextReapeatCount.Text) * 2;  k = k + 2)
                                        {
                                            DataRow dr = dtData.NewRow();

                                            dr["PlantCode"] = this.uGridMSADataList.ActiveRow.Cells["PlantCode"].Value.ToString();
                                            dr["ProjectCode"] = this.uTextProjectCode.Text;
                                            dr["ItemSeq"] = Convert.ToInt32(i);
                                            if (((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j - 1].Cells[k + 1].Value == null)
                                            {
                                                dr["DataSeq"] = Convert.ToDecimal(0);
                                            }
                                            else
                                            {
                                                dr["DataSeq"] = Convert.ToDecimal(((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j - 1].Cells[k + 1].Value);
                                            }
                                            dr["PartSeq"] = Convert.ToInt32(((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j - 1].RowSelectorNumber);
                                            if (k == 1)
                                            {
                                                dr["RepeatSeq"] = 1;
                                            }
                                            else if(k == 3)
                                            {
                                                dr["RepeatSeq"] = 2;
                                            }
                                            else if (k == 5)
                                            {
                                                dr["RepeatSeq"] = 3;
                                            }
                                            dr["MeasureValue"] = Convert.ToDecimal(((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j-1].Cells[k].Value);
                                            dtData.Rows.Add(dr);
                                        } 
                                    }
                                }
                            }
                        }
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        String strErrRtn = clsData.mfSaveQATMSAData(dtData, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001037", "M000930",
                                             Infragistics.Win.HAlign.Right);
                            mfSearch();
                        }
                        else
                        {
                             DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001037", "M000953",
                                             Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                ////// 펼침상태가 false 인경우

                ////if (this.uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    this.uGroupBoxContentsArea.Expanded = true;
                ////}
                ////// 이미 펼쳐진 상태이면 컴포넌트 초기화
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// 헤더 상세조회 메소드
        /// </summary>
        /// <param name="strProjectCode">프로젝트 코드</param>
        /// <param name="strLang">언어</param>
        private void Search_HeaderDetail(string strProjectCode, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAH), "MSAH");
                QRPQAT.BL.QATMSA.MSAH clsHeader = new QRPQAT.BL.QATMSA.MSAH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtRtn = clsHeader.mfReadQATMSAHDetail(PlantCode, strProjectCode, strLang);

                this.uTextPlantName.Text = dtRtn.Rows[0]["PlantName"].ToString();
                this.uTextProjectCode.Text = dtRtn.Rows[0]["ProjectCode"].ToString();
                this.uTextProcessName.Text = dtRtn.Rows[0]["ProcessName"].ToString();
                this.uTextProjectName.Text = dtRtn.Rows[0]["ProjectName"].ToString();
                this.uTextMeasureCount.Text = dtRtn.Rows[0]["MeasureCount"].ToString();
                this.uTextMeasureToolCode.Text = dtRtn.Rows[0]["MeasureToolCode"].ToString();
                this.uTextMeasureToolName.Text = dtRtn.Rows[0]["MeasureToolName"].ToString();
                this.uTextReapeatCount.Text = dtRtn.Rows[0]["RepeatCount"].ToString();
                this.uTextPartCount.Text = dtRtn.Rows[0]["PartCount"].ToString();
                this.uTextMaterialCode.Text = dtRtn.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dtRtn.Rows[0]["MaterialName"].ToString();
                this.uTextAccuracyValue.Text = dtRtn.Rows[0]["AccuracyValue"].ToString();
                this.uTextTolerance.Text = dtRtn.Rows[0]["Tolerance"].ToString();
                this.uTextInspectItemDesc.Text = dtRtn.Rows[0]["InspectItemDesc"].ToString();
                this.uTextRRValueChangeValue.Text = dtRtn.Rows[0]["RRValueChangeValue"].ToString();
                this.uTextInspectDesc.Text = dtRtn.Rows[0]["InspectDesc"].ToString();
                this.uCheckStabilityFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["StabilityFlag"]);
                this.uCheckAccuracyFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["AccuracyFlag"]);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// MSAData 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <param name="strLang">언어</param>
        private void SearchData(String strPlantCode, String strProjectCode, String strLang)
        {
            try
            {
                
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAData), "MSAData");
                QRPQAT.BL.QATMSA.MSAData clsData = new QRPQAT.BL.QATMSA.MSAData();
                brwChannel.mfCredentials(clsData);

                WinGrid grd = new WinGrid();

                for (int i = 1; i < Convert.ToInt32(this.uTextMeasureCount.Text) + 1; i++)
                {
                    DataTable dtData = clsData.mfReadQATMSAData(PlantCode, strProjectCode, i, strLang);


                    foreach( Control ctrl in this.uTabData.Tabs[i.ToString()].TabPage.Controls)
                    {
                        if (ctrl.Name == "uGridTab" + i.ToString())
                        {
                            ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DataSource = dtData;
                            ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DataBind();
                            GetRange();
                        }
                    }   
                }
                GetTotalAve();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uTextPlantName.Clear();
                this.uTextProjectCode.Clear();
                this.uTextProcessName.Clear();
                this.uTextProjectName.Clear();
                this.uTextMeasureCount.Clear();
                this.uTextMeasureToolCode.Clear();
                this.uTextMeasureToolName.Clear();
                this.uTextReapeatCount.Clear();
                this.uTextPartCount.Clear();
                this.uTextMaterialCode.Clear();
                this.uTextMaterialName.Clear();
                this.uTextAccuracyValue.Clear();
                this.uTextTolerance.Clear();
                this.uTextInspectItemDesc.Clear();
                this.uTextRRValueChangeValue.Clear();
                this.uTextInspectDesc.Clear();
                this.uCheckStabilityFlag.Checked = false;
                this.uCheckAccuracyFlag.Checked = false;

                // Tab 안에 모든 그리드 자원 헤제

                // 탭메뉴 모두 삭제
                this.uTabData.Tabs.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록화면에서 등록된 측정기기명 만큼 탭 생성
        private void CreateTab(string strProjectCode)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATMSA.MSAD), "MSAD");
                QRPQAT.BL.QATMSA.MSAD clsDetail = new QRPQAT.BL.QATMSA.MSAD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtRtn = clsDetail.mfReadQATMSADRegist(PlantCode, strProjectCode);

                if (dtRtn.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRtn.Rows.Count; i++)
                    {
                        this.uTabData.Tabs.Add(dtRtn.Rows[i]["ItemSeq"].ToString(), dtRtn.Rows[i]["MachineName"].ToString());
                        AddDataGridInTab(dtRtn.Rows[i]["ItemSeq"].ToString());
                        
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// uGridTab의 Range값 계산
        /// </summary>
        private void GetRange()
        {
            try
            {
                for (int i = 1; i <= Convert.ToInt32(this.uTextMeasureCount.Text); i++)
                {
                    foreach (Control ctrl in this.uTabData.Tabs[i.ToString()].TabPage.Controls)
                    {
                        for (int j = 0; j < ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows.Count; j++)
                        {
                            Decimal a, b, c;
                            a = Convert.ToDecimal(((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["1Repeat"].Value);
                            b = Convert.ToDecimal(((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["2Repeat"].Value);
                            if (this.uTextReapeatCount.Text == "2")
                            {
                                c = 0;
                            }
                            else
                            {
                                c = Convert.ToDecimal(((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["3Repeat"].Value);
                            }
                            if (c == 0)
                            {
                                ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = System.Math.Abs(a - b);
                            }
                            else if (a == b && b == c)
                            {
                                ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = 0;
                            }
                            else if (a > b && a > c)
                            {
                                if (b > c)
                                {
                                    ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = a - c;
                                }
                                else
                                {
                                    ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = a - b;
                                }
                            }
                            else if (b > a && b > c)
                            {
                                if (a > c)
                                {
                                    ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = b - c;
                                }
                                else
                                {
                                    ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = b - a;
                                }
                            }
                            else if (c > a && c > b)
                            {
                                if (a > b)
                                {
                                    ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = c - b;

                                }
                                else
                                {
                                    ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).Rows[j].Cells["Range"].Value = c - a;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 컬럼의 총합과 평균을 구하는 메소드
        /// </summary>
        private void GetTotalAve()
        {
            
            try
            {
                for (int i = 1; i < Convert.ToInt32(this.uTextMeasureCount.Text) + 1; i++)
                {
                    foreach (Control ctrl in this.uTabData.Tabs[i.ToString()].TabPage.Controls)
                    {
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["1Repeat"]);
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Average
                            , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["1Repeat"]);
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["2Repeat"]);
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Average
                            , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["2Repeat"]);
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["3Repeat"]);
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Average
                            , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["3Repeat"]);
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Sum
                                , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["Range"]);
                        ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Summaries.Add(Infragistics.Win.UltraWinGrid.SummaryType.Average
                            , ((Infragistics.Win.UltraWinGrid.UltraGrid)ctrl).DisplayLayout.Bands[0].Columns["Range"]);
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 데이터 입력 그리드 생성 Method
        /// </summary>
        private void AddDataGridInTab(string strTabkey)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                Infragistics.Win.UltraWinGrid.UltraGrid uGridTab = new Infragistics.Win.UltraWinGrid.UltraGrid();

                uGridTab.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridTab_AfterCellUpdate);
                uGridTab.KeyPress += new KeyPressEventHandler(uGridTab_KeyPress);

                uGridTab.Width = Convert.ToInt32(this.Parent.Width * 0.94);
                uGridTab.Height = Convert.ToInt32(this.Parent.Height * 0.53);

                //그리드 기본설정
                grd.mfInitGeneralGrid(uGridTab, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                                        , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                                        , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                                        , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼 설정
                

                for (int i = 0; i < Convert.ToInt32(this.uTextReapeatCount.Text); i++)
                {

                    grd.mfSetGridColumn(uGridTab, 0, "PartSeq", "부품", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", i.ToString());

                    grd.mfSetGridColumn(uGridTab, 0, (i+1) + "Repeat", (i + 1) + "회", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 300
                                            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                    grd.mfSetGridColumn(uGridTab, 0, (i + 1) + "DataSeq", (i + 1) + "회", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 300
                                            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");
                }

                grd.mfSetGridColumn(uGridTab, 0, "Range", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

               
                uGridTab.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridTab.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridTab.Name = "uGridTab" + strTabkey;

                Point point = new Point(12, 12);
                uGridTab.Location = point;

                this.uTabData.Tabs[strTabkey].TabPage.Controls.Add(uGridTab);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        void uGridTab_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back) || e.KeyChar == Convert.ToChar(46)))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 셀의 값이 변경될때, Row의 Repeat값을 받아 최대값과 최소값을 구하여 Range를 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void uGridTab_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "1Repeat" || e.Cell.Column.Key == "2Repeat" || e.Cell.Column.Key == "3Repeat")
                {
                    GetRange();
                    e.Cell.Row.Cells["PartSeq"].Value = e.Cell.Row.RowSelectorNumber;
                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        //#region 이벤트
        // 행이 공백이면 자동 행삭제
        private void uGrid_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                if (wGrid.mfCheckCellDataInRow(uGrid, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀수정시 RowSelector Image 설정하는 이벤트
        private void uGrid_CellChaging(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridMSADataList.Height = 55;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridMSADataList.Height = 715;
                    for (int i = 0; i < uGridMSADataList.Rows.Count; i++)
                    {
                        uGridMSADataList.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 헤더 그리드 더블클릭 이벤트
        private void uGridMSADataList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // 더블클릭행 고정
                e.Row.Fixed = true;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 그룹박스 펼침상태로 변경
                this.uGroupBoxContentsArea.Expanded = true;

                PlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strProjectCode = e.Row.Cells["ProjectCode"].Value.ToString();

                // 헤더 상세조회 Method 호출
                Search_HeaderDetail(strProjectCode, m_resSys.GetString("SYS_LANG"));
                
                // Tab 메뉴 생성
                CreateTab(strProjectCode);
                SearchData(PlantCode, strProjectCode, m_resSys.GetString("SYS_LANG"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQAT0021_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
