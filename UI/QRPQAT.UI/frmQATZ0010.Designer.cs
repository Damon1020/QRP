﻿namespace QRPQAT.UI
{
    partial class frmQATZ0010
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0010));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchEquipType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridAdmit = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAdmit)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchEquipType
            // 
            this.uComboSearchEquipType.Location = new System.Drawing.Point(99, 36);
            this.uComboSearchEquipType.Name = "uComboSearchEquipType";
            this.uComboSearchEquipType.Size = new System.Drawing.Size(173, 21);
            this.uComboSearchEquipType.TabIndex = 193;
            this.uComboSearchEquipType.Text = "ultraComboEditor1";
            this.uComboSearchEquipType.ValueChanged += new System.EventHandler(this.uComboSearchEquipType_ValueChanged);
            // 
            // uLabelSearchEquipType
            // 
            this.uLabelSearchEquipType.Location = new System.Drawing.Point(10, 36);
            this.uLabelSearchEquipType.Name = "uLabelSearchEquipType";
            this.uLabelSearchEquipType.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchEquipType.TabIndex = 192;
            this.uLabelSearchEquipType.Text = "ultraLabel1";
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(370, 36);
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(185, 21);
            this.uComboSearchEquipGroup.TabIndex = 191;
            this.uComboSearchEquipGroup.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(281, 36);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchEquipGroup.TabIndex = 190;
            this.uLabelSearchEquipGroup.Text = "ultraLabel1";
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(370, 12);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(185, 21);
            this.uComboSearchPackage.TabIndex = 189;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(281, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPackage.TabIndex = 188;
            this.uLabelSearchPackage.Text = "ultraLabel1";
            // 
            // uTextSearchCustomerName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance21;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(187, 12);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchCustomerName.TabIndex = 26;
            // 
            // uTextSearchCustomerCode
            // 
            appearance22.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance22;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomerCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchCustomerCode.Location = new System.Drawing.Point(99, 12);
            this.uTextSearchCustomerCode.Name = "uTextSearchCustomerCode";
            this.uTextSearchCustomerCode.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchCustomerCode.TabIndex = 25;
            this.uTextSearchCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchCustomerCode_KeyDown);
            this.uTextSearchCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomerCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(10, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCustomer.TabIndex = 24;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(891, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(18, 21);
            this.uComboSearchPlant.TabIndex = 23;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(871, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(18, 21);
            this.uLabelSearchPlant.TabIndex = 22;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uGridAdmit
            // 
            this.uGridAdmit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAdmit.DisplayLayout.Appearance = appearance5;
            this.uGridAdmit.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAdmit.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAdmit.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAdmit.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridAdmit.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAdmit.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridAdmit.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAdmit.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAdmit.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAdmit.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridAdmit.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAdmit.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAdmit.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAdmit.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridAdmit.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAdmit.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAdmit.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridAdmit.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridAdmit.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAdmit.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridAdmit.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridAdmit.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAdmit.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridAdmit.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAdmit.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAdmit.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAdmit.Location = new System.Drawing.Point(0, 100);
            this.uGridAdmit.Name = "uGridAdmit";
            this.uGridAdmit.Size = new System.Drawing.Size(917, 740);
            this.uGridAdmit.TabIndex = 2;
            this.uGridAdmit.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAdmit_AfterCellUpdate);
            this.uGridAdmit.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridAdmit_DoubleClickRow);
            this.uGridAdmit.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAdmit_AfterCellListCloseUp);
            this.uGridAdmit.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAdmit_CellChange);
            // 
            // frmQATZ0010
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridAdmit);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0010";
            this.Load += new System.EventHandler(this.frmQATZ0010_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0010_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAdmit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAdmit;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipType;
    }
}