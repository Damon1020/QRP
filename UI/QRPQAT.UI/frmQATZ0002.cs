﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : 설비인증관리                                          */
/* 프로그램ID   : frmQATZ0002.cs                                        */
/* 프로그램명   : 설비인증 접수/평가                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPQAT.UI
{
    public partial class frmQATZ0002 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";
        private int intSeq;

        public frmQATZ0002()
        {
            InitializeComponent();
        }

        #region From
        private void frmQATZ0002_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void frmQATZ0002_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
            
        }

        private void frmQATZ0002_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0002_Load(object sender, EventArgs e)
        {
            SetRunMode(); //디버깅위한 매서드
            SetToolAuth();

            //컨트롤초기화
            InitTitle();
            InitLabel();
            InitText();
            InitGrid();
            InitComboBox();
            InitValue();
            InitButton();
            InitTab();
            //uGroupBoxContentsArea숨기기
            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            

        }

        #endregion

        #region 컨트롤초기화

        /// <summary>
        /// 디버깅위한 매서드
        /// </summary>
        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 타이틀초기화
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //타이틀지정
                titleArea.mfSetLabelText("설비인증 접수/평가", m_resSys.GetString("SYS_FONTNAME"), 12);

                // E-Mail TEST
                //if (m_resSys.GetString("SYS_USERID").Equals("TESTUSER"))
                //    this.uButtonEMailTest.Visible = true;
                //else
                //    this.uButtonEMailTest.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값지정

                this.uTextPlantCode.Hide();
                this.uTextMDMTFlag.Hide();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                
                wButton.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_FillDown);
                wButton.mfSetButton(this.uButtonDelRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinTabControl wTab = new QRPCOM.QRPUI.WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlantName, "공장", m_resSys.GetString("SYSFONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel1, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel2, "설비인증의뢰유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel3, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.ultraLabel4, "의뢰번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel3, "의뢰일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel4, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel5, "설비인증의뢰유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel8, "설비인증 평가", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabel6, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel7, "설비Code", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel9, "Maker", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel10, "SerialNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel13, "목적", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPackage, "적용 PKG", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel14, "관련표준", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelSetup, "Setup현황", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMold, "금형치공구현황", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel15, "판정결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel16, "인증담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel17, "인증일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel18, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel5, "승인일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel19, "가동조건", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel20, "설비인증항목 및 평가결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel21, "첨부문서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel6, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.ultraLabel7, "진행상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchCertiReaDate, "인증의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode UCC = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(UCC);



                // Call Method

                DataTable dtEquipCertiReqTypeCode = UCC.mfReadUserCommonCode("EQU", "U0002", m_resSys.GetString("SYS_LANG"));
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //설비인증의뢰유형 콤보
                wCombo.mfSetComboEditor(this.uComboEquipCertiReqTypeCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "ComCode", "ComCodeName", dtEquipCertiReqTypeCode);

                // 공장콤보
                wCombo.mfSetComboEditor(this.uComboSerchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitValue()
        {
            try
            {
                intSeq = 0;
                this.uTextAdmitStatusCode.Text = "";
                this.uTextCertiAdmitID.Text = "";
                this.uTextCertiAdmitName.Text = "";
                this.uTextCertiChargeID.Text = "";
                this.uTextCertiChargeName.Text = "";
                this.uTextCertiReqDate.Text = "";
                this.uTextCertiReqDept.Text = "";
                this.uTextCertiReqID.Text = "";
                this.uTextCertiReqName.Text = "";
                this.uTextDocCode.Text = "";
                this.uTextEquipCertiReqTypeCode.Text = "";
                this.uTextEquipCode.Text = "";
                this.uTextObject.Text = "";
                this.uTextProcess.Text = "";
                this.uTextRejectReason.Text = "";
                this.uTextVendorName.Text = "";
                this.uTextSerial.Text = "";
                this.uTextPlantCode.Text = "";
                this.uTextPlantName.Text = "";
                this.uTextModel.Clear();

                this.uComboEquipCertiReqTypeCode.Value = "";

                this.uDateCertiAdmitDate.Value = DateTime.Now;
                this.uDateCertiDate.Value = DateTime.Now;
    
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }

                while (this.uGrid3.Rows.Count > 0)
                {
                    this.uGrid3.Rows[0].Delete(false);
                }

                while (this.uGrid4.Rows.Count > 0)
                {
                    this.uGrid4.Rows[0].Delete(false);
                }
                this.uCheckEquipStandardFlag.Enabled = false;
                this.uCheckWorkStandardFlag.Enabled = false;
                this.uCheckTechStandardFlag.Enabled = false;
                this.uCheckMESTFlag.Enabled = false;

                this.uCheckMESTFlag.Hide();
                this.uTextMDMTFlag.Text = "F";

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                #region 기본설정
                //기본설정
                //--설비인증정보
                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //Sub설비 정보
                grd.mfInitGeneralGrid(this.uGridSubEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--가동조건
                grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--설비인증항목및평가결과
                grd.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--첨부문서
                grd.mfInitGeneralGrid(this.uGrid4, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--Setup
                grd.mfInitGeneralGrid(this.uGridSetup, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--금형치공구
                grd.mfInitGeneralGrid(this.uGridMoldTool, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--설비인증평가
                grd.mfInitGeneralGrid(this.uGrid5, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                #endregion

                //컬럼설정
                #region 설비인증정보
                //--설비인증정보
                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "DocCode", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiReqDeptName", "의뢰부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 12
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiReqDate", "의뢰일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiReqID", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiReqName", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipCertiReqTypeCode", "설비인증의뢰유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "VendorCode", "VendorCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "Object", "Object", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "EquipStandardFlag", "EquipStandardFlag", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "WorkStandardFlag", "WorkStandardFlag", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "TechStandardFlag", "TechStandardFlag", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "VendorName", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "ModelName", "ModelName", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", ""); // 2012-11-16 추가

                grd.mfSetGridColumn(this.uGrid1, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MESTFlag", "MES전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MESTDate", "MES전송일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiResultName", "판정결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");  // 2012-12-04 추가
                
                grd.mfSetGridColumn(this.uGrid1, 0, "AdmitStatus", "AdmitStatusCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "AdmitStatusCode", "승인상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiAdmitID", "인증승인자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiChargeID", "인증담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "CertiResultCode", "인증결과코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MDMTFlag", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "MDMTDate", "MDM전송일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 부속설비

                //--Sub설비
                grd.mfSetGridColumn(this.uGridSubEquip, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubEquip, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubEquip, 0, "ModelName", "모델명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 100
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubEquip, 0, "VendorCode", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 100
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSubEquip, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 100
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                #endregion

                #region 가동조건
                //--가동조건
                grd.mfSetGridColumn(this.uGrid2, 0, "SpecStandard", "SPEC기준/장비사양", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "Parameter", "Parameter", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "WorkCondition", "가동조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 설비인증항목 및 평가
                //--설비인증항목및평가결과
                grd.mfSetGridColumn(this.uGrid3, 0, "CertiItem", "인증항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "JudgeStandard", "판정기준", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "EvalResultCode", "평가결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid3, 0, "JudgeResultCode", "판정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");


                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //QRPSYS.BL.SYSPGM.CommonCode UCC = new QRPSYS.BL.SYSPGM.CommonCode();
                //brwChannel.mfCredentials(UCC);
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode UCC = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(UCC);

                ////판정결과 콤보박스만들기
                DataTable dtJResult = UCC.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                ////그리드에 추가
                grd.mfSetGridColumnValueList(this.uGrid3, 0, "JudgeResultCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtJResult);

                #endregion

                #region 첨부문서

                //--첨부문서
                grd.mfSetGridColumn(this.uGrid4, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid4, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", ""); //필요함

                grd.mfSetGridColumn(this.uGrid4, 0, "Subject", "문서제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid4, 0, "FileName", "파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid4, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region 설비인증평가
                //설비인증평가
                grd.mfSetGridColumn(this.uGrid5, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid5, 0, "InspectGroupCode", "InspectGroupCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "InspectTypeCode", "InspectTypeCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "InspectItemCode", "InspectItemCode", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "CertiItem", "인증항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                //////grd.mfSetGridColumn(this.uGrid5, 0, "JudgeStandard", "판정기준", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1000
                //////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGrid5, 0, "UpperSpec", "규격상한", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid5, 0, "LowerSpec", "규격하한", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGrid5, 0, "EvalResultCode", "평가결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 250, false, false, 1000
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGrid5, 0, "EvalResultCode", "평가결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 250, false, false, 1000
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid5, 0, "JudgeResultCode", "판정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //QRPSYS.BL.SYSPGM.CommonCode UCC = new QRPSYS.BL.SYSPGM.CommonCode();
                //brwChannel.mfCredentials(UCC);

                ////판정결과 콤보박스만들기
                DataTable dtJudgeResult = UCC.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                ////그리드에 추가
                grd.mfSetGridColumnValueList(this.uGrid5, 0, "JudgeResultCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtJudgeResult);
                #endregion

                #region Setup
                //--Setup
                grd.mfSetGridColumn(this.uGridSetup, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 10
                    , Infragistics.Win.HAlign.Right,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridSetup, 0, "SetupDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGridSetup, 0, "ProgressDesc", "진행현황", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region 금형치공구현황
                //--금형 치공구 현황
                grd.mfSetGridColumn(this.uGridMoldTool, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 10
                    , Infragistics.Win.HAlign.Right,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "DurableMatCode", "금형mmmmm치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "Standard", "규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMoldTool, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid4.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid4.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGrid5.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid5.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                grd.mfAddRowGrid(this.uGrid5, 0);

                uGridMoldTool.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMoldTool.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridSetup.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridSetup.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void Writable()
        {
            try
            {
                this.uTextRejectReason.ReadOnly = false;
                this.uTextCertiAdmitID.ReadOnly = false;
                this.uTextCertiChargeID.ReadOnly = false;

                this.uOptionCertiResultCode.Enabled = true;

                this.uDateCertiAdmitDate.ReadOnly = false;
                this.uDateCertiDate.ReadOnly = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void DisWritable()
        {
            try
            {
                this.uTextRejectReason.ReadOnly = true;
                this.uTextCertiAdmitID.ReadOnly = true;
                this.uTextCertiChargeID.ReadOnly = true;

                this.uOptionCertiResultCode.Enabled = false;

                this.uDateCertiAdmitDate.ReadOnly = true;
                this.uDateCertiDate.ReadOnly = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }



        #endregion

        #region 툴바기능
        public void mfSearch()
        {
            try
            {

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();

                InitGrid();
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                //주석은 디버깅용

                QRPEQU.BL.EQUCCS.EQUEquipCertiH CertiH;
                if (m_bolDebugMode == false)
                {
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                CertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                brwChannel.mfCredentials(CertiH);
                }
                else
                    CertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH(m_strDBConn);

                string strPlantCode = this.uComboSerchPlant.Value.ToString();
                string strProcessCode = this.uComboSearchProcess.Value.ToString();
                string strEquipCertiReqTypeCode = this.uComboEquipCertiReqTypeCode.Value.ToString();
                string strEquipCode = this.uTextSearchEquipCode.Text;
                string strCertiReqDateFrom = Convert.ToDateTime(this.uDateCertiReqDateFrom.Value).ToString("yyyy-MM-dd");
                string strCertiReqDateTo = Convert.ToDateTime(this.uDateCertiReqDateTo.Value).ToString("yyyy-MM-dd");

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtH = CertiH.mfReadEQUEquipCertiHForAdmit(strPlantCode, strProcessCode, strEquipCertiReqTypeCode
                                                                       , strEquipCode, strCertiReqDateFrom, strCertiReqDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGrid1.DataSource = dtH;
                this.uGrid1.DataBind();

                // PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                if (this.uGrid1.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        //승인이 되었지만 전송이 안되어 있는 줄은 표시함.
                        if (this.uGrid1.Rows[i].Cells["AdmitStatusCode"].Value.ToString() == "FN" && this.uGrid1.Rows[i].Cells["MESTFlag"].Value.ToString() == "F")
                            this.uGrid1.DisplayLayout.Rows[i].Appearance.BackColor = System.Drawing.Color.Salmon;
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000881", "M000986", Infragistics.Win.HAlign.Center);

                    return;
                }
                if (this.uTextCertiAdmitID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000769", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextCertiAdmitID.Focus();
                    return;
                }
                if (this.uCheckCompleteFlag.Checked)
                {
                    if (this.uOptionCertiResultCode.Value == null)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001245", "M001246", Infragistics.Win.HAlign.Center);

                        this.uOptionCertiResultCode.Focus();
                        return;
                    }
                }
                if (this.uTextCertiChargeID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000857", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextCertiChargeID.Focus();
                    return;
                }



                DataTable dtH = new DataTable();
                DataTable SaveItemQC = new DataTable();
                DataTable DelItemQC = new DataTable();
                DataRow row;


                //헤더 BL
                QRPBrowser brwChannel = new QRPBrowser();
                QRPEQU.BL.EQUCCS.EQUEquipCertiH CertiH;
                QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC ItemQC;

                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                    CertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                    brwChannel.mfCredentials(CertiH);

                    //Item BL
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC), "EQUEquipCertiItemQC");
                    ItemQC = new QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC();
                    brwChannel.mfCredentials(ItemQC);
                }
                //디버깅용
                else
                {
                    CertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH(m_strDBConn);
                    ItemQC = new QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC();
                }

                dtH = CertiH.mfSetDataInfo();



                row = dtH.NewRow();

                row["PlantCode"] = this.uTextPlantCode.Text;
                row["DocCode"] = this.uTextDocCode.Text;
                row["EquipCode"] = this.uTextEquipCode.Text;
                if (this.uCheckCompleteFlag.Checked == true)
                {
                    row["AdmitStatusCode"] = "FN";
                }
                else
                {
                    row["AdmitStatusCode"] = "AR";
                }
                if (this.uCheckCompleteFlag.Enabled == false)
                {
                    row["WriteComplete"] = "T";
                }
                else
                {
                    row["WriteComplete"] = "F";
                }
                row["RejectReason"] = this.uTextRejectReason.Text;
                row["CertiChargeID"] = this.uTextCertiChargeID.Text;
                row["CertiAdmitID"] = this.uTextCertiAdmitID.Text;
                row["CertiAdmitDate"] = Convert.ToDateTime(this.uDateCertiAdmitDate.Value.ToString()).ToString("yyyy-MM-dd");
                row["CertiDate"] = Convert.ToDateTime(this.uDateCertiDate.Value.ToString()).ToString("yyyy-MM-dd");

                //row["CertiResultCode"] = this.uOptionCertiResultCode.Value.ToString();

                if (this.uOptionCertiResultCode.Value != null)
                {
                    row["CertiResultCode"] = this.uOptionCertiResultCode.Value.ToString();
                }

                if (this.uCheckMESTFlag.Checked == true)
                    row["MESTFlag"] = "T";
                else
                    row["MESTFlag"] = "F";
                if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                {
                    // 작성완료 체크하고 첫 저장시 설비인증의뢰요청에서 MDMTFlag가 'T'로 저장되었으므로 'F'로 바꾸어 저장
                    row["MDMTFlag"] = "F";
                }
                else
                {
                    row["MDMTFlag"] = this.uTextMDMTFlag.Text;
                }


                dtH.Rows.Add(row);

                SaveItemQC = ItemQC.mfSetDataInfo();
                DelItemQC = ItemQC.mfSetDataInfo();
                for (int i = 0; i < this.uGrid5.Rows.Count; i++)
                {
                    this.uGrid5.ActiveCell = this.uGrid5.Rows[0].Cells[0];
                    if (this.uGrid5.Rows[i].Hidden == false)
                    {

                        //if (this.uGrid3.Rows[i].Cells["EvalResultCode"].Value.ToString() == "")
                        //{
                        //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //        , "확인창", "필수입력사항 확인", "평가결과를 입력해주세요", Infragistics.Win.HAlign.Center);

                        //    //Focus
                        //    this.uGrid3.ActiveCell = this.uGrid3.Rows[i].Cells["EvalResultCode"];//.Selected = true;
                        //    this.uGrid3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        //    return;
                        //}
                        //else if (this.uGrid3.Rows[i].Cells["JudgeResultCode"].Value.ToString() == "")
                        //{
                        //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //        , "확인창", "필수입력사항 확인", "판정을 입력해주세요", Infragistics.Win.HAlign.Center);

                        //    //Focus
                        //    this.uGrid3.ActiveCell = this.uGrid3.Rows[i].Cells["JudgeResultCode"];//.Selected = true; 
                        //    this.uGrid3.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        //    return;
                        //}
                        //else
                        //{
                        row = SaveItemQC.NewRow();
                        row["PlantCode"] = this.uTextPlantCode.Text;
                        row["DocCode"] = this.uTextDocCode.Text;
                        row["Seq"] = this.uGrid5.Rows[i].RowSelectorNumber;
                        row["CertiItem"] = this.uGrid5.Rows[i].Cells["CertiItem"].Value.ToString();
                        row["SampleSize"] = this.uGrid5.Rows[i].Cells["SampleSize"].Value.ToString();
                        //row["UpperSpec"] = this.uGrid5.Rows[i].Cells["UpperSpec"].Value.ToString();
                        //row["LowerSpec"] = this.uGrid5.Rows[i].Cells["LowerSpec"].Value.ToString();
                        row["EvalResultCode"] = this.uGrid5.Rows[i].Cells["EvalResultCode"].Value.ToString();
                        row["JudgeResultCode"] = this.uGrid5.Rows[i].Cells["JudgeResultCode"].Value.ToString();

                        SaveItemQC.Rows.Add(row);
                    }
                    else
                    {
                        row = DelItemQC.NewRow();
                        row["PlantCode"] = this.uTextPlantCode.Text;
                        row["DocCode"] = this.uTextDocCode.Text;

                        DelItemQC.Rows.Add(row);
                    }
                }

                DialogResult dialogresult = new DialogResult();

                ////if (this.uCheckCompleteFlag.Enabled == false)
                ////{
                ////   dialogresult = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////        , "확인창", "작성 완료 문서", "작성이 완료된 문서는 수정할 수 없습니다. MES처리 하시겠습니까?", Infragistics.Win.HAlign.Right);


                ////}
                ////else
                ////{
                ////   dialogresult = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                ////                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "저장확인", "저장하시겠습니까?"
                ////                        , Infragistics.Win.HAlign.Right);
                ////}

                ////if (dialogresult == DialogResult.Yes)
                ////{
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                {
                    // 작성완료하고 MDM I/F 실패했을시
                    if (this.uTextMDMTFlag.Text.Equals("F"))
                    {
                        dialogresult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000976"
                            , "M000992"
                            , Infragistics.Win.HAlign.Right);

                        if (dialogresult == DialogResult.Yes)
                        {

                            // MDM I/F 메소드 호출
                            Save_MDM_IF();
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (this.uTextMDMTFlag.Text.Equals("T"))
                    {
                        dialogresult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000976", "M000977", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
                else if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                {
                    // 작성완료 체크하고 첫 저장시 설비인증의뢰요청에서 MDMTFlag 'F'로 설정
                    this.uTextMDMTFlag.Text = "F";
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M001039"
                                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;
                    
                    
                    

                    DataTable dtFile = new DataTable();
                    // 2012-12-17 임시주석처리 설비인증의뢰통보서 완성될시 주석해제요망.
                    //dtFile = RtnFileInfo(m_resSys.GetString("SYS_LANG"));

                    string strErrRtn = CertiH.mfSaveEQUEquipCertiHForAdmit(dtH, SaveItemQC, DelItemQC, dtFile, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), this.Name);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum == 0)
                    {
                        // 2012-12-17 임시주석처리 설비인증의뢰통보서 완성될시 주석해제요망.
                        //string strFileName = WriteActiveReport(m_resSys.GetString("SYS_LANG"));

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 2012-12-17 임시주석처리 설비인증의뢰통보서 완성될시 주석해제요망.
                        //SendMail(strFileName);

                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001135", "M001037", "M000930",
                                              Infragistics.Win.HAlign.Right);
                        if (Save_MDMIF())
                        {
                            InitValue();
                            this.uGroupBoxContentsArea.Expanded = false;
                            mfSearch();
                        }
                    }
                    else
                    {
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000953",
                                            Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", ErrRtn.ErrMessage,
                                            Infragistics.Win.HAlign.Right);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            InitValue();
            InitGrid();
            try
            {
                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid1.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid1);
                }

                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        ////// 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        ////private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        ////{
        ////    try
        ////    {
        ////        QRPGlobal grdImg = new QRPGlobal();
        ////        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        ////        QRPCOM.QRPUI.WinGrid grd = new WinGrid();
        ////        if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
        ////            e.Cell.Row.Delete(false);
        ////    }
        ////    catch(Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        private void uComboSerchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtProcess = new DataTable();



                String strPlantCode = this.uComboSerchPlant.Value.ToString();

                this.uComboSearchProcess.Items.Clear();

                strPlantCode = this.uComboSerchPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }
                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 175);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 720;
                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid3, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();

                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiFile), "EQUEquipCertiFile");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItem), "EQUEquipCertiItem");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupCondition), "EQUSetupCondition");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipSetup), "EQUEquipSetup");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUSetupDurableMat), "EQUSetupDurableMat");
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC), "EQUEquipCertiItemQC");

                QRPEQU.BL.EQUCCS.EQUEquipCertiH clsH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                QRPEQU.BL.EQUCCS.EQUEquipCertiFile CFile = new QRPEQU.BL.EQUCCS.EQUEquipCertiFile();
                QRPEQU.BL.EQUCCS.EQUEquipCertiItem Item = new QRPEQU.BL.EQUCCS.EQUEquipCertiItem();
                QRPEQU.BL.EQUCCS.EQUSetupCondition Condition = new QRPEQU.BL.EQUCCS.EQUSetupCondition();
                QRPEQU.BL.EQUCCS.EQUEquipSetup Setup = new QRPEQU.BL.EQUCCS.EQUEquipSetup();
                QRPEQU.BL.EQUCCS.EQUSetupDurableMat Durable = new QRPEQU.BL.EQUCCS.EQUSetupDurableMat();
                QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC ItemQC = new QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC();

                brwChannel.mfCredentials(clsH);
                brwChannel.mfCredentials(CFile);
                brwChannel.mfCredentials(Item);
                brwChannel.mfCredentials(Condition);
                brwChannel.mfCredentials(Setup);
                brwChannel.mfCredentials(Durable);
                brwChannel.mfCredentials(ItemQC);

                //검색 파라미터 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strDocCode = e.Row.Cells["DocCode"].Value.ToString();

                DataTable dtH = clsH.mfReadEquipCertiH_Detail(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                if (dtH.Rows.Count <= 0)
                    return;


                //그리드1에 미리 저장한 컴포넌트들의 값 대입
                this.uTextDocCode.Text = e.Row.Cells["DocCode"].Value.ToString();
                this.uTextPlantCode.Text = e.Row.Cells["PlantCode"].Value.ToString();
                this.uTextPlantName.Text = e.Row.Cells["PlantName"].Value.ToString();
                this.uTextCertiReqID.Text = e.Row.Cells["CertiReqID"].Value.ToString();
                this.uTextCertiReqName.Text = e.Row.Cells["CertiReqName"].Value.ToString();
                this.uTextCertiReqDept.Text = e.Row.Cells["CertiReqDeptName"].Value.ToString();
                this.uTextCertiReqDate.Text = e.Row.Cells["CertiReqDate"].Value.ToString();
                this.uTextEquipCertiReqTypeCode.Text = e.Row.Cells["EquipCertiReqTypeCode"].Value.ToString();
                this.uTextProcess.Text = e.Row.Cells["ProcessName"].Value.ToString();
                this.uTextEquipCode.Text = e.Row.Cells["EquipCode"].Value.ToString();
                this.uTextEquipName.Text = e.Row.Cells["EquipName"].Value.ToString();
                this.uTextVendorName.Text = e.Row.Cells["VendorName"].Value.ToString();
                this.uTextSerial.Text = e.Row.Cells["SerialNo"].Value.ToString();
                this.uTextObject.Text = e.Row.Cells["Object"].Value.ToString();
                this.uTextAdmitStatusCode.Text = e.Row.Cells["AdmitStatusCode"].Value.ToString();
                this.uTextCertiAdmitID.Text = e.Row.Cells["CertiAdmitID"].Value.ToString();
                this.uTextCertiChargeID.Text = e.Row.Cells["CertiChargeID"].Value.ToString();
                this.uOptionCertiResultCode.Value = e.Row.Cells["CertiResultCode"].Value.ToString();

                this.uTextPackage.Text = dtH.Rows[0]["Package"].ToString();
                this.uTextModel.Text = dtH.Rows[0]["ModelName"].ToString();

                if (dtH.Rows[0]["CertiAdmitDate"].ToString() !="") 
                {
                    this.uDateCertiAdmitDate.Value = Convert.ToDateTime(dtH.Rows[0]["CertiAdmitDate"]).ToString("yyyy-MM-dd");
                }
                if (dtH.Rows[0]["CertiDate"].ToString() != "")
                {
                    this.uDateCertiDate.Value = Convert.ToDateTime(dtH.Rows[0]["CertiDate"]).ToString("yyyy-MM-dd");
                }

                //CertiAdmitID가 있을경우 이름 검색
                if (this.uTextCertiAdmitID.Text != "")
                    this.uTextCertiAdmitName.Text = GetUserName(strPlantCode, this.uTextCertiAdmitID.Text);
                //CertiChargeID가 있을경우 이름 검색
                if (this.uTextCertiChargeID.Text != "")
                    this.uTextCertiChargeName.Text = GetUserName(strPlantCode, this.uTextCertiChargeID.Text);

                if (dtH.Rows[0]["AdmitStatus"].ToString() == "FN") 
                    this.uCheckCompleteFlag.Checked = true;
                else
                    this.uCheckCompleteFlag.Checked = false;


                if (e.Row.Cells["EquipStandardFlag"].Value.ToString() == "T")
                    this.uCheckEquipStandardFlag.Checked = true;
                else
                    this.uCheckEquipStandardFlag.Checked = false;
                
                if (e.Row.Cells["WorkStandardFlag"].Value.ToString() == "T")
                {
                    this.uCheckWorkStandardFlag.Checked = true;
                } 
                else
                {
                    this.uCheckWorkStandardFlag.Checked = false;
                }
                if (e.Row.Cells["TechStandardFlag"].Value.ToString() == "T")
                {
                    this.uCheckTechStandardFlag.Checked = true;
                } 
                else
                {
                    this.uCheckTechStandardFlag.Checked = false;
                }
                if (e.Row.Cells["MESTFlag"].Value.ToString() == "T")
                {
                    this.uCheckMESTFlag.Checked = true;
                }
                else
                {
                    this.uCheckMESTFlag.Checked = false;
                }

                if (this.uCheckCompleteFlag.Checked == true)
                    this.uCheckCompleteFlag.Enabled = false;
                else
                    this.uCheckCompleteFlag.Enabled = true;

                this.uTextMDMTFlag.Text = e.Row.Cells["MDMTFlag"].Value.ToString();

                //검색 BL 호출
                DataTable dtCFile = CFile.mfReadCertiFile(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtItem = Item.mfReadCertiItem(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtCondition = Condition.mfReadSetupCondition(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtSetup = Setup.mfReadEquipSetup(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtDurable = Durable.mfReadDurableMat(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtQT = ItemQC.mfReadCertiItemQC(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtSubEquip = clsH.mfReadEQUEquipCertiSubEquip(strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));
                
                WinGrid grd = new WinGrid();


                //그리드에 데이터 바인딩
                this.uGrid2.DataSource = dtCondition;
                this.uGrid2.DataBind();
                if (dtCondition.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);

                this.uGrid4.DataSource = dtCFile;
                this.uGrid4.DataBind();
                if (dtCFile.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid4, 0);

                this.uGrid3.DataSource = dtItem;
                this.uGrid3.DataBind();
                if (dtItem.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid3, 0);

                this.uGridSetup.DataSource = dtSetup;
                this.uGridSetup.DataBind();
                if (dtSetup.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGridSetup, 0);

                this.uGridMoldTool.DataSource = dtDurable;
                this.uGridMoldTool.DataBind();
                if (dtDurable.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGridMoldTool, 0);

                ////this.uGrid5.DataSource = dtQT;
                ////this.uGrid5.DataBind();
                ////if (dtQT.Rows.Count > 0)
                ////    grd.mfSetAutoResizeColWidth(this.uGrid5, 0);

                string strPackage = this.uTextPackage.Text;
                string strProcessCode = e.Row.Cells["ProcessCode"].Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");


                DataTable dtSpecD = ItemQC.mfReadCertiItemQC(strPlantCode, strDocCode, m_resSys.GetString("SYS_LANG"));
                this.uGrid5.DataSource = dtSpecD;
                this.uGrid5.DataBind();

                //for (int i = 0; i < dtSpecD.Rows.Count; i++)
                //{
                //    if (!dtSpecD.Rows[0]["InspectGroupCode"].ToString().Equals(string.Empty))
                //    {
                //        string strInspectGroupCode = dtSpecD.Rows[i]["InspectGroupCode"].ToString();

                //        this.uGrid5.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                //        this.uGrid5.Rows[i].Cells["InspectGroupCode"].Value = dtSpecD.Rows[i]["InspectGroupCode"].ToString();
                //        this.uGrid5.Rows[i].Cells["InspectTypeCode"].Value = dtSpecD.Rows[i]["InspectItemCode"].ToString();
                //        this.uGrid5.Rows[i].Cells["CertiItem"].Value = dtSpecD.Rows[i]["CertiItem"].ToString();
                //        this.uGrid5.Rows[i].Cells["SampleSize"].Value = dtSpecD.Rows[i]["SampleSize"].ToString();
                //        this.uGrid5.Rows[i].Cells["UpperSpec"].Value = dtSpecD.Rows[i]["UpperSpec"].ToString();
                //        this.uGrid5.Rows[i].Cells["LowerSpec"].Value = dtSpecD.Rows[i]["LowerSpec"].ToString();
                //        this.uGrid5.Rows[i].Cells["EvalResultCode"].Value = dtSpecD.Rows[i]["EvalResultCode"].ToString();
                //        this.uGrid5.Rows[i].Cells["JudgeResultCode"].Value = dtSpecD.Rows[i]["JudgeResultCode"].ToString();
                //    }
                //}

                this.uGridSubEquip.DataSource = dtSubEquip;
                this.uGridSubEquip.DataBind();
                if (dtSubEquip.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGridSubEquip, 0);

                //for (int i = 0; i < this.uGrid5.Rows.Count; i++)
                //{
                //    String strTrim = this.uGrid5.Rows[i].Cells["EvalResultCode"].Value.ToString().Trim();

                //    this.uGrid5.Rows[i].Cells["EvalResultCode"].Value = strTrim;
                //}


                DialogResult DResult = new DialogResult();

                if (this.uCheckCompleteFlag.Checked == true)
                {
                    DisWritable();
                }
                else
                {
                    Writable();
                }

                this.uGroupBoxContentsArea.Expanded = true;


                //문서번호가 없을때 검색 안됌
                if(this.uTextDocCode.Text == "")
                {
                    WinMessageBox msg = new WinMessageBox();

                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    this.uGroupBoxContentsArea.Expanded = false;
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGrid4_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (e.Cell.Column.ToString() == "FileName")
                {
                    if (e.Cell.Value.ToString() == "" || e.Cell.Value.ToString().Contains(":\\") == true)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000357",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                   
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0003");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        if (e.Cell.Value.ToString().Contains(":\\") == false)
                        {
                            arrFile.Add(e.Cell.Value.ToString());
                        }

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\", 
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region TextEvent

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSerchPlant.Value.ToString().Equals(string.Empty) || uComboSerchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = uComboSerchPlant.Value.ToString();
                frmEquip.ShowDialog();

                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;
                this.uTextEquipSearchName.Text = frmEquip.EquipName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            { }
            
        }

        private void uTextCertiChargeID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            
            try
            {
                if (uTextPlantCode.Text.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uTextPlantCode.Text;
                frmUser.ShowDialog();

                this.uTextCertiChargeID.Text = frmUser.UserID;
                this.uTextCertiChargeName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            { }
            
        }

        private void uTextCertiAdmitID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uTextPlantCode.Text.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uTextPlantCode.Text;
                frmUser.ShowDialog();

                this.uTextCertiAdmitID.Text = frmUser.UserID;
                this.uTextCertiAdmitName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            { }
            
        }

        private void uTextCertiChargeID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextCertiChargeID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextCertiChargeID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextCertiAdmitID.Text = "";
                            this.uTextCertiAdmitName.Text = "";
                        }

                        else
                        {
                            this.uTextCertiChargeName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextCertiAdmitID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextCertiAdmitID.Text != "")
                    {
                        
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextCertiAdmitID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextCertiAdmitID.Text = "";
                            this.uTextCertiAdmitName.Text = "";
                        }
                        else
                        {
                            this.uTextCertiAdmitName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Button Event

        private void uButtonDeny_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if(this.uTextDocCode.Text.Equals(string.Empty))
                    return;

                if (this.uTextRejectReason.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M000415", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextRejectReason.Focus();
                    return;
                }
                else
                {
                    DialogResult dialogresult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000419"
                                            , Infragistics.Win.HAlign.Right);
                    if (dialogresult == DialogResult.Yes)
                    {
                        DataTable dtH = new DataTable();
                        DataTable SaveItemQc = new DataTable();
                        DataTable DelItemQc = new DataTable();
                        DataRow row;


                        //헤더 BL
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                        QRPEQU.BL.EQUCCS.EQUEquipCertiH CertiH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                        brwChannel.mfCredentials(CertiH);

                        //Item BL
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC), "EQUEquipCertiItemQC");
                        QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC ItemQC = new QRPEQU.BL.EQUCCS.EQUEquipCertiItemQC();
                        brwChannel.mfCredentials(ItemQC);

                        this.uCheckCompleteFlag.Checked = false;

                        dtH = CertiH.mfSetDataInfo();

                        row = dtH.NewRow();

                        row["PlantCode"] = this.uTextPlantCode.Text;
                        row["DocCode"] = this.uTextDocCode.Text;
                        row["AdmitStatusCode"] = "RE";
                        row["RejectReason"] = this.uTextRejectReason.Text;
                        row["CertiChargeID"] = this.uTextCertiChargeID.Text;
                        row["CertiAdmitID"] = this.uTextCertiAdmitID.Text;
                        row["CertiAdmitDate"] = this.uDateCertiAdmitDate.Value.ToString();
                        row["CertiDate"] = this.uDateCertiDate.Value.ToString();

                        row["CertiResultCode"] = this.uOptionCertiResultCode.Value;

                        dtH.Rows.Add(row);

                        SaveItemQc = ItemQC.mfSetDataInfo();
                        DelItemQc = ItemQC.mfSetDataInfo();
                        for (int i = 0; i < this.uGrid5.Rows.Count; i++)
                        {
                            this.uGrid5.ActiveCell = this.uGrid5.Rows[0].Cells[0];
                            if (this.uGrid5.Rows[i].Hidden == false)
                            {
                                row = SaveItemQc.NewRow();
                                row["PlantCode"] = this.uTextPlantCode.Text;
                                row["DocCode"] = this.uTextDocCode.Text;
                                row["Seq"] = this.uGrid5.Rows[i].RowSelectorNumber;
                                row["CertiItem"] = this.uGrid5.Rows[i].Cells["CertiItem"].Value.ToString();
                                row["SampleSize"] = this.uGrid5.Rows[i].Cells["SampleSize"].Value.ToString();
                                row["JudgeStandard"] = this.uGrid5.Rows[i].Cells["JudgeStandard"].Value.ToString();
                                row["EvalResultCode"] = this.uGrid5.Rows[i].Cells["EvalResultCode"].Value.ToString();
                                row["JudgeResultCode"] = this.uGrid5.Rows[i].Cells["JudgeResultCode"].Value.ToString();

                                SaveItemQc.Rows.Add(row);
                            }
                            else
                            {
                                row = DelItemQc.NewRow();
                                row["PlantCode"] = this.uTextPlantCode.Text;
                                row["DocCode"] = this.uTextDocCode.Text;

                                DelItemQc.Rows.Add(row);
                            }
                            
                        }

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        DataTable dtFeil = new DataTable();
                        dtFeil = CertiH.mfSetDataInfo(); // 줄수 0으로 보냄 동일한 컬럼은 아님


                        string strErrRtn = CertiH.mfSaveEQUEquipCertiHForAdmit(dtH, SaveItemQc, DelItemQc, dtFeil, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), this.Name);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum == 0)
                        {
                            DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                  "M001135", "M001037", "M000930",
                                                  Infragistics.Win.HAlign.Right);

                            InitValue();
                            InitGrid();
                            this.uGroupBoxContentsArea.Expanded = false;
                        }
                        else
                        {
                            DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000953",
                                            Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid4.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGrid4.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\")
                            || this.uGrid4.Rows[i].Cells["FileName"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if(intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0003");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();



                        for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid4.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (this.uGrid4.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                                {
                                    arrFile.Add(this.uGrid4.Rows[i].Cells["FileName"].Value.ToString());
                                }
                            }
                        }
                        if (arrFile.Count != 0)
                        {
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());

                            fileAtt.ShowDialog();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDelRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid5.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid5.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid5.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #endregion

        #region Method

        /// <summary>
        /// MDM I/F 메소드
        /// </summary>
        /// <returns>저장 결과 (true:성공 | false:실패)</returns>
        private bool Save_MDM_IF()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                bool bolReturn = false;

                if (m_resSys.GetString("SYS_SERVERPATH").Contains("10.61.61.71") || m_resSys.GetString("SYS_SERVERPATH").Contains("10.61.61.73"))
                {

                    WinMessageBox msg = new WinMessageBox();
                   
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                    QRPEQU.BL.EQUCCS.EQUEquipCertiH clsEquCerti = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                    brwChannel.mfCredentials(clsEquCerti);

                    string strErrRtn = clsEquCerti.mfSave_MDMIF_QRP_MCH_SPEC(this.uTextPlantCode.Text
                                                                            , this.uTextDocCode.Text
                                                                            , "Y"
                                                                            , m_resSys.GetString("SYS_USERID")
                                                                            , m_resSys.GetString("SYS_USERIP"));

                    // 결과검사
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        bolReturn = true;
                    }
                    else
                    {
                        bolReturn = false;
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000230", "M000084"
                                                                    , "M000085"
                                                                    , Infragistics.Win.HAlign.Center);
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000230", "M000084"
                                                                    , ErrRtn.ErrMessage
                                                                    , Infragistics.Win.HAlign.Center);
                        }

                    }

                }
                else
                    return true;

                return bolReturn;
                    
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// MDM I/F 메소드
        /// </summary>
        /// <returns>저장 결과 (true:성공 | false:실패)</returns>
        private bool Save_MDMIF()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                bool bolReturn = false;

                if (m_resSys.GetString("SYS_SERVERPATH").Contains("10.61.61.71") || m_resSys.GetString("SYS_SERVERPATH").Contains("10.61.61.73"))
                {
                    if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                        QRPEQU.BL.EQUCCS.EQUEquipCertiH clsEquCerti = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                        brwChannel.mfCredentials(clsEquCerti);

                        string strErrRtn = clsEquCerti.mfSave_MDMIF_QRP_MCH_SPEC(this.uTextPlantCode.Text
                                                                                , this.uTextDocCode.Text
                                                                                , "Y"
                                                                                , m_resSys.GetString("SYS_USERID")
                                                                                , m_resSys.GetString("SYS_USERIP"));

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            bolReturn = true;
                        }
                        else
                        {
                            bolReturn = false;
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                        , "M000230", "M000084"
                                                                        , "M000085"
                                                                        , Infragistics.Win.HAlign.Center);
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                        , "M000230", "M000084"
                                                                        , ErrRtn.ErrMessage
                                                                        , Infragistics.Win.HAlign.Center);
                            }

                        }
                    }
                    else
                        return true;

                }
                else
                    return true;

                return bolReturn;

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 ID, 이름 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Active Report 작성( 설비인증/ 통보서) 및 파일 업로드
        /// </summary>
        private string WriteActiveReport(string strLang)
        {
            try
            {
                bool FileUploadCheck = false; // 파일 전송결과
                string strFileName = "";
                if (uCheckCompleteFlag.Checked && uCheckCompleteFlag.Enabled)
                {


                    frmQATZ0002_Report frmReport = new frmQATZ0002_Report(RtnHader(), RtnCondition(), RtnConfirmItem(), RtnMainSubEquip());


                    //리포트가 저장되있는 경로

                    string strBrowserPath = Application.ExecutablePath;
                    int intPos = strBrowserPath.LastIndexOf(@"\");
                    string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "GWpdf\\";

                    string strFileChk = "";

                    QRPBrowser brwChannel = new QRPBrowser();
                    //FileServer 연결
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0003");


                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrAtt = new ArrayList();

                    string strDefaultName = "Req.pdf";

                    // 공장-관리번호-순번-파일이름
                    strFileChk = this.uTextPlantCode.Text + "-" + this.uTextDocCode.Text + "-" + intSeq.ToString() + "-" + strDefaultName;

                    String strUploadFile = m_strQRPBrowserPath + strFileChk;

                    //같은 이름의 파일 있을경우 삭제
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);

                    //파일 카피
                    File.Copy(m_strQRPBrowserPath + strDefaultName, strUploadFile, true);
                    arrAtt.Add(strUploadFile);



                    //추가된 파일들 업로드
                    fileAtt.mfInitSetSystemFileInfo(arrAtt, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                             dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                             dtFilePath.Rows[0]["FolderName"].ToString(),
                                                             dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                             dtSysAccess.Rows[0]["AccessPassword"].ToString());

                    if (arrAtt.Count != 0)
                    {
                        FileUploadCheck = fileAtt.mfFileUpload_NonAssync();
                        //fileAtt.ShowDialog();
                    }
                    if (FileUploadCheck)
                        strFileName = strFileChk;

                }


                return strFileName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return "";
            }
            finally
            { }
        }

        /// <summary>
        /// 설비인증 통보서 Pdf 파일 정보 
        /// </summary>
        /// <returns></returns>
        private DataTable RtnFileInfo(string strLang)
        {
            DataTable dtRtn = new DataTable();

            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiFile), "EQUEquipCertiFile");
                QRPEQU.BL.EQUCCS.EQUEquipCertiFile clsFile = new QRPEQU.BL.EQUCCS.EQUEquipCertiFile();
                brwChannel.mfCredentials(clsFile);

                dtRtn = clsFile.mfSetDataInfo();

                //if (uCheckCompleteFlag.Checked && uCheckCompleteFlag.Enabled)
                //{

                int intCnt = 0;
                int intNum = 0;

                DataTable dtFileInfo = clsFile.mfReadCertiFile(this.uTextPlantCode.Text, this.uTextDocCode.Text, strLang);

                for (int i = 0; i < dtFileInfo.Rows.Count; i++)
                {
                    if (dtFileInfo.Rows[i]["FileName"].ToString().Contains("-Req.pdf"))
                    {
                        intCnt++;
                        intNum = Convert.ToInt32(dtFileInfo.Rows[i]["Seq"]);
                    }
                }
                if (intCnt == 1)
                {
                    DataTable dtRead = clsFile.mfReadACertiFileDocSeq(this.uTextPlantCode.Text, this.uTextDocCode.Text); // 최대순번 찾기

                    if (dtRead.Rows.Count > 0)
                        intSeq = Convert.ToInt32(dtRead.Rows[0]["Seq"]) + 1; // 첨부파일이 있는경우 최대순번 + 1
                    else
                        intSeq = 99;

                }
                else if (intCnt == 2)
                    intSeq = intNum;
                else
                    intSeq = 99;


                DataRow drRow = dtRtn.NewRow();

                drRow["PlantCode"] = this.uTextPlantCode.Text;
                drRow["DocCode"] = this.uTextDocCode.Text;
                drRow["Seq"] = intSeq;
                drRow["Subject"] = "설비인증/의뢰통보서";
                drRow["FileName"] = "Req.pdf";
                drRow["EtcDesc"] = "";

                dtRtn.Rows.Add(drRow);

                //}

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Report를 위한 가동조건 정보
        /// </summary>
        /// <returns></returns>
        private DataTable RtnCondition()
        {
            DataTable dtRtn = new DataTable();

            try
            {

                dtRtn.Columns.Add("SpecStandard", typeof(string));
                dtRtn.Columns.Add("Parameter", typeof(string));
                dtRtn.Columns.Add("WorkCondition", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                //uGrid2

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    DataRow drRow = dtRtn.NewRow();

                    drRow["SpecStandard"] = this.uGrid2.Rows[i].GetCellValue("SpecStandard");
                    drRow["Parameter"] = this.uGrid2.Rows[i].GetCellValue("Parameter");
                    drRow["WorkCondition"] = this.uGrid2.Rows[i].GetCellValue("WorkCondition");
                    drRow["EtcDesc"] = this.uGrid2.Rows[i].GetCellValue("EtcDesc");

                    dtRtn.Rows.Add(drRow);
                }


                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Report를 위한 설비인증 항목 및 평가 결과 정보
        /// </summary>
        /// <returns></returns>
        private DataTable RtnConfirmItem()
        {
            DataTable dtRtn = new DataTable();

            try
            {
                dtRtn.Columns.Add("CertiItem", typeof(string));
                dtRtn.Columns.Add("SampleSize", typeof(string));
                dtRtn.Columns.Add("JudgeStandard", typeof(string));
                dtRtn.Columns.Add("UpperSpec", typeof(string));
                dtRtn.Columns.Add("LowerSpec", typeof(string));
                dtRtn.Columns.Add("EvalResultCode", typeof(string));
                dtRtn.Columns.Add("JudgeResultCode", typeof(string));

                for (int i = 0; i < this.uGrid5.Rows.Count; i++)
                {
                    if (this.uGrid5.Rows[i].Hidden)
                        continue;

                    if (this.uGrid5.Rows[i].GetCellValue("JudgeResultCode").ToString().Equals(string.Empty))
                        continue;

                    DataRow drRow = dtRtn.NewRow();

                    drRow["CertiItem"] = this.uGrid5.Rows[i].GetCellValue("CertiItem");
                    drRow["SampleSize"] = this.uGrid5.Rows[i].GetCellValue("SampleSize");
                    //drRow["JudgeStandard"] = this.uGrid5.Rows[i].GetCellValue("JudgeStandard");
                    drRow["UpperSpec"] = this.uGrid5.Rows[i].GetCellValue("UpperSpec");
                    drRow["LowerSpec"] = this.uGrid5.Rows[i].GetCellValue("LowerSpec");
                    drRow["EvalResultCode"] = this.uGrid5.Rows[i].GetCellValue("EvalResultCode");
                    drRow["JudgeResultCode"] = this.uGrid5.Rows[i].Cells["JudgeResultCode"].Text;

                    dtRtn.Rows.Add(drRow);

                }


                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Report를 위한 Main & 부속 설비 정보
        /// </summary>
        /// <returns></returns>
        private DataTable RtnMainSubEquip()
        {
            DataTable dtRtn = new DataTable();

            try
            {

                dtRtn.Columns.Add("EquipCode", typeof(string));
                dtRtn.Columns.Add("EquipName", typeof(string));
                dtRtn.Columns.Add("ModelName", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("SerialNo", typeof(string));
                dtRtn.Columns.Add("Package", typeof(string));
                dtRtn.Columns.Add("ReProcName", typeof(string));
                dtRtn.Columns.Add("EquipStandard", typeof(string));
                dtRtn.Columns.Add("WorkStandard", typeof(string));
                dtRtn.Columns.Add("TechStandard", typeof(string));
                dtRtn.Columns.Add("Object", typeof(string));
                //적용Package 저장
                string strPackage = this.uTextPackage.Text;
                string strProc = this.uTextProcess.Text;
                DataRow drRow;

                drRow = dtRtn.NewRow();

                drRow["EquipCode"] = this.uTextEquipCode.Text;
                drRow["EquipName"] = this.uTextEquipName.Text;
                drRow["ModelName"] = this.uTextModel.Text;
                drRow["VendorCode"] = this.uTextVendorName.Text;
                drRow["SerialNo"] = this.uTextSerial.Text;
                drRow["Package"] = strPackage;
                drRow["ReProcName"] = strProc;
                drRow["EquipStandard"] = this.uCheckEquipStandardFlag.Checked == true ? "O" : "";
                drRow["WorkStandard"] = this.uCheckWorkStandardFlag.Checked == true ? "O" : "";
                drRow["TechStandard"] = this.uCheckTechStandardFlag.Checked == true ? "O" : "";
                drRow["Object"] = this.uTextObject.Text;

                dtRtn.Rows.Add(drRow);

                for (int i = 0; i < this.uGridSubEquip.Rows.Count; i++)
                {
                    drRow = dtRtn.NewRow();

                    drRow["EquipCode"] = this.uGridSubEquip.Rows[i].GetCellValue("EquipCode");
                    drRow["EquipName"] = this.uGridSubEquip.Rows[i].GetCellValue("EquipName");
                    drRow["ModelName"] = this.uGridSubEquip.Rows[i].GetCellValue("ModelName");
                    drRow["VendorCode"] = this.uGridSubEquip.Rows[i].GetCellValue("VendorCode");
                    drRow["SerialNo"] = this.uGridSubEquip.Rows[i].GetCellValue("SerialNo");
                    drRow["Package"] = strPackage;
                    drRow["ReProcName"] = strProc;

                    dtRtn.Rows.Add(drRow);
                }


                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        ///  Report를 위한 헤더 상세정보
        /// </summary>
        /// <returns></returns>
        private DataTable RtnHader()
        {
            DataTable dtRtn = new DataTable();

            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUCCS.EQUEquipCertiH), "EQUEquipCertiH");
                QRPEQU.BL.EQUCCS.EQUEquipCertiH clsH = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();
                brwChannel.mfCredentials(clsH);

                dtRtn = clsH.mfReadEquipCertiH_Detail(this.uTextPlantCode.Text, this.uTextDocCode.Text, m_resSys.GetString("SYS_LANG"));

                if (dtRtn.Rows.Count > 0)
                {
                    dtRtn.Columns.Add("CertiChargeName", typeof(string));
                    dtRtn.Columns.Add("CertiChargeDate", typeof(string));

                    dtRtn.Rows[0]["CertiChargeName"] = this.uTextCertiChargeName.Text;
                    dtRtn.Rows[0]["CertiChargeDate"] = this.uDateCertiDate.DateTime.Date.Year.ToString() + ". " + 
                                                        this.uDateCertiDate.DateTime.Date.Month.ToString()+ ". " +
                                                        this.uDateCertiDate.DateTime.Date.Day.ToString() + ". ";


                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 이메일 전송 메소드
        /// </summary>
        private void SendMail(string strFileName)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uOptionCertiResultCode.Value != null
                    && this.uOptionCertiResultCode.Value.ToString().Equals("OK")
                    && this.uCheckMESTFlag.Checked
                    && this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                {

                    string strPlantCode = this.uTextPlantCode.Text;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUserReq = clsUser.mfReadSYSUser(strPlantCode, uTextCertiReqID.Text, m_resSys.GetString("SYS_LANG")); // 설비인증의뢰자
                    DataTable dtUserInfo = clsUser.mfReadSYSUser(strPlantCode, uTextCertiAdmitID.Text, m_resSys.GetString("SYS_LANG")); //승인자

                    if (dtUserReq.Rows.Count <= 0 || dtUserInfo.Rows.Count <= 0)
                        return;

                    if (!string.IsNullOrEmpty(dtUserInfo.Rows[0]["EMail"].ToString()) &&
                        dtUserInfo.Rows[0]["Email"].ToString().Contains(@"@bokwang.com"))
                    {

                        #region PDF파일첨부
                        #region BL호출 및 연결정보,경로 저장
                        //메일보내기

                        //첨부파일을 위한 FileServer 연결정보
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                        //메일 발송 첨부파일 경로
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFileDestPath);
                        DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                        //설비인증 첨부파일 저장경로
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysTargetFilePath);
                        DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0003");

                        #endregion

                        QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                        ArrayList arrFile = new ArrayList();        // 복사할 파일서버경로및 파일명
                        ArrayList arrAttachFile = new ArrayList();  // 파일을 복사할 경로
                        ArrayList arrFileNonPath = new ArrayList(); // 첨부할 파일명 저장

                        //파일이 첨부되었을 경우
                        if (!strFileName.Equals(string.Empty))
                        {

                            string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName;
                            string TargetFile = "EQUEquipCertiFile/" + strFileName;
                            string NonPathFile = strFileName;

                            arrAttachFile.Add(TargetFile);
                            arrFile.Add(DestFile);
                            arrFileNonPath.Add(NonPathFile);

                        }

                        //메일에 첨부할 파일을 서버에서 찾아서 서버에 있는 메일 첨부파일폴더로 복사한다.
                        fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                                    , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrAttachFile
                                                                    , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                                    , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                      dtSysAccess.Rows[0]["AccessPassword"].ToString());


                        #endregion

                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);

                        //첨부파일 메일 첨부파일 경로로 복사
                        if (arrFile.Count != 0)
                        {
                            fileAtt.mfFileUpload_NonAssync();
                        }

                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strTitle = string.Empty;
                        string strName = string.Empty;
                        string[] strContents = new string[8];
                        if (strLang.Equals("KOR"))
                        {
                            strTitle = "설비인증 요청 완료 메일";
                            strName = "설비번호,설비모델,의뢰목적,의뢰자,의뢰일자,완료일자,판정결과,인증";
                        }
                        else if (strLang.Equals("CHN"))
                        {
                            strTitle = "设备认证邀请完成邮件";
                            strName = "设备号码,设备型号,委托目的,委托人,委托日期,完成日期,判定结果,认证";
                        }
                        else if (strLang.Equals("ENG"))
                        {
                            strTitle = "설비인증 요청 완료 메일";
                            strName = "설비번호,설비모델,의뢰목적,의뢰자,의뢰일자,완료일자,판정결과,인증";
                        }

                        strContents = strName.Split(',');

                        //&& this.uComboQualType.Value.ToString().Equals("PCN")
                        bool bolRtn = clsmail.mfSendSMTPMail(strPlantCode
                                                        , dtUserInfo.Rows[0]["EMail"].ToString()    //m_resSys.GetString("SYS_EMAIL")
                                                        , dtUserInfo.Rows[0]["UserName"].ToString()    //m_resSys.GetString("SYS_USERNAME")
                                                        , dtUserReq.Rows[0]["EMail"].ToString()       //보내려는 사람 이메일주소
                                                        , strTitle
                                                        , "<HTML>" +
                                                            "<HEAD>" +
                                                            "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                            "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                            "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                            "</HEAD>" +
                                                            "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strContents[0] +"</SPAN> : " + this.uTextEquipCode.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strContents[1] +"</SPAN> : " + this.uTextModel.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strContents[2] +"</SPAN> : " + this.uTextObject.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strContents[3] +"</SPAN>&nbsp&nbsp&nbsp : " + this.uTextCertiReqName.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strContents[4] +"</SPAN> : " + this.uTextCertiReqDate.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strContents[5] +"</SPAN> : " + Convert.ToDateTime(this.uDateCertiAdmitDate.Value).ToString("yyyy-MM-dd") + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. "+ strContents[6] +"</SPAN> : "+ strContents[7] +" </P>" +
                                                            "</BODY>" +
                                                            "</HTML>"
                                                        , arrFileNonPath);



                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();

                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M000083", m_resSys.GetString("SYS_LANG"))
                                    , msg.GetMessge_Text("M001455", m_resSys.GetString("SYS_LANG"))
                                    , msg.GetMessge_Text("M001456", m_resSys.GetString("SYS_LANG"))
                                      + dtUserReq.Rows[0]["EMail"].ToString() + msg.GetMessge_Text("M000005", m_resSys.GetString("SYS_LANG"))
                                    , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uButtonEMailTest_Click(object sender, EventArgs e)
        {
            if (this.uTextDocCode.Text.Equals(string.Empty))
                return;
            WinMessageBox msg = new WinMessageBox();

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001449", "M001448",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                return;

            SendMail("TEST.jpg");
        }


    }
}
