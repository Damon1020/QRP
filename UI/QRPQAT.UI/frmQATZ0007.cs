﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : 신뢰성 검사 관리                                      */
/* 프로그램ID   : frmQATZ0007.cs                                        */
/* 프로그램명   : 신뢰성검사 의뢰/등록                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2012-02-24 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

// 파일첨부
using System.IO;

namespace QRPQAT.UI
{
    public partial class frmQATZ0007 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        #region 폼이동을 위한 전역변수

        private string m_strPlantCode;
        private string m_strReliabilityNo;
        private string m_strMoveFormName;

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string ReliabilityNo
        {
            get { return m_strReliabilityNo; }
            set { m_strReliabilityNo = value; }
        }

        public string MoveFormName
        {
            get { return m_strMoveFormName; }
            set { m_strMoveFormName = value; }
        }
        #endregion

        public frmQATZ0007()
        {
            InitializeComponent();
        }

        private void frmQATZ0007_Activated(object sender, EventArgs e)
        {
            // 툴바 활성화 여부 설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0007_Load(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀지정
            titleArea.mfSetLabelText("신뢰성검사 의뢰/등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화 Method 호출
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitText();
            InitGroupBox();
            InituAppraisal();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            if (!string.IsNullOrEmpty(MoveFormName))
            {
                titleArea.TextName = titleArea.TextName + "(신규자재 인증등록 이동)";
                SearchDetailData(PlantCode, ReliabilityNo);
            }
            else
            {
                this.uGroupBoxContentsArea.Expanded = false;
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 컨트롤 초기화 Method
        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 기본값 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                this.uTextAppraisalObject.MaxLength = 200;
                this.uTextChangeBefore.MaxLength = 200;
                this.uTextChangeAfter.MaxLength = 200;
                this.uTextWriteUserID.MaxLength = 20;
                this.uTextProductCode.MaxLength = 20;
                this.uTextChip_Size.MaxLength = 100;
                this.uTextAdhesive_Model.MaxLength = 100;
                this.uTextAdhesive_Vender.MaxLength = 100;
                this.uTextLForPCBModel.MaxLength = 100;
                this.uTextLForPCBVender.MaxLength = 100;
                this.uTextWireModel.MaxLength = 100;
                this.uTextWireVender.MaxLength = 100;
                this.uTextEMCModel.MaxLength = 100;
                this.uTextEMCVender.MaxLength = 100;
                this.uTextSolderBallModel.MaxLength = 100;
                this.uTextSolderBallVender.MaxLength = 100;
                this.uNumReqQty.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
                this.uNumReqQty.PromptChar = ' ';
                this.uNumReqQty.MaskInput = "{double:5.0}";
                this.uTextEtcDesc.MaxLength = 200;
                this.uTextTCOptionTime.MaxLength = 20;
                this.uTextTCOptionDate.MaxLength = 20;
                this.uTextBakeDate.MaxLength = 20;
                this.uTextBakeTime.MaxLength = 20;
                this.uTextHumidityDate1.MaxLength = 20;
                this.uTextHumidityTime1.MaxLength = 20;
                this.uTextReflowDate.MaxLength = 20;
                this.uTextReflowTime.MaxLength = 20;
                this.uTextTCDate.MaxLength = 20;
                this.uTextTCTime.MaxLength = 20;
                this.uTextPCTDate.MaxLength = 20;
                this.uTextPCTTime.MaxLength = 20;
                this.uTextHASTDate.MaxLength = 20;
                this.uTextHASTTime.MaxLength = 20;
                this.uTextHumidityDate2.MaxLength = 20;
                this.uTextHumidityTime2.MaxLength = 20;
                this.uTextHTSDate.MaxLength = 20;
                this.uTextHTSTime.MaxLength = 20;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxStdInfo, GroupBoxType.INFO, "기본정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxRequest, GroupBoxType.INFO, "의뢰제품정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxAppraiseCondition, GroupBoxType.INFO, "평가조건", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                wGroupBox.mfSetGroupBox(this.uGroupBoxAppraisal, GroupBoxType.INFO, "평가", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxStdInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxStdInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxRequest.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxRequest.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxAppraiseCondition.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxAppraiseCondition.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxAppraisal.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxAppraisal.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchWriteDate, "등록일자", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReliabilityNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGubun, "구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteDate, "의뢰일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelWriteUser, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAppraisalObject, "평가목적", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChangeBefore, "변경전", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChangeAfter, "변경후", m_resSys.GetString("SYS_FONTNAME"), true, false); // 2012-11-07 비고 -> 제품L/N 로  변경
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProduct, "Package", m_resSys.GetString("SYS_FONTNAME"), true, true); // 2012-11-07 제품코드 -> Package 로 변경
                wLabel.mfSetLabel(this.uLabelChipSize, "Chip Size", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAdhesiveModel, "Adhesive Model", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAdhesiveVender, "Adhesive vender", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLFPCBModel, "L/F or PCB Model", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLFPCBVender, "L/F or PCB vender", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWireModel, "Wire Model", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWireVender, "Wire vender", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEMCModel, "EMC Model", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEMCVender, "EMC vender", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSolderBallModelorPlatingComposition, "제품P/N", m_resSys.GetString("SYS_FONTNAME"), true, false); // 2012-11-07 Solder Ball Model or Plating Composition -> 제품P/N로  변경
                wLabel.mfSetLabel(this.uLabelSolderBallVenderorPlatingSubcontractor, "Process Type", m_resSys.GetString("SYS_FONTNAME"), true, false); // 2012-11-07 Solder Ball vender or Plating Subcontractor - > Process Type로 변경
                wLabel.mfSetLabel(this.uLabelReqQty, "의뢰수량", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelTestName, "시험명", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.ulabelPreCondition, "Pre-Condition", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelPreTC, "TC (Option)", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelPreBakeOven, "Bake Oven", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelPreHumidity, "Humidity Soak", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelPreReflow, "Reflow", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelTC, "TC", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelPCT, "PCT", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelHAST, "HAST", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelHumidity, "Humidity Soak", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelHTS, "HTS", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelAcceptDate, "접수일", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelAcceptUser, "접수자", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelAcceptComplete, "접수완료", m_resSys.GetString("SYS_FONTNAME"), false, false);


                wLabel.mfSetLabel(this.uLabelYesNo, "진행여부", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelCondition, "진행조건", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelProgressTime, "진행시간", m_resSys.GetString("SYS_FONTNAME"), false, false);
                wLabel.mfSetLabel(this.uLabelschedule, "진행일정", m_resSys.GetString("SYS_FONTNAME"), false, false);

                wLabel.mfSetLabel(this.uLabelInspectDate, "평가일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInspectFilaName, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser, "평가자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPassFailFlag, "합부판정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelComplete, "작성완료", m_resSys.GetString("SYS_FONTNAME"), true, false);

                this.uLabelTestName.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.ulabelPreCondition.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelPreTC.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelPreBakeOven.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelPreHumidity.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelPreReflow.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelTC.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelPCT.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelHAST.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelHumidity.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelHTS.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;

                this.uLabelYesNo.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelCondition.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelProgressTime.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                this.uLabelschedule.Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "전체", "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "PlantCode", "PlantName", dtPlant);

                // 진행여부
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtYesNo = clsComCode.mfReadCommonCode("C0026", m_resSys.GetString("SYS_LANG"));

                DataTable dtGubun = clsComCode.mfReadCommonCode("C0064", m_resSys.GetString("SYS_LANG"));

                

                wCombo.mfSetComboEditor(this.uComboGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtGubun);

                wCombo.mfSetComboEditor(this.uComboTCOptionFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboBakeFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboHumidityFlag1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboReflowFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboTCFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboPCTFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboHASTFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboHumidityFlag2, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                wCombo.mfSetComboEditor(this.uComboHTSFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtYesNo);

                //진행조건
                // TCT
                DataTable dtCondition = clsComCode.mfReadCommonCode("C0047", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboTCCondition, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtCondition);

                wCombo.mfSetComboEditor(this.uComboTCOptionCondition, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtCondition);

                // Bake
                dtCondition = clsComCode.mfReadCommonCode("C0048", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboBakeCondition, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtCondition);

                // Soak
                dtCondition = clsComCode.mfReadCommonCode("C0049", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboHumidityCondition1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtCondition);

                wCombo.mfSetComboEditor(this.uComboHumidityCondition2, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtCondition);

                // Reflow
                dtCondition = clsComCode.mfReadCommonCode("C0050", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboReflowCondition, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtCondition);

                dtCondition = clsComCode.mfReadCommonCode("C0051", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboHTSCondition, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtCondition);
                // Process Type 2012-11-07
                DataTable dtProcessType = clsComCode.mfReadCommonCode("C0078", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboProcessType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "ComCode", "ComCodeName", dtProcessType);

                clsComCode.Dispose();
                dtCondition.Dispose();
                dtGubun.Dispose();
                dtPlant.Dispose();
                dtProcessType.Dispose();
                dtYesNo.Dispose();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridReliabilityList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "ReliabilityNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "ProductPN", "제품P/N", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "ProductLN", "제품L/N", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "Gubun", "유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 2012-11-07 주석
                //wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "ProductName", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "AppraisalObject", "신뢰도평가목적", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "WriteUserName", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "WriteDate", "등록일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "AcceptUserName", "접수자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "AcceptDate", "접수일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "InspectDate", "평가종료일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "PassFailFlag", "평가결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 2012-11-07 주석
                //wGrid.mfSetGridColumn(this.uGridReliabilityList, 0, "DocState", "진행현황", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridReliabilityList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridReliabilityList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 평가 권한
        /// </summary>
        private void InituAppraisal()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCom);
                DataTable dtQualityDept = clsUserCom.mfReadUserCommonCode("QUA", "S0003", m_resSys.GetString("SYS_LANG"));
                if (dtQualityDept.Rows.Count > 0)
                {
                    string strDeptCheck = string.Empty;
                    var varQualityDept = from getRow in dtQualityDept.AsEnumerable()
                                         //where getRow["ComCode"].ToString() != string.Empty//m_resSys.GetString("SYS_DEPTCODE")
                                         select getRow["ComCode"].ToString();

                    if (varQualityDept.Contains(m_resSys.GetString("SYS_DEPTCODE")))
                        uGroupBoxAppraisal.Visible = true;
                    else
                        uGroupBoxAppraisal.Visible = false;


                }
                else
                {
                    uGroupBoxAppraisal.Visible = false;
                }

                ////if (!m_resSys.GetString("SYS_DEPTCODE").Equals("S5100"))
                ////{
                ////    //foreach (Control Ctrl in this.uGroupBoxAppraisal.Controls)
                ////    //{
                ////    //    Ctrl.Visible = false;
                ////    //}

                ////    this.uGroupBoxAppraisal.Visible = false;
                ////}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 매개변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strProductCode = this.uTextSearchProductCode.Text;
                string strWriteDateFrom = Convert.ToDateTime(this.uDateSearchWriteFromDate.Value).ToString("yyyy-MM-dd");
                string strWriteDateTo = Convert.ToDateTime(this.uDateSearchWriteToDate.Value).ToString("yyyy-MM-dd");

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.ReliabilityInspect), "ReliabilityInspect");
                QRPQAT.BL.QATREL.ReliabilityInspect clsRel = new QRPQAT.BL.QATREL.ReliabilityInspect();
                brwChannel.mfCredentials(clsRel);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 메소드 호출
                DataTable dtSearch = clsRel.mfReadQATReliabilityInspect(strPlantCode, strProductCode, strWriteDateFrom, strWriteDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGridReliabilityList.DataSource = dtSearch;
                this.uGridReliabilityList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridReliabilityList, 0);
                }
                // ContentsArea 그룹박스 접힌 상태로

                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001041", Infragistics.Win.HAlign.Center);
                    return;
                }
                else if (!(this.uCheckCompleteFlag.Enabled))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000984", "M000993", Infragistics.Win.HAlign.Center);
                    return;
                }
                else if (this.uTextInspectFileName.Text.Contains("/")
                        || this.uTextInspectFileName.Text.Contains("#")
                        || this.uTextInspectFileName.Text.Contains("*")
                        || this.uTextInspectFileName.Text.Contains("<")
                        || this.uTextInspectFileName.Text.Contains(">"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);

                    return;
                }
                else
                {
                    // 필수 입력사항 확인
                    if (CheckRequire())
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        // 저장여부를 묻는다
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            // BL연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.ReliabilityInspect), "ReliabilityInspect");
                            QRPQAT.BL.QATREL.ReliabilityInspect clsREL = new QRPQAT.BL.QATREL.ReliabilityInspect();
                            brwChannel.mfCredentials(clsREL);

                            // 데이터 테이블 설정
                            DataTable dtSave = clsREL.mfSetDataInfo();
                            // 데이터 저장

                            DataRow drRow = dtSave.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReliabilityNo"] = this.uTextReliabilityNo.Text;
                            drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                            drRow["WriteDate"] = Convert.ToDateTime(this.uDateWriteDate.Value).ToString("yyyy-MM-dd");
                            drRow["AppraisalObject"] = this.uTextAppraisalObject.Text;
                            drRow["ChangeBefore"] = this.uTextChangeBefore.Text;
                            drRow["ChangeAfter"] = this.uTextChangeAfter.Text;
                            drRow["ProductCode"] = this.uTextProductCode.Text;
                            drRow["Customer"] = this.uTextCustomerCode.Text;        // 2012-10-14 추가 고객사 직접입력
                            drRow["Chip_Size"] = this.uTextChip_Size.Text;
                            drRow["Gubun"] = this.uComboGubun.Value;                    //구분
                            drRow["Adhesive_Model"] = this.uTextAdhesive_Model.Text;
                            drRow["Adhesive_Vender"] = this.uTextAdhesive_Vender.Text;
                            drRow["LForPCBModel"] = this.uTextLForPCBModel.Text;
                            drRow["LForPCBVender"] = this.uTextLForPCBVender.Text;
                            drRow["WireModel"] = this.uTextWireModel.Text;
                            drRow["WireVender"] = this.uTextWireVender.Text;
                            drRow["EMCModel"] = this.uTextEMCModel.Text;
                            drRow["EMCVender"] = this.uTextEMCVender.Text;
                            drRow["SolderBallModel"] = this.uTextSolderBallModel.Text;
                            drRow["SolderBallVender"] = this.uTextSolderBallVender.Text;
                            drRow["ReqQty"] = Convert.ToDecimal(this.uNumReqQty.Value);
                            drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                            drRow["TCOptionFlag"] = this.uComboTCOptionFlag.Value.ToString();
                            drRow["TCOptionCondition"] = this.uComboTCOptionCondition.Value.ToString();
                            drRow["TCOptionTime"] = this.uTextTCOptionTime.Text;
                            drRow["TCOptionDate"] = this.uTextTCOptionDate.Text;
                            drRow["BakeFlag"] = this.uComboBakeFlag.Value.ToString();
                            drRow["BakeCondition"] = this.uComboBakeCondition.Value.ToString();
                            drRow["BakeTime"] = this.uTextBakeTime.Text;
                            drRow["BakeDate"] = this.uTextBakeDate.Text;
                            drRow["HumidityFlag1"] = this.uComboHumidityFlag1.Value.ToString();
                            drRow["HumidityCondition1"] = this.uComboHumidityCondition1.Value.ToString();
                            drRow["HumidityTime1"] = this.uTextHumidityTime1.Text;
                            drRow["HumidityDate1"] = this.uTextHumidityDate1.Text;
                            drRow["ReflowFlag"] = this.uComboReflowFlag.Value.ToString();
                            drRow["ReflowCondition"] = this.uComboReflowCondition.Value.ToString();
                            drRow["ReflowTime"] = this.uTextReflowTime.Text;
                            drRow["ReflowDate"] = this.uTextReflowDate.Text;
                            drRow["TCFlag"] = this.uComboTCFlag.Value.ToString();
                            drRow["TCCondition"] = this.uComboTCCondition.Value.ToString();
                            drRow["TCTime"] = this.uTextTCTime.Text;
                            drRow["TCDate"] = this.uTextTCDate.Text;
                            drRow["PCTFlag"] = this.uComboPCTFlag.Value.ToString();
                            //drRow["PCTCondition"] = this.uComboPCTCondition.Value.ToString();
                            drRow["PCTCondition"] = this.uTextPCTCondition.Text;
                            drRow["PCTTime"] = this.uTextPCTTime.Text;
                            drRow["PCTDate"] = this.uTextPCTDate.Text;
                            drRow["HASTFlag"] = this.uComboHASTFlag.Value.ToString();
                            //drRow["HASTCondition"] = this.uComboHASTCondition.Value.ToString();
                            drRow["HASTCondition"] = this.uTextHASTCondition.Text;
                            drRow["HASTTime"] = this.uTextHASTTime.Text;
                            drRow["HASTDate"] = this.uTextHASTDate.Text;
                            drRow["HumidityFlag2"] = this.uComboHumidityFlag2.Value.ToString();
                            drRow["HumidityCondition2"] = this.uComboHumidityCondition2.Value.ToString();
                            drRow["HumidityTime2"] = this.uTextHumidityTime2.Text;
                            drRow["HumidityDate2"] = this.uTextHumidityDate2.Text;
                            drRow["HTSFlag"] = this.uComboHTSFlag.Value.ToString();
                            drRow["HTSCondition"] = this.uComboHTSCondition.Value.ToString();
                            drRow["HTSTime"] = this.uTextHTSTime.Text;
                            drRow["HTSDate"] = this.uTextHTSDate.Text;
                            drRow["InspectUserID"] = this.uTextInspectUserID.Text;
                            drRow["InspectDate"] = Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd");
                            if (this.uTextInspectFileName.Text.Contains(":\\") && !this.uTextReliabilityNo.Text.Equals(string.Empty))
                            {
                                FileInfo fileDoc = new FileInfo(this.uTextInspectFileName.Text);
                                drRow["InspectFileName"] = this.uComboPlantCode.Value.ToString() + "-" + this.uTextReliabilityNo.Text + "-" + fileDoc.Name;
                            }
                            else if (this.uTextInspectFileName.Text.Contains(":\\") && this.uTextReliabilityNo.Text.Equals(string.Empty))
                            {
                                FileInfo fileDoc = new FileInfo(this.uTextInspectFileName.Text);
                                drRow["InspectFileName"] = fileDoc.Name;
                            }
                            else
                            {
                                drRow["InspectFileName"] = this.uTextInspectFileName.Text;
                            }

                            if (!this.uTextAcceptUserID.Text.Equals(string.Empty) && !this.uTextAcceptUserName.Text.Equals(string.Empty)) //접수자
                            {
                                drRow["AcceptUserID"] = this.uTextAcceptUserID.Text;
                                drRow["AcceptDate"] = this.uDateAccept.Value == null ? "" : this.uDateAccept.DateTime.Date.ToString("yyyy-MM-dd"); //접수일
                                drRow["AcceptFlag"] = this.uCheckAcceptFlag.Checked == true ? "T" : "F"; //접수여부 
                            }
                            else
                            {
                                drRow["AcceptUserID"] = "";
                                drRow["AcceptDate"] = ""; //접수일
                                drRow["AcceptFlag"] = "F"; //접수여부 
                            }
                            
                            if (this.uOptionPassFailFlag.CheckedIndex.Equals(0))
                                drRow["PassFailFlag"] = "OK";
                            else if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                            {
                                drRow["PassFailFlag"] = "NG";
                            }
                            if (this.uCheckCompleteFlag.Checked)
                                drRow["CompleteFlag"] = "T";
                            else
                                drRow["CompleteFlag"] = "F";

                            drRow["Package"] = this.uTextPackage.Text;          // PSTS 2012-11-07 Package
                            drRow["ProductLN"] = this.uTextProductLN.Text;        // PSTS 2012-11-07 제품L/N
                            drRow["ProductPN"] = this.uTextProductPN.Text;        // PSTS 2012-11-07 제품P/N
                            drRow["ProcessType"] = this.uComboProcessType.Value == null ? string.Empty : this.uComboProcessType.Value;      // PSTS 2012-11-07 ProcessType (FS/HF선택)

                            dtSave.Rows.Add(drRow);

                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // 메소드 호출
                            string strErrRtn = clsREL.mfSaveQATReliabilityInspect(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // 처리결과에 따른 메세지 박스
                            if (ErrRtn.ErrNum == 0)
                            {
                                string strReliabilityNo = ErrRtn.mfGetReturnValue(0);
                                FileUpload(strReliabilityNo);

                                if (this.uCheckCompleteFlag.Checked)
                                {
                                    SendMail(this.uTextAcceptUserID.Text,m_resSys.GetString("SYS_LANG"));
                                    SendMail(this.uTextInspectUserID.Text, m_resSys.GetString("SYS_LANG"));
                                }

                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextReliabilityNo.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uComboPlantCode.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 호출
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.ReliabilityInspect), "ReliabilityInspect");
                        QRPQAT.BL.QATREL.ReliabilityInspect clsRel = new QRPQAT.BL.QATREL.ReliabilityInspect();
                        brwChannel.mfCredentials(clsRel);

                        string strPlantCode = this.uComboPlantCode.Value.ToString();
                        string strReliabilityNo = this.uTextReliabilityNo.Text;

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsRel.mfDeleteQATReliabilityInspect(strPlantCode, strReliabilityNo);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000923",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridReliabilityList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridReliabilityList);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method....
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 제품정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <returns></returns>
        private DataTable GetProductInfo(string strPlantCode, string strProductCode)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                dtRtn = clsProduct.mfReadMASMaterialDetail(strPlantCode, strProductCode, m_resSys.GetString("SYS_LANG"));

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 필수 입력사항 확인
        /// </summary>
        /// <returns></returns>
        private bool CheckRequire()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlantCode.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlantCode.DropDown();
                    return false;
                }
                else if (this.uTextWriteUserID.Text == "" || this.uTextWriteUserName.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000842", Infragistics.Win.HAlign.Center);

                    this.uTextWriteUserID.Focus();
                    return false;
                }
                //else if (this.uTextProductCode.Text.Equals(string.Empty))// || this.uTextProductName.Text == "")
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                        , "M001264", "M000881", "M001094", Infragistics.Win.HAlign.Center);

                //    this.uTextProductCode.Focus();
                //    return false;
                //}
                else if (this.uCheckAcceptFlag.Checked)
                {
                    if (this.uTextAcceptUserID.Text.Equals(string.Empty) || this.uTextAcceptUserName.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M001071", Infragistics.Win.HAlign.Center);

                        this.uTextAcceptUserID.Focus();
                        return false;
                    }
                    if (this.uDateAccept.Value == null || this.uDateAccept.Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M001069", Infragistics.Win.HAlign.Center);

                        this.uDateAccept.DropDown();
                        return false;
                    }
                }
                else if (this.uCheckCompleteFlag.Checked)
                {
                    if (this.uTextInspectUserID.Text.Equals(string.Empty) || this.uTextInspectUserName.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M001328", Infragistics.Win.HAlign.Center);

                        this.uTextInspectUserID.Focus();
                        return false;
                    }
                    else if (this.uDateInspectDate.Value == null || this.uDateInspectDate.Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M001327", Infragistics.Win.HAlign.Center);

                        this.uDateInspectDate.DropDown();
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화

        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                ClearControls(this.uGroupBoxStdInfo);
                ClearControls(this.uGroupBoxRequest);
                ClearControls(this.uGroupBoxAppraiseCondition);
                ClearControls(this.uGroupBoxAppraisal);

                this.uTextInspectUserName.Clear();
                this.uTextInspectFileName.Clear();
                this.uOptionPassFailFlag.CheckedIndex = -1;
                this.uCheckCompleteFlag.Checked = false;
                this.uCheckAcceptFlag.Checked = false;
                this.uCheckAcceptFlag.Enabled = true;

                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uDateInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateWriteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                //this.uTextAcceptUserID.Text = m_resSys.GetString("SYS_USERID");
                //this.uTextAcceptUserName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uTextPackage.Clear();   // PSTS 2012-11-07 Package
                this.uTextProductLN.Clear();   // PSTS 2012-11-07 제품L/N
                this.uTextProductPN.Clear();   // PSTS 2012-11-07 제품P/N
                this.uComboProcessType.Value = string.Empty;

                this.uComboPlantCode.Enabled = true;
                this.uCheckCompleteFlag.Enabled = true;
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 Clear Method
        /// </summary>
        /// <param name="c"></param>
        private void ClearControls(Control c)
        {
            try
            {
                foreach (Control Ctrl in c.Controls)
                {
                    if (Ctrl.GetType().ToString() != "Infragistics.Win.Misc.UltraLabel" || Ctrl.GetType().ToString() != "Infragistics.Win.Misc.UltraButton")
                    {
                        switch (Ctrl.GetType().ToString())
                        {
                            case "Infragistics.Win.UltraWinEditors.UltraTextEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraTextEditor)Ctrl).Clear();
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraComboEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraComboEditor)Ctrl).Value = "";
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraDateTimeEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)Ctrl).Value = DateTime.Now.ToString("yyyy-MM-dd");
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraNumericEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraNumericEditor)Ctrl).Value = 0;
                                break;
                            //case "Infragistics.Win.UltraWinEditors.UltraCheckEditor":
                            //    ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)Ctrl).Checked = false;
                            //    break;
                            //case "Infragistics.Win.UltraWinGrid.UltraGrid":
                            //    while (((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows.Count > 0)
                            //    {
                            //        ((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows[0].Delete(false);
                            //    }
                            //    break;
                            default:
                                //if (Ctrl.Controls.Count > 0)
                                //    ClearControls(Ctrl);
                                break;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 첨부파일 업로드 메소드

        /// </summary>
        /// <param name="strReliabilityNo"></param>
        private void FileUpload(string strReliabilityNo)
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                if (this.uTextInspectFileName.Text.Contains(":\\"))
                {
                    // 화일이름 변경(공장코드+신뢰성검사번호+파일명)하여 복사하기
                    FileInfo fileDoc = new FileInfo(this.uTextInspectFileName.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                            this.uComboPlantCode.Value.ToString() + "-" + strReliabilityNo + "-" + fileDoc.Name;
                    // 변경한 파일이 있으면 삭제
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    // 변경한 파일이름으로 복사
                    File.Copy(this.uTextInspectFileName.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                // 업로드할 파일이 있는경우 파일 업로드

                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                    // 첨부파일 저장경로정보 가져오기

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0020");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void SendMail(string strUserID, string strLang)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    string strPlantCode = this.uComboPlantCode.Value.ToString();
                    // 사용자정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    string strTitle = string.Empty;
                    string strContents = string.Empty;

                    if (strLang.Equals("KOR"))
                    {
                        //strTitle = "[QRP].제품코드[" + this.uTextProductCode.Text + "], 제품명[" + this.uTextProductName.Text +
                        //                                        "], 신뢰성검사의뢰하였습니다.";
                        strTitle = "[QRP].고객사[" + this.uTextCustomerCode.Text + "], PACKAGE[" + this.uTextPackage.Text +
                                                                "], 신뢰성검사의뢰하였습니다.";

                        //strContents = "[QRP] 신뢰성검사의뢰 :  제품코드[" + this.uTextProductCode.Text + "], 제품명[" + this.uTextProductName.Text +
                        //                                        "신뢰성검사의뢰하였습니다.";
                        //strContents = "[QRP] 신뢰성검사의뢰 :  고객사[" + this.uTextCustomerCode.Text + "], PACKAGE[" + this.uTextPackage.Text +
                        //                                        " ], 신뢰성검사의뢰하였습니다.";

                        strContents = "[QRP] 신뢰성검사의뢰 :  고객사[" + this.uTextCustomerCode.Text + "], PACKAGE[" + this.uTextPackage.Text +
                                                                "], 신뢰성검사의뢰 완료하였습니다.";

                    }
                    else if (strLang.Equals("CHN"))
                    {
                        strTitle = "[QRP].客户社[ " + this.uTextCustomerCode.Text + " ], PACKAGE[ " + this.uTextPackage.Text +
                                                                " ], 信赖性检查委托已进行.";
                        //strContents = "[QRP] 信赖性检查 申请 :  客户社[ " + this.uTextCustomerCode.Text + " ], PACKAGE[ " + this.uTextPackage.Text +
                        //                                        " ], 信赖性检查委托已进行.";

                        strContents = "[QRP] 信赖性检查 申请 :  客户社[ " + this.uTextCustomerCode.Text + " ], PACKAGE[ " + this.uTextPackage.Text +
                                                                                        " ], 已经完成 信赖性检查委托.";

                    }
                    else if (strLang.Equals("ENG"))
                    {
                        strTitle = "[QRP].고객사[" + this.uTextCustomerCode.Text + "], PACKAGE[" + this.uTextPackage.Text +
                                                                "], 신뢰성검사의뢰하였습니다.";
                        //strContents = "[QRP] 신뢰성검사의뢰 :  고객사[" + this.uTextCustomerCode.Text + "], PACKAGE[" + this.uTextPackage.Text +
                        //                                        "신뢰성검사의뢰하였습니다.";
                        strContents = "[QRP] 신뢰성검사의뢰 :  고객사[" + this.uTextCustomerCode.Text + "], PACKAGE[" + this.uTextPackage.Text +
                                                                "], 신뢰성검사의뢰 완료하였습니다.";
                    }

                    // 메일보내기
                    if (dtUser.Rows.Count > 0)
                    {
                        
                        ArrayList arrAttachFile = new ArrayList();

                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);

                        for (int i = 0; i < dtUser.Rows.Count; i++)
                        {
                            bool bolRtn = clsmail.mfSendSMTPMail(strPlantCode
                                                            , m_resSys.GetString("SYS_EMAIL")
                                                            , this.uTextWriteUserName.Text
                                                            , dtUser.Rows[i]["Email"].ToString()//보내려는 사람 이메일주소
                                                            , strTitle
                                                            , strContents
                                //+ dtVendorP.Rows[0]["PersonName"].ToString() + "님 좋은하루 되십시오."
                                                            , arrAttachFile);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Events...
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridReliabilityList.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridReliabilityList.Height = 740;

                    for (int i = 0; i < this.uGridReliabilityList.Rows.Count; i++)
                    {
                        this.uGridReliabilityList.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0007_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        #region 팝업창 이벤트
        // 등록자 팝업창 이벤트
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlantCode.Value.ToString() != "" && this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                {
                    this.uTextWriteUserID.Text = frmPOP.UserID;
                    this.uTextWriteUserName.Text = frmPOP.UserName;
                }
                else
                {
                    // SystemInfo ResourceSet
                    //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    //WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M000275",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001254",m_resSys.GetString("SYS_LANG"))
                                                  + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001",m_resSys.GetString("SYS_LANG"))
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextWriteUserID.Text = "";
                    this.uTextWriteUserName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //접수자 팝업창이벤트
        private void uTextAcceptUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlantCode.Value.ToString() != "" && this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                {
                    this.uTextAcceptUserID.Text = frmPOP.UserID;
                    this.uTextAcceptUserName.Text = frmPOP.UserName;
                }
                else
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M000275",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001254",m_resSys.GetString("SYS_LANG"))
                                                  + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001",m_resSys.GetString("SYS_LANG"))
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextAcceptUserID.Clear();
                    this.uTextAcceptUserName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 평가자 팝업창 이벤트
        private void uTextInspectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlantCode.Value.ToString() != "" && this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                {
                    this.uTextInspectUserID.Text = frmPOP.UserID;
                    this.uTextInspectUserName.Text = frmPOP.UserName;
                }
                else
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FTONANE"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M000275",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001254",m_resSys.GetString("SYS_LANG"))
                                                  + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001",m_resSys.GetString("SYS_LANG"))
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextInspectUserID.Clear();
                    this.uTextInspectUserName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 제품코드 팝업창
        private void uTextProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                if (this.uComboPlantCode.Value.ToString() != "" && this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                {
                    this.uTextProductCode.Text = frmPOP.ProductCode;
                    this.uTextProductName.Text = frmPOP.ProductName;
                    this.uTextCustomerCode.Text = frmPOP.CustomerCode;
                    this.uTextCustomerName.Text = frmPOP.CustomerName;
                }
                else
                {
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M000275",m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001254",m_resSys.GetString("SYS_LANG"))
                                                  + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001",m_resSys.GetString("SYS_LANG"))
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextProductCode.Text = "";
                    this.uTextProductName.Text = "";
                    this.uTextCustomerCode.Text = "";
                    this.uTextCustomerName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 제품코드 팝업창
        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
           try
           {
               frmPOP0002 frmPOP = new frmPOP0002();
               frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
               frmPOP.ShowDialog();

               this.uComboSearchPlant.Value = frmPOP.PlantCode;
               this.uTextSearchProductCode.Text = frmPOP.ProductCode;
               this.uTextSearchProductName.Text = frmPOP.ProductName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 텍스트박스 키다운 이벤트
        // 등록자 텍스트박스 키다운 이벤트
        private void uTextWriteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextWriteUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextWriteUserID.Text = "";
                            this.uTextWriteUserName.Text = "";
                        }
                        else
                        {
                            this.uTextWriteUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteUserID.TextLength <= 1 || this.uTextWriteUserID.SelectedText == this.uTextWriteUserID.Text)
                    {
                        this.uTextWriteUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 평가자 키다운 이벤트
        private void uTextInspectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextInspectUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextInspectUserID.Clear();
                            this.uTextInspectUserName.Clear();
                        }
                        else
                        {
                            this.uTextInspectUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID.TextLength <= 1 || this.uTextInspectUserID.SelectedText == this.uTextInspectUserID.Text)
                    {
                        this.uTextInspectUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //접수자 키다운이벤트
        private void uTextAcceptUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAcceptUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextAcceptUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextAcceptUserID.Clear();
                            this.uTextAcceptUserName.Clear();
                        }
                        else
                        {
                            this.uTextAcceptUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextAcceptUserID.TextLength <= 1 || this.uTextAcceptUserID.SelectedText == this.uTextAcceptUserID.Text)
                    {
                        this.uTextAcceptUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 제품텍스트박스 키다운 이벤트
        private void uTextProductCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uComboPlantCode.Value.ToString() != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        if (this.uTextProductCode.Text != "")
                        {
                            string strPlantCode = this.uComboPlantCode.Value.ToString();
                            string strProductCode = this.uTextProductCode.Text;

                            DataTable dtProduct = GetProductInfo(strPlantCode, strProductCode);

                            if (dtProduct.Rows.Count > 0)
                            {
                                this.uTextProductName.Text = dtProduct.Rows[0]["ProductName"].ToString();
                                this.uTextCustomerCode.Text = dtProduct.Rows[0]["CustomerCode"].ToString();
                                this.uTextCustomerName.Text = dtProduct.Rows[0]["CUstomerName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M001091",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextProductCode.Text = "";
                                this.uTextProductName.Text = "";
                                this.uTextCustomerCode.Text = "";
                                this.uTextCustomerName.Text = "";
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);
                            
                            this.uComboPlantCode.DropDown();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextProductCode.TextLength <= 1 || this.uTextProductCode.SelectedText == this.uTextProductCode.Text)
                    {
                        this.uTextProductName.Text = "";
                        this.uTextCustomerCode.Text = "";
                        this.uTextCustomerName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 제품 텍스트박스 키다운 이벤트
        private void uTextSearchProductCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uComboSearchPlant.Value.ToString() != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        if (this.uTextSearchProductCode.Text != "")
                        {
                            string strPlantCode = this.uComboSearchPlant.Value.ToString();
                            string strProductCode = this.uTextSearchProductCode.Text;

                            DataTable dtProduct = GetProductInfo(strPlantCode, strProductCode);

                            if (dtProduct.Rows.Count > 0)
                            {
                                this.uTextSearchProductName.Text = dtProduct.Rows[0]["ProductName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M001091",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchProductCode.Text = "";
                                this.uTextSearchProductName.Text = "";
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchProductCode.TextLength <= 1 || this.uTextSearchProductCode.SelectedText == this.uTextSearchProductCode.Text)
                    {
                        this.uTextSearchProductName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ValueChanged
        private void uTextInspectUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextInspectUserName.Text.Equals(string.Empty))
                this.uTextInspectUserName.Clear();
        }

        private void uTextWriteUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextWriteUserName.Text.Equals(string.Empty))
                this.uTextWriteUserName.Clear();
        }

        private void uTextProductCode_ValueChanged(object sender, EventArgs e)
        {
            if (this.uTextProductName.Text.Equals(string.Empty))
                this.uTextProductName.Clear();
        }

        private void uTextAcceptUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextAcceptUserName.Text.Equals(string.Empty))
                this.uTextAcceptUserName.Clear();
        }

        #endregion

        // 그리드 더블클릭 이벤트
        private void uGridReliabilityList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlantCode.Enabled = false;

                e.Row.Fixed = true;

                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strReliabilityNo = e.Row.Cells["ReliabilityNo"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                SearchDetailData(strPlantCode, strReliabilityNo);

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                #region  상세조회 메소드 처리에 의한 주석처리
                ////// 프로그래스 팝업창 생성
                ////QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                ////Thread threadPop = m_ProgressPopup.mfStartThread();
                ////m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                ////this.MdiParent.Cursor = Cursors.WaitCursor;

                ////QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.ReliabilityInspect), "ReliabilityInspect");
                ////QRPQAT.BL.QATREL.ReliabilityInspect clsRel = new QRPQAT.BL.QATREL.ReliabilityInspect();
                ////brwChannel.mfCredentials(clsRel);

                ////// Method 호출
                ////DataTable dtRtn = clsRel.mfReadQATReliabilityInspectDetail(strPlantCode, strReliabilityNo, m_resSys.GetString("SYS_LANG"));

                ////if (dtRtn.Rows.Count > 0)
                ////{
                ////    // 값 설정
                ////    this.uComboPlantCode.Value = dtRtn.Rows[0]["PlantCode"].ToString();
                ////    this.uTextReliabilityNo.Text = dtRtn.Rows[0]["ReliabilityNo"].ToString();
                ////    this.uTextWriteUserID.Text = dtRtn.Rows[0]["WriteUserID"].ToString();
                ////    this.uTextWriteUserName.Text = dtRtn.Rows[0]["WriteUserName"].ToString();
                ////    this.uDateWriteDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["WriteDate"].ToString()).ToString("yyyy-MM-dd");
                ////    this.uTextAppraisalObject.Text = dtRtn.Rows[0]["AppraisalObject"].ToString();
                ////    this.uTextChangeBefore.Text = dtRtn.Rows[0]["ChangeBefore"].ToString();
                ////    this.uTextChangeAfter.Text = dtRtn.Rows[0]["ChangeAfter"].ToString();
                ////    this.uTextProductCode.Text = dtRtn.Rows[0]["ProductCode"].ToString();
                ////    this.uTextProductName.Text = dtRtn.Rows[0]["ProductName"].ToString();
                ////    this.uTextCustomerCode.Text = dtRtn.Rows[0]["CustomerCode"].ToString();
                ////    this.uTextCustomerName.Text = dtRtn.Rows[0]["CustomerName"].ToString();
                ////    this.uTextChip_Size.Text = dtRtn.Rows[0]["Chip_Size"].ToString();
                ////    this.uTextAdhesive_Model.Text = dtRtn.Rows[0]["Adhesive_Model"].ToString();
                ////    this.uTextAdhesive_Vender.Text = dtRtn.Rows[0]["Adhesive_Vender"].ToString();
                ////    this.uTextLForPCBModel.Text = dtRtn.Rows[0]["LForPCBModel"].ToString();
                ////    this.uTextLForPCBVender.Text = dtRtn.Rows[0]["LForPCBVender"].ToString();
                ////    this.uTextWireModel.Text = dtRtn.Rows[0]["WireModel"].ToString();
                ////    this.uTextWireVender.Text = dtRtn.Rows[0]["WireVender"].ToString();
                ////    this.uTextEMCModel.Text = dtRtn.Rows[0]["EMCModel"].ToString();
                ////    this.uTextEMCVender.Text = dtRtn.Rows[0]["EMCVender"].ToString();
                ////    this.uTextSolderBallModel.Text = dtRtn.Rows[0]["SolderBallModel"].ToString();
                ////    this.uTextSolderBallVender.Text = dtRtn.Rows[0]["SolderBallVender"].ToString();
                ////    decimal dblReqQty = 0.0M;
                ////    if (decimal.TryParse(dtRtn.Rows[0]["ReqQty"].ToString(), out dblReqQty))
                ////        this.uNumReqQty.Value = dblReqQty.ToString();
                    
                ////    //this.uTextReqQty.Text = dtRtn.Rows[0]["ReqQty"].ToString();
                ////    this.uTextEtcDesc.Text = dtRtn.Rows[0]["EtcDesc"].ToString();
                ////    this.uComboTCOptionFlag.Value = dtRtn.Rows[0]["TCOptionFlag"].ToString();
                ////    this.uComboTCOptionCondition.Value = dtRtn.Rows[0]["TCOptionCondition"].ToString();
                ////    this.uTextTCOptionTime.Text = dtRtn.Rows[0]["TCOptionTime"].ToString();
                ////    this.uTextTCOptionDate.Text = dtRtn.Rows[0]["TCOptionDate"].ToString();
                ////    this.uComboBakeFlag.Value = dtRtn.Rows[0]["BakeFlag"].ToString();
                ////    this.uComboBakeCondition.Value = dtRtn.Rows[0]["BakeCondition"].ToString();
                ////    this.uTextBakeTime.Text = dtRtn.Rows[0]["BakeTime"].ToString();
                ////    this.uTextBakeDate.Text = dtRtn.Rows[0]["BakeDate"].ToString();
                ////    this.uComboHumidityFlag1.Value = dtRtn.Rows[0]["HumidityFlag1"].ToString();
                ////    this.uComboHumidityCondition1.Value = dtRtn.Rows[0]["HumidityCondition1"].ToString();
                ////    this.uTextHumidityTime1.Text = dtRtn.Rows[0]["HumidityTime1"].ToString();
                ////    this.uTextHumidityDate1.Text = dtRtn.Rows[0]["HumidityDate1"].ToString();
                ////    this.uComboReflowFlag.Value = dtRtn.Rows[0]["ReflowFlag"].ToString();
                ////    this.uComboReflowCondition.Value = dtRtn.Rows[0]["ReflowCondition"].ToString();
                ////    this.uTextReflowTime.Text = dtRtn.Rows[0]["ReflowTime"].ToString();
                ////    this.uTextReflowDate.Text = dtRtn.Rows[0]["ReflowDate"].ToString();
                ////    this.uComboTCFlag.Value = dtRtn.Rows[0]["TCFlag"].ToString();
                ////    this.uComboTCCondition.Value = dtRtn.Rows[0]["TCCondition"].ToString();
                ////    this.uTextTCTime.Text = dtRtn.Rows[0]["TCTime"].ToString();
                ////    this.uTextTCDate.Text = dtRtn.Rows[0]["TCDate"].ToString();
                ////    this.uComboPCTFlag.Value = dtRtn.Rows[0]["PCTFlag"].ToString();
                ////    //this.uComboPCTCondition.Value = dtRtn.Rows[0]["PCTCondition"].ToString();
                ////    this.uTextPCTCondition.Text = dtRtn.Rows[0]["PCTCondition"].ToString();
                ////    this.uTextPCTTime.Text = dtRtn.Rows[0]["PCTTime"].ToString();
                ////    this.uTextPCTDate.Text = dtRtn.Rows[0]["PCTDate"].ToString();
                ////    this.uComboHASTFlag.Value = dtRtn.Rows[0]["HASTFlag"].ToString();
                ////    //this.uComboHASTCondition.Value = dtRtn.Rows[0]["HASTCondition"].ToString();
                ////    this.uTextHASTCondition.Text = dtRtn.Rows[0]["HASTCondition"].ToString();
                ////    this.uTextHASTTime.Text = dtRtn.Rows[0]["HASTTime"].ToString();
                ////    this.uTextHASTDate.Text= dtRtn.Rows[0]["HASTDate"].ToString();
                ////    this.uComboHumidityFlag2.Value = dtRtn.Rows[0]["HumidityFlag2"].ToString();
                ////    this.uComboHumidityCondition2.Value = dtRtn.Rows[0]["HumidityCondition2"].ToString();
                ////    this.uTextHumidityTime2.Text = dtRtn.Rows[0]["HumidityTime2"].ToString();
                ////    this.uTextHumidityDate2.Text = dtRtn.Rows[0]["HumidityDate2"].ToString();
                ////    this.uComboHTSFlag.Value = dtRtn.Rows[0]["HTSFlag"].ToString();
                ////    this.uComboHTSCondition.Value = dtRtn.Rows[0]["HTSCondition"].ToString();
                ////    this.uTextHTSTime.Text = dtRtn.Rows[0]["HTSTime"].ToString();
                ////    this.uTextHTSDate.Text = dtRtn.Rows[0]["HTSDate"].ToString();

                ////    this.uTextInspectUserID.Text = dtRtn.Rows[0]["InspectUserID"].ToString();
                ////    this.uTextInspectUserName.Text = dtRtn.Rows[0]["InspectUserName"].ToString();
                ////    this.uTextInspectFileName.Text = dtRtn.Rows[0]["InspectFileName"].ToString();
                ////    this.uDateInspectDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["InspectDate"]).ToString("yyyy-MM-dd");
                ////    if (dtRtn.Rows[0]["PassFailFlag"].Equals(DBNull.Value) || dtRtn.Rows[0]["PassFailFlag"].ToString().Equals(string.Empty))
                ////        this.uOptionPassFailFlag.CheckedIndex = -1;
                ////    else
                ////        this.uOptionPassFailFlag.Value = dtRtn.Rows[0]["PassFailFlag"].ToString();

                ////    this.uComboGubun.Value = dtRtn.Rows[0]["Gubun"];    //구분
                ////    this.uTextAcceptUserID.Text = dtRtn.Rows[0]["AcceptUserID"].ToString(); //접수자
                ////    this.uTextAcceptUserName.Text = dtRtn.Rows[0]["AcceptUserName"].ToString(); //접수자
                ////    this.uDateAccept.Value = dtRtn.Rows[0]["AcceptDate"].ToString() == "" ? DateTime.Now.Date : dtRtn.Rows[0]["AcceptDate"];                   //접수일
                ////    this.uCheckAcceptFlag.Checked = dtRtn.Rows[0]["AcceptFlag"].ToString() == "T" ? true : false; //접수완료

                ////    if (dtRtn.Rows[0]["CompleteFlag"].ToString().Equals("T"))
                ////    {
                ////        this.uCheckCompleteFlag.Enabled = false;
                ////        this.uCheckCompleteFlag.Checked = true;
                ////    }
                ////    else
                ////    {
                ////        this.uCheckCompleteFlag.Enabled = true;
                ////        this.uCheckCompleteFlag.Checked = false;
                ////    }

                ////    this.uGroupBoxContentsArea.Expanded = true;
                ////}
                ////else
                ////{
                ////    WinMessageBox msg = new WinMessageBox();
                ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                ////                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                                        "M001135", "M000651", "M001074",
                ////                                        Infragistics.Win.HAlign.Right);
                ////}

                ////// 팦업창 Close
                ////this.MdiParent.Cursor = Cursors.Default;
                ////m_ProgressPopup.mfCloseProgressPopup(this);
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 0버튼 이벤트

        private void uNumReqQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 스핀버튼 이벤트

        private void uNumReqQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uNumReqQty.Focus();
                    uNumReqQty.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uNumReqQty.Focus();
                    uNumReqQty.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        // 첨부파일 버튼 이벤트

        private void uTextInspectFileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "UP")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;

                        if (CheckingSpecialText(strImageFile))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result =msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        
                        this.uTextInspectFileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "DOWN")
                {
                    if (this.uTextInspectFileName.Text.Equals(string.Empty))
                        return;

                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    
                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextInspectFileName.Text.Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0020");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextInspectFileName.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기

                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextInspectFileName.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 체크박스 체크시 이벤트

        private void uCheckCompleteFlag_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                // CheckEditor 가 활성화 상태일때
                if (this.uCheckCompleteFlag.Enabled == true)
                {
                    if (this.uCheckCompleteFlag.Checked)
                    {
                        if (!this.uCheckAcceptFlag.Checked)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001317", "M001318",
                                            Infragistics.Win.HAlign.Right);

                            this.uCheckCompleteFlag.Checked = false;
                            return;
                        }
                        if (this.uOptionPassFailFlag.CheckedIndex.Equals(-1))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001245", "M001246",
                                            Infragistics.Win.HAlign.Right);

                            this.uCheckCompleteFlag.Checked = false;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000984", "M000982",
                                            Infragistics.Win.HAlign.Right);
                            this.uCheckAcceptFlag.Enabled = false;
                        }
                    }
                    else
                        this.uCheckAcceptFlag.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmQATZ0007_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        private void SearchDetailData(string strPlantCode, string strReliabilityNo)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.ReliabilityInspect), "ReliabilityInspect");
                QRPQAT.BL.QATREL.ReliabilityInspect clsRel = new QRPQAT.BL.QATREL.ReliabilityInspect();
                brwChannel.mfCredentials(clsRel);

                // Method 호출
                DataTable dtRtn = clsRel.mfReadQATReliabilityInspectDetail(strPlantCode, strReliabilityNo, m_resSys.GetString("SYS_LANG"));

                if (dtRtn.Rows.Count > 0)
                {
                    // 값 설정
                    this.uComboPlantCode.Value = dtRtn.Rows[0]["PlantCode"].ToString();
                    this.uTextReliabilityNo.Text = dtRtn.Rows[0]["ReliabilityNo"].ToString();
                    this.uTextWriteUserID.Text = dtRtn.Rows[0]["WriteUserID"].ToString();
                    this.uTextWriteUserName.Text = dtRtn.Rows[0]["WriteUserName"].ToString();
                    this.uDateWriteDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["WriteDate"].ToString()).ToString("yyyy-MM-dd");
                    this.uTextAppraisalObject.Text = dtRtn.Rows[0]["AppraisalObject"].ToString();
                    this.uTextChangeBefore.Text = dtRtn.Rows[0]["ChangeBefore"].ToString();
                    this.uTextChangeAfter.Text = dtRtn.Rows[0]["ChangeAfter"].ToString();
                    this.uTextProductCode.Text = dtRtn.Rows[0]["ProductCode"].ToString();
                    this.uTextProductName.Text = dtRtn.Rows[0]["ProductName"].ToString();
                    this.uTextCustomerCode.Text = dtRtn.Rows[0]["CustomerCode"].ToString();
                    this.uTextCustomerName.Text = dtRtn.Rows[0]["CustomerName"].ToString();
                    this.uTextChip_Size.Text = dtRtn.Rows[0]["Chip_Size"].ToString();
                    this.uTextAdhesive_Model.Text = dtRtn.Rows[0]["Adhesive_Model"].ToString();
                    this.uTextAdhesive_Vender.Text = dtRtn.Rows[0]["Adhesive_Vender"].ToString();
                    this.uTextLForPCBModel.Text = dtRtn.Rows[0]["LForPCBModel"].ToString();
                    this.uTextLForPCBVender.Text = dtRtn.Rows[0]["LForPCBVender"].ToString();
                    this.uTextWireModel.Text = dtRtn.Rows[0]["WireModel"].ToString();
                    this.uTextWireVender.Text = dtRtn.Rows[0]["WireVender"].ToString();
                    this.uTextEMCModel.Text = dtRtn.Rows[0]["EMCModel"].ToString();
                    this.uTextEMCVender.Text = dtRtn.Rows[0]["EMCVender"].ToString();
                    this.uTextSolderBallModel.Text = dtRtn.Rows[0]["SolderBallModel"].ToString();
                    this.uTextSolderBallVender.Text = dtRtn.Rows[0]["SolderBallVender"].ToString();
                    decimal dblReqQty = 0.0M;
                    if (decimal.TryParse(dtRtn.Rows[0]["ReqQty"].ToString(), out dblReqQty))
                        this.uNumReqQty.Value = dblReqQty.ToString();

                    //this.uTextReqQty.Text = dtRtn.Rows[0]["ReqQty"].ToString();
                    this.uTextEtcDesc.Text = dtRtn.Rows[0]["EtcDesc"].ToString();
                    this.uComboTCOptionFlag.Value = dtRtn.Rows[0]["TCOptionFlag"].ToString();
                    this.uComboTCOptionCondition.Value = dtRtn.Rows[0]["TCOptionCondition"].ToString();
                    this.uTextTCOptionTime.Text = dtRtn.Rows[0]["TCOptionTime"].ToString();
                    this.uTextTCOptionDate.Text = dtRtn.Rows[0]["TCOptionDate"].ToString();
                    this.uComboBakeFlag.Value = dtRtn.Rows[0]["BakeFlag"].ToString();
                    this.uComboBakeCondition.Value = dtRtn.Rows[0]["BakeCondition"].ToString();
                    this.uTextBakeTime.Text = dtRtn.Rows[0]["BakeTime"].ToString();
                    this.uTextBakeDate.Text = dtRtn.Rows[0]["BakeDate"].ToString();
                    this.uComboHumidityFlag1.Value = dtRtn.Rows[0]["HumidityFlag1"].ToString();
                    this.uComboHumidityCondition1.Value = dtRtn.Rows[0]["HumidityCondition1"].ToString();
                    this.uTextHumidityTime1.Text = dtRtn.Rows[0]["HumidityTime1"].ToString();
                    this.uTextHumidityDate1.Text = dtRtn.Rows[0]["HumidityDate1"].ToString();
                    this.uComboReflowFlag.Value = dtRtn.Rows[0]["ReflowFlag"].ToString();
                    this.uComboReflowCondition.Value = dtRtn.Rows[0]["ReflowCondition"].ToString();
                    this.uTextReflowTime.Text = dtRtn.Rows[0]["ReflowTime"].ToString();
                    this.uTextReflowDate.Text = dtRtn.Rows[0]["ReflowDate"].ToString();
                    this.uComboTCFlag.Value = dtRtn.Rows[0]["TCFlag"].ToString();
                    this.uComboTCCondition.Value = dtRtn.Rows[0]["TCCondition"].ToString();
                    this.uTextTCTime.Text = dtRtn.Rows[0]["TCTime"].ToString();
                    this.uTextTCDate.Text = dtRtn.Rows[0]["TCDate"].ToString();
                    this.uComboPCTFlag.Value = dtRtn.Rows[0]["PCTFlag"].ToString();
                    //this.uComboPCTCondition.Value = dtRtn.Rows[0]["PCTCondition"].ToString();
                    this.uTextPCTCondition.Text = dtRtn.Rows[0]["PCTCondition"].ToString();
                    this.uTextPCTTime.Text = dtRtn.Rows[0]["PCTTime"].ToString();
                    this.uTextPCTDate.Text = dtRtn.Rows[0]["PCTDate"].ToString();
                    this.uComboHASTFlag.Value = dtRtn.Rows[0]["HASTFlag"].ToString();
                    //this.uComboHASTCondition.Value = dtRtn.Rows[0]["HASTCondition"].ToString();
                    this.uTextHASTCondition.Text = dtRtn.Rows[0]["HASTCondition"].ToString();
                    this.uTextHASTTime.Text = dtRtn.Rows[0]["HASTTime"].ToString();
                    this.uTextHASTDate.Text = dtRtn.Rows[0]["HASTDate"].ToString();
                    this.uComboHumidityFlag2.Value = dtRtn.Rows[0]["HumidityFlag2"].ToString();
                    this.uComboHumidityCondition2.Value = dtRtn.Rows[0]["HumidityCondition2"].ToString();
                    this.uTextHumidityTime2.Text = dtRtn.Rows[0]["HumidityTime2"].ToString();
                    this.uTextHumidityDate2.Text = dtRtn.Rows[0]["HumidityDate2"].ToString();
                    this.uComboHTSFlag.Value = dtRtn.Rows[0]["HTSFlag"].ToString();
                    this.uComboHTSCondition.Value = dtRtn.Rows[0]["HTSCondition"].ToString();
                    this.uTextHTSTime.Text = dtRtn.Rows[0]["HTSTime"].ToString();
                    this.uTextHTSDate.Text = dtRtn.Rows[0]["HTSDate"].ToString();

                    this.uTextInspectUserID.Text = dtRtn.Rows[0]["InspectUserID"].ToString();
                    this.uTextInspectUserName.Text = dtRtn.Rows[0]["InspectUserName"].ToString();
                    this.uTextInspectFileName.Text = dtRtn.Rows[0]["InspectFileName"].ToString();
                    this.uDateInspectDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["InspectDate"]).ToString("yyyy-MM-dd");
                    if (dtRtn.Rows[0]["PassFailFlag"].Equals(DBNull.Value) || dtRtn.Rows[0]["PassFailFlag"].ToString().Equals(string.Empty))
                        this.uOptionPassFailFlag.CheckedIndex = -1;
                    else
                        this.uOptionPassFailFlag.Value = dtRtn.Rows[0]["PassFailFlag"].ToString();

                    this.uComboGubun.Value = dtRtn.Rows[0]["Gubun"];    //구분
                    this.uTextAcceptUserID.Text = dtRtn.Rows[0]["AcceptUserID"].ToString(); //접수자
                    this.uTextAcceptUserName.Text = dtRtn.Rows[0]["AcceptUserName"].ToString(); //접수자
                    this.uDateAccept.Value = dtRtn.Rows[0]["AcceptDate"].ToString() == "" ? DateTime.Now.Date : dtRtn.Rows[0]["AcceptDate"];                   //접수일
                    this.uCheckAcceptFlag.Checked = dtRtn.Rows[0]["AcceptFlag"].ToString() == "T" ? true : false; //접수완료

                    this.uTextPackage.Text = dtRtn.Rows[0]["Package"].ToString();   // PSTS 2012-11-07 Package
                    this.uTextProductLN.Text = dtRtn.Rows[0]["ProductLN"].ToString();   // PSTS 2012-11-07 제품L/N
                    this.uTextProductPN.Text = dtRtn.Rows[0]["ProductPN"].ToString();   // PSTS 2012-11-07 제품P/N
                    this.uComboProcessType.Value = dtRtn.Rows[0]["ProcessType"].ToString();  // PSTS 2012-11-07 ProcessType (FS/HF선택)


                    if (dtRtn.Rows[0]["CompleteFlag"].ToString().Equals("T"))
                    {
                        this.uCheckCompleteFlag.Enabled = false;
                        this.uCheckCompleteFlag.Checked = true;
                    }
                    else
                    {
                        this.uCheckCompleteFlag.Enabled = true;
                        this.uCheckCompleteFlag.Checked = false;
                    }

                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000651", "M001074",
                                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }
    }
}
