﻿/*----------------------------------------------------------------------*/
/* 시스템명     : SRR 입력항목 등록                                     */
/* 모듈(분류)명 : 고객불만관리                                          */
/* 프로그램ID   : frmQATZ0020.cs                                        */
/* 프로그램명   : SRR 입력항목 등록                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-12-24                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPQAT.UI
{
    public partial class frmQATZ0020 : Form, QRPCOM.QRPGLO.IToolbar
    {
        public override Size MinimumSize
        {
            get
            {
                Size size = new Size(1070, 850);
                return size;
            }
            set
            {
                Size size = new Size(1070, 850);
                base.MinimumSize = size;
            }
        }

        // Resource 호출을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        public frmQATZ0020()
        {
            InitializeComponent();
            this.Activated += new EventHandler(frmQATZ0020_Activated);
            this.Load += new EventHandler(frmQATZ0020_Load);
            this.FormClosing += new FormClosingEventHandler(frmQATZ0020_FormClosing);
        }

        void frmQATZ0020_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
            wGrid.mfSaveGridColumnProperty(this);
        }

        void frmQATZ0020_Activated(object sender, EventArgs e)
        {
            // 툴바설정
            QRPCOM.QRPGLO.QRPBrowser ToolButton = new QRPCOM.QRPGLO.QRPBrowser();
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
            m_resSys.Close();
        }

        void frmQATZ0020_Load(object sender, EventArgs e)
        {
            // 권한설정
            SetToolAuth();

            // 초기화 메소드 호출
            InitEvent();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitEtc();

            // ContentsArea 접힘상태
            this.uGroupBoxContentsArea.Expanded = false;

            // 공장코드 숨김상태로
            this.uComboPlant.Hide();
            this.uComboSearchPlant.Hide();
            this.uLabelPlant.Hide();
            this.uLabelSearchPlant.Hide();
        }

        #region IToolbar 멤버

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (!this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = true;
                else
                    Clear();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            // ProgressBar 생성
            QRPCOM.QRPGLO.QRPProgressBar m_ProgressPopup = new QRPCOM.QRPGLO.QRPProgressBar();

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (!this.uGroupBoxContentsArea.Expanded)
                    return;
                else if (this.uComboPlant.SelectedIndex <= 0)
                    return;
                else if (this.uComboCustomer.SelectedIndex <= 0)
                    return;
                else if (this.uComboProdType.SelectedIndex < 0)
                    return;
                else if (this.uComboEvaluationPeriod.SelectedIndex <= 0)
                    return;

                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();

                if (msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {

                    System.Threading.Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // Connection BL
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");
                    using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                    {
                        brwChannel.mfCredentials(clsSRR_H);
                        // 변수설정
                        string strPlantCode = this.uComboPlant.Value.ToString();
                        string strCustomerCode = this.uComboCustomer.Value.ToString();
                        //string strProdType = this.uComboProdType.Text;
                        string strProdType = this.uComboProdType.SelectedItem.DisplayText;
                        string strEvaluationPeriod = this.uComboEvaluationPeriod.Value.ToString();

                        string strErrRtn = clsSRR_H.mfDeleteQATSRRMasterHeader(strPlantCode, strCustomerCode, strProdType, strEvaluationPeriod);

                        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000677",
                                                            Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                            , msg.GetMessge_Text("M000638", m_resSys.GetString("SYS_LANG"))
                                                            , ErrRtn.ErrMessage,
                                                            Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                m_resSys.Close();
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridSRRList.Rows.Count > 0)
                {
                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridSRRList);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {

            // 프로그래스 팝업창 생성
            QRPCOM.QRPGLO.QRPProgressBar m_ProgressPopup = new QRPCOM.QRPGLO.QRPProgressBar();

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();

                this.uComboSearchCustomer.Focus();

                #region 필수입력사항 확인

                if (this.uComboPlant.SelectedIndex <= 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.Focus();
                    this.uComboPlant.DropDown();
                    m_resSys.Close();
                    return;
                }
                else if (this.uComboCustomer.SelectedIndex <= 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001274", Infragistics.Win.HAlign.Right);

                    this.uComboCustomer.Focus();
                    this.uComboCustomer.DropDown();
                    m_resSys.Close();
                    return;
                }
                else if (this.uComboProdType.SelectedIndex < 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001533", Infragistics.Win.HAlign.Right);

                    this.uComboProdType.Focus();
                    this.uComboProdType.DropDown();
                    m_resSys.Close();
                    return;
                }
                else if (this.uComboEvaluationPeriod.SelectedIndex <= 0)
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001534", Infragistics.Win.HAlign.Right);

                    this.uComboEvaluationPeriod.Focus();
                    this.uComboEvaluationPeriod.DropDown();
                    m_resSys.Close();
                    return;
                }
                else if (this.uGridSRRItemList.Rows.Count.Equals(0))
                {
                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001234", "M001054", Infragistics.Win.HAlign.Right);

                    m_resSys.Close();
                    return;
                }

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;

                #endregion

                // 저장여부를 묻는다
                if (msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    System.Threading.Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    #region 저장정보 설정

                    this.uGridSRRItemList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    this.uGridSRRItemList.UpdateData();

                    // Header 정보
                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strCustomerCode = this.uComboCustomer.Value.ToString();
                    //string strProdType = this.uComboProdType.Text;
                    string strProdType = this.uComboProdType.SelectedItem.DisplayText;
                    string strEvaluationPeriod = this.uComboEvaluationPeriod.Value.ToString();
                    string strTotalScore = this.uTextTotal.Text;

                    // Item 정보 XML 형태로 Parsing
                    // Item List Set Seq And Value Check
                    if (!SetitemSeqAndCheckValue())
                        return;

                    string strXML = string.Empty;
                    using (DataTable dtItem = ((DataTable)this.uGridSRRItemList.DataSource).Copy())
                    {
                        using (System.IO.MemoryStream msTreamXML = new System.IO.MemoryStream())
                        {
                            dtItem.WriteXml(msTreamXML);
                            msTreamXML.Seek(0, System.IO.SeekOrigin.Begin);
                            using (System.IO.StreamReader srXML = new System.IO.StreamReader(msTreamXML))
                            {
                                strXML = srXML.ReadToEnd();
                                srXML.Close();
                            }
                            msTreamXML.Close();
                        }
                    }

                    #endregion

                    // Connection BL
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");
                    using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                    {
                        string strErrRtn = clsSRR_H.mfSaveQATSRRMasterHeader(strPlantCode, strCustomerCode, strProdType, strEvaluationPeriod, strTotalScore
                            , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), strXML);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                    , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                    , ErrRtn.ErrMessage, Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                m_resSys.Close();
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            // 프로그래스바 생성
            QRPCOM.QRPGLO.QRPProgressBar m_ProgressPopup = new QRPCOM.QRPGLO.QRPProgressBar();
            System.Threading.Thread threadPop = m_ProgressPopup.mfStartThread();
            m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
            this.MdiParent.Cursor = Cursors.WaitCursor;

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();
                
                if (this.uComboSearchPlant.SelectedIndex < 0)
                    this.uComboSearchPlant.SelectedIndex = 0;

                if (this.uComboSearchCustomer.SelectedIndex < 0)
                    this.uComboSearchCustomer.SelectedIndex = 0;

                // 검색조건 변수 설정
                string[] strSplit = {"||"};
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string[] strCustomerProdType = this.uComboSearchCustomer.Value.ToString().Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
                string strCustomerCode = string.Empty;
                string strProdType = string.Empty;
                if (strCustomerProdType.Length > 1)
                {
                    strCustomerCode = strCustomerProdType[0];
                    strProdType = strCustomerProdType[1];
                }

                // SET BL Connection
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");
                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsHeader = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsHeader);

                    #region 데이터 테이블 형식

                    ////using (DataTable dtHeader = clsHeader.mfReadQATSRRMasterHeader_datatable(strPlantCode, strCustomerCode, strProdType, m_resSys.GetString("SYS_LANG")))
                    ////{
                    ////    this.uGridSRRList.SetDataBinding(dtHeader.Copy(), string.Empty);

                    ////    if (dtHeader.Rows.Count > 0)
                    ////    {
                    ////        QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                    ////        wGrid.mfSetAutoResizeColWidth(this.uGridSRRList, 0);
                    ////    }
                    ////    else
                    ////    {
                    ////        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    ////    }
                    ////}

                    #endregion

                    #region Binary 형식

                    string strSearch = clsHeader.mfReadQATSRRMasterHeader(strPlantCode, strCustomerCode, strProdType, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        if (!string.IsNullOrEmpty(strSearch))
                        {
                            using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                            {
                                using (DataTable dtHeader = new DataTable())
                                {
                                    dtHeader.BeginLoadData();
                                    dtHeader.ReadXml(xmlSR);
                                    dtHeader.EndLoadData();

                                    this.uGridSRRList.SetDataBinding(dtHeader.Copy(), string.Empty);
                                }
                            }

                            #region Method 사용

                            ////// Datatable DeCompression
                            ////byte[] deflateData = Convert.FromBase64String(strSearch);
                            ////using (System.IO.MemoryStream deflatedStream = new System.IO.MemoryStream(deflateData, false))
                            ////{
                            ////    using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(deflatedStream, System.IO.Compression.CompressionMode.Decompress))
                            ////    {
                            ////        using (System.IO.MemoryStream DeCompressStream = new System.IO.MemoryStream())
                            ////        {
                            ////            int read = 0;
                            ////            byte[] readBuffer = new byte[64000];
                            ////            while ((read = deflateStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
                            ////                DeCompressStream.Write(readBuffer, 0, read);
                            ////            DeCompressStream.Seek(0L, System.IO.SeekOrigin.Begin);
                            ////            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                            ////            using (DataTable dtHeader = bf.Deserialize(DeCompressStream) as DataTable)
                            ////            {
                            ////                this.uGridSRRList.SetDataBinding(dtHeader.Copy(), string.Empty);
                            ////            }
                            ////        }
                            ////    }
                            ////}

                            #endregion

                            if (this.uGridSRRList.Rows.Count.Equals(0))
                            {
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                wGrid.mfSetAutoResizeColWidth(this.uGridSRRList, 0);
                            }

                        }
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                            , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                            , ErrRtn.SystemInnerException + "<br/>" +
                                ErrRtn.SystemMessage + "<br/>" +
                                ErrRtn.SystemStackTrace
                            , Infragistics.Win.HAlign.Right);
                    }

                    #endregion

                    #region XML 형식            // BL 오류;;;;

                    ////string strSearch = clsHeader.mfReadQATSRRMasterHeader(strPlantCode, strCustomerCode, strProdType, m_resSys.GetString("SYS_LANG"));

                    ////QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ////ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                    ////if (ErrRtn.SystemInnerException.Equals(string.Empty) &&
                    ////    ErrRtn.SystemMessage.Equals(string.Empty) &&
                    ////    ErrRtn.SystemStackTrace.Equals(string.Empty))
                    ////{

                    ////    System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                    ////    xmlDoc.LoadXml(strSearch);

                    ////    this.uGridSRRList.SetDataBinding(xmlDoc, "DocumentElement");

                    ////    if (this.uGridSRRList.Rows.Count.Equals(0))
                    ////    {
                    ////        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    ////    }
                    ////    else
                    ////    {
                    ////        QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                    ////        wGrid.mfSetAutoResizeColWidth(this.uGridSRRList, 0);
                    ////    }
                    ////}
                    ////else
                    ////{
                    ////    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                    ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////        , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                    ////        , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                    ////        , ErrRtn.SystemInnerException + "<br/>" +
                    ////            ErrRtn.SystemMessage + "<br/>" +
                    ////            ErrRtn.SystemStackTrace
                    ////        , Infragistics.Win.HAlign.Right);
                    ////}

                    #endregion
                }

                // ContentsArea 접힘상태
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                m_resSys.Close();
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        private void InitEvent()
        {
            this.uComboSearchPlant.ValueChanged += new EventHandler(uComboSearchPlant_ValueChanged);
            this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            this.uButtonDelete.Click += new EventHandler(uButtonDelete_Click);
            this.uGridSRRItemList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSRRItemList_AfterCellUpdate);
            this.uGridSRRItemList.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSRRItemList_CellChange);
            this.uGridSRRList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridSRRList_DoubleClickRow);
            this.uGridSRRList.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(uGridSRRList_InitializeLayout);
            this.uComboSearchCustomer.BeforeDropDown += new CancelEventHandler(uComboSearchCustomer_BeforeDropDown);
            this.uComboCustomer.ValueChanged += new EventHandler(uComboCustomer_ValueChanged);
            //this.uTextProdType.AfterExitEditMode += new EventHandler(uTextProdType_AfterExitEditMode);
            this.uComboProdType.ValueChanged += new EventHandler(uComboProdType_ValueChanged);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                using (QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth())
                {
                    brwChannel.mfCredentials(UAuth);
                    DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                    m_resSys.Close();
                    QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                    Global.mfMakeToolInfoResource(dtAuth);
                    dtAuth.Dispose();
                }
                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinLabel wLabel = new QRPCOM.QRPUI.WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사 구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProdType, "구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEvaluationPeriod, "평가주기", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelTotal, "Total", m_resSys.GetString("SYS_FONTNAME"), true, false);

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinButton wButton = new QRPCOM.QRPUI.WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                QRPCOM.QRPUI.WinComboEditor wCombo = new QRPCOM.QRPUI.WinComboEditor();
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                
                // 공장콤보 설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                using (QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant())
                {
                    brwChannel.mfCredentials(clsPlant);

                    DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "", "PlantCode", "PlantName", dtPlant);

                    wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "", "PlantCode", "PlantName", dtPlant);

                    dtPlant.Dispose();
                }

                // 고객사 콤보 설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                using (QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer())
                {
                    brwChannel.mfCredentials(clsCustomer);

                    DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "CustomerCode", "CustomerName", dtCustomer);

                    dtCustomer.Dispose();
                }

                // 평가주기 콤보설정
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                using (QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode())
                {
                    brwChannel.mfCredentials(clsCom);

                    DataTable dtComCode = clsCom.mfReadCommonCode("C0081", m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboEvaluationPeriod, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "MON", "", "", "ComCode", "ComCodeName", dtComCode);
                }

                // 구분 콤보 설정
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                using (QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode())
                {
                    brwChannel.mfCredentials(clsUserCom);

                    DataTable dtProdType = clsUserCom.mfReadUserCommonCode("QUA", "U0020", m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboProdType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComCode", "ComCodeName", dtProdType);
                }

                this.uComboProdType.Nullable = false;
                this.uComboProdType.NullText = string.Empty;
                this.uComboProdType.NullTextAppearance.BackColor = Color.Salmon;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                #region Search List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSRRList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "ProdType", "구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "EvaluationPeriod", "평가주기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridSRRList, 0, "MergeCell", "MergeCell", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 3
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "CustomerProdType", "고객사구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "EvaluationType", "평가구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "EvaluationItem", "평가항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "Score", "배점", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "n,nnn,nnn,nnn.n", "0.0");

                wGrid.mfSetGridColumn(this.uGridSRRList, 0, "TotalScore", "총점", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0.0");

                // 조회리스트 편집불가
                this.uGridSRRList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                // 헤더클릭시 Sorting 방지
                this.uGridSRRList.DisplayLayout.Bands[0].Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

                #endregion

                #region ItemList

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSRRItemList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 기본데이터 바인딩
                this.uGridSRRItemList.SetDataBinding(Set_ItemDataInfo(), string.Empty);

                // 컬럼설정
                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 70, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "EvaluationType", "평가구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "EvaluationItem", "평가항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfChangeGridColumnStyle(this.uGridSRRItemList, 0, "Score", "배점", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nn.n", "0.0");

                // Score 컬럼 PomptChar 설정
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Score"].PromptChar = ' ';
                // Null Value 설정
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Score"].Nullable = Infragistics.Win.UltraWinGrid.Nullable.Automatic;
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Score"].NullText = "0.0";

                // 선택 체크박스 설정
                this.uGridSRRItemList.DisplayLayout.Bands[0].Columns["Check"].DefaultCellValue = false;

                #endregion

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                this.titleArea.mfSetLabelText("SRR 입력항목 등록", m_resSys.GetString("SYS_FONTNAME"), 12);

                this.uTextTotal.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextTotal.ReadOnly = true;
                this.uTextTotal.Appearance.BackColor = Color.Gainsboro;

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Methods...

        // 아이템 데이터 테이블 정보 반환
        private DataTable Set_ItemDataInfo()
        {
            DataTable dtItem = new DataTable();
            
            dtItem.TableName = "Item";
            dtItem.Columns.AddRange(new DataColumn[] {
                new DataColumn { ColumnName = "Check", Caption = "선택", DataType = typeof(bool), DefaultValue = false }
                , new DataColumn { ColumnName = "Seq", Caption = "순번", DataType = typeof(Int32), DefaultValue = 0 }
                , new DataColumn { ColumnName = "EvaluationType", Caption = "평가구분", DataType = typeof(string), DefaultValue = string.Empty }
                , new DataColumn { ColumnName = "EvaluationItem", Caption = "평가항목", DataType = typeof(string), DefaultValue = string.Empty }
                , new DataColumn { ColumnName = "Score", Caption = "배점", DataType= typeof(decimal), DefaultValue = 0.0m }
            });

            return dtItem;
        }

        // Control Clear Method
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboCustomer.SelectedIndex = 0;
                this.uComboProdType.SelectedIndex = 0;
                //this.uComboEvaluationPeriod.SelectedIndex = 0;
                this.uComboEvaluationPeriod.Value = "MON";
                this.uTextTotal.Text = "0";

                this.uGridSRRItemList.SetDataBinding(Set_ItemDataInfo(), string.Empty);

                this.uComboPlant.ReadOnly = false;
                this.uComboCustomer.ReadOnly = false;
                this.uComboProdType.ReadOnly = false;
                this.uComboEvaluationPeriod.ReadOnly = false;

                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uComboCustomer.Appearance.BackColor = Color.PowderBlue;
                this.uComboProdType.Appearance.BackColor = Color.White;
                this.uComboEvaluationPeriod.Appearance.BackColor = Color.PowderBlue;

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Item List 순번 설정
        private bool SetitemSeqAndCheckValue()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                DialogResult result = new DialogResult();

                bool bolCheckValue = true;

                foreach (Infragistics.Win.UltraWinGrid.UltraGridRow uRow in this.uGridSRRItemList.Rows)
                {
                    // Score Data Check
                    if (!(Convert.ToDecimal(uRow.Cells["Score"].Value) >= 0m && Convert.ToDecimal(uRow.Cells["Score"].Value) < 100m))
                    {
                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000879", "M001535", Infragistics.Win.HAlign.Right);

                        uRow.Cells["Score"].Activate();
                        this.uGridSRRItemList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                        bolCheckValue = false;
                        break;
                    }

                    // Set Seq Value
                    uRow.Cells["Seq"].Value = uRow.RowSelectorNumber;
                }

                m_resSys.Close();
                return bolCheckValue;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        // Datatable 압축해제
        private DataTable DecompressDataTable(string base64String)
        {
            if (String.IsNullOrEmpty(base64String))
                throw new ArgumentNullException("base64String");

            byte[] deflatedData = Convert.FromBase64String(base64String);

            using (System.IO.MemoryStream deflatedStream = new System.IO.MemoryStream(deflatedData, false))
            {
                using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(deflatedStream, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (System.IO.MemoryStream uncompressedStream = new System.IO.MemoryStream())
                    {
                        int read = 0;
                        byte[] readBuffer = new byte[1024];

                        while ((read = deflateStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
                            uncompressedStream.Write(readBuffer, 0, read);

                        uncompressedStream.Seek(0L, System.IO.SeekOrigin.Begin);
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return binaryFormatter.Deserialize(uncompressedStream) as DataTable;
                    }
                }
            }
        }

        // DataSet 압축해제
        private DataSet DecompressDataSet(string base64String)
        {
            if (String.IsNullOrEmpty(base64String))
                throw new ArgumentNullException("base64String");

            byte[] deflatedData = Convert.FromBase64String(base64String);

            using (System.IO.MemoryStream deflatedStream = new System.IO.MemoryStream(deflatedData, false))
            {
                using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(deflatedStream, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (System.IO.MemoryStream uncompressedStream = new System.IO.MemoryStream())
                    {
                        int read = 0;
                        byte[] readBuffer = new byte[1024];

                        while ((read = deflateStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
                            uncompressedStream.Write(readBuffer, 0, read);

                        uncompressedStream.Seek(0L, System.IO.SeekOrigin.Begin);
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return binaryFormatter.Deserialize(uncompressedStream) as DataSet;
                    }
                }
            }
        }

        #endregion

        #region Events...

        // 공장값 변경 이벤트
        void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");

                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    DataTable dtCusProd = clsSRR_H.mfReadQATSRRMasterHeader_CustomerProdType_Combo(strPlantCode, "MASTER", m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPUI.WinComboEditor wComboE = new QRPCOM.QRPUI.WinComboEditor();
                    wComboE.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComboCode", "ComboName", dtCusProd);
                }

                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // ContentsArea 접힘상태 변화 이벤트 
        void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            if (this.uGroupBoxContentsArea.Expanded)
            {
                Clear();
                this.uGridSRRList.Rows.FixedRows.Clear();
            }
        }

        // 행삭제 Button Click Event
        void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridSRRItemList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridSRRItemList.Rows[i].Cells["Check"].Value))
                    {
                        this.uGridSRRItemList.Rows[i].Delete(false);
                        i--;
                    }
                }

                this.uGridSRRItemList.UpdateData();
                ((DataTable)this.uGridSRRItemList.DataSource).AcceptChanges();

                // TotalScore 계산
                Decimal dblTotalScore = 0.0m;
                if (this.uGridSRRItemList.Rows.Count > 0)
                    dblTotalScore = Convert.ToDecimal(((DataTable)this.uGridSRRItemList.DataSource).Compute("Sum(Score)", string.Empty));
                if (dblTotalScore > 100)
                    this.uTextTotal.Text = "100 + " + (dblTotalScore - 100).ToString();
                else
                    this.uTextTotal.Text = dblTotalScore.ToString();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        void uGridSRRItemList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            
        }

        // Item List 업데이트 이벤트
        void uGridSRRItemList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            this.uGridSRRItemList.UpdateData();
            try
            {
                if (e.Cell.Column.Key.Equals("Score"))
                {
                    Decimal dblTotalScore = 0.0m;

                    dblTotalScore = Convert.ToDecimal(((DataTable)this.uGridSRRItemList.DataSource).Compute("Sum(Score)", string.Empty));

                    if (dblTotalScore > 100)
                        this.uTextTotal.Text = "100 + " + (dblTotalScore - 100).ToString();
                    else
                        this.uTextTotal.Text = dblTotalScore.ToString();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 조회 리스트 더블클릭 이벤트
        void uGridSRRList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            try
            {
                // Fix Cliked Row
                e.Row.Fixed = true;

                // 변수 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strCustomerCode = e.Row.Cells["CustomerCode"].Value.ToString();
                string strProdType = e.Row.Cells["ProdType"].Value.ToString();
                string strEvaluationPeriod = e.Row.Cells["EvaluationPeriod"].Value.ToString();

                // Connect BL
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");
                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strSearch = clsSRR_H.mfReadQATSRRMasterHeader_Item_Detail(strPlantCode, strCustomerCode, strProdType
                        , strEvaluationPeriod, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                        {
                            using (DataSet dsSRR_Info = new DataSet())
                            {
                                foreach (DataTable dt in dsSRR_Info.Tables)
                                    dt.BeginLoadData();
                                dsSRR_Info.ReadXml(xmlSR);
                                foreach (DataTable dt in dsSRR_Info.Tables)
                                    dt.EndLoadData();

                                // 이벤트 해제
                                this.uComboProdType.ValueChanged -= new EventHandler(uComboProdType_ValueChanged);
                                this.uComboCustomer.ValueChanged -= new EventHandler(uComboCustomer_ValueChanged);

                                // Header
                                this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                this.uComboCustomer.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString();
                                this.uComboProdType.Text = dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                this.uTextTotal.Text = dsSRR_Info.Tables["Header"].Rows[0]["TotalScore"].ToString();

                                // 이벤트 등록
                                this.uComboCustomer.ValueChanged += new EventHandler(uComboCustomer_ValueChanged);
                                this.uComboProdType.ValueChanged += new EventHandler(uComboProdType_ValueChanged);

                                // Item
                                this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                if (this.uGridSRRItemList.Rows.Count > 0)
                                {
                                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                    wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                                }

                                this.uComboPlant.ReadOnly = true;
                                this.uComboCustomer.ReadOnly = true;
                                this.uComboProdType.ReadOnly = true;
                                this.uComboEvaluationPeriod.ReadOnly = true;

                                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                                this.uComboProdType.Appearance.BackColor = Color.Gainsboro;
                                this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;

                                this.uGroupBoxContentsArea.Expanded = true;
                            }
                        }

                        #region Binary
                        /*
                        using (DataSet dsSRR_Info = DecompressDataSet(ErrRtn.mfGetReturnValue(0)))
                        {
                            // Header
                            this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                            this.uComboCustomer.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString();
                            this.uComboProdType.Text = dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                            this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                            this.uTextTotal.Text = dsSRR_Info.Tables["Header"].Rows[0]["TotalScore"].ToString();

                            // Item
                            this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                            if (this.uGridSRRItemList.Rows.Count > 0)
                            {
                                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                            }

                            this.uComboPlant.ReadOnly = true;
                            this.uComboCustomer.ReadOnly = true;
                            this.uComboProdType.ReadOnly = true;
                            this.uComboEvaluationPeriod.ReadOnly = true;

                            this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                            this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                            this.uComboProdType.Appearance.BackColor = Color.Gainsboro;
                            this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;

                            this.uGroupBoxContentsArea.Expanded = true;
                        }
                        */
                        #endregion
                    }
                    else
                    {
                        QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                        DialogResult result = new DialogResult();
                        result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNANE"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                            , msg.GetMessge_Text("M001115", m_resSys.GetString("SYS_LANG"))
                            , ErrRtn.SystemInnerException + "<br/>" +
                                ErrRtn.SystemMessage + "<br/>" +
                                ErrRtn.SystemStackTrace
                            , Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // 검색조건 고객사 구분 콤보 Refresh
        void uComboSearchCustomer_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (this.uComboSearchCustomer.SelectedIndex.Equals(-1))
                return;

            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            try
            {
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");

                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    DataTable dtCusProd = clsSRR_H.mfReadQATSRRMasterHeader_CustomerProdType_Combo(strPlantCode, "MASTER", m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchCustomer.Items.Clear();

                    QRPCOM.QRPUI.WinComboEditor wComboE = new QRPCOM.QRPUI.WinComboEditor();
                    wComboE.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 500, Infragistics.Win.HAlign.Left, "", "", "", "ComboCode", "ComboName", dtCusProd);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        // Merge 스타일 설정
        void uGridSRRList_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            //e.Layout.Bands[0].Columns["CustomerProdType"].MergedCellEvaluator = new CustomerMergedCellEvaluator();
            e.Layout.Bands[0].Columns["TotalScore"].MergedCellEvaluator = new CustomerMergedCellEvaluator();
            //e.Layout.Bands[0].Columns["EvaluationType"].MergedCellEvaluator = new CustomerMergedCellEvaluator();
        }

        private class CustomerMergedCellEvaluator : Infragistics.Win.UltraWinGrid.IMergedCellEvaluator
        {
            public CustomerMergedCellEvaluator()
            {
            }
            public bool ShouldCellsBeMerged(Infragistics.Win.UltraWinGrid.UltraGridRow row1, Infragistics.Win.UltraWinGrid.UltraGridRow row2
                , Infragistics.Win.UltraWinGrid.UltraGridColumn col)
            {
                if ((row1.GetCellValue(col.Key).GetType().ToString() != "System.DBNull" &&
                    row2.GetCellValue(col.Key).GetType().ToString() != "System.DBNull")
                    )
                {
                    string strRow1 = (string)row1.GetCellValue("CustomerProdType");
                    string strRow2 = (string)row2.GetCellValue("CustomerProdType");

                    return strRow1 == strRow2;
                }
                else
                    return false;
            }
        }

        #region 콤보박스로 변경

        /*
        void uTextProdType_AfterExitEditMode(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            if (this.uTextProdType.Text.Equals(string.Empty))
                return;
            else if (this.uComboCustomer.SelectedIndex <= 0)
                return;
            else if (this.uComboPlant.SelectedIndex <= 0)
                return;

            try
            {
                // 변수 설정
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strCustomerCode = this.uComboCustomer.Value.ToString();
                string strProdType = this.uTextProdType.Text;
                //string strEvaluationPeriod = this.uComboEvaluationPeriod.Value.ToString();
                string strEvaluationPeriod = string.Empty;

                // Connect BL
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");
                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strSearch = clsSRR_H.mfReadQATSRRMasterHeader_Item_Detail(strPlantCode, strCustomerCode, strProdType
                        , strEvaluationPeriod, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        using (DataSet dsSRR_Info = DecompressDataSet(ErrRtn.mfGetReturnValue(0)))
                        {
                            if (dsSRR_Info.Tables["Header"].Rows.Count > 0)
                            {
                                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                                DialogResult result = new DialogResult();
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001034", "M000845", Infragistics.Win.HAlign.Right);
                                // Header
                                this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                this.uComboCustomer.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString();
                                this.uTextProdType.Text = dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                this.uTextTotal.Text = dsSRR_Info.Tables["Header"].Rows[0]["TotalScore"].ToString();

                                // Item
                                this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                if (this.uGridSRRItemList.Rows.Count > 0)
                                {
                                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                    wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                                }

                                this.uComboPlant.ReadOnly = true;
                                this.uComboCustomer.ReadOnly = true;
                                this.uTextProdType.ReadOnly = true;
                                this.uComboEvaluationPeriod.ReadOnly = true;

                                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                                this.uTextProdType.Appearance.BackColor = Color.Gainsboro;
                                this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;

                                this.uGroupBoxContentsArea.Expanded = true;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }
         * */

        #endregion

        void uComboCustomer_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            if (this.uComboProdType.SelectedIndex < 0)
                return;
            else if (this.uComboCustomer.SelectedIndex <= 0)
                return;
            else if (this.uComboPlant.SelectedIndex <= 0)
                return;
            else if (!this.uComboCustomer.Enabled)
                return;

            try
            {
                // 변수 설정
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strCustomerCode = this.uComboCustomer.Value.ToString();
                //string strProdType = this.uComboProdType.Text;
                string strProdType = this.uComboProdType.SelectedItem.DisplayText;
                //string strEvaluationPeriod = this.uComboEvaluationPeriod.Value.ToString();
                string strEvaluationPeriod = string.Empty;

                // Connect BL
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");
                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strSearch = clsSRR_H.mfReadQATSRRMasterHeader_Item_Detail(strPlantCode, strCustomerCode, strProdType
                        , strEvaluationPeriod, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                    if (ErrRtn.ErrNum.Equals(0))
                    {

                        #region XML

                        using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                        {
                            using (DataSet dsSRR_Info = new DataSet())
                            {
                                foreach (DataTable dt in dsSRR_Info.Tables)
                                    dt.BeginLoadData();
                                dsSRR_Info.ReadXml(xmlSR);
                                foreach (DataTable dt in dsSRR_Info.Tables)
                                    dt.EndLoadData();

                                if (dsSRR_Info.Tables["Header"].Rows.Count > 0)
                                {
                                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                                    DialogResult result = new DialogResult();
                                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001034", "M000845", Infragistics.Win.HAlign.Right);
                                    // Header
                                    this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                    this.uComboCustomer.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString();
                                    this.uComboProdType.Text = dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                    this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                    this.uTextTotal.Text = dsSRR_Info.Tables["Header"].Rows[0]["TotalScore"].ToString();

                                    // Item
                                    this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                    if (this.uGridSRRItemList.Rows.Count > 0)
                                    {
                                        QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                        wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                                    }

                                    this.uComboPlant.ReadOnly = true;
                                    this.uComboCustomer.ReadOnly = true;
                                    this.uComboProdType.ReadOnly = true;
                                    this.uComboEvaluationPeriod.ReadOnly = true;

                                    this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboProdType.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;

                                    this.uGroupBoxContentsArea.Expanded = true;
                                }
                            }
                        }

                        #endregion

                        #region Binary
                        /*
                        using (DataSet dsSRR_Info = DecompressDataSet(ErrRtn.mfGetReturnValue(0)))
                        {
                            if (dsSRR_Info.Tables["Header"].Rows.Count > 0)
                            {
                                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                                DialogResult result = new DialogResult();
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001034", "M000845", Infragistics.Win.HAlign.Right);
                                // Header
                                this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                this.uComboCustomer.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString();
                                this.uComboProdType.Text = dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                this.uTextTotal.Text = dsSRR_Info.Tables["Header"].Rows[0]["TotalScore"].ToString();

                                // Item
                                this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                if (this.uGridSRRItemList.Rows.Count > 0)
                                {
                                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                    wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                                }

                                this.uComboPlant.ReadOnly = true;
                                this.uComboCustomer.ReadOnly = true;
                                this.uComboProdType.ReadOnly = true;
                                this.uComboEvaluationPeriod.ReadOnly = true;

                                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                                this.uComboProdType.Appearance.BackColor = Color.Gainsboro;
                                this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;

                                this.uGroupBoxContentsArea.Expanded = true;
                            }
                        }
                        */
                        #endregion
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        void uComboProdType_ValueChanged(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

            if (this.uComboProdType.SelectedIndex < 0)
                return;
            else if (this.uComboCustomer.SelectedIndex <= 0)
                return;
            else if (this.uComboPlant.SelectedIndex <= 0)
                return;
            else if (!this.uComboProdType.Enabled)
                return;

            try
            {
                // 변수 설정
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strCustomerCode = this.uComboCustomer.Value.ToString();
                //string strProdType = this.uComboProdType.Text;
                string strProdType = this.uComboProdType.SelectedItem.DisplayText;
                //string strEvaluationPeriod = this.uComboEvaluationPeriod.Value.ToString();
                string strEvaluationPeriod = string.Empty;

                // Connect BL
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.SRRMasterHeader), "SRRMasterHeader");
                using (QRPQAT.BL.QATCLM.SRRMasterHeader clsSRR_H = new QRPQAT.BL.QATCLM.SRRMasterHeader())
                {
                    brwChannel.mfCredentials(clsSRR_H);

                    string strSearch = clsSRR_H.mfReadQATSRRMasterHeader_Item_Detail(strPlantCode, strCustomerCode, strProdType
                        , strEvaluationPeriod, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strSearch);

                    if (ErrRtn.ErrNum.Equals(0))
                    {

                        #region XML

                        using (System.IO.StringReader xmlSR = new System.IO.StringReader(ErrRtn.mfGetReturnValue(0)))
                        {
                            using (DataSet dsSRR_Info = new DataSet())
                            {
                                foreach (DataTable dt in dsSRR_Info.Tables)
                                    dt.BeginLoadData();
                                dsSRR_Info.ReadXml(xmlSR);
                                foreach (DataTable dt in dsSRR_Info.Tables)
                                    dt.EndLoadData();

                                if (dsSRR_Info.Tables["Header"].Rows.Count > 0)
                                {
                                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                                    DialogResult result = new DialogResult();
                                    result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001034", "M000845", Infragistics.Win.HAlign.Right);
                                    // Header
                                    this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                    this.uComboCustomer.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString();
                                    this.uComboProdType.Text = dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                    this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                    this.uTextTotal.Text = dsSRR_Info.Tables["Header"].Rows[0]["TotalScore"].ToString();

                                    // Item
                                    this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                    if (this.uGridSRRItemList.Rows.Count > 0)
                                    {
                                        QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                        wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                                    }

                                    this.uComboPlant.ReadOnly = true;
                                    this.uComboCustomer.ReadOnly = true;
                                    this.uComboProdType.ReadOnly = true;
                                    this.uComboEvaluationPeriod.ReadOnly = true;

                                    this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboProdType.Appearance.BackColor = Color.Gainsboro;
                                    this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;

                                    this.uGroupBoxContentsArea.Expanded = true;
                                }
                            }
                        }

                        #endregion

                        #region Binary
                        /*
                        using (DataSet dsSRR_Info = DecompressDataSet(ErrRtn.mfGetReturnValue(0)))
                        {
                            if (dsSRR_Info.Tables["Header"].Rows.Count > 0)
                            {
                                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                                DialogResult result = new DialogResult();
                                result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001034", "M000845", Infragistics.Win.HAlign.Right);
                                // Header
                                this.uComboPlant.Value = dsSRR_Info.Tables["Header"].Rows[0]["PlantCode"].ToString();
                                this.uComboCustomer.Value = dsSRR_Info.Tables["Header"].Rows[0]["CustomerCode"].ToString();
                                this.uComboProdType.Text = dsSRR_Info.Tables["Header"].Rows[0]["ProdType"].ToString();
                                this.uComboEvaluationPeriod.Value = dsSRR_Info.Tables["Header"].Rows[0]["EvaluationPeriod"].ToString();
                                this.uTextTotal.Text = dsSRR_Info.Tables["Header"].Rows[0]["TotalScore"].ToString();

                                // Item
                                this.uGridSRRItemList.SetDataBinding(dsSRR_Info.Tables["Item"].Copy(), string.Empty);

                                if (this.uGridSRRItemList.Rows.Count > 0)
                                {
                                    QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();
                                    wGrid.mfSetAutoResizeColWidth(this.uGridSRRItemList, 0);
                                }

                                this.uComboPlant.ReadOnly = true;
                                this.uComboCustomer.ReadOnly = true;
                                this.uComboProdType.ReadOnly = true;
                                this.uComboEvaluationPeriod.ReadOnly = true;

                                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                                this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                                this.uComboProdType.Appearance.BackColor = Color.Gainsboro;
                                this.uComboEvaluationPeriod.Appearance.BackColor = Color.Gainsboro;

                                this.uGroupBoxContentsArea.Expanded = true;
                            }
                        }
                        */
                        #endregion
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        #endregion
    }
}
