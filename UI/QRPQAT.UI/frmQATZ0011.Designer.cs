﻿namespace QRPQAT.UI
{
    partial class frmQATZ0011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0011));
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton3 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton9 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchReqUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchReqUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchReqUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchOccurToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchOccurFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchOccurDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridBasicAnalysis = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxAcceptAnalyzeInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboIssueType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelAnalysisComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uDateTimeAnalysisComplete = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateTimeReceipFinish = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateAnalysisComplete = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateReceipFinish = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextAnalyzeUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAnalyzeUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAnalyzeUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextResultFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAnalyzeResult = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckAnalysisCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelAnalyzeResult = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckReceipFinishFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelReceipFinishFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckReceipReturnFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelIssueType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelReceipReturnFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxRequestInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uNumReqQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uCheckAnalysisReqFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxFault = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEtcDesc2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckExterior2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckEtc2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckTDR2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckCurveTracer = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckCrossSection = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckDECAP = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckXray = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckSAT = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelComment = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelRequestAmount = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAnalysisObject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAnalysisPurpose = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxGeneral = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckExterior1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckEtc1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckTDR1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckTracer = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckCurve = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMoire = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckFTIR = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckSEM = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uComboAnalysisType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelAnalysisDivision = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRequestDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReqUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextBasicNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelManageNo = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchOccurToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchOccurFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridBasicAnalysis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAcceptAnalyzeInfo)).BeginInit();
            this.uGroupBoxAcceptAnalyzeInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboIssueType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeAnalysisComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeReceipFinish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAnalysisComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceipFinish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalyzeUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalyzeUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalyzeResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAnalysisCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceipFinishFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceipReturnFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRequestInfo)).BeginInit();
            this.uGroupBoxRequestInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumReqQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAnalysisReqFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFault)).BeginInit();
            this.uGroupBoxFault.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckExterior2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEtc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTDR2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCurveTracer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCrossSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDECAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckXray)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalysisObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGeneral)).BeginInit();
            this.uGroupBoxGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckExterior1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEtc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTDR1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTracer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCurve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMoire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFTIR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSEM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAnalysisType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRequestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBasicNo)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchReqUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchReqUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchReqUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProduct);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchOccurToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchOccurFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchOccurDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(96, 32);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(185, 21);
            this.uComboSearchPackage.TabIndex = 28;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(7, 32);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPackage.TabIndex = 27;
            this.uLabelSearchPackage.Text = "ultraLabel1";
            // 
            // uTextSearchReqUserName
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchReqUserName.Appearance = appearance35;
            this.uTextSearchReqUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchReqUserName.Location = new System.Drawing.Point(465, 8);
            this.uTextSearchReqUserName.Name = "uTextSearchReqUserName";
            this.uTextSearchReqUserName.ReadOnly = true;
            this.uTextSearchReqUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchReqUserName.TabIndex = 26;
            // 
            // uTextSearchReqUserID
            // 
            appearance36.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance36;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchReqUserID.ButtonsRight.Add(editorButton1);
            this.uTextSearchReqUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchReqUserID.Location = new System.Drawing.Point(377, 8);
            this.uTextSearchReqUserID.MaxLength = 20;
            this.uTextSearchReqUserID.Name = "uTextSearchReqUserID";
            this.uTextSearchReqUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchReqUserID.TabIndex = 25;
            this.uTextSearchReqUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchReqUserID_KeyDown);
            this.uTextSearchReqUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchReqUserID_EditorButtonClick);
            // 
            // uLabelSearchReqUser
            // 
            this.uLabelSearchReqUser.Location = new System.Drawing.Point(288, 8);
            this.uLabelSearchReqUser.Name = "uLabelSearchReqUser";
            this.uLabelSearchReqUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchReqUser.TabIndex = 24;
            this.uLabelSearchReqUser.Text = "ultraLabel1";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(96, 8);
            this.uComboSearchCustomer.MaxLength = 50;
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(129, 21);
            this.uComboSearchCustomer.TabIndex = 23;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(7, 8);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCustomer.TabIndex = 22;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uTextSearchProductName
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Appearance = appearance33;
            this.uTextSearchProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Location = new System.Drawing.Point(891, 16);
            this.uTextSearchProductName.Name = "uTextSearchProductName";
            this.uTextSearchProductName.ReadOnly = true;
            this.uTextSearchProductName.Size = new System.Drawing.Size(18, 21);
            this.uTextSearchProductName.TabIndex = 21;
            this.uTextSearchProductName.Visible = false;
            // 
            // uTextSearchProductCode
            // 
            appearance34.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance34;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchProductCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchProductCode.Location = new System.Drawing.Point(871, 16);
            this.uTextSearchProductCode.MaxLength = 20;
            this.uTextSearchProductCode.Name = "uTextSearchProductCode";
            this.uTextSearchProductCode.Size = new System.Drawing.Size(18, 21);
            this.uTextSearchProductCode.TabIndex = 20;
            this.uTextSearchProductCode.Visible = false;
            this.uTextSearchProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchProductCode_EditorButtonClick);
            // 
            // uLabelSearchProduct
            // 
            this.uLabelSearchProduct.Location = new System.Drawing.Point(850, 16);
            this.uLabelSearchProduct.Name = "uLabelSearchProduct";
            this.uLabelSearchProduct.Size = new System.Drawing.Size(17, 20);
            this.uLabelSearchProduct.TabIndex = 19;
            this.uLabelSearchProduct.Text = "ultraLabel1";
            this.uLabelSearchProduct.Visible = false;
            // 
            // uDateSearchOccurToDate
            // 
            this.uDateSearchOccurToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchOccurToDate.Location = new System.Drawing.Point(480, 32);
            this.uDateSearchOccurToDate.Name = "uDateSearchOccurToDate";
            this.uDateSearchOccurToDate.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchOccurToDate.TabIndex = 18;
            // 
            // ultraLabel1
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance4;
            this.ultraLabel1.Location = new System.Drawing.Point(466, 32);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(10, 20);
            this.ultraLabel1.TabIndex = 17;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchOccurFromDate
            // 
            this.uDateSearchOccurFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchOccurFromDate.Location = new System.Drawing.Point(377, 32);
            this.uDateSearchOccurFromDate.Name = "uDateSearchOccurFromDate";
            this.uDateSearchOccurFromDate.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchOccurFromDate.TabIndex = 16;
            // 
            // uLabelSearchOccurDate
            // 
            this.uLabelSearchOccurDate.Location = new System.Drawing.Point(288, 32);
            this.uLabelSearchOccurDate.Name = "uLabelSearchOccurDate";
            this.uLabelSearchOccurDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchOccurDate.TabIndex = 15;
            this.uLabelSearchOccurDate.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(891, 4);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(18, 21);
            this.uComboSearchPlant.TabIndex = 11;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(871, 4);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(17, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridBasicAnalysis
            // 
            this.uGridBasicAnalysis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridBasicAnalysis.DisplayLayout.Appearance = appearance5;
            this.uGridBasicAnalysis.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridBasicAnalysis.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridBasicAnalysis.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridBasicAnalysis.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridBasicAnalysis.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridBasicAnalysis.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridBasicAnalysis.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridBasicAnalysis.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridBasicAnalysis.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridBasicAnalysis.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridBasicAnalysis.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridBasicAnalysis.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridBasicAnalysis.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridBasicAnalysis.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridBasicAnalysis.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridBasicAnalysis.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridBasicAnalysis.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridBasicAnalysis.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridBasicAnalysis.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridBasicAnalysis.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridBasicAnalysis.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridBasicAnalysis.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridBasicAnalysis.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridBasicAnalysis.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridBasicAnalysis.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridBasicAnalysis.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridBasicAnalysis.Location = new System.Drawing.Point(0, 100);
            this.uGridBasicAnalysis.Name = "uGridBasicAnalysis";
            this.uGridBasicAnalysis.Size = new System.Drawing.Size(917, 720);
            this.uGridBasicAnalysis.TabIndex = 2;
            this.uGridBasicAnalysis.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridBasicAnalysis_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(917, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 175);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(917, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxAcceptAnalyzeInfo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxRequestInfo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(911, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxAcceptAnalyzeInfo
            // 
            this.uGroupBoxAcceptAnalyzeInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uComboIssueType);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uLabelAnalysisComplete);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uDateTimeAnalysisComplete);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uDateTimeReceipFinish);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uDateAnalysisComplete);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uDateReceipFinish);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uTextAnalyzeUserName);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uTextAnalyzeUserID);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uLabelAnalyzeUser);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uTextResultFileName);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uLabelAttachFile2);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uTextAnalyzeResult);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uCheckAnalysisCompleteFlag);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uLabelAnalyzeResult);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uCheckReceipFinishFlag);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uLabelReceipFinishFlag);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uCheckReceipReturnFlag);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uLabelIssueType);
            this.uGroupBoxAcceptAnalyzeInfo.Controls.Add(this.uLabelReceipReturnFlag);
            this.uGroupBoxAcceptAnalyzeInfo.Location = new System.Drawing.Point(10, 368);
            this.uGroupBoxAcceptAnalyzeInfo.Name = "uGroupBoxAcceptAnalyzeInfo";
            this.uGroupBoxAcceptAnalyzeInfo.Size = new System.Drawing.Size(894, 220);
            this.uGroupBoxAcceptAnalyzeInfo.TabIndex = 1;
            this.uGroupBoxAcceptAnalyzeInfo.Text = "ultraGroupBox1";
            // 
            // uComboIssueType
            // 
            this.uComboIssueType.Location = new System.Drawing.Point(387, 52);
            this.uComboIssueType.Name = "uComboIssueType";
            this.uComboIssueType.Size = new System.Drawing.Size(123, 21);
            this.uComboIssueType.TabIndex = 73;
            // 
            // uLabelAnalysisComplete
            // 
            this.uLabelAnalysisComplete.Location = new System.Drawing.Point(10, 188);
            this.uLabelAnalysisComplete.Name = "uLabelAnalysisComplete";
            this.uLabelAnalysisComplete.Size = new System.Drawing.Size(99, 20);
            this.uLabelAnalysisComplete.TabIndex = 72;
            this.uLabelAnalysisComplete.Text = "ultraLabel1";
            // 
            // uDateTimeAnalysisComplete
            // 
            appearance37.BackColor = System.Drawing.Color.White;
            this.uDateTimeAnalysisComplete.Appearance = appearance37;
            this.uDateTimeAnalysisComplete.BackColor = System.Drawing.Color.White;
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateTimeAnalysisComplete.ButtonsRight.Add(spinEditorButton1);
            this.uDateTimeAnalysisComplete.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateTimeAnalysisComplete.Location = new System.Drawing.Point(223, 188);
            this.uDateTimeAnalysisComplete.MaskInput = "hh:mm:ss";
            this.uDateTimeAnalysisComplete.Name = "uDateTimeAnalysisComplete";
            this.uDateTimeAnalysisComplete.ReadOnly = true;
            this.uDateTimeAnalysisComplete.Size = new System.Drawing.Size(69, 21);
            this.uDateTimeAnalysisComplete.TabIndex = 71;
            // 
            // uDateTimeReceipFinish
            // 
            appearance31.BackColor = System.Drawing.Color.White;
            this.uDateTimeReceipFinish.Appearance = appearance31;
            this.uDateTimeReceipFinish.BackColor = System.Drawing.Color.White;
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateTimeReceipFinish.ButtonsRight.Add(spinEditorButton2);
            this.uDateTimeReceipFinish.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateTimeReceipFinish.Location = new System.Drawing.Point(223, 52);
            this.uDateTimeReceipFinish.MaskInput = "hh:mm:ss";
            this.uDateTimeReceipFinish.Name = "uDateTimeReceipFinish";
            this.uDateTimeReceipFinish.ReadOnly = true;
            this.uDateTimeReceipFinish.Size = new System.Drawing.Size(69, 21);
            this.uDateTimeReceipFinish.TabIndex = 71;
            // 
            // uDateAnalysisComplete
            // 
            appearance38.BackColor = System.Drawing.Color.White;
            this.uDateAnalysisComplete.Appearance = appearance38;
            this.uDateAnalysisComplete.BackColor = System.Drawing.Color.White;
            this.uDateAnalysisComplete.Location = new System.Drawing.Point(134, 188);
            this.uDateAnalysisComplete.Name = "uDateAnalysisComplete";
            this.uDateAnalysisComplete.ReadOnly = true;
            this.uDateAnalysisComplete.Size = new System.Drawing.Size(86, 21);
            this.uDateAnalysisComplete.TabIndex = 71;
            // 
            // uDateReceipFinish
            // 
            appearance3.BackColor = System.Drawing.Color.White;
            this.uDateReceipFinish.Appearance = appearance3;
            this.uDateReceipFinish.BackColor = System.Drawing.Color.White;
            this.uDateReceipFinish.Location = new System.Drawing.Point(134, 52);
            this.uDateReceipFinish.Name = "uDateReceipFinish";
            this.uDateReceipFinish.ReadOnly = true;
            this.uDateReceipFinish.Size = new System.Drawing.Size(86, 21);
            this.uDateReceipFinish.TabIndex = 71;
            // 
            // uTextAnalyzeUserName
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAnalyzeUserName.Appearance = appearance20;
            this.uTextAnalyzeUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAnalyzeUserName.Location = new System.Drawing.Point(201, 140);
            this.uTextAnalyzeUserName.Name = "uTextAnalyzeUserName";
            this.uTextAnalyzeUserName.ReadOnly = true;
            this.uTextAnalyzeUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextAnalyzeUserName.TabIndex = 70;
            // 
            // uTextAnalyzeUserID
            // 
            appearance21.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance21.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance21;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAnalyzeUserID.ButtonsRight.Add(editorButton3);
            this.uTextAnalyzeUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAnalyzeUserID.Location = new System.Drawing.Point(113, 140);
            this.uTextAnalyzeUserID.MaxLength = 20;
            this.uTextAnalyzeUserID.Name = "uTextAnalyzeUserID";
            this.uTextAnalyzeUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextAnalyzeUserID.TabIndex = 69;
            this.uTextAnalyzeUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextAnalyzeUserID_KeyDown);
            this.uTextAnalyzeUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAnalyzeUserID_EditorButtonClick);
            // 
            // uLabelAnalyzeUser
            // 
            this.uLabelAnalyzeUser.Location = new System.Drawing.Point(10, 140);
            this.uLabelAnalyzeUser.Name = "uLabelAnalyzeUser";
            this.uLabelAnalyzeUser.Size = new System.Drawing.Size(99, 20);
            this.uLabelAnalyzeUser.TabIndex = 68;
            this.uLabelAnalyzeUser.Text = "ultraLabel1";
            // 
            // uTextResultFileName
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextResultFileName.Appearance = appearance24;
            this.uTextResultFileName.BackColor = System.Drawing.Color.Gainsboro;
            appearance28.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance28;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Key = "UP";
            appearance22.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance22;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "DOWN";
            this.uTextResultFileName.ButtonsRight.Add(editorButton4);
            this.uTextResultFileName.ButtonsRight.Add(editorButton5);
            this.uTextResultFileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextResultFileName.Location = new System.Drawing.Point(113, 164);
            this.uTextResultFileName.MaxLength = 1000;
            this.uTextResultFileName.Name = "uTextResultFileName";
            this.uTextResultFileName.ReadOnly = true;
            this.uTextResultFileName.Size = new System.Drawing.Size(444, 21);
            this.uTextResultFileName.TabIndex = 67;
            this.uTextResultFileName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextResultFileName_KeyDown);
            this.uTextResultFileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextResultFileName_EditorButtonClick);
            // 
            // uLabelAttachFile2
            // 
            this.uLabelAttachFile2.Location = new System.Drawing.Point(10, 164);
            this.uLabelAttachFile2.Name = "uLabelAttachFile2";
            this.uLabelAttachFile2.Size = new System.Drawing.Size(99, 20);
            this.uLabelAttachFile2.TabIndex = 66;
            this.uLabelAttachFile2.Text = "ultraLabel1";
            // 
            // uTextAnalyzeResult
            // 
            this.uTextAnalyzeResult.Location = new System.Drawing.Point(113, 76);
            this.uTextAnalyzeResult.MaxLength = 200;
            this.uTextAnalyzeResult.Multiline = true;
            this.uTextAnalyzeResult.Name = "uTextAnalyzeResult";
            this.uTextAnalyzeResult.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.uTextAnalyzeResult.Size = new System.Drawing.Size(444, 60);
            this.uTextAnalyzeResult.TabIndex = 65;
            // 
            // uCheckAnalysisCompleteFlag
            // 
            this.uCheckAnalysisCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckAnalysisCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckAnalysisCompleteFlag.Location = new System.Drawing.Point(113, 188);
            this.uCheckAnalysisCompleteFlag.Name = "uCheckAnalysisCompleteFlag";
            this.uCheckAnalysisCompleteFlag.Size = new System.Drawing.Size(17, 20);
            this.uCheckAnalysisCompleteFlag.TabIndex = 63;
            this.uCheckAnalysisCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelAnalyzeResult
            // 
            this.uLabelAnalyzeResult.Location = new System.Drawing.Point(10, 76);
            this.uLabelAnalyzeResult.Name = "uLabelAnalyzeResult";
            this.uLabelAnalyzeResult.Size = new System.Drawing.Size(99, 20);
            this.uLabelAnalyzeResult.TabIndex = 64;
            this.uLabelAnalyzeResult.Text = "ultraLabel1";
            // 
            // uCheckReceipFinishFlag
            // 
            this.uCheckReceipFinishFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckReceipFinishFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckReceipFinishFlag.Location = new System.Drawing.Point(113, 52);
            this.uCheckReceipFinishFlag.Name = "uCheckReceipFinishFlag";
            this.uCheckReceipFinishFlag.Size = new System.Drawing.Size(17, 20);
            this.uCheckReceipFinishFlag.TabIndex = 63;
            this.uCheckReceipFinishFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelReceipFinishFlag
            // 
            this.uLabelReceipFinishFlag.Location = new System.Drawing.Point(10, 52);
            this.uLabelReceipFinishFlag.Name = "uLabelReceipFinishFlag";
            this.uLabelReceipFinishFlag.Size = new System.Drawing.Size(99, 20);
            this.uLabelReceipFinishFlag.TabIndex = 62;
            this.uLabelReceipFinishFlag.Text = "ultraLabel1";
            // 
            // uCheckReceipReturnFlag
            // 
            this.uCheckReceipReturnFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckReceipReturnFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckReceipReturnFlag.Location = new System.Drawing.Point(113, 28);
            this.uCheckReceipReturnFlag.Name = "uCheckReceipReturnFlag";
            this.uCheckReceipReturnFlag.Size = new System.Drawing.Size(17, 20);
            this.uCheckReceipReturnFlag.TabIndex = 61;
            this.uCheckReceipReturnFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelIssueType
            // 
            this.uLabelIssueType.Location = new System.Drawing.Point(298, 52);
            this.uLabelIssueType.Name = "uLabelIssueType";
            this.uLabelIssueType.Size = new System.Drawing.Size(86, 20);
            this.uLabelIssueType.TabIndex = 60;
            this.uLabelIssueType.Text = "ultraLabel1";
            // 
            // uLabelReceipReturnFlag
            // 
            this.uLabelReceipReturnFlag.Location = new System.Drawing.Point(10, 28);
            this.uLabelReceipReturnFlag.Name = "uLabelReceipReturnFlag";
            this.uLabelReceipReturnFlag.Size = new System.Drawing.Size(99, 20);
            this.uLabelReceipReturnFlag.TabIndex = 60;
            this.uLabelReceipReturnFlag.Text = "ultraLabel1";
            // 
            // uGroupBoxRequestInfo
            // 
            this.uGroupBoxRequestInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelPackage);
            this.uGroupBoxRequestInfo.Controls.Add(this.uNumReqQty);
            this.uGroupBoxRequestInfo.Controls.Add(this.uCheckAnalysisReqFlag);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelComplete);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextReqFileName);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelAttachFile1);
            this.uGroupBoxRequestInfo.Controls.Add(this.uGroupBoxFault);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextComment);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelComment);
            this.uGroupBoxRequestInfo.Controls.Add(this.uComboPlant);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelRequestAmount);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextPackage);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextLotNo);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelLotNo);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextCustomerName);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextCustomerCode);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelCustomer);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextAnalysisObject);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelAnalysisPurpose);
            this.uGroupBoxRequestInfo.Controls.Add(this.uGroupBoxGeneral);
            this.uGroupBoxRequestInfo.Controls.Add(this.uComboAnalysisType);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelAnalysisDivision);
            this.uGroupBoxRequestInfo.Controls.Add(this.uDateRequestDate);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelRequestDate);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextReqUserName);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextReqUserID);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelReqUser);
            this.uGroupBoxRequestInfo.Controls.Add(this.uTextBasicNo);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxRequestInfo.Controls.Add(this.uLabelManageNo);
            this.uGroupBoxRequestInfo.Location = new System.Drawing.Point(10, 12);
            this.uGroupBoxRequestInfo.Name = "uGroupBoxRequestInfo";
            this.uGroupBoxRequestInfo.Size = new System.Drawing.Size(895, 352);
            this.uGroupBoxRequestInfo.TabIndex = 0;
            this.uGroupBoxRequestInfo.Text = "ultraGroupBox1";
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(10, 228);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(99, 20);
            this.uLabelPackage.TabIndex = 63;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uNumReqQty
            // 
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Text = "0";
            this.uNumReqQty.ButtonsLeft.Add(editorButton6);
            spinEditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumReqQty.ButtonsRight.Add(spinEditorButton3);
            this.uNumReqQty.Location = new System.Drawing.Point(384, 252);
            this.uNumReqQty.MaxValue = 99999;
            this.uNumReqQty.MinValue = -100;
            this.uNumReqQty.Name = "uNumReqQty";
            this.uNumReqQty.PromptChar = ' ';
            this.uNumReqQty.Size = new System.Drawing.Size(86, 21);
            this.uNumReqQty.TabIndex = 61;
            this.uNumReqQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumReqQty_EditorSpinButtonClick);
            this.uNumReqQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumReqQty_EditorButtonClick);
            // 
            // uCheckAnalysisReqFlag
            // 
            this.uCheckAnalysisReqFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckAnalysisReqFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckAnalysisReqFlag.Location = new System.Drawing.Point(113, 324);
            this.uCheckAnalysisReqFlag.Name = "uCheckAnalysisReqFlag";
            this.uCheckAnalysisReqFlag.Size = new System.Drawing.Size(17, 20);
            this.uCheckAnalysisReqFlag.TabIndex = 59;
            this.uCheckAnalysisReqFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelComplete
            // 
            this.uLabelComplete.Location = new System.Drawing.Point(10, 324);
            this.uLabelComplete.Name = "uLabelComplete";
            this.uLabelComplete.Size = new System.Drawing.Size(99, 20);
            this.uLabelComplete.TabIndex = 58;
            this.uLabelComplete.Text = "ultraLabel1";
            // 
            // uTextReqFileName
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqFileName.Appearance = appearance32;
            this.uTextReqFileName.BackColor = System.Drawing.Color.Gainsboro;
            appearance29.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance29.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance29;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton7.Key = "UP";
            appearance19.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton8.Appearance = appearance19;
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton8.Key = "DOWN";
            this.uTextReqFileName.ButtonsRight.Add(editorButton7);
            this.uTextReqFileName.ButtonsRight.Add(editorButton8);
            this.uTextReqFileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReqFileName.Location = new System.Drawing.Point(113, 300);
            this.uTextReqFileName.MaxLength = 1000;
            this.uTextReqFileName.Name = "uTextReqFileName";
            this.uTextReqFileName.ReadOnly = true;
            this.uTextReqFileName.Size = new System.Drawing.Size(444, 21);
            this.uTextReqFileName.TabIndex = 57;
            this.uTextReqFileName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReqFileName_KeyDown);
            this.uTextReqFileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReqFileName_EditorButtonClick);
            // 
            // uLabelAttachFile1
            // 
            this.uLabelAttachFile1.Location = new System.Drawing.Point(10, 300);
            this.uLabelAttachFile1.Name = "uLabelAttachFile1";
            this.uLabelAttachFile1.Size = new System.Drawing.Size(99, 20);
            this.uLabelAttachFile1.TabIndex = 56;
            this.uLabelAttachFile1.Text = "ultraLabel1";
            // 
            // uGroupBoxFault
            // 
            this.uGroupBoxFault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxFault.Controls.Add(this.uTextEtcDesc2);
            this.uGroupBoxFault.Controls.Add(this.uCheckExterior2);
            this.uGroupBoxFault.Controls.Add(this.uCheckEtc2);
            this.uGroupBoxFault.Controls.Add(this.uCheckTDR2);
            this.uGroupBoxFault.Controls.Add(this.uCheckCurveTracer);
            this.uGroupBoxFault.Controls.Add(this.uCheckCrossSection);
            this.uGroupBoxFault.Controls.Add(this.uCheckDECAP);
            this.uGroupBoxFault.Controls.Add(this.uCheckXray);
            this.uGroupBoxFault.Controls.Add(this.uCheckSAT);
            this.uGroupBoxFault.Location = new System.Drawing.Point(10, 104);
            this.uGroupBoxFault.Name = "uGroupBoxFault";
            this.uGroupBoxFault.Size = new System.Drawing.Size(877, 56);
            this.uGroupBoxFault.TabIndex = 41;
            this.uGroupBoxFault.Text = "ultraGroupBox1";
            // 
            // uTextEtcDesc2
            // 
            this.uTextEtcDesc2.Location = new System.Drawing.Point(603, 28);
            this.uTextEtcDesc2.MaxLength = 100;
            this.uTextEtcDesc2.Name = "uTextEtcDesc2";
            this.uTextEtcDesc2.Size = new System.Drawing.Size(129, 21);
            this.uTextEtcDesc2.TabIndex = 10;
            // 
            // uCheckExterior2
            // 
            this.uCheckExterior2.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckExterior2.Location = new System.Drawing.Point(480, 28);
            this.uCheckExterior2.Name = "uCheckExterior2";
            this.uCheckExterior2.Size = new System.Drawing.Size(58, 20);
            this.uCheckExterior2.TabIndex = 64;
            this.uCheckExterior2.Text = "외관";
            this.uCheckExterior2.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckEtc2
            // 
            this.uCheckEtc2.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckEtc2.Location = new System.Drawing.Point(542, 28);
            this.uCheckEtc2.Name = "uCheckEtc2";
            this.uCheckEtc2.Size = new System.Drawing.Size(58, 20);
            this.uCheckEtc2.TabIndex = 8;
            this.uCheckEtc2.Text = "기타";
            this.uCheckEtc2.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckTDR2
            // 
            this.uCheckTDR2.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckTDR2.Location = new System.Drawing.Point(422, 28);
            this.uCheckTDR2.Name = "uCheckTDR2";
            this.uCheckTDR2.Size = new System.Drawing.Size(58, 20);
            this.uCheckTDR2.TabIndex = 7;
            this.uCheckTDR2.Text = "TDR";
            this.uCheckTDR2.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckCurveTracer
            // 
            this.uCheckCurveTracer.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCurveTracer.Location = new System.Drawing.Point(322, 28);
            this.uCheckCurveTracer.Name = "uCheckCurveTracer";
            this.uCheckCurveTracer.Size = new System.Drawing.Size(96, 20);
            this.uCheckCurveTracer.TabIndex = 5;
            this.uCheckCurveTracer.Text = "CurveTracer";
            this.uCheckCurveTracer.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckCrossSection
            // 
            this.uCheckCrossSection.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCrossSection.Location = new System.Drawing.Point(216, 28);
            this.uCheckCrossSection.Name = "uCheckCrossSection";
            this.uCheckCrossSection.Size = new System.Drawing.Size(103, 20);
            this.uCheckCrossSection.TabIndex = 3;
            this.uCheckCrossSection.Text = "CrossSection";
            this.uCheckCrossSection.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckDECAP
            // 
            this.uCheckDECAP.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckDECAP.Location = new System.Drawing.Point(141, 28);
            this.uCheckDECAP.Name = "uCheckDECAP";
            this.uCheckDECAP.Size = new System.Drawing.Size(72, 20);
            this.uCheckDECAP.TabIndex = 2;
            this.uCheckDECAP.Text = "DECAP";
            this.uCheckDECAP.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckXray
            // 
            this.uCheckXray.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckXray.Location = new System.Drawing.Point(72, 28);
            this.uCheckXray.Name = "uCheckXray";
            this.uCheckXray.Size = new System.Drawing.Size(65, 20);
            this.uCheckXray.TabIndex = 1;
            this.uCheckXray.Text = "X-ray";
            this.uCheckXray.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckSAT
            // 
            this.uCheckSAT.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckSAT.Location = new System.Drawing.Point(10, 28);
            this.uCheckSAT.Name = "uCheckSAT";
            this.uCheckSAT.Size = new System.Drawing.Size(58, 20);
            this.uCheckSAT.TabIndex = 0;
            this.uCheckSAT.Text = "SAT";
            this.uCheckSAT.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextComment
            // 
            this.uTextComment.Location = new System.Drawing.Point(113, 276);
            this.uTextComment.MaxLength = 200;
            this.uTextComment.Name = "uTextComment";
            this.uTextComment.Size = new System.Drawing.Size(444, 21);
            this.uTextComment.TabIndex = 55;
            // 
            // uLabelComment
            // 
            this.uLabelComment.Location = new System.Drawing.Point(10, 276);
            this.uLabelComment.Name = "uLabelComment";
            this.uLabelComment.Size = new System.Drawing.Size(99, 20);
            this.uLabelComment.TabIndex = 54;
            this.uLabelComment.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(871, 20);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(18, 21);
            this.uComboPlant.TabIndex = 11;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.Visible = false;
            // 
            // uLabelRequestAmount
            // 
            this.uLabelRequestAmount.Location = new System.Drawing.Point(295, 252);
            this.uLabelRequestAmount.Name = "uLabelRequestAmount";
            this.uLabelRequestAmount.Size = new System.Drawing.Size(86, 20);
            this.uLabelRequestAmount.TabIndex = 52;
            this.uLabelRequestAmount.Text = "ultraLabel1";
            // 
            // uTextPackage
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPackage.Appearance = appearance2;
            this.uTextPackage.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPackage.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextPackage.Location = new System.Drawing.Point(113, 228);
            this.uTextPackage.MaxLength = 50;
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.Size = new System.Drawing.Size(173, 21);
            this.uTextPackage.TabIndex = 51;
            // 
            // uTextLotNo
            // 
            this.uTextLotNo.Location = new System.Drawing.Point(113, 252);
            this.uTextLotNo.MaxLength = 50;
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(173, 21);
            this.uTextLotNo.TabIndex = 51;
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(10, 252);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(99, 20);
            this.uLabelLotNo.TabIndex = 50;
            this.uLabelLotNo.Text = "ultraLabel1";
            // 
            // uTextCustomerName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance23;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(471, 228);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.Size = new System.Drawing.Size(86, 21);
            this.uTextCustomerName.TabIndex = 46;
            // 
            // uTextCustomerCode
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance30;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(384, 228);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.Size = new System.Drawing.Size(86, 21);
            this.uTextCustomerCode.TabIndex = 45;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(295, 228);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelCustomer.TabIndex = 44;
            this.uLabelCustomer.Text = "ultraLabel1";
            // 
            // uTextAnalysisObject
            // 
            this.uTextAnalysisObject.Location = new System.Drawing.Point(113, 168);
            this.uTextAnalysisObject.MaxLength = 100;
            this.uTextAnalysisObject.Multiline = true;
            this.uTextAnalysisObject.Name = "uTextAnalysisObject";
            this.uTextAnalysisObject.Scrollbars = System.Windows.Forms.ScrollBars.Horizontal;
            this.uTextAnalysisObject.Size = new System.Drawing.Size(444, 56);
            this.uTextAnalysisObject.TabIndex = 43;
            // 
            // uLabelAnalysisPurpose
            // 
            this.uLabelAnalysisPurpose.Location = new System.Drawing.Point(10, 168);
            this.uLabelAnalysisPurpose.Name = "uLabelAnalysisPurpose";
            this.uLabelAnalysisPurpose.Size = new System.Drawing.Size(99, 20);
            this.uLabelAnalysisPurpose.TabIndex = 42;
            this.uLabelAnalysisPurpose.Text = "ultraLabel1";
            // 
            // uGroupBoxGeneral
            // 
            this.uGroupBoxGeneral.Controls.Add(this.uCheckExterior1);
            this.uGroupBoxGeneral.Controls.Add(this.uCheckEtc1);
            this.uGroupBoxGeneral.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxGeneral.Controls.Add(this.uCheckTDR1);
            this.uGroupBoxGeneral.Controls.Add(this.uCheckTracer);
            this.uGroupBoxGeneral.Controls.Add(this.uCheckCurve);
            this.uGroupBoxGeneral.Controls.Add(this.uCheckMoire);
            this.uGroupBoxGeneral.Controls.Add(this.uCheckFTIR);
            this.uGroupBoxGeneral.Controls.Add(this.uCheckSEM);
            this.uGroupBoxGeneral.Location = new System.Drawing.Point(10, 104);
            this.uGroupBoxGeneral.Name = "uGroupBoxGeneral";
            this.uGroupBoxGeneral.Size = new System.Drawing.Size(857, 56);
            this.uGroupBoxGeneral.TabIndex = 40;
            this.uGroupBoxGeneral.Text = "ultraGroupBox1";
            // 
            // uCheckExterior1
            // 
            this.uCheckExterior1.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckExterior1.Location = new System.Drawing.Point(411, 28);
            this.uCheckExterior1.Name = "uCheckExterior1";
            this.uCheckExterior1.Size = new System.Drawing.Size(58, 20);
            this.uCheckExterior1.TabIndex = 65;
            this.uCheckExterior1.Text = "외관";
            this.uCheckExterior1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckEtc1
            // 
            this.uCheckEtc1.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckEtc1.Location = new System.Drawing.Point(473, 28);
            this.uCheckEtc1.Name = "uCheckEtc1";
            this.uCheckEtc1.Size = new System.Drawing.Size(58, 20);
            this.uCheckEtc1.TabIndex = 6;
            this.uCheckEtc1.Text = "기타";
            this.uCheckEtc1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(535, 28);
            this.uTextEtcDesc.MaxLength = 100;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(129, 21);
            this.uTextEtcDesc.TabIndex = 9;
            // 
            // uCheckTDR1
            // 
            this.uCheckTDR1.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckTDR1.Location = new System.Drawing.Point(350, 28);
            this.uCheckTDR1.Name = "uCheckTDR1";
            this.uCheckTDR1.Size = new System.Drawing.Size(58, 20);
            this.uCheckTDR1.TabIndex = 5;
            this.uCheckTDR1.Text = "TDR";
            this.uCheckTDR1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckTracer
            // 
            this.uCheckTracer.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckTracer.Location = new System.Drawing.Point(278, 28);
            this.uCheckTracer.Name = "uCheckTracer";
            this.uCheckTracer.Size = new System.Drawing.Size(69, 20);
            this.uCheckTracer.TabIndex = 4;
            this.uCheckTracer.Text = "Tracer";
            this.uCheckTracer.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckCurve
            // 
            this.uCheckCurve.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCurve.Location = new System.Drawing.Point(209, 28);
            this.uCheckCurve.Name = "uCheckCurve";
            this.uCheckCurve.Size = new System.Drawing.Size(65, 20);
            this.uCheckCurve.TabIndex = 3;
            this.uCheckCurve.Text = "Curve";
            this.uCheckCurve.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMoire
            // 
            this.uCheckMoire.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMoire.Location = new System.Drawing.Point(141, 28);
            this.uCheckMoire.Name = "uCheckMoire";
            this.uCheckMoire.Size = new System.Drawing.Size(65, 20);
            this.uCheckMoire.TabIndex = 2;
            this.uCheckMoire.Text = "Moire";
            this.uCheckMoire.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckFTIR
            // 
            this.uCheckFTIR.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFTIR.Location = new System.Drawing.Point(72, 28);
            this.uCheckFTIR.Name = "uCheckFTIR";
            this.uCheckFTIR.Size = new System.Drawing.Size(65, 20);
            this.uCheckFTIR.TabIndex = 1;
            this.uCheckFTIR.Text = "FT-IR";
            this.uCheckFTIR.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckSEM
            // 
            this.uCheckSEM.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckSEM.Location = new System.Drawing.Point(10, 28);
            this.uCheckSEM.Name = "uCheckSEM";
            this.uCheckSEM.Size = new System.Drawing.Size(58, 20);
            this.uCheckSEM.TabIndex = 0;
            this.uCheckSEM.Text = "SEM";
            this.uCheckSEM.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uComboAnalysisType
            // 
            this.uComboAnalysisType.Location = new System.Drawing.Point(113, 76);
            this.uComboAnalysisType.MaxLength = 10;
            this.uComboAnalysisType.Name = "uComboAnalysisType";
            this.uComboAnalysisType.Size = new System.Drawing.Size(86, 21);
            this.uComboAnalysisType.TabIndex = 39;
            this.uComboAnalysisType.Text = "ultraComboEditor1";
            this.uComboAnalysisType.ValueChanged += new System.EventHandler(this.uComboAnalysisDivision_ValueChanged);
            // 
            // uLabelAnalysisDivision
            // 
            this.uLabelAnalysisDivision.Location = new System.Drawing.Point(10, 76);
            this.uLabelAnalysisDivision.Name = "uLabelAnalysisDivision";
            this.uLabelAnalysisDivision.Size = new System.Drawing.Size(99, 20);
            this.uLabelAnalysisDivision.TabIndex = 38;
            this.uLabelAnalysisDivision.Text = "ultraLabel1";
            // 
            // uDateRequestDate
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRequestDate.Appearance = appearance18;
            this.uDateRequestDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRequestDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRequestDate.Location = new System.Drawing.Point(384, 52);
            this.uDateRequestDate.Name = "uDateRequestDate";
            this.uDateRequestDate.Size = new System.Drawing.Size(86, 21);
            this.uDateRequestDate.TabIndex = 37;
            // 
            // uLabelRequestDate
            // 
            this.uLabelRequestDate.Location = new System.Drawing.Point(295, 52);
            this.uLabelRequestDate.Name = "uLabelRequestDate";
            this.uLabelRequestDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelRequestDate.TabIndex = 36;
            this.uLabelRequestDate.Text = "ultraLabel1";
            // 
            // uTextReqUserName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqUserName.Appearance = appearance25;
            this.uTextReqUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqUserName.Location = new System.Drawing.Point(201, 52);
            this.uTextReqUserName.Name = "uTextReqUserName";
            this.uTextReqUserName.ReadOnly = true;
            this.uTextReqUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextReqUserName.TabIndex = 35;
            // 
            // uTextReqUserID
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReqUserID.Appearance = appearance26;
            this.uTextReqUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance27.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance27.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton9.Appearance = appearance27;
            editorButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReqUserID.ButtonsRight.Add(editorButton9);
            this.uTextReqUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReqUserID.Location = new System.Drawing.Point(113, 52);
            this.uTextReqUserID.MaxLength = 20;
            this.uTextReqUserID.Name = "uTextReqUserID";
            this.uTextReqUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextReqUserID.TabIndex = 34;
            this.uTextReqUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRequestUserID_KeyDown);
            this.uTextReqUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRequestUserID_EditorButtonClick);
            // 
            // uLabelReqUser
            // 
            this.uLabelReqUser.Location = new System.Drawing.Point(10, 52);
            this.uLabelReqUser.Name = "uLabelReqUser";
            this.uLabelReqUser.Size = new System.Drawing.Size(99, 20);
            this.uLabelReqUser.TabIndex = 33;
            this.uLabelReqUser.Text = "ultraLabel1";
            // 
            // uTextBasicNo
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBasicNo.Appearance = appearance17;
            this.uTextBasicNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBasicNo.Location = new System.Drawing.Point(113, 28);
            this.uTextBasicNo.Name = "uTextBasicNo";
            this.uTextBasicNo.Size = new System.Drawing.Size(173, 21);
            this.uTextBasicNo.TabIndex = 29;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(854, 20);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(17, 20);
            this.uLabelPlant.TabIndex = 28;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // uLabelManageNo
            // 
            this.uLabelManageNo.Location = new System.Drawing.Point(10, 28);
            this.uLabelManageNo.Name = "uLabelManageNo";
            this.uLabelManageNo.Size = new System.Drawing.Size(99, 20);
            this.uLabelManageNo.TabIndex = 28;
            this.uLabelManageNo.Text = "ultraLabel1";
            // 
            // frmQATZ0011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridBasicAnalysis);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0011";
            this.Load += new System.EventHandler(this.frmQATZ0011_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0011_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0011_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0011_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchOccurToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchOccurFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridBasicAnalysis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAcceptAnalyzeInfo)).EndInit();
            this.uGroupBoxAcceptAnalyzeInfo.ResumeLayout(false);
            this.uGroupBoxAcceptAnalyzeInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboIssueType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeAnalysisComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeReceipFinish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAnalysisComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceipFinish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalyzeUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalyzeUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalyzeResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAnalysisCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceipFinishFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReceipReturnFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRequestInfo)).EndInit();
            this.uGroupBoxRequestInfo.ResumeLayout(false);
            this.uGroupBoxRequestInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uNumReqQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAnalysisReqFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFault)).EndInit();
            this.uGroupBoxFault.ResumeLayout(false);
            this.uGroupBoxFault.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckExterior2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEtc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTDR2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCurveTracer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCrossSection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDECAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckXray)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAnalysisObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGeneral)).EndInit();
            this.uGroupBoxGeneral.ResumeLayout(false);
            this.uGroupBoxGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckExterior1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEtc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTDR1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckTracer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCurve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMoire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFTIR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSEM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAnalysisType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRequestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBasicNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProduct;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchOccurToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchOccurFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchOccurDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridBasicAnalysis;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxRequestInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBasicNo;
        private Infragistics.Win.Misc.UltraLabel uLabelManageNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelReqUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRequestDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAnalysisType;
        private Infragistics.Win.Misc.UltraLabel uLabelAnalysisDivision;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxGeneral;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSEM;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEtc1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckTDR1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckTracer;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCurve;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMoire;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFTIR;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxFault;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCurveTracer;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCrossSection;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckDECAP;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckXray;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSAT;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEtc2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckTDR2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextComment;
        private Infragistics.Win.Misc.UltraLabel uLabelComment;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAnalysisObject;
        private Infragistics.Win.Misc.UltraLabel uLabelAnalysisPurpose;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxAcceptAnalyzeInfo;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckReceipFinishFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelReceipFinishFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckReceipReturnFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelReceipReturnFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAnalysisReqFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqFileName;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAnalyzeResult;
        private Infragistics.Win.Misc.UltraLabel uLabelAnalyzeResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextResultFileName;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumReqQty;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchReqUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchReqUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchReqUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAnalyzeUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAnalyzeUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelAnalyzeUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReceipFinish;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeReceipFinish;
        private Infragistics.Win.Misc.UltraLabel uLabelAnalysisComplete;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeAnalysisComplete;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAnalysisComplete;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAnalysisCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboIssueType;
        private Infragistics.Win.Misc.UltraLabel uLabelIssueType;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckExterior2;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckExterior1;
    }
}