﻿namespace QRPQAT.UI
{
    partial class frmQATZ0014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0014));
            this.uDateSearchInspectNextDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchstrInspectNextDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchInspectNextDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchAcquireDeptCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchAcquireDept = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMeasurename = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboMTMType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMeasureToolCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMTType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxHistory = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridHistory = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabelMTMType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMTLType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMTLType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMTFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMTFileName = new Infragistics.Win.Misc.UltraLabel();
            this.uDateLastInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelLastInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.richTextEditor = new QRPUserControl.RichTextEditor();
            this.uLabelDiscardReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDiscardFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uComboDiscardFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateDiscardDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDiscardDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateInspectNextDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelInspectNextDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectPeriodUnitCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInspectPeriodUnitCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uNumInspectPeriod = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelInspectPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAcquireReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcquireReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAcquireDeptCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboAcquireDeptCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSerialNo = new Infragistics.Win.Misc.UltraLabel();
            this.uNumAcquireAmt = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelAcquireAmt = new Infragistics.Win.Misc.UltraLabel();
            this.uDateAcquireDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAcquireDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMakerNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakerCompany = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMakerNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMakerCompany = new Infragistics.Win.Misc.UltraLabel();
            this.uTextModelName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelModelName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMeasureToolCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMeasureToolCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMeasureToolNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMeasureToolName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMeasureToolNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMeasureToolNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMeasureToolName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMTType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboMTType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchMTType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckSearchInspectNextDateOver = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelSearchInspectNextDateOver = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uGridMeasureToolList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelResult = new Infragistics.Win.Misc.UltraLabel();
            this.uComboResult = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextCustomer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPurpose = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPurpose = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboClass = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelClass = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAllowance = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAllowance = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectNextDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchstrInspectNextDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchAcquireDeptCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMTMType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMTType)).BeginInit();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxHistory)).BeginInit();
            this.uGroupBoxHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMTLType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMTFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateLastInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDiscardFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectNextDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectPeriodUnitCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInspectPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcquireReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAcquireDeptCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumAcquireAmt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAcquireDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModelName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMTType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchInspectNextDateOver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMeasureToolList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAllowance)).BeginInit();
            this.SuspendLayout();
            // 
            // uDateSearchInspectNextDateTo
            // 
            this.uDateSearchInspectNextDateTo.Location = new System.Drawing.Point(516, 36);
            this.uDateSearchInspectNextDateTo.Name = "uDateSearchInspectNextDateTo";
            this.uDateSearchInspectNextDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchInspectNextDateTo.TabIndex = 64;
            // 
            // ultraLabel2
            // 
            appearance19.TextHAlignAsString = "Center";
            appearance19.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance19;
            this.ultraLabel2.Location = new System.Drawing.Point(500, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel2.TabIndex = 63;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchstrInspectNextDateFrom
            // 
            this.uDateSearchstrInspectNextDateFrom.Location = new System.Drawing.Point(396, 36);
            this.uDateSearchstrInspectNextDateFrom.Name = "uDateSearchstrInspectNextDateFrom";
            this.uDateSearchstrInspectNextDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchstrInspectNextDateFrom.TabIndex = 62;
            // 
            // uLabelSearchInspectNextDate
            // 
            this.uLabelSearchInspectNextDate.Location = new System.Drawing.Point(268, 36);
            this.uLabelSearchInspectNextDate.Name = "uLabelSearchInspectNextDate";
            this.uLabelSearchInspectNextDate.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchInspectNextDate.TabIndex = 61;
            // 
            // uComboSearchAcquireDeptCode
            // 
            this.uComboSearchAcquireDeptCode.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchAcquireDeptCode.Name = "uComboSearchAcquireDeptCode";
            this.uComboSearchAcquireDeptCode.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchAcquireDeptCode.TabIndex = 38;
            // 
            // uLabelSearchAcquireDept
            // 
            this.uLabelSearchAcquireDept.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchAcquireDept.Name = "uLabelSearchAcquireDept";
            this.uLabelSearchAcquireDept.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchAcquireDept.TabIndex = 37;
            // 
            // uTextSearchMeasureToolName
            // 
            this.uTextSearchMeasureToolName.Location = new System.Drawing.Point(752, 36);
            this.uTextSearchMeasureToolName.Name = "uTextSearchMeasureToolName";
            this.uTextSearchMeasureToolName.Size = new System.Drawing.Size(144, 21);
            this.uTextSearchMeasureToolName.TabIndex = 36;
            // 
            // uLabelSearchMeasurename
            // 
            this.uLabelSearchMeasurename.Location = new System.Drawing.Point(624, 36);
            this.uLabelSearchMeasurename.Name = "uLabelSearchMeasurename";
            this.uLabelSearchMeasurename.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchMeasurename.TabIndex = 35;
            // 
            // uTextSearchMeasureToolCode
            // 
            this.uTextSearchMeasureToolCode.Location = new System.Drawing.Point(752, 12);
            this.uTextSearchMeasureToolCode.Name = "uTextSearchMeasureToolCode";
            this.uTextSearchMeasureToolCode.Size = new System.Drawing.Size(144, 21);
            this.uTextSearchMeasureToolCode.TabIndex = 34;
            // 
            // uComboMTMType
            // 
            this.uComboMTMType.Location = new System.Drawing.Point(688, 16);
            this.uComboMTMType.Name = "uComboMTMType";
            this.uComboMTMType.Size = new System.Drawing.Size(144, 21);
            this.uComboMTMType.TabIndex = 169;
            this.uComboMTMType.ValueChanged += new System.EventHandler(this.uComboMTMType_ValueChanged);
            // 
            // uLabelSearchMeasureToolCode
            // 
            this.uLabelSearchMeasureToolCode.Location = new System.Drawing.Point(624, 12);
            this.uLabelSearchMeasureToolCode.Name = "uLabelSearchMeasureToolCode";
            this.uLabelSearchMeasureToolCode.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchMeasureToolCode.TabIndex = 33;
            // 
            // uComboSearchMTType
            // 
            this.uComboSearchMTType.Location = new System.Drawing.Point(396, 12);
            this.uComboSearchMTType.Name = "uComboSearchMTType";
            this.uComboSearchMTType.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchMTType.TabIndex = 5;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxHistory);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboMTMType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMTMType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboMTLType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMTLType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMTFileName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMTFileName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateLastInspectDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelClass);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelResult);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLastInspectDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPurpose);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCustomer);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAllowance);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.richTextEditor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDiscardReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboClass);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDiscardFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboResult);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboDiscardFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateDiscardDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDiscardDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateInspectNextDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectNextDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectPeriodUnitCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboInspectPeriodUnitCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uNumInspectPeriod);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectPeriod);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAcquireReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcquireReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcquireDeptCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboAcquireDeptCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uNumAcquireAmt);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcquireAmt);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateAcquireDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcquireDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakerNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakerCompany);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMakerNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMakerCompany);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextModelName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelModelName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMeasureToolCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPurpose);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAllowance);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCustomer);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMeasureToolName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMeasureToolNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMeasureToolNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMeasureToolName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMTType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboMTType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxHistory
            // 
            this.uGroupBoxHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxHistory.Controls.Add(this.uGridHistory);
            this.uGroupBoxHistory.Location = new System.Drawing.Point(12, 432);
            this.uGroupBoxHistory.Name = "uGroupBoxHistory";
            this.uGroupBoxHistory.Size = new System.Drawing.Size(1040, 212);
            this.uGroupBoxHistory.TabIndex = 170;
            // 
            // uGridHistory
            // 
            this.uGridHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHistory.DisplayLayout.Appearance = appearance20;
            this.uGridHistory.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHistory.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHistory.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHistory.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGridHistory.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHistory.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGridHistory.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHistory.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHistory.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHistory.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGridHistory.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHistory.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHistory.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHistory.DisplayLayout.Override.CellAppearance = appearance28;
            this.uGridHistory.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHistory.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHistory.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.uGridHistory.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.uGridHistory.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHistory.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            this.uGridHistory.DisplayLayout.Override.RowAppearance = appearance31;
            this.uGridHistory.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHistory.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridHistory.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHistory.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHistory.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHistory.Location = new System.Drawing.Point(12, 12);
            this.uGridHistory.Name = "uGridHistory";
            this.uGridHistory.Size = new System.Drawing.Size(1016, 188);
            this.uGridHistory.TabIndex = 0;
            this.uGridHistory.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridHistory_DoubleClickCell);
            // 
            // uLabelMTMType
            // 
            this.uLabelMTMType.Location = new System.Drawing.Point(560, 16);
            this.uLabelMTMType.Name = "uLabelMTMType";
            this.uLabelMTMType.Size = new System.Drawing.Size(124, 20);
            this.uLabelMTMType.TabIndex = 168;
            // 
            // uComboMTLType
            // 
            this.uComboMTLType.Location = new System.Drawing.Point(408, 16);
            this.uComboMTLType.Name = "uComboMTLType";
            this.uComboMTLType.Size = new System.Drawing.Size(144, 21);
            this.uComboMTLType.TabIndex = 167;
            this.uComboMTLType.ValueChanged += new System.EventHandler(this.uComboMTLType_ValueChanged);
            // 
            // uLabelMTLType
            // 
            this.uLabelMTLType.Location = new System.Drawing.Point(280, 16);
            this.uLabelMTLType.Name = "uLabelMTLType";
            this.uLabelMTLType.Size = new System.Drawing.Size(124, 20);
            this.uLabelMTLType.TabIndex = 166;
            // 
            // uTextMTFileName
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMTFileName.Appearance = appearance53;
            this.uTextMTFileName.BackColor = System.Drawing.Color.Gainsboro;
            appearance43.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance43.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance43;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Key = "UP";
            appearance26.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance26.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance26;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Key = "DOWN";
            this.uTextMTFileName.ButtonsRight.Add(editorButton1);
            this.uTextMTFileName.ButtonsRight.Add(editorButton2);
            this.uTextMTFileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMTFileName.Location = new System.Drawing.Point(128, 280);
            this.uTextMTFileName.Name = "uTextMTFileName";
            this.uTextMTFileName.ReadOnly = true;
            this.uTextMTFileName.Size = new System.Drawing.Size(404, 21);
            this.uTextMTFileName.TabIndex = 165;
            this.uTextMTFileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMTFileName_EditorButtonClick);
            // 
            // uLabelMTFileName
            // 
            this.uLabelMTFileName.Location = new System.Drawing.Point(12, 280);
            this.uLabelMTFileName.Name = "uLabelMTFileName";
            this.uLabelMTFileName.Size = new System.Drawing.Size(112, 20);
            this.uLabelMTFileName.TabIndex = 164;
            // 
            // uDateLastInspectDate
            // 
            this.uDateLastInspectDate.DateTime = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            this.uDateLastInspectDate.Location = new System.Drawing.Point(688, 232);
            this.uDateLastInspectDate.Name = "uDateLastInspectDate";
            this.uDateLastInspectDate.Size = new System.Drawing.Size(144, 21);
            this.uDateLastInspectDate.TabIndex = 72;
            this.uDateLastInspectDate.Value = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            // 
            // uLabelLastInspectDate
            // 
            this.uLabelLastInspectDate.Location = new System.Drawing.Point(560, 232);
            this.uLabelLastInspectDate.Name = "uLabelLastInspectDate";
            this.uLabelLastInspectDate.Size = new System.Drawing.Size(124, 20);
            this.uLabelLastInspectDate.TabIndex = 71;
            // 
            // uTextWriteID
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteID.Appearance = appearance10;
            this.uTextWriteID.BackColor = System.Drawing.Color.PowderBlue;
            appearance11.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance11;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteID.ButtonsRight.Add(editorButton3);
            this.uTextWriteID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteID.Location = new System.Drawing.Point(688, 256);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteID.TabIndex = 70;
            this.uTextWriteID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteID_KeyDown);
            this.uTextWriteID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uTextWriteName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance4;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(792, 256);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteName.TabIndex = 69;
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(560, 256);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(124, 20);
            this.uLabelWriteUser.TabIndex = 68;
            // 
            // richTextEditor
            // 
            this.richTextEditor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextEditor.FontSize = QRPUserControl.FontSize.Three;
            this.richTextEditor.Location = new System.Drawing.Point(12, 328);
            this.richTextEditor.Name = "richTextEditor";
            this.richTextEditor.Size = new System.Drawing.Size(1024, 100);
            this.richTextEditor.TabIndex = 67;
            // 
            // uLabelDiscardReason
            // 
            this.uLabelDiscardReason.Location = new System.Drawing.Point(12, 304);
            this.uLabelDiscardReason.Name = "uLabelDiscardReason";
            this.uLabelDiscardReason.Size = new System.Drawing.Size(112, 20);
            this.uLabelDiscardReason.TabIndex = 66;
            // 
            // uLabelDiscardFlag
            // 
            this.uLabelDiscardFlag.Location = new System.Drawing.Point(284, 232);
            this.uLabelDiscardFlag.Name = "uLabelDiscardFlag";
            this.uLabelDiscardFlag.Size = new System.Drawing.Size(120, 20);
            this.uLabelDiscardFlag.TabIndex = 65;
            // 
            // uComboDiscardFlag
            // 
            this.uComboDiscardFlag.Location = new System.Drawing.Point(408, 232);
            this.uComboDiscardFlag.Name = "uComboDiscardFlag";
            this.uComboDiscardFlag.Size = new System.Drawing.Size(144, 21);
            this.uComboDiscardFlag.TabIndex = 64;
            // 
            // uDateDiscardDate
            // 
            this.uDateDiscardDate.DateTime = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            this.uDateDiscardDate.Location = new System.Drawing.Point(128, 232);
            this.uDateDiscardDate.Name = "uDateDiscardDate";
            this.uDateDiscardDate.Size = new System.Drawing.Size(144, 21);
            this.uDateDiscardDate.TabIndex = 63;
            this.uDateDiscardDate.Value = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            this.uDateDiscardDate.AfterExitEditMode += new System.EventHandler(this.uDateDiscardDate_AfterExitEditMode);
            // 
            // uLabelDiscardDate
            // 
            this.uLabelDiscardDate.Location = new System.Drawing.Point(12, 232);
            this.uLabelDiscardDate.Name = "uLabelDiscardDate";
            this.uLabelDiscardDate.Size = new System.Drawing.Size(112, 20);
            this.uLabelDiscardDate.TabIndex = 62;
            // 
            // uDateInspectNextDate
            // 
            this.uDateInspectNextDate.DateTime = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            this.uDateInspectNextDate.Location = new System.Drawing.Point(688, 208);
            this.uDateInspectNextDate.Name = "uDateInspectNextDate";
            this.uDateInspectNextDate.Size = new System.Drawing.Size(144, 21);
            this.uDateInspectNextDate.TabIndex = 60;
            this.uDateInspectNextDate.Value = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            // 
            // uLabelInspectNextDate
            // 
            this.uLabelInspectNextDate.Location = new System.Drawing.Point(560, 208);
            this.uLabelInspectNextDate.Name = "uLabelInspectNextDate";
            this.uLabelInspectNextDate.Size = new System.Drawing.Size(124, 20);
            this.uLabelInspectNextDate.TabIndex = 59;
            // 
            // uLabelInspectPeriodUnitCode
            // 
            this.uLabelInspectPeriodUnitCode.Location = new System.Drawing.Point(284, 208);
            this.uLabelInspectPeriodUnitCode.Name = "uLabelInspectPeriodUnitCode";
            this.uLabelInspectPeriodUnitCode.Size = new System.Drawing.Size(120, 20);
            this.uLabelInspectPeriodUnitCode.TabIndex = 57;
            // 
            // uComboInspectPeriodUnitCode
            // 
            this.uComboInspectPeriodUnitCode.Location = new System.Drawing.Point(408, 208);
            this.uComboInspectPeriodUnitCode.Name = "uComboInspectPeriodUnitCode";
            this.uComboInspectPeriodUnitCode.Size = new System.Drawing.Size(144, 21);
            this.uComboInspectPeriodUnitCode.TabIndex = 56;
            // 
            // uNumInspectPeriod
            // 
            this.uNumInspectPeriod.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Disabled;
            this.uNumInspectPeriod.Location = new System.Drawing.Point(128, 208);
            this.uNumInspectPeriod.Name = "uNumInspectPeriod";
            this.uNumInspectPeriod.Size = new System.Drawing.Size(144, 21);
            this.uNumInspectPeriod.TabIndex = 55;
            this.uNumInspectPeriod.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumInspectPeriod_EditorSpinButtonClick);
            this.uNumInspectPeriod.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumInspectPeriod_EditorButtonClick);
            // 
            // uLabelInspectPeriod
            // 
            this.uLabelInspectPeriod.Location = new System.Drawing.Point(12, 208);
            this.uLabelInspectPeriod.Name = "uLabelInspectPeriod";
            this.uLabelInspectPeriod.Size = new System.Drawing.Size(112, 20);
            this.uLabelInspectPeriod.TabIndex = 54;
            // 
            // uTextAcquireReason
            // 
            this.uTextAcquireReason.Location = new System.Drawing.Point(128, 184);
            this.uTextAcquireReason.Name = "uTextAcquireReason";
            this.uTextAcquireReason.Size = new System.Drawing.Size(424, 21);
            this.uTextAcquireReason.TabIndex = 53;
            // 
            // uLabelAcquireReason
            // 
            this.uLabelAcquireReason.Location = new System.Drawing.Point(12, 184);
            this.uLabelAcquireReason.Name = "uLabelAcquireReason";
            this.uLabelAcquireReason.Size = new System.Drawing.Size(112, 20);
            this.uLabelAcquireReason.TabIndex = 52;
            // 
            // uLabelAcquireDeptCode
            // 
            this.uLabelAcquireDeptCode.Location = new System.Drawing.Point(280, 160);
            this.uLabelAcquireDeptCode.Name = "uLabelAcquireDeptCode";
            this.uLabelAcquireDeptCode.Size = new System.Drawing.Size(124, 20);
            this.uLabelAcquireDeptCode.TabIndex = 51;
            // 
            // uComboAcquireDeptCode
            // 
            this.uComboAcquireDeptCode.Location = new System.Drawing.Point(408, 160);
            this.uComboAcquireDeptCode.Name = "uComboAcquireDeptCode";
            this.uComboAcquireDeptCode.Size = new System.Drawing.Size(144, 21);
            this.uComboAcquireDeptCode.TabIndex = 50;
            // 
            // uTextSerialNo
            // 
            this.uTextSerialNo.Location = new System.Drawing.Point(688, 136);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.Size = new System.Drawing.Size(240, 21);
            this.uTextSerialNo.TabIndex = 49;
            // 
            // uLabelSerialNo
            // 
            this.uLabelSerialNo.Location = new System.Drawing.Point(560, 136);
            this.uLabelSerialNo.Name = "uLabelSerialNo";
            this.uLabelSerialNo.Size = new System.Drawing.Size(124, 20);
            this.uLabelSerialNo.TabIndex = 48;
            // 
            // uNumAcquireAmt
            // 
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Text = "0";
            this.uNumAcquireAmt.ButtonsLeft.Add(editorButton4);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumAcquireAmt.ButtonsRight.Add(spinEditorButton1);
            this.uNumAcquireAmt.Location = new System.Drawing.Point(688, 160);
            this.uNumAcquireAmt.Name = "uNumAcquireAmt";
            this.uNumAcquireAmt.Size = new System.Drawing.Size(144, 21);
            this.uNumAcquireAmt.TabIndex = 47;
            this.uNumAcquireAmt.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumAcquireAmt_EditorSpinButtonClick);
            this.uNumAcquireAmt.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumAcquireAmt_EditorButtonClick);
            // 
            // uLabelAcquireAmt
            // 
            this.uLabelAcquireAmt.Location = new System.Drawing.Point(560, 160);
            this.uLabelAcquireAmt.Name = "uLabelAcquireAmt";
            this.uLabelAcquireAmt.Size = new System.Drawing.Size(124, 20);
            this.uLabelAcquireAmt.TabIndex = 46;
            // 
            // uDateAcquireDate
            // 
            this.uDateAcquireDate.DateTime = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            this.uDateAcquireDate.Location = new System.Drawing.Point(128, 160);
            this.uDateAcquireDate.Name = "uDateAcquireDate";
            this.uDateAcquireDate.Size = new System.Drawing.Size(144, 21);
            this.uDateAcquireDate.TabIndex = 45;
            this.uDateAcquireDate.Value = new System.DateTime(2011, 10, 11, 0, 0, 0, 0);
            // 
            // uLabelAcquireDate
            // 
            this.uLabelAcquireDate.Location = new System.Drawing.Point(12, 160);
            this.uLabelAcquireDate.Name = "uLabelAcquireDate";
            this.uLabelAcquireDate.Size = new System.Drawing.Size(112, 20);
            this.uLabelAcquireDate.TabIndex = 44;
            // 
            // uTextMakerNo
            // 
            this.uTextMakerNo.Location = new System.Drawing.Point(956, 184);
            this.uTextMakerNo.Name = "uTextMakerNo";
            this.uTextMakerNo.Size = new System.Drawing.Size(16, 21);
            this.uTextMakerNo.TabIndex = 42;
            this.uTextMakerNo.Visible = false;
            // 
            // uTextMakerCompany
            // 
            this.uTextMakerCompany.Location = new System.Drawing.Point(128, 136);
            this.uTextMakerCompany.Name = "uTextMakerCompany";
            this.uTextMakerCompany.Size = new System.Drawing.Size(424, 21);
            this.uTextMakerCompany.TabIndex = 41;
            // 
            // uLabelMakerNo
            // 
            this.uLabelMakerNo.Location = new System.Drawing.Point(936, 184);
            this.uLabelMakerNo.Name = "uLabelMakerNo";
            this.uLabelMakerNo.Size = new System.Drawing.Size(16, 20);
            this.uLabelMakerNo.TabIndex = 40;
            this.uLabelMakerNo.Visible = false;
            // 
            // uLabelMakerCompany
            // 
            this.uLabelMakerCompany.Location = new System.Drawing.Point(12, 136);
            this.uLabelMakerCompany.Name = "uLabelMakerCompany";
            this.uLabelMakerCompany.Size = new System.Drawing.Size(112, 20);
            this.uLabelMakerCompany.TabIndex = 39;
            // 
            // uTextModelName
            // 
            this.uTextModelName.Location = new System.Drawing.Point(688, 112);
            this.uTextModelName.Name = "uTextModelName";
            this.uTextModelName.Size = new System.Drawing.Size(240, 21);
            this.uTextModelName.TabIndex = 38;
            // 
            // uTextSpec
            // 
            this.uTextSpec.Location = new System.Drawing.Point(128, 112);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.Size = new System.Drawing.Size(424, 21);
            this.uTextSpec.TabIndex = 37;
            // 
            // uLabelModelName
            // 
            this.uLabelModelName.Location = new System.Drawing.Point(560, 112);
            this.uLabelModelName.Name = "uLabelModelName";
            this.uLabelModelName.Size = new System.Drawing.Size(124, 20);
            this.uLabelModelName.TabIndex = 36;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 112);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(112, 20);
            this.uLabelSpec.TabIndex = 35;
            // 
            // uTextMeasureToolCode
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMeasureToolCode.Appearance = appearance14;
            this.uTextMeasureToolCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMeasureToolCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMeasureToolCode.Location = new System.Drawing.Point(128, 64);
            this.uTextMeasureToolCode.Name = "uTextMeasureToolCode";
            this.uTextMeasureToolCode.Size = new System.Drawing.Size(144, 21);
            this.uTextMeasureToolCode.TabIndex = 34;
            // 
            // uLabelMeasureToolCode
            // 
            this.uLabelMeasureToolCode.Location = new System.Drawing.Point(12, 64);
            this.uLabelMeasureToolCode.Name = "uLabelMeasureToolCode";
            this.uLabelMeasureToolCode.Size = new System.Drawing.Size(112, 20);
            this.uLabelMeasureToolCode.TabIndex = 33;
            // 
            // uTextMeasureToolNameEn
            // 
            this.uTextMeasureToolNameEn.Location = new System.Drawing.Point(688, 88);
            this.uTextMeasureToolNameEn.Name = "uTextMeasureToolNameEn";
            this.uTextMeasureToolNameEn.Size = new System.Drawing.Size(144, 21);
            this.uTextMeasureToolNameEn.TabIndex = 32;
            // 
            // uTextMeasureToolNameCh
            // 
            this.uTextMeasureToolNameCh.Location = new System.Drawing.Point(408, 88);
            this.uTextMeasureToolNameCh.Name = "uTextMeasureToolNameCh";
            this.uTextMeasureToolNameCh.Size = new System.Drawing.Size(144, 21);
            this.uTextMeasureToolNameCh.TabIndex = 31;
            // 
            // uTextMeasureToolName
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMeasureToolName.Appearance = appearance15;
            this.uTextMeasureToolName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMeasureToolName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMeasureToolName.Location = new System.Drawing.Point(128, 88);
            this.uTextMeasureToolName.Name = "uTextMeasureToolName";
            this.uTextMeasureToolName.Size = new System.Drawing.Size(144, 21);
            this.uTextMeasureToolName.TabIndex = 30;
            // 
            // uLabelMeasureToolNameEn
            // 
            this.uLabelMeasureToolNameEn.Location = new System.Drawing.Point(560, 88);
            this.uLabelMeasureToolNameEn.Name = "uLabelMeasureToolNameEn";
            this.uLabelMeasureToolNameEn.Size = new System.Drawing.Size(124, 20);
            this.uLabelMeasureToolNameEn.TabIndex = 29;
            // 
            // uLabelMeasureToolNameCh
            // 
            this.uLabelMeasureToolNameCh.Location = new System.Drawing.Point(280, 88);
            this.uLabelMeasureToolNameCh.Name = "uLabelMeasureToolNameCh";
            this.uLabelMeasureToolNameCh.Size = new System.Drawing.Size(124, 20);
            this.uLabelMeasureToolNameCh.TabIndex = 28;
            // 
            // uLabelMeasureToolName
            // 
            this.uLabelMeasureToolName.Location = new System.Drawing.Point(12, 88);
            this.uLabelMeasureToolName.Name = "uLabelMeasureToolName";
            this.uLabelMeasureToolName.Size = new System.Drawing.Size(112, 20);
            this.uLabelMeasureToolName.TabIndex = 27;
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(280, 40);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(124, 20);
            this.uLabelUseFlag.TabIndex = 23;
            // 
            // uLabelMTType
            // 
            this.uLabelMTType.Location = new System.Drawing.Point(12, 40);
            this.uLabelMTType.Name = "uLabelMTType";
            this.uLabelMTType.Size = new System.Drawing.Size(112, 20);
            this.uLabelMTType.TabIndex = 22;
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(408, 40);
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(144, 21);
            this.uComboUseFlag.TabIndex = 21;
            // 
            // uComboMTType
            // 
            this.uComboMTType.Location = new System.Drawing.Point(128, 40);
            this.uComboMTType.Name = "uComboMTType";
            this.uComboMTType.Size = new System.Drawing.Size(144, 21);
            this.uComboMTType.TabIndex = 20;
            this.uComboMTType.ValueChanged += new System.EventHandler(this.uComboMTType_ValueChanged);
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(128, 16);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboPlant.TabIndex = 19;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 16);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(112, 20);
            this.uLabelPlant.TabIndex = 18;
            // 
            // uLabelSearchMTType
            // 
            this.uLabelSearchMTType.Location = new System.Drawing.Point(268, 12);
            this.uLabelSearchMTType.Name = "uLabelSearchMTType";
            this.uLabelSearchMTType.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchMTType.TabIndex = 4;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckSearchInspectNextDateOver);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectNextDateOver);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchInspectNextDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchstrInspectNextDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectNextDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchAcquireDeptCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchAcquireDept);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMeasurename);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMeasureToolCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMeasureToolCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMTType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMTType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 9;
            // 
            // uCheckSearchInspectNextDateOver
            // 
            this.uCheckSearchInspectNextDateOver.Location = new System.Drawing.Point(900, 36);
            this.uCheckSearchInspectNextDateOver.Name = "uCheckSearchInspectNextDateOver";
            this.uCheckSearchInspectNextDateOver.Size = new System.Drawing.Size(16, 20);
            this.uCheckSearchInspectNextDateOver.TabIndex = 68;
            // 
            // uLabelSearchInspectNextDateOver
            // 
            this.uLabelSearchInspectNextDateOver.Location = new System.Drawing.Point(916, 36);
            this.uLabelSearchInspectNextDateOver.Name = "uLabelSearchInspectNextDateOver";
            this.uLabelSearchInspectNextDateOver.Size = new System.Drawing.Size(132, 20);
            this.uLabelSearchInspectNextDateOver.TabIndex = 67;
            this.uLabelSearchInspectNextDateOver.Text = "차기교정일 초과";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 2;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 182);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 11;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // uGridMeasureToolList
            // 
            this.uGridMeasureToolList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMeasureToolList.DisplayLayout.Appearance = appearance2;
            this.uGridMeasureToolList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMeasureToolList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMeasureToolList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMeasureToolList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridMeasureToolList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMeasureToolList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridMeasureToolList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMeasureToolList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMeasureToolList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMeasureToolList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridMeasureToolList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMeasureToolList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMeasureToolList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMeasureToolList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridMeasureToolList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMeasureToolList.DisplayLayout.Override.CellPadding = 0;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMeasureToolList.DisplayLayout.Override.GroupByRowAppearance = appearance17;
            appearance18.TextHAlignAsString = "Left";
            this.uGridMeasureToolList.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGridMeasureToolList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMeasureToolList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridMeasureToolList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridMeasureToolList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMeasureToolList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridMeasureToolList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMeasureToolList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMeasureToolList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMeasureToolList.Location = new System.Drawing.Point(0, 100);
            this.uGridMeasureToolList.Name = "uGridMeasureToolList";
            this.uGridMeasureToolList.Size = new System.Drawing.Size(1070, 720);
            this.uGridMeasureToolList.TabIndex = 10;
            this.uGridMeasureToolList.Text = "ultraGrid1";
            this.uGridMeasureToolList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridMeasureToolList_DoubleClickCell);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 8;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(280, 64);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(124, 20);
            this.uLabelCustomer.TabIndex = 68;
            this.uLabelCustomer.Text = "고객";
            // 
            // uLabelResult
            // 
            this.uLabelResult.Location = new System.Drawing.Point(284, 256);
            this.uLabelResult.Name = "uLabelResult";
            this.uLabelResult.Size = new System.Drawing.Size(120, 20);
            this.uLabelResult.TabIndex = 71;
            this.uLabelResult.Text = "검증결과";
            // 
            // uComboResult
            // 
            this.uComboResult.Location = new System.Drawing.Point(408, 256);
            this.uComboResult.Name = "uComboResult";
            this.uComboResult.Size = new System.Drawing.Size(144, 21);
            this.uComboResult.TabIndex = 64;
            // 
            // uTextCustomer
            // 
            this.uTextCustomer.Location = new System.Drawing.Point(408, 64);
            this.uTextCustomer.Name = "uTextCustomer";
            this.uTextCustomer.Size = new System.Drawing.Size(144, 21);
            this.uTextCustomer.TabIndex = 31;
            // 
            // uLabelPurpose
            // 
            this.uLabelPurpose.Location = new System.Drawing.Point(560, 64);
            this.uLabelPurpose.Name = "uLabelPurpose";
            this.uLabelPurpose.Size = new System.Drawing.Size(124, 20);
            this.uLabelPurpose.TabIndex = 68;
            this.uLabelPurpose.Text = "Purpose";
            // 
            // uTextPurpose
            // 
            this.uTextPurpose.Location = new System.Drawing.Point(688, 64);
            this.uTextPurpose.Name = "uTextPurpose";
            this.uTextPurpose.Size = new System.Drawing.Size(144, 21);
            this.uTextPurpose.TabIndex = 31;
            // 
            // uComboClass
            // 
            this.uComboClass.Location = new System.Drawing.Point(688, 40);
            this.uComboClass.Name = "uComboClass";
            this.uComboClass.Size = new System.Drawing.Size(144, 21);
            this.uComboClass.TabIndex = 64;
            // 
            // uLabelClass
            // 
            this.uLabelClass.Location = new System.Drawing.Point(560, 40);
            this.uLabelClass.Name = "uLabelClass";
            this.uLabelClass.Size = new System.Drawing.Size(124, 20);
            this.uLabelClass.TabIndex = 71;
            this.uLabelClass.Text = "Class";
            // 
            // uLabelAllowance
            // 
            this.uLabelAllowance.Location = new System.Drawing.Point(12, 256);
            this.uLabelAllowance.Name = "uLabelAllowance";
            this.uLabelAllowance.Size = new System.Drawing.Size(112, 20);
            this.uLabelAllowance.TabIndex = 68;
            this.uLabelAllowance.Text = "공차";
            // 
            // uTextAllowance
            // 
            this.uTextAllowance.Location = new System.Drawing.Point(128, 256);
            this.uTextAllowance.Name = "uTextAllowance";
            this.uTextAllowance.Size = new System.Drawing.Size(144, 21);
            this.uTextAllowance.TabIndex = 31;
            // 
            // frmQATZ0014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridMeasureToolList);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0014";
            this.Load += new System.EventHandler(this.frmQATZ0014_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0014_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0014_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0014_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchInspectNextDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchstrInspectNextDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchAcquireDeptCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMeasureToolCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMTMType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMTType)).EndInit();
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxHistory)).EndInit();
            this.uGroupBoxHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMTLType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMTFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateLastInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDiscardFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDiscardDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectNextDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectPeriodUnitCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInspectPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcquireReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAcquireDeptCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumAcquireAmt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAcquireDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModelName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureToolName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMTType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchInspectNextDateOver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMeasureToolList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAllowance)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchInspectNextDateTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchstrInspectNextDateFrom;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectNextDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchAcquireDeptCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchAcquireDept;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMeasurename;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMeasureToolCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMTMType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMeasureToolCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMTType;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelMTMType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMTLType;
        private Infragistics.Win.Misc.UltraLabel uLabelMTLType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMTFileName;
        private Infragistics.Win.Misc.UltraLabel uLabelMTFileName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateLastInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelLastInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private QRPUserControl.RichTextEditor richTextEditor;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardReason;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDiscardFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDiscardDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDiscardDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectNextDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectNextDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectPeriodUnitCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectPeriodUnitCode;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumInspectPeriod;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectPeriod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcquireReason;
        private Infragistics.Win.Misc.UltraLabel uLabelAcquireReason;
        private Infragistics.Win.Misc.UltraLabel uLabelAcquireDeptCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAcquireDeptCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumAcquireAmt;
        private Infragistics.Win.Misc.UltraLabel uLabelAcquireAmt;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAcquireDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAcquireDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakerNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakerCompany;
        private Infragistics.Win.Misc.UltraLabel uLabelMakerNo;
        private Infragistics.Win.Misc.UltraLabel uLabelMakerCompany;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModelName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelModelName;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureToolCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureToolName;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureToolNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureToolNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelMeasureToolName;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelMTType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMTType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMTType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMeasureToolList;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxHistory;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHistory;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSearchInspectNextDateOver;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectNextDateOver;
        private Infragistics.Win.Misc.UltraLabel uLabelResult;
        private Infragistics.Win.Misc.UltraLabel uLabelPurpose;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboResult;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelClass;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboClass;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPurpose;
        private Infragistics.Win.Misc.UltraLabel uLabelAllowance;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAllowance;
    }
}