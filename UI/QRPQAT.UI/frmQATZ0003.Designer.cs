﻿namespace QRPQAT.UI
{
    partial class frmQATZ0003
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0003));
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraTextEditor18 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor16 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.uCheck5 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckE4 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor20 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor17 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor15 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheck1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheck3 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheck2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor19 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckE4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 168);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 8;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox4.Controls.Add(this.uLabel21);
            this.uGroupBox4.Controls.Add(this.uLabel20);
            this.uGroupBox4.Controls.Add(this.uLabel19);
            this.uGroupBox4.Controls.Add(this.uGrid4);
            this.uGroupBox4.Controls.Add(this.uGrid2);
            this.uGroupBox4.Controls.Add(this.uGrid3);
            this.uGroupBox4.Location = new System.Drawing.Point(12, 248);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(1040, 404);
            this.uGroupBox4.TabIndex = 105;
            // 
            // uLabel21
            // 
            this.uLabel21.Location = new System.Drawing.Point(528, 12);
            this.uLabel21.Name = "uLabel21";
            this.uLabel21.Size = new System.Drawing.Size(120, 20);
            this.uLabel21.TabIndex = 98;
            // 
            // uLabel20
            // 
            this.uLabel20.Location = new System.Drawing.Point(12, 196);
            this.uLabel20.Name = "uLabel20";
            this.uLabel20.Size = new System.Drawing.Size(216, 20);
            this.uLabel20.TabIndex = 98;
            // 
            // uLabel19
            // 
            this.uLabel19.Location = new System.Drawing.Point(12, 12);
            this.uLabel19.Name = "uLabel19";
            this.uLabel19.Size = new System.Drawing.Size(120, 20);
            this.uLabel19.TabIndex = 98;
            // 
            // uGrid4
            // 
            this.uGrid4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4.DisplayLayout.Appearance = appearance16;
            this.uGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.uGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            this.uGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Highlight;
            appearance21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance21;
            this.uGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance24.BackColor = System.Drawing.SystemColors.Control;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance24;
            appearance25.TextHAlignAsString = "Left";
            this.uGrid4.DisplayLayout.Override.HeaderAppearance = appearance25;
            this.uGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4.DisplayLayout.Override.RowAppearance = appearance26;
            this.uGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance27;
            this.uGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4.Location = new System.Drawing.Point(524, 36);
            this.uGrid4.Name = "uGrid4";
            this.uGrid4.Size = new System.Drawing.Size(504, 156);
            this.uGrid4.TabIndex = 0;
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance40;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 36);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(504, 156);
            this.uGrid2.TabIndex = 0;
            // 
            // uGrid3
            // 
            this.uGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance28;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance29;
            appearance30.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance30;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance31.BackColor2 = System.Drawing.SystemColors.Control;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance31;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance32;
            appearance33.BackColor = System.Drawing.SystemColors.Highlight;
            appearance33.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance33;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance34;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            appearance35.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance35;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance36.BackColor = System.Drawing.SystemColors.Control;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance36;
            appearance37.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance37;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance38;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance39;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(8, 220);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(1016, 176);
            this.uGrid3.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.ultraDateTimeEditor2);
            this.uGroupBox3.Controls.Add(this.ultraTextEditor18);
            this.uGroupBox3.Controls.Add(this.ultraTextEditor16);
            this.uGroupBox3.Controls.Add(this.uLabel18);
            this.uGroupBox3.Controls.Add(this.uCheck5);
            this.uGroupBox3.Controls.Add(this.uLabel2);
            this.uGroupBox3.Controls.Add(this.uCheckE4);
            this.uGroupBox3.Controls.Add(this.uLabel17);
            this.uGroupBox3.Controls.Add(this.uLabel16);
            this.uGroupBox3.Controls.Add(this.uLabel15);
            this.uGroupBox3.Controls.Add(this.ultraTextEditor20);
            this.uGroupBox3.Controls.Add(this.ultraTextEditor17);
            this.uGroupBox3.Controls.Add(this.ultraTextEditor15);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 180);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1040, 64);
            this.uGroupBox3.TabIndex = 104;
            // 
            // ultraDateTimeEditor2
            // 
            appearance65.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor2.Appearance = appearance65;
            this.ultraDateTimeEditor2.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(868, 36);
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor2.TabIndex = 93;
            // 
            // ultraTextEditor18
            // 
            appearance52.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor18.Appearance = appearance52;
            this.ultraTextEditor18.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor18.Location = new System.Drawing.Point(596, 36);
            this.ultraTextEditor18.Name = "ultraTextEditor18";
            this.ultraTextEditor18.ReadOnly = true;
            this.ultraTextEditor18.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor18.TabIndex = 79;
            // 
            // ultraTextEditor16
            // 
            appearance68.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Appearance = appearance68;
            this.ultraTextEditor16.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor16.Location = new System.Drawing.Point(596, 12);
            this.ultraTextEditor16.Name = "ultraTextEditor16";
            this.ultraTextEditor16.ReadOnly = true;
            this.ultraTextEditor16.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor16.TabIndex = 79;
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(368, 36);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(120, 20);
            this.uLabel18.TabIndex = 98;
            // 
            // uCheck5
            // 
            this.uCheck5.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheck5.Enabled = false;
            this.uCheck5.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheck5.Location = new System.Drawing.Point(188, 12);
            this.uCheck5.Name = "uCheck5";
            this.uCheck5.Size = new System.Drawing.Size(60, 20);
            this.uCheck5.TabIndex = 103;
            this.uCheck5.Text = "불합격";
            this.uCheck5.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(744, 36);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(120, 20);
            this.uLabel2.TabIndex = 98;
            // 
            // uCheckE4
            // 
            this.uCheckE4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckE4.Enabled = false;
            this.uCheckE4.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckE4.Location = new System.Drawing.Point(136, 12);
            this.uCheckE4.Name = "uCheckE4";
            this.uCheckE4.Size = new System.Drawing.Size(48, 20);
            this.uCheckE4.TabIndex = 103;
            this.uCheckE4.Text = "합격";
            this.uCheckE4.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(12, 36);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(120, 20);
            this.uLabel17.TabIndex = 98;
            // 
            // uLabel16
            // 
            this.uLabel16.Location = new System.Drawing.Point(368, 12);
            this.uLabel16.Name = "uLabel16";
            this.uLabel16.Size = new System.Drawing.Size(120, 20);
            this.uLabel16.TabIndex = 98;
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(12, 12);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(120, 20);
            this.uLabel15.TabIndex = 98;
            // 
            // ultraTextEditor20
            // 
            appearance58.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor20.Appearance = appearance58;
            this.ultraTextEditor20.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor20.Location = new System.Drawing.Point(136, 36);
            this.ultraTextEditor20.Name = "ultraTextEditor20";
            this.ultraTextEditor20.ReadOnly = true;
            this.ultraTextEditor20.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor20.TabIndex = 79;
            // 
            // ultraTextEditor17
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor17.Appearance = appearance59;
            this.ultraTextEditor17.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor17.Location = new System.Drawing.Point(492, 36);
            this.ultraTextEditor17.Name = "ultraTextEditor17";
            this.ultraTextEditor17.ReadOnly = true;
            this.ultraTextEditor17.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor17.TabIndex = 79;
            // 
            // ultraTextEditor15
            // 
            appearance73.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor15.Appearance = appearance73;
            this.ultraTextEditor15.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor15.Location = new System.Drawing.Point(492, 12);
            this.ultraTextEditor15.Name = "ultraTextEditor15";
            this.ultraTextEditor15.ReadOnly = true;
            this.ultraTextEditor15.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor15.TabIndex = 79;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uCheck1);
            this.uGroupBox1.Controls.Add(this.uCheck3);
            this.uGroupBox1.Controls.Add(this.uCheck2);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor10);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor9);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor14);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor13);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor12);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor11);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor8);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor7);
            this.uGroupBox1.Controls.Add(this.uLabel7);
            this.uGroupBox1.Controls.Add(this.uLabel10);
            this.uGroupBox1.Controls.Add(this.uLabel14);
            this.uGroupBox1.Controls.Add(this.uLabel8);
            this.uGroupBox1.Controls.Add(this.uLabel12);
            this.uGroupBox1.Controls.Add(this.uLabel13);
            this.uGroupBox1.Controls.Add(this.uLabel11);
            this.uGroupBox1.Controls.Add(this.uLabel9);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor6);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor19);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor5);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor4);
            this.uGroupBox1.Controls.Add(this.ultraTextEditor3);
            this.uGroupBox1.Controls.Add(this.uLabel1);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.uLabel3);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 164);
            this.uGroupBox1.TabIndex = 102;
            // 
            // uCheck1
            // 
            this.uCheck1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheck1.Enabled = false;
            this.uCheck1.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheck1.Location = new System.Drawing.Point(700, 132);
            this.uCheck1.Name = "uCheck1";
            this.uCheck1.Size = new System.Drawing.Size(72, 20);
            this.uCheck1.TabIndex = 122;
            this.uCheck1.Text = "설비표준";
            this.uCheck1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheck3
            // 
            this.uCheck3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheck3.Enabled = false;
            this.uCheck3.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheck3.Location = new System.Drawing.Point(852, 132);
            this.uCheck3.Name = "uCheck3";
            this.uCheck3.Size = new System.Drawing.Size(72, 20);
            this.uCheck3.TabIndex = 123;
            this.uCheck3.Text = "기술표준";
            this.uCheck3.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheck2
            // 
            this.uCheck2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheck2.Enabled = false;
            this.uCheck2.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheck2.Location = new System.Drawing.Point(776, 132);
            this.uCheck2.Name = "uCheck2";
            this.uCheck2.Size = new System.Drawing.Size(72, 20);
            this.uCheck2.TabIndex = 121;
            this.uCheck2.Text = "작업표준";
            this.uCheck2.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(12, 60);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(120, 20);
            this.uLabel6.TabIndex = 112;
            this.uLabel6.Text = "6";
            // 
            // ultraTextEditor10
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Appearance = appearance2;
            this.ultraTextEditor10.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor10.Location = new System.Drawing.Point(136, 132);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.ReadOnly = true;
            this.ultraTextEditor10.Size = new System.Drawing.Size(496, 21);
            this.ultraTextEditor10.TabIndex = 111;
            // 
            // ultraTextEditor9
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Appearance = appearance54;
            this.ultraTextEditor9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Location = new System.Drawing.Point(136, 108);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.ReadOnly = true;
            this.ultraTextEditor9.Size = new System.Drawing.Size(496, 21);
            this.ultraTextEditor9.TabIndex = 109;
            // 
            // ultraTextEditor14
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Appearance = appearance55;
            this.ultraTextEditor14.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor14.Location = new System.Drawing.Point(824, 84);
            this.ultraTextEditor14.Name = "ultraTextEditor14";
            this.ultraTextEditor14.ReadOnly = true;
            this.ultraTextEditor14.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor14.TabIndex = 105;
            // 
            // ultraTextEditor13
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Appearance = appearance60;
            this.ultraTextEditor13.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor13.Location = new System.Drawing.Point(824, 60);
            this.ultraTextEditor13.Name = "ultraTextEditor13";
            this.ultraTextEditor13.ReadOnly = true;
            this.ultraTextEditor13.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor13.TabIndex = 104;
            // 
            // ultraTextEditor12
            // 
            appearance61.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Appearance = appearance61;
            this.ultraTextEditor12.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor12.Location = new System.Drawing.Point(492, 84);
            this.ultraTextEditor12.Name = "ultraTextEditor12";
            this.ultraTextEditor12.ReadOnly = true;
            this.ultraTextEditor12.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor12.TabIndex = 106;
            // 
            // ultraTextEditor11
            // 
            appearance62.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Appearance = appearance62;
            this.ultraTextEditor11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Location = new System.Drawing.Point(492, 60);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.ReadOnly = true;
            this.ultraTextEditor11.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor11.TabIndex = 110;
            // 
            // ultraTextEditor8
            // 
            appearance63.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Appearance = appearance63;
            this.ultraTextEditor8.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor8.Location = new System.Drawing.Point(136, 84);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.ReadOnly = true;
            this.ultraTextEditor8.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor8.TabIndex = 108;
            // 
            // ultraTextEditor7
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Appearance = appearance56;
            this.ultraTextEditor7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Location = new System.Drawing.Point(136, 60);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.ReadOnly = true;
            this.ultraTextEditor7.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor7.TabIndex = 107;
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(368, 60);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(120, 20);
            this.uLabel7.TabIndex = 113;
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(368, 84);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(120, 20);
            this.uLabel10.TabIndex = 120;
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(700, 108);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(120, 20);
            this.uLabel14.TabIndex = 114;
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(700, 60);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(120, 20);
            this.uLabel8.TabIndex = 115;
            // 
            // uLabel12
            // 
            this.uLabel12.Location = new System.Drawing.Point(12, 108);
            this.uLabel12.Name = "uLabel12";
            this.uLabel12.Size = new System.Drawing.Size(120, 20);
            this.uLabel12.TabIndex = 116;
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(12, 132);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(120, 20);
            this.uLabel13.TabIndex = 117;
            // 
            // uLabel11
            // 
            this.uLabel11.Location = new System.Drawing.Point(700, 84);
            this.uLabel11.Name = "uLabel11";
            this.uLabel11.Size = new System.Drawing.Size(120, 20);
            this.uLabel11.TabIndex = 118;
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(12, 84);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(120, 20);
            this.uLabel9.TabIndex = 119;
            // 
            // ultraTextEditor6
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Appearance = appearance57;
            this.ultraTextEditor6.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor6.Location = new System.Drawing.Point(492, 36);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.ReadOnly = true;
            this.ultraTextEditor6.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor6.TabIndex = 79;
            // 
            // ultraTextEditor19
            // 
            appearance74.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor19.Appearance = appearance74;
            this.ultraTextEditor19.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor19.Location = new System.Drawing.Point(492, 12);
            this.ultraTextEditor19.Name = "ultraTextEditor19";
            this.ultraTextEditor19.ReadOnly = true;
            this.ultraTextEditor19.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor19.TabIndex = 79;
            // 
            // ultraTextEditor5
            // 
            appearance71.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Appearance = appearance71;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Location = new System.Drawing.Point(240, 36);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor5.TabIndex = 79;
            // 
            // ultraTextEditor4
            // 
            appearance75.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Appearance = appearance75;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Location = new System.Drawing.Point(136, 36);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 79;
            // 
            // ultraTextEditor3
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Appearance = appearance53;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Location = new System.Drawing.Point(136, 13);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.ReadOnly = true;
            this.ultraTextEditor3.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor3.TabIndex = 79;
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(12, 12);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(120, 20);
            this.uLabel1.TabIndex = 89;
            this.uLabel1.Text = "1";
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(368, 36);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(120, 20);
            this.uLabel5.TabIndex = 92;
            this.uLabel5.Text = "5";
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(12, 36);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(120, 20);
            this.uLabel4.TabIndex = 91;
            this.uLabel4.Text = "4";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(368, 12);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(120, 20);
            this.uLabel3.TabIndex = 90;
            this.uLabel3.Text = "3";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance4;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 98);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 720);
            this.uGrid1.TabIndex = 7;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor1);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraComboEditor2);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraComboEditor1);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 38);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // ultraTextEditor2
            // 
            appearance66.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance66;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(656, 36);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(140, 21);
            this.ultraTextEditor2.TabIndex = 79;
            // 
            // ultraTextEditor1
            // 
            appearance67.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance67.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance67;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor1.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor1.Location = new System.Drawing.Point(508, 36);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.Size = new System.Drawing.Size(144, 21);
            this.ultraTextEditor1.TabIndex = 79;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(384, 36);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(120, 20);
            this.ultraLabel3.TabIndex = 78;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(12, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(120, 20);
            this.ultraLabel2.TabIndex = 78;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(384, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(120, 20);
            this.ultraLabel1.TabIndex = 78;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(120, 20);
            this.uLabelPlant.TabIndex = 78;
            this.uLabelPlant.Text = "공장";
            // 
            // ultraComboEditor2
            // 
            this.ultraComboEditor2.Location = new System.Drawing.Point(136, 36);
            this.ultraComboEditor2.Name = "ultraComboEditor2";
            this.ultraComboEditor2.Size = new System.Drawing.Size(144, 21);
            this.ultraComboEditor2.TabIndex = 77;
            this.ultraComboEditor2.Text = "ultraComboEditor1";
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.Location = new System.Drawing.Point(508, 12);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(144, 21);
            this.ultraComboEditor1.TabIndex = 77;
            this.ultraComboEditor1.Text = "ultraComboEditor1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(136, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboPlant.TabIndex = 77;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 5;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQATZ0003
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0003";
            this.Load += new System.EventHandler(this.frmQATZ0003_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0003_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckE4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheck2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.Misc.UltraLabel uLabel21;
        private Infragistics.Win.Misc.UltraLabel uLabel20;
        private Infragistics.Win.Misc.UltraLabel uLabel19;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor16;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck5;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckE4;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.Misc.UltraLabel uLabel16;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor15;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck3;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck2;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.Misc.UltraLabel uLabel12;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.Misc.UltraLabel uLabel11;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
    }
}