﻿namespace QRPQAT.UI
{
    partial class frmQATZ0015
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0015));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchCheckDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchCheckDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchCheckDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGridStaticCheck = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxDetail = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInsEtcItem20 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem19 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem18 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem17 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem16 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem15 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem14 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem13 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem12 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInsEtcItem1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextInsResultFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInsResultFIle = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel43 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel42 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel41 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel26 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel27 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel73 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel72 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel71 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel59 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel58 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel57 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel24 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel70 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel56 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel40 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel25 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel09 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel08 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel23 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel69 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsResItem3 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel55 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel39 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel68 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsResItem2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel54 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel38 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel67 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsResItem1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel53 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel37 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsWorkTableItem4 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel52 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel36 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsWorkTableItem3 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel66 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel51 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel35 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel65 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsWorkTableItem2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel50 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel34 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel64 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsWorkTableItem1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel49 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel33 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel07 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel63 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsEquipItem5 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel48 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel32 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel62 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsEquipItem4 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel47 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel31 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel61 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsEquipItem3 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel46 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel30 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsEquipItem2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel45 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel29 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInsEquipItem1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabel60 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel44 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel28 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel06 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel05 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel04 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel03 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel02 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel01 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEuipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCheckDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCheckDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCheckUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCheckUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCheckUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextManageNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelManageNo = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchCheckDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchCheckDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridStaticCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDetail)).BeginInit();
            this.uGroupBoxDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsResultFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsResItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsResItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsResItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCheckDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCheckUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCheckUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManageNo)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchCheckDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchCheckDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCheckDate);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(112, 12);
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(148, 21);
            this.uComboSearchEquipLoc.TabIndex = 28;
            this.uComboSearchEquipLoc.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(8, 12);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 27;
            this.uLabelSearchEquipLoc.Text = "측정위치";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(868, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 23;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(764, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 22;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uDateSearchCheckDateTo
            // 
            this.uDateSearchCheckDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchCheckDateTo.Location = new System.Drawing.Point(492, 12);
            this.uDateSearchCheckDateTo.Name = "uDateSearchCheckDateTo";
            this.uDateSearchCheckDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchCheckDateTo.TabIndex = 18;
            // 
            // ultraLabel
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel.Appearance = appearance4;
            this.ultraLabel.Location = new System.Drawing.Point(476, 12);
            this.ultraLabel.Name = "ultraLabel";
            this.ultraLabel.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel.TabIndex = 17;
            this.ultraLabel.Text = "~";
            // 
            // uDateSearchCheckDateFrom
            // 
            this.uDateSearchCheckDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchCheckDateFrom.Location = new System.Drawing.Point(372, 12);
            this.uDateSearchCheckDateFrom.Name = "uDateSearchCheckDateFrom";
            this.uDateSearchCheckDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchCheckDateFrom.TabIndex = 16;
            // 
            // uLabelSearchCheckDate
            // 
            this.uLabelSearchCheckDate.Location = new System.Drawing.Point(268, 12);
            this.uLabelSearchCheckDate.Name = "uLabelSearchCheckDate";
            this.uLabelSearchCheckDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCheckDate.TabIndex = 15;
            this.uLabelSearchCheckDate.Text = "점검일자";
            // 
            // uGridStaticCheck
            // 
            this.uGridStaticCheck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridStaticCheck.DisplayLayout.Appearance = appearance5;
            this.uGridStaticCheck.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridStaticCheck.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridStaticCheck.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridStaticCheck.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridStaticCheck.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridStaticCheck.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridStaticCheck.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridStaticCheck.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridStaticCheck.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridStaticCheck.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridStaticCheck.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridStaticCheck.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridStaticCheck.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridStaticCheck.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridStaticCheck.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridStaticCheck.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridStaticCheck.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridStaticCheck.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridStaticCheck.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridStaticCheck.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridStaticCheck.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridStaticCheck.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridStaticCheck.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridStaticCheck.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridStaticCheck.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridStaticCheck.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridStaticCheck.Location = new System.Drawing.Point(0, 80);
            this.uGridStaticCheck.Name = "uGridStaticCheck";
            this.uGridStaticCheck.Size = new System.Drawing.Size(1070, 760);
            this.uGridStaticCheck.TabIndex = 3;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 4;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxDetail);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxDetail
            // 
            this.uGroupBoxDetail.Controls.Add(this.uButtonFileDown);
            this.uGroupBoxDetail.Controls.Add(this.uButtonDelete);
            this.uGroupBoxDetail.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxDetail.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem20);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem19);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem18);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem17);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem16);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem15);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem14);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem13);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem12);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem11);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem10);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem9);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem8);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem7);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem6);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem5);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem4);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem3);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem2);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsEtcItem1);
            this.uGroupBoxDetail.Controls.Add(this.uGridDetail);
            this.uGroupBoxDetail.Controls.Add(this.uTextInsResultFile);
            this.uGroupBoxDetail.Controls.Add(this.uLabelInsResultFIle);
            this.uGroupBoxDetail.Controls.Add(this.uLabel43);
            this.uGroupBoxDetail.Controls.Add(this.uLabel42);
            this.uGroupBoxDetail.Controls.Add(this.uLabel41);
            this.uGroupBoxDetail.Controls.Add(this.uLabel26);
            this.uGroupBoxDetail.Controls.Add(this.uLabel27);
            this.uGroupBoxDetail.Controls.Add(this.uLabel73);
            this.uGroupBoxDetail.Controls.Add(this.uLabel72);
            this.uGroupBoxDetail.Controls.Add(this.uLabel71);
            this.uGroupBoxDetail.Controls.Add(this.uLabel59);
            this.uGroupBoxDetail.Controls.Add(this.uLabel58);
            this.uGroupBoxDetail.Controls.Add(this.uLabel57);
            this.uGroupBoxDetail.Controls.Add(this.uLabel24);
            this.uGroupBoxDetail.Controls.Add(this.uLabel70);
            this.uGroupBoxDetail.Controls.Add(this.uLabel56);
            this.uGroupBoxDetail.Controls.Add(this.uLabel40);
            this.uGroupBoxDetail.Controls.Add(this.uLabel25);
            this.uGroupBoxDetail.Controls.Add(this.uLabel13);
            this.uGroupBoxDetail.Controls.Add(this.uLabel12);
            this.uGroupBoxDetail.Controls.Add(this.uLabel11);
            this.uGroupBoxDetail.Controls.Add(this.uLabel10);
            this.uGroupBoxDetail.Controls.Add(this.uLabel09);
            this.uGroupBoxDetail.Controls.Add(this.uLabel08);
            this.uGroupBoxDetail.Controls.Add(this.uLabel23);
            this.uGroupBoxDetail.Controls.Add(this.uLabel69);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsResItem3);
            this.uGroupBoxDetail.Controls.Add(this.uLabel55);
            this.uGroupBoxDetail.Controls.Add(this.uLabel39);
            this.uGroupBoxDetail.Controls.Add(this.uLabel22);
            this.uGroupBoxDetail.Controls.Add(this.uLabel68);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsResItem2);
            this.uGroupBoxDetail.Controls.Add(this.uLabel54);
            this.uGroupBoxDetail.Controls.Add(this.uLabel38);
            this.uGroupBoxDetail.Controls.Add(this.uLabel21);
            this.uGroupBoxDetail.Controls.Add(this.uLabel67);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsResItem1);
            this.uGroupBoxDetail.Controls.Add(this.uLabel53);
            this.uGroupBoxDetail.Controls.Add(this.uLabel37);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsWorkTableItem4);
            this.uGroupBoxDetail.Controls.Add(this.uLabel52);
            this.uGroupBoxDetail.Controls.Add(this.uLabel36);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsWorkTableItem3);
            this.uGroupBoxDetail.Controls.Add(this.uLabel66);
            this.uGroupBoxDetail.Controls.Add(this.uLabel51);
            this.uGroupBoxDetail.Controls.Add(this.uLabel35);
            this.uGroupBoxDetail.Controls.Add(this.uLabel20);
            this.uGroupBoxDetail.Controls.Add(this.uLabel19);
            this.uGroupBoxDetail.Controls.Add(this.uLabel65);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsWorkTableItem2);
            this.uGroupBoxDetail.Controls.Add(this.uLabel50);
            this.uGroupBoxDetail.Controls.Add(this.uLabel34);
            this.uGroupBoxDetail.Controls.Add(this.uLabel18);
            this.uGroupBoxDetail.Controls.Add(this.uLabel64);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsWorkTableItem1);
            this.uGroupBoxDetail.Controls.Add(this.uLabel49);
            this.uGroupBoxDetail.Controls.Add(this.uLabel33);
            this.uGroupBoxDetail.Controls.Add(this.uLabel07);
            this.uGroupBoxDetail.Controls.Add(this.uLabel17);
            this.uGroupBoxDetail.Controls.Add(this.uLabel63);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsEquipItem5);
            this.uGroupBoxDetail.Controls.Add(this.uLabel48);
            this.uGroupBoxDetail.Controls.Add(this.uLabel32);
            this.uGroupBoxDetail.Controls.Add(this.uLabel16);
            this.uGroupBoxDetail.Controls.Add(this.uLabel62);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsEquipItem4);
            this.uGroupBoxDetail.Controls.Add(this.uLabel47);
            this.uGroupBoxDetail.Controls.Add(this.uLabel31);
            this.uGroupBoxDetail.Controls.Add(this.uLabel15);
            this.uGroupBoxDetail.Controls.Add(this.uLabel61);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsEquipItem3);
            this.uGroupBoxDetail.Controls.Add(this.uLabel46);
            this.uGroupBoxDetail.Controls.Add(this.uLabel30);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsEquipItem2);
            this.uGroupBoxDetail.Controls.Add(this.uLabel45);
            this.uGroupBoxDetail.Controls.Add(this.uLabel29);
            this.uGroupBoxDetail.Controls.Add(this.uComboInsEquipItem1);
            this.uGroupBoxDetail.Controls.Add(this.uLabel60);
            this.uGroupBoxDetail.Controls.Add(this.uLabel44);
            this.uGroupBoxDetail.Controls.Add(this.uLabel28);
            this.uGroupBoxDetail.Controls.Add(this.uLabel14);
            this.uGroupBoxDetail.Controls.Add(this.uLabel06);
            this.uGroupBoxDetail.Controls.Add(this.uLabel05);
            this.uGroupBoxDetail.Controls.Add(this.uLabel04);
            this.uGroupBoxDetail.Controls.Add(this.uLabel03);
            this.uGroupBoxDetail.Controls.Add(this.uLabel02);
            this.uGroupBoxDetail.Controls.Add(this.uLabel01);
            this.uGroupBoxDetail.Controls.Add(this.uTextSpec);
            this.uGroupBoxDetail.Controls.Add(this.uLabelSpec);
            this.uGroupBoxDetail.Controls.Add(this.uTextPlantCode);
            this.uGroupBoxDetail.Controls.Add(this.uLabelPlant);
            this.uGroupBoxDetail.Controls.Add(this.uComboEquipLoc);
            this.uGroupBoxDetail.Controls.Add(this.uLabelEuipLoc);
            this.uGroupBoxDetail.Controls.Add(this.uDateCheckDate);
            this.uGroupBoxDetail.Controls.Add(this.uLabelCheckDate);
            this.uGroupBoxDetail.Controls.Add(this.uTextCheckUserName);
            this.uGroupBoxDetail.Controls.Add(this.uTextCheckUserID);
            this.uGroupBoxDetail.Controls.Add(this.uLabelCheckUser);
            this.uGroupBoxDetail.Controls.Add(this.uTextManageNo);
            this.uGroupBoxDetail.Controls.Add(this.uLabelManageNo);
            this.uGroupBoxDetail.Location = new System.Drawing.Point(4, 4);
            this.uGroupBoxDetail.Name = "uGroupBoxDetail";
            this.uGroupBoxDetail.Size = new System.Drawing.Size(1052, 648);
            this.uGroupBoxDetail.TabIndex = 0;
            this.uGroupBoxDetail.Text = "ultraGroupBox1";
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(12, 536);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDown.TabIndex = 167;
            this.uButtonFileDown.Text = "다운";
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 508);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 166;
            this.uButtonDelete.Text = "행삭제";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 620);
            this.uTextEtcDesc.MaxLength = 50;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(448, 21);
            this.uTextEtcDesc.TabIndex = 165;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 620);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtcDesc.TabIndex = 164;
            this.uLabelEtcDesc.Text = "비고";
            // 
            // uTextInsEtcItem20
            // 
            this.uTextInsEtcItem20.Location = new System.Drawing.Point(960, 460);
            this.uTextInsEtcItem20.MaxLength = 50;
            this.uTextInsEtcItem20.Name = "uTextInsEtcItem20";
            this.uTextInsEtcItem20.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem20.TabIndex = 163;
            // 
            // uTextInsEtcItem19
            // 
            this.uTextInsEtcItem19.Location = new System.Drawing.Point(872, 460);
            this.uTextInsEtcItem19.MaxLength = 50;
            this.uTextInsEtcItem19.Name = "uTextInsEtcItem19";
            this.uTextInsEtcItem19.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem19.TabIndex = 162;
            // 
            // uTextInsEtcItem18
            // 
            this.uTextInsEtcItem18.Location = new System.Drawing.Point(784, 460);
            this.uTextInsEtcItem18.MaxLength = 50;
            this.uTextInsEtcItem18.Name = "uTextInsEtcItem18";
            this.uTextInsEtcItem18.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem18.TabIndex = 161;
            // 
            // uTextInsEtcItem17
            // 
            this.uTextInsEtcItem17.Location = new System.Drawing.Point(696, 460);
            this.uTextInsEtcItem17.MaxLength = 50;
            this.uTextInsEtcItem17.Name = "uTextInsEtcItem17";
            this.uTextInsEtcItem17.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem17.TabIndex = 160;
            // 
            // uTextInsEtcItem16
            // 
            this.uTextInsEtcItem16.Location = new System.Drawing.Point(608, 460);
            this.uTextInsEtcItem16.MaxLength = 50;
            this.uTextInsEtcItem16.Name = "uTextInsEtcItem16";
            this.uTextInsEtcItem16.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem16.TabIndex = 159;
            // 
            // uTextInsEtcItem15
            // 
            this.uTextInsEtcItem15.Location = new System.Drawing.Point(960, 436);
            this.uTextInsEtcItem15.MaxLength = 50;
            this.uTextInsEtcItem15.Name = "uTextInsEtcItem15";
            this.uTextInsEtcItem15.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem15.TabIndex = 158;
            // 
            // uTextInsEtcItem14
            // 
            this.uTextInsEtcItem14.Location = new System.Drawing.Point(872, 436);
            this.uTextInsEtcItem14.MaxLength = 50;
            this.uTextInsEtcItem14.Name = "uTextInsEtcItem14";
            this.uTextInsEtcItem14.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem14.TabIndex = 157;
            // 
            // uTextInsEtcItem13
            // 
            this.uTextInsEtcItem13.Location = new System.Drawing.Point(784, 436);
            this.uTextInsEtcItem13.MaxLength = 50;
            this.uTextInsEtcItem13.Name = "uTextInsEtcItem13";
            this.uTextInsEtcItem13.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem13.TabIndex = 156;
            // 
            // uTextInsEtcItem12
            // 
            this.uTextInsEtcItem12.Location = new System.Drawing.Point(696, 436);
            this.uTextInsEtcItem12.MaxLength = 50;
            this.uTextInsEtcItem12.Name = "uTextInsEtcItem12";
            this.uTextInsEtcItem12.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem12.TabIndex = 155;
            // 
            // uTextInsEtcItem11
            // 
            this.uTextInsEtcItem11.Location = new System.Drawing.Point(608, 436);
            this.uTextInsEtcItem11.MaxLength = 50;
            this.uTextInsEtcItem11.Name = "uTextInsEtcItem11";
            this.uTextInsEtcItem11.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem11.TabIndex = 154;
            // 
            // uTextInsEtcItem10
            // 
            this.uTextInsEtcItem10.Location = new System.Drawing.Point(960, 412);
            this.uTextInsEtcItem10.MaxLength = 50;
            this.uTextInsEtcItem10.Name = "uTextInsEtcItem10";
            this.uTextInsEtcItem10.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem10.TabIndex = 153;
            // 
            // uTextInsEtcItem9
            // 
            this.uTextInsEtcItem9.Location = new System.Drawing.Point(872, 412);
            this.uTextInsEtcItem9.MaxLength = 50;
            this.uTextInsEtcItem9.Name = "uTextInsEtcItem9";
            this.uTextInsEtcItem9.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem9.TabIndex = 152;
            // 
            // uTextInsEtcItem8
            // 
            this.uTextInsEtcItem8.Location = new System.Drawing.Point(784, 412);
            this.uTextInsEtcItem8.MaxLength = 50;
            this.uTextInsEtcItem8.Name = "uTextInsEtcItem8";
            this.uTextInsEtcItem8.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem8.TabIndex = 151;
            // 
            // uTextInsEtcItem7
            // 
            this.uTextInsEtcItem7.Location = new System.Drawing.Point(696, 412);
            this.uTextInsEtcItem7.MaxLength = 50;
            this.uTextInsEtcItem7.Name = "uTextInsEtcItem7";
            this.uTextInsEtcItem7.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem7.TabIndex = 150;
            // 
            // uTextInsEtcItem6
            // 
            this.uTextInsEtcItem6.Location = new System.Drawing.Point(608, 412);
            this.uTextInsEtcItem6.MaxLength = 50;
            this.uTextInsEtcItem6.Name = "uTextInsEtcItem6";
            this.uTextInsEtcItem6.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem6.TabIndex = 149;
            // 
            // uTextInsEtcItem5
            // 
            this.uTextInsEtcItem5.Location = new System.Drawing.Point(960, 388);
            this.uTextInsEtcItem5.MaxLength = 50;
            this.uTextInsEtcItem5.Name = "uTextInsEtcItem5";
            this.uTextInsEtcItem5.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem5.TabIndex = 148;
            // 
            // uTextInsEtcItem4
            // 
            this.uTextInsEtcItem4.Location = new System.Drawing.Point(872, 388);
            this.uTextInsEtcItem4.MaxLength = 50;
            this.uTextInsEtcItem4.Name = "uTextInsEtcItem4";
            this.uTextInsEtcItem4.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem4.TabIndex = 147;
            // 
            // uTextInsEtcItem3
            // 
            this.uTextInsEtcItem3.Location = new System.Drawing.Point(784, 388);
            this.uTextInsEtcItem3.MaxLength = 50;
            this.uTextInsEtcItem3.Name = "uTextInsEtcItem3";
            this.uTextInsEtcItem3.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem3.TabIndex = 146;
            // 
            // uTextInsEtcItem2
            // 
            this.uTextInsEtcItem2.Location = new System.Drawing.Point(696, 388);
            this.uTextInsEtcItem2.MaxLength = 50;
            this.uTextInsEtcItem2.Name = "uTextInsEtcItem2";
            this.uTextInsEtcItem2.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem2.TabIndex = 145;
            // 
            // uTextInsEtcItem1
            // 
            this.uTextInsEtcItem1.Location = new System.Drawing.Point(608, 388);
            this.uTextInsEtcItem1.MaxLength = 50;
            this.uTextInsEtcItem1.Name = "uTextInsEtcItem1";
            this.uTextInsEtcItem1.Size = new System.Drawing.Size(84, 21);
            this.uTextInsEtcItem1.TabIndex = 144;
            // 
            // uGridDetail
            // 
            this.uGridDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetail.DisplayLayout.Appearance = appearance19;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.uGridDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.uGridDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetail.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetail.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.uGridDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetail.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetail.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.uGridDetail.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.uGridDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetail.DisplayLayout.Override.RowAppearance = appearance32;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetail.Location = new System.Drawing.Point(104, 508);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(944, 104);
            this.uGridDetail.TabIndex = 143;
            this.uGridDetail.Text = "ultraGrid1";
            // 
            // uTextInsResultFile
            // 
            editorButton1.Key = "Up";
            editorButton2.Key = "Down";
            this.uTextInsResultFile.ButtonsRight.Add(editorButton1);
            this.uTextInsResultFile.ButtonsRight.Add(editorButton2);
            this.uTextInsResultFile.Location = new System.Drawing.Point(220, 484);
            this.uTextInsResultFile.MaxLength = 50;
            this.uTextInsResultFile.Name = "uTextInsResultFile";
            this.uTextInsResultFile.Size = new System.Drawing.Size(470, 21);
            this.uTextInsResultFile.TabIndex = 142;
            // 
            // uLabelInsResultFIle
            // 
            this.uLabelInsResultFIle.Location = new System.Drawing.Point(12, 484);
            this.uLabelInsResultFIle.Name = "uLabelInsResultFIle";
            this.uLabelInsResultFIle.Size = new System.Drawing.Size(204, 20);
            this.uLabelInsResultFIle.TabIndex = 141;
            this.uLabelInsResultFIle.Text = "점검 결과 첨부";
            // 
            // uLabel43
            // 
            this.uLabel43.Location = new System.Drawing.Point(220, 460);
            this.uLabel43.Name = "uLabel43";
            this.uLabel43.Size = new System.Drawing.Size(180, 20);
            this.uLabel43.TabIndex = 140;
            this.uLabel43.Text = "≥1x10^5,＜3.5x10^7Ω";
            // 
            // uLabel42
            // 
            this.uLabel42.Location = new System.Drawing.Point(220, 436);
            this.uLabel42.Name = "uLabel42";
            this.uLabel42.Size = new System.Drawing.Size(180, 20);
            this.uLabel42.TabIndex = 139;
            this.uLabel42.Text = "≥1x10^5,＜1x10^8Ω";
            // 
            // uLabel41
            // 
            this.uLabel41.Location = new System.Drawing.Point(220, 412);
            this.uLabel41.Name = "uLabel41";
            this.uLabel41.Size = new System.Drawing.Size(180, 20);
            this.uLabel41.TabIndex = 138;
            this.uLabel41.Text = "≥1x10^5,＜1x10^11Ω";
            // 
            // uLabel26
            // 
            this.uLabel26.Location = new System.Drawing.Point(116, 436);
            this.uLabel26.Name = "uLabel26";
            this.uLabel26.Size = new System.Drawing.Size(100, 20);
            this.uLabel26.TabIndex = 137;
            this.uLabel26.Text = "체적저항";
            // 
            // uLabel27
            // 
            this.uLabel27.Location = new System.Drawing.Point(116, 460);
            this.uLabel27.Name = "uLabel27";
            this.uLabel27.Size = new System.Drawing.Size(100, 20);
            this.uLabel27.TabIndex = 136;
            this.uLabel27.Text = "접지저항";
            // 
            // uLabel73
            // 
            this.uLabel73.Location = new System.Drawing.Point(476, 460);
            this.uLabel73.Name = "uLabel73";
            this.uLabel73.Size = new System.Drawing.Size(125, 20);
            this.uLabel73.TabIndex = 135;
            this.uLabel73.Text = "5명/공정";
            // 
            // uLabel72
            // 
            this.uLabel72.Location = new System.Drawing.Point(476, 436);
            this.uLabel72.Name = "uLabel72";
            this.uLabel72.Size = new System.Drawing.Size(125, 20);
            this.uLabel72.TabIndex = 134;
            this.uLabel72.Text = "5명/공정";
            // 
            // uLabel71
            // 
            this.uLabel71.Location = new System.Drawing.Point(476, 412);
            this.uLabel71.Name = "uLabel71";
            this.uLabel71.Size = new System.Drawing.Size(125, 20);
            this.uLabel71.TabIndex = 133;
            this.uLabel71.Text = "5명/공정";
            // 
            // uLabel59
            // 
            this.uLabel59.Location = new System.Drawing.Point(404, 460);
            this.uLabel59.Name = "uLabel59";
            this.uLabel59.Size = new System.Drawing.Size(65, 20);
            this.uLabel59.TabIndex = 131;
            this.uLabel59.Text = "1회/월";
            // 
            // uLabel58
            // 
            this.uLabel58.Location = new System.Drawing.Point(404, 436);
            this.uLabel58.Name = "uLabel58";
            this.uLabel58.Size = new System.Drawing.Size(65, 20);
            this.uLabel58.TabIndex = 130;
            this.uLabel58.Text = "1회/월";
            // 
            // uLabel57
            // 
            this.uLabel57.Location = new System.Drawing.Point(404, 412);
            this.uLabel57.Name = "uLabel57";
            this.uLabel57.Size = new System.Drawing.Size(65, 20);
            this.uLabel57.TabIndex = 129;
            this.uLabel57.Text = "1회/월";
            // 
            // uLabel24
            // 
            this.uLabel24.Location = new System.Drawing.Point(116, 388);
            this.uLabel24.Name = "uLabel24";
            this.uLabel24.Size = new System.Drawing.Size(100, 20);
            this.uLabel24.TabIndex = 128;
            this.uLabel24.Text = "정전기";
            // 
            // uLabel70
            // 
            this.uLabel70.Location = new System.Drawing.Point(476, 388);
            this.uLabel70.Name = "uLabel70";
            this.uLabel70.Size = new System.Drawing.Size(125, 20);
            this.uLabel70.TabIndex = 127;
            this.uLabel70.Text = "5명/공정";
            // 
            // uLabel56
            // 
            this.uLabel56.Location = new System.Drawing.Point(404, 388);
            this.uLabel56.Name = "uLabel56";
            this.uLabel56.Size = new System.Drawing.Size(65, 20);
            this.uLabel56.TabIndex = 126;
            this.uLabel56.Text = "1회/월";
            // 
            // uLabel40
            // 
            this.uLabel40.Location = new System.Drawing.Point(220, 388);
            this.uLabel40.Name = "uLabel40";
            this.uLabel40.Size = new System.Drawing.Size(180, 20);
            this.uLabel40.TabIndex = 125;
            this.uLabel40.Text = "±200V";
            // 
            // uLabel25
            // 
            this.uLabel25.Location = new System.Drawing.Point(116, 412);
            this.uLabel25.Name = "uLabel25";
            this.uLabel25.Size = new System.Drawing.Size(100, 20);
            this.uLabel25.TabIndex = 124;
            this.uLabel25.Text = "표면저항";
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(12, 460);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(100, 20);
            this.uLabel13.TabIndex = 122;
            this.uLabel13.Text = "손목접지띠";
            // 
            // uLabel12
            // 
            this.uLabel12.Location = new System.Drawing.Point(12, 436);
            this.uLabel12.Name = "uLabel12";
            this.uLabel12.Size = new System.Drawing.Size(100, 20);
            this.uLabel12.TabIndex = 121;
            this.uLabel12.Text = "제전화";
            // 
            // uLabel11
            // 
            this.uLabel11.Location = new System.Drawing.Point(12, 388);
            this.uLabel11.Name = "uLabel11";
            this.uLabel11.Size = new System.Drawing.Size(100, 44);
            this.uLabel11.TabIndex = 120;
            this.uLabel11.Text = "방진복";
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(12, 364);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(100, 20);
            this.uLabel10.TabIndex = 119;
            this.uLabel10.Text = "도전타일";
            // 
            // uLabel09
            // 
            this.uLabel09.Location = new System.Drawing.Point(12, 340);
            this.uLabel09.Name = "uLabel09";
            this.uLabel09.Size = new System.Drawing.Size(100, 20);
            this.uLabel09.TabIndex = 118;
            this.uLabel09.Text = "의자";
            // 
            // uLabel08
            // 
            this.uLabel08.Location = new System.Drawing.Point(12, 316);
            this.uLabel08.Name = "uLabel08";
            this.uLabel08.Size = new System.Drawing.Size(100, 20);
            this.uLabel08.TabIndex = 117;
            this.uLabel08.Text = "대차";
            // 
            // uLabel23
            // 
            this.uLabel23.Location = new System.Drawing.Point(116, 364);
            this.uLabel23.Name = "uLabel23";
            this.uLabel23.Size = new System.Drawing.Size(100, 20);
            this.uLabel23.TabIndex = 116;
            this.uLabel23.Text = "표면저항";
            // 
            // uLabel69
            // 
            this.uLabel69.Location = new System.Drawing.Point(476, 364);
            this.uLabel69.Name = "uLabel69";
            this.uLabel69.Size = new System.Drawing.Size(125, 20);
            this.uLabel69.TabIndex = 115;
            this.uLabel69.Text = "5point/공정";
            // 
            // uComboInsResItem3
            // 
            this.uComboInsResItem3.Location = new System.Drawing.Point(608, 364);
            this.uComboInsResItem3.Name = "uComboInsResItem3";
            this.uComboInsResItem3.Size = new System.Drawing.Size(100, 21);
            this.uComboInsResItem3.TabIndex = 114;
            this.uComboInsResItem3.Text = "ultraComboEditor10";
            // 
            // uLabel55
            // 
            this.uLabel55.Location = new System.Drawing.Point(404, 364);
            this.uLabel55.Name = "uLabel55";
            this.uLabel55.Size = new System.Drawing.Size(65, 20);
            this.uLabel55.TabIndex = 113;
            this.uLabel55.Text = "1회/월";
            // 
            // uLabel39
            // 
            this.uLabel39.Location = new System.Drawing.Point(220, 364);
            this.uLabel39.Name = "uLabel39";
            this.uLabel39.Size = new System.Drawing.Size(180, 20);
            this.uLabel39.TabIndex = 112;
            this.uLabel39.Text = "≥1x10^5,＜1x10^9Ω";
            // 
            // uLabel22
            // 
            this.uLabel22.Location = new System.Drawing.Point(116, 340);
            this.uLabel22.Name = "uLabel22";
            this.uLabel22.Size = new System.Drawing.Size(100, 20);
            this.uLabel22.TabIndex = 111;
            this.uLabel22.Text = "접지저항";
            // 
            // uLabel68
            // 
            this.uLabel68.Location = new System.Drawing.Point(476, 340);
            this.uLabel68.Name = "uLabel68";
            this.uLabel68.Size = new System.Drawing.Size(125, 20);
            this.uLabel68.TabIndex = 110;
            this.uLabel68.Text = "10%/공정별 총수량";
            // 
            // uComboInsResItem2
            // 
            this.uComboInsResItem2.Location = new System.Drawing.Point(608, 340);
            this.uComboInsResItem2.Name = "uComboInsResItem2";
            this.uComboInsResItem2.Size = new System.Drawing.Size(100, 21);
            this.uComboInsResItem2.TabIndex = 109;
            this.uComboInsResItem2.Text = "ultraComboEditor11";
            // 
            // uLabel54
            // 
            this.uLabel54.Location = new System.Drawing.Point(404, 340);
            this.uLabel54.Name = "uLabel54";
            this.uLabel54.Size = new System.Drawing.Size(65, 20);
            this.uLabel54.TabIndex = 108;
            this.uLabel54.Text = "1회/월";
            // 
            // uLabel38
            // 
            this.uLabel38.Location = new System.Drawing.Point(220, 340);
            this.uLabel38.Name = "uLabel38";
            this.uLabel38.Size = new System.Drawing.Size(180, 20);
            this.uLabel38.TabIndex = 107;
            this.uLabel38.Text = "≥1x10^5,＜1x10^9Ω";
            // 
            // uLabel21
            // 
            this.uLabel21.Location = new System.Drawing.Point(116, 316);
            this.uLabel21.Name = "uLabel21";
            this.uLabel21.Size = new System.Drawing.Size(100, 20);
            this.uLabel21.TabIndex = 106;
            this.uLabel21.Text = "접지저항";
            // 
            // uLabel67
            // 
            this.uLabel67.Location = new System.Drawing.Point(476, 316);
            this.uLabel67.Name = "uLabel67";
            this.uLabel67.Size = new System.Drawing.Size(125, 20);
            this.uLabel67.TabIndex = 105;
            this.uLabel67.Text = "10%/공정별 총수량";
            // 
            // uComboInsResItem1
            // 
            this.uComboInsResItem1.Location = new System.Drawing.Point(608, 316);
            this.uComboInsResItem1.Name = "uComboInsResItem1";
            this.uComboInsResItem1.Size = new System.Drawing.Size(100, 21);
            this.uComboInsResItem1.TabIndex = 104;
            this.uComboInsResItem1.Text = "ultraComboEditor12";
            // 
            // uLabel53
            // 
            this.uLabel53.Location = new System.Drawing.Point(404, 316);
            this.uLabel53.Name = "uLabel53";
            this.uLabel53.Size = new System.Drawing.Size(65, 20);
            this.uLabel53.TabIndex = 103;
            this.uLabel53.Text = "1회/월";
            // 
            // uLabel37
            // 
            this.uLabel37.Location = new System.Drawing.Point(220, 316);
            this.uLabel37.Name = "uLabel37";
            this.uLabel37.Size = new System.Drawing.Size(180, 20);
            this.uLabel37.TabIndex = 102;
            this.uLabel37.Text = "≥1x10^5,＜1x10^9Ω";
            // 
            // uComboInsWorkTableItem4
            // 
            this.uComboInsWorkTableItem4.Location = new System.Drawing.Point(608, 292);
            this.uComboInsWorkTableItem4.Name = "uComboInsWorkTableItem4";
            this.uComboInsWorkTableItem4.Size = new System.Drawing.Size(100, 21);
            this.uComboInsWorkTableItem4.TabIndex = 101;
            this.uComboInsWorkTableItem4.Text = "ultraComboEditor6";
            // 
            // uLabel52
            // 
            this.uLabel52.Location = new System.Drawing.Point(404, 292);
            this.uLabel52.Name = "uLabel52";
            this.uLabel52.Size = new System.Drawing.Size(65, 20);
            this.uLabel52.TabIndex = 100;
            this.uLabel52.Text = "1회/월";
            // 
            // uLabel36
            // 
            this.uLabel36.Location = new System.Drawing.Point(220, 292);
            this.uLabel36.Name = "uLabel36";
            this.uLabel36.Size = new System.Drawing.Size(180, 20);
            this.uLabel36.TabIndex = 99;
            this.uLabel36.Text = "Balance : ±50V";
            // 
            // uComboInsWorkTableItem3
            // 
            this.uComboInsWorkTableItem3.Location = new System.Drawing.Point(608, 268);
            this.uComboInsWorkTableItem3.Name = "uComboInsWorkTableItem3";
            this.uComboInsWorkTableItem3.Size = new System.Drawing.Size(100, 21);
            this.uComboInsWorkTableItem3.TabIndex = 98;
            this.uComboInsWorkTableItem3.Text = "ultraComboEditor9";
            // 
            // uLabel66
            // 
            this.uLabel66.Location = new System.Drawing.Point(476, 268);
            this.uLabel66.Name = "uLabel66";
            this.uLabel66.Size = new System.Drawing.Size(125, 44);
            this.uLabel66.TabIndex = 97;
            this.uLabel66.Text = "10%/공정별 총수량";
            // 
            // uLabel51
            // 
            this.uLabel51.Location = new System.Drawing.Point(404, 268);
            this.uLabel51.Name = "uLabel51";
            this.uLabel51.Size = new System.Drawing.Size(65, 20);
            this.uLabel51.TabIndex = 96;
            this.uLabel51.Text = "1회/월";
            // 
            // uLabel35
            // 
            this.uLabel35.Location = new System.Drawing.Point(220, 268);
            this.uLabel35.Name = "uLabel35";
            this.uLabel35.Size = new System.Drawing.Size(180, 20);
            this.uLabel35.TabIndex = 95;
            this.uLabel35.Text = "Decay Time : 10s 이하";
            // 
            // uLabel20
            // 
            this.uLabel20.Location = new System.Drawing.Point(116, 268);
            this.uLabel20.Name = "uLabel20";
            this.uLabel20.Size = new System.Drawing.Size(100, 44);
            this.uLabel20.TabIndex = 94;
            this.uLabel20.Text = "Ionizer";
            // 
            // uLabel19
            // 
            this.uLabel19.Location = new System.Drawing.Point(116, 244);
            this.uLabel19.Name = "uLabel19";
            this.uLabel19.Size = new System.Drawing.Size(100, 20);
            this.uLabel19.TabIndex = 93;
            this.uLabel19.Text = "정전기";
            // 
            // uLabel65
            // 
            this.uLabel65.Location = new System.Drawing.Point(476, 244);
            this.uLabel65.Name = "uLabel65";
            this.uLabel65.Size = new System.Drawing.Size(125, 20);
            this.uLabel65.TabIndex = 92;
            this.uLabel65.Text = "10%/공정별 총수량";
            // 
            // uComboInsWorkTableItem2
            // 
            this.uComboInsWorkTableItem2.Location = new System.Drawing.Point(608, 244);
            this.uComboInsWorkTableItem2.Name = "uComboInsWorkTableItem2";
            this.uComboInsWorkTableItem2.Size = new System.Drawing.Size(100, 21);
            this.uComboInsWorkTableItem2.TabIndex = 91;
            this.uComboInsWorkTableItem2.Text = "ultraComboEditor7";
            // 
            // uLabel50
            // 
            this.uLabel50.Location = new System.Drawing.Point(404, 244);
            this.uLabel50.Name = "uLabel50";
            this.uLabel50.Size = new System.Drawing.Size(65, 20);
            this.uLabel50.TabIndex = 90;
            this.uLabel50.Text = "1회/월";
            // 
            // uLabel34
            // 
            this.uLabel34.Location = new System.Drawing.Point(220, 244);
            this.uLabel34.Name = "uLabel34";
            this.uLabel34.Size = new System.Drawing.Size(180, 20);
            this.uLabel34.TabIndex = 89;
            this.uLabel34.Text = "±200V";
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(116, 220);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(100, 20);
            this.uLabel18.TabIndex = 88;
            this.uLabel18.Text = "접지저항";
            // 
            // uLabel64
            // 
            this.uLabel64.Location = new System.Drawing.Point(476, 220);
            this.uLabel64.Name = "uLabel64";
            this.uLabel64.Size = new System.Drawing.Size(125, 20);
            this.uLabel64.TabIndex = 87;
            this.uLabel64.Text = "10%/공정별 총수량";
            // 
            // uComboInsWorkTableItem1
            // 
            this.uComboInsWorkTableItem1.Location = new System.Drawing.Point(608, 220);
            this.uComboInsWorkTableItem1.Name = "uComboInsWorkTableItem1";
            this.uComboInsWorkTableItem1.Size = new System.Drawing.Size(100, 21);
            this.uComboInsWorkTableItem1.TabIndex = 86;
            this.uComboInsWorkTableItem1.Text = "ultraComboEditor8";
            // 
            // uLabel49
            // 
            this.uLabel49.Location = new System.Drawing.Point(404, 220);
            this.uLabel49.Name = "uLabel49";
            this.uLabel49.Size = new System.Drawing.Size(65, 20);
            this.uLabel49.TabIndex = 85;
            this.uLabel49.Text = "1회/월";
            // 
            // uLabel33
            // 
            this.uLabel33.Location = new System.Drawing.Point(220, 220);
            this.uLabel33.Name = "uLabel33";
            this.uLabel33.Size = new System.Drawing.Size(180, 20);
            this.uLabel33.TabIndex = 84;
            this.uLabel33.Text = "≥1x10^5,＜1x10^9Ω";
            // 
            // uLabel07
            // 
            this.uLabel07.Location = new System.Drawing.Point(12, 220);
            this.uLabel07.Name = "uLabel07";
            this.uLabel07.Size = new System.Drawing.Size(100, 92);
            this.uLabel07.TabIndex = 83;
            this.uLabel07.Text = "Work Table";
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(116, 196);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(100, 20);
            this.uLabel17.TabIndex = 82;
            this.uLabel17.Text = "정전기";
            // 
            // uLabel63
            // 
            this.uLabel63.Location = new System.Drawing.Point(476, 196);
            this.uLabel63.Name = "uLabel63";
            this.uLabel63.Size = new System.Drawing.Size(125, 20);
            this.uLabel63.TabIndex = 81;
            this.uLabel63.Text = "5point/공정";
            // 
            // uComboInsEquipItem5
            // 
            this.uComboInsEquipItem5.Location = new System.Drawing.Point(608, 196);
            this.uComboInsEquipItem5.Name = "uComboInsEquipItem5";
            this.uComboInsEquipItem5.Size = new System.Drawing.Size(100, 21);
            this.uComboInsEquipItem5.TabIndex = 80;
            this.uComboInsEquipItem5.Text = "ultraComboEditor5";
            // 
            // uLabel48
            // 
            this.uLabel48.Location = new System.Drawing.Point(404, 196);
            this.uLabel48.Name = "uLabel48";
            this.uLabel48.Size = new System.Drawing.Size(65, 20);
            this.uLabel48.TabIndex = 79;
            this.uLabel48.Text = "1회/월";
            // 
            // uLabel32
            // 
            this.uLabel32.Location = new System.Drawing.Point(220, 196);
            this.uLabel32.Name = "uLabel32";
            this.uLabel32.Size = new System.Drawing.Size(180, 20);
            this.uLabel32.TabIndex = 78;
            this.uLabel32.Text = "±200V 이내 (마찰)";
            // 
            // uLabel16
            // 
            this.uLabel16.Location = new System.Drawing.Point(116, 172);
            this.uLabel16.Name = "uLabel16";
            this.uLabel16.Size = new System.Drawing.Size(100, 20);
            this.uLabel16.TabIndex = 77;
            this.uLabel16.Text = "누설전류";
            // 
            // uLabel62
            // 
            this.uLabel62.Location = new System.Drawing.Point(476, 172);
            this.uLabel62.Name = "uLabel62";
            this.uLabel62.Size = new System.Drawing.Size(125, 20);
            this.uLabel62.TabIndex = 76;
            this.uLabel62.Text = "5point/공정";
            // 
            // uComboInsEquipItem4
            // 
            this.uComboInsEquipItem4.Location = new System.Drawing.Point(608, 172);
            this.uComboInsEquipItem4.Name = "uComboInsEquipItem4";
            this.uComboInsEquipItem4.Size = new System.Drawing.Size(100, 21);
            this.uComboInsEquipItem4.TabIndex = 75;
            this.uComboInsEquipItem4.Text = "ultraComboEditor4";
            // 
            // uLabel47
            // 
            this.uLabel47.Location = new System.Drawing.Point(404, 172);
            this.uLabel47.Name = "uLabel47";
            this.uLabel47.Size = new System.Drawing.Size(65, 20);
            this.uLabel47.TabIndex = 74;
            this.uLabel47.Text = "1회/월";
            // 
            // uLabel31
            // 
            this.uLabel31.Location = new System.Drawing.Point(220, 172);
            this.uLabel31.Name = "uLabel31";
            this.uLabel31.Size = new System.Drawing.Size(180, 20);
            this.uLabel31.TabIndex = 73;
            this.uLabel31.Text = "5V (AC) 이하";
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(116, 148);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(100, 20);
            this.uLabel15.TabIndex = 72;
            this.uLabel15.Text = "접지저항";
            // 
            // uLabel61
            // 
            this.uLabel61.Location = new System.Drawing.Point(476, 148);
            this.uLabel61.Name = "uLabel61";
            this.uLabel61.Size = new System.Drawing.Size(125, 20);
            this.uLabel61.TabIndex = 71;
            this.uLabel61.Text = "5point/공정";
            // 
            // uComboInsEquipItem3
            // 
            this.uComboInsEquipItem3.Location = new System.Drawing.Point(608, 148);
            this.uComboInsEquipItem3.Name = "uComboInsEquipItem3";
            this.uComboInsEquipItem3.Size = new System.Drawing.Size(100, 21);
            this.uComboInsEquipItem3.TabIndex = 70;
            this.uComboInsEquipItem3.Text = "ultraComboEditor3";
            // 
            // uLabel46
            // 
            this.uLabel46.Location = new System.Drawing.Point(404, 148);
            this.uLabel46.Name = "uLabel46";
            this.uLabel46.Size = new System.Drawing.Size(65, 20);
            this.uLabel46.TabIndex = 69;
            this.uLabel46.Text = "1회/월";
            // 
            // uLabel30
            // 
            this.uLabel30.Location = new System.Drawing.Point(220, 148);
            this.uLabel30.Name = "uLabel30";
            this.uLabel30.Size = new System.Drawing.Size(180, 20);
            this.uLabel30.TabIndex = 68;
            this.uLabel30.Text = "1Ω 이하 (인접설비 25Ω이하)";
            // 
            // uComboInsEquipItem2
            // 
            this.uComboInsEquipItem2.Location = new System.Drawing.Point(608, 124);
            this.uComboInsEquipItem2.Name = "uComboInsEquipItem2";
            this.uComboInsEquipItem2.Size = new System.Drawing.Size(100, 21);
            this.uComboInsEquipItem2.TabIndex = 67;
            this.uComboInsEquipItem2.Text = "ultraComboEditor2";
            // 
            // uLabel45
            // 
            this.uLabel45.Location = new System.Drawing.Point(404, 124);
            this.uLabel45.Name = "uLabel45";
            this.uLabel45.Size = new System.Drawing.Size(65, 20);
            this.uLabel45.TabIndex = 66;
            this.uLabel45.Text = "1회/월";
            // 
            // uLabel29
            // 
            this.uLabel29.Location = new System.Drawing.Point(220, 124);
            this.uLabel29.Name = "uLabel29";
            this.uLabel29.Size = new System.Drawing.Size(180, 20);
            this.uLabel29.TabIndex = 65;
            this.uLabel29.Text = "Balance : ±50V";
            // 
            // uComboInsEquipItem1
            // 
            this.uComboInsEquipItem1.Location = new System.Drawing.Point(608, 100);
            this.uComboInsEquipItem1.Name = "uComboInsEquipItem1";
            this.uComboInsEquipItem1.Size = new System.Drawing.Size(100, 21);
            this.uComboInsEquipItem1.TabIndex = 64;
            this.uComboInsEquipItem1.Text = "ultraComboEditor1";
            // 
            // uLabel60
            // 
            this.uLabel60.Location = new System.Drawing.Point(476, 100);
            this.uLabel60.Name = "uLabel60";
            this.uLabel60.Size = new System.Drawing.Size(125, 44);
            this.uLabel60.TabIndex = 63;
            this.uLabel60.Text = "5point/공정";
            // 
            // uLabel44
            // 
            this.uLabel44.Location = new System.Drawing.Point(404, 100);
            this.uLabel44.Name = "uLabel44";
            this.uLabel44.Size = new System.Drawing.Size(65, 20);
            this.uLabel44.TabIndex = 62;
            this.uLabel44.Text = "1회/월";
            // 
            // uLabel28
            // 
            this.uLabel28.Location = new System.Drawing.Point(220, 100);
            this.uLabel28.Name = "uLabel28";
            this.uLabel28.Size = new System.Drawing.Size(180, 20);
            this.uLabel28.TabIndex = 61;
            this.uLabel28.Text = "Decay Time : 5s 이하";
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(116, 100);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(100, 44);
            this.uLabel14.TabIndex = 60;
            this.uLabel14.Text = "Ionizer";
            // 
            // uLabel06
            // 
            this.uLabel06.Location = new System.Drawing.Point(12, 100);
            this.uLabel06.Name = "uLabel06";
            this.uLabel06.Size = new System.Drawing.Size(100, 116);
            this.uLabel06.TabIndex = 59;
            this.uLabel06.Text = "설비";
            // 
            // uLabel05
            // 
            this.uLabel05.Location = new System.Drawing.Point(608, 76);
            this.uLabel05.Name = "uLabel05";
            this.uLabel05.Size = new System.Drawing.Size(100, 20);
            this.uLabel05.TabIndex = 58;
            this.uLabel05.Text = "결과";
            // 
            // uLabel04
            // 
            this.uLabel04.Location = new System.Drawing.Point(476, 76);
            this.uLabel04.Name = "uLabel04";
            this.uLabel04.Size = new System.Drawing.Size(125, 20);
            this.uLabel04.TabIndex = 57;
            this.uLabel04.Text = "점검수";
            // 
            // uLabel03
            // 
            this.uLabel03.Location = new System.Drawing.Point(404, 76);
            this.uLabel03.Name = "uLabel03";
            this.uLabel03.Size = new System.Drawing.Size(65, 20);
            this.uLabel03.TabIndex = 56;
            this.uLabel03.Text = "주기";
            // 
            // uLabel02
            // 
            this.uLabel02.Location = new System.Drawing.Point(220, 76);
            this.uLabel02.Name = "uLabel02";
            this.uLabel02.Size = new System.Drawing.Size(180, 20);
            this.uLabel02.TabIndex = 55;
            this.uLabel02.Text = "점검기준";
            // 
            // uLabel01
            // 
            this.uLabel01.Location = new System.Drawing.Point(12, 76);
            this.uLabel01.Name = "uLabel01";
            this.uLabel01.Size = new System.Drawing.Size(204, 20);
            this.uLabel01.TabIndex = 54;
            this.uLabel01.Text = "점검항목";
            // 
            // uTextSpec
            // 
            this.uTextSpec.Location = new System.Drawing.Point(368, 48);
            this.uTextSpec.MaxLength = 50;
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.Size = new System.Drawing.Size(224, 21);
            this.uTextSpec.TabIndex = 53;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(264, 48);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpec.TabIndex = 52;
            this.uLabelSpec.Text = "관련Spec";
            // 
            // uTextPlantCode
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance17;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(700, 48);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 50;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(596, 48);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 49;
            this.uLabelPlant.Text = "공장";
            // 
            // uComboEquipLoc
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboEquipLoc.Appearance = appearance3;
            this.uComboEquipLoc.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboEquipLoc.Location = new System.Drawing.Point(116, 48);
            this.uComboEquipLoc.MaxLength = 10;
            this.uComboEquipLoc.Name = "uComboEquipLoc";
            this.uComboEquipLoc.Size = new System.Drawing.Size(144, 21);
            this.uComboEquipLoc.TabIndex = 48;
            this.uComboEquipLoc.Text = "ultraComboEditor1";
            // 
            // uLabelEuipLoc
            // 
            this.uLabelEuipLoc.Location = new System.Drawing.Point(12, 48);
            this.uLabelEuipLoc.Name = "uLabelEuipLoc";
            this.uLabelEuipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEuipLoc.TabIndex = 47;
            this.uLabelEuipLoc.Text = "측정위치";
            // 
            // uDateCheckDate
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCheckDate.Appearance = appearance18;
            this.uDateCheckDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCheckDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCheckDate.Location = new System.Drawing.Point(700, 24);
            this.uDateCheckDate.Name = "uDateCheckDate";
            this.uDateCheckDate.Size = new System.Drawing.Size(200, 21);
            this.uDateCheckDate.TabIndex = 46;
            // 
            // uLabelCheckDate
            // 
            this.uLabelCheckDate.Location = new System.Drawing.Point(596, 24);
            this.uLabelCheckDate.Name = "uLabelCheckDate";
            this.uLabelCheckDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCheckDate.TabIndex = 45;
            this.uLabelCheckDate.Text = "점검일자";
            // 
            // uTextCheckUserName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCheckUserName.Appearance = appearance25;
            this.uTextCheckUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCheckUserName.Location = new System.Drawing.Point(470, 24);
            this.uTextCheckUserName.Name = "uTextCheckUserName";
            this.uTextCheckUserName.ReadOnly = true;
            this.uTextCheckUserName.Size = new System.Drawing.Size(122, 21);
            this.uTextCheckUserName.TabIndex = 44;
            // 
            // uTextCheckUserID
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCheckUserID.Appearance = appearance26;
            this.uTextCheckUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance27.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance27.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance27;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCheckUserID.ButtonsRight.Add(editorButton3);
            this.uTextCheckUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCheckUserID.Location = new System.Drawing.Point(368, 24);
            this.uTextCheckUserID.MaxLength = 20;
            this.uTextCheckUserID.Name = "uTextCheckUserID";
            this.uTextCheckUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCheckUserID.TabIndex = 43;
            // 
            // uLabelCheckUser
            // 
            this.uLabelCheckUser.Location = new System.Drawing.Point(264, 24);
            this.uLabelCheckUser.Name = "uLabelCheckUser";
            this.uLabelCheckUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelCheckUser.TabIndex = 42;
            this.uLabelCheckUser.Text = "점검자";
            // 
            // uTextManageNo
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextManageNo.Appearance = appearance2;
            this.uTextManageNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextManageNo.Location = new System.Drawing.Point(116, 24);
            this.uTextManageNo.Name = "uTextManageNo";
            this.uTextManageNo.ReadOnly = true;
            this.uTextManageNo.Size = new System.Drawing.Size(144, 21);
            this.uTextManageNo.TabIndex = 41;
            // 
            // uLabelManageNo
            // 
            this.uLabelManageNo.Location = new System.Drawing.Point(12, 24);
            this.uLabelManageNo.Name = "uLabelManageNo";
            this.uLabelManageNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelManageNo.TabIndex = 40;
            this.uLabelManageNo.Text = "관리번호";
            // 
            // frmQATZ0015
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGridStaticCheck);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0015";
            this.Text = "frmQATZ0015";
            this.Load += new System.EventHandler(this.frmQATZ0015_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0015_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmQATZ0015_FormClosed);
            this.Resize += new System.EventHandler(this.frmQATZ0015_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchCheckDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchCheckDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridStaticCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDetail)).EndInit();
            this.uGroupBoxDetail.ResumeLayout(false);
            this.uGroupBoxDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsEtcItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInsResultFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsResItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsResItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsResItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsWorkTableItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInsEquipItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCheckDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCheckUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCheckUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManageNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchCheckDateTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchCheckDateFrom;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCheckDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridStaticCheck;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxDetail;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelEuipLoc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCheckDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCheckDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCheckUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCheckUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCheckUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextManageNo;
        private Infragistics.Win.Misc.UltraLabel uLabelManageNo;
        private Infragistics.Win.Misc.UltraLabel uLabel04;
        private Infragistics.Win.Misc.UltraLabel uLabel03;
        private Infragistics.Win.Misc.UltraLabel uLabel02;
        private Infragistics.Win.Misc.UltraLabel uLabel01;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.Misc.UltraLabel uLabel45;
        private Infragistics.Win.Misc.UltraLabel uLabel29;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsEquipItem1;
        private Infragistics.Win.Misc.UltraLabel uLabel60;
        private Infragistics.Win.Misc.UltraLabel uLabel44;
        private Infragistics.Win.Misc.UltraLabel uLabel28;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.Misc.UltraLabel uLabel06;
        private Infragistics.Win.Misc.UltraLabel uLabel05;
        private Infragistics.Win.Misc.UltraLabel uLabel16;
        private Infragistics.Win.Misc.UltraLabel uLabel62;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsEquipItem4;
        private Infragistics.Win.Misc.UltraLabel uLabel47;
        private Infragistics.Win.Misc.UltraLabel uLabel31;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.Misc.UltraLabel uLabel61;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsEquipItem3;
        private Infragistics.Win.Misc.UltraLabel uLabel46;
        private Infragistics.Win.Misc.UltraLabel uLabel30;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsEquipItem2;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.Misc.UltraLabel uLabel63;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsEquipItem5;
        private Infragistics.Win.Misc.UltraLabel uLabel48;
        private Infragistics.Win.Misc.UltraLabel uLabel32;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsWorkTableItem4;
        private Infragistics.Win.Misc.UltraLabel uLabel52;
        private Infragistics.Win.Misc.UltraLabel uLabel36;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsWorkTableItem3;
        private Infragistics.Win.Misc.UltraLabel uLabel66;
        private Infragistics.Win.Misc.UltraLabel uLabel51;
        private Infragistics.Win.Misc.UltraLabel uLabel35;
        private Infragistics.Win.Misc.UltraLabel uLabel20;
        private Infragistics.Win.Misc.UltraLabel uLabel19;
        private Infragistics.Win.Misc.UltraLabel uLabel65;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsWorkTableItem2;
        private Infragistics.Win.Misc.UltraLabel uLabel50;
        private Infragistics.Win.Misc.UltraLabel uLabel34;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.Misc.UltraLabel uLabel64;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsWorkTableItem1;
        private Infragistics.Win.Misc.UltraLabel uLabel49;
        private Infragistics.Win.Misc.UltraLabel uLabel33;
        private Infragistics.Win.Misc.UltraLabel uLabel07;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.Misc.UltraLabel uLabel09;
        private Infragistics.Win.Misc.UltraLabel uLabel08;
        private Infragistics.Win.Misc.UltraLabel uLabel23;
        private Infragistics.Win.Misc.UltraLabel uLabel69;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsResItem3;
        private Infragistics.Win.Misc.UltraLabel uLabel55;
        private Infragistics.Win.Misc.UltraLabel uLabel39;
        private Infragistics.Win.Misc.UltraLabel uLabel22;
        private Infragistics.Win.Misc.UltraLabel uLabel68;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsResItem2;
        private Infragistics.Win.Misc.UltraLabel uLabel54;
        private Infragistics.Win.Misc.UltraLabel uLabel38;
        private Infragistics.Win.Misc.UltraLabel uLabel21;
        private Infragistics.Win.Misc.UltraLabel uLabel67;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInsResItem1;
        private Infragistics.Win.Misc.UltraLabel uLabel53;
        private Infragistics.Win.Misc.UltraLabel uLabel37;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.Misc.UltraLabel uLabel12;
        private Infragistics.Win.Misc.UltraLabel uLabel11;
        private Infragistics.Win.Misc.UltraLabel uLabel24;
        private Infragistics.Win.Misc.UltraLabel uLabel70;
        private Infragistics.Win.Misc.UltraLabel uLabel56;
        private Infragistics.Win.Misc.UltraLabel uLabel40;
        private Infragistics.Win.Misc.UltraLabel uLabel25;
        private Infragistics.Win.Misc.UltraLabel uLabel26;
        private Infragistics.Win.Misc.UltraLabel uLabel27;
        private Infragistics.Win.Misc.UltraLabel uLabel73;
        private Infragistics.Win.Misc.UltraLabel uLabel72;
        private Infragistics.Win.Misc.UltraLabel uLabel71;
        private Infragistics.Win.Misc.UltraLabel uLabel59;
        private Infragistics.Win.Misc.UltraLabel uLabel58;
        private Infragistics.Win.Misc.UltraLabel uLabel57;
        private Infragistics.Win.Misc.UltraLabel uLabel43;
        private Infragistics.Win.Misc.UltraLabel uLabel42;
        private Infragistics.Win.Misc.UltraLabel uLabel41;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsResultFile;
        private Infragistics.Win.Misc.UltraLabel uLabelInsResultFIle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem18;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem15;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem7;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInsEtcItem3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
    }
}