﻿namespace QRPQAT.UI
{
    partial class frmQATZ0007
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0007));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchWriteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchWriteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridReliabilityList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxAppraisal = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPassFailFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectFilaName = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionPassFailFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGroupBoxAppraiseCondition = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateAccept = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uCheckAcceptFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextAcceptUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSolderBallModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAcceptUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHASTCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSolderBallVender = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPCTCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHTSDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHumidityDate2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHASTDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPCTDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTCDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReflowDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcceptComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAcceptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAcceptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextHumidityDate1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextBakeDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTCOptionDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHTSTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHumidityTime2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHASTTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPCTTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTCTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReflowTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHumidityTime1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextBakeTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTCOptionTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboHTSCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboHumidityCondition2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboHASTCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPCTCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboTCCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboReflowCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboHumidityCondition1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboBakeCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboTCOptionCondition = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboHTSFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboHumidityFlag2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboHASTFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPCTFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboTCFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboReflowFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboHumidityFlag1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboBakeFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboTCOptionFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelschedule = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgressTime = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCondition = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelYesNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelHTS = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelHumidity = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelHAST = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPCT = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTC = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPreReflow = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPreHumidity = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPreBakeOven = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPreTC = new Infragistics.Win.Misc.UltraLabel();
            this.ulabelPreCondition = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTestName = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxRequest = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uNumReqQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelReqQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductPN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSolderBallModelorPlatingComposition = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductLN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEMCVender = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEMCVender = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEMCModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEMCModel = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWireVender = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWireVender = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWireModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWireModel = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLForPCBVender = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLFPCBVender = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLForPCBModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLFPCBModel = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdhesive_Vender = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAdhesiveVender = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdhesive_Model = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAdhesiveModel = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChip_Size = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChipSize = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSolderBallVenderorPlatingSubcontractor = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxStdInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChangeAfter = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChangeAfter = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChangeBefore = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChangeBefore = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAppraisalObject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAppraisalObject = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReliabilityNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReliabilityNo = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridReliabilityList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAppraisal)).BeginInit();
            this.uGroupBoxAppraisal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAppraiseCondition)).BeginInit();
            this.uGroupBoxAppraiseCondition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAcceptFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSolderBallModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHASTCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSolderBallVender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCTCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHTSDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHASTDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCTDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReflowDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBakeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCOptionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHTSTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHASTTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCTTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReflowTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityTime1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBakeTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCOptionTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHTSCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityCondition2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHASTCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPCTCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReflowCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityCondition1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboBakeCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCOptionCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHTSFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityFlag2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHASTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPCTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReflowFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityFlag1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboBakeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCOptionFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRequest)).BeginInit();
            this.uGroupBoxRequest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumReqQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductLN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMCVender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMCModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWireVender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWireModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLForPCBVender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLForPCBModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdhesive_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdhesive_Model)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChip_Size)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).BeginInit();
            this.uGroupBoxStdInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeAfter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeBefore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAppraisalObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReliabilityNo)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchWriteToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchWriteFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProduct);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uDateSearchWriteToDate
            // 
            this.uDateSearchWriteToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchWriteToDate.Location = new System.Drawing.Point(202, 12);
            this.uDateSearchWriteToDate.Name = "uDateSearchWriteToDate";
            this.uDateSearchWriteToDate.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchWriteToDate.TabIndex = 14;
            // 
            // ultraLabel1
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance4;
            this.ultraLabel1.Location = new System.Drawing.Point(189, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(10, 20);
            this.ultraLabel1.TabIndex = 13;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchWriteFromDate
            // 
            this.uDateSearchWriteFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchWriteFromDate.Location = new System.Drawing.Point(99, 12);
            this.uDateSearchWriteFromDate.Name = "uDateSearchWriteFromDate";
            this.uDateSearchWriteFromDate.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchWriteFromDate.TabIndex = 12;
            // 
            // uLabelSearchWriteDate
            // 
            this.uLabelSearchWriteDate.Location = new System.Drawing.Point(10, 12);
            this.uLabelSearchWriteDate.Name = "uLabelSearchWriteDate";
            this.uLabelSearchWriteDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchWriteDate.TabIndex = 11;
            // 
            // uTextSearchProductName
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Appearance = appearance20;
            this.uTextSearchProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Location = new System.Drawing.Point(878, 12);
            this.uTextSearchProductName.Name = "uTextSearchProductName";
            this.uTextSearchProductName.ReadOnly = true;
            this.uTextSearchProductName.Size = new System.Drawing.Size(29, 21);
            this.uTextSearchProductName.TabIndex = 10;
            this.uTextSearchProductName.Visible = false;
            // 
            // uTextSearchProductCode
            // 
            appearance21.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance21.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance21;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchProductCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchProductCode.Location = new System.Drawing.Point(847, 12);
            this.uTextSearchProductCode.Name = "uTextSearchProductCode";
            this.uTextSearchProductCode.Size = new System.Drawing.Size(27, 21);
            this.uTextSearchProductCode.TabIndex = 9;
            this.uTextSearchProductCode.Visible = false;
            this.uTextSearchProductCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchProductCode_KeyDown);
            this.uTextSearchProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchProductCode_EditorButtonClick);
            // 
            // uLabelSearchProduct
            // 
            this.uLabelSearchProduct.Location = new System.Drawing.Point(823, 12);
            this.uLabelSearchProduct.Name = "uLabelSearchProduct";
            this.uLabelSearchProduct.Size = new System.Drawing.Size(17, 20);
            this.uLabelSearchProduct.TabIndex = 8;
            this.uLabelSearchProduct.Text = "ultraLabel1";
            this.uLabelSearchProduct.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(782, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(31, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(761, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(17, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridReliabilityList
            // 
            this.uGridReliabilityList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridReliabilityList.DisplayLayout.Appearance = appearance2;
            this.uGridReliabilityList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridReliabilityList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReliabilityList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReliabilityList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.uGridReliabilityList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReliabilityList.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGridReliabilityList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridReliabilityList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridReliabilityList.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridReliabilityList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridReliabilityList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridReliabilityList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGridReliabilityList.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridReliabilityList.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridReliabilityList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridReliabilityList.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReliabilityList.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.uGridReliabilityList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridReliabilityList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridReliabilityList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGridReliabilityList.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGridReliabilityList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridReliabilityList.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.uGridReliabilityList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridReliabilityList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridReliabilityList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridReliabilityList.Location = new System.Drawing.Point(0, 80);
            this.uGridReliabilityList.Name = "uGridReliabilityList";
            this.uGridReliabilityList.Size = new System.Drawing.Size(909, 740);
            this.uGridReliabilityList.TabIndex = 2;
            this.uGridReliabilityList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridReliabilityList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(909, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(909, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxAppraisal);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxAppraiseCondition);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxRequest);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxStdInfo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(903, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxAppraisal
            // 
            this.uGroupBoxAppraisal.Controls.Add(this.uLabelInspectUser);
            this.uGroupBoxAppraisal.Controls.Add(this.uTextInspectFileName);
            this.uGroupBoxAppraisal.Controls.Add(this.uLabelComplete);
            this.uGroupBoxAppraisal.Controls.Add(this.uLabelPassFailFlag);
            this.uGroupBoxAppraisal.Controls.Add(this.uLabelInspectFilaName);
            this.uGroupBoxAppraisal.Controls.Add(this.uOptionPassFailFlag);
            this.uGroupBoxAppraisal.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBoxAppraisal.Controls.Add(this.uTextInspectUserID);
            this.uGroupBoxAppraisal.Controls.Add(this.uTextInspectUserName);
            this.uGroupBoxAppraisal.Controls.Add(this.uLabelInspectDate);
            this.uGroupBoxAppraisal.Controls.Add(this.uDateInspectDate);
            this.uGroupBoxAppraisal.Location = new System.Drawing.Point(10, 612);
            this.uGroupBoxAppraisal.Name = "uGroupBoxAppraisal";
            this.uGroupBoxAppraisal.Size = new System.Drawing.Size(888, 76);
            this.uGroupBoxAppraisal.TabIndex = 188;
            // 
            // uLabelInspectUser
            // 
            this.uLabelInspectUser.Location = new System.Drawing.Point(10, 28);
            this.uLabelInspectUser.Name = "uLabelInspectUser";
            this.uLabelInspectUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelInspectUser.TabIndex = 87;
            // 
            // uTextInspectFileName
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectFileName.Appearance = appearance53;
            this.uTextInspectFileName.BackColor = System.Drawing.Color.Gainsboro;
            appearance43.Image = global::QRPQAT.UI.Properties.Resources.btn_Fileupload;
            appearance43.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance43;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Key = "UP";
            appearance26.Image = global::QRPQAT.UI.Properties.Resources.btn_Filedownload;
            appearance26.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance26;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Key = "DOWN";
            this.uTextInspectFileName.ButtonsRight.Add(editorButton2);
            this.uTextInspectFileName.ButtonsRight.Add(editorButton3);
            this.uTextInspectFileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectFileName.Location = new System.Drawing.Point(545, 48);
            this.uTextInspectFileName.Name = "uTextInspectFileName";
            this.uTextInspectFileName.ReadOnly = true;
            this.uTextInspectFileName.Size = new System.Drawing.Size(333, 21);
            this.uTextInspectFileName.TabIndex = 165;
            this.uTextInspectFileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectFileName_EditorButtonClick);
            // 
            // uLabelComplete
            // 
            this.uLabelComplete.Location = new System.Drawing.Point(278, 52);
            this.uLabelComplete.Name = "uLabelComplete";
            this.uLabelComplete.Size = new System.Drawing.Size(86, 20);
            this.uLabelComplete.TabIndex = 188;
            // 
            // uLabelPassFailFlag
            // 
            this.uLabelPassFailFlag.Location = new System.Drawing.Point(10, 52);
            this.uLabelPassFailFlag.Name = "uLabelPassFailFlag";
            this.uLabelPassFailFlag.Size = new System.Drawing.Size(86, 20);
            this.uLabelPassFailFlag.TabIndex = 68;
            // 
            // uLabelInspectFilaName
            // 
            this.uLabelInspectFilaName.Location = new System.Drawing.Point(456, 48);
            this.uLabelInspectFilaName.Name = "uLabelInspectFilaName";
            this.uLabelInspectFilaName.Size = new System.Drawing.Size(86, 20);
            this.uLabelInspectFilaName.TabIndex = 164;
            // 
            // uOptionPassFailFlag
            // 
            this.uOptionPassFailFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionPassFailFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem1.DataValue = "OK";
            valueListItem1.DisplayText = "합격";
            valueListItem2.DataValue = "NG";
            valueListItem2.DisplayText = "불합격";
            this.uOptionPassFailFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionPassFailFlag.Location = new System.Drawing.Point(99, 56);
            this.uOptionPassFailFlag.Name = "uOptionPassFailFlag";
            this.uOptionPassFailFlag.Size = new System.Drawing.Size(123, 16);
            this.uOptionPassFailFlag.TabIndex = 86;
            this.uOptionPassFailFlag.TextIndentation = 2;
            this.uOptionPassFailFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(367, 52);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(14, 20);
            this.uCheckCompleteFlag.TabIndex = 187;
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedValueChanged);
            // 
            // uTextInspectUserID
            // 
            appearance16.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance16.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance16;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton4);
            this.uTextInspectUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(99, 28);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextInspectUserID.TabIndex = 88;
            this.uTextInspectUserID.ValueChanged += new System.EventHandler(this.uTextInspectUserID_ValueChanged);
            this.uTextInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextInspectUserID_KeyDown);
            this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectUserID_EditorButtonClick);
            // 
            // uTextInspectUserName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance15;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(187, 28);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextInspectUserName.TabIndex = 89;
            // 
            // uLabelInspectDate
            // 
            this.uLabelInspectDate.Location = new System.Drawing.Point(278, 28);
            this.uLabelInspectDate.Name = "uLabelInspectDate";
            this.uLabelInspectDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelInspectDate.TabIndex = 90;
            // 
            // uDateInspectDate
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDate.Appearance = appearance18;
            this.uDateInspectDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateInspectDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateInspectDate.Location = new System.Drawing.Point(367, 28);
            this.uDateInspectDate.Name = "uDateInspectDate";
            this.uDateInspectDate.Size = new System.Drawing.Size(86, 21);
            this.uDateInspectDate.TabIndex = 91;
            // 
            // uGroupBoxAppraiseCondition
            // 
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uDateAccept);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uCheckAcceptFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextAcceptUserName);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextSolderBallModel);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextAcceptUserID);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHASTCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextSolderBallVender);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextPCTCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHTSDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHumidityDate2);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHASTDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextPCTDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextTCDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextReflowDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelAcceptComplete);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelAcceptDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelAcceptUser);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHumidityDate1);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextBakeDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextTCOptionDate);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHTSTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHumidityTime2);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHASTTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextPCTTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextTCTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextProductName);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextReflowTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextHumidityTime1);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextBakeTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextCustomerName);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextTCOptionTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHTSCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHumidityCondition2);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uTextProductCode);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHASTCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboPCTCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboTCCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboReflowCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHumidityCondition1);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboBakeCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboTCOptionCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHTSFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHumidityFlag2);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHASTFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboPCTFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboTCFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboReflowFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboHumidityFlag1);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboBakeFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uComboTCOptionFlag);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelschedule);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelProgressTime);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelYesNo);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelHTS);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelHumidity);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelHAST);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelPCT);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelTC);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelPreReflow);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelPreHumidity);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelPreBakeOven);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelPreTC);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.ulabelPreCondition);
            this.uGroupBoxAppraiseCondition.Controls.Add(this.uLabelTestName);
            this.uGroupBoxAppraiseCondition.Location = new System.Drawing.Point(10, 316);
            this.uGroupBoxAppraiseCondition.Name = "uGroupBoxAppraiseCondition";
            this.uGroupBoxAppraiseCondition.Size = new System.Drawing.Size(887, 292);
            this.uGroupBoxAppraiseCondition.TabIndex = 28;
            this.uGroupBoxAppraiseCondition.Text = "uGroupBoxAppraiseCondition";
            // 
            // uDateAccept
            // 
            this.uDateAccept.Location = new System.Drawing.Point(367, 268);
            this.uDateAccept.Name = "uDateAccept";
            this.uDateAccept.Size = new System.Drawing.Size(86, 21);
            this.uDateAccept.TabIndex = 116;
            // 
            // uCheckAcceptFlag
            // 
            this.uCheckAcceptFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckAcceptFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckAcceptFlag.Location = new System.Drawing.Point(549, 268);
            this.uCheckAcceptFlag.Name = "uCheckAcceptFlag";
            this.uCheckAcceptFlag.Size = new System.Drawing.Size(14, 20);
            this.uCheckAcceptFlag.TabIndex = 187;
            this.uCheckAcceptFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextAcceptUserName
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptUserName.Appearance = appearance32;
            this.uTextAcceptUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptUserName.Location = new System.Drawing.Point(189, 268);
            this.uTextAcceptUserName.Name = "uTextAcceptUserName";
            this.uTextAcceptUserName.ReadOnly = true;
            this.uTextAcceptUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextAcceptUserName.TabIndex = 115;
            // 
            // uTextSolderBallModel
            // 
            this.uTextSolderBallModel.Location = new System.Drawing.Point(809, 80);
            this.uTextSolderBallModel.Name = "uTextSolderBallModel";
            this.uTextSolderBallModel.ReadOnly = true;
            this.uTextSolderBallModel.Size = new System.Drawing.Size(34, 21);
            this.uTextSolderBallModel.TabIndex = 60;
            this.uTextSolderBallModel.Visible = false;
            // 
            // uTextAcceptUserID
            // 
            appearance19.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance19;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAcceptUserID.ButtonsRight.Add(editorButton5);
            this.uTextAcceptUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextAcceptUserID.Location = new System.Drawing.Point(99, 268);
            this.uTextAcceptUserID.MaxLength = 20;
            this.uTextAcceptUserID.Name = "uTextAcceptUserID";
            this.uTextAcceptUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextAcceptUserID.TabIndex = 115;
            this.uTextAcceptUserID.ValueChanged += new System.EventHandler(this.uTextAcceptUserID_ValueChanged);
            this.uTextAcceptUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextAcceptUserID_KeyDown);
            this.uTextAcceptUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAcceptUserID_EditorButtonClick);
            // 
            // uTextHASTCondition
            // 
            this.uTextHASTCondition.Location = new System.Drawing.Point(322, 196);
            this.uTextHASTCondition.MaxLength = 100;
            this.uTextHASTCondition.Name = "uTextHASTCondition";
            this.uTextHASTCondition.Size = new System.Drawing.Size(129, 21);
            this.uTextHASTCondition.TabIndex = 114;
            // 
            // uTextSolderBallVender
            // 
            this.uTextSolderBallVender.Location = new System.Drawing.Point(809, 104);
            this.uTextSolderBallVender.Name = "uTextSolderBallVender";
            this.uTextSolderBallVender.ReadOnly = true;
            this.uTextSolderBallVender.Size = new System.Drawing.Size(58, 21);
            this.uTextSolderBallVender.TabIndex = 58;
            this.uTextSolderBallVender.Visible = false;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(809, 56);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.ReadOnly = true;
            this.uTextEtcDesc.Size = new System.Drawing.Size(69, 21);
            this.uTextEtcDesc.TabIndex = 40;
            this.uTextEtcDesc.Visible = false;
            // 
            // uTextPCTCondition
            // 
            this.uTextPCTCondition.Location = new System.Drawing.Point(322, 172);
            this.uTextPCTCondition.MaxLength = 100;
            this.uTextPCTCondition.Name = "uTextPCTCondition";
            this.uTextPCTCondition.Size = new System.Drawing.Size(129, 21);
            this.uTextPCTCondition.TabIndex = 113;
            // 
            // uTextHTSDate
            // 
            this.uTextHTSDate.Location = new System.Drawing.Point(631, 244);
            this.uTextHTSDate.Name = "uTextHTSDate";
            this.uTextHTSDate.Size = new System.Drawing.Size(171, 21);
            this.uTextHTSDate.TabIndex = 112;
            // 
            // uTextHumidityDate2
            // 
            this.uTextHumidityDate2.Location = new System.Drawing.Point(631, 220);
            this.uTextHumidityDate2.Name = "uTextHumidityDate2";
            this.uTextHumidityDate2.Size = new System.Drawing.Size(171, 21);
            this.uTextHumidityDate2.TabIndex = 111;
            // 
            // uTextHASTDate
            // 
            this.uTextHASTDate.Location = new System.Drawing.Point(631, 196);
            this.uTextHASTDate.Name = "uTextHASTDate";
            this.uTextHASTDate.Size = new System.Drawing.Size(171, 21);
            this.uTextHASTDate.TabIndex = 110;
            // 
            // uTextPCTDate
            // 
            this.uTextPCTDate.Location = new System.Drawing.Point(631, 172);
            this.uTextPCTDate.Name = "uTextPCTDate";
            this.uTextPCTDate.Size = new System.Drawing.Size(171, 21);
            this.uTextPCTDate.TabIndex = 109;
            // 
            // uTextTCDate
            // 
            this.uTextTCDate.Location = new System.Drawing.Point(631, 148);
            this.uTextTCDate.Name = "uTextTCDate";
            this.uTextTCDate.Size = new System.Drawing.Size(171, 21);
            this.uTextTCDate.TabIndex = 108;
            // 
            // uTextReflowDate
            // 
            this.uTextReflowDate.Location = new System.Drawing.Point(631, 124);
            this.uTextReflowDate.Name = "uTextReflowDate";
            this.uTextReflowDate.Size = new System.Drawing.Size(171, 21);
            this.uTextReflowDate.TabIndex = 107;
            // 
            // uLabelAcceptComplete
            // 
            this.uLabelAcceptComplete.Location = new System.Drawing.Point(459, 268);
            this.uLabelAcceptComplete.Name = "uLabelAcceptComplete";
            this.uLabelAcceptComplete.Size = new System.Drawing.Size(86, 20);
            this.uLabelAcceptComplete.TabIndex = 87;
            // 
            // uLabelAcceptDate
            // 
            this.uLabelAcceptDate.Location = new System.Drawing.Point(278, 268);
            this.uLabelAcceptDate.Name = "uLabelAcceptDate";
            this.uLabelAcceptDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelAcceptDate.TabIndex = 87;
            // 
            // uLabelAcceptUser
            // 
            this.uLabelAcceptUser.Location = new System.Drawing.Point(10, 268);
            this.uLabelAcceptUser.Name = "uLabelAcceptUser";
            this.uLabelAcceptUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelAcceptUser.TabIndex = 87;
            // 
            // uTextHumidityDate1
            // 
            this.uTextHumidityDate1.Location = new System.Drawing.Point(631, 100);
            this.uTextHumidityDate1.Name = "uTextHumidityDate1";
            this.uTextHumidityDate1.Size = new System.Drawing.Size(171, 21);
            this.uTextHumidityDate1.TabIndex = 106;
            // 
            // uTextBakeDate
            // 
            this.uTextBakeDate.Location = new System.Drawing.Point(631, 76);
            this.uTextBakeDate.Name = "uTextBakeDate";
            this.uTextBakeDate.Size = new System.Drawing.Size(171, 21);
            this.uTextBakeDate.TabIndex = 105;
            // 
            // uTextTCOptionDate
            // 
            this.uTextTCOptionDate.Location = new System.Drawing.Point(631, 52);
            this.uTextTCOptionDate.Name = "uTextTCOptionDate";
            this.uTextTCOptionDate.Size = new System.Drawing.Size(171, 21);
            this.uTextTCOptionDate.TabIndex = 104;
            // 
            // uTextHTSTime
            // 
            this.uTextHTSTime.Location = new System.Drawing.Point(456, 244);
            this.uTextHTSTime.Name = "uTextHTSTime";
            this.uTextHTSTime.Size = new System.Drawing.Size(171, 21);
            this.uTextHTSTime.TabIndex = 103;
            // 
            // uTextHumidityTime2
            // 
            this.uTextHumidityTime2.Location = new System.Drawing.Point(456, 220);
            this.uTextHumidityTime2.Name = "uTextHumidityTime2";
            this.uTextHumidityTime2.Size = new System.Drawing.Size(171, 21);
            this.uTextHumidityTime2.TabIndex = 102;
            // 
            // uTextHASTTime
            // 
            this.uTextHASTTime.Location = new System.Drawing.Point(456, 196);
            this.uTextHASTTime.Name = "uTextHASTTime";
            this.uTextHASTTime.Size = new System.Drawing.Size(171, 21);
            this.uTextHASTTime.TabIndex = 101;
            // 
            // uTextPCTTime
            // 
            this.uTextPCTTime.Location = new System.Drawing.Point(456, 172);
            this.uTextPCTTime.Name = "uTextPCTTime";
            this.uTextPCTTime.Size = new System.Drawing.Size(171, 21);
            this.uTextPCTTime.TabIndex = 100;
            // 
            // uTextTCTime
            // 
            this.uTextTCTime.Location = new System.Drawing.Point(456, 148);
            this.uTextTCTime.Name = "uTextTCTime";
            this.uTextTCTime.Size = new System.Drawing.Size(171, 21);
            this.uTextTCTime.TabIndex = 99;
            // 
            // uTextProductName
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance24;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(840, 124);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(31, 21);
            this.uTextProductName.TabIndex = 38;
            this.uTextProductName.Visible = false;
            // 
            // uTextReflowTime
            // 
            this.uTextReflowTime.Location = new System.Drawing.Point(456, 124);
            this.uTextReflowTime.Name = "uTextReflowTime";
            this.uTextReflowTime.Size = new System.Drawing.Size(171, 21);
            this.uTextReflowTime.TabIndex = 98;
            // 
            // uTextHumidityTime1
            // 
            this.uTextHumidityTime1.Location = new System.Drawing.Point(456, 100);
            this.uTextHumidityTime1.Name = "uTextHumidityTime1";
            this.uTextHumidityTime1.Size = new System.Drawing.Size(171, 21);
            this.uTextHumidityTime1.TabIndex = 97;
            // 
            // uTextBakeTime
            // 
            this.uTextBakeTime.Location = new System.Drawing.Point(456, 76);
            this.uTextBakeTime.Name = "uTextBakeTime";
            this.uTextBakeTime.Size = new System.Drawing.Size(171, 21);
            this.uTextBakeTime.TabIndex = 96;
            // 
            // uTextCustomerName
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance22;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(840, 148);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(31, 21);
            this.uTextCustomerName.TabIndex = 35;
            this.uTextCustomerName.Visible = false;
            // 
            // uTextTCOptionTime
            // 
            this.uTextTCOptionTime.Location = new System.Drawing.Point(456, 52);
            this.uTextTCOptionTime.Name = "uTextTCOptionTime";
            this.uTextTCOptionTime.Size = new System.Drawing.Size(171, 21);
            this.uTextTCOptionTime.TabIndex = 95;
            // 
            // uComboHTSCondition
            // 
            this.uComboHTSCondition.Location = new System.Drawing.Point(322, 244);
            this.uComboHTSCondition.Name = "uComboHTSCondition";
            this.uComboHTSCondition.Size = new System.Drawing.Size(129, 21);
            this.uComboHTSCondition.TabIndex = 94;
            this.uComboHTSCondition.Text = "ultraComboEditor9";
            // 
            // uComboHumidityCondition2
            // 
            this.uComboHumidityCondition2.Location = new System.Drawing.Point(322, 220);
            this.uComboHumidityCondition2.Name = "uComboHumidityCondition2";
            this.uComboHumidityCondition2.Size = new System.Drawing.Size(129, 21);
            this.uComboHumidityCondition2.TabIndex = 93;
            this.uComboHumidityCondition2.Text = "ultraComboEditor8";
            // 
            // uTextProductCode
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProductCode.Appearance = appearance29;
            this.uTextProductCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProductCode.Location = new System.Drawing.Point(809, 28);
            this.uTextProductCode.MaxLength = 20;
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(69, 21);
            this.uTextProductCode.TabIndex = 37;
            this.uTextProductCode.Visible = false;
            this.uTextProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextProductCode_EditorButtonClick);
            // 
            // uComboHASTCondition
            // 
            this.uComboHASTCondition.Location = new System.Drawing.Point(754, 196);
            this.uComboHASTCondition.Name = "uComboHASTCondition";
            this.uComboHASTCondition.Size = new System.Drawing.Size(129, 21);
            this.uComboHASTCondition.TabIndex = 92;
            this.uComboHASTCondition.Text = "ultraComboEditor7";
            this.uComboHASTCondition.Visible = false;
            // 
            // uComboPCTCondition
            // 
            this.uComboPCTCondition.Location = new System.Drawing.Point(754, 172);
            this.uComboPCTCondition.Name = "uComboPCTCondition";
            this.uComboPCTCondition.Size = new System.Drawing.Size(129, 21);
            this.uComboPCTCondition.TabIndex = 91;
            this.uComboPCTCondition.Text = "ultraComboEditor6";
            this.uComboPCTCondition.Visible = false;
            // 
            // uComboTCCondition
            // 
            this.uComboTCCondition.Location = new System.Drawing.Point(322, 148);
            this.uComboTCCondition.Name = "uComboTCCondition";
            this.uComboTCCondition.Size = new System.Drawing.Size(129, 21);
            this.uComboTCCondition.TabIndex = 90;
            this.uComboTCCondition.Text = "ultraComboEditor5";
            // 
            // uComboReflowCondition
            // 
            this.uComboReflowCondition.Location = new System.Drawing.Point(322, 124);
            this.uComboReflowCondition.Name = "uComboReflowCondition";
            this.uComboReflowCondition.Size = new System.Drawing.Size(129, 21);
            this.uComboReflowCondition.TabIndex = 89;
            this.uComboReflowCondition.Text = "ultraComboEditor4";
            // 
            // uComboHumidityCondition1
            // 
            this.uComboHumidityCondition1.Location = new System.Drawing.Point(322, 100);
            this.uComboHumidityCondition1.Name = "uComboHumidityCondition1";
            this.uComboHumidityCondition1.Size = new System.Drawing.Size(129, 21);
            this.uComboHumidityCondition1.TabIndex = 88;
            this.uComboHumidityCondition1.Text = "ultraComboEditor3";
            // 
            // uComboBakeCondition
            // 
            this.uComboBakeCondition.Location = new System.Drawing.Point(322, 76);
            this.uComboBakeCondition.Name = "uComboBakeCondition";
            this.uComboBakeCondition.Size = new System.Drawing.Size(129, 21);
            this.uComboBakeCondition.TabIndex = 87;
            this.uComboBakeCondition.Text = "ultraComboEditor2";
            // 
            // uComboTCOptionCondition
            // 
            this.uComboTCOptionCondition.Location = new System.Drawing.Point(322, 52);
            this.uComboTCOptionCondition.Name = "uComboTCOptionCondition";
            this.uComboTCOptionCondition.Size = new System.Drawing.Size(129, 21);
            this.uComboTCOptionCondition.TabIndex = 86;
            this.uComboTCOptionCondition.Text = "ultraComboEditor1";
            // 
            // uComboHTSFlag
            // 
            this.uComboHTSFlag.Location = new System.Drawing.Point(189, 244);
            this.uComboHTSFlag.Name = "uComboHTSFlag";
            this.uComboHTSFlag.Size = new System.Drawing.Size(129, 21);
            this.uComboHTSFlag.TabIndex = 85;
            this.uComboHTSFlag.Text = "ultraComboEditor9";
            // 
            // uComboHumidityFlag2
            // 
            this.uComboHumidityFlag2.Location = new System.Drawing.Point(189, 220);
            this.uComboHumidityFlag2.Name = "uComboHumidityFlag2";
            this.uComboHumidityFlag2.Size = new System.Drawing.Size(129, 21);
            this.uComboHumidityFlag2.TabIndex = 84;
            this.uComboHumidityFlag2.Text = "ultraComboEditor8";
            // 
            // uComboHASTFlag
            // 
            this.uComboHASTFlag.Location = new System.Drawing.Point(189, 196);
            this.uComboHASTFlag.Name = "uComboHASTFlag";
            this.uComboHASTFlag.Size = new System.Drawing.Size(129, 21);
            this.uComboHASTFlag.TabIndex = 83;
            this.uComboHASTFlag.Text = "ultraComboEditor7";
            // 
            // uComboPCTFlag
            // 
            this.uComboPCTFlag.Location = new System.Drawing.Point(189, 172);
            this.uComboPCTFlag.Name = "uComboPCTFlag";
            this.uComboPCTFlag.Size = new System.Drawing.Size(129, 21);
            this.uComboPCTFlag.TabIndex = 82;
            this.uComboPCTFlag.Text = "ultraComboEditor6";
            // 
            // uComboTCFlag
            // 
            this.uComboTCFlag.Location = new System.Drawing.Point(189, 148);
            this.uComboTCFlag.Name = "uComboTCFlag";
            this.uComboTCFlag.Size = new System.Drawing.Size(129, 21);
            this.uComboTCFlag.TabIndex = 81;
            this.uComboTCFlag.Text = "ultraComboEditor5";
            // 
            // uComboReflowFlag
            // 
            this.uComboReflowFlag.Location = new System.Drawing.Point(189, 124);
            this.uComboReflowFlag.Name = "uComboReflowFlag";
            this.uComboReflowFlag.Size = new System.Drawing.Size(129, 21);
            this.uComboReflowFlag.TabIndex = 80;
            this.uComboReflowFlag.Text = "ultraComboEditor4";
            // 
            // uComboHumidityFlag1
            // 
            this.uComboHumidityFlag1.Location = new System.Drawing.Point(189, 100);
            this.uComboHumidityFlag1.Name = "uComboHumidityFlag1";
            this.uComboHumidityFlag1.Size = new System.Drawing.Size(129, 21);
            this.uComboHumidityFlag1.TabIndex = 79;
            this.uComboHumidityFlag1.Text = "ultraComboEditor3";
            // 
            // uComboBakeFlag
            // 
            this.uComboBakeFlag.Location = new System.Drawing.Point(189, 76);
            this.uComboBakeFlag.Name = "uComboBakeFlag";
            this.uComboBakeFlag.Size = new System.Drawing.Size(129, 21);
            this.uComboBakeFlag.TabIndex = 78;
            this.uComboBakeFlag.Text = "ultraComboEditor2";
            // 
            // uComboTCOptionFlag
            // 
            this.uComboTCOptionFlag.Location = new System.Drawing.Point(189, 52);
            this.uComboTCOptionFlag.Name = "uComboTCOptionFlag";
            this.uComboTCOptionFlag.Size = new System.Drawing.Size(129, 21);
            this.uComboTCOptionFlag.TabIndex = 77;
            this.uComboTCOptionFlag.Text = "ultraComboEditor1";
            // 
            // uLabelschedule
            // 
            this.uLabelschedule.Location = new System.Drawing.Point(631, 28);
            this.uLabelschedule.Name = "uLabelschedule";
            this.uLabelschedule.Size = new System.Drawing.Size(171, 20);
            this.uLabelschedule.TabIndex = 76;
            this.uLabelschedule.Text = "ultraLabel1";
            // 
            // uLabelProgressTime
            // 
            this.uLabelProgressTime.Location = new System.Drawing.Point(456, 28);
            this.uLabelProgressTime.Name = "uLabelProgressTime";
            this.uLabelProgressTime.Size = new System.Drawing.Size(171, 20);
            this.uLabelProgressTime.TabIndex = 75;
            this.uLabelProgressTime.Text = "ultraLabel1";
            // 
            // uLabelCondition
            // 
            this.uLabelCondition.Location = new System.Drawing.Point(322, 28);
            this.uLabelCondition.Name = "uLabelCondition";
            this.uLabelCondition.Size = new System.Drawing.Size(129, 20);
            this.uLabelCondition.TabIndex = 74;
            this.uLabelCondition.Text = "ultraLabel1";
            // 
            // uLabelYesNo
            // 
            this.uLabelYesNo.Location = new System.Drawing.Point(189, 28);
            this.uLabelYesNo.Name = "uLabelYesNo";
            this.uLabelYesNo.Size = new System.Drawing.Size(129, 20);
            this.uLabelYesNo.TabIndex = 73;
            this.uLabelYesNo.Text = "ultraLabel1";
            // 
            // uLabelHTS
            // 
            this.uLabelHTS.Location = new System.Drawing.Point(10, 244);
            this.uLabelHTS.Name = "uLabelHTS";
            this.uLabelHTS.Size = new System.Drawing.Size(175, 20);
            this.uLabelHTS.TabIndex = 72;
            this.uLabelHTS.Text = "ultraLabel1";
            // 
            // uLabelHumidity
            // 
            this.uLabelHumidity.Location = new System.Drawing.Point(10, 220);
            this.uLabelHumidity.Name = "uLabelHumidity";
            this.uLabelHumidity.Size = new System.Drawing.Size(175, 20);
            this.uLabelHumidity.TabIndex = 71;
            this.uLabelHumidity.Text = "ultraLabel1";
            // 
            // uLabelHAST
            // 
            this.uLabelHAST.Location = new System.Drawing.Point(10, 196);
            this.uLabelHAST.Name = "uLabelHAST";
            this.uLabelHAST.Size = new System.Drawing.Size(175, 20);
            this.uLabelHAST.TabIndex = 70;
            this.uLabelHAST.Text = "ultraLabel1";
            // 
            // uLabelPCT
            // 
            this.uLabelPCT.Location = new System.Drawing.Point(10, 172);
            this.uLabelPCT.Name = "uLabelPCT";
            this.uLabelPCT.Size = new System.Drawing.Size(175, 20);
            this.uLabelPCT.TabIndex = 69;
            this.uLabelPCT.Text = "ultraLabel1";
            // 
            // uLabelTC
            // 
            this.uLabelTC.Location = new System.Drawing.Point(10, 148);
            this.uLabelTC.Name = "uLabelTC";
            this.uLabelTC.Size = new System.Drawing.Size(175, 20);
            this.uLabelTC.TabIndex = 68;
            this.uLabelTC.Text = "ultraLabel1";
            // 
            // uLabelPreReflow
            // 
            this.uLabelPreReflow.Location = new System.Drawing.Point(99, 124);
            this.uLabelPreReflow.Name = "uLabelPreReflow";
            this.uLabelPreReflow.Size = new System.Drawing.Size(86, 20);
            this.uLabelPreReflow.TabIndex = 67;
            this.uLabelPreReflow.Text = "ultraLabel1";
            // 
            // uLabelPreHumidity
            // 
            this.uLabelPreHumidity.Location = new System.Drawing.Point(99, 100);
            this.uLabelPreHumidity.Name = "uLabelPreHumidity";
            this.uLabelPreHumidity.Size = new System.Drawing.Size(86, 20);
            this.uLabelPreHumidity.TabIndex = 66;
            this.uLabelPreHumidity.Text = "ultraLabel1";
            // 
            // uLabelPreBakeOven
            // 
            this.uLabelPreBakeOven.Location = new System.Drawing.Point(99, 76);
            this.uLabelPreBakeOven.Name = "uLabelPreBakeOven";
            this.uLabelPreBakeOven.Size = new System.Drawing.Size(86, 20);
            this.uLabelPreBakeOven.TabIndex = 65;
            this.uLabelPreBakeOven.Text = "ultraLabel1";
            // 
            // uLabelPreTC
            // 
            this.uLabelPreTC.Location = new System.Drawing.Point(99, 52);
            this.uLabelPreTC.Name = "uLabelPreTC";
            this.uLabelPreTC.Size = new System.Drawing.Size(86, 20);
            this.uLabelPreTC.TabIndex = 64;
            this.uLabelPreTC.Text = "ultraLabel1";
            // 
            // ulabelPreCondition
            // 
            this.ulabelPreCondition.Location = new System.Drawing.Point(10, 52);
            this.ulabelPreCondition.Name = "ulabelPreCondition";
            this.ulabelPreCondition.Size = new System.Drawing.Size(86, 92);
            this.ulabelPreCondition.TabIndex = 63;
            this.ulabelPreCondition.Text = "ultraLabel1";
            // 
            // uLabelTestName
            // 
            this.uLabelTestName.Location = new System.Drawing.Point(10, 28);
            this.uLabelTestName.Name = "uLabelTestName";
            this.uLabelTestName.Size = new System.Drawing.Size(175, 20);
            this.uLabelTestName.TabIndex = 62;
            this.uLabelTestName.Text = "ultraLabel1";
            // 
            // uGroupBoxRequest
            // 
            this.uGroupBoxRequest.Controls.Add(this.uComboProcessType);
            this.uGroupBoxRequest.Controls.Add(this.uNumReqQty);
            this.uGroupBoxRequest.Controls.Add(this.uLabelReqQty);
            this.uGroupBoxRequest.Controls.Add(this.uTextProductPN);
            this.uGroupBoxRequest.Controls.Add(this.uLabelSolderBallModelorPlatingComposition);
            this.uGroupBoxRequest.Controls.Add(this.uTextProductLN);
            this.uGroupBoxRequest.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBoxRequest.Controls.Add(this.uTextEMCVender);
            this.uGroupBoxRequest.Controls.Add(this.uLabelEMCVender);
            this.uGroupBoxRequest.Controls.Add(this.uTextEMCModel);
            this.uGroupBoxRequest.Controls.Add(this.uLabelEMCModel);
            this.uGroupBoxRequest.Controls.Add(this.uTextWireVender);
            this.uGroupBoxRequest.Controls.Add(this.uLabelWireVender);
            this.uGroupBoxRequest.Controls.Add(this.uTextWireModel);
            this.uGroupBoxRequest.Controls.Add(this.uLabelWireModel);
            this.uGroupBoxRequest.Controls.Add(this.uTextLForPCBVender);
            this.uGroupBoxRequest.Controls.Add(this.uLabelLFPCBVender);
            this.uGroupBoxRequest.Controls.Add(this.uTextLForPCBModel);
            this.uGroupBoxRequest.Controls.Add(this.uLabelLFPCBModel);
            this.uGroupBoxRequest.Controls.Add(this.uTextAdhesive_Vender);
            this.uGroupBoxRequest.Controls.Add(this.uLabelAdhesiveVender);
            this.uGroupBoxRequest.Controls.Add(this.uTextAdhesive_Model);
            this.uGroupBoxRequest.Controls.Add(this.uLabelAdhesiveModel);
            this.uGroupBoxRequest.Controls.Add(this.uTextChip_Size);
            this.uGroupBoxRequest.Controls.Add(this.uLabelChipSize);
            this.uGroupBoxRequest.Controls.Add(this.uLabelSolderBallVenderorPlatingSubcontractor);
            this.uGroupBoxRequest.Controls.Add(this.uTextPackage);
            this.uGroupBoxRequest.Controls.Add(this.uLabelProduct);
            this.uGroupBoxRequest.Controls.Add(this.uTextCustomerCode);
            this.uGroupBoxRequest.Controls.Add(this.uLabelCustomer);
            this.uGroupBoxRequest.Location = new System.Drawing.Point(10, 136);
            this.uGroupBoxRequest.Name = "uGroupBoxRequest";
            this.uGroupBoxRequest.Size = new System.Drawing.Size(887, 176);
            this.uGroupBoxRequest.TabIndex = 27;
            this.uGroupBoxRequest.Text = "ultraGroupBox1";
            // 
            // uComboProcessType
            // 
            this.uComboProcessType.Location = new System.Drawing.Point(703, 76);
            this.uComboProcessType.MaxLength = 10;
            this.uComboProcessType.Name = "uComboProcessType";
            this.uComboProcessType.Size = new System.Drawing.Size(123, 21);
            this.uComboProcessType.TabIndex = 63;
            // 
            // uNumReqQty
            // 
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Text = "0";
            this.uNumReqQty.ButtonsLeft.Add(editorButton6);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumReqQty.ButtonsRight.Add(spinEditorButton1);
            this.uNumReqQty.Location = new System.Drawing.Point(429, 52);
            this.uNumReqQty.MaskInput = "nnn,nnn,nnn";
            this.uNumReqQty.MinValue = 0;
            this.uNumReqQty.Name = "uNumReqQty";
            this.uNumReqQty.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uNumReqQty.PromptChar = ' ';
            this.uNumReqQty.Size = new System.Drawing.Size(129, 21);
            this.uNumReqQty.TabIndex = 62;
            this.uNumReqQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumReqQty_EditorSpinButtonClick);
            this.uNumReqQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumReqQty_EditorButtonClick);
            // 
            // uLabelReqQty
            // 
            this.uLabelReqQty.Location = new System.Drawing.Point(309, 52);
            this.uLabelReqQty.Name = "uLabelReqQty";
            this.uLabelReqQty.Size = new System.Drawing.Size(117, 20);
            this.uLabelReqQty.TabIndex = 61;
            this.uLabelReqQty.Text = "ultraLabel1";
            // 
            // uTextProductPN
            // 
            this.uTextProductPN.Location = new System.Drawing.Point(703, 52);
            this.uTextProductPN.MaxLength = 50;
            this.uTextProductPN.Name = "uTextProductPN";
            this.uTextProductPN.Size = new System.Drawing.Size(174, 21);
            this.uTextProductPN.TabIndex = 60;
            // 
            // uLabelSolderBallModelorPlatingComposition
            // 
            this.uLabelSolderBallModelorPlatingComposition.Location = new System.Drawing.Point(610, 52);
            this.uLabelSolderBallModelorPlatingComposition.Name = "uLabelSolderBallModelorPlatingComposition";
            this.uLabelSolderBallModelorPlatingComposition.Size = new System.Drawing.Size(87, 20);
            this.uLabelSolderBallModelorPlatingComposition.TabIndex = 59;
            this.uLabelSolderBallModelorPlatingComposition.Text = "제품P/N";
            // 
            // uTextProductLN
            // 
            this.uTextProductLN.Location = new System.Drawing.Point(703, 28);
            this.uTextProductLN.MaxLength = 50;
            this.uTextProductLN.Name = "uTextProductLN";
            this.uTextProductLN.Size = new System.Drawing.Size(175, 21);
            this.uTextProductLN.TabIndex = 40;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(610, 28);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(87, 20);
            this.uLabelEtcDesc.TabIndex = 39;
            this.uLabelEtcDesc.Text = "비고 -> 제품L/N";
            // 
            // uTextEMCVender
            // 
            this.uTextEMCVender.Location = new System.Drawing.Point(429, 148);
            this.uTextEMCVender.Name = "uTextEMCVender";
            this.uTextEMCVender.Size = new System.Drawing.Size(174, 21);
            this.uTextEMCVender.TabIndex = 57;
            // 
            // uLabelEMCVender
            // 
            this.uLabelEMCVender.Location = new System.Drawing.Point(309, 148);
            this.uLabelEMCVender.Name = "uLabelEMCVender";
            this.uLabelEMCVender.Size = new System.Drawing.Size(117, 20);
            this.uLabelEMCVender.TabIndex = 56;
            this.uLabelEMCVender.Text = "ultraLabel1";
            // 
            // uTextEMCModel
            // 
            this.uTextEMCModel.Location = new System.Drawing.Point(127, 148);
            this.uTextEMCModel.Name = "uTextEMCModel";
            this.uTextEMCModel.Size = new System.Drawing.Size(174, 21);
            this.uTextEMCModel.TabIndex = 55;
            // 
            // uLabelEMCModel
            // 
            this.uLabelEMCModel.Location = new System.Drawing.Point(10, 148);
            this.uLabelEMCModel.Name = "uLabelEMCModel";
            this.uLabelEMCModel.Size = new System.Drawing.Size(111, 20);
            this.uLabelEMCModel.TabIndex = 54;
            this.uLabelEMCModel.Text = "ultraLabel1";
            // 
            // uTextWireVender
            // 
            this.uTextWireVender.Location = new System.Drawing.Point(429, 124);
            this.uTextWireVender.Name = "uTextWireVender";
            this.uTextWireVender.Size = new System.Drawing.Size(174, 21);
            this.uTextWireVender.TabIndex = 53;
            // 
            // uLabelWireVender
            // 
            this.uLabelWireVender.Location = new System.Drawing.Point(309, 124);
            this.uLabelWireVender.Name = "uLabelWireVender";
            this.uLabelWireVender.Size = new System.Drawing.Size(117, 20);
            this.uLabelWireVender.TabIndex = 52;
            this.uLabelWireVender.Text = "ultraLabel1";
            // 
            // uTextWireModel
            // 
            this.uTextWireModel.Location = new System.Drawing.Point(127, 124);
            this.uTextWireModel.Name = "uTextWireModel";
            this.uTextWireModel.Size = new System.Drawing.Size(174, 21);
            this.uTextWireModel.TabIndex = 51;
            // 
            // uLabelWireModel
            // 
            this.uLabelWireModel.Location = new System.Drawing.Point(10, 124);
            this.uLabelWireModel.Name = "uLabelWireModel";
            this.uLabelWireModel.Size = new System.Drawing.Size(111, 20);
            this.uLabelWireModel.TabIndex = 50;
            this.uLabelWireModel.Text = "ultraLabel1";
            // 
            // uTextLForPCBVender
            // 
            this.uTextLForPCBVender.Location = new System.Drawing.Point(429, 100);
            this.uTextLForPCBVender.Name = "uTextLForPCBVender";
            this.uTextLForPCBVender.Size = new System.Drawing.Size(174, 21);
            this.uTextLForPCBVender.TabIndex = 49;
            // 
            // uLabelLFPCBVender
            // 
            this.uLabelLFPCBVender.Location = new System.Drawing.Point(309, 100);
            this.uLabelLFPCBVender.Name = "uLabelLFPCBVender";
            this.uLabelLFPCBVender.Size = new System.Drawing.Size(117, 20);
            this.uLabelLFPCBVender.TabIndex = 48;
            this.uLabelLFPCBVender.Text = "ultraLabel1";
            // 
            // uTextLForPCBModel
            // 
            this.uTextLForPCBModel.Location = new System.Drawing.Point(127, 100);
            this.uTextLForPCBModel.Name = "uTextLForPCBModel";
            this.uTextLForPCBModel.Size = new System.Drawing.Size(174, 21);
            this.uTextLForPCBModel.TabIndex = 47;
            // 
            // uLabelLFPCBModel
            // 
            this.uLabelLFPCBModel.Location = new System.Drawing.Point(10, 100);
            this.uLabelLFPCBModel.Name = "uLabelLFPCBModel";
            this.uLabelLFPCBModel.Size = new System.Drawing.Size(111, 20);
            this.uLabelLFPCBModel.TabIndex = 46;
            this.uLabelLFPCBModel.Text = "ultraLabel1";
            // 
            // uTextAdhesive_Vender
            // 
            this.uTextAdhesive_Vender.Location = new System.Drawing.Point(429, 76);
            this.uTextAdhesive_Vender.Name = "uTextAdhesive_Vender";
            this.uTextAdhesive_Vender.Size = new System.Drawing.Size(174, 21);
            this.uTextAdhesive_Vender.TabIndex = 45;
            // 
            // uLabelAdhesiveVender
            // 
            this.uLabelAdhesiveVender.Location = new System.Drawing.Point(309, 76);
            this.uLabelAdhesiveVender.Name = "uLabelAdhesiveVender";
            this.uLabelAdhesiveVender.Size = new System.Drawing.Size(117, 20);
            this.uLabelAdhesiveVender.TabIndex = 44;
            this.uLabelAdhesiveVender.Text = "ultraLabel1";
            // 
            // uTextAdhesive_Model
            // 
            this.uTextAdhesive_Model.Location = new System.Drawing.Point(127, 76);
            this.uTextAdhesive_Model.Name = "uTextAdhesive_Model";
            this.uTextAdhesive_Model.Size = new System.Drawing.Size(174, 21);
            this.uTextAdhesive_Model.TabIndex = 43;
            // 
            // uLabelAdhesiveModel
            // 
            this.uLabelAdhesiveModel.Location = new System.Drawing.Point(10, 76);
            this.uLabelAdhesiveModel.Name = "uLabelAdhesiveModel";
            this.uLabelAdhesiveModel.Size = new System.Drawing.Size(111, 20);
            this.uLabelAdhesiveModel.TabIndex = 42;
            this.uLabelAdhesiveModel.Text = "ultraLabel1";
            // 
            // uTextChip_Size
            // 
            this.uTextChip_Size.Location = new System.Drawing.Point(127, 52);
            this.uTextChip_Size.Name = "uTextChip_Size";
            this.uTextChip_Size.Size = new System.Drawing.Size(174, 21);
            this.uTextChip_Size.TabIndex = 41;
            // 
            // uLabelChipSize
            // 
            this.uLabelChipSize.Location = new System.Drawing.Point(10, 52);
            this.uLabelChipSize.Name = "uLabelChipSize";
            this.uLabelChipSize.Size = new System.Drawing.Size(111, 20);
            this.uLabelChipSize.TabIndex = 40;
            this.uLabelChipSize.Text = "ultraLabel1";
            // 
            // uLabelSolderBallVenderorPlatingSubcontractor
            // 
            this.uLabelSolderBallVenderorPlatingSubcontractor.Location = new System.Drawing.Point(610, 76);
            this.uLabelSolderBallVenderorPlatingSubcontractor.Name = "uLabelSolderBallVenderorPlatingSubcontractor";
            this.uLabelSolderBallVenderorPlatingSubcontractor.Size = new System.Drawing.Size(87, 20);
            this.uLabelSolderBallVenderorPlatingSubcontractor.TabIndex = 39;
            this.uLabelSolderBallVenderorPlatingSubcontractor.Text = "ProcessType";
            // 
            // uTextPackage
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPackage.Appearance = appearance28;
            this.uTextPackage.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPackage.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextPackage.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextPackage.Location = new System.Drawing.Point(127, 28);
            this.uTextPackage.MaxLength = 50;
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.Size = new System.Drawing.Size(171, 21);
            this.uTextPackage.TabIndex = 37;
            this.uTextPackage.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextProductCode_EditorButtonClick);
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(10, 28);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(111, 20);
            this.uLabelProduct.TabIndex = 36;
            this.uLabelProduct.Text = "ultraLabel1";
            // 
            // uTextCustomerCode
            // 
            appearance23.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomerCode.Appearance = appearance23;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomerCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCustomerCode.Location = new System.Drawing.Point(429, 28);
            this.uTextCustomerCode.MaxLength = 50;
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.Size = new System.Drawing.Size(127, 21);
            this.uTextCustomerCode.TabIndex = 34;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(309, 28);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(117, 20);
            this.uLabelCustomer.TabIndex = 33;
            this.uLabelCustomer.Text = "ultraLabel1";
            // 
            // uGroupBoxStdInfo
            // 
            this.uGroupBoxStdInfo.Controls.Add(this.uComboGubun);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelGubun);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboPlantCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextChangeAfter);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelChangeAfter);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextChangeBefore);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelChangeBefore);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextAppraisalObject);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelAppraisalObject);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextWriteUserName);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextWriteUserID);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelWriteUser);
            this.uGroupBoxStdInfo.Controls.Add(this.uDateWriteDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelWriteDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextReliabilityNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelReliabilityNo);
            this.uGroupBoxStdInfo.Location = new System.Drawing.Point(10, 12);
            this.uGroupBoxStdInfo.Name = "uGroupBoxStdInfo";
            this.uGroupBoxStdInfo.Size = new System.Drawing.Size(887, 120);
            this.uGroupBoxStdInfo.TabIndex = 26;
            this.uGroupBoxStdInfo.Text = "ultraGroupBox1";
            // 
            // uComboGubun
            // 
            this.uComboGubun.Location = new System.Drawing.Point(99, 48);
            this.uComboGubun.MaxLength = 500;
            this.uComboGubun.Name = "uComboGubun";
            this.uComboGubun.Size = new System.Drawing.Size(130, 21);
            this.uComboGubun.TabIndex = 44;
            this.uComboGubun.Text = "ultraComboEditor1";
            // 
            // uLabelGubun
            // 
            this.uLabelGubun.Location = new System.Drawing.Point(10, 48);
            this.uLabelGubun.Name = "uLabelGubun";
            this.uLabelGubun.Size = new System.Drawing.Size(86, 20);
            this.uLabelGubun.TabIndex = 43;
            this.uLabelGubun.Text = "구분";
            // 
            // uComboPlantCode
            // 
            this.uComboPlantCode.Location = new System.Drawing.Point(99, 24);
            this.uComboPlantCode.MaxLength = 10;
            this.uComboPlantCode.Name = "uComboPlantCode";
            this.uComboPlantCode.Size = new System.Drawing.Size(130, 21);
            this.uComboPlantCode.TabIndex = 42;
            this.uComboPlantCode.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(10, 24);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(86, 20);
            this.uLabelPlant.TabIndex = 41;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uTextChangeAfter
            // 
            this.uTextChangeAfter.Location = new System.Drawing.Point(552, 96);
            this.uTextChangeAfter.Name = "uTextChangeAfter";
            this.uTextChangeAfter.Size = new System.Drawing.Size(309, 21);
            this.uTextChangeAfter.TabIndex = 38;
            // 
            // uLabelChangeAfter
            // 
            this.uLabelChangeAfter.Location = new System.Drawing.Point(463, 96);
            this.uLabelChangeAfter.Name = "uLabelChangeAfter";
            this.uLabelChangeAfter.Size = new System.Drawing.Size(86, 20);
            this.uLabelChangeAfter.TabIndex = 37;
            this.uLabelChangeAfter.Text = "ultraLabel1";
            // 
            // uTextChangeBefore
            // 
            this.uTextChangeBefore.Location = new System.Drawing.Point(552, 72);
            this.uTextChangeBefore.Name = "uTextChangeBefore";
            this.uTextChangeBefore.Size = new System.Drawing.Size(309, 21);
            this.uTextChangeBefore.TabIndex = 36;
            // 
            // uLabelChangeBefore
            // 
            this.uLabelChangeBefore.Location = new System.Drawing.Point(463, 72);
            this.uLabelChangeBefore.Name = "uLabelChangeBefore";
            this.uLabelChangeBefore.Size = new System.Drawing.Size(86, 20);
            this.uLabelChangeBefore.TabIndex = 35;
            this.uLabelChangeBefore.Text = "ultraLabel1";
            // 
            // uTextAppraisalObject
            // 
            this.uTextAppraisalObject.Location = new System.Drawing.Point(99, 72);
            this.uTextAppraisalObject.Multiline = true;
            this.uTextAppraisalObject.Name = "uTextAppraisalObject";
            this.uTextAppraisalObject.Size = new System.Drawing.Size(353, 44);
            this.uTextAppraisalObject.TabIndex = 34;
            // 
            // uLabelAppraisalObject
            // 
            this.uLabelAppraisalObject.Location = new System.Drawing.Point(10, 72);
            this.uLabelAppraisalObject.Name = "uLabelAppraisalObject";
            this.uLabelAppraisalObject.Size = new System.Drawing.Size(86, 20);
            this.uLabelAppraisalObject.TabIndex = 33;
            this.uLabelAppraisalObject.Text = "평가목적";
            // 
            // uTextWriteUserName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Appearance = appearance25;
            this.uTextWriteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Location = new System.Drawing.Point(420, 48);
            this.uTextWriteUserName.Name = "uTextWriteUserName";
            this.uTextWriteUserName.ReadOnly = true;
            this.uTextWriteUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextWriteUserName.TabIndex = 32;
            // 
            // uTextWriteUserID
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteUserID.Appearance = appearance31;
            this.uTextWriteUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance27.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance27.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance27;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteUserID.ButtonsRight.Add(editorButton7);
            this.uTextWriteUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextWriteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteUserID.Location = new System.Drawing.Point(333, 48);
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextWriteUserID.TabIndex = 31;
            this.uTextWriteUserID.ValueChanged += new System.EventHandler(this.uTextWriteUserID_ValueChanged);
            this.uTextWriteUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteUserID_KeyDown);
            this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteUserID_EditorButtonClick);
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(243, 48);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelWriteUser.TabIndex = 30;
            this.uLabelWriteUser.Text = "ultraLabel1";
            // 
            // uDateWriteDate
            // 
            appearance30.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Appearance = appearance30;
            this.uDateWriteDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(610, 48);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(86, 21);
            this.uDateWriteDate.TabIndex = 29;
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(521, 48);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelWriteDate.TabIndex = 28;
            this.uLabelWriteDate.Text = "ultraLabel1";
            // 
            // uTextReliabilityNo
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReliabilityNo.Appearance = appearance17;
            this.uTextReliabilityNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReliabilityNo.Location = new System.Drawing.Point(333, 24);
            this.uTextReliabilityNo.Name = "uTextReliabilityNo";
            this.uTextReliabilityNo.Size = new System.Drawing.Size(129, 21);
            this.uTextReliabilityNo.TabIndex = 27;
            // 
            // uLabelReliabilityNo
            // 
            this.uLabelReliabilityNo.Location = new System.Drawing.Point(243, 24);
            this.uLabelReliabilityNo.Name = "uLabelReliabilityNo";
            this.uLabelReliabilityNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelReliabilityNo.TabIndex = 26;
            this.uLabelReliabilityNo.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQATZ0007
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridReliabilityList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0007";
            this.Load += new System.EventHandler(this.frmQATZ0007_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0007_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0007_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0007_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridReliabilityList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAppraisal)).EndInit();
            this.uGroupBoxAppraisal.ResumeLayout(false);
            this.uGroupBoxAppraisal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxAppraiseCondition)).EndInit();
            this.uGroupBoxAppraiseCondition.ResumeLayout(false);
            this.uGroupBoxAppraiseCondition.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAcceptFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSolderBallModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHASTCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSolderBallVender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCTCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHTSDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHASTDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCTDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReflowDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBakeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCOptionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHTSTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHASTTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCTTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReflowTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHumidityTime1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBakeTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTCOptionTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHTSCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityCondition2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHASTCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPCTCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReflowCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityCondition1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboBakeCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCOptionCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHTSFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityFlag2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHASTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPCTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReflowFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHumidityFlag1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboBakeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboTCOptionFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxRequest)).EndInit();
            this.uGroupBoxRequest.ResumeLayout(false);
            this.uGroupBoxRequest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumReqQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductLN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMCVender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEMCModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWireVender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWireModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLForPCBVender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLForPCBModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdhesive_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdhesive_Model)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChip_Size)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).EndInit();
            this.uGroupBoxStdInfo.ResumeLayout(false);
            this.uGroupBoxStdInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeAfter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeBefore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAppraisalObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReliabilityNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProduct;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchWriteToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchWriteFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchWriteDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridReliabilityList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxRequest;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxStdInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeAfter;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeAfter;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeBefore;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeBefore;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAppraisalObject;
        private Infragistics.Win.Misc.UltraLabel uLabelAppraisalObject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReliabilityNo;
        private Infragistics.Win.Misc.UltraLabel uLabelReliabilityNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSolderBallVenderorPlatingSubcontractor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChip_Size;
        private Infragistics.Win.Misc.UltraLabel uLabelChipSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdhesive_Vender;
        private Infragistics.Win.Misc.UltraLabel uLabelAdhesiveVender;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdhesive_Model;
        private Infragistics.Win.Misc.UltraLabel uLabelAdhesiveModel;
        private Infragistics.Win.Misc.UltraLabel uLabelReqQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSolderBallModel;
        private Infragistics.Win.Misc.UltraLabel uLabelSolderBallModelorPlatingComposition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSolderBallVender;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEMCVender;
        private Infragistics.Win.Misc.UltraLabel uLabelEMCVender;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEMCModel;
        private Infragistics.Win.Misc.UltraLabel uLabelEMCModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWireVender;
        private Infragistics.Win.Misc.UltraLabel uLabelWireVender;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWireModel;
        private Infragistics.Win.Misc.UltraLabel uLabelWireModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLForPCBVender;
        private Infragistics.Win.Misc.UltraLabel uLabelLFPCBVender;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLForPCBModel;
        private Infragistics.Win.Misc.UltraLabel uLabelLFPCBModel;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxAppraiseCondition;
        private Infragistics.Win.Misc.UltraLabel uLabelHumidity;
        private Infragistics.Win.Misc.UltraLabel uLabelHAST;
        private Infragistics.Win.Misc.UltraLabel uLabelPCT;
        private Infragistics.Win.Misc.UltraLabel uLabelTC;
        private Infragistics.Win.Misc.UltraLabel uLabelPreReflow;
        private Infragistics.Win.Misc.UltraLabel uLabelPreHumidity;
        private Infragistics.Win.Misc.UltraLabel uLabelPreBakeOven;
        private Infragistics.Win.Misc.UltraLabel uLabelPreTC;
        private Infragistics.Win.Misc.UltraLabel ulabelPreCondition;
        private Infragistics.Win.Misc.UltraLabel uLabelTestName;
        private Infragistics.Win.Misc.UltraLabel uLabelHTS;
        private Infragistics.Win.Misc.UltraLabel uLabelschedule;
        private Infragistics.Win.Misc.UltraLabel uLabelProgressTime;
        private Infragistics.Win.Misc.UltraLabel uLabelCondition;
        private Infragistics.Win.Misc.UltraLabel uLabelYesNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHTSFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHumidityFlag2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHASTFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPCTFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboTCFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboReflowFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHumidityFlag1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboBakeFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboTCOptionFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTCTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReflowTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHumidityTime1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBakeTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTCOptionTime;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHTSCondition;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHumidityCondition2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHASTCondition;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPCTCondition;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboTCCondition;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboReflowCondition;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHumidityCondition1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboBakeCondition;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboTCOptionCondition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHTSTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHumidityTime2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHASTTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPCTTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHTSDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHumidityDate2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHASTDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPCTDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTCDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReflowDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHumidityDate1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBakeDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTCOptionDate;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumReqQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHASTCondition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPCTCondition;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelGubun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptUser;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAccept;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAcceptFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptComplete;
        private Infragistics.Win.Misc.UltraLabel uLabelComplete;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectFileName;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectFilaName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectUser;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionPassFailFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelPassFailFlag;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxAppraisal;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductPN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductLN;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessType;
    }
}