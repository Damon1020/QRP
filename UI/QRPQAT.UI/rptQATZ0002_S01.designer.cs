﻿namespace QRPQAT.UI
{
    /// <summary>
    /// Summary description for rptEQUZ0002_02.
    /// </summary>
    partial class rptQATZ0002_S01
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptQATZ0002_S01));
            this.label20 = new DataDynamics.ActiveReports.Label();
            this.label19 = new DataDynamics.ActiveReports.Label();
            this.labelEtc = new DataDynamics.ActiveReports.Label();
            this.label21 = new DataDynamics.ActiveReports.Label();
            this.label22 = new DataDynamics.ActiveReports.Label();
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.textRe = new DataDynamics.ActiveReports.TextBox();
            this.textSample = new DataDynamics.ActiveReports.TextBox();
            this.textBox3 = new DataDynamics.ActiveReports.TextBox();
            this.textBox4 = new DataDynamics.ActiveReports.TextBox();
            this.textBox5 = new DataDynamics.ActiveReports.TextBox();
            this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
            this.crossSectionLine1 = new DataDynamics.ActiveReports.CrossSectionLine();
            this.crossSectionLine2 = new DataDynamics.ActiveReports.CrossSectionLine();
            this.label18 = new DataDynamics.ActiveReports.Label();
            this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
            this.oleObject4 = new DataDynamics.ActiveReports.OleObject();
            this.label23 = new DataDynamics.ActiveReports.Label();
            this.txtBoxEtcDesc = new DataDynamics.ActiveReports.TextBox();
            this.line1 = new DataDynamics.ActiveReports.Line();
            this.textBox1 = new DataDynamics.ActiveReports.TextBox();
            this.label1 = new DataDynamics.ActiveReports.Label();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textRe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoxEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // label20
            // 
            this.label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label20.Height = 0.2F;
            this.label20.HyperLink = null;
            this.label20.Left = 2.884F;
            this.label20.Name = "label20";
            this.label20.Style = "font-weight: bold; text-align: center";
            this.label20.Text = "규격상한";
            this.label20.Top = 0.237F;
            this.label20.Width = 1.046F;
            // 
            // label19
            // 
            this.label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label19.Height = 0.2F;
            this.label19.HyperLink = null;
            this.label19.Left = 4.977F;
            this.label19.Name = "label19";
            this.label19.Style = "font-weight: bold; text-align: center";
            this.label19.Text = "평가결과";
            this.label19.Top = 0.238F;
            this.label19.Width = 1.137001F;
            // 
            // labelEtc
            // 
            this.labelEtc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelEtc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelEtc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelEtc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.labelEtc.Height = 0.2F;
            this.labelEtc.HyperLink = null;
            this.labelEtc.Left = 6.114F;
            this.labelEtc.Name = "labelEtc";
            this.labelEtc.Style = "font-weight: bold; text-align: center";
            this.labelEtc.Text = "판정";
            this.labelEtc.Top = 0.237F;
            this.labelEtc.Width = 1.062001F;
            // 
            // label21
            // 
            this.label21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label21.Height = 0.2F;
            this.label21.HyperLink = null;
            this.label21.Left = 1.781F;
            this.label21.Name = "label21";
            this.label21.Style = "font-weight: bold; text-align: center";
            this.label21.Text = "SAMPLE  SIZE";
            this.label21.Top = 0.238F;
            this.label21.Width = 1.103F;
            // 
            // label22
            // 
            this.label22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label22.Height = 0.2F;
            this.label22.HyperLink = null;
            this.label22.Left = 0F;
            this.label22.Name = "label22";
            this.label22.Style = "font-weight: bold; text-align: center";
            this.label22.Text = "인증항목";
            this.label22.Top = 0.237F;
            this.label22.Width = 1.781F;
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textRe,
            this.textSample,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox1});
            this.detail.Height = 0.4F;
            this.detail.Name = "detail";
            // 
            // textRe
            // 
            this.textRe.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textRe.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textRe.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textRe.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textRe.DataField = "CertiItem";
            this.textRe.Height = 0.4F;
            this.textRe.Left = 0F;
            this.textRe.Name = "textRe";
            this.textRe.Style = "font-size: 7.5pt; text-align: left; vertical-align: middle";
            this.textRe.Text = null;
            this.textRe.Top = 0F;
            this.textRe.Width = 1.781F;
            // 
            // textSample
            // 
            this.textSample.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSample.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSample.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSample.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textSample.DataField = "SampleSize";
            this.textSample.Height = 0.4F;
            this.textSample.Left = 1.781F;
            this.textSample.Name = "textSample";
            this.textSample.Style = "font-size: 7.5pt; text-align: right; vertical-align: middle";
            this.textSample.Text = null;
            this.textSample.Top = 0F;
            this.textSample.Width = 1.103F;
            // 
            // textBox3
            // 
            this.textBox3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.DataField = "UpperSpec";
            this.textBox3.Height = 0.4F;
            this.textBox3.Left = 2.884F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "font-size: 7.5pt; text-align: right; vertical-align: middle";
            this.textBox3.Text = null;
            this.textBox3.Top = 0F;
            this.textBox3.Width = 1.046F;
            // 
            // textBox4
            // 
            this.textBox4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.DataField = "EvalResultCode";
            this.textBox4.Height = 0.4F;
            this.textBox4.Left = 4.977F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "font-size: 7.5pt; vertical-align: middle";
            this.textBox4.Text = null;
            this.textBox4.Top = 0F;
            this.textBox4.Width = 1.137F;
            // 
            // textBox5
            // 
            this.textBox5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.DataField = "JudgeResultName";
            this.textBox5.Height = 0.4F;
            this.textBox5.Left = 6.114F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "font-size: 7.5pt; text-align: center; vertical-align: middle";
            this.textBox5.Text = null;
            this.textBox5.Top = 0F;
            this.textBox5.Width = 1.062F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.crossSectionLine1,
            this.crossSectionLine2,
            this.label18,
            this.label22,
            this.label19,
            this.labelEtc,
            this.label21,
            this.label20,
            this.label1});
            this.groupHeader1.Height = 0.438F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0.531F;
            this.crossSectionLine1.Left = 7.181F;
            this.crossSectionLine1.LineWeight = 1F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 0.237F;
            // 
            // crossSectionLine2
            // 
            this.crossSectionLine2.Bottom = 0.531F;
            this.crossSectionLine2.Left = 0F;
            this.crossSectionLine2.LineWeight = 2F;
            this.crossSectionLine2.Name = "crossSectionLine2";
            this.crossSectionLine2.Top = 0.237F;
            // 
            // label18
            // 
            this.label18.Height = 0.2F;
            this.label18.HyperLink = null;
            this.label18.Left = 0F;
            this.label18.Name = "label18";
            this.label18.Style = "";
            this.label18.Text = "5 . 설비 인증 항목 및 평가 결과";
            this.label18.Top = 0F;
            this.label18.Width = 2.134F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.oleObject4,
            this.label23,
            this.txtBoxEtcDesc,
            this.line1});
            this.groupFooter1.Height = 0.53125F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // oleObject4
            // 
            this.oleObject4.Class = "Paint.Picture";
            this.oleObject4.DataValue = ((System.IO.Stream)(resources.GetObject("oleObject4.DataValue")));
            this.oleObject4.Height = 0.117F;
            this.oleObject4.Left = 0.08000001F;
            this.oleObject4.Name = "oleObject4";
            this.oleObject4.Top = 0.083F;
            this.oleObject4.Width = 0.113F;
            // 
            // label23
            // 
            this.label23.Height = 0.2F;
            this.label23.HyperLink = null;
            this.label23.Left = 0.238F;
            this.label23.Name = "label23";
            this.label23.Style = "font-size: 10pt; font-weight: normal";
            this.label23.Text = "비   고  :";
            this.label23.Top = 0.04F;
            this.label23.Width = 0.679F;
            // 
            // txtBoxEtcDesc
            // 
            this.txtBoxEtcDesc.Height = 0.357F;
            this.txtBoxEtcDesc.Left = 0.9170001F;
            this.txtBoxEtcDesc.Name = "txtBoxEtcDesc";
            this.txtBoxEtcDesc.Style = "font-size: 9pt";
            this.txtBoxEtcDesc.Text = null;
            this.txtBoxEtcDesc.Top = 0.053F;
            this.txtBoxEtcDesc.Width = 6.233F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.531F;
            this.line1.Width = 7.181F;
            this.line1.X1 = 0F;
            this.line1.X2 = 7.181F;
            this.line1.Y1 = 0.531F;
            this.line1.Y2 = 0.531F;
            // 
            // textBox1
            // 
            this.textBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.DataField = "LowerSpec";
            this.textBox1.Height = 0.4F;
            this.textBox1.Left = 3.93F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-size: 7.5pt; text-align: right; vertical-align: middle";
            this.textBox1.Text = null;
            this.textBox1.Top = 0F;
            this.textBox1.Width = 1.046F;
            // 
            // label1
            // 
            this.label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 3.931F;
            this.label1.Name = "label1";
            this.label1.Style = "font-weight: bold; text-align: center";
            this.label1.Text = "규격하한";
            this.label1.Top = 0.237F;
            this.label1.Width = 1.046F;
            // 
            // rptQATZ0002_S01
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.208333F;
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textRe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoxEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.TextBox textRe;
        private DataDynamics.ActiveReports.TextBox textSample;
        private DataDynamics.ActiveReports.TextBox textBox3;
        private DataDynamics.ActiveReports.TextBox textBox4;
        private DataDynamics.ActiveReports.TextBox textBox5;
        private DataDynamics.ActiveReports.Label label20;
        private DataDynamics.ActiveReports.Label label19;
        private DataDynamics.ActiveReports.Label labelEtc;
        private DataDynamics.ActiveReports.Label label21;
        private DataDynamics.ActiveReports.Label label22;
        private DataDynamics.ActiveReports.GroupHeader groupHeader1;
        private DataDynamics.ActiveReports.GroupFooter groupFooter1;
        private DataDynamics.ActiveReports.OleObject oleObject4;
        private DataDynamics.ActiveReports.Label label23;
        private DataDynamics.ActiveReports.TextBox txtBoxEtcDesc;
        private DataDynamics.ActiveReports.CrossSectionLine crossSectionLine1;
        private DataDynamics.ActiveReports.CrossSectionLine crossSectionLine2;
        private DataDynamics.ActiveReports.Line line1;
        private DataDynamics.ActiveReports.Label label18;
        private DataDynamics.ActiveReports.TextBox textBox1;
        private DataDynamics.ActiveReports.Label label1;
    }
}
