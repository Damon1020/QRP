﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 고객관리                                              */
/* 모듈(분류)명 : 고객불만관리                                          */
/* 프로그램ID   : frmQAT0002.cs                                         */
/* 프로그램명   : 내부공정사고 등록                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

// 파일첨부
using System.IO;
using System.Collections;

namespace QRPQAT.UI
{
    public partial class frmQATZ0013 : Form, IToolbar
    {
        // 리소스 사용을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmQATZ0013()
        {
            InitializeComponent();
        }

        private void frmQATZ0013_Activated(object sender, EventArgs e)
        {
            // 툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQATZ0013_Load(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀지정

            titleArea.mfSetLabelText("내부공정사고 등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화

            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitButton();
            InitGrid();
            InitComboBox();
            initEtc();

            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmQATZ0013_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region IToolbar 멤버
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded.Equals(false) ||
                    this.uComboPlant.Value.ToString().Equals(string.Empty) ||
                    this.uTextManagementNo.Text.Trim().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    ////// 그리드에 내용이 없을시 조회 메소드 호출
                    ////if (!(this.uGridInprocAccidentList.Rows.Count > 0))
                    ////    mfSearch();

                    return;
                }
                else
                {
                    // 삭제여부를 묻는다

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 변수 설정
                        string strPlantCode = this.uComboPlant.Value.ToString();
                        string strManagementNo = this.uTextManagementNo.Text;

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATIPA.InProcessAccidentH), "InProcessAccidentH");
                        QRPQAT.BL.QATIPA.InProcessAccidentH clsHeader = new QRPQAT.BL.QATIPA.InProcessAccidentH();
                        brwChannel.mfCredentials(clsHeader);

                        string strErrRtn = clsHeader.mfDeleteQATInProcessAccidentH(strPlantCode, strManagementNo);

                        // 결과검사

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if(ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638"
                                                        , "M000923",
                                                        Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638"
                                                        , ErrRtn.ErrMessage,
                                                        Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridInprocAccidentList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridInprocAccidentList);
                    if (this.uGridDetailList.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridDetailList);
                    }
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");
                // 입력사항 확인
                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000881"
                                                , "M001048"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (this.uCheckCompleteFlag.Enabled.Equals(false))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000881"
                                                , "M000993"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (this.uDateAriseDate.Value == DBNull.Value || this.uDateAriseDate.Value == null || this.uDateAriseDate.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000881"
                                                , "M000433"
                                                , Infragistics.Win.HAlign.Right);
                    
                    this.uDateAriseDate.DropDown();
                    return;
                }
                else if (this.uComboAriseProcessCode.Value == DBNull.Value || this.uComboAriseProcessCode.Value == null || string.IsNullOrEmpty(this.uComboAriseProcessCode.Value.ToString()))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000881"
                                                , "M000432"
                                                , Infragistics.Win.HAlign.Right);
                    
                    this.uComboAriseProcessCode.DropDown();
                    return;
                }
                else if (!(this.uGridDetailList.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000881"
                                                , "M001058"
                                                , Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (this.uTextFilePath.Text.Contains("/")
                        || this.uTextFilePath.Text.Contains("#")
                        || this.uTextFilePath.Text.Contains("*")
                        || this.uTextFilePath.Text.Contains("<")
                        || this.uTextFilePath.Text.Contains(">"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                      , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }
                else
                {
                    // 상세정보 입력 확인
                    if (this.uGridDetailList.Rows.Count > 0)
                    {
                        this.uGridDetailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                        for (int i = 0; i < this.uGridDetailList.Rows.Count; i++)
                        {
                            if (string.IsNullOrEmpty(this.uGridDetailList.Rows[i].Cells["CustomerCode"].Value.ToString()) || this.uGridDetailList.Rows[i].Cells["CustomerCode"].Value == DBNull.Value)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000881",strLang)
                                                    , (i + 1).ToString() + msg.GetMessge_Text("M000589",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridDetailList.Rows[i].Cells["CustomerCode"].Activate();
                                this.uGridDetailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            else if (string.IsNullOrEmpty(this.uGridDetailList.Rows[i].Cells["Package"].Value.ToString()) || this.uGridDetailList.Rows[i].Cells["Package"].Value == DBNull.Value)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000881",strLang)
                                                    , (i + 1).ToString() + msg.GetMessge_Text("M000584",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridDetailList.Rows[i].Cells["Package"].Activate();
                                this.uGridDetailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            else if (string.IsNullOrEmpty(this.uGridDetailList.Rows[i].Cells["LotNo"].Value.ToString()) || this.uGridDetailList.Rows[i].Cells["LotNo"].Value == DBNull.Value)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000881",strLang)
                                                    , (i + 1).ToString() + msg.GetMessge_Text("M000582",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridDetailList.Rows[i].Cells["LotNo"].Activate();
                                this.uGridDetailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else if (string.IsNullOrEmpty(this.uGridDetailList.Rows[i].Cells["LotQty"].Value.ToString()) || this.uGridDetailList.Rows[i].Cells["LotQty"].Value == DBNull.Value)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000881",strLang)
                                                    , (i + 1).ToString() + msg.GetMessge_Text("M000581",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridDetailList.Rows[i].Cells["LotQty"].Activate();
                                this.uGridDetailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else if (string.IsNullOrEmpty(this.uGridDetailList.Rows[i].Cells["LossQty"].Value.ToString()) || this.uGridDetailList.Rows[i].Cells["LossQty"].Value == DBNull.Value)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001264",strLang)
                                                    , msg.GetMessge_Text("M000881",strLang)
                                                    , (i + 1).ToString() + msg.GetMessge_Text("M000583",strLang)
                                                    , Infragistics.Win.HAlign.Right);

                                this.uGridDetailList.Rows[i].Cells["LossQty"].Activate();
                                this.uGridDetailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                        }
                    }

                    // 콤보박스 상태 확인
                    QRPCOM.QRPUI.CommonControl clsCom = new CommonControl();
                    if (!clsCom.mfCheckValidValueBeforSave(this))
                        return;

                    string strMsg = string.Empty;
                    if (this.uCheckCompleteFlag.Checked)
                    {
                        strMsg = "M000983";
                    }
                    else
                    {
                        strMsg = "M000905";
                    }
                    // 저장확인창
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053"
                                        , strMsg, Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 저장용 데이터 테이블

                        DataTable dtHeader = Rtn_HeaderData();
                        DataTable dtDetail = Rtn_DetailData();

                        // 프로그래스바 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATIPA.InProcessAccidentH), "InProcessAccidentH");
                        QRPQAT.BL.QATIPA.InProcessAccidentH clsHeader = new QRPQAT.BL.QATIPA.InProcessAccidentH();
                        brwChannel.mfCredentials(clsHeader);

                        // 저장메소드 호출
                        string strErrRtn = clsHeader.mfSaveQATInProcessAccidentH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail);

                        // POPUP창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            string strManagementNo = ErrRtn.mfGetReturnValue(0);

                            // Call FileUpload Method
                            if (this.uTextFilePath.Text.Length > 0)
                            {
                                FileUpload(strManagementNo, this.uTextFilePath.Text, false);
                            }
                            if (this.uTextCAAttachFile.Text.Length > 0)
                            {
                                FileUpload(strManagementNo, this.uTextCAAttachFile.Text, true);
                            }

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            string strMes = "";

                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                                strMes = msg.GetMessge_Text("M000953", strLang);
                            else
                                strMes = ErrRtn.ErrMessage;


                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001135",strLang)
                                                , msg.GetMessge_Text("M001037",strLang), strMes
                                                , Infragistics.Win.HAlign.Right);
                            
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 조회조건 확인
                if (Convert.ToDateTime(this.uDateSearchFromAriseDate.Value).CompareTo(Convert.ToDateTime(this.uDateSearchToAriseDate.Value)) > 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001112"
                                                    , "M000049"
                                                    , Infragistics.Win.HAlign.Right);

                    this.uDateSearchToAriseDate.DropDown();
                    return;
                }

                // 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strAriseFromDate = Convert.ToDateTime(this.uDateSearchFromAriseDate.Value).ToString("yyyy-MM-dd");
                string strAriseToDate = Convert.ToDateTime(this.uDateSearchToAriseDate.Value).ToString("yyyy-MM-dd");
                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strLotNo = this.uTextSearchLotNo.Text;
                string strDetailProcessOperationType = this.uComboSearchDetailProcessOperationType.Value.ToString();
                string strInspectFaultItemCode = this.uComboSearchInspectFaultTypeCode.Value.ToString();

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATIPA.InProcessAccidentH), "InProcessAccidentH");
                QRPQAT.BL.QATIPA.InProcessAccidentH clsHeader = new QRPQAT.BL.QATIPA.InProcessAccidentH();
                brwChannel.mfCredentials(clsHeader);

                // Call Method
                DataTable dtSearch = clsHeader.mfReadQATInProcessAccidentH(strPlantCode, strAriseFromDate, strAriseToDate, strCustomerCode, strPackage
                                                                        , strLotNo, strDetailProcessOperationType, strInspectFaultItemCode, m_resSys.GetString("SYS_LANG"));

                this.uGridInprocAccidentList.DataSource = dtSearch;
                this.uGridInprocAccidentList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtSearch.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridInprocAccidentList, 0);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001101"
                                                    , "M001102"
                                                    , Infragistics.Win.HAlign.Right);
                }

                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxAriseInfo, GroupBoxType.INFO, "발생정보", m_resSys.GetString("SYS_FONTNAME")
                                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxAriseInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxAriseInfo.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBoxReason_Action, GroupBoxType.INFO, "원인/대책", m_resSys.GetString("SYS_FONTNAME")
                                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxReason_Action.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxReason_Action.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBoxCA, GroupBoxType.INFO, "CA 유효성 점검", m_resSys.GetString("SYS_FONTNAME")
                                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxCA.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxCA.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                // 검색조건

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchAriseDate, "발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDetailProcessOperationType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectFaultType, "불량명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 - 발생정보
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelManagementNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMgnNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseDate, "발생일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelIssueDate, "Issue일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectFaultType, "불량명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseProcess, "발생공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseEquip, "발생설비", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // 상세 - 원인/대책                
                wLabel.mfSetLabel(this.uLabelReasonDesc, "원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterialTreat, "자재처리", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReasonDate, "원인/대책 접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelActionDesc, "대책", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelActionDate, "최종완료일", m_resSys.GetString("SYS_FONTANME"), true, false);
                wLabel.mfSetLabel(this.uLabelFilePath, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel4ME, "4M+1E", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteFlag, "최종완료", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //CA
                wLabel.mfSetLabel(this.uLabelRegistUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCAInspectDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCAAttachFile, "CA점검 첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectResult, "CA점검결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                #region 콤보박스용 DataTable DB 조회
                // 공장
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 고객
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                #endregion

                #region 검색조건

                // 공장
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE")
                    , "", "전체", "PlantCode", "PlantName", dtPlant);

                // 고객사

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "CustomerCode", "CustomerName", dtCustomer);
                #endregion

                #region 상세
                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE")
                    , "", "선택", "PlantCode", "PlantName", dtPlant);
                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridInprocAccidentList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true
                                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "ManagementNo", "관리번호", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false
                                    , 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "AriseDate", "발생일", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "IssueDate", "Issue일", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "Customername", "고객사", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "Package", "Package", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false
                                    , 40, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 40, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "AriseEquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                //                    , 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                //                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                    , 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "LotQty", "In Qty", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false
                                    , 0, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "LossQty", "Out Qty", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false
                                    , 0, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "Yield", "Yield", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false
                                    , 0, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn.nnn", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "FaultTypeName", "불량명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false
                                    , 0, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "ReasonDesc", "원인", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false
                                    , 0, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "ActionDesc", "대책", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false
                                    , 0, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "ReasonDate", "원인 / 대책 접수일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                                    ,   0, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "ActionTAT", "대책접수TAT", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "MaterialTAT", "자재처리TAT", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "CompleteDate", "완료일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "ManFlag", "Man", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "MethodFlag", "Method", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "MaterialFlag", "Material", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "MachineFlag", "Machine", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInprocAccidentList, 0, "EnviromentFlag", "Enviro", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false
                                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 그리드 편집불가 상태로

                this.uGridInprocAccidentList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                //Font 설정
                this.uGridInprocAccidentList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridInprocAccidentList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridInprocAccidentList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
                #endregion

                #region 상세화면
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDetailList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 40
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 40
                                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "LotQty", "In Qty", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false
                                    , 0, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "LossQty", "Out Qty", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false
                                    , 0, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "Yield", "Yield", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false
                                    , 0, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn.nnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridDetailList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false
                                    , 500, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "","", "");

                // 대문자 입력
                this.uGridDetailList.DisplayLayout.Bands[0].Columns["LotNo"].ImeMode = ImeMode.Disable;
                this.uGridDetailList.DisplayLayout.Bands[0].Columns["LotNo"].CharacterCasing = CharacterCasing.Upper;

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridDetailList, 0);

                // 고객사 콤보 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDetailList, 0, "CustomerCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCustomer);

                // 패키지 콤보 설정
                DataTable dtPackage = new DataTable();
                wGrid.mfSetGridColumnValueList(this.uGridDetailList, 0, "Package", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPackage);

                // 설비 콤보 설정
                DataTable dtEquip = new DataTable();
                wGrid.mfSetGridColumnValueList(this.uGridDetailList, 0, "EquipCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtEquip);

                //Font 설정
                this.uGridDetailList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridDetailList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화

        /// </summary>
        private void initEtc()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region 텍스트박스

                // 최대길이 설정
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextReasonDesc.MaxLength = 1000;
                this.uTextActionDesc.MaxLength = 1000;
                this.uTextFilePath.MaxLength = 1000;

                // 항상 대문자로 입력되게 설정
                this.uTextSearchLotNo.ImeMode = ImeMode.Disable;
                this.uTextSearchLotNo.CharacterCasing = CharacterCasing.Upper;
                #endregion

                #region DateTime
                // 기본값(오늘날짜)
                this.uDateSearchFromAriseDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateSearchToAriseDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateAriseDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateIssueDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateCAInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");

                this.uDateAriseDate.Appearance.BackColor = Color.PowderBlue;
                this.uDateAriseDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                this.uDateIssueDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                this.uDateSearchFromAriseDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                this.uDateSearchToAriseDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Events...
        #region Combo Events
        // 공장코드에 따라서 값이 변하는 컨트롤 설정 이벤트

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinGrid wGrid = new WinGrid();

                // 공장코드
                string strPlantCode = this.uComboPlant.Value.ToString();

                // 데이터 테이블 생성
                DataTable dtProcess = new DataTable();
                DataTable dtPackage = new DataTable();
                DataTable dtEquip = new DataTable();
                DataTable dtInspectFault = new DataTable();
                
                // 콤보박스 초기화

                this.uComboAriseEquipCode.Items.Clear();
                this.uComboAriseProcessCode.Items.Clear();
                this.uComboInspectFaultTypeCode.Items.Clear();
                while (this.uGridDetailList.Rows.Count > 0)
                {
                    this.uGridDetailList.Rows[0].Delete(false);
                }

                if (!strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    
                    // 공정
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProc);
                    dtProcess = clsProc.mfReadProcessDetailProcessOperationType(strPlantCode);

                    // Package
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);
                    dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 설비
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    dtEquip = clsEquip.mfReadEquipForPlantCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 불량명

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsInspectFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsInspectFaultType);
                    dtInspectFault = clsInspectFaultType.mfReadInspectFaultTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                // 공정
                wCombo.mfSetComboEditor(this.uComboAriseProcessCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, "", "", "선택", "DetailProcessOperationType", "ComboName", dtProcess);

                // 상세화면(헤더) : 설비
                wCombo.mfSetComboEditor(this.uComboAriseEquipCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, "", "", "", "EquipCode", "EquipName", dtEquip);

                // 불량명

                wCombo.mfSetComboEditor(this.uComboInspectFaultTypeCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, "", "", "전체", "InspectFaultTypeCode", "InspectFaultTypeName", dtInspectFault);

                // Package
                wGrid.mfSetGridColumnValueList(this.uGridDetailList, 0, "Package", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPackage);
                
                // 상세화면(아이템) : 설비
                wGrid.mfSetGridColumnValueList(this.uGridDetailList, 0, "EquipCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtEquip);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 공장코드 변경에 따라 값이 변하는 컨트롤 설정 이벤트

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinGrid wGrid = new WinGrid();

                // 공장코드
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                // 데이터 테이블 생성
                DataTable dtProcType = new DataTable();
                DataTable dtPackage = new DataTable();
                DataTable dtInspectFault = new DataTable();

                // 콤보박스 초기화

                this.uComboSearchPackage.Items.Clear();
                this.uComboSearchDetailProcessOperationType.Items.Clear();
                this.uComboSearchInspectFaultTypeCode.Items.Clear();

                if (!strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();

                    // 공정
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProc);
                    dtProcType = clsProc.mfReadProcessDetailProcessOperationType(strPlantCode);

                    // Package
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);
                    dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 불량명

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsInspectFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsInspectFaultType);
                    dtInspectFault = clsInspectFaultType.mfReadInspectFaultTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                // 공정Type
                wCombo.mfSetComboEditor(this.uComboSearchDetailProcessOperationType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, "", "", "전체", "DetailProcessOperationType", "ComboName", dtProcType);

                // 불량명

                wCombo.mfSetComboEditor(this.uComboSearchInspectFaultTypeCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, "", "", "전체", "InspectFaultTypeCode", "InspectFaultTypeName", dtInspectFault);

                // Package
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, "", "", "전체", "Package", "ComboName", dtPackage);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// 상세그리드 수량 입력시 수율 자동계산 이벤트

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridDetailList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("LotQty") || e.Cell.Column.Key.Equals("LossQty"))
                {
                    if (e.Cell.Row.Cells["LotQty"].Value == null || e.Cell.Row.Cells["LotQty"].Value == DBNull.Value)
                        return;

                    if (Convert.ToDecimal(e.Cell.Row.Cells["LotQty"].Value).Equals(0.0m))
                    {
                        e.Cell.Row.Cells["Yield"].Value = 0.0m;
                    }
                    else
                    {
                        //e.Cell.Row.Cells["Yield"].Value = Math.Round(100.0m - (Convert.ToDecimal(e.Cell.Row.Cells["LossQty"].Value) / Convert.ToDecimal(e.Cell.Row.Cells["LotQty"].Value) * 100), 4, MidpointRounding.AwayFromZero);\
                        ////e.Cell.Row.Cells["Yield"].Value = Math.Round(Convert.ToDecimal(e.Cell.Row.Cells["LossQty"].Value.ToString()) / Convert.ToDecimal(e.Cell.Row.Cells["LotQty"].Value.ToString()) * 100, 4, MidpointRounding.AwayFromZero);

                        double dbInQty = Convert.ToDouble(e.Cell.Row.Cells["LotQty"].Value.ToString());
                        double dbOutQty = Convert.ToDouble(e.Cell.Row.Cells["LossQty"].Value.ToString());

                        //e.Cell.Row.Cells["Yield"].Value = (dbInQty - dbOutQty) / dbInQty * 100;
                        e.Cell.Row.Cells["Yield"].Value = dbOutQty / dbInQty * 100;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 그리드 더블클릭시 상세정보 조회 하는 이벤트

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridInprocAccidentList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                e.Row.Fixed = true;

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 변수 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strManagementNo = e.Row.Cells["ManagementNo"].Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");

                // 조회 메소드 호출
                Search_HeaderDate(strPlantCode, strManagementNo, strLang);
                Search_DetailDate(strPlantCode, strManagementNo, strLang);

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region GroupBox Events
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInprocAccidentList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInprocAccidentList.Height = 720;
                    for (int i = 0; i < this.uGridInprocAccidentList.Rows.Count; i++)
                    {
                        this.uGridInprocAccidentList.Rows[i].Fixed = false;
                    }

                    // 컨트롤 초기화

                    Clear();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Etc Events
        /// <summary>
        /// 행삭제 버튼 클릭 이벤트

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridDetailList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDetailList.Rows[i].Cells["Check"].Value))
                    {
                        this.uGridDetailList.Rows[i].Delete(false);
                        i--;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 텍스트박스 버튼 이벤트

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextFilePath_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.Equals("UP"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextFilePath.Text = strImageFile;
                    }
                }
                else if (e.Button.Key.Equals("DOWN"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextFilePath.Text.Contains(":\\") || this.uTextFilePath.Text.Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000796",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0027");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextFilePath.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기

                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFilePath.Text);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
        #endregion

        #region Methods...
        #region 저장정보 반환 메소드

        /// <summary>
        /// 헤더저장정보 반환 메소드

        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_HeaderData()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATIPA.InProcessAccidentH), "InProcessAccidentH");
                QRPQAT.BL.QATIPA.InProcessAccidentH clsHeader = new QRPQAT.BL.QATIPA.InProcessAccidentH();
                brwChannel.mfCredentials(clsHeader);

                dtRtn = clsHeader.mfSetDataInfo();
                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                drRow["ManagementNo"] = this.uTextManagementNo.Text;
                drRow["AriseDate"] = Convert.ToDateTime(this.uDateAriseDate.Value).ToString("yyyy-MM-dd");
                if (this.uDateIssueDate.Value == null || this.uDateIssueDate.Value == DBNull.Value)
                    drRow["IssueDate"] = DBNull.Value;
                else
                    drRow["IssueDate"] = Convert.ToDateTime(this.uDateIssueDate.Value).ToString("yyyy-MM-dd");
                drRow["InspectFaultTypeCode"] = this.uComboInspectFaultTypeCode.Value.ToString();
                drRow["AriseDetailProcessOperationTypeCode"] = this.uComboAriseProcessCode.Value.ToString();
                drRow["AriseEquipCode"] = this.uComboAriseEquipCode.Value.ToString();
                drRow["ReasonDesc"] = this.uTextReasonDesc.Text;
                drRow["ActionDesc"] = this.uTextActionDesc.Text;
                drRow["MaterialTreat"] = this.uTextMaterialTreat.Text;
                if (this.uTextFilePath.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextFilePath.Text);
                    drRow["FilePath"] = fileDoc.Name;
                }
                else
                {
                    drRow["FilePath"] = this.uTextFilePath.Text;
                }
                drRow["ManFlag"] = this.uCheckManFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MethodFlag"] = this.uCheckMethodFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MachineFlag"] = this.uCheckMachineFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MaterialFlag"] = this.uCheckMaterialFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["EnviromentFlag"] = this.uCheckEnviromentFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["CompleteFlag"] = this.uCheckCompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ReasonDate"] = Convert.ToDateTime(this.uDateReasonDate.Value).ToString("yyyy-MM-dd");
                drRow["ActionDate"] = Convert.ToDateTime(this.uDateActionDate.Value).ToString("yyyy-MM-dd");
                drRow["InspectDate"] = Convert.ToDateTime(this.uDateCAInspectDate.Value).ToString("yyyy-MM-dd");
                drRow["InspectResult"] = this.uTextInspectResult.Text;
                if (this.uTextCAAttachFile.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextCAAttachFile.Text);
                    drRow["CAAttachFile"] = fileDoc.Name.ToString();
                }
                else
                {
                    drRow["CAAttachFile"] = this.uTextCAAttachFile.Text;
                }
                drRow["InspectUserID"] = this.uTextRegistUserID.Text;
                dtRtn.Rows.Add(drRow);
                return dtRtn;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 상세저장정보 반환메소드

        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_DetailData()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATIPA.InProcessAccidentD), "InProcessAccidentD");
                QRPQAT.BL.QATIPA.InProcessAccidentD clsDetail = new QRPQAT.BL.QATIPA.InProcessAccidentD();
                brwChannel.mfCredentials(clsDetail);

                dtRtn = clsDetail.mfSetDataInfo();
                DataRow drRow;

                if (this.uGridDetailList.Rows.Count > 0)
                    this.uGridDetailList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                for (int i = 0; i < this.uGridDetailList.Rows.Count; i++)
                {
                    if (this.uGridDetailList.Rows[i].Hidden.Equals(false))
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["ManagementNo"] = this.uTextManagementNo.Text;
                        drRow["CustomerCode"] = this.uGridDetailList.Rows[i].Cells["CustomerCode"].Value.ToString();
                        drRow["Package"] = this.uGridDetailList.Rows[i].Cells["Package"].Value.ToString();
                        drRow["LotNo"] = this.uGridDetailList.Rows[i].Cells["LotNo"].Value.ToString();
                        drRow["EquipCode"] = this.uGridDetailList.Rows[i].Cells["EquipCode"].Value.ToString();
                        drRow["LotQty"] = this.uGridDetailList.Rows[i].Cells["LotQty"].Value;
                        drRow["LossQty"] = this.uGridDetailList.Rows[i].Cells["LossQty"].Value;
                        drRow["Yield"] = this.uGridDetailList.Rows[i].Cells["Yield"].Value;
                        drRow["EtcDesc"] = this.uGridDetailList.Rows[i].Cells["EtcDesc"].Value.ToString();
                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }
        #endregion

        #region 조회 메소드

        /// <summary>
        /// 헤더 상세정보 조회 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManagementNo">관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_HeaderDate(string strPlantCode, string strManagementNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATIPA.InProcessAccidentH), "InProcessAccidentH");
                QRPQAT.BL.QATIPA.InProcessAccidentH clsHeader = new QRPQAT.BL.QATIPA.InProcessAccidentH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtHeader = clsHeader.mfReadQATInProcessAccidentH_Detail(strPlantCode, strManagementNo, strLang);

                if (dtHeader.Rows.Count > 0)
                {
                    this.uComboPlant.Value = dtHeader.Rows[0]["PlantCode"];
                    this.uTextManagementNo.Text = dtHeader.Rows[0]["ManagementNo"].ToString();
                    this.uTextMgnNo.Text = dtHeader.Rows[0]["ManagementNo"].ToString();
                    this.uDateAriseDate.Value = Convert.ToDateTime(dtHeader.Rows[0]["AriseDate"]).ToString("yyyy-MM-dd");
                    if (string.IsNullOrEmpty(dtHeader.Rows[0]["IssueDate"].ToString()) || dtHeader.Rows[0]["IssueDate"] == DBNull.Value)
                        this.uDateIssueDate.Value = null;
                    else
                        this.uDateIssueDate.Value = Convert.ToDateTime(dtHeader.Rows[0]["IssueDate"]).ToString("yyyy-MM-dd");
                    this.uComboInspectFaultTypeCode.Value = dtHeader.Rows[0]["InspectFaultTypeCode"];
                    this.uComboAriseProcessCode.Value = dtHeader.Rows[0]["AriseDetailProcessOperationTypeCode"];
                    this.uComboAriseEquipCode.Value = dtHeader.Rows[0]["AriseEquipCode"];
                    this.uTextReasonDesc.Text = dtHeader.Rows[0]["ReasonDesc"].ToString();
                    this.uTextActionDesc.Text = dtHeader.Rows[0]["ActionDesc"].ToString();
                    this.uTextMaterialTreat.Text = dtHeader.Rows[0]["MaterialTreat"].ToString();
                    this.uTextFilePath.Text = dtHeader.Rows[0]["FilePath"].ToString();
                    this.uCheckManFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["ManFlag"]);
                    this.uCheckMethodFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["MethodFlag"]);
                    this.uCheckMachineFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["MachineFlag"]);
                    this.uCheckMaterialFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["MaterialFlag"]);
                    this.uCheckEnviromentFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["EnviromentFlag"]);
                    this.uCheckCompleteFlag.Checked = Convert.ToBoolean(dtHeader.Rows[0]["CompleteFlag"]);
                    this.uDateReasonDate.Value = dtHeader.Rows[0]["ReasonDate"].ToString();
                    this.uDateActionDate.Value = dtHeader.Rows[0]["ActionDate"].ToString();
                    this.uDateCAInspectDate.Value = dtHeader.Rows[0]["InspectDate"].ToString();
                    this.uTextInspectResult.Text = dtHeader.Rows[0]["InspectResult"].ToString();
                    this.uTextCAAttachFile.Text = dtHeader.Rows[0]["CAAttachFile"].ToString();
                    this.uTextRegistUserID.Text = dtHeader.Rows[0]["InspectUserID"].ToString();
                    this.uTextRegistUserName.Text = dtHeader.Rows[0]["IsnepctUserName"].ToString();


                    // 작성완료된 정보인 경우
                    if (Convert.ToBoolean(dtHeader.Rows[0]["CompleteFlag"]))
                    {
                        this.uCheckCompleteFlag.Enabled = !Convert.ToBoolean(dtHeader.Rows[0]["CompleteFlag"]);
                        this.uTextFilePath.ButtonsRight["UP"].Visible = false;
                        this.uGridDetailList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                        this.uDateAriseDate.ReadOnly = true;
                        this.uComboAriseProcessCode.ReadOnly = true;
                        this.uDateIssueDate.ReadOnly = true;
                        this.uTextReasonDesc.ReadOnly = true;
                        this.uTextActionDesc.ReadOnly = true;
                        this.uTextMaterialTreat.ReadOnly = true;
                        this.uDateActionDate.ReadOnly = true;
                        this.uDateReasonDate.ReadOnly = true;
                        this.uCheckEnviromentFlag.Enabled = false;
                        this.uCheckMachineFlag.Enabled = false;
                        this.uCheckManFlag.Enabled = false;
                        this.uCheckMaterialFlag.Enabled = false;
                        this.uCheckMethodFlag.Enabled = false;
                        this.uGridDetailList.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                    }
                    else
                    {
                        this.uTextReasonDesc.ReadOnly = false;
                        this.uTextActionDesc.ReadOnly = false;
                        this.uTextMaterialTreat.ReadOnly = false;
                        this.uDateActionDate.ReadOnly = false;
                        this.uDateReasonDate.ReadOnly = false;
                        this.uCheckEnviromentFlag.Enabled = true;
                        this.uCheckMachineFlag.Enabled = true;
                        this.uCheckManFlag.Enabled = true;
                        this.uCheckMaterialFlag.Enabled = true;
                        this.uCheckMethodFlag.Enabled = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 조회 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManagementNo">관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_DetailDate(string strPlantCode, string strManagementNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATIPA.InProcessAccidentD), "InProcessAccidentD");
                QRPQAT.BL.QATIPA.InProcessAccidentD clsDetail = new QRPQAT.BL.QATIPA.InProcessAccidentD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtDetail = clsDetail.mfReadQATInProcessAccidentD(strPlantCode, strManagementNo, strLang);

                this.uGridDetailList.DataSource = dtDetail;
                this.uGridDetailList.DataBind();

                if (dtDetail.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridDetailList, 0);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 기타 메소드

        /// <summary>
        /// 초기화 메소드

        /// </summary>
        private void Clear()
        {
            try
            {
                if (!this.uCheckCompleteFlag.Enabled)
                {
                    this.uCheckCompleteFlag.Enabled = true;
                    this.uTextFilePath.ButtonsRight["UP"].Visible = true;
                    this.uGridDetailList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    this.uGridDetailList.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                }

                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                while (this.uGridDetailList.Rows.Count > 0)
                {
                    this.uGridDetailList.Rows[0].Delete(false);
                }

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextManagementNo.Clear();
                this.uTextMgnNo.Clear();
                this.uDateAriseDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateIssueDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uComboAriseProcessCode.Value = string.Empty;
                this.uComboAriseEquipCode.Value = string.Empty;
                this.uComboInspectFaultTypeCode.Value = string.Empty;
                this.uTextCAAttachFile.Text = string.Empty;
                this.uTextInspectResult.Text = string.Empty;
                this.uTextRegistUserID.Text = string.Empty;
                this.uTextRegistUserName.Text = string.Empty;

                this.uTextReasonDesc.Clear();
                this.uTextActionDesc.Clear();
                this.uTextFilePath.Clear();
                this.uCheckManFlag.Checked = false;
                this.uCheckManFlag.Enabled = true;
                this.uCheckMethodFlag.Checked = false;
                this.uCheckMethodFlag.Enabled = true;
                this.uCheckMachineFlag.Checked = false;
                this.uCheckMachineFlag.Enabled = true;
                this.uCheckEnviromentFlag.Checked = false;
                this.uCheckEnviromentFlag.Enabled = true;
                this.uCheckMaterialFlag.Checked = false;
                this.uCheckMaterialFlag.Enabled = true;
                this.uCheckCompleteFlag.Checked = false;
                this.uCheckCompleteFlag.Enabled = true;

                this.uDateReasonDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateActionDate.Value = "";
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 Upload 메소드

        /// </summary>
        /// <param name="strClaimNo">관리번호</param>
        private void FileUpload(string strManagementNo, string strFileName, bool bolCA)
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                // AttachmentFile
                if (strFileName.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(strFileName);
                    string strUploadFile = string.Empty;
                    if (bolCA == false)
                    {
                        strUploadFile = fileDoc.DirectoryName + "\\" +
                                               this.uComboPlant.Value.ToString() + "-" + strManagementNo + "-" + fileDoc.Name;
                    }
                    else
                    {
                        strUploadFile = fileDoc.DirectoryName + "\\" +
                                                this.uComboPlant.Value.ToString() + "-" + strManagementNo + "-CAReport-" + fileDoc.Name;
                    }
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(strFileName, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                // 업로드할 파일이 있는경우 파일 업로드

                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    // 첨부파일 저장경로정보 가져오기

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0027");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uCheckCompleteFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked == true)
                    this.uDateActionDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                else
                    this.uDateActionDate.Value = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        //CA 첨부파일 업로드 / 다운로드 버튼 클릭
        private void uTextCAAttachFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.Equals("up"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files(*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextCAAttachFile.Text = strImageFile;
                    }
                }
                else if (e.Button.Key.Equals("down"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextCAAttachFile.Text.Contains(":\\") || this.uTextCAAttachFile.Text.Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000796",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), "S02");

                        //첨부파일 저장경로정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_resSys.GetString("SYS_PLANTCODE"), "D0027");
                        
                        //Download
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextCAAttachFile.Text);

                        //Download 정보
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기

                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextCAAttachFile.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextRegistUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uTextRegistUserID.ReadOnly == false)
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                    frmPOP.ShowDialog();

                    this.uTextRegistUserID.Text = frmPOP.UserID;
                    this.uTextRegistUserName.Text = frmPOP.UserName;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextRegistUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextRegistUserID.ReadOnly == false)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (!this.uTextRegistUserID.Text.Equals(string.Empty))
                        {
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                            QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                            brwChannel.mfCredentials(clsUser);

                            DataTable dtRtnName = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), this.uTextRegistUserID.Text, m_resSys.GetString("SYSLANG"));

                            if (dtRtnName.Rows.Count > 0)
                            {
                                this.uTextRegistUserName.Text = dtRtnName.Rows[0]["UserName"].ToString();
                            }
                            else
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000199", "M000203", "M000621", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                    else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                    {
                        this.uTextRegistUserName.Text = string.Empty;
                        this.uTextRegistUserID.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
        }
    }
}
