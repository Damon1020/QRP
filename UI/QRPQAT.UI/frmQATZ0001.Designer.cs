﻿namespace QRPQAT.UI
{
    partial class frmQATZ0001
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQATZ0001));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchQualType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchQualType = new Infragistics.Win.Misc.UltraLabel();
            this.uDateWrtiteDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateWriteDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridQualList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxEmail = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDel_Email = new Infragistics.Win.Misc.UltraButton();
            this.uGridEmail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionFinalInspectResultFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uDateFinalCompleteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelFinalCompleteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFinalInspectResultFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxEvidenceInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDel_EvidenceList = new Infragistics.Win.Misc.UltraButton();
            this.uButtonFileDown_EvidenceList = new Infragistics.Win.Misc.UltraButton();
            this.uGridEvidenceList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxStdInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGroupBoxPCN = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelChangeBefore = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelChangeAfter = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChangeAfter = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextChangeBefore = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBoxBomList = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDel_MatList = new Infragistics.Win.Misc.UltraButton();
            this.uGridMatList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxPackage = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDel_PackageList = new Infragistics.Win.Misc.UltraButton();
            this.uGridPackageList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextPurpose = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPurpose = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckMan = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMOther = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextQualNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelQualNo = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckEnvironment = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMaterial = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMethod = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMachine = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabel4MDivision = new Infragistics.Win.Misc.UltraLabel();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboQualType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelQualType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchQualType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWrtiteDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQualList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEmail)).BeginInit();
            this.uGroupBoxEmail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionFinalInspectResultFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinalCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEvidenceInfo)).BeginInit();
            this.uGroupBoxEvidenceInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEvidenceList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).BeginInit();
            this.uGroupBoxStdInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxPCN)).BeginInit();
            this.uGroupBoxPCN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeAfter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeBefore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxBomList)).BeginInit();
            this.uGroupBoxBomList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxPackage)).BeginInit();
            this.uGroupBoxPackage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackageList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQualNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEnvironment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMachine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboQualType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchQualType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchQualType);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateWrtiteDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateWriteDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel9);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchQualType
            // 
            this.uComboSearchQualType.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchQualType.Name = "uComboSearchQualType";
            this.uComboSearchQualType.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchQualType.TabIndex = 164;
            this.uComboSearchQualType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchQualType
            // 
            this.uLabelSearchQualType.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchQualType.Name = "uLabelSearchQualType";
            this.uLabelSearchQualType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchQualType.TabIndex = 163;
            this.uLabelSearchQualType.Text = "구분";
            // 
            // uDateWrtiteDateTo
            // 
            this.uDateWrtiteDateTo.Location = new System.Drawing.Point(532, 36);
            this.uDateWrtiteDateTo.Name = "uDateWrtiteDateTo";
            this.uDateWrtiteDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateWrtiteDateTo.TabIndex = 161;
            // 
            // uDateWriteDateFrom
            // 
            this.uDateWriteDateFrom.Location = new System.Drawing.Point(408, 36);
            this.uDateWriteDateFrom.Name = "uDateWriteDateFrom";
            this.uDateWriteDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDateFrom.TabIndex = 160;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(511, 40);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(16, 12);
            this.ultraLabel9.TabIndex = 162;
            this.ultraLabel9.Text = "~";
            // 
            // uLabelSearchWriteDate
            // 
            this.uLabelSearchWriteDate.Location = new System.Drawing.Point(303, 36);
            this.uLabelSearchWriteDate.Name = "uLabelSearchWriteDate";
            this.uLabelSearchWriteDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchWriteDate.TabIndex = 159;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(876, 36);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(180, 21);
            this.uComboSearchPackage.TabIndex = 6;
            this.uComboSearchPackage.Text = "ultraComboEditor1";
            this.uComboSearchPackage.Visible = false;
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(772, 36);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 5;
            this.uLabelSearchPackage.Text = "ultraLabel1";
            this.uLabelSearchPackage.Visible = false;
            // 
            // uTextSearchCustomerName
            // 
            appearance76.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance76;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(512, 12);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchCustomerName.TabIndex = 4;
            // 
            // uTextSearchCustomerCode
            // 
            appearance77.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance77.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance77;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomerCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchCustomerCode.Location = new System.Drawing.Point(408, 12);
            this.uTextSearchCustomerCode.Name = "uTextSearchCustomerCode";
            this.uTextSearchCustomerCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerCode.TabIndex = 3;
            this.uTextSearchCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchCustomerCode_KeyDown);
            this.uTextSearchCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomerCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(304, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 2;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(180, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridQualList
            // 
            this.uGridQualList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQualList.DisplayLayout.Appearance = appearance6;
            this.uGridQualList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQualList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQualList.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQualList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGridQualList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQualList.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGridQualList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQualList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQualList.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQualList.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridQualList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQualList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQualList.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQualList.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGridQualList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQualList.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQualList.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.uGridQualList.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridQualList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQualList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.uGridQualList.DisplayLayout.Override.RowAppearance = appearance16;
            this.uGridQualList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQualList.DisplayLayout.Override.TemplateAddRowAppearance = appearance17;
            this.uGridQualList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQualList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQualList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQualList.Location = new System.Drawing.Point(0, 100);
            this.uGridQualList.Name = "uGridQualList";
            this.uGridQualList.Size = new System.Drawing.Size(1070, 720);
            this.uGridQualList.TabIndex = 2;
            this.uGridQualList.Text = "ultraGrid1";
            this.uGridQualList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridQualList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1065, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1065, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxEmail);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckCompleteFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCompleteFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uOptionFinalInspectResultFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateFinalCompleteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFinalCompleteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFinalInspectResultFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxEvidenceInfo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxStdInfo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1059, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxEmail
            // 
            this.uGroupBoxEmail.Controls.Add(this.uButtonDel_Email);
            this.uGroupBoxEmail.Controls.Add(this.uGridEmail);
            this.uGroupBoxEmail.Location = new System.Drawing.Point(796, 388);
            this.uGroupBoxEmail.Name = "uGroupBoxEmail";
            this.uGroupBoxEmail.Size = new System.Drawing.Size(256, 236);
            this.uGroupBoxEmail.TabIndex = 95;
            this.uGroupBoxEmail.Text = "ultraGroupBox1";
            // 
            // uButtonDel_Email
            // 
            this.uButtonDel_Email.Location = new System.Drawing.Point(16, 28);
            this.uButtonDel_Email.Name = "uButtonDel_Email";
            this.uButtonDel_Email.Size = new System.Drawing.Size(92, 28);
            this.uButtonDel_Email.TabIndex = 87;
            this.uButtonDel_Email.Text = "ultraButton1";
            this.uButtonDel_Email.Click += new System.EventHandler(this.uButtonDel_Email_Click);
            // 
            // uGridEmail
            // 
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEmail.DisplayLayout.Appearance = appearance19;
            this.uGridEmail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEmail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEmail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridEmail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEmail.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridEmail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEmail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEmail.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEmail.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.uGridEmail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEmail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEmail.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridEmail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEmail.DisplayLayout.Override.CellPadding = 0;
            appearance41.BackColor = System.Drawing.SystemColors.Control;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.Override.GroupByRowAppearance = appearance41;
            appearance43.TextHAlignAsString = "Left";
            this.uGridEmail.DisplayLayout.Override.HeaderAppearance = appearance43;
            this.uGridEmail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEmail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            this.uGridEmail.DisplayLayout.Override.RowAppearance = appearance42;
            this.uGridEmail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEmail.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.uGridEmail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEmail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEmail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEmail.Location = new System.Drawing.Point(12, 56);
            this.uGridEmail.Name = "uGridEmail";
            this.uGridEmail.Size = new System.Drawing.Size(236, 172);
            this.uGridEmail.TabIndex = 0;
            this.uGridEmail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_AfterCellUpdate);
            this.uGridEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridEmail_KeyDown);
            this.uGridEmail.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_ClickCellButton);
            this.uGridEmail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_CellChange);
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2010CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(1020, 632);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(20, 20);
            this.uCheckCompleteFlag.TabIndex = 94;
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCompleteFlag.CheckedChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedChanged);
            // 
            // uLabelCompleteFlag
            // 
            this.uLabelCompleteFlag.Location = new System.Drawing.Point(916, 632);
            this.uLabelCompleteFlag.Name = "uLabelCompleteFlag";
            this.uLabelCompleteFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelCompleteFlag.TabIndex = 93;
            this.uLabelCompleteFlag.Text = "작성완료";
            // 
            // uOptionFinalInspectResultFlag
            // 
            this.uOptionFinalInspectResultFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "OK";
            valueListItem1.DisplayText = "합격";
            valueListItem2.DataValue = "NG";
            valueListItem2.DisplayText = "불합격";
            this.uOptionFinalInspectResultFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionFinalInspectResultFlag.Location = new System.Drawing.Point(560, 632);
            this.uOptionFinalInspectResultFlag.Name = "uOptionFinalInspectResultFlag";
            this.uOptionFinalInspectResultFlag.Size = new System.Drawing.Size(124, 20);
            this.uOptionFinalInspectResultFlag.TabIndex = 92;
            // 
            // uDateFinalCompleteDate
            // 
            this.uDateFinalCompleteDate.Location = new System.Drawing.Point(806, 632);
            this.uDateFinalCompleteDate.Name = "uDateFinalCompleteDate";
            this.uDateFinalCompleteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateFinalCompleteDate.TabIndex = 91;
            // 
            // uLabelFinalCompleteDate
            // 
            this.uLabelFinalCompleteDate.Location = new System.Drawing.Point(692, 632);
            this.uLabelFinalCompleteDate.Name = "uLabelFinalCompleteDate";
            this.uLabelFinalCompleteDate.Size = new System.Drawing.Size(112, 20);
            this.uLabelFinalCompleteDate.TabIndex = 90;
            this.uLabelFinalCompleteDate.Text = "ultraLabel1";
            // 
            // uLabelFinalInspectResultFlag
            // 
            this.uLabelFinalInspectResultFlag.Location = new System.Drawing.Point(456, 632);
            this.uLabelFinalInspectResultFlag.Name = "uLabelFinalInspectResultFlag";
            this.uLabelFinalInspectResultFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelFinalInspectResultFlag.TabIndex = 89;
            this.uLabelFinalInspectResultFlag.Text = "ultraLabel1";
            // 
            // uGroupBoxEvidenceInfo
            // 
            this.uGroupBoxEvidenceInfo.Controls.Add(this.uButtonDel_EvidenceList);
            this.uGroupBoxEvidenceInfo.Controls.Add(this.uButtonFileDown_EvidenceList);
            this.uGroupBoxEvidenceInfo.Controls.Add(this.uGridEvidenceList);
            this.uGroupBoxEvidenceInfo.Location = new System.Drawing.Point(12, 388);
            this.uGroupBoxEvidenceInfo.Name = "uGroupBoxEvidenceInfo";
            this.uGroupBoxEvidenceInfo.Size = new System.Drawing.Size(780, 236);
            this.uGroupBoxEvidenceInfo.TabIndex = 2;
            this.uGroupBoxEvidenceInfo.Text = "ultraGroupBox1";
            // 
            // uButtonDel_EvidenceList
            // 
            this.uButtonDel_EvidenceList.Location = new System.Drawing.Point(12, 28);
            this.uButtonDel_EvidenceList.Name = "uButtonDel_EvidenceList";
            this.uButtonDel_EvidenceList.Size = new System.Drawing.Size(92, 28);
            this.uButtonDel_EvidenceList.TabIndex = 86;
            this.uButtonDel_EvidenceList.Text = "ultraButton1";
            this.uButtonDel_EvidenceList.Click += new System.EventHandler(this.uButtonDel_EvidenceList_Click);
            // 
            // uButtonFileDown_EvidenceList
            // 
            this.uButtonFileDown_EvidenceList.Location = new System.Drawing.Point(108, 28);
            this.uButtonFileDown_EvidenceList.Name = "uButtonFileDown_EvidenceList";
            this.uButtonFileDown_EvidenceList.Size = new System.Drawing.Size(92, 28);
            this.uButtonFileDown_EvidenceList.TabIndex = 81;
            this.uButtonFileDown_EvidenceList.Text = "ultraButton1";
            this.uButtonFileDown_EvidenceList.Click += new System.EventHandler(this.uButtonFileDown_EvidenceList_Click);
            // 
            // uGridEvidenceList
            // 
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            appearance79.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEvidenceList.DisplayLayout.Appearance = appearance79;
            this.uGridEvidenceList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEvidenceList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance80.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance80.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance80.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEvidenceList.DisplayLayout.GroupByBox.Appearance = appearance80;
            appearance81.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEvidenceList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance81;
            this.uGridEvidenceList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance82.BackColor2 = System.Drawing.SystemColors.Control;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance82.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEvidenceList.DisplayLayout.GroupByBox.PromptAppearance = appearance82;
            this.uGridEvidenceList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEvidenceList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEvidenceList.DisplayLayout.Override.ActiveCellAppearance = appearance83;
            appearance84.BackColor = System.Drawing.SystemColors.Highlight;
            appearance84.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEvidenceList.DisplayLayout.Override.ActiveRowAppearance = appearance84;
            this.uGridEvidenceList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEvidenceList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEvidenceList.DisplayLayout.Override.CardAreaAppearance = appearance85;
            appearance86.BorderColor = System.Drawing.Color.Silver;
            appearance86.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEvidenceList.DisplayLayout.Override.CellAppearance = appearance86;
            this.uGridEvidenceList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEvidenceList.DisplayLayout.Override.CellPadding = 0;
            appearance87.BackColor = System.Drawing.SystemColors.Control;
            appearance87.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance87.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance87.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance87.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEvidenceList.DisplayLayout.Override.GroupByRowAppearance = appearance87;
            appearance88.TextHAlignAsString = "Left";
            this.uGridEvidenceList.DisplayLayout.Override.HeaderAppearance = appearance88;
            this.uGridEvidenceList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEvidenceList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.BorderColor = System.Drawing.Color.Silver;
            this.uGridEvidenceList.DisplayLayout.Override.RowAppearance = appearance89;
            this.uGridEvidenceList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEvidenceList.DisplayLayout.Override.TemplateAddRowAppearance = appearance90;
            this.uGridEvidenceList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEvidenceList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEvidenceList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEvidenceList.Location = new System.Drawing.Point(12, 56);
            this.uGridEvidenceList.Name = "uGridEvidenceList";
            this.uGridEvidenceList.Size = new System.Drawing.Size(760, 172);
            this.uGridEvidenceList.TabIndex = 25;
            this.uGridEvidenceList.Text = "ultraGrid2";
            this.uGridEvidenceList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridEvidenceList_KeyDown);
            this.uGridEvidenceList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEvidenceList_ClickCellButton);
            this.uGridEvidenceList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridEvidenceList_DoubleClickCell);
            // 
            // uGroupBoxStdInfo
            // 
            this.uGroupBoxStdInfo.Controls.Add(this.uGroupBoxPCN);
            this.uGroupBoxStdInfo.Controls.Add(this.uGroupBoxBomList);
            this.uGroupBoxStdInfo.Controls.Add(this.uGroupBoxPackage);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextPurpose);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPurpose);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckMan);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckMOther);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextQualNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelQualNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckEnvironment);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckMaterial);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckMethod);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckMachine);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabel4MDivision);
            this.uGroupBoxStdInfo.Controls.Add(this.uDateWriteDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelWriteDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextWriteName);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextWriteID);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelWriteUser);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboQualType);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelQualType);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerName);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelCustomer);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboPlant);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxStdInfo.Location = new System.Drawing.Point(12, 12);
            this.uGroupBoxStdInfo.Name = "uGroupBoxStdInfo";
            this.uGroupBoxStdInfo.Size = new System.Drawing.Size(1040, 376);
            this.uGroupBoxStdInfo.TabIndex = 0;
            this.uGroupBoxStdInfo.Text = "ultraGroupBox1";
            // 
            // uGroupBoxPCN
            // 
            this.uGroupBoxPCN.Controls.Add(this.uLabelChangeBefore);
            this.uGroupBoxPCN.Controls.Add(this.uLabelChangeAfter);
            this.uGroupBoxPCN.Controls.Add(this.uTextChangeAfter);
            this.uGroupBoxPCN.Controls.Add(this.uTextChangeBefore);
            this.uGroupBoxPCN.Location = new System.Drawing.Point(504, 148);
            this.uGroupBoxPCN.Name = "uGroupBoxPCN";
            this.uGroupBoxPCN.Size = new System.Drawing.Size(530, 220);
            this.uGroupBoxPCN.TabIndex = 49;
            // 
            // uLabelChangeBefore
            // 
            this.uLabelChangeBefore.Location = new System.Drawing.Point(8, 24);
            this.uLabelChangeBefore.Name = "uLabelChangeBefore";
            this.uLabelChangeBefore.Size = new System.Drawing.Size(96, 20);
            this.uLabelChangeBefore.TabIndex = 53;
            this.uLabelChangeBefore.Text = "변경전";
            // 
            // uLabelChangeAfter
            // 
            this.uLabelChangeAfter.Location = new System.Drawing.Point(8, 124);
            this.uLabelChangeAfter.Name = "uLabelChangeAfter";
            this.uLabelChangeAfter.Size = new System.Drawing.Size(96, 20);
            this.uLabelChangeAfter.TabIndex = 55;
            this.uLabelChangeAfter.Text = "변경후";
            // 
            // uTextChangeAfter
            // 
            this.uTextChangeAfter.Location = new System.Drawing.Point(108, 120);
            this.uTextChangeAfter.Multiline = true;
            this.uTextChangeAfter.Name = "uTextChangeAfter";
            this.uTextChangeAfter.Size = new System.Drawing.Size(407, 86);
            this.uTextChangeAfter.TabIndex = 56;
            // 
            // uTextChangeBefore
            // 
            this.uTextChangeBefore.Location = new System.Drawing.Point(108, 20);
            this.uTextChangeBefore.Multiline = true;
            this.uTextChangeBefore.Name = "uTextChangeBefore";
            this.uTextChangeBefore.Size = new System.Drawing.Size(407, 86);
            this.uTextChangeBefore.TabIndex = 54;
            // 
            // uGroupBoxBomList
            // 
            this.uGroupBoxBomList.Controls.Add(this.uButtonDel_MatList);
            this.uGroupBoxBomList.Controls.Add(this.uGridMatList);
            this.uGroupBoxBomList.Location = new System.Drawing.Point(504, 148);
            this.uGroupBoxBomList.Name = "uGroupBoxBomList";
            this.uGroupBoxBomList.Size = new System.Drawing.Size(530, 220);
            this.uGroupBoxBomList.TabIndex = 48;
            // 
            // uButtonDel_MatList
            // 
            this.uButtonDel_MatList.Location = new System.Drawing.Point(8, 24);
            this.uButtonDel_MatList.Name = "uButtonDel_MatList";
            this.uButtonDel_MatList.Size = new System.Drawing.Size(88, 27);
            this.uButtonDel_MatList.TabIndex = 47;
            this.uButtonDel_MatList.Text = "ultraButton1";
            this.uButtonDel_MatList.Click += new System.EventHandler(this.uButtonDel_MatList_Click);
            // 
            // uGridMatList
            // 
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMatList.DisplayLayout.Appearance = appearance45;
            this.uGridMatList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMatList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance46.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance46.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatList.DisplayLayout.GroupByBox.Appearance = appearance46;
            appearance47.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance47;
            this.uGridMatList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance48.BackColor2 = System.Drawing.SystemColors.Control;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatList.DisplayLayout.GroupByBox.PromptAppearance = appearance48;
            this.uGridMatList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMatList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMatList.DisplayLayout.Override.ActiveCellAppearance = appearance49;
            appearance50.BackColor = System.Drawing.SystemColors.Highlight;
            appearance50.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMatList.DisplayLayout.Override.ActiveRowAppearance = appearance50;
            this.uGridMatList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMatList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMatList.DisplayLayout.Override.CardAreaAppearance = appearance51;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            appearance52.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMatList.DisplayLayout.Override.CellAppearance = appearance52;
            this.uGridMatList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMatList.DisplayLayout.Override.CellPadding = 0;
            appearance53.BackColor = System.Drawing.SystemColors.Control;
            appearance53.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance53.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance53.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatList.DisplayLayout.Override.GroupByRowAppearance = appearance53;
            appearance54.TextHAlignAsString = "Left";
            this.uGridMatList.DisplayLayout.Override.HeaderAppearance = appearance54;
            this.uGridMatList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMatList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.Color.Silver;
            this.uGridMatList.DisplayLayout.Override.RowAppearance = appearance55;
            this.uGridMatList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMatList.DisplayLayout.Override.TemplateAddRowAppearance = appearance56;
            this.uGridMatList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMatList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMatList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMatList.Location = new System.Drawing.Point(8, 59);
            this.uGridMatList.Name = "uGridMatList";
            this.uGridMatList.Size = new System.Drawing.Size(498, 152);
            this.uGridMatList.TabIndex = 48;
            this.uGridMatList.Text = "ultraGrid1";
            this.uGridMatList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMatList_AfterCellUpdate);
            this.uGridMatList.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMatList_AfterCellListCloseUp);
            // 
            // uGroupBoxPackage
            // 
            this.uGroupBoxPackage.Controls.Add(this.uButtonDel_PackageList);
            this.uGroupBoxPackage.Controls.Add(this.uGridPackageList);
            this.uGroupBoxPackage.Location = new System.Drawing.Point(8, 148);
            this.uGroupBoxPackage.Name = "uGroupBoxPackage";
            this.uGroupBoxPackage.Size = new System.Drawing.Size(492, 220);
            this.uGroupBoxPackage.TabIndex = 47;
            // 
            // uButtonDel_PackageList
            // 
            this.uButtonDel_PackageList.Location = new System.Drawing.Point(8, 25);
            this.uButtonDel_PackageList.Name = "uButtonDel_PackageList";
            this.uButtonDel_PackageList.Size = new System.Drawing.Size(92, 28);
            this.uButtonDel_PackageList.TabIndex = 25;
            this.uButtonDel_PackageList.Text = "ultraButton1";
            this.uButtonDel_PackageList.Click += new System.EventHandler(this.uButtonDel_PackageList_Click);
            // 
            // uGridPackageList
            // 
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            appearance64.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPackageList.DisplayLayout.Appearance = appearance64;
            this.uGridPackageList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPackageList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance65.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance65.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance65.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance65.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.GroupByBox.Appearance = appearance65;
            appearance66.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackageList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance66;
            this.uGridPackageList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance67.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance67.BackColor2 = System.Drawing.SystemColors.Control;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance67.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackageList.DisplayLayout.GroupByBox.PromptAppearance = appearance67;
            this.uGridPackageList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPackageList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            appearance68.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPackageList.DisplayLayout.Override.ActiveCellAppearance = appearance68;
            appearance69.BackColor = System.Drawing.SystemColors.Highlight;
            appearance69.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPackageList.DisplayLayout.Override.ActiveRowAppearance = appearance69;
            this.uGridPackageList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPackageList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.Override.CardAreaAppearance = appearance70;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            appearance71.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPackageList.DisplayLayout.Override.CellAppearance = appearance71;
            this.uGridPackageList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPackageList.DisplayLayout.Override.CellPadding = 0;
            appearance72.BackColor = System.Drawing.SystemColors.Control;
            appearance72.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance72.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance72.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance72.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.Override.GroupByRowAppearance = appearance72;
            appearance73.TextHAlignAsString = "Left";
            this.uGridPackageList.DisplayLayout.Override.HeaderAppearance = appearance73;
            this.uGridPackageList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPackageList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.BorderColor = System.Drawing.Color.Silver;
            this.uGridPackageList.DisplayLayout.Override.RowAppearance = appearance74;
            this.uGridPackageList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance75.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPackageList.DisplayLayout.Override.TemplateAddRowAppearance = appearance75;
            this.uGridPackageList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPackageList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPackageList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPackageList.Location = new System.Drawing.Point(8, 57);
            this.uGridPackageList.Name = "uGridPackageList";
            this.uGridPackageList.Size = new System.Drawing.Size(476, 156);
            this.uGridPackageList.TabIndex = 40;
            this.uGridPackageList.Text = "ultraGrid1";
            // 
            // uTextPurpose
            // 
            this.uTextPurpose.Location = new System.Drawing.Point(116, 76);
            this.uTextPurpose.Multiline = true;
            this.uTextPurpose.Name = "uTextPurpose";
            this.uTextPurpose.Size = new System.Drawing.Size(444, 68);
            this.uTextPurpose.TabIndex = 42;
            // 
            // uLabelPurpose
            // 
            this.uLabelPurpose.Location = new System.Drawing.Point(12, 76);
            this.uLabelPurpose.Name = "uLabelPurpose";
            this.uLabelPurpose.Size = new System.Drawing.Size(100, 20);
            this.uLabelPurpose.TabIndex = 41;
            this.uLabelPurpose.Text = "목적";
            // 
            // uCheckMan
            // 
            this.uCheckMan.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2010CheckBoxGlyphInfo;
            this.uCheckMan.Location = new System.Drawing.Point(676, 52);
            this.uCheckMan.Name = "uCheckMan";
            this.uCheckMan.Size = new System.Drawing.Size(92, 20);
            this.uCheckMan.TabIndex = 33;
            this.uCheckMan.Text = "Man";
            this.uCheckMan.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMOther
            // 
            this.uCheckMOther.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2010CheckBoxGlyphInfo;
            this.uCheckMOther.Location = new System.Drawing.Point(792, 100);
            this.uCheckMOther.Name = "uCheckMOther";
            this.uCheckMOther.Size = new System.Drawing.Size(92, 20);
            this.uCheckMOther.TabIndex = 30;
            this.uCheckMOther.Text = "Other";
            this.uCheckMOther.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextQualNo
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQualNo.Appearance = appearance21;
            this.uTextQualNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQualNo.Location = new System.Drawing.Point(676, 28);
            this.uTextQualNo.Name = "uTextQualNo";
            this.uTextQualNo.ReadOnly = true;
            this.uTextQualNo.Size = new System.Drawing.Size(172, 21);
            this.uTextQualNo.TabIndex = 27;
            // 
            // uLabelQualNo
            // 
            this.uLabelQualNo.Location = new System.Drawing.Point(572, 28);
            this.uLabelQualNo.Name = "uLabelQualNo";
            this.uLabelQualNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelQualNo.TabIndex = 26;
            this.uLabelQualNo.Text = "관리번호";
            // 
            // uCheckEnvironment
            // 
            this.uCheckEnvironment.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2010CheckBoxGlyphInfo;
            this.uCheckEnvironment.Location = new System.Drawing.Point(676, 100);
            this.uCheckEnvironment.Name = "uCheckEnvironment";
            this.uCheckEnvironment.Size = new System.Drawing.Size(112, 20);
            this.uCheckEnvironment.TabIndex = 19;
            this.uCheckEnvironment.Text = "Environment";
            this.uCheckEnvironment.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMaterial
            // 
            this.uCheckMaterial.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2010CheckBoxGlyphInfo;
            this.uCheckMaterial.Location = new System.Drawing.Point(792, 76);
            this.uCheckMaterial.Name = "uCheckMaterial";
            this.uCheckMaterial.Size = new System.Drawing.Size(92, 20);
            this.uCheckMaterial.TabIndex = 18;
            this.uCheckMaterial.Text = "Material";
            this.uCheckMaterial.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMethod
            // 
            this.uCheckMethod.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2010CheckBoxGlyphInfo;
            this.uCheckMethod.Location = new System.Drawing.Point(676, 76);
            this.uCheckMethod.Name = "uCheckMethod";
            this.uCheckMethod.Size = new System.Drawing.Size(92, 20);
            this.uCheckMethod.TabIndex = 17;
            this.uCheckMethod.Text = "Method";
            this.uCheckMethod.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMachine
            // 
            this.uCheckMachine.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2010CheckBoxGlyphInfo;
            this.uCheckMachine.Location = new System.Drawing.Point(792, 52);
            this.uCheckMachine.Name = "uCheckMachine";
            this.uCheckMachine.Size = new System.Drawing.Size(92, 20);
            this.uCheckMachine.TabIndex = 16;
            this.uCheckMachine.Text = "Machine";
            this.uCheckMachine.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabel4MDivision
            // 
            this.uLabel4MDivision.Location = new System.Drawing.Point(572, 52);
            this.uLabel4MDivision.Name = "uLabel4MDivision";
            this.uLabel4MDivision.Size = new System.Drawing.Size(100, 20);
            this.uLabel4MDivision.TabIndex = 15;
            this.uLabel4MDivision.Text = "4M구분";
            // 
            // uDateWriteDate
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Appearance = appearance39;
            this.uDateWriteDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Location = new System.Drawing.Point(116, 52);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDate.TabIndex = 14;
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(12, 52);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelWriteDate.TabIndex = 13;
            this.uLabelWriteDate.Text = "등록일";
            // 
            // uTextWriteName
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance5;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(434, 52);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(126, 21);
            this.uTextWriteName.TabIndex = 12;
            // 
            // uTextWriteID
            // 
            appearance37.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteID.Appearance = appearance37;
            this.uTextWriteID.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance4;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteID.ButtonsRight.Add(editorButton2);
            this.uTextWriteID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteID.Location = new System.Drawing.Point(332, 52);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteID.TabIndex = 11;
            this.uTextWriteID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteID_KeyDown);
            this.uTextWriteID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(228, 52);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelWriteUser.TabIndex = 10;
            this.uLabelWriteUser.Text = "등록자";
            // 
            // uComboQualType
            // 
            this.uComboQualType.Location = new System.Drawing.Point(116, 28);
            this.uComboQualType.Name = "uComboQualType";
            this.uComboQualType.Size = new System.Drawing.Size(100, 21);
            this.uComboQualType.TabIndex = 9;
            this.uComboQualType.Text = "ultraComboEditor1";
            this.uComboQualType.ValueChanged += new System.EventHandler(this.uComboQualType_ValueChanged);
            // 
            // uLabelQualType
            // 
            this.uLabelQualType.Location = new System.Drawing.Point(12, 28);
            this.uLabelQualType.Name = "uLabelQualType";
            this.uLabelQualType.Size = new System.Drawing.Size(100, 20);
            this.uLabelQualType.TabIndex = 8;
            this.uLabelQualType.Text = "구분";
            // 
            // uTextCustomerName
            // 
            appearance78.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance78;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(434, 28);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(126, 21);
            this.uTextCustomerName.TabIndex = 7;
            // 
            // uTextCustomerCode
            // 
            appearance38.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomerCode.Appearance = appearance38;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance35.Image = global::QRPQAT.UI.Properties.Resources.btn_Zoom;
            appearance35.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance35;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCustomerCode.ButtonsRight.Add(editorButton3);
            this.uTextCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCustomerCode.Location = new System.Drawing.Point(332, 28);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.Size = new System.Drawing.Size(100, 21);
            this.uTextCustomerCode.TabIndex = 6;
            this.uTextCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCustomerCode_KeyDown);
            this.uTextCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCustomerCode_EditorButtonClick);
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(228, 28);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomer.TabIndex = 5;
            this.uLabelCustomer.Text = "고객사";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(944, 20);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(21, 21);
            this.uComboPlant.TabIndex = 2;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.Visible = false;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(920, 20);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmQATZ0001
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridQualList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmQATZ0001";
            this.Load += new System.EventHandler(this.frmQATZ0001_Load);
            this.Activated += new System.EventHandler(this.frmQATZ0001_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQATZ0001_FormClosing);
            this.Resize += new System.EventHandler(this.frmQATZ0001_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchQualType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWrtiteDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQualList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEmail)).EndInit();
            this.uGroupBoxEmail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionFinalInspectResultFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFinalCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEvidenceInfo)).EndInit();
            this.uGroupBoxEvidenceInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEvidenceList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).EndInit();
            this.uGroupBoxStdInfo.ResumeLayout(false);
            this.uGroupBoxStdInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxPCN)).EndInit();
            this.uGroupBoxPCN.ResumeLayout(false);
            this.uGroupBoxPCN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeAfter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeBefore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxBomList)).EndInit();
            this.uGroupBoxBomList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxPackage)).EndInit();
            this.uGroupBoxPackage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackageList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQualNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckEnvironment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMachine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboQualType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQualList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxStdInfo;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboQualType;
        private Infragistics.Win.Misc.UltraLabel uLabelQualType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMachine;
        private Infragistics.Win.Misc.UltraLabel uLabel4MDivision;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckEnvironment;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMaterial;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMethod;
        private Infragistics.Win.Misc.UltraButton uButtonDel_PackageList;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxEvidenceInfo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEvidenceList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextQualNo;
        private Infragistics.Win.Misc.UltraLabel uLabelQualNo;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMOther;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMan;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown_EvidenceList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraButton uButtonDel_EvidenceList;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWrtiteDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDateFrom;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchWriteDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPackageList;
        private Infragistics.Win.Misc.UltraLabel uLabelPurpose;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPurpose;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchQualType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchQualType;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxPackage;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxBomList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeAfter;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeAfter;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeBefore;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeBefore;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionFinalInspectResultFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFinalCompleteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelFinalCompleteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelFinalInspectResultFlag;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxEmail;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEmail;
        private Infragistics.Win.Misc.UltraButton uButtonDel_Email;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxPCN;
        private Infragistics.Win.Misc.UltraButton uButtonDel_MatList;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMatList;
    }
}