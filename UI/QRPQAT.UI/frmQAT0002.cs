﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 고객관리                                              */
/* 모듈(분류)명 : 고객불만관리                                          */
/* 프로그램ID   : frmQAT0002.cs                                         */
/* 프로그램명   : 고객불만 등록/조회                                    */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-21 : 기능 추가 (이종호)                       */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

// 파일첨부
using System.IO;
using System.Collections;

namespace QRPQAT.UI
{
    public partial class frmQAT0002 : Form, IToolbar
    {
        // 리소스 사용을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmQAT0002()
        {
            InitializeComponent();
        }

        private void frmQAT0002_Activated(object sender, EventArgs e)
        {
            // 툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmQAT0002_Load(object sender, EventArgs e)
        {
            // System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀지정
            titleArea.mfSetLabelText("고객불만 등록/조회", m_resSys.GetString("SYS_FONTNAME"), 12);            

            // 컨트롤 초기화
            SetToolAuth();
            InitButton();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitGroupBox();
            InitEtc();

            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            //// E-Mail TEST
            //if (m_resSys.GetString("SYS_USERID").Equals("TESTUSER"))
            //    this.ultraButton1.Visible = true;
            //else
            //    this.ultraButton1.Visible = false;

        }

        private void frmQAT0002_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        private void InitEtc()
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 초기값 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextReceiptUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextReceiptUserName.Text = m_resSys.GetString("SYS_USERNAME");

                // 텍스트박스 MaxLenth 설정
                this.uTextSearchCustomerCode.MaxLength = 20;                
                this.uTextCustomerCode.MaxLength = 20;                
                this.uTextReceiptUserID.MaxLength = 20;
                this.uTextCustomerAnalysisResult.MaxLength = 500;
                this.uTextComment.MaxLength = 200;

                this.uTextReasonDesc.MaxLength = 1000;
                this.uTextActionDesc.MaxLength = 1000;
                this.uTextInspectUserID.MaxLength = 20;
                this.uTextInspectComment.MaxLength = 200;

                this.uTextCustomerAnalysisResult.Scrollbars = ScrollBars.Vertical;
                this.uTextCustomerAnalysisResult.ScrollToCaret();
                this.uTextReasonDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextReasonDesc.ScrollToCaret();
                this.uTextActionDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextActionDesc.ScrollToCaret();
                this.uTextMaterialDisposal.Scrollbars = ScrollBars.Vertical;
                this.uTextMaterialDisposal.ScrollToCaret();
                this.uTextInspectComment.Scrollbars = ScrollBars.Vertical;
                this.uTextInspectComment.ScrollToCaret();

                this.uTextInspectUserID.ImeMode = ImeMode.Disable;
                this.uTextInspectUserID.CharacterCasing = CharacterCasing.Upper;
                this.uTextReceiptUserID.ImeMode = ImeMode.Disable;
                this.uTextReceiptUserID.CharacterCasing = CharacterCasing.Upper;

                this.uTextCustomerCode.Hide();
                this.uTextCustomerName.Hide();
                this.uTextPakcage.Hide();
                this.uLabelProduct.Hide();
                this.uComboProductCode.Hide();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "접수내용", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "원인/분석", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.INFO, "완료등록", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReceiptDate, "접수일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchIssueType, "발행구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchComplete, "완료여부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelClaimNo, "발행번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCustomerProductSpec, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReceiptUser, "접수자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReceiptDate, "접수일자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelIssueType, "발행구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelDetectProcess, "Detect 공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFaultType, "불량명", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelCustomerAnalysisResult, "Customer Analysis Result", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachmentFileName, "Attachment", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceipComplete, "접수완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEmail, "E-mail", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSampleReceiptDate, "시료접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelComment, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelImputeProcess, "귀책공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelImputeDept, "귀책부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFourMOneE, "4M + 1E", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReasonCompleteFlag, "분석완료", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelCompleteDate, "완료일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelComplete, "작성완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterialDisposal, "자재처리", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelClaimCancelFlag, "접수취소", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabel4D8D, "4D/8D", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReasonDesc, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelActionDesc, "대책사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDueDate, "DueDate", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectDate, "점검일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser, "점검자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectResultFlag, "점검결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectComment, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDelete4D8D, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonFileDown, "FileDown", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_FillDown);
                wButton.mfSetButton(this.uButtonDel_Email, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonEmailPopUp, "수신인등록", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Mail);
                wButton.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonOK.Click += new EventHandler(uButtonOK_Click);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchArea Plant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // ContentsArea Plant ComboBox
                wCombo.mfSetComboEditor(this.uComboPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 4M + 1E
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUComCode);
                DataTable dtUCom = clsUComCode.mfReadUserCommonCode("QUA", "U0006", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboFourMOneE, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dtUCom);

                // 발행구분
                dtUCom = clsUComCode.mfReadUserCommonCode("QUA", "U0005", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchIssueTypeCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ComCode", "ComCodeName", dtUCom);
                wCombo.mfSetComboEditor(this.uComboIssueTypeCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                                    , "ComCode", "ComCodeName", dtUCom);

                // 검색조건 : Package 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);
                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));                
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "Package", "ComboName", dtPackage);

                // 고객사 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);
                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboCustomer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CustomerCode", "CustomerName", dtCustomer);

                // 검사결과
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);
                DataTable dtCom = clsCom.mfReadCommonCode("C0056", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboInspectResultFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCom);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 List

                // 고객불만 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridHeader, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ClaimNo", "발행번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ReceiptDate", "접수일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "IssueTypeName", "발행구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "LotCount", "Lot수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "Qty", "IN Qty",false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false,20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "RejectQty", "Out Qty", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "Yield", "Yield", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn.nn", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "FaultTypeDesc", "불량명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "SampleReceiptDate", "시료접수일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly,100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ReasonDesc", "발생원인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ActionDesc", "대책사항", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ImputeProcessCode", "귀책공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "DueDate", "DueDate", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "CompleteFlag", "완료여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "DueDateFlag", "경과여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "DateDifference", "경과일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "InspectResultFlag", "유효성 점검", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Grid 편집불가 상태로
                this.uGridHeader.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                #endregion

                #region Detail List

                //접수내용Grid
                //기본설정
                wGrid.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 90, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");
                
                wGrid.mfSetGridColumn(this.uGridDetail, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Qty", "IN Qty", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.0}", "0.0");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "RejectQty", "Out Qty", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.0}", "0.0");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "Yield", "Yield", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnn.nn", "0.0");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "FailureMode", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 4D/8D List

                wGrid.mfInitGeneralGrid(this.uGrid4D8DList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.None
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid4D8DList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                ////wGrid.mfSetGridColumn(this.uGrid4D8DList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid4D8DList, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4D8DList, 0, "SendDate", "등록일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime, "", "yyyy-mm-dd", DBNull.Value.ToString());

                wGrid.mfSetGridColumn(this.uGrid4D8DList, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4D8DList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region Email

                wGrid.mfInitGeneralGrid(this.uGridEmail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.None
                   , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEmail, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "UserID", "사용자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "UserName", "사용자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "EMail", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGridEmail, 0);

                #endregion

                
                //폰트설정
                this.uGridHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid4D8DList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid4D8DList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridEmail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEmail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // DropDown 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0062", m_resSys.GetString("SYS_LANG"));
                
                wGrid.mfSetGridColumnValueList(this.uGrid4D8DList, 0, "Gubun", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);
                
                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridDetail, 0);
                wGrid.mfAddRowGrid(this.uGrid4D8DList, 0); 
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                QRPQAT.BL.QATCLM.CustomerClaimH clsHeader = new QRPQAT.BL.QATCLM.CustomerClaimH();
                brwChannel.mfCredentials(clsHeader);

                // 저장용 데이터 테이블 컬럼 설정
                DataTable dtSearch = clsHeader.mfSetSearchDateInfo();
                DataRow drRow = dtSearch.NewRow();

                // 검색조건변수 저장
                drRow["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                drRow["CustomerCode"] = this.uTextSearchCustomerCode.Text;                
                drRow["Package"] = this.uComboSearchPackage.Value.ToString();
                drRow["IssueTypeCode"] = this.uComboSearchIssueTypeCode.Value.ToString();
                drRow["CompleteFlag"] = this.uCheckSearchCompleteFlag.CheckedValue;
                drRow["ReceiptDateFrom"] = Convert.ToDateTime(this.uDateSearchFromReceiptDate.Value).ToString("yyyy-MM-dd");
                drRow["ReceiptDateTo"] = Convert.ToDateTime(this.uDateSearchToReceiptDate.Value).ToString("yyyy-MM-dd");
                dtSearch.Rows.Add(drRow);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 검색메소드 호출
                DataTable dtRtn = clsHeader.mfReadQATCustomerClaimH(dtSearch, m_resSys.GetString("SYS_LANG"));

                this.uGridHeader.DataSource = dtRtn;
                this.uGridHeader.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtn.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridHeader, 0);

                    for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                    {
                        if (this.uGridHeader.Rows[i].Cells["DueDateFlag"].Value.ToString().Equals("T"))
                            this.uGridHeader.Rows[i].Appearance.BackColor = Color.Salmon;
                    }
                }

                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001041", Infragistics.Win.HAlign.Center);
                    return;
                }
                else if (this.uCheckReceiptComplete.Checked == false && this.uCheckCompleteFlag.Checked == true)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "접수확인", "접수확인", "접수완료가 안된 문서는 작성완료를 할 수 없습니다.", Infragistics.Win.HAlign.Right);

                    this.uCheckCompleteFlag.Checked = false;
                }
                else if (this.uTextAttachmentFileName.Text.Contains("/")
                        || this.uTextAttachmentFileName.Text.Contains("#")
                        || this.uTextAttachmentFileName.Text.Contains("*")
                        || this.uTextAttachmentFileName.Text.Contains("<")
                        || this.uTextAttachmentFileName.Text.Contains(">"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }
                for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                {
                    if (this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains("/")
                        || this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains("#")
                        || this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains("*")
                        || this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains("<")
                        || this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(">"))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
                //else if (this.uCheckCompleteFlag.Enabled == false)
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                        , "확인창", "작성완료 확인", "작성완료된 데이터는 저장할 수 없습니다", Infragistics.Win.HAlign.Center);
                //    return;
                //}
                {
                    // 필수입력사항 확인
                    if (CheckRequire())
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        // 저장여부를 묻는다
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            // 데이터 테이블에 저장할 데이터 저장하는 Method 호출
                            DataTable dtHeader = Rtn_HeaderDataTable();
                            DataTable dtDetail = Rtn_DetailDatatable();
                            DataTable dt4D8D = Rtn_4D8D_Datatable();
                            DataTable dtEmail = Rtn_Email_Datatable();

                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                            QRPQAT.BL.QATCLM.CustomerClaimH clsHeader = new QRPQAT.BL.QATCLM.CustomerClaimH();
                            brwChannel.mfCredentials(clsHeader);

                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string strErrRtn = clsHeader.mfSaveQATCustomerClaimH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail, dt4D8D, dtEmail,"W"); //W : 작성완료 F: 분석완료 (strMailGubun - 작성완료 메일리스트 or 분석완료 메일리스트)

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // 처리결과에 따른 메세지 박스
                            if (ErrRtn.ErrNum == 0)
                            {
                                // 발행번호 OUTPUT값 저장
                                string strClaimNo = ErrRtn.mfGetReturnValue(0);

                                // FileUpload 메소드 호출
                                FileUpload(strClaimNo);

                                if (this.uCheckReceiptComplete.Checked == true && this.uCheckReceiptComplete.Enabled == true)
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "메일전송 확인", "메일을 발송하시겠습니까?", Infragistics.Win.HAlign.Right);
                                    if (Result == DialogResult.Yes)
                                    {
                                        SendMail_Receipt(this.uComboPlantCode.Value.ToString(), strClaimNo);
                                    }
                                }
                                if (this.uCheckReasonCompleteFlag.Checked && this.uCheckReasonCompleteFlag.Enabled)
                                {
                                     Result = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "확인창", "메일전송 확인", "메일을 발송하시겠습니까?", Infragistics.Win.HAlign.Right);
                                     if (Result == DialogResult.Yes)
                                     {
                                         SendMail_Complete(this.uComboPlantCode.Value.ToString(), strClaimNo);
                                     }
                                }

                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextClaimNo.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                        QRPQAT.BL.QATCLM.CustomerClaimH clsHeader = new QRPQAT.BL.QATCLM.CustomerClaimH();
                        brwChannel.mfCredentials(clsHeader);

                        string strPlantCode = this.uComboPlantCode.Value.ToString();
                        string strClaimNo = this.uTextClaimNo.Text;

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 삭제메소드 호출
                        string strErrRtn = clsHeader.mfDeleteQATCustomerClaimH(strPlantCode, strClaimNo);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);
                            // 첨부파일 삭제 메소드 호출
                            FileDelete();

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000923",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
                SetWritable_Receipt();
                SetWritable_Analyse();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridHeader.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridHeader);
                    if (this.uGridDetail.Rows.Count > 0)
                        wGrid.mfDownLoadGridToExcel(this.uGridDetail);
                    if (this.uGrid4D8DList.Rows.Count > 0)
                        wGrid.mfDownLoadGridToExcel(this.uGrid4D8DList);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000254",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method...
        /// <summary>
        /// 입력사항 확인 Method
        /// </summary>
        /// <returns></returns>
        private bool CheckRequire()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (string.IsNullOrEmpty(this.uComboPlantCode.Value.ToString()) || this.uComboPlantCode.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlantCode.DropDown();
                    return false;
                }
                //else if (this.uComboIssueTypeCode.Value.ToString() == "" && this.uCheckCompleteFlag.Checked == true)
                else if (string.IsNullOrEmpty(this.uComboIssueTypeCode.Value.ToString()) || this.uComboIssueTypeCode.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M000881", "M000435", Infragistics.Win.HAlign.Center);

                    this.uComboIssueTypeCode.DropDown();
                    return false;
                }
                else if (this.uTextReceiptUserID.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001071", Infragistics.Win.HAlign.Center);

                    this.uTextReceiptUserID.Focus();
                    return false;
                }
                else if (string.IsNullOrEmpty(this.uComboCustomer.Value.ToString()) || this.uComboCustomer.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001274", Infragistics.Win.HAlign.Center);

                    this.uComboCustomer.DropDown();
                    return false;
                }
                else if (string.IsNullOrEmpty(this.uComboPackage.Value.ToString()) || this.uComboPackage.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000097", Infragistics.Win.HAlign.Center);

                    this.uComboPackage.DropDown();
                    return false;
                }
                else if (string.IsNullOrEmpty(this.uComboCustomerProductSpec.Value.ToString()) || this.uComboCustomerProductSpec.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000253", Infragistics.Win.HAlign.Center);

                    this.uComboCustomerProductSpec.DropDown();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 데이터 테이블로 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_HeaderDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                QRPQAT.BL.QATCLM.CustomerClaimH clsHeader = new QRPQAT.BL.QATCLM.CustomerClaimH();
                brwChannel.mfCredentials(clsHeader);

                ArrayList arrFile = new ArrayList();

                dtRtn = clsHeader.mfSetDateInfo();
                DataRow drRow = dtRtn.NewRow();

                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                drRow["ClaimNo"] = this.uTextClaimNo.Text;
                drRow["CustomerProductSpec"] = this.uComboCustomerProductSpec.Value.ToString();
                drRow["ProductCode"] = this.uComboProductCode.Value.ToString();
                drRow["CustomerCode"] = this.uComboCustomer.Value.ToString();
                drRow["Package"] = this.uComboPackage.Value.ToString();               
                drRow["IssueTypeCode"] = this.uComboIssueTypeCode.Value.ToString();
                drRow["FaultTypeDesc"] = this.uTextFaultTypeDesc.Text;
                drRow["DetectProcessCode"] = this.uComboDetectProcessCode.Value.ToString();
                drRow["ReceiptUserID"] = this.uTextReceiptUserID.Text;
                drRow["ReceiptDate"] = Convert.ToDateTime(this.uDateReceiptDate.Value).ToString("yyyy-MM-dd");
                if (this.uCheckReceiptComplete.Checked == false)
                    drRow["ReceiptCompleteFlag"] = "F";
                else
                    drRow["ReceiptCompleteFlag"] = "T";
                drRow["CustomerAnalysisResult"] = this.uTextCustomerAnalysisResult.Text;
                if (this.uTextAttachmentFileName.Text.Contains(":\\") && this.uTextClaimNo.Text != "")
                {
                    FileInfo fileDoc = new FileInfo(this.uTextAttachmentFileName.Text);
                    drRow["AttachmentFileName"] = this.uComboPlantCode.Value.ToString() + "-" + this.uTextClaimNo.Text + "-" + "ATT-" + fileDoc.Name;
                }
                else if (this.uTextAttachmentFileName.Text.Contains(":\\") && this.uTextClaimNo.Text == "")
                {
                    FileInfo fileDoc = new FileInfo(this.uTextAttachmentFileName.Text);
                    drRow["AttachmentFileName"] = fileDoc.Name;
                }
                else
                {
                    drRow["AttachmentFileName"] = this.uTextAttachmentFileName.Text;
                }

                if (this.uDateSampleReceiptDate.Value == null)
                {
                    drRow["SampleReceiptDate"] = DBNull.Value;
                }
                else
                {
                    drRow["SampleReceiptDate"] = Convert.ToDateTime(this.uDateSampleReceiptDate.Value).ToString("yyyy-MM-dd");
                }                        
                drRow["Comment"] = this.uTextComment.Text;
                drRow["ImputeProcessCode"] = this.uComboImputeProcess.Value.ToString();
                drRow["ImputeDeptCode"] = this.uComboImputeDept.Value.ToString();
                drRow["FourMOneE"] = this.uComboFourMOneE.Value.ToString();
                if (this.uDateCompleteDate.Value == null || this.uDateCompleteDate.Value == DBNull.Value)
                    drRow["CompleteDate"] = string.Empty;
                else
                    drRow["CompleteDate"] = Convert.ToDateTime(this.uDateCompleteDate.Value).ToString("yyyy-MM-dd");
                drRow["CompleteFlag"] = this.uCheckCompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MaterialDisposal"] = this.uTextMaterialDisposal.Text;
                drRow["ClaimCancelFlag"] = this.uCheckClaimCancelFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["EnviromentFlag"] = this.uCheckEnviromentFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MachineFlag"] = this.uCheckMachineFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ManFlag"] = this.uCheckManFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MethodFlag"] = this.uCheckMethodFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MaterialFlag"] = this.uCheckMaterialFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ReasonDesc"] = this.uTextReasonDesc.Text;
                drRow["ActionDesc"] = this.uTextActionDesc.Text;

                drRow["AnalyseCompleteFlag"] = this.uCheckReasonCompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                
                if (this.uDateDueDate.Value == null || this.uDateDueDate.Value == DBNull.Value)
                    drRow["DueDate"] = string.Empty;
                else
                    drRow["DueDate"] = Convert.ToDateTime(this.uDateDueDate.Value).ToString("yyyy-MM-dd");
                if (this.uDateInspectDate.Value == null || this.uDateInspectDate.Value == DBNull.Value)
                    drRow["InspectDate"] = string.Empty;
                else
                    drRow["InspectDate"] = Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd");
                drRow["InspectUserID"] = this.uTextInspectUserID.Text;
                drRow["InspectResultFlag"] = this.uComboInspectResultFlag.Value.ToString();
                drRow["InspectComment"] = this.uTextInspectComment.Text;

                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 데이터테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_DetailDatatable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimD), "CustomerClaimD");
                QRPQAT.BL.QATCLM.CustomerClaimD clsDetail = new QRPQAT.BL.QATCLM.CustomerClaimD();
                brwChannel.mfCredentials(clsDetail);

                dtRtn = clsDetail.mfSetDataInfo();
                DataRow drRow;

                this.uGridDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (this.uGridDetail.Rows[i].Hidden == false)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["Seq"] = this.uGridDetail.Rows[i].RowSelectorNumber;
                        drRow["FailureMode"] = this.uGridDetail.Rows[i].Cells["FailureMode"].Value.ToString();
                        drRow["LotNo"] = this.uGridDetail.Rows[i].Cells["LotNo"].Value.ToString();
                        drRow["Qty"] = Convert.ToDouble(this.uGridDetail.Rows[i].Cells["Qty"].Value);
                        drRow["RejectQty"] = Convert.ToDouble(this.uGridDetail.Rows[i].Cells["RejectQty"].Value);
                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 4D8D 정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_4D8D_Datatable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaim4D8D), "CustomerClaim4D8D");
                QRPQAT.BL.QATCLM.CustomerClaim4D8D cls4D8D = new QRPQAT.BL.QATCLM.CustomerClaim4D8D();
                brwChannel.mfCredentials(cls4D8D);

                dtRtn = cls4D8D.mfSetDataInfo();
                DataRow drRow;

                this.uGrid4D8DList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                {
                    if (!this.uGrid4D8DList.Rows[i].Hidden)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["Seq"] = this.uGrid4D8DList.Rows[i].RowSelectorNumber;
                        drRow["Gubun"] = this.uGrid4D8DList.Rows[i].Cells["Gubun"].Value.ToString();
                        if (this.uGrid4D8DList.Rows[i].Cells["SendDate"].Value == null ||
                            this.uGrid4D8DList.Rows[i].Cells["SendDate"].Value == DBNull.Value ||
                            this.uGrid4D8DList.Rows[i].Cells["SendDate"].Value.ToString().Equals(string.Empty))
                            drRow["SendDate"] = string.Empty;
                        else
                            drRow["SendDate"] = Convert.ToDateTime(this.uGrid4D8DList.Rows[i].Cells["SendDate"].Value).ToString("yyyy-MM-dd");

                        if (this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString());
                            drRow["FilePath"] = fileDoc.Name;
                        }
                        else
                        {
                            drRow["FilePath"] = this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString();
                        }
                        drRow["EtcDesc"] = this.uGrid4D8DList.Rows[i].Cells["EtcDesc"].Value.ToString();

                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// Email 정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_Email_Datatable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimEMailUser), "CustomerClaimEMailUser");
                QRPQAT.BL.QATCLM.CustomerClaimEMailUser clsEmail = new QRPQAT.BL.QATCLM.CustomerClaimEMailUser();
                brwChannel.mfCredentials(clsEmail);

                dtRtn = clsEmail.mfSetDataInfo();
                DataRow drRow;

                this.uGridEmail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                {
                    if (!this.uGridEmail.Rows[i].Hidden)
                    {
                        drRow = dtRtn.NewRow();

                        drRow["Seq"] = this.uGridEmail.Rows[i].RowSelectorNumber;
                        drRow["Gubun"] = "W";
                        drRow["MailUserID"] = this.uGridEmail.Rows[i].Cells["UserID"].Value;
                        drRow["EtcDesc"] = this.uGridEmail.Rows[i].Cells["EtcDesc"].Value;
                       

                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        private void Search_HeaderDetail(string strPlantCode, string strClaimNo)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                QRPQAT.BL.QATCLM.CustomerClaimH clsHeader = new QRPQAT.BL.QATCLM.CustomerClaimH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtRtn = clsHeader.mfReadQATCustomerClaimHDetail(strPlantCode, strClaimNo, m_resSys.GetString("SYS_LANG"));

                if (dtRtn.Rows.Count > 0)
                {
                    this.uComboPlantCode.Value = dtRtn.Rows[0]["PlantCode"].ToString();
                    this.uTextClaimNo.Text = dtRtn.Rows[0]["ClaimNo"].ToString();
                    this.uComboCustomer.Value = dtRtn.Rows[0]["CustomerCode"].ToString();
                    this.uComboPackage.Value = dtRtn.Rows[0]["Package"].ToString();
                    this.uComboCustomerProductSpec.Value = dtRtn.Rows[0]["CustomerProductSpec"].ToString();
                    this.uComboProductCode.Value = dtRtn.Rows[0]["ProductCode"].ToString();
                    this.uComboIssueTypeCode.Value = dtRtn.Rows[0]["IssueTypeCode"].ToString();
                    this.uTextFaultTypeDesc.Text = dtRtn.Rows[0]["FaultTypeDesc"].ToString();
                    this.uComboDetectProcessCode.Value = dtRtn.Rows[0]["DetectProcessCode"].ToString();
                    this.uDateReceiptDate.Value = dtRtn.Rows[0]["ReceiptDate"].ToString();
                    this.uTextReceiptUserID.Text = dtRtn.Rows[0]["ReceiptUserID"].ToString();
                    this.uTextReceiptUserName.Text = dtRtn.Rows[0]["ReceiptUserName"].ToString();
                    this.uTextCustomerAnalysisResult.Text = dtRtn.Rows[0]["CustomerAnalysisResult"].ToString();
                    this.uTextAttachmentFileName.Text = dtRtn.Rows[0]["AttachmentFileName"].ToString();
                    this.uDateSampleReceiptDate.Value = dtRtn.Rows[0]["SampleReceiptDate"].ToString();
                    this.uTextComment.Text = dtRtn.Rows[0]["Comment"].ToString();
                    this.uComboImputeProcess.Value = dtRtn.Rows[0]["ImputeProcessCode"].ToString();
                    this.uComboImputeDept.Value = dtRtn.Rows[0]["ImputeDeptCode"].ToString();
                    this.uComboFourMOneE.Value = dtRtn.Rows[0]["FourMOneE"].ToString();
                    this.uDateCompleteDate.Value = dtRtn.Rows[0]["CompleteDate"].ToString();
                    this.uCheckCompleteFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["CompleteFlag"]);
                    this.uTextMaterialDisposal.Text = dtRtn.Rows[0]["MaterialDisposal"].ToString();
                    this.uCheckClaimCancelFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["ClaimCancelFlag"]);
                    this.uCheckManFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["ManFlag"]);
                    this.uCheckMaterialFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["MaterialFlag"]);
                    this.uCheckMethodFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["MethodFlag"]);
                    this.uCheckMachineFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["MachineFlag"]);
                    this.uCheckEnviromentFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["EnviromentFlag"]);
                    this.uTextReasonDesc.Text = dtRtn.Rows[0]["ReasonDesc"].ToString();
                    this.uTextActionDesc.Text = dtRtn.Rows[0]["ActionDesc"].ToString();
                    this.uCheckReasonCompleteFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["AnalyseCompleteFlag"]);
                    this.uDateDueDate.Value = dtRtn.Rows[0]["DueDate"];
                    this.uDateInspectDate.Value = dtRtn.Rows[0]["InspectDate"];
                    this.uTextInspectUserID.Text = dtRtn.Rows[0]["InspectUserID"].ToString();
                    this.uTextInspectUserName.Text = dtRtn.Rows[0]["InspectUserName"].ToString();
                    this.uComboInspectResultFlag.Value = dtRtn.Rows[0]["InspectResultFlag"];
                    this.uTextInspectComment.Text = dtRtn.Rows[0]["InspectComment"].ToString();
                    this.uCheckCompleteFlag.Enabled = !(Convert.ToBoolean(dtRtn.Rows[0]["CompleteFlag"]));
                    if (dtRtn.Rows[0]["ReceiptCompleteFlag"].ToString().Equals("T"))
                    {
                        this.uCheckReceiptComplete.Checked = true;
                        this.uCheckReceiptComplete.Enabled = false;
                        this.uDateReceiptDate.ReadOnly = true;
                    }
                    else
                    {
                        this.uCheckReceiptComplete.Checked = false;
                        this.uCheckReceiptComplete.Enabled = true;
                        this.uDateReceiptDate.ReadOnly = true;
                    }
                    if (this.uCheckReasonCompleteFlag.Checked == true)
                    {
                        this.uCheckReasonCompleteFlag.Enabled = false;
                    }
                    else
                    {
                        this.uCheckReasonCompleteFlag.Enabled = true;
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 조회후 그리드에 바인딩
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        private void Search_Detail(string strPlantCode, string strClaimNo)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimD), "CustomerClaimD");
                QRPQAT.BL.QATCLM.CustomerClaimD clsDetail = new QRPQAT.BL.QATCLM.CustomerClaimD();
                brwChannel.mfCredentials(clsDetail);

                // 조회 Method 호출
                DataTable dtRtn = clsDetail.mfReadQATCustomerClaimD(strPlantCode, strClaimNo);

                this.uGridDetail.DataSource = dtRtn;
                this.uGridDetail.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDetail, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 4D8D 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="strLang">언어</param>
        private void Search_4D8D(string strPlantCode, string strClaimNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaim4D8D), "CustomerClaim4D8D");
                QRPQAT.BL.QATCLM.CustomerClaim4D8D cls4D8D = new QRPQAT.BL.QATCLM.CustomerClaim4D8D();
                brwChannel.mfCredentials(cls4D8D);

                DataTable dtRtn = cls4D8D.mfReadQATCustomerClaim4D8D(strPlantCode, strClaimNo, strLang);

                this.uGrid4D8DList.DataSource = dtRtn;
                this.uGrid4D8DList.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid4D8DList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 작성완료 Mail 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="strGubun"> W: 작성완료메일리스트 , F : 분석완료 메일리스트</param>
        /// <param name="strLang">언어</param>
        private void Search_Email(string strPlantCode, string strClaimNo, string strGubun, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimEMailUser), "CustomerClaimEMailUser");
                QRPQAT.BL.QATCLM.CustomerClaimEMailUser clsEmail = new QRPQAT.BL.QATCLM.CustomerClaimEMailUser();
                brwChannel.mfCredentials(clsEmail);

                DataTable dtRtn = clsEmail.mfReadQATCustomerClaimEMailUser(strPlantCode, strClaimNo, strGubun, strLang); 

                this.uGridEmail.DataSource = dtRtn;
                this.uGridEmail.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 Upload 메소드
        /// </summary>
        /// <param name="strClaimNo">발행번호</param>
        private void FileUpload(string strClaimNo)
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                // AttachmentFile
                if (this.uTextAttachmentFileName.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+발행번호+ATT+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextAttachmentFileName.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                           this.uComboPlantCode.Value.ToString() + "-" + strClaimNo + "-" + "ATT-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextAttachmentFileName.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                {
                    if (!this.uGrid4D8DList.Rows[i].Hidden)
                    {
                        if (this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            // 파일이름변경(공장코드+관리번호+순번+화일명)하여 복사하기//
                            FileInfo fileDoc = new FileInfo(this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                   this.uComboPlantCode.Value.ToString() + "-" + strClaimNo + "-" +
                                                   this.uGrid4D8DList.Rows[i].RowSelectorNumber.ToString() + "-" + fileDoc.Name;
                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            //변경한 화일이름으로 복사하기
                            File.Copy(this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                    }
                }

                // 업로드할 파일이 있는경우 파일 업로드
                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0016");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUpload_NonAssync();

                    // 업로드 완료후 복사된 파일 삭제
                    for (int i = 0; i < arrFile.Count; i++)
                    {
                        File.Delete(arrFile[i].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 삭제 메소드(전체)
        /// </summary>
        private void FileDelete()
        {
            try
            {
                // 첨부파일 저장경로정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0016");

                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                // AttachmentFile
                if (this.uTextAttachmentFileName.Text.Contains(this.uComboPlantCode.Value.ToString()) &&
                    this.uTextAttachmentFileName.Text.Contains(this.uTextClaimNo.Text) &&
                    !this.uTextAttachmentFileName.Text.Contains(":\\"))
                {
                    arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextAttachmentFileName.Text);
                }

                // 4D8D File
                for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                {
                    if (this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uComboPlantCode.Value.ToString()) &&
                        this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uTextClaimNo.Text) &&
                        !this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                    {
                        arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString());
                    }
                }

                if (arrFile.Count > 0)
                {
                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrFile
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUploadNoProgView();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }        

        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        public void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 컨토를 초기화 Method 호출
                ClearControls(this.uGroupBox1);
                ClearControls(this.uGroupBox2);
                ClearControls(this.uGroupBox3);

                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextReceiptUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextReceiptUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uCheckCompleteFlag.Enabled = true;
                this.uComboPlantCode.Enabled = true;

                this.uCheckReceiptComplete.Checked = false;
                this.uCheckReceiptComplete.Enabled = true;
                this.uDateReceiptDate.Value = DateTime.Now.ToString("yyyy-MM-dd");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 접수내용 편집 불가
        /// </summary>
        public void SetDiswritable_Receipt()
        {
            try
            {
                this.uComboIssueTypeCode.ReadOnly = true;
                this.uComboIssueTypeCode.Appearance.BackColor = Color.Gainsboro;
                this.uTextReceiptUserID.ReadOnly = true;
                this.uTextReceiptUserID.Appearance.BackColor = Color.Gainsboro;
                this.uDateReceiptDate.ReadOnly = true;
                this.uDateReceiptDate.Appearance.BackColor = Color.Gainsboro;
                this.uComboProductCode.ReadOnly = true;
                this.uComboProductCode.Appearance.BackColor = Color.Gainsboro;
                this.uComboCustomer.ReadOnly = true;
                this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
                this.uComboPackage.ReadOnly = true;
                this.uComboPackage.Appearance.BackColor = Color.Gainsboro;
                this.uComboCustomerProductSpec.ReadOnly = true;
                this.uComboCustomerProductSpec.Appearance.BackColor = Color.Gainsboro;
                this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
                this.uTextCustomerAnalysisResult.ReadOnly = true;
                this.uTextCustomerAnalysisResult.Appearance.BackColor = Color.Gainsboro;
                this.uTextAttachmentFileName.Appearance.BackColor = Color.Gainsboro;
                this.uTextFaultTypeDesc.ReadOnly = true;
                this.uTextFaultTypeDesc.Appearance.BackColor = Color.Gainsboro;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 원인 / 분석 편집 불가
        /// </summary>
        public void SetDiswritable_Analyse()
        {
            try
            {
                this.uDateSampleReceiptDate.ReadOnly = true;
                this.uDateSampleReceiptDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateCompleteDate.ReadOnly = true;
                this.uDateCompleteDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextComment.ReadOnly = true;
                this.uTextComment.Appearance.BackColor = Color.Gainsboro;
                this.uTextMaterialDisposal.ReadOnly = true;
                this.uTextMaterialDisposal.Appearance.BackColor = Color.Gainsboro;
                this.uTextReasonDesc.ReadOnly = true;
                this.uTextReasonDesc.Appearance.BackColor = Color.Gainsboro;
                this.uTextActionDesc.ReadOnly = true;
                this.uTextActionDesc.Appearance.BackColor = Color.Gainsboro;
                this.uComboImputeProcess.ReadOnly = true;
                this.uComboImputeProcess.Appearance.BackColor = Color.Gainsboro;
                this.uComboImputeDept.ReadOnly = true;
                this.uComboImputeDept.Appearance.BackColor = Color.Gainsboro;
                this.uDateDueDate.ReadOnly = true;
                this.uDateDueDate.Appearance.BackColor = Color.Gainsboro;
                this.uCheckManFlag.Enabled = false;
                this.uCheckMachineFlag.Enabled = false;
                this.uCheckMaterialFlag.Enabled = false;
                this.uCheckMethodFlag.Enabled = false;
                this.uCheckEnviromentFlag.Enabled = false;
                this.uGrid4D8DList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 접수내용 편집 가능
        /// </summary>
        public void SetWritable_Receipt()
        {
            try
            {
                this.uComboIssueTypeCode.ReadOnly = false;
                this.uComboIssueTypeCode.Appearance.BackColor = Color.PowderBlue;
                this.uTextReceiptUserID.ReadOnly = false;
                this.uTextReceiptUserID.Appearance.BackColor = Color.PowderBlue;
                this.uDateReceiptDate.ReadOnly = false;
                this.uDateReceiptDate.Appearance.BackColor = Color.White;
                this.uComboProductCode.ReadOnly = false;
                this.uComboProductCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboCustomer.ReadOnly = false;
                this.uComboCustomer.Appearance.BackColor = Color.PowderBlue;
                this.uComboPackage.ReadOnly = false;
                this.uComboPackage.Appearance.BackColor = Color.PowderBlue;
                this.uComboCustomerProductSpec.ReadOnly = false;
                this.uComboCustomerProductSpec.Appearance.BackColor = Color.White;
                this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
                this.uTextCustomerAnalysisResult.ReadOnly = false;
                this.uTextCustomerAnalysisResult.Appearance.BackColor = Color.White;
                this.uTextAttachmentFileName.Appearance.BackColor = Color.White;
                this.uTextFaultTypeDesc.ReadOnly = false;
                this.uTextFaultTypeDesc.Appearance.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void SetWritable_Analyse()
        {
            try
            {
                this.uDateSampleReceiptDate.ReadOnly = false;
                this.uDateSampleReceiptDate.Appearance.BackColor = Color.White;
                this.uDateCompleteDate.ReadOnly = false;
                this.uDateCompleteDate.Appearance.BackColor = Color.White;
                this.uTextComment.ReadOnly = false;
                this.uTextComment.Appearance.BackColor = Color.White;
                this.uTextMaterialDisposal.ReadOnly = false;
                this.uTextMaterialDisposal.Appearance.BackColor = Color.White;
                this.uTextReasonDesc.ReadOnly = false;
                this.uTextReasonDesc.Appearance.BackColor = Color.White;
                this.uTextActionDesc.ReadOnly = false;
                this.uTextActionDesc.Appearance.BackColor = Color.White;
                this.uComboImputeProcess.ReadOnly = false;
                this.uComboImputeProcess.Appearance.BackColor = Color.White;
                this.uComboImputeDept.ReadOnly = false;
                this.uComboImputeDept.Appearance.BackColor = Color.White;
                this.uDateDueDate.ReadOnly = false;
                this.uDateDueDate.Appearance.BackColor = Color.White;
                this.uCheckManFlag.Enabled = true;
                this.uCheckMachineFlag.Enabled = true;
                this.uCheckMaterialFlag.Enabled = true;
                this.uCheckMethodFlag.Enabled = true;
                this.uCheckEnviromentFlag.Enabled = true;
                this.uGrid4D8DList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //////public void SetDisWritable()
        //////{
        //////    try
        //////    {
        //////        #region 수정 불가능 / 색 변경
        //////        this.uComboIssueTypeCode.ReadOnly = true;
        //////        this.uComboIssueTypeCode.Appearance.BackColor = Color.Gainsboro;
        //////        this.uDateReceiptDate.ReadOnly = true;
        //////        this.uDateReceiptDate.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextReceiptUserID.ReadOnly = true;
        //////        this.uTextReceiptUserID.Appearance.BackColor = Color.Gainsboro;
        //////        this.uCheckReceiptComplete.Enabled = false;
        //////        this.uTextFaultTypeDesc.ReadOnly = true;
        //////        this.uTextFaultTypeDesc.Appearance.BackColor = Color.Gainsboro;
        //////        this.uComboProductCode.ReadOnly = true;
        //////        this.uComboProductCode.Appearance.BackColor = Color.Gainsboro;
        //////        this.uComboCustomer.ReadOnly = true;
        //////        this.uComboCustomer.Appearance.BackColor = Color.Gainsboro;
        //////        this.uComboPackage.ReadOnly = true;
        //////        this.uComboPackage.Appearance.BackColor = Color.Gainsboro;
        //////        this.uComboCustomerProductSpec.ReadOnly = true;
        //////        this.uComboCustomerProductSpec.Appearance.BackColor = Color.Gainsboro;
        //////        this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
        //////        this.uGrid4D8DList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
        //////        this.uTextCustomerAnalysisResult.ReadOnly = true;
        //////        this.uTextCustomerAnalysisResult.Appearance.BackColor = Color.Gainsboro;
        //////        this.uDateSampleReceiptDate.ReadOnly = true;
        //////        this.uDateSampleReceiptDate.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextComment.ReadOnly = true;
        //////        this.uTextComment.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextReasonDesc.ReadOnly = true;
        //////        this.uTextReasonDesc.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextActionDesc.ReadOnly = true;
        //////        this.uTextActionDesc.Appearance.BackColor = Color.Gainsboro;
        //////        this.uDateDueDate.ReadOnly = true;
        //////        this.uDateDueDate.Appearance.BackColor = Color.Gainsboro;
        //////        this.uComboImputeProcess.ReadOnly = true;
        //////        this.uComboImputeProcess.Appearance.BackColor = Color.Gainsboro;
        //////        this.uComboImputeDept.ReadOnly = true;
        //////        this.uComboImputeDept.Appearance.BackColor = Color.Gainsboro;
        //////        this.uCheckManFlag.Enabled = false;
        //////        this.uCheckMachineFlag.Enabled = false;
        //////        this.uCheckMaterialFlag.Enabled = false;
        //////        this.uCheckMethodFlag.Enabled = false;
        //////        this.uCheckEnviromentFlag.Enabled = false;
        //////        this.uDateInspectDate.ReadOnly = true;
        //////        this.uDateInspectDate.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextInspectUserID.ReadOnly = true;
        //////        this.uTextInspectUserID.Appearance.BackColor = Color.Gainsboro;
        //////        this.uComboInspectResultFlag.ReadOnly = true;
        //////        this.uComboInspectResultFlag.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextInspectComment.ReadOnly = true;
        //////        this.uTextInspectComment.Appearance.BackColor = Color.Gainsboro;
        //////        this.uDateCompleteDate.ReadOnly = true;
        //////        this.uDateCompleteDate.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextMaterialDisposal.ReadOnly = true;
        //////        this.uTextMaterialDisposal.Appearance.BackColor = Color.Gainsboro;
        //////        this.uTextAttachmentFileName.ReadOnly = true;
        //////        this.uTextAttachmentFileName.Appearance.BackColor = Color.Gainsboro;
        //////        #endregion
        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //////        frmErr.ShowDialog();
        //////    }
        //////    finally
        //////    { }
        //////}

        //////public void SetWritable()
        //////{
        //////    try
        //////    {
        //////        this.uComboIssueTypeCode.ReadOnly = false;
        //////        this.uComboIssueTypeCode.Appearance.BackColor = Color.PowderBlue;
        //////        this.uDateReceiptDate.ReadOnly = false;
        //////        this.uDateReceiptDate.Appearance.BackColor = Color.White;
        //////        this.uTextReceiptUserID.ReadOnly = false;
        //////        this.uTextReceiptUserID.Appearance.BackColor = Color.PowderBlue;
        //////        this.uCheckReceiptComplete.Enabled = true;
        //////        this.uTextFaultTypeDesc.ReadOnly = false;
        //////        this.uTextFaultTypeDesc.Appearance.BackColor = Color.White;
        //////        this.uComboProductCode.ReadOnly = false;
        //////        this.uComboProductCode.Appearance.BackColor = Color.PowderBlue;
        //////        this.uComboCustomer.ReadOnly = false;
        //////        this.uComboCustomer.Appearance.BackColor = Color.PowderBlue;
        //////        this.uComboPackage.ReadOnly = false;
        //////        this.uComboPackage.Appearance.BackColor = Color.PowderBlue;
        //////        this.uComboCustomerProductSpec.ReadOnly = false;
        //////        this.uComboCustomerProductSpec.Appearance.BackColor = Color.PowderBlue;
        //////        this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
        //////        this.uGrid4D8DList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Default;
        //////        this.uTextCustomerAnalysisResult.ReadOnly = false;
        //////        this.uTextCustomerAnalysisResult.Appearance.BackColor = Color.White;
        //////        this.uDateSampleReceiptDate.ReadOnly = false;
        //////        this.uDateSampleReceiptDate.Appearance.BackColor = Color.White;
        //////        this.uTextComment.ReadOnly = false;
        //////        this.uTextComment.Appearance.BackColor = Color.White;
        //////        this.uTextReasonDesc.ReadOnly = false;
        //////        this.uTextReasonDesc.Appearance.BackColor = Color.White;
        //////        this.uTextActionDesc.ReadOnly = false;
        //////        this.uTextActionDesc.Appearance.BackColor = Color.White;
        //////        this.uDateDueDate.ReadOnly = false;
        //////        this.uDateDueDate.Appearance.BackColor = Color.White;
        //////        this.uComboImputeProcess.ReadOnly = false;
        //////        this.uComboImputeProcess.Appearance.BackColor = Color.White;
        //////        this.uComboImputeDept.ReadOnly = false;
        //////        this.uComboImputeDept.Appearance.BackColor = Color.White;
        //////        this.uCheckManFlag.Enabled = true;
        //////        this.uCheckMachineFlag.Enabled = true;
        //////        this.uCheckMaterialFlag.Enabled = true;
        //////        this.uCheckMethodFlag.Enabled = true;
        //////        this.uCheckEnviromentFlag.Enabled = true;
        //////        this.uDateInspectDate.ReadOnly = false;
        //////        this.uDateInspectDate.Appearance.BackColor = Color.White;
        //////        this.uTextInspectUserID.ReadOnly = false;
        //////        this.uTextInspectUserID.Appearance.BackColor = Color.White;
        //////        this.uComboInspectResultFlag.ReadOnly = false;
        //////        this.uComboInspectResultFlag.Appearance.BackColor = Color.White;
        //////        this.uTextInspectComment.ReadOnly = false;
        //////        this.uTextInspectComment.Appearance.BackColor = Color.White;
        //////        this.uDateCompleteDate.ReadOnly = false;
        //////        this.uDateCompleteDate.Appearance.BackColor = Color.White;
        //////        this.uTextMaterialDisposal.ReadOnly = false;
        //////        this.uTextMaterialDisposal.Appearance.BackColor = Color.White;
        //////        this.uTextAttachmentFileName.ReadOnly = false;
        //////        this.uTextAttachmentFileName.Appearance.BackColor = Color.White;
        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //////        frmErr.ShowDialog();
        //////    }
        //////    finally
        //////    { }
        //////}

        /// <summary>
        /// 컨트롤 Clear Method
        /// </summary>
        /// <param name="c"></param>
        private void ClearControls(Control c)
        {
            try
            {
                foreach (Control Ctrl in c.Controls)
                {
                    if (Ctrl.GetType().ToString() != "Infragistics.Win.Misc.UltraLabel" || Ctrl.GetType().ToString() != "Infragistics.Win.Misc.UltraButton")
                    {
                        switch (Ctrl.GetType().ToString())
                        {
                            case "Infragistics.Win.UltraWinEditors.UltraTextEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraTextEditor)Ctrl).Text = "";
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraComboEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraComboEditor)Ctrl).Value = "";
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraDateTimeEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)Ctrl).Value = null;
                                break;
                            case "Infragistics.Win.UltraWinEditors.UltraCheckEditor":
                                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)Ctrl).Checked = false;
                                break;
                            case "Infragistics.Win.UltraWinGrid.UltraGrid":
                                while (((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows.Count > 0)
                                {
                                    ((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows[0].Delete(false);
                                }
                                break;
                            default:
                                //if (Ctrl.Controls.Count > 0)
                                //    ClearControls(Ctrl);
                                break;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        #endregion

        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 720;
                    for (int i = 0; i < uGridHeader.Rows.Count; i++)
                    {
                        uGridHeader.Rows[i].Fixed = false;
                    }

                    // 컨트롤 초기화
                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자동 행삭제
        private void uGridDetail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "Qty" || e.Cell.Column.Key == "RejectQty")
                {
                    if (Convert.ToDouble(e.Cell.Row.Cells["Qty"].Value).Equals(0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000881", "M000729",
                                        Infragistics.Win.HAlign.Right);

                        e.Cell.Row.Cells["Qty"].Activate();
                        this.uGridDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }

                    if (Convert.ToDouble(e.Cell.Row.Cells["Qty"].Value) >= Convert.ToDouble(e.Cell.Row.Cells["RejectQty"].Value))
                    {
                        //e.Cell.Row.Cells["Yield"].Value = (Convert.ToDouble(e.Cell.Row.Cells["Qty"].Value) / ((Convert.ToDouble(e.Cell.Row.Cells["Qty"].Value) - Convert.ToDouble(e.Cell.Row.Cells["RejectQty"].Value)) * 100));
                        //e.Cell.Row.Cells["Yield"].Value = 100 - (Convert.ToDouble(e.Cell.Row.Cells["RejectQty"].Value) / Convert.ToDouble(e.Cell.Row.Cells["Qty"].Value) * 100);
                        //e.Cell.Row.Cells["Yield"].Value = e.Cell.Row.Cells["Yield"].Value 
                            //= Math.Round(Convert.ToDecimal(e.Cell.Row.Cells["RejectQty"].Value.ToString()) / Convert.ToDecimal(e.Cell.Row.Cells["Qty"].Value.ToString()) * 100, 4, MidpointRounding.AwayFromZero);
                        double dbInQty = Convert.ToDouble(e.Cell.Row.Cells["Qty"].Value.ToString());
                        double dbOutQty = Convert.ToDouble(e.Cell.Row.Cells["RejectQty"].Value.ToString());

                        //e.Cell.Row.Cells["Yield"].Value = (dbInQty - dbOutQty) / dbInQty * 100;
                        e.Cell.Row.Cells["Yield"].Value = dbOutQty / dbInQty * 100;
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000881", "M000727",
                                        Infragistics.Win.HAlign.Right);

                        e.Cell.Row.Cells["Yield"].Value = 0.0;
                        e.Cell.Row.Cells["RejectQty"].Value = 0.0;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀에 변화가 있으면 이미지 변하게 하는 이벤트
        private void uGridDetail_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장콤보 Value Change 이벤트
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlantCode.Value == null)
                {
                    return;
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    //DataTable dtFaultType = new DataTable();
                    DataTable dtProcess = new DataTable();
                    DataTable dtDept = new DataTable();
                    DataTable dtPackage = new DataTable();

                    // ComboBox Item 삭제
                    //this.uComboFaultTypeCode.Items.Clear();
                    this.uComboDetectProcessCode.Items.Clear();
                    this.uComboImputeProcess.Items.Clear();
                    this.uComboImputeDept.Items.Clear();
                    this.uComboPackage.Items.Clear();

                    if (this.uComboPlantCode.Value.ToString() != "")
                    {
                        ////// 불량유형
                        ////// BL 연결
                        ////QRPBrowser brwChannel = new QRPBrowser();
                        ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                        ////QRPMAS.BL.MASQUA.InspectFaultType clsFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                        ////brwChannel.mfCredentials(clsFaultType);

                        ////dtFaultType = clsFaultType.mfReadInspectFaultTypeCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                        // 공정
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                        QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                        brwChannel.mfCredentials(clsProcess);

                        //dtProcess = clsProcess.mfReadProcessForCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                        dtProcess = clsProcess.mfReadProcessDetailProcessOperationType(this.uComboPlantCode.Value.ToString());

                        // 귀책부서
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                        QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                        brwChannel.mfCredentials(clsDept);

                        dtDept = clsDept.mfReadSYSDeptForCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                        // Package
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                        QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                        brwChannel.mfCredentials(clsPackage);
                        dtPackage = clsPackage.mfReadMASProductPackage_Customer(this.uComboPlantCode.Value.ToString(), this.uComboCustomer.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    }

                    WinComboEditor wCombo = new WinComboEditor();
                    ////// 불량유형
                    ////wCombo.mfSetComboEditor(this.uComboFaultTypeCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    ////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    ////    , "InspectFaultTypeCode", "InspectFaultTypeName", dtFaultType);

                    // Detect 공정
                    wCombo.mfSetComboEditor(this.uComboDetectProcessCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                        , "ProcessCode", "ProcessName", dtProcess);                    

                    // 귀책공정
                    wCombo.mfSetComboEditor(this.uComboImputeProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "선택", "선택", "선택"
                        , "DetailProcessOperationType", "ComboName", dtProcess);
                    this.uComboImputeProcess.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;

                    // 귀책부서
                    wCombo.mfSetComboEditor(this.uComboImputeDept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                        , "DeptCode", "DeptName", dtDept);

                    // Package
                    wCombo.mfSetComboEditor(this.uComboPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                            , "Package", "ComboName", dtPackage);

                    // 부서정보 콤보
                    wCombo.mfSetComboEditor(this.uComboDept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, true, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                        , "DeptCode", "DeptName", dtDept);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 부서 확인 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                // 발행완료된정보 Return
                if (this.uCheckReceiptComplete.Checked && !this.uCheckReceiptComplete.Enabled)
                    return;

                // 부서정보 콤보정보가 0인경우 Return
                if (this.uComboDept.Items.Count == 0)
                    return;

                // 부서코드 저장
                //string strDept = this.uComboDept.Value == null ? string.Empty : this.uComboDept.Value.ToString();
                string strDept = string.Empty; // "'1010','1008'";//,'D','E','H'";


                //Combo MultiSelect 선택된 정보 저장방법
                for (int i = 0; i < this.uComboDept.Items.Count; i++)
                {
                    if (this.uComboDept.Items[i].CheckState == CheckState.Checked)
                    {
                        if (strDept.Equals(string.Empty))
                            strDept = "'" + this.uComboDept.Items[i].DataValue.ToString() + "'";
                        else
                            strDept = strDept + ", '" + this.uComboDept.Items[i].DataValue.ToString() + "'";

                    }
                }

                // 공백이면 Return
                if (strDept.Equals(string.Empty))
                    return;

                // 사용자 정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자 정보 조회 매서드 실행
                DataTable dtUser = clsUser.mfReadSYSUser_InDept(this.uComboPlantCode.Value.ToString(), strDept, m_resSys.GetString("SYS_LANG"));

                // Resource 해제
                m_resSys.Close();
                clsUser.Dispose();

                // 유저정보중 Email정보가 있는 정보확인
                if (dtUser.Select("Email <> ''").Count() != 0)
                    dtUser = dtUser.Select("Email <> ''").CopyToDataTable();
                else
                    dtUser.Clear();

                // Email정보가 있는 정보만 Grid에 Bind
                this.uGridEmail.DataSource = dtUser;
                this.uGridEmail.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 텍스트 박스 팝업창
        // 고객사(검색조건)
        private void uTextSearchCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();
                frmPOP.ShowDialog();

                this.uTextSearchCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextSearchCustomerName.Text = frmPOP.CustomerName;                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }        

        // 고객사
        private void uTextCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();
                frmPOP.ShowDialog();

                this.uTextCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextCustomerName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }        

        // 접수자
        private void uTextReceiptUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uTextReceiptUserID.ReadOnly == false)
                {
                    if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlantCode.Value.ToString() != "" && this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                    {
                        this.uTextReceiptUserID.Text = frmPOP.UserID;
                        this.uTextReceiptUserName.Text = frmPOP.UserName;
                    }
                    else
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M000798", "M000275", "M001254" + this.uComboPlantCode.Text + "M000001"
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextReceiptUserID.Text = "";
                        this.uTextReceiptUserName.Text = "";
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region KeyDown 이벤트
        // 접수자 키다운이벤트
        private void uTextReceiptUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextReceiptUserID.ReadOnly == false)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uTextReceiptUserID.Text != "")
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            WinMessageBox msg = new WinMessageBox();

                            String strWriteID = this.uTextReceiptUserID.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextReceiptUserID.Text = "";
                                this.uTextReceiptUserName.Text = "";
                            }
                            else
                            {
                                this.uTextReceiptUserName.Text = strRtnUserName;
                            }
                        }
                    }
                    if (e.KeyCode == Keys.Back)
                    {
                        if (this.uTextReceiptUserID.TextLength <= 1 || this.uTextReceiptUserID.SelectedText == this.uTextReceiptUserID.Text)
                        {
                            this.uTextReceiptUserName.Text = "";
                        }
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }               
        #endregion

        // 행삭제 버튼 이벤트
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled == false)
                {
                    return;
                }
                else
                {
                    for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridDetail.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridDetail.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 작성완료 체크후 저장하면 수정불가하다는 메세지 띄워주는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckCompleteFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                // CheckEditor 가 활성화 상태일때
                //if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true)
                //{
                //    WinMessageBox msg = new WinMessageBox();
                //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "작성완료 확인", "작성완료 체크후 저장시 이후 수정할 수 없습니다",
                //                    Infragistics.Win.HAlign.Right);
                //}
                if (this.uCheckCompleteFlag.Checked == true && this.uComboImputeProcess.Value.ToString() == "선택")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000984", "M000321",
                                    Infragistics.Win.HAlign.Right);

                    this.uCheckCompleteFlag.Checked = false;                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 파일다운 UP/DOWN 버튼 이벤트
        // Attachment FIle
        private void uTextAttachmentFileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "UP")
                {
                    if (this.uTextAttachmentFileName.Enabled == true)
                    {
                        if (this.uCheckReceiptComplete.Enabled == true && this.uCheckReceiptComplete.Checked == false)
                        {
                            System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                            openFile.Filter = "All files (*.*)|*.*";
                            openFile.FilterIndex = 1;
                            openFile.RestoreDirectory = true;

                            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                string strImageFile = openFile.FileName;
                                if (CheckingSpecialText(strImageFile))
                                {
                                    WinMessageBox msg = new WinMessageBox();
                                    //첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);

                                    return;
                                }
                                this.uTextAttachmentFileName.Text = strImageFile;
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else if (e.Button.Key == "DOWN")
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextAttachmentFileName.Text.Contains(":\\") || this.uTextAttachmentFileName.Text.Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000796",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0016");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextAttachmentFileName.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextAttachmentFileName.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 헤더 그리드 더블클릭시 상세정보 조회
        private void uGridHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 클릭된 행 고정
                e.Row.Fixed = true;

                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strClaimNo = e.Row.Cells["ClaimNo"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 메소드 호출
                Search_HeaderDetail(strPlantCode, strClaimNo);
                Search_Detail(strPlantCode, strClaimNo);
                Search_4D8D(strPlantCode, strClaimNo, m_resSys.GetString("SYS_LANG"));
                Search_Email(strPlantCode, strClaimNo, "W", m_resSys.GetString("SYS_LANG"));

                if (this.uCheckReceiptComplete.Checked == true)
                    SetDiswritable_Receipt();
                else
                    SetWritable_Receipt();

                if (this.uCheckReasonCompleteFlag.Checked == true)
                    SetDiswritable_Analyse();
                else
                    SetWritable_Analyse();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = true;

                this.uComboPlantCode.Enabled = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 거래처 정보 조회 함수
        /// </summary>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        private DataTable GetVendorInfo(String strVendorCode)
        {
            DataTable dtVendor = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

                return dtVendor;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtVendor;
            }
            finally
            {
            }
        }

        private void frmQAT0002_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    //
                    //uGroupBoxContentsArea.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    //uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    //uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 고객사코드 입력 후 엔터시 고객사명을 보여준다.
        private void uTextCustomerCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //코드지우면 이름도 지움
                if (e.KeyData == Keys.Delete)
                {
                    this.uTextCustomerCode.Text = "";
                    this.uTextCustomerName.Text = "";
                }

                if (e.KeyData == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);                   
                    WinMessageBox msg = new WinMessageBox();
                   
                    string strCustomerCode = this.uTextCustomerCode.Text;
                    if (strCustomerCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M001184", "M000256", Infragistics.Win.HAlign.Left);
                        this.uTextCustomerCode.Focus();
                        return;
                    }
                    //BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                    QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                    brwChannel.mfCredentials(clsCustomer);

                    DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                    if (dtCustomer.Rows.Count > 0)
                    {
                        this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001115", "M000889", Infragistics.Win.HAlign.Left);
                        this.uTextCustomerName.Clear();                                                                   
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        // 고객사제품코드 입력 시 
        private void uTextCustomerProductSpec_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    WinComboEditor wCombo = new WinComboEditor();

                    string strCustomerProductSpec = this.uTextCustomerProductSpec.Text;
                    if (strCustomerProductSpec == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M000240", "M001184", "M000253", Infragistics.Win.HAlign.Default);
                        this.uTextCustomerProductSpec.Focus();
                        return;
                    }
                    //제품코드 정보를 불러올 BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);

                    DataTable dtProduct = clsProduct.mfReadMASCustomerProductSpec(m_resSys.GetString("SYS_PLANTCODE"), strCustomerProductSpec, m_resSys.GetString("SYS_LANG"));

                    if (dtProduct.Rows.Count > 0)
                    {
                        // 제품코드 콤보박스
                        wCombo.mfSetComboEditor(this.uComboProductCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "선택", "선택", "선택"
                            , "ProductCode", "ProductName", dtProduct);
                        this.uComboProductCode.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M000240", "M001184", "M001250", Infragistics.Win.HAlign.Default);
                        this.uTextCustomerProductSpec.Focus();
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //제품코드 선택시 고객사, Package 정보를 가지고 온다.
        private void uComboProductCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                WinComboEditor wCombo = new WinComboEditor();

                if (this.uComboProductCode.Value == null)
                {
                    return;
                }
                this.uTextCustomerCode.Text = "";
                this.uTextCustomerName.Text = "";
                this.uTextPakcage.Text = "";
                
                this.uComboProductCode.Appearance.BackColor = Color.PowderBlue;
                
                if (this.uComboProductCode.Value.ToString() != "선택")
                {
                    string strProductCode = this.uComboProductCode.Value.ToString();
                    //BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);

                    DataTable dtProduct = clsProduct.mfReadMASMaterialDetail(m_resSys.GetString("SYS_PLANTCODE"), strProductCode, m_resSys.GetString("SYS_LANG"));

                    if (dtProduct.Rows.Count > 0)
                    {
                        this.uTextCustomerCode.Text = dtProduct.Rows[0]["CustomerCode"].ToString();
                        this.uTextCustomerName.Text = dtProduct.Rows[0]["CustomerName"].ToString();
                        this.uTextPakcage.Text = dtProduct.Rows[0]["Package"].ToString();                   
                    }
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M000240", "M001184", "M001093", Infragistics.Win.HAlign.Default);
                    this.uTextCustomerProductSpec.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        private void uComboCustomer_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboCustomer.Value == null)
                {
                    return;
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DataTable dtPackage = new DataTable();

                    // ComboBox Item 삭제
                    this.uComboPackage.Items.Clear();

                    if (this.uComboCustomer.Value.ToString() != "")
                    {
                        // Package
                        QRPBrowser brwChannel= new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                        QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                        brwChannel.mfCredentials(clsPackage);
                        dtPackage = clsPackage.mfReadMASProductPackage_Customer(this.uComboPlantCode.Value.ToString(), this.uComboCustomer.Value.ToString(),m_resSys.GetString("SYS_LANG"));
                    }

                    WinComboEditor wCombo = new WinComboEditor();

                    // Package
                    wCombo.mfSetComboEditor(this.uComboPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                            , "Package", "ComboName", dtPackage);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPackage_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPackage.Value == null)
                {
                    return;
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DataTable dtPackage = new DataTable();

                    // ComboBox Item 삭제
                    this.uComboCustomerProductSpec.Items.Clear();

                    if (this.uComboPackage.Value.ToString() != "")
                    {
                        // Package
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                        QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                        brwChannel.mfCredentials(clsPackage);
                        dtPackage = clsPackage.mfReadMAProduct_CustomerSpec_WithPackage_CustomerCode(this.uComboPlantCode.Value.ToString()
                                                                                                , this.uComboCustomer.Value.ToString()
                                                                                                , this.uComboPackage.Value.ToString()
                                                                                                , m_resSys.GetString("SYS_LANG"));
                    }

                    WinComboEditor wCombo = new WinComboEditor();

                    // Package
                    wCombo.mfSetComboEditor(this.uComboCustomerProductSpec, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                            , "ComboCode", "ComboName", dtPackage);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboCustomerProductSpec_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboCustomerProductSpec.Value == null)
                {
                    return;
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DataTable dtProduct = new DataTable();

                    // ComboBox Item 삭제
                    this.uComboProductCode.Items.Clear();

                    if (this.uComboCustomerProductSpec.Value.ToString() != "")
                    {
                        // Package
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                        QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                        brwChannel.mfCredentials(clsProduct);
                        dtProduct = clsProduct.mfReadMASCustomerProductSpec(this.uComboPlantCode.Value.ToString()
                                                                            , this.uComboCustomerProductSpec.Value.ToString()
                                                                            , m_resSys.GetString("SYS_LANG"));
                    }

                    WinComboEditor wCombo = new WinComboEditor();

                    // Package
                    wCombo.mfSetComboEditor(this.uComboProductCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                            , "ProductCode", "ProductName", dtProduct);

                    if (dtProduct.Rows.Count > 0)
                    {
                        this.uComboProductCode.SelectedIndex = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Attachmen 첨부파일삭제
        private void uTextAttachmentFileName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextAttachmentFileName.ReadOnly == false)
                {
                    if (this.uTextAttachmentFileName.Text.Contains(this.uComboPlantCode.Value.ToString()) &&
                        this.uTextAttachmentFileName.Text.Contains(this.uTextClaimNo.Text) &&
                        this.uTextAttachmentFileName.Text.Contains("ATT"))
                    {
                        if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                        {
                            //화일서버 연결정보 가져오기
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                            //첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0016");

                            //첨부파일 삭제하기
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                            arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextAttachmentFileName.Text);

                            fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                , arrFile
                                                                , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.mfFileUploadNoProgView();

                            this.uTextAttachmentFileName.Clear();
                        }
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 4D8D List 행삭제 이벤트
        private void uButtonDelete4D8D_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    this.uGrid4D8DList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    // 첨부파일 저장경로정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0016");

                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                    for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid4D8DList.Rows[i].Cells["Check"].Value))
                        {
                            if (this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uComboPlantCode.Value.ToString()) &&
                                this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uTextClaimNo.Text) &&
                                !this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                            {
                                arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString());

                                this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value = string.Empty;
                                this.uGrid4D8DList.Rows[i].Cells["FilePath"].SetValue(string.Empty, false);
                            }
                            this.uGrid4D8DList.Rows[i].Hidden = true;
                        }
                    }

                    if (arrFile.Count > 0)
                    {
                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                            , arrFile
                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileUploadNoProgView();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 4D8D CellButton Click Event
        private void uGrid4D8DList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.uCheckReasonCompleteFlag.Enabled)
                {
                    if (e.Cell.Column.Key.Equals("FilePath"))
                    {
                        System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                        openFile.Filter = "All files (*.*)|*.*"; //"Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                        openFile.FilterIndex = 1;
                        openFile.RestoreDirectory = true;

                        if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            string strImageFile = openFile.FileName;
                            e.Cell.Value = strImageFile;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid4D8DList_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell CurrentCell;

                if (this.uGrid4D8DList.ActiveCell == null)
                    return;
                else
                    CurrentCell = this.uGrid4D8DList.ActiveCell;

                // 아직 작성완료한 상태가 아니면
                if (this.uCheckCompleteFlag.Enabled)
                {
                    if (CurrentCell.Column.Key.Equals("FilePath"))
                    {
                        // 삭제를 하는경우
                        if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                        {
                            // 아직 첨부파일이 올라간 상태가 아니면 셀 Text만 삭제
                            if (CurrentCell.Value.ToString().Contains(":\\"))
                            {
                                CurrentCell.SetValue(string.Empty, false);
                            }
                            else if (CurrentCell.Value.ToString().Contains(this.uComboPlantCode.Value.ToString()) &&
                                    CurrentCell.Value.ToString().Contains(this.uTextClaimNo.Text))
                            {
                                // 첨부파일이 서버에 올라간경우 첨부파일 삭제
                                //화일서버 연결정보 가져오기
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                brwChannel.mfCredentials(clsSysAccess);
                                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                                //첨부파일 경로정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                brwChannel.mfCredentials(clsSysFilePath);
                                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0016");

                                //첨부파일 삭제하기
                                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                                arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + CurrentCell.Value.ToString());

                                fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrFile
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.mfFileUploadNoProgView();

                                CurrentCell.SetValue(string.Empty, false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 셀 더블클릭시 파일 다운로드 이벤트
        private void uGrid4D8DList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (e.Cell.Column.ToString() == "FilePath")
                {
                    if (string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value.ToString().Contains(":\\") || e.Cell.Value == DBNull.Value)
                    {
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            // 화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                            // 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0016");

                            // 첨부파일 Download
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            arrFile.Add(e.Cell.Value.ToString());

                            // Download정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + "\\" + e.Cell.Value.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid4D8DList.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\")
                            || this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        // 화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlantCode.Value.ToString(), "S02");

                        // 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlantCode.Value.ToString(), "D0016");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid4D8DList.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (!this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\") &&
                                    !string.IsNullOrEmpty(this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString()))
                                {
                                    arrFile.Add(this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        if (arrFile.Count > 0)
                        {
                            fileAtt.ShowDialog();
                        }

                        // 폴더 열기
                        System.Diagnostics.Process.Start(strSaveFolder);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextInspectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uTextInspectUserID.ReadOnly == false)
                {
                    if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlantCode.Value.ToString() != "" && this.uComboPlantCode.Value.ToString() == frmPOP.PlantCode)
                    {
                        this.uTextInspectUserID.Text = frmPOP.UserID;
                        this.uTextInspectUserName.Text = frmPOP.UserName;
                    }
                    else
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M000798", "M000275", "M001254" + this.uComboPlantCode.Text + "M000001"
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextInspectUserID.Text = "";
                        this.uTextInspectUserName.Text = "";
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextInspectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextInspectUserID.ReadOnly == false)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uTextInspectUserID.Text != "")
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            WinMessageBox msg = new WinMessageBox();

                            String strWriteID = this.uTextInspectUserID.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextInspectUserID.Text = "";
                                this.uTextInspectUserName.Text = "";
                            }
                            else
                            {
                                this.uTextInspectUserName.Text = strRtnUserName;
                            }
                        }
                    }

                    if (e.KeyCode == Keys.Back)
                    {
                        if (this.uTextInspectUserID.TextLength <= 1 || this.uTextInspectUserID.SelectedText == this.uTextInspectUserID.Text)
                        {
                            this.uTextInspectUserName.Text = "";
                        }
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //원인 분석 완료 체크시
        private void uCheckReasonCompleteFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckReasonCompleteFlag.Checked == true)
                {
                    if (this.uDateCompleteDate.Value == null || this.uDateCompleteDate.Value.ToString().Length < 0)
                    {
                        this.uDateCompleteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                }
                else
                {
                    this.uDateCompleteDate.Value = null;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 접수완료 이메일
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strClaimNo"></param>
        public void SendMail_Receipt(string strPlantCode, string strClaimNo)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                //QRPQAT.BL.QATCLM.CustomerClaimH clsClaim = new QRPQAT.BL.QATCLM.CustomerClaimH();
                //brwChannel.mfCredentials(clsClaim);

                //DataTable dtSendLine = clsClaim.mfReadQATCustomerSendMailLine(strPlantCode);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUserInfo = clsUser.mfReadSYSUser(strPlantCode, this.uTextReceiptUserID.Text, m_resSys.GetString("SYS_LANG"));

                //첨부파일을 위한 FileServer 연결정보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlantCode.Value.ToString(), "S02");

                //메일 발송 첨부파일 경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFileDestPath);
                DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                //고객불만관리 첨부파일 저장경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysTargetFilePath);
                DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0016");

                QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                ArrayList arrFile = new ArrayList();    //복사할 파일 서버경로, 파일명
                ArrayList arrAttachFile = new ArrayList();  //파일을 복사할 경로
                ArrayList arrFileNonPath = new ArrayList(); //첨부할 파일명

                //AttachentFileName이 있을경우

                string strFileName = "";
                if (!this.uTextAttachmentFileName.Text.Equals(string.Empty))
                {
                    if (this.uTextAttachmentFileName.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextAttachmentFileName.Text);
                        strFileName = this.uComboPlantCode.Value.ToString() + "-" + strClaimNo + "-ATT-" + fileDoc.Name;
                    }
                    else
                    {
                        strFileName = this.uTextAttachmentFileName.Text;
                    }
                    string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName;
                    string TargetFile = "QATCustomerClaimFile/" + strFileName;
                    string NonPathFile = strFileName;
                    arrAttachFile.Add(TargetFile);
                    arrFile.Add(DestFile);
                    arrFileNonPath.Add(NonPathFile);

                    //메일에 첨부할 파일을 서버에서 검색, 메일 첨부파일 폴더로 복사
                    fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                        , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrAttachFile
                                                        , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                        , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                }

                brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                QRPCOM.BL.Mail clsMail = new QRPCOM.BL.Mail();
                brwChannel.mfCredentials(clsMail);

                if (arrFile.Count != 0)
                {
                    fileAtt.mfFileUpload_NonAssync();
                }

                // 2012-11-16 추가
                string strLang = m_resSys.GetString("SYS_LANG");

                string strMail = string.Empty;

                string strCustomer = string.Empty;
                string strPackage = string.Empty;
                string strFaultTypeName = string.Empty;
                string strQty = string.Empty;
                string strFont = string.Empty;
                string strTilte = string.Empty;
                string strContents = string.Empty;
                string strContents1 = string.Empty;

                if (strLang.Equals("KOR"))
                {
                    strMail = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객불만 접수 통보 메일</SPAN></P>";

                    strCustomer = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객사</SPAN>&nbsp&nbsp&nbsp : " + this.uComboCustomer.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uComboPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">불량명</SPAN>&nbsp&nbsp&nbsp : " + this.uTextFaultTypeDesc.Text + "</P></BR>";
                    strQty = "수량";
                    strFont = "굴림";
                    strTilte = "[QRP]. 고객불만 등록 확인 부탁드립니다.";
                    strContents = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 접수 통보메일</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";
                }
                else if (strLang.Equals("CHN"))
                {
                    strMail = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">顾客投诉接收通知邮件</SPAN></P>";

                    strCustomer = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">客户社</SPAN>&nbsp&nbsp&nbsp : " + this.uComboCustomer.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uComboPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">不良名</SPAN>&nbsp&nbsp&nbsp : " + this.uTextFaultTypeDesc.Text + "</P></BR>";
                    strQty = "数量";
                    strFont = "SimSun";
                    strTilte = "[QRP]. 请确认顾客投诉登录";
                    strContents = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">顾客投诉接收通知邮件</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";

                }
                else if (strLang.Equals("ENG"))
                {
                    strMail = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객불만 접수 통보 메일</SPAN></P>";

                    strCustomer = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객사</SPAN>&nbsp&nbsp&nbsp : " + this.uComboCustomer.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uComboPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">불량명</SPAN>&nbsp&nbsp&nbsp : " + this.uTextFaultTypeDesc.Text + "</P></BR>";
                    strQty = "수량";
                    strFont = "굴림";
                    strTilte = "[QRP]. 고객불만 등록 확인 부탁드립니다.";
                    strContents = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 접수 통보메일</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";
                }

                // 2012-11-16
              

                string strLotNo = "";
                //string strYield = "";
                for (int k = 0; k < this.uGridDetail.Rows.Count; k++)
                {
                    double dbYield = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["Yield"].Value.ToString());
                    string strRoundYield = RoundValue(dbYield);
                    double dbInQty = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["Qty"].Value.ToString());
                    string strInQty = Math.Round(dbInQty).ToString();
                    double dbReject = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["RejectQty"].Value.ToString());
                    string strReject = Math.Round(dbReject).ToString();
                    
                    if (!this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString().Equals(string.Empty))
                    {
                        if (strLotNo.Equals(string.Empty))
                        {
                            strLotNo = "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString() + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">" + strQty + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "/" + (dbInQty - dbReject).ToString() + " - " + strRoundYield + "&nbsp; &nbsp; ";
                                    //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">불량</SPAN> : " + strReject + "&nbsp; &nbsp; "
                                    //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + strRoundYield + "%</P><br>";
                            //strYield = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + this.uGridDetail.Rows[k].Cells["Yield"].Value.ToString();
                        }
                        else
                        {
                            strLotNo = strLotNo + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString() + "&nbsp; &nbsp; "
                                + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">" + strQty + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "/" + (dbInQty - dbReject).ToString() + " - " + strRoundYield + "&nbsp; &nbsp; ";
                                    //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">IN</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "&nbsp; &nbsp; "
                                    //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Reject</SPAN> : " + strReject + "&nbsp; &nbsp; "
                                    //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + strRoundYield + "%</P><br>";
                            //strYield = strYield + ", " + this.uGridDetail.Rows[k].Cells["Yield"].Value.ToString();
                        }
                    }
                }

                //// 메일수신인   : 품질부서, CS부서, 공정기술부서   S3800, S5001, S5100, 그리드 E-mail List -- 2012-07-09 원하는 유저로 변경
                //for (int i = 0; i < dtSendLine.Rows.Count; i++)
                //{
                //    bool bolRtn = clsMail.mfSendSMTPMail(strPlantCode
                //                            , dtUserInfo.Rows[0]["EMail"].ToString()
                //                            , dtUserInfo.Rows[0]["UserName"].ToString()
                //                            , dtSendLine.Rows[i]["EMail"].ToString() //Test : mespjt36@bokwang.com , dtSendLine.Rows[i]["EMail"].ToString()
                //                            , "[QRP]. 고객불만 등록 확인 부탁드립니다."
                //                            , "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 접수 통보메일</SPAN></STRONG></P>"
                //                            + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>"
                //                            + strCustomer + strPackage + strFaultTypeName + strLotNo + "</Body>" + "</HTML>"
                //                            , arrFileNonPath);

                //    if (!bolRtn)
                //    {
                //        WinMessageBox msg = new WinMessageBox();

                //        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                //                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                msg.GetMessge_Text("M000083",m_resSys.GetString("SYS_LANG")), "고객불만 등록 메일 전송결과 확인"
                //                , "고객불만 등록 / 조회 에서  " + dtSendLine.Rows[i]["UserName"].ToString() + msg.GetMessge_Text("M000005",m_resSys.GetString("SYS_LANG")),
                //                Infragistics.Win.HAlign.Right);
                //        return;
                //    }
                //}
                for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                {
                    if (this.uGridEmail.Rows[i].Hidden)
                        continue;

                    if (this.uGridEmail.Rows[i].Cells["EMail"].Value.ToString().Equals(string.Empty))
                        continue;

                    bool bolRtn = clsMail.mfSendSMTPMail(strPlantCode
                                            , dtUserInfo.Rows[0]["EMail"].ToString()
                                            , dtUserInfo.Rows[0]["UserName"].ToString() //Email List Grid 사용자에게 메일 발송
                                            , this.uGridEmail.Rows[i].GetCellValue("EMail").ToString()
                                            , strTilte
                                            , strContents
                                            + strContents1
                                            + strCustomer + strPackage + strFaultTypeName + strLotNo + "</Body>" + "</HTML>"
                                            , arrFileNonPath);

                    if (!bolRtn)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M000083", "M001444"
                                , "M001445" + this.uGridEmail.Rows[i].Cells["UserName"].Value.ToString() + "M000005",
                                Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

        }

        /// <summary>
        /// 분석완료 이메일
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strClaimNo"></param>
        public void SendMail_Complete(string strPlantCode, string strClaimNo)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimEMailUser), "CustomerClaimEMailUser");
                QRPQAT.BL.QATCLM.CustomerClaimEMailUser clsAssyMail = new QRPQAT.BL.QATCLM.CustomerClaimEMailUser();
                brwChannel.mfCredentials(clsAssyMail);

                DataTable dtSendLine = clsAssyMail.mfReadQATCustomerClaimEMailUser(strPlantCode, strClaimNo, "F", m_resSys.GetString("SYS_LANG"));

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUserInfo = clsUser.mfReadSYSUser(strPlantCode, this.uTextReceiptUserID.Text, m_resSys.GetString("SYS_LANG"));

                //첨부파일을 위한 FileServer 연결정보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlantCode.Value.ToString(), "S02");

                //메일 발송 첨부파일 경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFileDestPath);
                DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                //고객불만관리 첨부파일 저장경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysTargetFilePath);
                DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0016");

                QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                ArrayList arrFile = new ArrayList();    //복사할 파일 서버경로, 파일명
                ArrayList arrAttachFile = new ArrayList();  //파일을 복사할 경로
                ArrayList arrFileNonPath = new ArrayList(); //첨부할 파일명

                string strFileName = "";
                for (int i = 0; i < this.uGrid4D8DList.Rows.Count; i++)
                {
                    if (!this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Equals(string.Empty))
                    {
                        if (this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString());
                            strFileName = strPlantCode + "-" + strClaimNo + "-" + this.uGrid4D8DList.Rows[i].Cells["Seq"].Value.ToString() + "-" + fileDoc.Name;
                        }
                        else
                        {
                            strFileName = this.uGrid4D8DList.Rows[i].Cells["FilePath"].Value.ToString();
                        }
                    }

                    string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName;
                    string TargetFile = "QATCustomerClaimFile/" + strFileName;
                    string NonPathFile = strFileName;
                    arrAttachFile.Add(TargetFile);
                    arrFile.Add(DestFile);
                    arrFileNonPath.Add(NonPathFile);
                }
                //파일 복사
                if (arrFile.Count > 0)
                {
                    fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                        , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrAttachFile
                                                        , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                        , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                }

                brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                QRPCOM.BL.Mail clsMail = new QRPCOM.BL.Mail();
                brwChannel.mfCredentials(clsMail);

                if (arrFile.Count != 0)
                {
                    fileAtt.mfFileUpload_NonAssync();
                }

                string strLang = m_resSys.GetString("SYS_LANG");
                string strQty = string.Empty;
                string strFont = string.Empty;
                string strTilte = string.Empty;
                string strContents = string.Empty;
                string strContents1 = string.Empty;


                string strCustomer = string.Empty;
                string strMail= string.Empty;
                string strPackage = string.Empty;
                string strFaultTypeName = string.Empty;
                string strReason = string.Empty;
                string strActionDesc = string.Empty;

                if (strLang.Equals("KOR"))
                {
                    strMail = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객불만 접수 통보 메일</SPAN></P>";

                    strCustomer = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객사</SPAN>&nbsp&nbsp&nbsp : " + this.uComboCustomer.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uComboPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">불량명</SPAN>&nbsp&nbsp&nbsp : " + this.uTextFaultTypeDesc.Text + "</P><BR>";

                    strReason = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">발생원인</SPAN> : " + this.uTextReasonDesc.Text + "</P>";
                    strActionDesc = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">대책사항</SPAN> : " + this.uTextActionDesc.Text + "</P>";

                    strQty = "수량";
                    strFont = "굴림";
                    strTilte = "[QRP]. 고객불만 처리완료 확인 부탁드립니다.";
                    strContents = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 처리가 완료되었습니다.</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";
                }
                else if (strLang.Equals("CHN"))
                {
                    strMail = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\"> 顾客投诉接收通知邮件</SPAN></P>";

                    strCustomer = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">客户社</SPAN>&nbsp&nbsp&nbsp : " + this.uComboCustomer.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uComboPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">不良名</SPAN>&nbsp&nbsp&nbsp : " + this.uTextFaultTypeDesc.Text + "</P><BR>";

                    strReason = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">发生原因</SPAN> : " + this.uTextReasonDesc.Text + "</P>";
                    strActionDesc = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">对策事项</SPAN> : " + this.uTextActionDesc.Text + "</P>";

                    strQty = "数量";
                    strFont = "SimSun";
                    strTilte = "[QRP]. 请确认顾客不满处理完成";
                    strContents = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"> 顾客投诉已处理完成</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";
                }
                else if (strLang.Equals("ENG"))
                {
                    strMail = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객불만 접수 통보 메일</SPAN></P>";

                    strCustomer = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객사</SPAN>&nbsp&nbsp&nbsp : " + this.uComboCustomer.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uComboPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">불량명</SPAN>&nbsp&nbsp&nbsp : " + this.uTextFaultTypeDesc.Text + "</P><BR>";

                    strReason = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">발생원인</SPAN> : " + this.uTextReasonDesc.Text + "</P>";
                    strActionDesc = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">대책사항</SPAN> : " + this.uTextActionDesc.Text + "</P>";

                    strQty = "수량";
                    strFont = "굴림";
                    strTilte = "[QRP]. 고객불만 처리완료 확인 부탁드립니다.";
                    strContents = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 처리가 완료되었습니다.</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";
                }

                

                string strLotNo = "";
                string strYield = "";
                for (int k = 0; k < this.uGridDetail.Rows.Count; k++)
                {
                    double dbYield = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["Yield"].Value.ToString());
                    string strRoundYield = RoundValue(dbYield);
                    double dbInQty = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["Qty"].Value.ToString());
                    string strInQty = Math.Round(dbInQty).ToString();
                    double dbReject = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["RejectQty"].Value.ToString());
                    string strReject = Math.Round(dbReject).ToString();
                    if (!this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString().Equals(string.Empty))
                    {
                        if (strLotNo.Equals(string.Empty))
                        {
                            strLotNo = "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString() + "&nbsp; &nbsp; "
                                + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">" + strQty + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "/" + (dbInQty - dbReject).ToString() + " - " + strRoundYield + "&nbsp; &nbsp; ";
                                    // + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">IN</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "&nbsp; &nbsp; "
                                    //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Reject</SPAN> : " + strReject + "&nbsp; &nbsp; "
                                    //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + strRoundYield + "%</P><br>";
                        }
                        else
                        {
                            strLotNo = strLotNo + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString() + "&nbsp; &nbsp; "
                                + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">" + strQty + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "/" + (dbInQty - dbReject).ToString() + " - " + strRoundYield + "&nbsp; &nbsp; ";
                                     //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">IN</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "&nbsp; &nbsp; "
                                     //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Reject</SPAN> : " + strReject + "&nbsp; &nbsp; "
                                     //+ "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + strRoundYield + "%</P><br>";
                        }
                    }
                }



                ///테스트
                ////////clsMail.mfSendSMTPMail(strPlantCode
                ////////                            , dtUserInfo.Rows[0]["EMail"].ToString()
                ////////                            , dtUserInfo.Rows[0]["UserName"].ToString()
                ////////                            , "mespjt37@bokwang.com"
                ////////                            , "[QRP].품질보증관리 ▶ 고객불만 등록 / 조회▶ 고객불만 처리완료 확인 부탁드립니다."
                ////////                            , "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG>[QRP].품질보증관리 ▶ 고객불만 관리 ▶ <SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 접수 통보메일</SPAN></STRONG></P>"
                ////////                            + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>"
                ////////                            + strCustomer + strPackage + strFaultTypeName + strLotNo + strReason + strActionDesc + "</Body>" + "</HTML>"
                ////////                            , arrFileNonPath);


                for (int i = 0; i < dtSendLine.Rows.Count; i++)
                {
                    bool bolRtn = clsMail.mfSendSMTPMail(strPlantCode
                                            , dtUserInfo.Rows[0]["EMail"].ToString()
                                            , dtUserInfo.Rows[0]["UserName"].ToString()
                                            , dtSendLine.Rows[i]["EMail"].ToString() //mespjt36
                                            , strTilte
                                            , "<HTML>" +
                                                            "<HEAD>" +
                                                            "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                            "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                            "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                            "</HEAD>" +
                                                            "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" 
                                            + strContents
                                            + strContents1
                                            + strCustomer + strPackage + strFaultTypeName + strLotNo + strReason + strActionDesc + "</Body>" + "</HTML>"
                                            , arrFileNonPath);

                    if (!bolRtn)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M000083", "M001444"
                                , "M001445  " + dtSendLine.Rows[i]["UserName"].ToString() + "M000005",
                                Infragistics.Win.HAlign.Right);
                        return;
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Yeild값 반올림
        /// </summary>
        /// <param name="dbYield"></param>
        /// <returns></returns>
        private string RoundValue(double dbYield)
        {
            try
            {
                double returnValue = Math.Round(dbYield * 1000);

                return (returnValue / 1000).ToString();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return "0";
            }
            finally
            { }
        }

        private void uGridEmail_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    if (e.Cell.Column.Key.Equals("UserID"))
                    {
                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        if (this.uComboPlantCode.Value.ToString().Equals(string.Empty) || this.uComboPlantCode.SelectedIndex.Equals(0))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);
                            return;
                        }

                        frmPOP0011 frmPOP = new frmPOP0011();
                        frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                        frmPOP.ShowDialog();

                        if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                        {
                            DialogResult Result = new DialogResult();
                            WinMessageBox msg = new WinMessageBox();
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M000798", "M000275", "M001254" + this.uComboPlantCode.Text + "M000009"
                                                        , Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Cells["UserID"].Value = string.Empty;
                            e.Cell.Row.Cells["UserName"].Value = string.Empty;
                            e.Cell.Row.Cells["DeptCode"].Value = string.Empty;
                            e.Cell.Row.Cells["DeptName"].Value = string.Empty;
                            e.Cell.Row.Cells["EMail"].Value = string.Empty;
                        }
                        else
                        {
                            e.Cell.Row.Cells["UserID"].Value = frmPOP.UserID;
                            e.Cell.Row.Cells["UserName"].Value = frmPOP.UserName;
                            e.Cell.Row.Cells["DeptName"].Value = frmPOP.DeptName;
                            e.Cell.Row.Cells["DeptCode"].Value = frmPOP.DeptCode;
                            e.Cell.Row.Cells["EMail"].Value = frmPOP.EMail;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridEmail.ActiveCell == null)
                    return;

                if (this.uCheckReceiptComplete.Enabled)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridEmail.ActiveCell;
                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (!uCell.Text.Equals(string.Empty))
                            {
                                //System ResourceInfo
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                                // BL 연결
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                                brwChannel.mfCredentials(clsUser);

                                DataTable dtUserInfo = clsUser.mfReadSYSUser(uCell.Value.ToString(), uCell.Text, m_resSys.GetString("SYS_LANG"));

                                if (dtUserInfo.Rows.Count > 0)
                                {
                                    uCell.Row.Cells["UserName"].Value = dtUserInfo.Rows[0]["UserName"].ToString();
                                    uCell.Row.Cells["DeptCode"].Value = dtUserInfo.Rows[0]["DeptCode"].ToString();
                                    uCell.Row.Cells["DeptName"].Value = dtUserInfo.Rows[0]["DeptName"].ToString();
                                    uCell.Row.Cells["EMail"].Value = dtUserInfo.Rows[0]["Email"].ToString();
                                }
                                else
                                {
                                    DialogResult Result = new DialogResult();
                                    WinMessageBox msg = new WinMessageBox();
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001095", "M000366", "M000368"
                                                                , Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                    else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridEmail.ActiveCell;
                        if (uCell.Text.Equals(string.Empty))
                            return;

                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (uCell.Text.Length <= 1 || uCell.SelText == uCell.Text)
                            {
                                uCell.Row.Delete(false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                    e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridEmail, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 작성완료 메일리스트 행삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDel_Email_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridEmail.Rows[i].Cells["Check"].Value))
                            this.uGridEmail.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 분석완료 메일리스트 팝업창
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonEmailPopUp_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboPlantCode.Value == null || this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                    return;

                if (this.uTextClaimNo.Text.Equals(string.Empty))
                    return;

                if (this.uCheckReasonCompleteFlag.Checked && this.uCheckReasonCompleteFlag.Enabled.Equals(false))
                    return;

                frmQAT0002_P1 frmEmail = new frmQAT0002_P1();
                frmEmail.PlantCode = this.uComboPlantCode.Value.ToString();
                frmEmail.ClaimNo = this.uTextClaimNo.Text;
                frmEmail.ShowDialog();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        public void SendMail_Receipt_TEST(string strPlantCode, string strClaimNo,string strUserID)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                //QRPQAT.BL.QATCLM.CustomerClaimH clsClaim = new QRPQAT.BL.QATCLM.CustomerClaimH();
                //brwChannel.mfCredentials(clsClaim);

                //DataTable dtSendLine = clsClaim.mfReadQATCustomerSendMailLine(strPlantCode);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUserInfo = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                //첨부파일을 위한 FileServer 연결정보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlantCode.Value.ToString(), "S02");

                //메일 발송 첨부파일 경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFileDestPath);
                DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                //고객불만관리 첨부파일 저장경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysTargetFilePath);
                DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0016");

                QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                ArrayList arrFile = new ArrayList();    //복사할 파일 서버경로, 파일명
                ArrayList arrAttachFile = new ArrayList();  //파일을 복사할 경로
                ArrayList arrFileNonPath = new ArrayList(); //첨부할 파일명

                //AttachentFileName이 있을경우

                string strFileName = "";
                if (!this.uTextAttachmentFileName.Text.Equals(string.Empty))
                {
                    if (this.uTextAttachmentFileName.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextAttachmentFileName.Text);
                        strFileName = this.uComboPlantCode.Value.ToString() + "-" + strClaimNo + "-ATT-" + fileDoc.Name;
                    }
                    else
                    {
                        strFileName = this.uTextAttachmentFileName.Text;
                    }
                    string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName;
                    string TargetFile = "QATCustomerClaimFile/" + strFileName;
                    string NonPathFile = strFileName;
                    arrAttachFile.Add(TargetFile);
                    arrFile.Add(DestFile);
                    arrFileNonPath.Add(NonPathFile);

                    //메일에 첨부할 파일을 서버에서 검색, 메일 첨부파일 폴더로 복사
                    fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                        , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrAttachFile
                                                        , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                        , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                }

                brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                QRPCOM.BL.Mail clsMail = new QRPCOM.BL.Mail();
                brwChannel.mfCredentials(clsMail);

                if (arrFile.Count != 0)
                {
                    fileAtt.mfFileUpload_NonAssync();
                }
                string strMail = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객불만 접수 통보 메일</SPAN></P>";

                string strCustomer = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객사</SPAN>&nbsp&nbsp&nbsp : " + this.uComboCustomer.Value.ToString() + "</P>";
                string strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uComboPackage.Value.ToString() + "</P>";
                string strFaultTypeName = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">불량명</SPAN>&nbsp&nbsp&nbsp : " + this.uTextFaultTypeDesc.Text + "</P></BR>";

                string strLotNo = "";
                //string strYield = "";
                for (int k = 0; k < this.uGridDetail.Rows.Count; k++)
                {
                    double dbYield = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["Yield"].Value.ToString());
                    string strRoundYield = RoundValue(dbYield);
                    double dbInQty = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["Qty"].Value.ToString());
                    string strInQty = Math.Round(dbInQty).ToString();
                    double dbReject = Convert.ToDouble(this.uGridDetail.Rows[k].Cells["RejectQty"].Value.ToString());
                    string strReject = Math.Round(dbReject).ToString();

                    if (!this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString().Equals(string.Empty))
                    {
                        if (strLotNo.Equals(string.Empty))
                        {
                            strLotNo = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString() + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">IN</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Reject</SPAN> : " + strReject + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + strRoundYield + "%</P><br>";
                            //strYield = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + this.uGridDetail.Rows[k].Cells["Yield"].Value.ToString();
                        }
                        else
                        {
                            strLotNo = strLotNo + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + this.uGridDetail.Rows[k].Cells["LotNo"].Value.ToString() + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">IN</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Reject</SPAN> : " + strReject + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Yield</SPAN> : " + strRoundYield + "%</P><br>";
                            //strYield = strYield + ", " + this.uGridDetail.Rows[k].Cells["Yield"].Value.ToString();
                        }
                    }
                }

               

                // 메일수신인   : 그리드 E-mail List
                for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                {
                    if (this.uGridEmail.Rows[i].Hidden)
                        continue;

                    if (this.uGridEmail.Rows[i].Cells["EMail"].Value.ToString().Equals(string.Empty))
                        continue;

                    bool bolRtn = clsMail.mfSendSMTPMail(strPlantCode
                                            , dtUserInfo.Rows[0]["EMail"].ToString()
                                            , dtUserInfo.Rows[0]["UserName"].ToString() //Email List Grid 사용자에게 메일 발송
                                            , this.uGridEmail.Rows[i].GetCellValue("EMail").ToString()
                                            , "[QRP]. 고객불만 등록 확인 부탁드립니다."
                                            , "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 접수 통보메일</SPAN></STRONG></P>"
                                            + "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>"
                                            + strCustomer + strPackage + strFaultTypeName + strLotNo + "</Body>" + "</HTML>"
                                            , arrFileNonPath);

                    if (!bolRtn)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                msg.GetMessge_Text("M000083", m_resSys.GetString("SYS_LANG")), "고객불만 등록 메일 전송결과 확인"
                                , "고객불만 등록 / 조회 에서  " + this.uGridEmail.Rows[i].Cells["UserName"].Value.ToString() + msg.GetMessge_Text("M000005", m_resSys.GetString("SYS_LANG")),
                                Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
             
            WinMessageBox msg = new WinMessageBox();
            DialogResult dr = msg.mfSetMessageBox(MessageBoxType.YesNoCancel, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "선택", "", "Y : 접수 , N : 분석완료", Infragistics.Win.HAlign.Right);

            if(dr == DialogResult.Yes)
                SendMail_Receipt_TEST(this.uComboPlantCode.Value.ToString(), this.uTextClaimNo.Text, "TESTUSER");
            if(dr == DialogResult.No)
                SendMail_Complete(this.uComboPlantCode.Value.ToString(), this.uTextClaimNo.Text);
        }


        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }


    }
}
