﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINS0017L.cs                                        */
/* 프로그램명   : 원자재 이상발생 관리(Hold Lot처리)                    */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-07-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINS0017L : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수


        QRPGlobal SysRes = new QRPGlobal();

        public frmINS0017L()
        {
            InitializeComponent();
        }


        private void frmINS0017L_Activated(object sender, EventArgs e)
        {
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINS0017L_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("원자재 이상발생 Hold Lot관리", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method
        private void InitEtc()
        {
            try
            {
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextSearchMaterialCode.MaxLength = 20;

                this.uDateSearchFromGRDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateSearchToGRDate.Value = DateTime.Now.ToString("yyyy-MM-dd");


                this.uTextSearchLotNo.ImeMode = ImeMode.Disable;
                this.uTextSearchLotNo.CharacterCasing = CharacterCasing.Upper;
                this.uTextSearchMaterialCode.ImeMode = ImeMode.Disable;
                this.uTextSearchMaterialCode.CharacterCasing = CharacterCasing.Upper;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 라벨 초기화
                WinLabel winLabel = new WinLabel();
                winLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSearchGRDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSearchHistory, "이력조회", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //콤보박스 초기화
                WinComboEditor combo = new WinComboEditor();

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                QRPBrowser brwChannel = new QRPBrowser();

                #region 조회 그리드

                //일반설정
                wGrid.mfInitGeneralGrid(this.uGridAbnormal, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "OK", "합격", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "NG", "불합격", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "ReqNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "ReqSeq", "관리번호순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 4
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "ReqLotSeq", "관리번호Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "0", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "FullReqNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "AbnormalType", "이상발생구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "VendorName", "거래처명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "GRNo", "GRNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "StdNumber", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "WriteDate", "등록일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "AbnormalType", "이상발생구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "StatusFlag", "상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "VendorName", "거래처명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "MaterialGroupName", "자재그룹", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "TotalQty", "입고수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnn", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "ShortAcceptDate", "단기조치 접수일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "LongAcceptDate", "장기조치 접수일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "MESTFlag", "MES전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "CompleteFlag", "작성완료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridAbnormal, 0, "FinalCompleteFlag", "최종완료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // FontSize
                this.uGridAbnormal.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridAbnormal.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                // DropDown 설정
                // 검사결과
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtComCode = clsComCode.mfReadCommonCode("C0034", m_resSys.GetString("SYS_LANG"));
                // 검사결과
                wGrid.mfSetGridColumnValueList(this.uGridAbnormal, 0, "AbnormalType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);

                #endregion

                this.uGridAbnormal.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridAbnormal_AfterCellUpdate);
                this.uGridAbnormal.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridAbnormal_CellChange);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
         #endregion

        #region IToolbar 멤버
        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridAbnormal.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridAbnormal);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (!(this.uGridAbnormal.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000334", Infragistics.Win.HAlign.Center);

                    return;
                }
                //else if (true)
                //{

                //}
                else
                {
                    if (this.uGridAbnormal.Rows.Count > 0)
                    {
                        this.uGridAbnormal.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    }

                    //HoldLotNo검사수
                    int intcnt = 0;

                    // HOLDLotNo검사 테이블 저장용 데이터 테이블 설정
                    for (int i = 0; i < this.uGridAbnormal.Rows.Count; i++)
                    {
                        if (this.uGridAbnormal.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["OK"].Value) || Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["NG"].Value))
                            {
                                intcnt++;
                            }
                        }
                    }

                    if (!(intcnt > 0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001055", Infragistics.Win.HAlign.Center);

                        return;
                    }

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.No)
                        return;

                    #region BL 호출
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                    QRPINS.BL.INSIMP.MatInspectReqLot clsReqLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                    brwChannel.mfCredentials(clsReqLot);

                    DataTable dtSave = clsReqLot.mfSetDataInfo_Abnormal();
                    DataRow drRow;

                    // BL 연결
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                    QRPINS.BL.INSIMP.MatInspectReqH clsReqH = new QRPINS.BL.INSIMP.MatInspectReqH();
                    brwChannel.mfCredentials(clsReqH);

                    // BL 연결
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                    QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                    brwChannel.mfCredentials(clsHeader);


                    ////MES서버경로 가져오기
                    //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    //QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    //brwChannel.mfCredentials(clsSysAcce);

                    ////MES연결정보 조회 매서드 실행
                    ////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04"); //Live
                    //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), "S07");    //Test

                    //// 공정이상 MES
                    //QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);

                    #endregion

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    TransErrRtn ErrRtn = new TransErrRtn();
                    intcnt = 0; // 변수초기화
                    string strErrorMessage = "";
                    // 전송 데이터 설정
                    for (int i = 0; i < this.uGridAbnormal.Rows.Count; i++)
                    {
                        if (this.uGridAbnormal.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["OK"].Value) || Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["NG"].Value))
                            {
                                DataTable dtMes = new DataTable();
                                string strErrRtn = "";

                                #region QRPSaveInfo

                                drRow = dtSave.NewRow();
                                drRow["PlantCode"] = this.uGridAbnormal.Rows[i].Cells["PlantCode"].Value.ToString();
                                drRow["ReqNo"] = this.uGridAbnormal.Rows[i].Cells["ReqNo"].Value.ToString();
                                drRow["ReqSeq"] = this.uGridAbnormal.Rows[i].Cells["ReqSeq"].Value.ToString();
                                drRow["ReqLotSeq"] = this.uGridAbnormal.Rows[i].Cells["ReqLotSeq"].Value;
                                if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["OK"].Value).Equals(true))
                                    drRow["AbnormalResultFlag"] = "RELEASE";
                                else if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["NG"].Value).Equals(true))
                                    drRow["AbnormalResultFlag"] = "NG";
                                dtSave.Rows.Add(drRow);

                                #endregion

                                #region WMS, MES

                                if (this.uGridAbnormal.Rows[i].Cells["AbnormalType"].Value.ToString().Equals("1"))
                                {
                                    // WMS 전송용 데이터 테이블 설정
                                    DataTable dtWMS = clsReqH.mfSetWMSDataTable();

                                    drRow = dtWMS.NewRow();
                                    drRow["PlantCode"] = this.uGridAbnormal.Rows[i].Cells["PlantCode"].Value.ToString();
                                    drRow["GRNo"] = this.uGridAbnormal.Rows[i].Cells["GRNo"].Value.ToString();
                                    drRow["LotNo"] = this.uGridAbnormal.Rows[i].Cells["LotNo"].Value.ToString();
                                    if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["OK"].Value).Equals(true))
                                        drRow["InspectResultFlag"] = "T";
                                    else if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["NG"].Value).Equals(true))
                                        drRow["InspectResultFlag"] = "F";
                                    drRow["InspectDesc"] = "";
                                    //drRow["ValidationDate"] = "";
                                    drRow["IFFlag"] = "P";
                                    dtWMS.Rows.Add(drRow);

                                    // WMS I/F 메소드 호출
                                    strErrRtn = clsReqH.mfSaveWMSMATInspectReq(dtWMS, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                    #region WMS 결과 검사및 QRP정보저장

                                    //WMS 결과 검사
                                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                    if (ErrRtn.ErrMessage.Equals("00") && ErrRtn.ErrNum.Equals(0))
                                    {
                                        // 수입검사 테이블 결과 저장메소드 호출
                                        strErrRtn = clsReqLot.mfSaveINSMatInspectReqLot_Abnormal_Release(dtSave, m_resSys.GetString("SYS_USERID")
                                                                                                        , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                        //QRPSave 결과검사
                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                        if (!ErrRtn.ErrNum.Equals(0))
                                        {
                                            intcnt = 3;
                                            strErrorMessage = ErrRtn.ErrMessage;
                                            break;
                                        }

                                    }
                                    else
                                    {
                                        intcnt = 1;
                                        strErrorMessage = ErrRtn.ErrMessage;
                                        break;
                                    }

                                    #endregion

                                }
                                else if (this.uGridAbnormal.Rows[i].Cells["AbnormalType"].Value.ToString().Equals("2"))
                                {
                                    DataTable dtProcWMS = clsHeader.mfSetWMSDataTable();
                                    DataTable dtProcInspectLotNo = new DataTable();

                                    //컬럼설정
                                    dtProcInspectLotNo.Columns.Add("CONSUMABLEID", typeof(string));
                                    dtProcInspectLotNo.Columns.Add("HOLDCODE", typeof(string));
                                    dtProcInspectLotNo.Columns.Add("ACTIONTYPE", typeof(string));

                                    //WMS전송데이터
                                    drRow = dtProcWMS.NewRow();
                                    drRow["PlantCode"] = this.uGridAbnormal.Rows[i].Cells["PlantCode"].Value.ToString();
                                    drRow["GRNo"] = this.uGridAbnormal.Rows[i].Cells["GRNo"].Value.ToString();
                                    drRow["LotNo"] = this.uGridAbnormal.Rows[i].Cells["LotNo"].Value.ToString();
                                    if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["OK"].Value).Equals(true))
                                        drRow["InspectResultFlag"] = "T";
                                    else if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["NG"].Value).Equals(true))
                                        drRow["InspectResultFlag"] = "F";
                                    drRow["IFFlag"] = "P";
                                    drRow["AdmitDesc"] = "";
                                    dtProcWMS.Rows.Add(drRow);

                                    // WMS 데이터 전송 메소드 호출
                                    strErrRtn = clsHeader.mfSaveWMSMATAbnormal(dtProcWMS, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                    //WMS 결과 검사
                                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                    #region WMS 결과 검사및 ,MES/IF,QRP정보저장

                                    if (ErrRtn.ErrMessage.Equals("00") && ErrRtn.ErrNum.Equals(0))
                                    {

                                        //MESLotNoList
                                        drRow = dtProcInspectLotNo.NewRow();
                                        drRow["CONSUMABLEID"] = this.uGridAbnormal.Rows[i].Cells["LotNo"].Value.ToString();
                                        drRow["HOLDCODE"] = "HQ04";
                                        if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["OK"].Value).Equals(true))
                                            drRow["ACTIONTYPE"] = "RELEASE";
                                        else if (Convert.ToBoolean(this.uGridAbnormal.Rows[i].Cells["NG"].Value).Equals(true))
                                            drRow["ACTIONTYPE"] = "SCRAP";
                                        dtProcInspectLotNo.Rows.Add(drRow);


                                        dtMes = clsReqLot.mfSaveINSMatInspectReqLot_MESRELEASE(m_resSys.GetString("SYS_PLANTCODE"),
                                                                                                dtProcInspectLotNo,
                                                                                                m_resSys.GetString("SYS_USERID"),
                                                                                                "",
                                                                                                m_resSys.GetString("SYS_USERIP")
                                                                                                , this.Name);

                                        //MES 결과검사
                                        if (dtMes.Rows.Count > 0)
                                        {
                                            if (dtMes.Rows[0]["returncode"].ToString().Equals("0"))
                                            {
                                                // 수입검사 테이블 결과 저장메소드 호출
                                                strErrRtn = clsReqLot.mfSaveINSMatInspectReqLot_Abnormal_Release(dtSave, m_resSys.GetString("SYS_USERID")
                                                                                                                , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                                //QRPSave 결과검사
                                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                                if (!ErrRtn.ErrNum.Equals(0))
                                                {
                                                    intcnt = 3;
                                                    strErrorMessage = ErrRtn.ErrMessage;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                intcnt = 2;
                                                strErrorMessage = dtMes.Rows[0][1].ToString();
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            intcnt = 2;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        intcnt = 1;
                                        strErrorMessage = ErrRtn.ErrMessage;
                                        break;
                                    }

                                    #endregion


                                }

                                #endregion

                            }
                        }
                    }

                    // 팦업창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (intcnt.Equals(0))
                    {


                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001022", "M001023", "M000954", Infragistics.Win.HAlign.Center);

                        mfSearch();
                    }
                    else
                    {
                        #region ErrorMessage

                        string strLang = m_resSys.GetString("SYS_LANG");

                        if (intcnt.Equals(1) && strErrorMessage.Trim().Equals(string.Empty))
                            strErrorMessage = msg.GetMessge_Text("M001394", strLang);
                        if (intcnt.Equals(2))
                            strErrorMessage = msg.GetMessge_Text("M001395", strLang) + strErrorMessage;
                        if (intcnt.Equals(3) && strErrorMessage.Trim().Equals(string.Empty))
                            strErrorMessage = msg.GetMessge_Text("M001396", strLang);
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001022", strLang), msg.GetMessge_Text("M001023", strLang)
                                , strErrorMessage, Infragistics.Win.HAlign.Center);
                        #endregion
                    } 
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");
                if (this.uDateSearchFromGRDate.Value == null || this.uDateSearchFromGRDate.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000217", strLang), msg.GetMessge_Text("M000837", strLang)
                            + msg.GetMessge_Text("M000866", strLang)
                        , Infragistics.Win.HAlign.Right);
                    
                    this.uDateSearchFromGRDate.DropDown();
                    return;
                }
                else if (this.uDateSearchToGRDate.Value == null || this.uDateSearchToGRDate.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000217", strLang), msg.GetMessge_Text("M000837", strLang)
                            + msg.GetMessge_Text("M000866", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uDateSearchToGRDate.DropDown();
                    return;
                }
                else if (Convert.ToDateTime(this.uDateSearchFromGRDate.Value).CompareTo(Convert.ToDateTime(this.uDateSearchToGRDate.Value)) > 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000217", strLang), msg.GetMessge_Text("M000868", strLang)
                            + msg.GetMessge_Text("M000866", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromGRDate.DropDown();
                    return;
                }
                else
                {

                    // 변수설정

                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strMaterialCode = this.uTextSearchMaterialCode.Text;
                    string strLotNo = this.uTextSearchLotNo.Text;
                    string strFromGRDate = Convert.ToDateTime(this.uDateSearchFromGRDate.Value).ToString("yyyy-MM-dd");
                    string strToGRDate = Convert.ToDateTime(this.uDateSearchToGRDate.Value).ToString("yyyy-MM-dd");
                    string strHistoryCheck = this.uCheckSearchHistory.Checked.ToString().ToUpper().Substring(0, 1);

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                    QRPINS.BL.INSIMP.MatInspectReqLot clsReqLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                    brwChannel.mfCredentials(clsReqLot);

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    DataTable dtSearch = clsReqLot.mfReadINSMatInspectReqLot_AbnormalHold(strPlantCode, strMaterialCode, strLotNo
                                                                                    , strFromGRDate, strToGRDate, strHistoryCheck, m_resSys.GetString("SYS_LANG"));

                    this.uGridAbnormal.DataSource = dtSearch;
                    this.uGridAbnormal.DataBind();

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 조회결과 없을시 MessageBox 로 알림
                    if (dtSearch.Rows.Count == 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        WinGrid grd = new WinGrid();
                        grd.mfSetAutoResizeColWidth(this.uGridAbnormal, 0);

                        if (this.uCheckSearchHistory.Checked)
                        {
                            this.uGridAbnormal.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            this.uGridAbnormal.DisplayLayout.Bands[0].Columns["OK"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                            this.uGridAbnormal.DisplayLayout.Bands[0].Columns["NG"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                            QRPBrowser toolButton = new QRPBrowser();
                            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
                        }
                        else
                        {
                            this.uGridAbnormal.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGridAbnormal.DisplayLayout.Bands[0].Columns["OK"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.WhenUsingCheckEditor;
                            this.uGridAbnormal.DisplayLayout.Bands[0].Columns["NG"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.WhenUsingCheckEditor;

                            QRPBrowser toolButton = new QRPBrowser();
                            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region 일반메소드

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }
        #endregion

        #region 검색조건 자재텍스트박스 이벤트

        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.SelectedText == this.uTextSearchMaterialCode.Text)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                try
                {
                    if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }

                    frmPOP0001 frmPOP = new frmPOP0001();
                    frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                    frmPOP.ShowDialog();

                    this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                    this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                    this.uComboSearchPlant.Value = frmPOP.PlantCode;
                }
                catch (Exception ex)
                {
                    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                    frmErr.ShowDialog();
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uGridAbnormal_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("OK"))
                {
                    if (Convert.ToBoolean(e.Cell.Value).Equals(true))
                        e.Cell.Row.Cells["NG"].Value = false;
                }
                else if (e.Cell.Column.Key.Equals("NG"))
                {
                    if (Convert.ToBoolean(e.Cell.Value).Equals(true))
                        e.Cell.Row.Cells["OK"].Value = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridAbnormal_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                if (e.Cell.Column.Key.Equals("OK") || e.Cell.Column.Key.Equals("NG"))
                {
                    this.uGridAbnormal.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    //this.uGridAbnormal.UpdateData();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}