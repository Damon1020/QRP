﻿namespace QRPINS.UI
{
    partial class frmINSZ0010
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0010));
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton3 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSerachAriseDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchAriseDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchAriseDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchQCNNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchQCNNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcessCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridQCNList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupQCNUser = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridQCNUser = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWorkUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkUser = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckActionFlag9 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag8 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag7 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag6 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag5 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag4 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag3 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridAffectLotList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBoxStdInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextFaultImg = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFaultImg = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridExpectEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLotHoldState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLotProcessState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboAriseEquipCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboExpectProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uComboAriseProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPublishComplete = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerProductSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckPublishFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelPublishFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMESWorkUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateAriseTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uNumFaultQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumInspectQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uCheckWorkStepEquipFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckWorkStepMaterialFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelWorkStep = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInspectFaultTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInspectFaultType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectQty = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelQty = new Infragistics.Win.Misc.UltraLabel();
            this.uDateAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAriseDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelExpectProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAriseEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAriseProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextQCNNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelQCNNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSerachAriseDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchAriseDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchQCNNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupQCNUser)).BeginInit();
            this.uGroupQCNUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAffectLotList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).BeginInit();
            this.uGroupBoxStdInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExpectEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotHoldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboExpectProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPublishComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckPublishFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESWorkUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumFaultQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInspectQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepEquipFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepMaterialFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFaultTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQCNNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance21;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSerachAriseDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchAriseDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchQCNNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchQCNNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProduct);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcessCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlantCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 4;
            this.uGroupBoxSearchArea.Visible = false;
            // 
            // uDateSerachAriseDateTo
            // 
            this.uDateSerachAriseDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSerachAriseDateTo.Location = new System.Drawing.Point(808, 36);
            this.uDateSerachAriseDateTo.Name = "uDateSerachAriseDateTo";
            this.uDateSerachAriseDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSerachAriseDateTo.TabIndex = 55;
            // 
            // ultraLabel2
            // 
            appearance67.TextHAlignAsString = "Center";
            appearance67.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance67;
            this.ultraLabel2.Location = new System.Drawing.Point(792, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel2.TabIndex = 54;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchAriseDateFrom
            // 
            this.uDateSearchAriseDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchAriseDateFrom.Location = new System.Drawing.Point(692, 36);
            this.uDateSearchAriseDateFrom.Name = "uDateSearchAriseDateFrom";
            this.uDateSearchAriseDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchAriseDateFrom.TabIndex = 53;
            // 
            // uLabelSearchAriseDate
            // 
            this.uLabelSearchAriseDate.Location = new System.Drawing.Point(588, 36);
            this.uLabelSearchAriseDate.Name = "uLabelSearchAriseDate";
            this.uLabelSearchAriseDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchAriseDate.TabIndex = 52;
            this.uLabelSearchAriseDate.Text = "ultraLabel1";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(380, 36);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchLotNo.TabIndex = 51;
            // 
            // uTextSearchQCNNo
            // 
            this.uTextSearchQCNNo.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchQCNNo.Name = "uTextSearchQCNNo";
            this.uTextSearchQCNNo.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchQCNNo.TabIndex = 50;
            // 
            // uLabelSearchQCNNo
            // 
            this.uLabelSearchQCNNo.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchQCNNo.Name = "uLabelSearchQCNNo";
            this.uLabelSearchQCNNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchQCNNo.TabIndex = 49;
            this.uLabelSearchQCNNo.Text = "ultraLabel1";
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(276, 36);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 48;
            this.uLabelSearchLotNo.Text = "ultraLabel1";
            // 
            // uTextSearchProductName
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Appearance = appearance41;
            this.uTextSearchProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Location = new System.Drawing.Point(794, 12);
            this.uTextSearchProductName.Name = "uTextSearchProductName";
            this.uTextSearchProductName.ReadOnly = true;
            this.uTextSearchProductName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchProductName.TabIndex = 47;
            // 
            // uTextSearchProductCode
            // 
            appearance27.Image = ((object)(resources.GetObject("appearance27.Image")));
            appearance27.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance27;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchProductCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchProductCode.Location = new System.Drawing.Point(692, 12);
            this.uTextSearchProductCode.Name = "uTextSearchProductCode";
            this.uTextSearchProductCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchProductCode.TabIndex = 46;
            this.uTextSearchProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchProductCode_EditorButtonClick);
            // 
            // uLabelSearchProduct
            // 
            this.uLabelSearchProduct.Location = new System.Drawing.Point(588, 12);
            this.uLabelSearchProduct.Name = "uLabelSearchProduct";
            this.uLabelSearchProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProduct.TabIndex = 5;
            this.uLabelSearchProduct.Text = "ultraLabel1";
            // 
            // uComboSearchProcessCode
            // 
            this.uComboSearchProcessCode.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchProcessCode.Name = "uComboSearchProcessCode";
            this.uComboSearchProcessCode.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchProcessCode.TabIndex = 4;
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcess.TabIndex = 3;
            this.uLabelSearchProcess.Text = "ultraLabel1";
            // 
            // uComboSearchPlantCode
            // 
            this.uComboSearchPlantCode.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlantCode.Name = "uComboSearchPlantCode";
            this.uComboSearchPlantCode.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlantCode.TabIndex = 2;
            this.uComboSearchPlantCode.ValueChanged += new System.EventHandler(this.uComboSearchPlantCode_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 1;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridQCNList
            // 
            this.uGridQCNList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQCNList.DisplayLayout.Appearance = appearance29;
            this.uGridQCNList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQCNList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.uGridQCNList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance32.BackColor2 = System.Drawing.SystemColors.Control;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNList.DisplayLayout.GroupByBox.PromptAppearance = appearance32;
            this.uGridQCNList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQCNList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQCNList.DisplayLayout.Override.ActiveCellAppearance = appearance33;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQCNList.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.uGridQCNList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQCNList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.Override.CardAreaAppearance = appearance35;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            appearance36.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQCNList.DisplayLayout.Override.CellAppearance = appearance36;
            this.uGridQCNList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQCNList.DisplayLayout.Override.CellPadding = 0;
            appearance37.BackColor = System.Drawing.SystemColors.Control;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.Override.GroupByRowAppearance = appearance37;
            appearance38.TextHAlignAsString = "Left";
            this.uGridQCNList.DisplayLayout.Override.HeaderAppearance = appearance38;
            this.uGridQCNList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQCNList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            this.uGridQCNList.DisplayLayout.Override.RowAppearance = appearance39;
            this.uGridQCNList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQCNList.DisplayLayout.Override.TemplateAddRowAppearance = appearance40;
            this.uGridQCNList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQCNList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQCNList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQCNList.Location = new System.Drawing.Point(0, 40);
            this.uGridQCNList.Name = "uGridQCNList";
            this.uGridQCNList.Size = new System.Drawing.Size(1060, 780);
            this.uGridQCNList.TabIndex = 5;
            this.uGridQCNList.Text = "ultraGrid1";
            this.uGridQCNList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridQCNList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 735);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 110);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 735);
            this.uGroupBoxContentsArea.TabIndex = 6;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupQCNUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxStdInfo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 715);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupQCNUser
            // 
            this.uGroupQCNUser.Controls.Add(this.uGridQCNUser);
            this.uGroupQCNUser.Location = new System.Drawing.Point(532, 444);
            this.uGroupQCNUser.Name = "uGroupQCNUser";
            this.uGroupQCNUser.Size = new System.Drawing.Size(512, 220);
            this.uGroupQCNUser.TabIndex = 7;
            this.uGroupQCNUser.Text = "ultraGroupBox1";
            // 
            // uGridQCNUser
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQCNUser.DisplayLayout.Appearance = appearance4;
            this.uGridQCNUser.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQCNUser.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNUser.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNUser.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridQCNUser.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNUser.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridQCNUser.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQCNUser.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQCNUser.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQCNUser.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridQCNUser.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQCNUser.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQCNUser.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQCNUser.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGridQCNUser.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQCNUser.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNUser.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGridQCNUser.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridQCNUser.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQCNUser.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridQCNUser.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridQCNUser.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQCNUser.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGridQCNUser.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQCNUser.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQCNUser.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQCNUser.Location = new System.Drawing.Point(4, 28);
            this.uGridQCNUser.Name = "uGridQCNUser";
            this.uGridQCNUser.Size = new System.Drawing.Size(504, 188);
            this.uGridQCNUser.TabIndex = 36;
            this.uGridQCNUser.Text = "ultraGrid1";
            this.uGridQCNUser.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridQCNUser_AfterCellUpdate);
            this.uGridQCNUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridQCNUser_KeyDown);
            this.uGridQCNUser.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridQCNUser_ClickCellButton);
            this.uGridQCNUser.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridQCNUser_CellChange);
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox3.Controls.Add(this.uLabelEtc);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserName);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserID);
            this.uGroupBox3.Controls.Add(this.uLabelInspectUser);
            this.uGroupBox3.Controls.Add(this.uTextWorkUserName);
            this.uGroupBox3.Controls.Add(this.uTextWorkUserID);
            this.uGroupBox3.Controls.Add(this.uLabelWorkUser);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag9);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag8);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag7);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag6);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag5);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag4);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag3);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag2);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag1);
            this.uGroupBox3.Location = new System.Drawing.Point(532, 216);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(512, 228);
            this.uGroupBox3.TabIndex = 6;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 204);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(388, 21);
            this.uTextEtcDesc.TabIndex = 50;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 204);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc.TabIndex = 49;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uTextInspectUserName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance17;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(218, 180);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserName.TabIndex = 48;
            // 
            // uTextInspectUserID
            // 
            appearance23.Image = ((object)(resources.GetObject("appearance23.Image")));
            appearance23.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance23;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton2);
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(116, 180);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserID.TabIndex = 47;
            this.uTextInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextInspectUserID_KeyDown);
            this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectUserID_EditorButtonClick);
            // 
            // uLabelInspectUser
            // 
            this.uLabelInspectUser.Location = new System.Drawing.Point(12, 180);
            this.uLabelInspectUser.Name = "uLabelInspectUser";
            this.uLabelInspectUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectUser.TabIndex = 46;
            this.uLabelInspectUser.Text = "ultraLabel1";
            // 
            // uTextWorkUserName
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Appearance = appearance24;
            this.uTextWorkUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Location = new System.Drawing.Point(218, 156);
            this.uTextWorkUserName.Name = "uTextWorkUserName";
            this.uTextWorkUserName.ReadOnly = true;
            this.uTextWorkUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextWorkUserName.TabIndex = 45;
            // 
            // uTextWorkUserID
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserID.Appearance = appearance55;
            this.uTextWorkUserID.BackColor = System.Drawing.Color.Gainsboro;
            appearance25.Image = ((object)(resources.GetObject("appearance25.Image")));
            appearance25.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance25;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Visible = false;
            this.uTextWorkUserID.ButtonsRight.Add(editorButton3);
            this.uTextWorkUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkUserID.Location = new System.Drawing.Point(116, 156);
            this.uTextWorkUserID.Name = "uTextWorkUserID";
            this.uTextWorkUserID.ReadOnly = true;
            this.uTextWorkUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextWorkUserID.TabIndex = 44;
            this.uTextWorkUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWorkUserID_KeyDown);
            this.uTextWorkUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWorkUserID_EditorButtonClick);
            // 
            // uLabelWorkUser
            // 
            this.uLabelWorkUser.Location = new System.Drawing.Point(12, 156);
            this.uLabelWorkUser.Name = "uLabelWorkUser";
            this.uLabelWorkUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkUser.TabIndex = 43;
            this.uLabelWorkUser.Text = "ultraLabel1";
            // 
            // uCheckActionFlag9
            // 
            this.uCheckActionFlag9.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag9.Location = new System.Drawing.Point(12, 124);
            this.uCheckActionFlag9.Name = "uCheckActionFlag9";
            this.uCheckActionFlag9.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag9.TabIndex = 42;
            this.uCheckActionFlag9.Text = "전수 선별 QC 의뢰";
            this.uCheckActionFlag9.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag8
            // 
            this.uCheckActionFlag8.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag8.Location = new System.Drawing.Point(216, 100);
            this.uCheckActionFlag8.Name = "uCheckActionFlag8";
            this.uCheckActionFlag8.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag8.TabIndex = 41;
            this.uCheckActionFlag8.Text = "작업 중지";
            this.uCheckActionFlag8.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag7
            // 
            this.uCheckActionFlag7.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag7.Location = new System.Drawing.Point(12, 100);
            this.uCheckActionFlag7.Name = "uCheckActionFlag7";
            this.uCheckActionFlag7.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag7.TabIndex = 40;
            this.uCheckActionFlag7.Text = "기술부서 검토 의뢰";
            this.uCheckActionFlag7.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag6
            // 
            this.uCheckActionFlag6.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag6.Location = new System.Drawing.Point(216, 76);
            this.uCheckActionFlag6.Name = "uCheckActionFlag6";
            this.uCheckActionFlag6.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag6.TabIndex = 39;
            this.uCheckActionFlag6.Text = "표준 발행 유무 점검";
            this.uCheckActionFlag6.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag5
            // 
            this.uCheckActionFlag5.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag5.Location = new System.Drawing.Point(12, 76);
            this.uCheckActionFlag5.Name = "uCheckActionFlag5";
            this.uCheckActionFlag5.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag5.TabIndex = 38;
            this.uCheckActionFlag5.Text = "장비 상태 및 가동 조건 점검";
            this.uCheckActionFlag5.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag4
            // 
            this.uCheckActionFlag4.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag4.Location = new System.Drawing.Point(216, 52);
            this.uCheckActionFlag4.Name = "uCheckActionFlag4";
            this.uCheckActionFlag4.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag4.TabIndex = 37;
            this.uCheckActionFlag4.Text = "교육 실시";
            this.uCheckActionFlag4.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag3
            // 
            this.uCheckActionFlag3.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag3.Location = new System.Drawing.Point(12, 52);
            this.uCheckActionFlag3.Name = "uCheckActionFlag3";
            this.uCheckActionFlag3.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag3.TabIndex = 36;
            this.uCheckActionFlag3.Text = "원/부자재 상태 점검";
            this.uCheckActionFlag3.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag2
            // 
            this.uCheckActionFlag2.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag2.Location = new System.Drawing.Point(216, 28);
            this.uCheckActionFlag2.Name = "uCheckActionFlag2";
            this.uCheckActionFlag2.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag2.TabIndex = 35;
            this.uCheckActionFlag2.Text = "부적합 처리 절차에 의해 처리";
            this.uCheckActionFlag2.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag1
            // 
            this.uCheckActionFlag1.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag1.Location = new System.Drawing.Point(12, 28);
            this.uCheckActionFlag1.Name = "uCheckActionFlag1";
            this.uCheckActionFlag1.Size = new System.Drawing.Size(200, 20);
            this.uCheckActionFlag1.TabIndex = 34;
            this.uCheckActionFlag1.Text = "작업 방법 점검";
            this.uCheckActionFlag1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uGridAffectLotList);
            this.uGroupBox2.Controls.Add(this.uButtonDelete);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 216);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(512, 448);
            this.uGroupBox2.TabIndex = 5;
            // 
            // uGridAffectLotList
            // 
            this.uGridAffectLotList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAffectLotList.DisplayLayout.Appearance = appearance74;
            this.uGridAffectLotList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAffectLotList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance75.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance75.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance75.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.Appearance = appearance75;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance76;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance77.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance77.BackColor2 = System.Drawing.SystemColors.Control;
            appearance77.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance77.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.PromptAppearance = appearance77;
            this.uGridAffectLotList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAffectLotList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAffectLotList.DisplayLayout.Override.ActiveCellAppearance = appearance78;
            appearance79.BackColor = System.Drawing.SystemColors.Highlight;
            appearance79.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAffectLotList.DisplayLayout.Override.ActiveRowAppearance = appearance79;
            this.uGridAffectLotList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAffectLotList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAffectLotList.DisplayLayout.Override.CardAreaAppearance = appearance80;
            appearance81.BorderColor = System.Drawing.Color.Silver;
            appearance81.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAffectLotList.DisplayLayout.Override.CellAppearance = appearance81;
            this.uGridAffectLotList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAffectLotList.DisplayLayout.Override.CellPadding = 0;
            appearance82.BackColor = System.Drawing.SystemColors.Control;
            appearance82.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance82.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance82.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAffectLotList.DisplayLayout.Override.GroupByRowAppearance = appearance82;
            appearance83.TextHAlignAsString = "Left";
            this.uGridAffectLotList.DisplayLayout.Override.HeaderAppearance = appearance83;
            this.uGridAffectLotList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAffectLotList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance84.BackColor = System.Drawing.SystemColors.Window;
            appearance84.BorderColor = System.Drawing.Color.Silver;
            this.uGridAffectLotList.DisplayLayout.Override.RowAppearance = appearance84;
            this.uGridAffectLotList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance85.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAffectLotList.DisplayLayout.Override.TemplateAddRowAppearance = appearance85;
            this.uGridAffectLotList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAffectLotList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAffectLotList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAffectLotList.Location = new System.Drawing.Point(12, 56);
            this.uGridAffectLotList.Name = "uGridAffectLotList";
            this.uGridAffectLotList.Size = new System.Drawing.Size(488, 384);
            this.uGridAffectLotList.TabIndex = 34;
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 28);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 33;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uGroupBoxStdInfo
            // 
            this.uGroupBoxStdInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxStdInfo.Controls.Add(this.uTextFaultImg);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelFaultImg);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uGridExpectEquipList);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextProductName);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextReqSeq);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextReqNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextLotHoldState);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextLotProcessState);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboAriseEquipCode);
            this.uGroupBoxStdInfo.Controls.Add(this.ultraLabel1);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboExpectProcessCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboAriseProcessCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextPackage);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPackage);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextPublishComplete);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextMESTFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerProductSpec);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelCustomerProductSpec);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckPublishFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPublishFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextMESWorkUserID);
            this.uGroupBoxStdInfo.Controls.Add(this.uDateAriseTime);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumFaultQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumInspectQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckWorkStepEquipFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckWorkStepMaterialFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelWorkStep);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboInspectFaultTypeCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelInspectFaultType);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelInspectQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uDateAriseDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelAriseDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelExpectProcess);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelAriseEquip);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelAriseProcess);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextProductCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelProduct);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerName);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelCustomerName);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextLotNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelLotNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextQCNNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelQCNNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboPlantCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxStdInfo.Location = new System.Drawing.Point(12, 12);
            this.uGroupBoxStdInfo.Name = "uGroupBoxStdInfo";
            this.uGroupBoxStdInfo.Size = new System.Drawing.Size(1032, 196);
            this.uGroupBoxStdInfo.TabIndex = 4;
            // 
            // uTextFaultImg
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFaultImg.Appearance = appearance16;
            this.uTextFaultImg.BackColor = System.Drawing.Color.Gainsboro;
            appearance88.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance88.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance88;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Key = "UP";
            appearance89.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance89.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance89;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "DOWN";
            this.uTextFaultImg.ButtonsRight.Add(editorButton4);
            this.uTextFaultImg.ButtonsRight.Add(editorButton5);
            this.uTextFaultImg.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextFaultImg.Location = new System.Drawing.Point(116, 108);
            this.uTextFaultImg.Name = "uTextFaultImg";
            this.uTextFaultImg.ReadOnly = true;
            this.uTextFaultImg.Size = new System.Drawing.Size(150, 21);
            this.uTextFaultImg.TabIndex = 68;
            this.uTextFaultImg.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextFaultImg_KeyDown);
            this.uTextFaultImg.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFaultImg_EditorButtonClick);
            // 
            // uLabelFaultImg
            // 
            this.uLabelFaultImg.Location = new System.Drawing.Point(12, 108);
            this.uLabelFaultImg.Name = "uLabelFaultImg";
            this.uLabelFaultImg.Size = new System.Drawing.Size(100, 20);
            this.uLabelFaultImg.TabIndex = 69;
            this.uLabelFaultImg.Text = "ultraLabel1";
            // 
            // uTextCustomerCode
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance57;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(244, 36);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(21, 21);
            this.uTextCustomerCode.TabIndex = 67;
            this.uTextCustomerCode.Visible = false;
            // 
            // uGridExpectEquipList
            // 
            appearance60.BackColor = System.Drawing.SystemColors.Window;
            appearance60.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridExpectEquipList.DisplayLayout.Appearance = appearance60;
            this.uGridExpectEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridExpectEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance61.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance61.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance61.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.Appearance = appearance61;
            appearance62.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance62;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance63.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance63.BackColor2 = System.Drawing.SystemColors.Control;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance63;
            this.uGridExpectEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridExpectEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            appearance64.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridExpectEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance64;
            appearance65.BackColor = System.Drawing.SystemColors.Highlight;
            appearance65.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridExpectEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance65;
            this.uGridExpectEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridExpectEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance66.BackColor = System.Drawing.SystemColors.Window;
            this.uGridExpectEquipList.DisplayLayout.Override.CardAreaAppearance = appearance66;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridExpectEquipList.DisplayLayout.Override.CellAppearance = appearance68;
            this.uGridExpectEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridExpectEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridExpectEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.uGridExpectEquipList.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.uGridExpectEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridExpectEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.uGridExpectEquipList.DisplayLayout.Override.RowAppearance = appearance71;
            this.uGridExpectEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridExpectEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.uGridExpectEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridExpectEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridExpectEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridExpectEquipList.Location = new System.Drawing.Point(536, 84);
            this.uGridExpectEquipList.Name = "uGridExpectEquipList";
            this.uGridExpectEquipList.Size = new System.Drawing.Size(480, 104);
            this.uGridExpectEquipList.TabIndex = 66;
            this.uGridExpectEquipList.Text = "ultraGrid1";
            this.uGridExpectEquipList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridExpectEquipList_DoubleClickRow);
            // 
            // uTextProductName
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance20;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(768, 12);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(26, 21);
            this.uTextProductName.TabIndex = 10;
            this.uTextProductName.Visible = false;
            // 
            // uTextReqSeq
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Appearance = appearance26;
            this.uTextReqSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Location = new System.Drawing.Point(956, 32);
            this.uTextReqSeq.Name = "uTextReqSeq";
            this.uTextReqSeq.ReadOnly = true;
            this.uTextReqSeq.Size = new System.Drawing.Size(21, 21);
            this.uTextReqSeq.TabIndex = 65;
            this.uTextReqSeq.Visible = false;
            // 
            // uTextReqNo
            // 
            appearance86.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Appearance = appearance86;
            this.uTextReqNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Location = new System.Drawing.Point(932, 32);
            this.uTextReqNo.Name = "uTextReqNo";
            this.uTextReqNo.ReadOnly = true;
            this.uTextReqNo.Size = new System.Drawing.Size(21, 21);
            this.uTextReqNo.TabIndex = 64;
            this.uTextReqNo.Visible = false;
            // 
            // uTextLotHoldState
            // 
            appearance58.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotHoldState.Appearance = appearance58;
            this.uTextLotHoldState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotHoldState.Location = new System.Drawing.Point(1004, 32);
            this.uTextLotHoldState.Name = "uTextLotHoldState";
            this.uTextLotHoldState.ReadOnly = true;
            this.uTextLotHoldState.Size = new System.Drawing.Size(21, 21);
            this.uTextLotHoldState.TabIndex = 63;
            this.uTextLotHoldState.Visible = false;
            // 
            // uTextLotProcessState
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Appearance = appearance59;
            this.uTextLotProcessState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Location = new System.Drawing.Point(980, 32);
            this.uTextLotProcessState.Name = "uTextLotProcessState";
            this.uTextLotProcessState.ReadOnly = true;
            this.uTextLotProcessState.Size = new System.Drawing.Size(21, 21);
            this.uTextLotProcessState.TabIndex = 62;
            this.uTextLotProcessState.Visible = false;
            // 
            // uComboAriseEquipCode
            // 
            this.uComboAriseEquipCode.CheckedListSettings.CheckStateMember = "";
            appearance104.BackColor = System.Drawing.SystemColors.Window;
            appearance104.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboAriseEquipCode.DisplayLayout.Appearance = appearance104;
            this.uComboAriseEquipCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboAriseEquipCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance105.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance105.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance105.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance105.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.Appearance = appearance105;
            appearance106.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance106;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance107.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance107.BackColor2 = System.Drawing.SystemColors.Control;
            appearance107.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance107.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.PromptAppearance = appearance107;
            this.uComboAriseEquipCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboAriseEquipCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance108.BackColor = System.Drawing.SystemColors.Window;
            appearance108.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboAriseEquipCode.DisplayLayout.Override.ActiveCellAppearance = appearance108;
            appearance109.BackColor = System.Drawing.SystemColors.Highlight;
            appearance109.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboAriseEquipCode.DisplayLayout.Override.ActiveRowAppearance = appearance109;
            this.uComboAriseEquipCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboAriseEquipCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance110.BackColor = System.Drawing.SystemColors.Window;
            this.uComboAriseEquipCode.DisplayLayout.Override.CardAreaAppearance = appearance110;
            appearance111.BorderColor = System.Drawing.Color.Silver;
            appearance111.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboAriseEquipCode.DisplayLayout.Override.CellAppearance = appearance111;
            this.uComboAriseEquipCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboAriseEquipCode.DisplayLayout.Override.CellPadding = 0;
            appearance112.BackColor = System.Drawing.SystemColors.Control;
            appearance112.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance112.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance112.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance112.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseEquipCode.DisplayLayout.Override.GroupByRowAppearance = appearance112;
            appearance113.TextHAlignAsString = "Left";
            this.uComboAriseEquipCode.DisplayLayout.Override.HeaderAppearance = appearance113;
            this.uComboAriseEquipCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboAriseEquipCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance114.BackColor = System.Drawing.SystemColors.Window;
            appearance114.BorderColor = System.Drawing.Color.Silver;
            this.uComboAriseEquipCode.DisplayLayout.Override.RowAppearance = appearance114;
            this.uComboAriseEquipCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance115.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboAriseEquipCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance115;
            this.uComboAriseEquipCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboAriseEquipCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboAriseEquipCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboAriseEquipCode.Location = new System.Drawing.Point(644, 60);
            this.uComboAriseEquipCode.Name = "uComboAriseEquipCode";
            this.uComboAriseEquipCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboAriseEquipCode.Size = new System.Drawing.Size(150, 22);
            this.uComboAriseEquipCode.TabIndex = 59;
            this.uComboAriseEquipCode.Text = "ultraCombo1";
            this.uComboAriseEquipCode.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboAriseEquipCode_RowSelected);
            this.uComboAriseEquipCode.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboAriseEquipCode_BeforeDropDown);
            // 
            // ultraLabel1
            // 
            appearance28.TextHAlignAsString = "Center";
            appearance28.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance28;
            this.ultraLabel1.Location = new System.Drawing.Point(228, 156);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel1.TabIndex = 58;
            this.ultraLabel1.Text = "/";
            // 
            // uComboExpectProcessCode
            // 
            this.uComboExpectProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboExpectProcessCode.DisplayLayout.Appearance = appearance46;
            this.uComboExpectProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboExpectProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.Appearance = appearance43;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance44;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance45.BackColor2 = System.Drawing.SystemColors.Control;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance45;
            this.uComboExpectProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboExpectProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            appearance54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboExpectProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance54;
            appearance49.BackColor = System.Drawing.SystemColors.Highlight;
            appearance49.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboExpectProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance49;
            this.uComboExpectProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboExpectProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            this.uComboExpectProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance48;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboExpectProcessCode.DisplayLayout.Override.CellAppearance = appearance47;
            this.uComboExpectProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboExpectProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance51.BackColor = System.Drawing.SystemColors.Control;
            appearance51.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance51.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance51.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboExpectProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance51;
            appearance53.TextHAlignAsString = "Left";
            this.uComboExpectProcessCode.DisplayLayout.Override.HeaderAppearance = appearance53;
            this.uComboExpectProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboExpectProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            this.uComboExpectProcessCode.DisplayLayout.Override.RowAppearance = appearance52;
            this.uComboExpectProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboExpectProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance50;
            this.uComboExpectProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboExpectProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboExpectProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboExpectProcessCode.Location = new System.Drawing.Point(380, 84);
            this.uComboExpectProcessCode.Name = "uComboExpectProcessCode";
            this.uComboExpectProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboExpectProcessCode.Size = new System.Drawing.Size(150, 22);
            this.uComboExpectProcessCode.TabIndex = 57;
            this.uComboExpectProcessCode.Text = "ultraCombo1";
            this.uComboExpectProcessCode.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboExpectProcessCode_RowSelected);
            this.uComboExpectProcessCode.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboExpectProcessCode_BeforeDropDown);
            // 
            // uComboAriseProcessCode
            // 
            this.uComboAriseProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance92.BackColor = System.Drawing.SystemColors.Window;
            appearance92.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboAriseProcessCode.DisplayLayout.Appearance = appearance92;
            this.uComboAriseProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboAriseProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance93.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.Appearance = appearance93;
            appearance94.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance94;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance95.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance95.BackColor2 = System.Drawing.SystemColors.Control;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance95;
            this.uComboAriseProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboAriseProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance96.BackColor = System.Drawing.SystemColors.Window;
            appearance96.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboAriseProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance96;
            appearance97.BackColor = System.Drawing.SystemColors.Highlight;
            appearance97.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboAriseProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance97;
            this.uComboAriseProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboAriseProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance98.BackColor = System.Drawing.SystemColors.Window;
            this.uComboAriseProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance98;
            appearance99.BorderColor = System.Drawing.Color.Silver;
            appearance99.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboAriseProcessCode.DisplayLayout.Override.CellAppearance = appearance99;
            this.uComboAriseProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboAriseProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance100.BackColor = System.Drawing.SystemColors.Control;
            appearance100.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance100.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance100;
            appearance101.TextHAlignAsString = "Left";
            this.uComboAriseProcessCode.DisplayLayout.Override.HeaderAppearance = appearance101;
            this.uComboAriseProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboAriseProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance102.BackColor = System.Drawing.SystemColors.Window;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            this.uComboAriseProcessCode.DisplayLayout.Override.RowAppearance = appearance102;
            this.uComboAriseProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance103.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboAriseProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance103;
            this.uComboAriseProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboAriseProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboAriseProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboAriseProcessCode.Location = new System.Drawing.Point(380, 60);
            this.uComboAriseProcessCode.Name = "uComboAriseProcessCode";
            this.uComboAriseProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboAriseProcessCode.Size = new System.Drawing.Size(150, 22);
            this.uComboAriseProcessCode.TabIndex = 56;
            this.uComboAriseProcessCode.Text = "ultraCombo1";
            // 
            // uTextPackage
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance18;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(644, 36);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(150, 21);
            this.uTextPackage.TabIndex = 54;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(540, 36);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackage.TabIndex = 55;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uTextPublishComplete
            // 
            this.uTextPublishComplete.Location = new System.Drawing.Point(956, 8);
            this.uTextPublishComplete.Name = "uTextPublishComplete";
            this.uTextPublishComplete.ReadOnly = true;
            this.uTextPublishComplete.Size = new System.Drawing.Size(21, 21);
            this.uTextPublishComplete.TabIndex = 53;
            this.uTextPublishComplete.Visible = false;
            // 
            // uTextMESTFlag
            // 
            this.uTextMESTFlag.Location = new System.Drawing.Point(980, 8);
            this.uTextMESTFlag.Name = "uTextMESTFlag";
            this.uTextMESTFlag.ReadOnly = true;
            this.uTextMESTFlag.Size = new System.Drawing.Size(21, 21);
            this.uTextMESTFlag.TabIndex = 52;
            this.uTextMESTFlag.Visible = false;
            // 
            // uTextCustomerProductSpec
            // 
            appearance87.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Appearance = appearance87;
            this.uTextCustomerProductSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Location = new System.Drawing.Point(380, 36);
            this.uTextCustomerProductSpec.Name = "uTextCustomerProductSpec";
            this.uTextCustomerProductSpec.ReadOnly = true;
            this.uTextCustomerProductSpec.Size = new System.Drawing.Size(150, 21);
            this.uTextCustomerProductSpec.TabIndex = 12;
            // 
            // uLabelCustomerProductSpec
            // 
            this.uLabelCustomerProductSpec.Location = new System.Drawing.Point(276, 36);
            this.uLabelCustomerProductSpec.Name = "uLabelCustomerProductSpec";
            this.uLabelCustomerProductSpec.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomerProductSpec.TabIndex = 51;
            this.uLabelCustomerProductSpec.Text = "ultraLabel1";
            // 
            // uCheckPublishFlag
            // 
            this.uCheckPublishFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckPublishFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckPublishFlag.Location = new System.Drawing.Point(380, 132);
            this.uCheckPublishFlag.Name = "uCheckPublishFlag";
            this.uCheckPublishFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckPublishFlag.TabIndex = 39;
            this.uCheckPublishFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelPublishFlag
            // 
            this.uLabelPublishFlag.Location = new System.Drawing.Point(276, 132);
            this.uLabelPublishFlag.Name = "uLabelPublishFlag";
            this.uLabelPublishFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelPublishFlag.TabIndex = 49;
            this.uLabelPublishFlag.Text = "ultraLabel1";
            // 
            // uTextMESWorkUserID
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESWorkUserID.Appearance = appearance56;
            this.uTextMESWorkUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESWorkUserID.Location = new System.Drawing.Point(1004, 8);
            this.uTextMESWorkUserID.Name = "uTextMESWorkUserID";
            this.uTextMESWorkUserID.ReadOnly = true;
            this.uTextMESWorkUserID.Size = new System.Drawing.Size(21, 21);
            this.uTextMESWorkUserID.TabIndex = 48;
            this.uTextMESWorkUserID.Visible = false;
            // 
            // uDateAriseTime
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseTime.Appearance = appearance3;
            this.uDateAriseTime.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateAriseTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateAriseTime.Location = new System.Drawing.Point(932, 8);
            this.uDateAriseTime.MaskInput = "{LOC}hh:mm:ss";
            this.uDateAriseTime.Name = "uDateAriseTime";
            this.uDateAriseTime.Size = new System.Drawing.Size(20, 21);
            this.uDateAriseTime.TabIndex = 40;
            this.uDateAriseTime.Visible = false;
            // 
            // uNumFaultQty
            // 
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Text = "0";
            this.uNumFaultQty.ButtonsLeft.Add(editorButton6);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumFaultQty.ButtonsRight.Add(spinEditorButton1);
            this.uNumFaultQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumFaultQty.Location = new System.Drawing.Point(116, 156);
            this.uNumFaultQty.MaskInput = "nnnnn";
            this.uNumFaultQty.MinValue = 0;
            this.uNumFaultQty.Name = "uNumFaultQty";
            this.uNumFaultQty.Size = new System.Drawing.Size(110, 21);
            this.uNumFaultQty.TabIndex = 32;
            this.uNumFaultQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumFaultQty_EditorSpinButtonClick);
            this.uNumFaultQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumFaultQty_EditorButtonClick);
            // 
            // uNumInspectQty
            // 
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton7.Text = "0";
            this.uNumInspectQty.ButtonsLeft.Add(editorButton7);
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumInspectQty.ButtonsRight.Add(spinEditorButton2);
            this.uNumInspectQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumInspectQty.Location = new System.Drawing.Point(244, 156);
            this.uNumInspectQty.MaskInput = "nnnnn";
            this.uNumInspectQty.MinValue = 0;
            this.uNumInspectQty.Name = "uNumInspectQty";
            this.uNumInspectQty.Size = new System.Drawing.Size(110, 21);
            this.uNumInspectQty.TabIndex = 30;
            this.uNumInspectQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumInspectQty_EditorSpinButtonClick);
            this.uNumInspectQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumInspectQty_EditorButtonClick);
            // 
            // uNumQty
            // 
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton8.Text = "0";
            this.uNumQty.ButtonsLeft.Add(editorButton8);
            spinEditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumQty.ButtonsRight.Add(spinEditorButton3);
            this.uNumQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumQty.Location = new System.Drawing.Point(116, 132);
            this.uNumQty.MaskInput = "nnnnnnnnnn";
            this.uNumQty.Name = "uNumQty";
            this.uNumQty.Size = new System.Drawing.Size(110, 21);
            this.uNumQty.TabIndex = 28;
            this.uNumQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumQty_EditorSpinButtonClick);
            this.uNumQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumQty_EditorButtonClick);
            // 
            // uCheckWorkStepEquipFlag
            // 
            this.uCheckWorkStepEquipFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckWorkStepEquipFlag.Location = new System.Drawing.Point(456, 108);
            this.uCheckWorkStepEquipFlag.Name = "uCheckWorkStepEquipFlag";
            this.uCheckWorkStepEquipFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckWorkStepEquipFlag.TabIndex = 37;
            this.uCheckWorkStepEquipFlag.Text = "장비 재검";
            this.uCheckWorkStepEquipFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckWorkStepMaterialFlag
            // 
            appearance73.BackColorDisabled = System.Drawing.Color.Transparent;
            appearance73.ForeColorDisabled = System.Drawing.Color.Black;
            this.uCheckWorkStepMaterialFlag.Appearance = appearance73;
            this.uCheckWorkStepMaterialFlag.Checked = true;
            this.uCheckWorkStepMaterialFlag.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uCheckWorkStepMaterialFlag.Enabled = false;
            this.uCheckWorkStepMaterialFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckWorkStepMaterialFlag.Location = new System.Drawing.Point(380, 108);
            this.uCheckWorkStepMaterialFlag.Name = "uCheckWorkStepMaterialFlag";
            this.uCheckWorkStepMaterialFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckWorkStepMaterialFlag.TabIndex = 36;
            this.uCheckWorkStepMaterialFlag.Text = "자재 재검";
            this.uCheckWorkStepMaterialFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelWorkStep
            // 
            this.uLabelWorkStep.Location = new System.Drawing.Point(276, 108);
            this.uLabelWorkStep.Name = "uLabelWorkStep";
            this.uLabelWorkStep.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkStep.TabIndex = 31;
            this.uLabelWorkStep.Text = "ultraLabel1";
            // 
            // uComboInspectFaultTypeCode
            // 
            this.uComboInspectFaultTypeCode.Location = new System.Drawing.Point(116, 84);
            this.uComboInspectFaultTypeCode.Name = "uComboInspectFaultTypeCode";
            this.uComboInspectFaultTypeCode.Size = new System.Drawing.Size(150, 21);
            this.uComboInspectFaultTypeCode.TabIndex = 34;
            // 
            // uLabelInspectFaultType
            // 
            this.uLabelInspectFaultType.Location = new System.Drawing.Point(12, 84);
            this.uLabelInspectFaultType.Name = "uLabelInspectFaultType";
            this.uLabelInspectFaultType.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectFaultType.TabIndex = 29;
            this.uLabelInspectFaultType.Text = "ultraLabel1";
            // 
            // uLabelInspectQty
            // 
            this.uLabelInspectQty.Location = new System.Drawing.Point(12, 156);
            this.uLabelInspectQty.Name = "uLabelInspectQty";
            this.uLabelInspectQty.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectQty.TabIndex = 27;
            this.uLabelInspectQty.Text = "ultraLabel1";
            // 
            // uLabelQty
            // 
            this.uLabelQty.Location = new System.Drawing.Point(12, 132);
            this.uLabelQty.Name = "uLabelQty";
            this.uLabelQty.Size = new System.Drawing.Size(100, 20);
            this.uLabelQty.TabIndex = 25;
            this.uLabelQty.Text = "ultraLabel1";
            // 
            // uDateAriseDate
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseDate.Appearance = appearance22;
            this.uDateAriseDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateAriseDate.Location = new System.Drawing.Point(908, 8);
            this.uDateAriseDate.Name = "uDateAriseDate";
            this.uDateAriseDate.Size = new System.Drawing.Size(20, 21);
            this.uDateAriseDate.TabIndex = 18;
            this.uDateAriseDate.Visible = false;
            // 
            // uLabelAriseDate
            // 
            this.uLabelAriseDate.Location = new System.Drawing.Point(884, 8);
            this.uLabelAriseDate.Name = "uLabelAriseDate";
            this.uLabelAriseDate.Size = new System.Drawing.Size(20, 20);
            this.uLabelAriseDate.TabIndex = 23;
            this.uLabelAriseDate.Text = "ultraLabel1";
            this.uLabelAriseDate.Visible = false;
            // 
            // uLabelExpectProcess
            // 
            this.uLabelExpectProcess.Location = new System.Drawing.Point(276, 84);
            this.uLabelExpectProcess.Name = "uLabelExpectProcess";
            this.uLabelExpectProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelExpectProcess.TabIndex = 17;
            this.uLabelExpectProcess.Text = "ultraLabel1";
            // 
            // uLabelAriseEquip
            // 
            this.uLabelAriseEquip.Location = new System.Drawing.Point(540, 60);
            this.uLabelAriseEquip.Name = "uLabelAriseEquip";
            this.uLabelAriseEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelAriseEquip.TabIndex = 15;
            this.uLabelAriseEquip.Text = "ultraLabel1";
            // 
            // uLabelAriseProcess
            // 
            this.uLabelAriseProcess.Location = new System.Drawing.Point(276, 60);
            this.uLabelAriseProcess.Name = "uLabelAriseProcess";
            this.uLabelAriseProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelAriseProcess.TabIndex = 13;
            this.uLabelAriseProcess.Text = "ultraLabel1";
            // 
            // uTextProductCode
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance19;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(644, 12);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(150, 21);
            this.uTextProductCode.TabIndex = 9;
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(540, 12);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelProduct.TabIndex = 8;
            this.uLabelProduct.Text = "ultraLabel1";
            // 
            // uTextCustomerName
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance42;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(116, 36);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(150, 21);
            this.uTextCustomerName.TabIndex = 7;
            // 
            // uLabelCustomerName
            // 
            this.uLabelCustomerName.Location = new System.Drawing.Point(12, 36);
            this.uLabelCustomerName.Name = "uLabelCustomerName";
            this.uLabelCustomerName.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomerName.TabIndex = 6;
            this.uLabelCustomerName.Text = "ultraLabel1";
            // 
            // uTextLotNo
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.Appearance = appearance1;
            this.uTextLotNo.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLotNo.Location = new System.Drawing.Point(116, 60);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(150, 21);
            this.uTextLotNo.TabIndex = 5;
            this.uTextLotNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLotNo_KeyDown);
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(12, 60);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelLotNo.TabIndex = 4;
            this.uLabelLotNo.Text = "ultraLabel1";
            // 
            // uTextQCNNo
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQCNNo.Appearance = appearance2;
            this.uTextQCNNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQCNNo.Location = new System.Drawing.Point(380, 12);
            this.uTextQCNNo.Name = "uTextQCNNo";
            this.uTextQCNNo.ReadOnly = true;
            this.uTextQCNNo.Size = new System.Drawing.Size(150, 21);
            this.uTextQCNNo.TabIndex = 3;
            // 
            // uLabelQCNNo
            // 
            this.uLabelQCNNo.Location = new System.Drawing.Point(276, 12);
            this.uLabelQCNNo.Name = "uLabelQCNNo";
            this.uLabelQCNNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelQCNNo.TabIndex = 2;
            this.uLabelQCNNo.Text = "ultraLabel1";
            // 
            // uComboPlantCode
            // 
            this.uComboPlantCode.Location = new System.Drawing.Point(116, 12);
            this.uComboPlantCode.Name = "uComboPlantCode";
            this.uComboPlantCode.Size = new System.Drawing.Size(150, 21);
            this.uComboPlantCode.TabIndex = 1;
            this.uComboPlantCode.ValueChanged += new System.EventHandler(this.uComboPlantCode_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // frmINSZ0010
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGridQCNList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0010";
            this.Load += new System.EventHandler(this.frmINSZ0010_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0010_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0010_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSerachAriseDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchAriseDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchQCNNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupQCNUser)).EndInit();
            this.uGroupQCNUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAffectLotList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).EndInit();
            this.uGroupBoxStdInfo.ResumeLayout(false);
            this.uGroupBoxStdInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExpectEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotHoldState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboExpectProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPublishComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckPublishFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESWorkUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumFaultQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInspectQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepEquipFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepMaterialFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFaultTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQCNNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProduct;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcessCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchQCNNo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSerachAriseDateTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchAriseDateFrom;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchQCNNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQCNList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkUser;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag9;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag8;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag7;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag6;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag5;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag3;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAffectLotList;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxStdInfo;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumFaultQty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumInspectQty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumQty;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckWorkStepEquipFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckWorkStepMaterialFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkStep;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectFaultTypeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectFaultType;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectQty;
        private Infragistics.Win.Misc.UltraLabel uLabelQty;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAriseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelExpectProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextQCNNo;
        private Infragistics.Win.Misc.UltraLabel uLabelQCNNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAriseTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESWorkUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProductSpec;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckPublishFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelPublishFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPublishComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboAriseProcessCode;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboExpectProcessCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboAriseEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotProcessState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotHoldState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridExpectEquipList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraGroupBox uGroupQCNUser;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQCNUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultImg;
        private Infragistics.Win.Misc.UltraLabel uLabelFaultImg;
    }
}