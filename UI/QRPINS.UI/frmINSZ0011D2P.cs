﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0011D2P.cs                                     */
/* 프로그램명   : QCN LIST 자재재검 검사값 입력 창                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-07-30                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~  추가 (홍길동)                     */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPINS.UI
{
    public partial class frmINSZ0011D2P : Form
    {
        // 다국어 지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        #region 전역변수

        private string m_strPlantCode;
        private string m_strQCNNo;
        private int m_intInspectSeq;
        private int m_intSeq;
        private bool m_bolSaveCheck;

        public bool SaveCheck
        {
            get { return m_bolSaveCheck; }
            set { m_bolSaveCheck = value; }
        }

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string QCNNo
        {
            get { return m_strQCNNo; }
            set { m_strQCNNo = value; }
        }

        public int InspectSeq
        {
            get { return m_intInspectSeq; }
            set { m_intInspectSeq = value; }
        }

        public int Seq
        {
            get { return m_intSeq; }
            set { m_intSeq = value; }
        }

        #endregion

        public frmINSZ0011D2P()
        {
            InitializeComponent();
        }

        private void frmINSZ0011D2P_Load(object sender, EventArgs e)
        {
            InitButton();
            InitGrid();
            InitEtc();

            Search();
        }

        private void frmINSZ0011D2P_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        #region 컨트롤 초기화 Method

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinButton wButton = new QRPCOM.QRPUI.WinButton();

                wButton.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtonSave, "저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonSave.Click += new EventHandler(uButtonSave_Click);
                this.uButtonClose.Click += new EventHandler(uButtonClose_Click);
                this.uButtonDelete.Click += new EventHandler(uButtonDelete_Click);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // 그리드 속성설정
                wGrid.mfInitGeneralGrid(this.uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show
                    , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true, Infragistics.Win.DefaultableBoolean.False
                    , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼
                wGrid.mfSetGridColumn(this.uGridList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridList, 0, "ValueSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridList, 0, "InspectValue", "검사값", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnnnnnnn.nnnnn", "0.0");

                this.uGridList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
                grd.mfLoadGridColumnProperty(this);

                if (SaveCheck)
                {
                    this.uGridList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    this.uGridList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                }
                else
                {
                    this.uGridList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    this.uGridList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                this.titleArea.mfSetLabelText("자재재검 검사값", m_resSys.GetString("SYS_FONTNAME"), 12);

                QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
                brw.mfSetFormLanguage(this);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void Search()
        {
            try
            {
                System.Resources.ResourceSet m_SysRes = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNMatReInspectValue), "ProcQCNMatReInspectValue");
                QRPINS.BL.INSPRC.ProcQCNMatReInspectValue clsValue = new QRPINS.BL.INSPRC.ProcQCNMatReInspectValue();
                brwChannel.mfCredentials(clsValue);

                DataTable dtValueList = clsValue.mfReadProcQCNMatReInspectValue(PlantCode, QCNNo, InspectSeq, Seq, 0, m_SysRes.GetString("SYS_LANG"));

                this.uGridList.SetDataBinding(dtValueList, string.Empty);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장버튼 이벤트
        void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!SaveCheck)
                    return;

                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                DataTable dtValue = SetDataInfo();
                DataRow _dr;

                for (int i = 0; i < this.uGridList.Rows.Count; i++)
                {
                    if (this.uGridList.Rows[i].Hidden.Equals(false))
                    {
                        _dr = dtValue.NewRow();
                        _dr["PlantCode"] = PlantCode;
                        _dr["QCNNo"] = QCNNo;
                        _dr["InspectSeq"] = InspectSeq;
                        _dr["Seq"] = Seq;
                        _dr["ValueSeq"] = this.uGridList.Rows[i].RowSelectorNumber;
                        _dr["InspectValue"] = ReturnDecimalValue(this.uGridList.Rows[i].Cells["InspectValue"].Value.ToString());
                        dtValue.Rows.Add(_dr);
                    }
                }

                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNMatReInspectValue), "ProcQCNMatReInspectValue");
                QRPINS.BL.INSPRC.ProcQCNMatReInspectValue clsValue = new QRPINS.BL.INSPRC.ProcQCNMatReInspectValue();
                brwChannel.mfCredentials(clsValue);

                string strErrRtn = clsValue.mfSaveProcQCNMatReInspectValue(dtValue, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum.Equals(0))
                {
                    DialogResult Result = new DialogResult();
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                    Result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000954",
                                        Infragistics.Win.HAlign.Right);

                    this.Close();
                }
                else
                {
                    DialogResult Result = new DialogResult();
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();

                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000953",
                                            Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                            , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                            , ErrRtn.ErrMessage
                                            , Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 닫기 버튼 이벤트
        void uButtonClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 버튼 이벤트
        void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridList.Rows[i].Cells["Check"].Value))
                    {
                        this.uGridList.Rows[i].Hidden = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private DataTable SetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                dtRtn.Columns.Add("InspectSeq", typeof(Int32));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("ValueSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(decimal));
                return dtRtn;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }
    }
}
