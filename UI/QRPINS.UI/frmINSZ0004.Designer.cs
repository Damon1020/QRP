﻿namespace QRPINS.UI
{
    partial class frmINSZ0004
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0004));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uDateSearchToWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchAuditGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchAuditGrade = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridAVLList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextImprove = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelImprove = new Infragistics.Win.Misc.UltraLabel();
            this.uTextResult = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDownFile = new Infragistics.Win.Misc.UltraButton();
            this.uGridAVLFile = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uLabelResult1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridAVLHistory = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDownConfirm = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteRow2 = new Infragistics.Win.Misc.UltraButton();
            this.uGridAVLConfirm = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextWriteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAuditGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboAuditGrade = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchAuditGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextImprove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAuditGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchAuditGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchAuditGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchPlant.TabIndex = 235;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uDateSearchToWriteDate
            // 
            this.uDateSearchToWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToWriteDate.Location = new System.Drawing.Point(720, 8);
            this.uDateSearchToWriteDate.Name = "uDateSearchToWriteDate";
            this.uDateSearchToWriteDate.Size = new System.Drawing.Size(25, 21);
            this.uDateSearchToWriteDate.TabIndex = 233;
            this.uDateSearchToWriteDate.Visible = false;
            // 
            // uDateSearchFromWriteDate
            // 
            this.uDateSearchFromWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromWriteDate.Location = new System.Drawing.Point(706, 8);
            this.uDateSearchFromWriteDate.Name = "uDateSearchFromWriteDate";
            this.uDateSearchFromWriteDate.Size = new System.Drawing.Size(28, 21);
            this.uDateSearchFromWriteDate.TabIndex = 233;
            this.uDateSearchFromWriteDate.Visible = false;
            // 
            // uTextSearchVendorName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance40;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(570, 12);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchVendorName.TabIndex = 232;
            // 
            // uTextSearchVendorCode
            // 
            appearance41.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance41.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance41;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(468, 12);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchVendorCode.TabIndex = 232;
            this.uTextSearchVendorCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchVendorCode_KeyDown);
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uLabelSearchAuditGrade
            // 
            this.uLabelSearchAuditGrade.Location = new System.Drawing.Point(788, 12);
            this.uLabelSearchAuditGrade.Name = "uLabelSearchAuditGrade";
            this.uLabelSearchAuditGrade.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchAuditGrade.TabIndex = 229;
            this.uLabelSearchAuditGrade.Text = "3";
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(352, 12);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchVendor.TabIndex = 227;
            this.uLabelSearchVendor.Text = "1";
            // 
            // uComboSearchAuditGrade
            // 
            this.uComboSearchAuditGrade.Location = new System.Drawing.Point(904, 12);
            this.uComboSearchAuditGrade.Name = "uComboSearchAuditGrade";
            this.uComboSearchAuditGrade.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchAuditGrade.TabIndex = 1;
            this.uComboSearchAuditGrade.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridAVLList
            // 
            this.uGridAVLList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAVLList.DisplayLayout.Appearance = appearance7;
            this.uGridAVLList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAVLList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance4.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance4.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLList.DisplayLayout.GroupByBox.Appearance = appearance4;
            appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance49;
            this.uGridAVLList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLList.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGridAVLList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAVLList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAVLList.DisplayLayout.Override.ActiveCellAppearance = appearance15;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAVLList.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridAVLList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAVLList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAVLList.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAVLList.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridAVLList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAVLList.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLList.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance14.TextHAlignAsString = "Left";
            this.uGridAVLList.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridAVLList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAVLList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGridAVLList.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGridAVLList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance11.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAVLList.DisplayLayout.Override.TemplateAddRowAppearance = appearance11;
            this.uGridAVLList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAVLList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAVLList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAVLList.Location = new System.Drawing.Point(0, 80);
            this.uGridAVLList.Name = "uGridAVLList";
            this.uGridAVLList.Size = new System.Drawing.Size(1060, 650);
            this.uGridAVLList.TabIndex = 2;
            this.uGridAVLList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridAVLList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 128);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextImprove);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelImprove);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextResult);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelResult1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendorName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAuditGrade);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendorCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVendor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboAuditGrade);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextImprove
            // 
            appearance44.BackColor = System.Drawing.Color.White;
            this.uTextImprove.Appearance = appearance44;
            this.uTextImprove.BackColor = System.Drawing.Color.White;
            appearance45.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance45.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance45;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ScenicRibbonButton;
            this.uTextImprove.ButtonsRight.Add(editorButton2);
            this.uTextImprove.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextImprove.Location = new System.Drawing.Point(930, 60);
            this.uTextImprove.Name = "uTextImprove";
            this.uTextImprove.Size = new System.Drawing.Size(114, 21);
            this.uTextImprove.TabIndex = 240;
            this.uTextImprove.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextImprove_EditorButtonClick);
            // 
            // uLabelImprove
            // 
            this.uLabelImprove.Location = new System.Drawing.Point(816, 60);
            this.uLabelImprove.Name = "uLabelImprove";
            this.uLabelImprove.Size = new System.Drawing.Size(110, 20);
            this.uLabelImprove.TabIndex = 239;
            this.uLabelImprove.Text = "ultraLabel2";
            // 
            // uTextResult
            // 
            appearance47.BackColor = System.Drawing.Color.White;
            this.uTextResult.Appearance = appearance47;
            this.uTextResult.BackColor = System.Drawing.Color.White;
            appearance62.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance62.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance62;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ScenicRibbonButton;
            this.uTextResult.ButtonsRight.Add(editorButton3);
            this.uTextResult.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextResult.Location = new System.Drawing.Point(930, 36);
            this.uTextResult.Name = "uTextResult";
            this.uTextResult.Size = new System.Drawing.Size(114, 21);
            this.uTextResult.TabIndex = 238;
            this.uTextResult.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextResult_EditorButtonClick);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uButtonDownFile);
            this.uGroupBox1.Controls.Add(this.uGridAVLFile);
            this.uGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox1.Location = new System.Drawing.Point(928, 4);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(104, 36);
            this.uGroupBox1.TabIndex = 227;
            this.uGroupBox1.Visible = false;
            // 
            // uButtonDownFile
            // 
            this.uButtonDownFile.Location = new System.Drawing.Point(104, 28);
            this.uButtonDownFile.Name = "uButtonDownFile";
            this.uButtonDownFile.Size = new System.Drawing.Size(88, 28);
            this.uButtonDownFile.TabIndex = 2;
            this.uButtonDownFile.Text = "ultraButton1";
            this.uButtonDownFile.Click += new System.EventHandler(this.uButtonDownFile_Click);
            // 
            // uGridAVLFile
            // 
            this.uGridAVLFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAVLFile.DisplayLayout.Appearance = appearance16;
            this.uGridAVLFile.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAVLFile.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLFile.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLFile.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.uGridAVLFile.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLFile.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            this.uGridAVLFile.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAVLFile.DisplayLayout.MaxRowScrollRegions = 1;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAVLFile.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Highlight;
            appearance21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAVLFile.DisplayLayout.Override.ActiveRowAppearance = appearance21;
            this.uGridAVLFile.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAVLFile.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAVLFile.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAVLFile.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGridAVLFile.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAVLFile.DisplayLayout.Override.CellPadding = 0;
            appearance24.BackColor = System.Drawing.SystemColors.Control;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLFile.DisplayLayout.Override.GroupByRowAppearance = appearance24;
            appearance25.TextHAlignAsString = "Left";
            this.uGridAVLFile.DisplayLayout.Override.HeaderAppearance = appearance25;
            this.uGridAVLFile.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAVLFile.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            this.uGridAVLFile.DisplayLayout.Override.RowAppearance = appearance26;
            this.uGridAVLFile.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAVLFile.DisplayLayout.Override.TemplateAddRowAppearance = appearance27;
            this.uGridAVLFile.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAVLFile.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAVLFile.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAVLFile.Location = new System.Drawing.Point(12, -100);
            this.uGridAVLFile.Name = "uGridAVLFile";
            this.uGridAVLFile.Size = new System.Drawing.Size(83, 128);
            this.uGridAVLFile.TabIndex = 1;
            this.uGridAVLFile.Text = "ultraGrid1";
            this.uGridAVLFile.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGridAVLFile.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAVLFile_ClickCellButton);
            this.uGridAVLFile.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAVLFile_CellChange);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 0;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uLabelResult1
            // 
            this.uLabelResult1.Location = new System.Drawing.Point(816, 36);
            this.uLabelResult1.Name = "uLabelResult1";
            this.uLabelResult1.Size = new System.Drawing.Size(110, 20);
            this.uLabelResult1.TabIndex = 237;
            this.uLabelResult1.Text = "ultraLabel1";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(128, 60);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(684, 21);
            this.uTextEtcDesc.TabIndex = 235;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 60);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelEtcDesc.TabIndex = 234;
            this.uLabelEtcDesc.Text = "ultraLabel1";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uGridAVLHistory);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 404);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1036, 244);
            this.uGroupBox3.TabIndex = 228;
            // 
            // uGridAVLHistory
            // 
            this.uGridAVLHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAVLHistory.DisplayLayout.Appearance = appearance31;
            this.uGridAVLHistory.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAVLHistory.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLHistory.DisplayLayout.GroupByBox.Appearance = appearance28;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLHistory.DisplayLayout.GroupByBox.BandLabelAppearance = appearance29;
            this.uGridAVLHistory.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance30.BackColor2 = System.Drawing.SystemColors.Control;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLHistory.DisplayLayout.GroupByBox.PromptAppearance = appearance30;
            this.uGridAVLHistory.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAVLHistory.DisplayLayout.MaxRowScrollRegions = 1;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAVLHistory.DisplayLayout.Override.ActiveCellAppearance = appearance39;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAVLHistory.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.uGridAVLHistory.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAVLHistory.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAVLHistory.DisplayLayout.Override.CardAreaAppearance = appearance33;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAVLHistory.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGridAVLHistory.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAVLHistory.DisplayLayout.Override.CellPadding = 0;
            appearance36.BackColor = System.Drawing.SystemColors.Control;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLHistory.DisplayLayout.Override.GroupByRowAppearance = appearance36;
            appearance38.TextHAlignAsString = "Left";
            this.uGridAVLHistory.DisplayLayout.Override.HeaderAppearance = appearance38;
            this.uGridAVLHistory.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAVLHistory.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            this.uGridAVLHistory.DisplayLayout.Override.RowAppearance = appearance37;
            this.uGridAVLHistory.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAVLHistory.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridAVLHistory.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAVLHistory.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAVLHistory.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAVLHistory.Location = new System.Drawing.Point(12, 28);
            this.uGridAVLHistory.Name = "uGridAVLHistory";
            this.uGridAVLHistory.Size = new System.Drawing.Size(1015, 208);
            this.uGridAVLHistory.TabIndex = 0;
            this.uGridAVLHistory.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridAVLHistory_DoubleClickCell);
            // 
            // uDateWriteDate
            // 
            appearance43.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Appearance = appearance43;
            this.uDateWriteDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(128, 37);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDate.TabIndex = 233;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uButtonDownConfirm);
            this.uGroupBox2.Controls.Add(this.uButtonDeleteRow2);
            this.uGroupBox2.Controls.Add(this.uGridAVLConfirm);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 88);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1036, 308);
            this.uGroupBox2.TabIndex = 227;
            // 
            // uButtonDownConfirm
            // 
            this.uButtonDownConfirm.Location = new System.Drawing.Point(104, 28);
            this.uButtonDownConfirm.Name = "uButtonDownConfirm";
            this.uButtonDownConfirm.Size = new System.Drawing.Size(88, 28);
            this.uButtonDownConfirm.TabIndex = 3;
            this.uButtonDownConfirm.Text = "ultraButton1";
            this.uButtonDownConfirm.Click += new System.EventHandler(this.uButtonDownConfirm_Click);
            // 
            // uButtonDeleteRow2
            // 
            this.uButtonDeleteRow2.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow2.Name = "uButtonDeleteRow2";
            this.uButtonDeleteRow2.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow2.TabIndex = 1;
            this.uButtonDeleteRow2.Text = "ultraButton1";
            this.uButtonDeleteRow2.Click += new System.EventHandler(this.uButtonDeleteRow2_Click);
            // 
            // uGridAVLConfirm
            // 
            this.uGridAVLConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAVLConfirm.DisplayLayout.Appearance = appearance50;
            this.uGridAVLConfirm.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAVLConfirm.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance51.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance51.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLConfirm.DisplayLayout.GroupByBox.Appearance = appearance51;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLConfirm.DisplayLayout.GroupByBox.BandLabelAppearance = appearance52;
            this.uGridAVLConfirm.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance53.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance53.BackColor2 = System.Drawing.SystemColors.Control;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance53.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAVLConfirm.DisplayLayout.GroupByBox.PromptAppearance = appearance53;
            this.uGridAVLConfirm.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAVLConfirm.DisplayLayout.MaxRowScrollRegions = 1;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            appearance54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAVLConfirm.DisplayLayout.Override.ActiveCellAppearance = appearance54;
            appearance55.BackColor = System.Drawing.SystemColors.Highlight;
            appearance55.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAVLConfirm.DisplayLayout.Override.ActiveRowAppearance = appearance55;
            this.uGridAVLConfirm.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAVLConfirm.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance56.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAVLConfirm.DisplayLayout.Override.CardAreaAppearance = appearance56;
            appearance57.BorderColor = System.Drawing.Color.Silver;
            appearance57.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAVLConfirm.DisplayLayout.Override.CellAppearance = appearance57;
            this.uGridAVLConfirm.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAVLConfirm.DisplayLayout.Override.CellPadding = 0;
            appearance58.BackColor = System.Drawing.SystemColors.Control;
            appearance58.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance58.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance58.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAVLConfirm.DisplayLayout.Override.GroupByRowAppearance = appearance58;
            appearance59.TextHAlignAsString = "Left";
            this.uGridAVLConfirm.DisplayLayout.Override.HeaderAppearance = appearance59;
            this.uGridAVLConfirm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAVLConfirm.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance60.BackColor = System.Drawing.SystemColors.Window;
            appearance60.BorderColor = System.Drawing.Color.Silver;
            this.uGridAVLConfirm.DisplayLayout.Override.RowAppearance = appearance60;
            this.uGridAVLConfirm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance61.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAVLConfirm.DisplayLayout.Override.TemplateAddRowAppearance = appearance61;
            this.uGridAVLConfirm.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAVLConfirm.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAVLConfirm.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAVLConfirm.Location = new System.Drawing.Point(12, 60);
            this.uGridAVLConfirm.Name = "uGridAVLConfirm";
            this.uGridAVLConfirm.Size = new System.Drawing.Size(1015, 236);
            this.uGridAVLConfirm.TabIndex = 0;
            this.uGridAVLConfirm.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid3_AfterCellUpdate);
            this.uGridAVLConfirm.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAVLConfirm_ClickCellButton);
            this.uGridAVLConfirm.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAVLConfirm_CellChange);
            // 
            // uTextWriteUserName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Appearance = appearance3;
            this.uTextWriteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Location = new System.Drawing.Point(456, 36);
            this.uTextWriteUserName.Name = "uTextWriteUserName";
            this.uTextWriteUserName.ReadOnly = true;
            this.uTextWriteUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteUserName.TabIndex = 232;
            // 
            // uTextVendorName
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance48;
            this.uTextVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Location = new System.Drawing.Point(456, 13);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(100, 21);
            this.uTextVendorName.TabIndex = 232;
            // 
            // uLabelAuditGrade
            // 
            this.uLabelAuditGrade.Location = new System.Drawing.Point(564, 36);
            this.uLabelAuditGrade.Name = "uLabelAuditGrade";
            this.uLabelAuditGrade.Size = new System.Drawing.Size(110, 20);
            this.uLabelAuditGrade.TabIndex = 226;
            this.uLabelAuditGrade.Text = "11";
            // 
            // uTextWriteUserID
            // 
            appearance46.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteUserID.Appearance = appearance46;
            this.uTextWriteUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance2.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance2;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteUserID.ButtonsRight.Add(editorButton4);
            this.uTextWriteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteUserID.Location = new System.Drawing.Point(352, 36);
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteUserID.TabIndex = 232;
            this.uTextWriteUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteUserID_KeyDown);
            this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteUserID_EditorButtonClick);
            // 
            // uTextVendorCode
            // 
            appearance63.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextVendorCode.Appearance = appearance63;
            this.uTextVendorCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance64.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance64.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance64;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextVendorCode.ButtonsRight.Add(editorButton5);
            this.uTextVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextVendorCode.Location = new System.Drawing.Point(352, 13);
            this.uTextVendorCode.Name = "uTextVendorCode";
            this.uTextVendorCode.Size = new System.Drawing.Size(100, 21);
            this.uTextVendorCode.TabIndex = 232;
            this.uTextVendorCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextVendorCode_KeyDown);
            this.uTextVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextVendorCode_EditorButtonClick);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 221;
            this.uLabelPlant.Text = "6";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(236, 12);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelVendor.TabIndex = 222;
            this.uLabelVendor.Text = "7";
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(236, 36);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(110, 20);
            this.uLabelWriteUser.TabIndex = 225;
            this.uLabelWriteUser.Text = "10";
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelWriteDate.TabIndex = 224;
            this.uLabelWriteDate.Text = "9";
            // 
            // uComboAuditGrade
            // 
            this.uComboAuditGrade.Location = new System.Drawing.Point(680, 36);
            this.uComboAuditGrade.Name = "uComboAuditGrade";
            this.uComboAuditGrade.Size = new System.Drawing.Size(130, 21);
            this.uComboAuditGrade.TabIndex = 1;
            this.uComboAuditGrade.Text = "ultraComboEditor1";
            // 
            // uComboPlant
            // 
            appearance42.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance42;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(128, 13);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(100, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // frmINSZ0004
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridAVLList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0004";
            this.Load += new System.EventHandler(this.frmINSZ0004_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0004_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0004_FormClosing);
            this.Resize += new System.EventHandler(this.frmINSZ0004_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchAuditGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextImprove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAVLConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAuditGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchAuditGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchAuditGrade;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAVLList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelAuditGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAVLConfirm;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAVLFile;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAuditGrade;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAVLHistory;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraButton uButtonDownConfirm;
        private Infragistics.Win.Misc.UltraButton uButtonDownFile;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelResult1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextImprove;
        private Infragistics.Win.Misc.UltraLabel uLabelImprove;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextResult;
    }
}