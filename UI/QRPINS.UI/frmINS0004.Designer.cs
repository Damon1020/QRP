﻿namespace QRPINS.UI
{
    partial class frmINS0004
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINS0004));
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridReqItem = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridReqSampling = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid5 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchInspectResultFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspectResultFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMaterialGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMaterialGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateSearchToInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridLot = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextLotCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotCount = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShipmentCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelShipmentCount = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFloorPlanNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialGradeCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWMSTransFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFloorPlanNo = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.uTextSamplingLot = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateTimeReceiptTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uOptionICPCheckFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelICPCheck = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionInspectResultType = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelInspectResult = new Infragistics.Win.Misc.UltraLabel();
            this.uDateReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelReceiptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextGRDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialGradeName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpecNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uTextGRQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReqNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonExcelUpload = new Infragistics.Win.Misc.UltraButton();
            this.uButtonMoveAbnormal = new Infragistics.Win.Misc.UltraButton();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateInspectTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextEtcDescQA = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateAdmitDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextFaultDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAdmitUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFaultContent = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uOptionPassFailFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelAcceptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudge = new Infragistics.Win.Misc.UltraLabel();
            this.uDateReviewDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextReviewUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReviewUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReviewUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTestUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcQA = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridInspectList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridReqItem)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridReqSampling)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectResultFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGradeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWMSTransFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSamplingLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeReceiptTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionICPCheckFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionInspectResultType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGradeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDescQA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAdmitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReviewDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReviewUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReviewUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInspectList)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridReqItem);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1060, 266);
            // 
            // uGridReqItem
            // 
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridReqItem.DisplayLayout.Appearance = appearance42;
            this.uGridReqItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridReqItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReqItem.DisplayLayout.GroupByBox.Appearance = appearance43;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReqItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance44;
            this.uGridReqItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance45.BackColor2 = System.Drawing.SystemColors.Control;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReqItem.DisplayLayout.GroupByBox.PromptAppearance = appearance45;
            this.uGridReqItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridReqItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridReqItem.DisplayLayout.Override.ActiveCellAppearance = appearance46;
            appearance47.BackColor = System.Drawing.SystemColors.Highlight;
            appearance47.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridReqItem.DisplayLayout.Override.ActiveRowAppearance = appearance47;
            this.uGridReqItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridReqItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            this.uGridReqItem.DisplayLayout.Override.CardAreaAppearance = appearance48;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            appearance49.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridReqItem.DisplayLayout.Override.CellAppearance = appearance49;
            this.uGridReqItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridReqItem.DisplayLayout.Override.CellPadding = 0;
            appearance50.BackColor = System.Drawing.SystemColors.Control;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReqItem.DisplayLayout.Override.GroupByRowAppearance = appearance50;
            appearance51.TextHAlignAsString = "Left";
            this.uGridReqItem.DisplayLayout.Override.HeaderAppearance = appearance51;
            this.uGridReqItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridReqItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            this.uGridReqItem.DisplayLayout.Override.RowAppearance = appearance52;
            this.uGridReqItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance53.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridReqItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance53;
            this.uGridReqItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridReqItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridReqItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridReqItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridReqItem.Location = new System.Drawing.Point(0, 0);
            this.uGridReqItem.Name = "uGridReqItem";
            this.uGridReqItem.Size = new System.Drawing.Size(1060, 266);
            this.uGridReqItem.TabIndex = 1;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridReqSampling);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1060, 266);
            // 
            // uGridReqSampling
            // 
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            appearance57.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridReqSampling.DisplayLayout.Appearance = appearance57;
            this.uGridReqSampling.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridReqSampling.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance54.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance54.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance54.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReqSampling.DisplayLayout.GroupByBox.Appearance = appearance54;
            appearance55.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReqSampling.DisplayLayout.GroupByBox.BandLabelAppearance = appearance55;
            this.uGridReqSampling.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance56.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance56.BackColor2 = System.Drawing.SystemColors.Control;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance56.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReqSampling.DisplayLayout.GroupByBox.PromptAppearance = appearance56;
            this.uGridReqSampling.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridReqSampling.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridReqSampling.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance60.BackColor = System.Drawing.SystemColors.Highlight;
            appearance60.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridReqSampling.DisplayLayout.Override.ActiveRowAppearance = appearance60;
            this.uGridReqSampling.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridReqSampling.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            this.uGridReqSampling.DisplayLayout.Override.CardAreaAppearance = appearance59;
            appearance58.BorderColor = System.Drawing.Color.Silver;
            appearance58.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridReqSampling.DisplayLayout.Override.CellAppearance = appearance58;
            this.uGridReqSampling.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridReqSampling.DisplayLayout.Override.CellPadding = 0;
            appearance62.BackColor = System.Drawing.SystemColors.Control;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReqSampling.DisplayLayout.Override.GroupByRowAppearance = appearance62;
            appearance64.TextHAlignAsString = "Left";
            this.uGridReqSampling.DisplayLayout.Override.HeaderAppearance = appearance64;
            this.uGridReqSampling.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridReqSampling.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.BorderColor = System.Drawing.Color.Silver;
            this.uGridReqSampling.DisplayLayout.Override.RowAppearance = appearance63;
            this.uGridReqSampling.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance61.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridReqSampling.DisplayLayout.Override.TemplateAddRowAppearance = appearance61;
            this.uGridReqSampling.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridReqSampling.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridReqSampling.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridReqSampling.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridReqSampling.Location = new System.Drawing.Point(0, 0);
            this.uGridReqSampling.Name = "uGridReqSampling";
            this.uGridReqSampling.Size = new System.Drawing.Size(1060, 266);
            this.uGridReqSampling.TabIndex = 0;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGrid4);
            this.ultraTabPageControl3.Controls.Add(this.ultraGroupBox3);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1060, 266);
            // 
            // uGrid4
            // 
            appearance66.BackColor = System.Drawing.SystemColors.Window;
            appearance66.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4.DisplayLayout.Appearance = appearance66;
            this.uGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance67.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance67.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance67.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.GroupByBox.Appearance = appearance67;
            appearance68.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance68;
            this.uGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance69.BackColor2 = System.Drawing.SystemColors.Control;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance69;
            this.uGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance70;
            appearance71.BackColor = System.Drawing.SystemColors.Highlight;
            appearance71.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance71;
            this.uGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.CardAreaAppearance = appearance72;
            appearance73.BorderColor = System.Drawing.Color.Silver;
            appearance73.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4.DisplayLayout.Override.CellAppearance = appearance73;
            this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance74.BackColor = System.Drawing.SystemColors.Control;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance74;
            appearance75.TextHAlignAsString = "Left";
            this.uGrid4.DisplayLayout.Override.HeaderAppearance = appearance75;
            this.uGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            appearance76.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4.DisplayLayout.Override.RowAppearance = appearance76;
            this.uGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance77.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance77;
            this.uGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid4.Location = new System.Drawing.Point(0, 0);
            this.uGrid4.Name = "uGrid4";
            this.uGrid4.Size = new System.Drawing.Size(684, 266);
            this.uGrid4.TabIndex = 2;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.uGrid5);
            this.ultraGroupBox3.Controls.Add(this.ultraGroupBox4);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraGroupBox3.Location = new System.Drawing.Point(684, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(376, 266);
            this.ultraGroupBox3.TabIndex = 1;
            // 
            // uGrid5
            // 
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid5.DisplayLayout.Appearance = appearance78;
            this.uGrid5.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid5.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance79.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance79.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance79.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.GroupByBox.Appearance = appearance79;
            appearance80.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid5.DisplayLayout.GroupByBox.BandLabelAppearance = appearance80;
            this.uGrid5.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance81.BackColor2 = System.Drawing.SystemColors.Control;
            appearance81.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance81.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid5.DisplayLayout.GroupByBox.PromptAppearance = appearance81;
            this.uGrid5.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid5.DisplayLayout.MaxRowScrollRegions = 1;
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid5.DisplayLayout.Override.ActiveCellAppearance = appearance82;
            appearance83.BackColor = System.Drawing.SystemColors.Highlight;
            appearance83.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid5.DisplayLayout.Override.ActiveRowAppearance = appearance83;
            this.uGrid5.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid5.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance84.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.Override.CardAreaAppearance = appearance84;
            appearance85.BorderColor = System.Drawing.Color.Silver;
            appearance85.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid5.DisplayLayout.Override.CellAppearance = appearance85;
            this.uGrid5.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid5.DisplayLayout.Override.CellPadding = 0;
            appearance86.BackColor = System.Drawing.SystemColors.Control;
            appearance86.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance86.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance86.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.Override.GroupByRowAppearance = appearance86;
            appearance87.TextHAlignAsString = "Left";
            this.uGrid5.DisplayLayout.Override.HeaderAppearance = appearance87;
            this.uGrid5.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid5.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance88.BackColor = System.Drawing.SystemColors.Window;
            appearance88.BorderColor = System.Drawing.Color.Silver;
            this.uGrid5.DisplayLayout.Override.RowAppearance = appearance88;
            this.uGrid5.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance89.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid5.DisplayLayout.Override.TemplateAddRowAppearance = appearance89;
            this.uGrid5.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid5.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid5.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid5.Location = new System.Drawing.Point(3, 40);
            this.uGrid5.Name = "uGrid5";
            this.uGrid5.Size = new System.Drawing.Size(370, 223);
            this.uGrid5.TabIndex = 6;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.uButtonDelete);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox4.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(370, 40);
            this.ultraGroupBox4.TabIndex = 5;
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(8, 8);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 4;
            this.uButtonDelete.Text = "ultraButton1";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectResultFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectResultFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterialGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchInspectResultFlag
            // 
            this.uComboSearchInspectResultFlag.Location = new System.Drawing.Point(960, 12);
            this.uComboSearchInspectResultFlag.Name = "uComboSearchInspectResultFlag";
            this.uComboSearchInspectResultFlag.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchInspectResultFlag.TabIndex = 45;
            this.uComboSearchInspectResultFlag.Text = "ultraComboEditor1";
            // 
            // uLabelSearchInspectResultFlag
            // 
            this.uLabelSearchInspectResultFlag.Location = new System.Drawing.Point(856, 12);
            this.uLabelSearchInspectResultFlag.Name = "uLabelSearchInspectResultFlag";
            this.uLabelSearchInspectResultFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectResultFlag.TabIndex = 44;
            this.uLabelSearchInspectResultFlag.Text = "ultraLabel1";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(696, 12);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchLotNo.TabIndex = 43;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(592, 12);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 42;
            this.uLabelSearchLotNo.Text = "ultraLabel1";
            // 
            // uComboSearchMaterialGroup
            // 
            this.uComboSearchMaterialGroup.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchMaterialGroup.Name = "uComboSearchMaterialGroup";
            this.uComboSearchMaterialGroup.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchMaterialGroup.TabIndex = 23;
            this.uComboSearchMaterialGroup.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMaterialGroup
            // 
            this.uLabelSearchMaterialGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchMaterialGroup.Name = "uLabelSearchMaterialGroup";
            this.uLabelSearchMaterialGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialGroup.TabIndex = 22;
            this.uLabelSearchMaterialGroup.Text = "ultraLabel1";
            // 
            // uTextSearchVendorName
            // 
            appearance91.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance91;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(440, 36);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(144, 21);
            this.uTextSearchVendorName.TabIndex = 21;
            // 
            // uTextSearchVendorCode
            // 
            appearance90.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance90.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance90;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(328, 36);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorCode.TabIndex = 20;
            // 
            // uDateSearchToInspectDate
            // 
            this.uDateSearchToInspectDate.DateTime = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateSearchToInspectDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToInspectDate.Location = new System.Drawing.Point(812, 36);
            this.uDateSearchToInspectDate.Name = "uDateSearchToInspectDate";
            this.uDateSearchToInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToInspectDate.TabIndex = 19;
            this.uDateSearchToInspectDate.Value = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(796, 36);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 18;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchFromInspectDate
            // 
            this.uDateSearchFromInspectDate.DateTime = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateSearchFromInspectDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromInspectDate.Location = new System.Drawing.Point(696, 36);
            this.uDateSearchFromInspectDate.Name = "uDateSearchFromInspectDate";
            this.uDateSearchFromInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromInspectDate.TabIndex = 17;
            this.uDateSearchFromInspectDate.Value = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(592, 36);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectDate.TabIndex = 16;
            this.uLabelSearchInspectDate.Text = "ultraLabel2";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(224, 36);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 10;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uTextSearchMaterialName
            // 
            appearance93.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Appearance = appearance93;
            this.uTextSearchMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(440, 12);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.ReadOnly = true;
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(144, 21);
            this.uTextSearchMaterialName.TabIndex = 7;
            // 
            // uTextSearchMaterialCode
            // 
            appearance92.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance92.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance92;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(328, 12);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialCode.TabIndex = 5;
            // 
            // uLabelSearchMaterialCode
            // 
            this.uLabelSearchMaterialCode.Location = new System.Drawing.Point(224, 12);
            this.uLabelSearchMaterialCode.Name = "uLabelSearchMaterialCode";
            this.uLabelSearchMaterialCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialCode.TabIndex = 4;
            this.uLabelSearchMaterialCode.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 660);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 190);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 660);
            this.uGroupBoxContentsArea.TabIndex = 4;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 640);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTab
            // 
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Controls.Add(this.ultraTabPageControl3);
            this.uTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTab.Location = new System.Drawing.Point(0, 264);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(1064, 292);
            this.uTab.TabIndex = 44;
            ultraTab1.Key = "0";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "검사결과";
            ultraTab2.Key = "1";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "샘플링검사";
            ultraTab3.Key = "2";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "전수검사";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1060, 266);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGridLot);
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1064, 264);
            this.ultraGroupBox1.TabIndex = 43;
            // 
            // uGridLot
            // 
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridLot.DisplayLayout.Appearance = appearance30;
            this.uGridLot.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridLot.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance31.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance31.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLot.DisplayLayout.GroupByBox.Appearance = appearance31;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLot.DisplayLayout.GroupByBox.BandLabelAppearance = appearance32;
            this.uGridLot.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance33.BackColor2 = System.Drawing.SystemColors.Control;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLot.DisplayLayout.GroupByBox.PromptAppearance = appearance33;
            this.uGridLot.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridLot.DisplayLayout.MaxRowScrollRegions = 1;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridLot.DisplayLayout.Override.ActiveCellAppearance = appearance34;
            appearance35.BackColor = System.Drawing.SystemColors.Highlight;
            appearance35.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridLot.DisplayLayout.Override.ActiveRowAppearance = appearance35;
            this.uGridLot.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridLot.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance106.BackColor = System.Drawing.SystemColors.Window;
            this.uGridLot.DisplayLayout.Override.CardAreaAppearance = appearance106;
            appearance107.BorderColor = System.Drawing.Color.Silver;
            appearance107.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridLot.DisplayLayout.Override.CellAppearance = appearance107;
            this.uGridLot.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridLot.DisplayLayout.Override.CellPadding = 0;
            appearance108.BackColor = System.Drawing.SystemColors.Control;
            appearance108.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance108.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance108.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance108.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLot.DisplayLayout.Override.GroupByRowAppearance = appearance108;
            appearance109.TextHAlignAsString = "Left";
            this.uGridLot.DisplayLayout.Override.HeaderAppearance = appearance109;
            this.uGridLot.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridLot.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance110.BackColor = System.Drawing.SystemColors.Window;
            appearance110.BorderColor = System.Drawing.Color.Silver;
            this.uGridLot.DisplayLayout.Override.RowAppearance = appearance110;
            this.uGridLot.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance111.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridLot.DisplayLayout.Override.TemplateAddRowAppearance = appearance111;
            this.uGridLot.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridLot.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridLot.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridLot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridLot.Location = new System.Drawing.Point(3, 148);
            this.uGridLot.Name = "uGridLot";
            this.uGridLot.Size = new System.Drawing.Size(1058, 113);
            this.uGridLot.TabIndex = 77;
            this.uGridLot.Text = "ultraGrid1";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uTextLotCount);
            this.ultraGroupBox2.Controls.Add(this.uLabelLotCount);
            this.ultraGroupBox2.Controls.Add(this.uTextShipmentCount);
            this.ultraGroupBox2.Controls.Add(this.uLabelShipmentCount);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorCode);
            this.ultraGroupBox2.Controls.Add(this.uTextFloorPlanNo);
            this.ultraGroupBox2.Controls.Add(this.uTextMaterialGradeCode);
            this.ultraGroupBox2.Controls.Add(this.uTextWMSTransFlag);
            this.ultraGroupBox2.Controls.Add(this.uLabelFloorPlanNo);
            this.ultraGroupBox2.Controls.Add(this.uGroupBox2);
            this.ultraGroupBox2.Controls.Add(this.uGroupBox1);
            this.ultraGroupBox2.Controls.Add(this.uTextGRDate);
            this.ultraGroupBox2.Controls.Add(this.uLabelGRDate);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorName);
            this.ultraGroupBox2.Controls.Add(this.uLabelCustomer);
            this.ultraGroupBox2.Controls.Add(this.uTextMaterialGradeName);
            this.ultraGroupBox2.Controls.Add(this.uLabelMaterialGrade);
            this.ultraGroupBox2.Controls.Add(this.uTextSpecNo);
            this.ultraGroupBox2.Controls.Add(this.uLabelSpecNo);
            this.ultraGroupBox2.Controls.Add(this.uTextMoldSeq);
            this.ultraGroupBox2.Controls.Add(this.uLabelMoldSeq);
            this.ultraGroupBox2.Controls.Add(this.uTextGRQty);
            this.ultraGroupBox2.Controls.Add(this.uLabelGRQty);
            this.ultraGroupBox2.Controls.Add(this.uTextMaterialName);
            this.ultraGroupBox2.Controls.Add(this.uTextMaterialCode);
            this.ultraGroupBox2.Controls.Add(this.uLabelMaterialCode);
            this.ultraGroupBox2.Controls.Add(this.uTextReqNo);
            this.ultraGroupBox2.Controls.Add(this.uLabelReqNo);
            this.ultraGroupBox2.Controls.Add(this.uTextPlantName);
            this.ultraGroupBox2.Controls.Add(this.uLabelPlant);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1058, 148);
            this.ultraGroupBox2.TabIndex = 76;
            // 
            // uTextLotCount
            // 
            appearance95.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotCount.Appearance = appearance95;
            this.uTextLotCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotCount.Location = new System.Drawing.Point(660, 80);
            this.uTextLotCount.Name = "uTextLotCount";
            this.uTextLotCount.ReadOnly = true;
            this.uTextLotCount.Size = new System.Drawing.Size(110, 21);
            this.uTextLotCount.TabIndex = 115;
            // 
            // uLabelLotCount
            // 
            this.uLabelLotCount.Location = new System.Drawing.Point(544, 80);
            this.uLabelLotCount.Name = "uLabelLotCount";
            this.uLabelLotCount.Size = new System.Drawing.Size(110, 20);
            this.uLabelLotCount.TabIndex = 114;
            this.uLabelLotCount.Text = "ultraLabel1";
            // 
            // uTextShipmentCount
            // 
            appearance97.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentCount.Appearance = appearance97;
            this.uTextShipmentCount.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentCount.Location = new System.Drawing.Point(436, 80);
            this.uTextShipmentCount.Name = "uTextShipmentCount";
            this.uTextShipmentCount.ReadOnly = true;
            this.uTextShipmentCount.Size = new System.Drawing.Size(100, 21);
            this.uTextShipmentCount.TabIndex = 113;
            // 
            // uLabelShipmentCount
            // 
            this.uLabelShipmentCount.Location = new System.Drawing.Point(320, 80);
            this.uLabelShipmentCount.Name = "uLabelShipmentCount";
            this.uLabelShipmentCount.Size = new System.Drawing.Size(110, 20);
            this.uLabelShipmentCount.TabIndex = 112;
            this.uLabelShipmentCount.Text = "ultraLabel1";
            // 
            // uTextVendorCode
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorCode.Appearance = appearance40;
            this.uTextVendorCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorCode.Location = new System.Drawing.Point(660, 32);
            this.uTextVendorCode.Name = "uTextVendorCode";
            this.uTextVendorCode.ReadOnly = true;
            this.uTextVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextVendorCode.TabIndex = 111;
            // 
            // uTextFloorPlanNo
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Appearance = appearance28;
            this.uTextFloorPlanNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Location = new System.Drawing.Point(124, 56);
            this.uTextFloorPlanNo.Name = "uTextFloorPlanNo";
            this.uTextFloorPlanNo.ReadOnly = true;
            this.uTextFloorPlanNo.Size = new System.Drawing.Size(136, 21);
            this.uTextFloorPlanNo.TabIndex = 110;
            // 
            // uTextMaterialGradeCode
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGradeCode.Appearance = appearance24;
            this.uTextMaterialGradeCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGradeCode.Location = new System.Drawing.Point(272, 80);
            this.uTextMaterialGradeCode.Name = "uTextMaterialGradeCode";
            this.uTextMaterialGradeCode.ReadOnly = true;
            this.uTextMaterialGradeCode.Size = new System.Drawing.Size(21, 21);
            this.uTextMaterialGradeCode.TabIndex = 107;
            this.uTextMaterialGradeCode.Visible = false;
            // 
            // uTextWMSTransFlag
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWMSTransFlag.Appearance = appearance15;
            this.uTextWMSTransFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWMSTransFlag.Location = new System.Drawing.Point(1016, 66);
            this.uTextWMSTransFlag.Name = "uTextWMSTransFlag";
            this.uTextWMSTransFlag.ReadOnly = true;
            this.uTextWMSTransFlag.Size = new System.Drawing.Size(20, 21);
            this.uTextWMSTransFlag.TabIndex = 108;
            this.uTextWMSTransFlag.Visible = false;
            // 
            // uLabelFloorPlanNo
            // 
            this.uLabelFloorPlanNo.Location = new System.Drawing.Point(8, 56);
            this.uLabelFloorPlanNo.Name = "uLabelFloorPlanNo";
            this.uLabelFloorPlanNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelFloorPlanNo.TabIndex = 109;
            this.uLabelFloorPlanNo.Text = "ultraLabel1";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.ultraButton1);
            this.uGroupBox2.Controls.Add(this.uTextSamplingLot);
            this.uGroupBox2.Controls.Add(this.ultraLabel1);
            this.uGroupBox2.Location = new System.Drawing.Point(692, 104);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(352, 40);
            this.uGroupBox2.TabIndex = 106;
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(220, 8);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(128, 24);
            this.ultraButton1.TabIndex = 41;
            this.ultraButton1.Text = "ultraButton1";
            // 
            // uTextSamplingLot
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSamplingLot.Appearance = appearance27;
            this.uTextSamplingLot.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSamplingLot.Location = new System.Drawing.Point(136, 12);
            this.uTextSamplingLot.Name = "uTextSamplingLot";
            this.uTextSamplingLot.ReadOnly = true;
            this.uTextSamplingLot.Size = new System.Drawing.Size(80, 21);
            this.uTextSamplingLot.TabIndex = 40;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(12, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(120, 20);
            this.ultraLabel1.TabIndex = 39;
            this.ultraLabel1.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uDateTimeReceiptTime);
            this.uGroupBox1.Controls.Add(this.uOptionICPCheckFlag);
            this.uGroupBox1.Controls.Add(this.uLabelICPCheck);
            this.uGroupBox1.Controls.Add(this.uOptionInspectResultType);
            this.uGroupBox1.Controls.Add(this.uLabelInspectResult);
            this.uGroupBox1.Controls.Add(this.uDateReceiptDate);
            this.uGroupBox1.Controls.Add(this.uLabelReceiptDate);
            this.uGroupBox1.Location = new System.Drawing.Point(8, 104);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(684, 40);
            this.uGroupBox1.TabIndex = 105;
            // 
            // uDateTimeReceiptTime
            // 
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateTimeReceiptTime.ButtonsRight.Add(spinEditorButton1);
            this.uDateTimeReceiptTime.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.uDateTimeReceiptTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateTimeReceiptTime.Location = new System.Drawing.Point(196, 12);
            this.uDateTimeReceiptTime.MaskInput = "{LOC}hh:mm:ss";
            this.uDateTimeReceiptTime.Name = "uDateTimeReceiptTime";
            this.uDateTimeReceiptTime.Size = new System.Drawing.Size(90, 21);
            this.uDateTimeReceiptTime.TabIndex = 78;
            this.uDateTimeReceiptTime.Value = null;
            // 
            // uOptionICPCheckFlag
            // 
            this.uOptionICPCheckFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionICPCheckFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance29.TextVAlignAsString = "Middle";
            valueListItem3.Appearance = appearance29;
            valueListItem3.DataValue = "P";
            valueListItem3.DisplayText = "Pass";
            appearance94.TextVAlignAsString = "Middle";
            valueListItem4.Appearance = appearance94;
            valueListItem4.DataValue = "F";
            valueListItem4.DisplayText = "False";
            this.uOptionICPCheckFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.uOptionICPCheckFlag.Location = new System.Drawing.Point(380, 12);
            this.uOptionICPCheckFlag.Name = "uOptionICPCheckFlag";
            this.uOptionICPCheckFlag.Size = new System.Drawing.Size(120, 20);
            this.uOptionICPCheckFlag.TabIndex = 43;
            this.uOptionICPCheckFlag.TextIndentation = 2;
            this.uOptionICPCheckFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelICPCheck
            // 
            this.uLabelICPCheck.Location = new System.Drawing.Point(296, 12);
            this.uLabelICPCheck.Name = "uLabelICPCheck";
            this.uLabelICPCheck.Size = new System.Drawing.Size(79, 20);
            this.uLabelICPCheck.TabIndex = 42;
            this.uLabelICPCheck.Text = "ultraLabel1";
            // 
            // uOptionInspectResultType
            // 
            this.uOptionInspectResultType.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionInspectResultType.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem2.DataValue = "S";
            valueListItem2.DisplayText = "특채";
            valueListItem5.DataValue = "F";
            valueListItem5.DisplayText = "불합격";
            this.uOptionInspectResultType.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem2,
            valueListItem5});
            this.uOptionInspectResultType.Location = new System.Drawing.Point(570, 12);
            this.uOptionInspectResultType.Name = "uOptionInspectResultType";
            this.uOptionInspectResultType.Size = new System.Drawing.Size(108, 20);
            this.uOptionInspectResultType.TabIndex = 39;
            this.uOptionInspectResultType.TextIndentation = 2;
            this.uOptionInspectResultType.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionInspectResultType.Visible = false;
            // 
            // uLabelInspectResult
            // 
            this.uLabelInspectResult.Location = new System.Drawing.Point(488, 12);
            this.uLabelInspectResult.Name = "uLabelInspectResult";
            this.uLabelInspectResult.Size = new System.Drawing.Size(77, 20);
            this.uLabelInspectResult.TabIndex = 38;
            this.uLabelInspectResult.Text = "ultraLabel1";
            this.uLabelInspectResult.Visible = false;
            // 
            // uDateReceiptDate
            // 
            this.uDateReceiptDate.DateTime = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateReceiptDate.Location = new System.Drawing.Point(92, 12);
            this.uDateReceiptDate.Name = "uDateReceiptDate";
            this.uDateReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReceiptDate.TabIndex = 35;
            this.uDateReceiptDate.Value = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            // 
            // uLabelReceiptDate
            // 
            this.uLabelReceiptDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelReceiptDate.Name = "uLabelReceiptDate";
            this.uLabelReceiptDate.Size = new System.Drawing.Size(80, 20);
            this.uLabelReceiptDate.TabIndex = 34;
            this.uLabelReceiptDate.Text = "ultraLabel1";
            // 
            // uTextGRDate
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Appearance = appearance26;
            this.uTextGRDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Location = new System.Drawing.Point(436, 8);
            this.uTextGRDate.Name = "uTextGRDate";
            this.uTextGRDate.ReadOnly = true;
            this.uTextGRDate.Size = new System.Drawing.Size(100, 21);
            this.uTextGRDate.TabIndex = 104;
            // 
            // uLabelGRDate
            // 
            this.uLabelGRDate.Location = new System.Drawing.Point(320, 8);
            this.uLabelGRDate.Name = "uLabelGRDate";
            this.uLabelGRDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelGRDate.TabIndex = 103;
            this.uLabelGRDate.Text = "ultraLabel1";
            // 
            // uTextVendorName
            // 
            appearance96.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance96;
            this.uTextVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Location = new System.Drawing.Point(772, 32);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(270, 21);
            this.uTextVendorName.TabIndex = 102;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(544, 32);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(110, 20);
            this.uLabelCustomer.TabIndex = 101;
            this.uLabelCustomer.Text = "ultraLabel1";
            // 
            // uTextMaterialGradeName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGradeName.Appearance = appearance25;
            this.uTextMaterialGradeName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGradeName.Location = new System.Drawing.Point(124, 80);
            this.uTextMaterialGradeName.Name = "uTextMaterialGradeName";
            this.uTextMaterialGradeName.ReadOnly = true;
            this.uTextMaterialGradeName.Size = new System.Drawing.Size(188, 21);
            this.uTextMaterialGradeName.TabIndex = 100;
            // 
            // uLabelMaterialGrade
            // 
            this.uLabelMaterialGrade.Location = new System.Drawing.Point(8, 80);
            this.uLabelMaterialGrade.Name = "uLabelMaterialGrade";
            this.uLabelMaterialGrade.Size = new System.Drawing.Size(110, 20);
            this.uLabelMaterialGrade.TabIndex = 99;
            this.uLabelMaterialGrade.Text = "ultraLabel1";
            // 
            // uTextSpecNo
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Appearance = appearance20;
            this.uTextSpecNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Location = new System.Drawing.Point(262, 56);
            this.uTextSpecNo.Name = "uTextSpecNo";
            this.uTextSpecNo.ReadOnly = true;
            this.uTextSpecNo.Size = new System.Drawing.Size(50, 21);
            this.uTextSpecNo.TabIndex = 98;
            // 
            // uLabelSpecNo
            // 
            this.uLabelSpecNo.Location = new System.Drawing.Point(792, 80);
            this.uLabelSpecNo.Name = "uLabelSpecNo";
            this.uLabelSpecNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpecNo.TabIndex = 97;
            this.uLabelSpecNo.Text = "ultraLabel1";
            this.uLabelSpecNo.Visible = false;
            // 
            // uTextMoldSeq
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Appearance = appearance18;
            this.uTextMoldSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Location = new System.Drawing.Point(436, 56);
            this.uTextMoldSeq.Name = "uTextMoldSeq";
            this.uTextMoldSeq.ReadOnly = true;
            this.uTextMoldSeq.Size = new System.Drawing.Size(100, 21);
            this.uTextMoldSeq.TabIndex = 96;
            // 
            // uLabelMoldSeq
            // 
            this.uLabelMoldSeq.Location = new System.Drawing.Point(320, 56);
            this.uLabelMoldSeq.Name = "uLabelMoldSeq";
            this.uLabelMoldSeq.Size = new System.Drawing.Size(110, 20);
            this.uLabelMoldSeq.TabIndex = 95;
            this.uLabelMoldSeq.Text = "ultraLabel1";
            // 
            // uTextGRQty
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRQty.Appearance = appearance41;
            this.uTextGRQty.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRQty.Location = new System.Drawing.Point(436, 32);
            this.uTextGRQty.Name = "uTextGRQty";
            this.uTextGRQty.ReadOnly = true;
            this.uTextGRQty.Size = new System.Drawing.Size(100, 21);
            this.uTextGRQty.TabIndex = 94;
            // 
            // uLabelGRQty
            // 
            this.uLabelGRQty.Location = new System.Drawing.Point(320, 32);
            this.uLabelGRQty.Name = "uLabelGRQty";
            this.uLabelGRQty.Size = new System.Drawing.Size(110, 20);
            this.uLabelGRQty.TabIndex = 93;
            this.uLabelGRQty.Text = "ultraLabel1";
            // 
            // uTextMaterialName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance19;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(772, 8);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(270, 21);
            this.uTextMaterialName.TabIndex = 92;
            // 
            // uTextMaterialCode
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Appearance = appearance16;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Location = new System.Drawing.Point(660, 8);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.ReadOnly = true;
            this.uTextMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextMaterialCode.TabIndex = 91;
            // 
            // uLabelMaterialCode
            // 
            this.uLabelMaterialCode.Location = new System.Drawing.Point(544, 8);
            this.uLabelMaterialCode.Name = "uLabelMaterialCode";
            this.uLabelMaterialCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelMaterialCode.TabIndex = 90;
            this.uLabelMaterialCode.Text = "ultraLabel1";
            // 
            // uTextReqNo
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Appearance = appearance17;
            this.uTextReqNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Location = new System.Drawing.Point(124, 32);
            this.uTextReqNo.Name = "uTextReqNo";
            this.uTextReqNo.ReadOnly = true;
            this.uTextReqNo.Size = new System.Drawing.Size(188, 21);
            this.uTextReqNo.TabIndex = 89;
            // 
            // uLabelReqNo
            // 
            this.uLabelReqNo.Location = new System.Drawing.Point(8, 32);
            this.uLabelReqNo.Name = "uLabelReqNo";
            this.uLabelReqNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelReqNo.TabIndex = 88;
            this.uLabelReqNo.Text = "ultraLabel1";
            // 
            // uTextPlantName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance23;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(124, 8);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(188, 21);
            this.uTextPlantName.TabIndex = 87;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(8, 8);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 86;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uButtonExcelUpload);
            this.uGroupBox3.Controls.Add(this.uButtonMoveAbnormal);
            this.uGroupBox3.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBox3.Controls.Add(this.uDateInspectTime);
            this.uGroupBox3.Controls.Add(this.uTextEtcDescQA);
            this.uGroupBox3.Controls.Add(this.uDateAdmitDate);
            this.uGroupBox3.Controls.Add(this.uTextFaultDesc);
            this.uGroupBox3.Controls.Add(this.uTextAdmitUserName);
            this.uGroupBox3.Controls.Add(this.uLabelFaultContent);
            this.uGroupBox3.Controls.Add(this.uTextAdmitUserID);
            this.uGroupBox3.Controls.Add(this.uOptionPassFailFlag);
            this.uGroupBox3.Controls.Add(this.uLabelAcceptUser);
            this.uGroupBox3.Controls.Add(this.uLabelJudge);
            this.uGroupBox3.Controls.Add(this.uDateReviewDate);
            this.uGroupBox3.Controls.Add(this.uTextReviewUserName);
            this.uGroupBox3.Controls.Add(this.uTextReviewUserID);
            this.uGroupBox3.Controls.Add(this.uLabelReviewUser);
            this.uGroupBox3.Controls.Add(this.uDateInspectDate);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserName);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserID);
            this.uGroupBox3.Controls.Add(this.uLabelTestUser);
            this.uGroupBox3.Controls.Add(this.uLabelEtcQA);
            this.uGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBox3.Location = new System.Drawing.Point(0, 556);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1064, 84);
            this.uGroupBox3.TabIndex = 42;
            // 
            // uButtonExcelUpload
            // 
            this.uButtonExcelUpload.Location = new System.Drawing.Point(12, 55);
            this.uButtonExcelUpload.Name = "uButtonExcelUpload";
            this.uButtonExcelUpload.Size = new System.Drawing.Size(150, 28);
            this.uButtonExcelUpload.TabIndex = 87;
            this.uButtonExcelUpload.Text = "ultraButton2";
            // 
            // uButtonMoveAbnormal
            // 
            this.uButtonMoveAbnormal.Location = new System.Drawing.Point(164, 55);
            this.uButtonMoveAbnormal.Name = "uButtonMoveAbnormal";
            this.uButtonMoveAbnormal.Size = new System.Drawing.Size(150, 28);
            this.uButtonMoveAbnormal.TabIndex = 86;
            this.uButtonMoveAbnormal.Text = "ultraButton2";
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(784, 56);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(100, 20);
            this.uCheckCompleteFlag.TabIndex = 85;
            this.uCheckCompleteFlag.Text = "작성완료";
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateInspectTime
            // 
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateInspectTime.ButtonsRight.Add(spinEditorButton2);
            this.uDateInspectTime.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.uDateInspectTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateInspectTime.Location = new System.Drawing.Point(428, 8);
            this.uDateInspectTime.MaskInput = "{LOC}hh:mm:ss";
            this.uDateInspectTime.Name = "uDateInspectTime";
            this.uDateInspectTime.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectTime.TabIndex = 77;
            this.uDateInspectTime.Value = null;
            this.uDateInspectTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uDateInspectTime_EditorSpinButtonClick);
            // 
            // uTextEtcDescQA
            // 
            this.uTextEtcDescQA.Location = new System.Drawing.Point(640, 32);
            this.uTextEtcDescQA.Multiline = true;
            this.uTextEtcDescQA.Name = "uTextEtcDescQA";
            this.uTextEtcDescQA.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.uTextEtcDescQA.Size = new System.Drawing.Size(380, 20);
            this.uTextEtcDescQA.TabIndex = 76;
            // 
            // uDateAdmitDate
            // 
            this.uDateAdmitDate.DateTime = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateAdmitDate.Location = new System.Drawing.Point(324, 32);
            this.uDateAdmitDate.Name = "uDateAdmitDate";
            this.uDateAdmitDate.Size = new System.Drawing.Size(100, 21);
            this.uDateAdmitDate.TabIndex = 75;
            this.uDateAdmitDate.Value = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            // 
            // uTextFaultDesc
            // 
            this.uTextFaultDesc.Location = new System.Drawing.Point(640, 7);
            this.uTextFaultDesc.Name = "uTextFaultDesc";
            this.uTextFaultDesc.Size = new System.Drawing.Size(380, 21);
            this.uTextFaultDesc.TabIndex = 41;
            // 
            // uTextAdmitUserName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserName.Appearance = appearance21;
            this.uTextAdmitUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserName.Location = new System.Drawing.Point(218, 32);
            this.uTextAdmitUserName.Name = "uTextAdmitUserName";
            this.uTextAdmitUserName.ReadOnly = true;
            this.uTextAdmitUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitUserName.TabIndex = 73;
            // 
            // uLabelFaultContent
            // 
            this.uLabelFaultContent.Location = new System.Drawing.Point(536, 7);
            this.uLabelFaultContent.Name = "uLabelFaultContent";
            this.uLabelFaultContent.Size = new System.Drawing.Size(100, 20);
            this.uLabelFaultContent.TabIndex = 40;
            this.uLabelFaultContent.Text = "ultraLabel1";
            // 
            // uTextAdmitUserID
            // 
            appearance22.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance22;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAdmitUserID.ButtonsRight.Add(editorButton3);
            this.uTextAdmitUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAdmitUserID.Location = new System.Drawing.Point(116, 32);
            this.uTextAdmitUserID.Name = "uTextAdmitUserID";
            this.uTextAdmitUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitUserID.TabIndex = 72;
            // 
            // uOptionPassFailFlag
            // 
            this.uOptionPassFailFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionPassFailFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem6.DataValue = "OK";
            valueListItem6.DisplayText = "합격";
            valueListItem7.DataValue = "NG";
            valueListItem7.DisplayText = "불합격";
            this.uOptionPassFailFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem6,
            valueListItem7});
            this.uOptionPassFailFlag.Location = new System.Drawing.Point(640, 56);
            this.uOptionPassFailFlag.Name = "uOptionPassFailFlag";
            this.uOptionPassFailFlag.Size = new System.Drawing.Size(140, 20);
            this.uOptionPassFailFlag.TabIndex = 37;
            this.uOptionPassFailFlag.TextIndentation = 2;
            this.uOptionPassFailFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelAcceptUser
            // 
            this.uLabelAcceptUser.Location = new System.Drawing.Point(12, 32);
            this.uLabelAcceptUser.Name = "uLabelAcceptUser";
            this.uLabelAcceptUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelAcceptUser.TabIndex = 71;
            this.uLabelAcceptUser.Text = "ultraLabel2";
            // 
            // uLabelJudge
            // 
            this.uLabelJudge.Location = new System.Drawing.Point(536, 56);
            this.uLabelJudge.Name = "uLabelJudge";
            this.uLabelJudge.Size = new System.Drawing.Size(100, 20);
            this.uLabelJudge.TabIndex = 36;
            this.uLabelJudge.Text = "ultraLabel1";
            // 
            // uDateReviewDate
            // 
            this.uDateReviewDate.DateTime = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateReviewDate.Location = new System.Drawing.Point(264, 60);
            this.uDateReviewDate.Name = "uDateReviewDate";
            this.uDateReviewDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReviewDate.TabIndex = 70;
            this.uDateReviewDate.Value = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateReviewDate.Visible = false;
            // 
            // uTextReviewUserName
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReviewUserName.Appearance = appearance38;
            this.uTextReviewUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReviewUserName.Location = new System.Drawing.Point(248, 60);
            this.uTextReviewUserName.Name = "uTextReviewUserName";
            this.uTextReviewUserName.ReadOnly = true;
            this.uTextReviewUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextReviewUserName.TabIndex = 68;
            this.uTextReviewUserName.Visible = false;
            // 
            // uTextReviewUserID
            // 
            appearance37.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance37.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance37;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReviewUserID.ButtonsRight.Add(editorButton4);
            this.uTextReviewUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReviewUserID.Location = new System.Drawing.Point(232, 60);
            this.uTextReviewUserID.Name = "uTextReviewUserID";
            this.uTextReviewUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextReviewUserID.TabIndex = 67;
            this.uTextReviewUserID.Visible = false;
            this.uTextReviewUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReviewUserID_KeyDown);
            this.uTextReviewUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReviewUserID_EditorButtonClick);
            // 
            // uLabelReviewUser
            // 
            this.uLabelReviewUser.Location = new System.Drawing.Point(216, 64);
            this.uLabelReviewUser.Name = "uLabelReviewUser";
            this.uLabelReviewUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelReviewUser.TabIndex = 66;
            this.uLabelReviewUser.Text = "ultraLabel2";
            this.uLabelReviewUser.Visible = false;
            // 
            // uDateInspectDate
            // 
            this.uDateInspectDate.DateTime = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateInspectDate.Location = new System.Drawing.Point(324, 8);
            this.uDateInspectDate.Name = "uDateInspectDate";
            this.uDateInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDate.TabIndex = 65;
            this.uDateInspectDate.Value = new System.DateTime(2015, 4, 7, 0, 0, 0, 0);
            this.uDateInspectDate.ValueChanged += new System.EventHandler(this.uDateInspectDate_ValueChanged);
            // 
            // uTextInspectUserName
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance39;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(218, 8);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserName.TabIndex = 63;
            // 
            // uTextInspectUserID
            // 
            appearance36.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance36;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton5);
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(116, 8);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserID.TabIndex = 62;
            // 
            // uLabelTestUser
            // 
            this.uLabelTestUser.Location = new System.Drawing.Point(12, 8);
            this.uLabelTestUser.Name = "uLabelTestUser";
            this.uLabelTestUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelTestUser.TabIndex = 61;
            this.uLabelTestUser.Text = "ultraLabel2";
            // 
            // uLabelEtcQA
            // 
            this.uLabelEtcQA.Location = new System.Drawing.Point(536, 32);
            this.uLabelEtcQA.Name = "uLabelEtcQA";
            this.uLabelEtcQA.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtcQA.TabIndex = 60;
            this.uLabelEtcQA.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridInspectList
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInspectList.DisplayLayout.Appearance = appearance2;
            this.uGridInspectList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInspectList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInspectList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInspectList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridInspectList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInspectList.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGridInspectList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInspectList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInspectList.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInspectList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridInspectList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInspectList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInspectList.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            appearance10.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInspectList.DisplayLayout.Override.CellAppearance = appearance10;
            this.uGridInspectList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInspectList.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInspectList.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance12.TextHAlignAsString = "Left";
            this.uGridInspectList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridInspectList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInspectList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGridInspectList.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGridInspectList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInspectList.DisplayLayout.Override.TemplateAddRowAppearance = appearance14;
            this.uGridInspectList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInspectList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInspectList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInspectList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridInspectList.Location = new System.Drawing.Point(0, 100);
            this.uGridInspectList.Name = "uGridInspectList";
            this.uGridInspectList.Size = new System.Drawing.Size(1070, 90);
            this.uGridInspectList.TabIndex = 5;
            this.uGridInspectList.Text = "ultraGrid1";
            // 
            // frmINS0004
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridInspectList);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINS0004";
            this.Load += new System.EventHandler(this.frmINS0004_Load);
            this.Activated += new System.EventHandler(this.frmINS0004_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINS0004_FormClosing);
            this.Resize += new System.EventHandler(this.frmINS0004_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridReqItem)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridReqSampling)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectResultFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGradeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWMSTransFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSamplingLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeReceiptTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionICPCheckFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionInspectResultType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGradeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDescQA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAdmitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReviewDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReviewUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReviewUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInspectList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToInspectDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelFaultContent;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionPassFailFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelJudge;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcQA;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAdmitDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReviewDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReviewUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReviewUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelReviewUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelTestUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDescQA;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectTime;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectResultFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectResultFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialGroup;
        private Infragistics.Win.Misc.UltraButton uButtonMoveAbnormal;
        private Infragistics.Win.Misc.UltraButton uButtonExcelUpload;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInspectList;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridReqItem;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridReqSampling;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridLot;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotCount;
        private Infragistics.Win.Misc.UltraLabel uLabelLotCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShipmentCount;
        private Infragistics.Win.Misc.UltraLabel uLabelShipmentCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFloorPlanNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialGradeCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWMSTransFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelFloorPlanNo;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSamplingLot;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeReceiptTime;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionICPCheckFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelICPCheck;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionInspectResultType;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectResult;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRDate;
        private Infragistics.Win.Misc.UltraLabel uLabelGRDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialGradeName;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialGrade;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRQty;
        private Infragistics.Win.Misc.UltraLabel uLabelGRQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqNo;
        private Infragistics.Win.Misc.UltraLabel uLabelReqNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid5;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
    }
}