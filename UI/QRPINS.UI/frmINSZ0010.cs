﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0010.cs                                        */
/* 프로그램명   : QCN 발행                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-10-05 : MES 추가 (권종구)                        */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPINS.UI
{
    public partial class frmINSZ0010 : Form, IToolbar
    {
      
        #region 전역변수
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";

        // 화면에서 넘겨받을 변수 속성
        private String m_strPlantCode;
        private String m_strLotNo;
        private string m_strReqNo;
        private string m_strReqSeq;
        private int m_intReqLotSeq;
        private int m_intReqItemSeq;
        private int m_intReqInspectSeq;
        private string m_strFormName;

        public String PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public String LotNo
        {
            get { return m_strLotNo; }
            set { m_strLotNo = value; }
        }        

        public string ReqNo
        {
            get { return m_strReqNo; }
            set { m_strReqNo = value; }
        }       

        public string ReqSeq
        {
            get { return m_strReqSeq; }
            set { m_strReqSeq = value; }
        }

        public int ReqLotSeq
        {
            get { return m_intReqLotSeq; }
            set { m_intReqLotSeq = value; }
        }

        public int ReqItemSeq
        {
            get { return m_intReqItemSeq; }
            set { m_intReqItemSeq = value; }
        }

        public int ReqInspectSeq
        {
            get { return m_intReqInspectSeq; }
            set { m_intReqInspectSeq = value; }
        }

        public string FormName
        {
            get { return m_strFormName; }
            set { m_strFormName = value; }
        }

        #endregion

        public frmINSZ0010()
        {
            InitializeComponent();
        }

        private void frmINSZ0010_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, false, true, true, true, true, true, m_resSys.GetString("SYS_USERID"), this.Name);

            if(FormName == null || FormName.Equals(string.Empty))
                mfSearch();

        }

        private void frmINSZ0010_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("QCN 발행", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            InitGroupBox();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitEtc();

            if (FormName.StartsWith("frmINS0008"))
            {
                QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
                brw.mfSetFormLanguage(this);

                string strLang = m_SysRes.GetString("SYS_LANG");
                InitQCN_MoveFromINSProc(strLang);
            }
            else
            {
                //디버깅모드
                SetRunMode();
                //this.uGroupBoxContentsArea.Expanded = false;
            }

            SetToolAuth();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 공정검사에서 넘어온경우 디비에서 정보 읽어와서 Display하는 메소드
        /// </summary>
        private void InitQCN_MoveFromINSProc(string strLang)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uComboPlantCode.Value = PlantCode;
                this.uTextLotNo.Text = LotNo;
                this.uTextReqNo.Text = ReqNo;
                this.uTextReqSeq.Text = ReqSeq;

                // Key Down 이벤트 발생
                System.Windows.Forms.KeyEventArgs aa = new KeyEventArgs(Keys.Enter);
                this.uTextLotNo_KeyDown(this.uTextLotNo, aa);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                brwChannel.mfCredentials(clsFault);

                DataTable dtInit = clsFault.mfReadINSProcInspectResultFault_InitQCN(PlantCode, ReqNo, ReqSeq, ReqLotSeq, ReqItemSeq, ReqInspectSeq, strLang);

                if (dtInit.Rows.Count > 0)
                {
                    //this.uComboPlantCode.Value = dtInit.Rows[0]["PlantCode"].ToString();
                    //this.uTextLotNo.Text = dtInit.Rows[0]["LotNo"].ToString();
                    //this.uTextCustomerName.Text = dtInit.Rows[0]["CustomerName"].ToString();
                    //this.uTextProductCode.Text = dtInit.Rows[0]["ProductCode"].ToString();
                    //this.uTextProductName.Text = dtInit.Rows[0]["ProductName"].ToString();
                    //this.uTextCustomerProductSpec.Text = dtInit.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                    //this.uComboAriseProcessCode.Value = dtInit.Rows[0]["ProcessCode"].ToString();
                    //this.uComboAriseEquipCode.Value = dtInit.Rows[0]["EquipCode"].ToString();
                    //this.uNumQty.Value = Convert.ToInt32(dtInit.Rows[0]["LotSize"]);
                    //this.uTextPackage.Text = dtInit.Rows[0]["Package"].ToString();
                    this.uNumInspectQty.Value = Convert.ToInt32(dtInit.Rows[0]["InspectQty"]);
                    this.uNumFaultQty.Value = Convert.ToInt32(dtInit.Rows[0]["FaultQty"]);
                    this.uComboInspectFaultTypeCode.Value = dtInit.Rows[0]["FaultTypeCode"].ToString();
                }

                // TrackIn 공정인경우 검사자 공란으로
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                brwChannel.mfCredentials(clsHeader);

                // TrackIn 여부 조회
                DataTable dtCheck = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlantCode.Value.ToString(), this.uComboAriseEquipCode.Value.ToString(), m_resSys.GetString("SYS_FONTNAME"));

                // 저장
                // 저장할 정보를 데이터 테이블로 받는다
                DataTable dtHeader = Rtn_HeaderInfoByDataTable();
                DataTable dtAffectLot = Rtn_AffectLotInfoByDataTable();
                DataTable dtEquipList = Rtn_ExpectEquipList();
                DataTable dtQCNUser = new DataTable();

                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQCN);

                // 메소드 호출
                string strErrRtn = clsQCN.mfSaveINSProcQCN(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtAffectLot, dtEquipList, dtQCNUser, this.Name);

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리결과에 따른 메세지 박스
                if (!ErrRtn.ErrNum.Equals(0))
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001053", "M001035", Infragistics.Win.HAlign.Center);
                }
                else
                {
                    string strQCNNo = ErrRtn.mfGetReturnValue(0);

                    Search_HeaderData(PlantCode, strQCNNo, m_resSys.GetString("SYS_LANG"));
                    Search_AffectLotData(PlantCode, strQCNNo, m_resSys.GetString("SYS_LANG"));
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINSZ0010_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 디버깅 Method
        /// </summary>
        private void SetRunMode()
        {
            if (this.Tag != null)
            {
                //MessageBox.Show(this.Tag.ToString());
                string[] sep = { "|" };
                string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                if (arrArg.Count() > 2)
                {
                    if (arrArg[1].ToString().ToUpper() == "DEBUG")
                    {
                        m_bolDebugMode = true;
                        if (arrArg.Count() > 3)
                            m_strDBConn = arrArg[2].ToString();
                    }

                }
                //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.

            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 그룹박스 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "Affect Lot 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "조치사항", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupQCNUser, GroupBoxType.LIST, "E-mail 송부", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupQCNUser.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupQCNUser.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchAriseDate, "발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchQCNNo, "QCNNo", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelQCNNo, "QCNNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCustomerName, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerProductSpec, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseProcess, "발생공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseEquip, "발생설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelExpectProcess, "예상공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseDate, "발행일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelQty, "수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectQty, "검사수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //wLabel.mfSetLabel(this.uLabelFaultQty, "불량수량", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelInspectFaultType, "불량유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFaultImg, "불량Image", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWorkStep, "작업Step", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPublishFlag, "발행완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);


                wLabel.mfSetLabel(this.uLabelWorkUser, "작업자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser, "검사자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinComboGrid combo = new WinComboGrid();

                //// 발생공정 콤보
                combo.mfInitGeneralComboGrid(this.uComboAriseProcessCode, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboAriseProcessCode.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;
                this.uComboAriseProcessCode.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;

                combo.mfSetComboGridColumn(this.uComboAriseProcessCode, 0, "ProcessCode", "발생공정코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                combo.mfSetComboGridColumn(this.uComboAriseProcessCode, 0, "ProcessName", "발생공정이름", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");
                //this.uComboAriseProcessCode.Text = "선택";

                ////예상공정 콤보
                combo.mfInitGeneralComboGrid(this.uComboExpectProcessCode, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboExpectProcessCode.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;
                this.uComboExpectProcessCode.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;

                combo.mfSetComboGridColumn(this.uComboExpectProcessCode, 0, "ProcessCode", "발생공정코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                combo.mfSetComboGridColumn(this.uComboExpectProcessCode, 0, "ProcessName", "발생공정이름", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");
                //this.uComboExpectProcessCode.Text = "선택";


                //발생설비 콤보
                combo.mfInitGeneralComboGrid(this.uComboAriseEquipCode, false, false, true, true, "EquipCode", "EquipCode", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboAriseEquipCode.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;
                this.uComboAriseEquipCode.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;

                combo.mfSetComboGridColumn(this.uComboAriseEquipCode, 0, "EquipCode", "발생설비코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                combo.mfSetComboGridColumn(this.uComboAriseEquipCode, 0, "EquipName", "발생설비명", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");
                //this.uComboAriseEquipCode.Text = "선택";

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 검색조건
                wCombo.mfSetComboEditor(this.uComboSearchPlantCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // 입력
                wCombo.mfSetComboEditor(this.uComboPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 그리드

                // 헤더 그리드 일반설정
                wGrid.mfInitGeneralGrid(this.uGridQCNList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "QCNNo", "QCNNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseProcessCode", "발생공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseProcessName", "발생공정명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseEquipCode", "발생설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "InspectFaultTypeName", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "PublishFlagCode", "발행완료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "PublishFlag", "발행완료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "MESHoldTFlag", "MESHold요청여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region AffectLot List

                // Affect Lot 그리드 일반설정
                wGrid.mfInitGeneralGrid(this.uGridAffectLotList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "LotType", "Lot구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "Qty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "WorkDateTime", "TrackIn일시", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "WorkUserID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "WorkUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "MESHoldTFlag", "Hold 여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");

                #endregion

                #region 예상설비 List

                // 설비 그리드
                wGrid.mfInitGeneralGrid(this.uGridExpectEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "CheckFlag", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "EQPID", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "USERID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "USERNAME", "작업자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region E-Mail 송부 List

                // 수신인 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridQCNUser, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                ////wGrid.mfSetGridColumn(this.uGridQCNUser, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridQCNUser, 0, "UserID", "수신자 사번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNUser, 0, "UserName", "수신자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNUser, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNUser, 0, "EMail", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNUser, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                // Affect Lot정보 Lot구분 콤보 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtCom = clsComCode.mfReadCommonCode("C0044", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridAffectLotList, 0, "LotType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                // Set PontSize
                this.uGridQCNList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridQCNList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridAffectLotList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridAffectLotList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridExpectEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridExpectEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridQCNUser.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridQCNUser.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 공백줄추가
                wGrid.mfAddRowGrid(this.uGridAffectLotList, 0);
                wGrid.mfAddRowGrid(this.uGridQCNUser, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 콤보그리드 초기화
        ///// <summary>
        ///// 발생공정 콤보 초기화
        ///// </summary>
        //private void InituComboAriseProcessCode()
        //{
        //    try
        //    {
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //        String strPlantCode = this.uComboPlantCode.Value.ToString();

        //        QRPBrowser brwChannel = new QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
        //        QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
        //        brwChannel.mfCredentials(clsProcess);

        //        DataTable dtProc = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

        //        this.uComboAriseProcessCode.DataSource = dtProc;
        //        this.uComboAriseProcessCode.DataBind();

        //        if (!(dtProc.Rows.Count > 0))
        //        {
        //            WinMessageBox msg = new WinMessageBox();
        //            DialogResult Result = new DialogResult();

        //            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        //            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
        //            , "공정이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { }
        //}
        ///// <summary>
        ///// 예상공정 콤보 초기화
        ///// </summary>
        //private void InituComboExpectProcessCode()
        //{
        //    try
        //    {
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //        String strPlantCode = this.uComboPlantCode.Value.ToString();

        //        QRPBrowser brwChannel = new QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
        //        QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
        //        brwChannel.mfCredentials(clsProcess);

        //        DataTable dtProc = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

        //        this.uComboExpectProcessCode.DataSource = dtProc;
        //        this.uComboExpectProcessCode.DataBind();

        //        if (!(dtProc.Rows.Count > 0))
        //        {
        //            WinMessageBox msg = new WinMessageBox();
        //            DialogResult Result = new DialogResult();

        //            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        //            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
        //            , "공정이 존재하지 않습니다.", Infragistics.Win.HAlign.Center);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { }

        //}
        ///// <summary>
        ///// 발생설비 콤보 초기화
        ///// </summary>
        //private void InitComboAriseEquipCode()
        //{
        //    try
        //    {
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //        String strPlantCode = this.uComboPlantCode.Value.ToString();

        //        QRPBrowser brwChannel = new QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        //        QRPMAS.BL.MASEQU.Equip clsEqu = new QRPMAS.BL.MASEQU.Equip();
        //        brwChannel.mfCredentials(clsEqu);

        //        DataTable dtEqu = clsEqu.mfReadEquipForPlantCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

        //        this.uComboAriseEquipCode.DataSource = dtEqu;
        //        this.uComboAriseEquipCode.DataBind();

        //        //if (!(dtEqu.Rows.Count > 0))
        //        //{
        //        //    WinMessageBox msg = new WinMessageBox();
        //        //    DialogResult Result = new DialogResult();

        //        //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
        //        //                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
        //        //                , "장비가 존재하지 않습니다.", Infragistics.Win.HAlign.Center);
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { }
        //}
        ///// <summary>
        ///// 예상설비 콤보 초기화
        ///// </summary>
        //private void InitComboExpectEquipCode()
        //{
        //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //    String strPlantCode = this.uComboPlantCode.Value.ToString();

        //    QRPBrowser brwChannel = new QRPBrowser();
        //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        //    QRPMAS.BL.MASEQU.Equip clsEqu = new QRPMAS.BL.MASEQU.Equip();
        //    brwChannel.mfCredentials(clsEqu);

        //    DataTable dtEqu = clsEqu.mfReadEquipForPlantCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

        //    this.uComboExpectEquipCode.DataSource = dtEqu;
        //    this.uComboExpectEquipCode.DataBind();

        //    //if (!(dtEqu.Rows.Count > 0))
        //    //{
        //    //    WinMessageBox msg = new WinMessageBox();
        //    //    DialogResult Result = new DialogResult();

        //    //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        //    //                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
        //    //                , "장비가 존재하지 않습니다.", Infragistics.Win.HAlign.Center);
        //    //}

        //}
        ///// <summary>
        ///// Multi설비 콤보
        ///// </summary>
        //private void InitMultiEquipCode()
        //{
        //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //    String strPlantCode = this.uComboPlantCode.Value.ToString();

        //    QRPBrowser brwChannel = new QRPBrowser();
        //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        //    QRPMAS.BL.MASEQU.Equip clsEqu = new QRPMAS.BL.MASEQU.Equip();
        //    brwChannel.mfCredentials(clsEqu);

        //    DataTable dtEqu = clsEqu.mfReadEquipForPlantCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

        //    this.uComboMultiEquipCode.DataSource = dtEqu;
        //    this.uComboMultiEquipCode.DataBind();

        //    //if (!(dtEqu.Rows.Count > 0))
        //    //{
        //    //    WinMessageBox msg = new WinMessageBox();
        //    //    DialogResult Result = new DialogResult();

        //    //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        //    //                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "조회결과", "조회결과 확인"
        //    //                , "장비가 존재하지 않습니다.", Infragistics.Win.HAlign.Center);
        //    //}

        //}
        #endregion

        /// <summary>
        /// 그밖의 초기화들...
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // TextBox MaxLength 지정
                this.uTextLotNo.MaxLength = 50;
                this.uTextWorkUserID.MaxLength = 20;
                this.uTextInspectUserID.MaxLength = 20;
                this.uTextEtcDesc.MaxLength = 1000;

                this.uTextInspectUserID.ImeMode = ImeMode.Disable;
                this.uTextInspectUserID.CharacterCasing = CharacterCasing.Upper;

                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");

                this.uTextLotNo.ImeMode = ImeMode.Disable;
                this.uTextLotNo.CharacterCasing = CharacterCasing.Upper;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 검색조건 변수 설정
                //string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                //string strProductCode = this.uTextProductCode.Text;
                //string strProcessCode = this.uComboSearchProcessCode.Value.ToString();
                //string strQCNNo = this.uTextSearchQCNNo.Text;
                //string strLotNo = this.uTextSearchLotNo.Text;
                //string strAriseDateFrom = Convert.ToDateTime(this.uDateSearchAriseDateFrom.Value).ToString("yyyy-MM-dd");
                //string strAriseDateTo = Convert.ToDateTime(this.uDateSerachAriseDateTo.Value).ToString("yyyy-MM-dd");

                QRPBrowser brwChannel = new QRPBrowser();
                QRPINS.BL.INSPRC.ProcQCN clsQCN;
                if (m_bolDebugMode == false)
                {
                    // BL연결
                    
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                    clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                    brwChannel.mfCredentials(clsQCN);
                }
                else
                {
                    clsQCN = new QRPINS.BL.INSPRC.ProcQCN(m_strDBConn);
                }

                //// 프로그래스바 생성
                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread threadPop = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                // 메소드 호출
                //DataTable dtSearch = clsQCN.mfReadInsProcQCN(strPlantCode, strProcessCode, strQCNNo, strLotNo, strProductCode, strAriseDateFrom, strAriseDateTo, m_resSys.GetString("SYS_LANG"));
                DataTable dtSearch = clsQCN.mfReadInsProcQCN("", "", "", "", "", "", "", m_resSys.GetString("SYS_LANG"));

                this.uGridQCNList.DataSource = dtSearch;
                this.uGridQCNList.DataBind();

                //// POPUP창 Close
                //this.MdiParent.Cursor = Cursors.Default;
                //m_ProgressPopup.mfCloseProgressPopup(this);

                //// 조회결과 없을시 MessageBox 로 알림
                //if (dtSearch.Rows.Count == 0)
                //{
                //    WinMessageBox msg = new WinMessageBox();
                //    DialogResult Result = new DialogResult();

                //    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
                //}
                //else
                //{
                //    //발행완료가 T 이고 MESHoldTFlag 가 F 인 줄의 색을 변환 한다.
                //    for (int i = 0; i < this.uGridQCNList.Rows.Count; i++)
                //    {
                //        if (this.uGridQCNList.Rows[i].Cells["PublishFlagCode"].Value.ToString() == "T" && this.uGridQCNList.Rows[i].Cells["MESHoldTFlag"].Value.ToString() == "F")
                //        {
                //            this.uGridQCNList.Rows[i].Appearance.BackColor = Color.Salmon;
                //        }

                //    }
                //}
                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001041", Infragistics.Win.HAlign.Center);
                    return;
                }
                else
                {
                    // 저장할 정보를 데이터 테이블로 받는다
                    DataTable dtHeader = Rtn_HeaderInfoByDataTable();

                    // AffectLot 의 MES I/F여부 확인
                    bool bolAffectLotMESCheck = true;
                    for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                    {
                        if (!this.uGridAffectLotList.Rows[i].Cells["MESHoldTFlag"].Value.ToString().Equals("T"))
                        {
                            bolAffectLotMESCheck = false;
                            break;
                        }
                    }

                    // 필수입력사항 확인
                    if (CheckRequire())
                    {
                        if (dtHeader.Rows[0]["PublicshFlag"].ToString().Equals("True") && dtHeader.Rows[0]["MESHoldTFlag"].ToString().Equals("T") && bolAffectLotMESCheck)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                   , "M001264", "M001137", "M000438", Infragistics.Win.HAlign.Center);

                            return;
                        }

                        if (this.uCheckPublishFlag.Enabled == true && this.uCheckPublishFlag.Checked == true)
                        {

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000437", "M000436",
                                            Infragistics.Win.HAlign.Right);
                        }

                        DialogResult dirResult = new DialogResult();

                        if (dtHeader.Rows[0]["PublicshComplete"].ToString().Equals("T") && !dtHeader.Rows[0]["MESHoldTFlag"].ToString().Equals("T") && !bolAffectLotMESCheck)
                        {
                            dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000439", Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                            if (!check.mfCheckValidValueBeforSave(this)) return;
                           dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right);
                        }
                        // 저장여부를 묻는다
                        if (dirResult == DialogResult.Yes)
                        {
                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            QRPINS.BL.INSPRC.ProcQCN clsQCN;
                            if (m_bolDebugMode == false)
                            {
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                                clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                                brwChannel.mfCredentials(clsQCN);
                            }
                            else
                            {
                                clsQCN = new QRPINS.BL.INSPRC.ProcQCN(m_strDBConn);
                            }

                            // 저장할 정보를 데이터 테이블로 받는다
                            DataTable dtAffectLot = Rtn_AffectLotInfoByDataTable();
                            DataTable dtExpectEquipList = Rtn_ExpectEquipList();
                            DataTable dtQCNUser = Rtn_QCNUser();

                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // 메소드 호출
                            string strErrRtn = clsQCN.mfSaveINSProcQCN(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtAffectLot, dtExpectEquipList, dtQCNUser, this.Name);

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // 처리결과에 따른 메세지 박스
                            if (ErrRtn.ErrNum == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000954",
                                                    Infragistics.Win.HAlign.Right);
                                
                                string strQCNNo = ErrRtn.mfGetReturnValue(0);
                                bool bolSaveCheck = true;

                                // 첨부파일 전송 메소드 호출
                                FileUpload(strQCNNo);

                                // Mail전송 메소드 호출
                                SendMail(strQCNNo);

                                // 발행완료가 체크되어있고, MESHoldTFlag가 F 일경우 MES처리 정보
                                if (this.uCheckPublishFlag.Checked && !this.uTextMESTFlag.Text.Equals("T"))
                                {
                                    // Lot 상태가 Hold상태가 아닐때 헤더 Hold 요청
                                    if (this.uTextLotHoldState.Text.Equals("NotOnHold"))
                                    {
                                        // LotLIst DataTable 설정
                                        DataTable dtLotList = new DataTable();
                                        dtLotList.Columns.Add("LOTID", typeof(string));
                                        DataRow drRow = dtLotList.NewRow();
                                        drRow["LOTID"] = this.uTextLotNo.Text;
                                        dtLotList.Rows.Add(drRow);

                                        // Hold 코드 가져오기
                                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                                        QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                                        brwChannel.mfCredentials(clsReason);

                                        DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

                                        string strHoldCode = string.Empty;
                                        if (dtReason.Rows.Count > 0)
                                            strHoldCode = dtReason.Rows[0]["ReasonCode"].ToString();

                                        strErrRtn = clsQCN.mfSaveINSProcQCN_MESHold(this.Name
                                                                                , this.uTextInspectUserID.Text
                                                                                , this.uTextEtcDesc.Text
                                                                                , strHoldCode
                                                                                , dtLotList
                                                                                , this.uComboPlantCode.Value.ToString()
                                                                                , strQCNNo
                                                                                , m_resSys.GetString("SYS_USERID")
                                                                                , m_resSys.GetString("SYS_USERIP"));

                                        // 결과검사
                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                        if (ErrRtn.ErrNum.Equals(0) && ErrRtn.InterfaceResultCode.Equals("0"))
                                        {
                                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000929", Infragistics.Win.HAlign.Right);
                                        }
                                        else
                                        {
                                            bolSaveCheck = false;
                                            if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                            {
                                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000095", "M000951", Infragistics.Win.HAlign.Right);
                                            }
                                            else
                                            {
                                                string strLang = m_resSys.GetString("SYS_LANG");
                                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                                    , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                            }
                                        }
                                    }
                                }
                                // AffectLot MES
                                if (this.uCheckPublishFlag.Checked && !bolAffectLotMESCheck)
                                {
                                    // 이미 MES Hold 요청한것은 삭제
                                    for (int i = 0; i < dtAffectLot.Rows.Count; i++)
                                    {
                                        if (dtAffectLot.Rows[i]["MESHoldTFlag"].ToString().Equals("T"))
                                            dtAffectLot.Rows[i].Delete();
                                    }
                                    // Affect Lot 정보가 있는경우 Hold 요청
                                    if (dtAffectLot.Rows.Count > 0)
                                    {
                                        DataTable dtLotList = dtAffectLot.DefaultView.ToTable(true, "LotNo");
                                        dtLotList.Columns["LotNo"].ColumnName = "LOTID";

                                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNAffectLot), "ProcQCNAffectLot");
                                        QRPINS.BL.INSPRC.ProcQCNAffectLot clsAffectLot = new QRPINS.BL.INSPRC.ProcQCNAffectLot();
                                        brwChannel.mfCredentials(clsAffectLot);

                                        strErrRtn = clsAffectLot.mfSaveINSProcQCN_MESHold_AffectLot(this.Name
                                                                                , this.uTextInspectUserID.Text
                                                                                , this.uTextEtcDesc.Text
                                                                                , "HQ11"                                // AffectLot Hold 코드 하드코딩
                                                                                , dtLotList
                                                                                , this.uComboPlantCode.Value.ToString()
                                                                                , strQCNNo
                                                                                , m_resSys.GetString("SYS_USERID")
                                                                                , m_resSys.GetString("SYS_USERIP"));

                                        // 결과검사
                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                        if (ErrRtn.ErrNum.Equals(0) && ErrRtn.InterfaceResultCode.Equals("0"))
                                        {
                                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000928", Infragistics.Win.HAlign.Right);
                                        }
                                        else
                                        {
                                            bolSaveCheck = false;
                                            if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                            {
                                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000095", "M000945", Infragistics.Win.HAlign.Right);
                                            }
                                            else
                                            {
                                                string strLang = m_resSys.GetString("SYS_LANG");
                                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                                    , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                            }
                                        }
                                    }
                                }

                                if (bolSaveCheck)
                                {
                                    //Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    //                   "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장하였습니다.", Infragistics.Win.HAlign.Right);
                                    // 리스트 갱신
                                    mfSearch();
                                }

                                // AffectLot 정보가 있을경우 Lot Hold 요청


                                //// 발행완료가 체크되어있고, MESHoldTFlag가 F 일경우 MES처리 정보
                                //if (dtHeader.Rows[0]["PublicshFlag"].ToString().Equals("True") && dtHeader.Rows[0]["MESHoldTFlag"].ToString().Equals("F"))
                                //{
                                //    //MES 처리정보가 실패면
                                //    if (ErrRtn.InterfaceResultCode != "0")
                                //    {
                                //        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                //        {
                                //            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
                                //        }
                                //        else
                                //        {
                                //            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                //        }
                                //    }
                                //    else
                                //    {
                                //         Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                       "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.", Infragistics.Win.HAlign.Right);
                                //    }
                                //}
                                //else
                                //{
                                //    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                //                    "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.", Infragistics.Win.HAlign.Right);

                                //}

                                // 리스트 갱신
                                //mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextQCNNo.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                        QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                        brwChannel.mfCredentials(clsQCN);

                        // 매개변수로 전달할 변수 설정
                        string strPlantCode = this.uComboPlantCode.Value.ToString();
                        string strQCNNo = this.uTextQCNNo.Text;

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsQCN.mfDeleteINSProcQCN(strPlantCode, strQCNNo);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);
                            // 첨부파일 삭제 메소드
                            FileDelete();

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000923",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                    InitComboBox();
                }
                this.uTextLotNo.Focus();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridQCNList.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridQCNList);
                }

                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 메소드..
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;

            }
            finally
            {
                dtUser.Dispose();
            }
        }

        /// <summary>
        /// NumericEditor 왼쪽버튼(0) 클릭시 Value 값 0으로 만드는 Method
        /// </summary>
        /// <param name="sender"></param>
        private void SetNumericEditorZeroButton(object sender)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// NumericEditor SpinButton Method
        /// </summary>
        /// <param name="uNumEditor">NumericEditor 이름</param>
        /// <param name="e"></param>
        private void NumericEditorSpinButton(Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumEditor, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uNumEditor.Focus();
                    uNumEditor.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uNumEditor.Focus();
                    uNumEditor.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 필수입력사항 확인 메소드 //
        /// </summary>
        /// <returns></returns>
        private bool CheckRequire()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlantCode.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlantCode.DropDown();
                    return false;
                }
                else if (this.uTextLotNo.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000078", Infragistics.Win.HAlign.Center);

                    this.uTextLotNo.Focus();
                    return false;
                }
                else if (this.uDateAriseDate.Value == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000440", Infragistics.Win.HAlign.Center);

                    this.uDateAriseDate.DropDown();
                    return false;
                }
                else if (this.uTextProductCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000076", Infragistics.Win.HAlign.Center);

                    this.uTextLotNo.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Return HeaderInfo By DataTable
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_HeaderInfoByDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPINS.BL.INSPRC.ProcQCN clsQCN;

                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                    clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                    brwChannel.mfCredentials(clsQCN);
                }
                else
                {
                    clsQCN = new QRPINS.BL.INSPRC.ProcQCN(m_strDBConn);
                }

                //// HoldCode 가져오는 메소드 호출
                //QRPBrowser brwChannel2 = new QRPBrowser();
                //brwChannel2.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                //QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                //brwChannel2.mfCredentials(clsReason);

                //DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

                //string strHoldCode = string.Empty;
                //if (dtReason.Rows.Count > 0)
                //    strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                // 데이터 테이블 컬럼설정
                dtRtn = clsQCN.mfSetQCNDataInfo();

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                drRow["QCNNo"] = this.uTextQCNNo.Text;
                drRow["ProductCode"] = this.uTextProductCode.Text;
                drRow["LotNo"] = this.uTextLotNo.Text;
                if (this.uComboAriseProcessCode.SelectedRow != null)
                    drRow["AriseProcessCode"] = this.uComboAriseProcessCode.SelectedRow.Cells["ProcessCode"].Value.ToString();
                else
                    drRow["AriseProcessCode"] = string.Empty;
                if (this.uComboAriseEquipCode.SelectedRow != null)
                    drRow["AriseEquipCode"] = this.uComboAriseEquipCode.SelectedRow.Cells["EquipCode"].Value.ToString();
                else
                    drRow["AriseEquipCode"] = string.Empty;
                if (this.uComboExpectProcessCode.SelectedRow != null)
                    drRow["ExpectProcessCode"] = this.uComboExpectProcessCode.SelectedRow.Cells["ProcessCode"].Value.ToString();
                else
                    drRow["ExpectProcessCode"] = string.Empty;
                drRow["AriseDate"] = Convert.ToDateTime(this.uDateAriseDate.Value).ToString("yyyy-MM-dd");
                //drRow["AriseTime"] = Convert.ToDateTime(this.uDateAriseTime.Value).ToString("HH:mm:ss");
                drRow["AriseTime"] = DateTime.Now.ToString("HH:mm:ss");
                
                drRow["MESWorkUserID"] = this.uTextMESWorkUserID.Text;
                drRow["Qty"] = Convert.ToDecimal(this.uNumQty.Value);
                drRow["InspectQty"] = Convert.ToDecimal(this.uNumInspectQty.Value);
                drRow["FaultQty"] = Convert.ToDecimal(this.uNumFaultQty.Value);
                drRow["InspectFaultTypeCode"] = this.uComboInspectFaultTypeCode.Value.ToString();

                // 블량 Image파일
                if (this.uTextFaultImg.Text.Contains(":\\") && this.uTextQCNNo.Text != "")
                {
                    System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uTextFaultImg.Text);
                    drRow["FaultFilePath"] = this.uComboPlantCode.Value.ToString() + "-" + this.uTextQCNNo.Text + "-F-" + fileDoc.Name;
                }
                else if (this.uTextFaultImg.Text.Contains(":\\") && this.uTextQCNNo.Text == "")
                {
                    System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uTextFaultImg.Text);
                    drRow["FaultFilePath"] = fileDoc.Name;
                }
                else
                {
                    drRow["FaultFilePath"] = this.uTextFaultImg.Text;
                }

                drRow["WorkStepMaterialFlag"] = this.uCheckWorkStepMaterialFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["WorkStepEquipFlag"] = this.uCheckWorkStepEquipFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag1"] = this.uCheckActionFlag1.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag2"] = this.uCheckActionFlag2.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag3"] = this.uCheckActionFlag3.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag4"] = this.uCheckActionFlag4.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag5"] = this.uCheckActionFlag5.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag6"] = this.uCheckActionFlag6.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag7"] = this.uCheckActionFlag7.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag8"] = this.uCheckActionFlag8.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ActionFlag9"] = this.uCheckActionFlag9.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["WorkUserID"] = this.uTextWorkUserID.Text;
                drRow["InspectUserID"] = this.uTextInspectUserID.Text;
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                drRow["PublicshFlag"] = this.uCheckPublishFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["MESHoldTFlag"] = this.uTextMESTFlag.Text;
                drRow["PublicshComplete"] = this.uTextPublishComplete.Text;
                //drRow["HoldCode"] = strHoldCode;
                drRow["QCConfirmComplete"] = "F";
                drRow["ReqNo"] = this.uTextReqNo.Text;
                drRow["ReqSeq"] = this.uTextReqSeq.Text;
                drRow["LotProcessState"] = this.uTextLotProcessState.Text;
                drRow["LotHoldState"] = this.uTextLotHoldState.Text;
                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Return AffectLot Info By DataTable
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_AffectLotInfoByDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNAffectLot), "ProcQCNAffectLot");
                QRPINS.BL.INSPRC.ProcQCNAffectLot clsALot = new QRPINS.BL.INSPRC.ProcQCNAffectLot();
                brwChannel.mfCredentials(clsALot);

                dtRtn = clsALot.mfSetDataInfo();

                if (this.uGridAffectLotList.Rows.Count > 0)
                {
                    // 데이터 테이블 컬럼 설정
                    DataRow drRow;

                    this.uGridAffectLotList.ActiveCell = this.uGridAffectLotList.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                    {
                        if (this.uGridAffectLotList.Rows[i].Hidden == false)
                        {
                            if (!string.IsNullOrEmpty(this.uGridAffectLotList.Rows[i].Cells["LotNo"].Value.ToString()))
                            {
                                drRow = dtRtn.NewRow();
                                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                drRow["QCNNo"] = this.uTextQCNNo.Text;
                                drRow["Seq"] = this.uGridAffectLotList.Rows[i].RowSelectorNumber;
                                drRow["LotType"] = this.uGridAffectLotList.Rows[i].Cells["LotType"].Value.ToString();
                                drRow["LotNo"] = this.uGridAffectLotList.Rows[i].Cells["LotNo"].Value.ToString();
                                drRow["ProductCode"] = this.uGridAffectLotList.Rows[i].Cells["ProductCode"].Value.ToString();
                                drRow["ProcessCode"] = this.uGridAffectLotList.Rows[i].Cells["ProcessCode"].Value.ToString();
                                drRow["EquipCode"] = this.uGridAffectLotList.Rows[i].Cells["EquipCode"].Value.ToString();
                                drRow["Qty"] = this.uGridAffectLotList.Rows[i].Cells["Qty"].Value.ToString();
                                drRow["WorkDateTime"] = Convert.ToDateTime(this.uGridAffectLotList.Rows[i].Cells["WorkDateTime"].Value);
                                drRow["WorkUserID"] = this.uGridAffectLotList.Rows[i].Cells["WorkUserID"].Value.ToString();
                                drRow["MESHoldTFlag"] = this.uGridAffectLotList.Rows[i].Cells["MESHoldTFlag"].Value.ToString();
                                dtRtn.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 예상설비 정보 데이터 테이블로 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_ExpectEquipList()
        {
            DataTable dtEquipList = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNExpectEquip), "ProcQCNExpectEquip");
                QRPINS.BL.INSPRC.ProcQCNExpectEquip clsExpectEquip = new QRPINS.BL.INSPRC.ProcQCNExpectEquip();
                brwChannel.mfCredentials(clsExpectEquip);

                dtEquipList = clsExpectEquip.mfSetDataInfo();

                if (this.uGridExpectEquipList.Rows.Count > 0)
                {
                    DataRow drRow;

                    this.uGridExpectEquipList.ActiveCell = this.uGridExpectEquipList.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridExpectEquipList.Rows.Count; i++)
                    {
                        drRow = dtEquipList.NewRow();
                        drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                        drRow["QCNNo"] = this.uTextQCNNo.Text;
                        drRow["Seq"] = this.uGridExpectEquipList.Rows[i].Cells["Seq"].Value.ToString();
                        drRow["CheckFlag"] = this.uGridExpectEquipList.Rows[i].Cells["CheckFlag"].Value.ToString().ToUpper().Substring(0, 1);
                        drRow["ExpectEquipCode"] = this.uGridExpectEquipList.Rows[i].Cells["EQPID"].Value.ToString();
                        drRow["WorkUserID"] = this.uGridExpectEquipList.Rows[i].Cells["USERID"].Value.ToString();
                        drRow["EtcDesc"] = this.uGridExpectEquipList.Rows[i].Cells["EtcDesc"].Value.ToString();
                        dtEquipList.Rows.Add(drRow);
                    }
                }
                return dtEquipList;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtEquipList;
            }
            finally
            {
                dtEquipList.Dispose();
            }
        }

        private DataTable Rtn_QCNUser()
        {
            DataTable dtUserList = new DataTable();
            try
            {
                if (this.uGridQCNUser.Rows.Count > 0)
                {
                    this.uGridQCNUser.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNUser), "ProcQCNUser");
                    QRPINS.BL.INSPRC.ProcQCNUser clsQCNUser = new QRPINS.BL.INSPRC.ProcQCNUser();
                    brwChannel.mfCredentials(clsQCNUser);

                    dtUserList = clsQCNUser.mfSetDataInfo();
                    DataRow drRow;
                    for (int i = 0; i < this.uGridQCNUser.Rows.Count; i++)
                    {
                        if (!this.uGridQCNUser.Rows[i].Cells["UserID"].Value.ToString().Equals(string.Empty) &&
                            this.uGridQCNUser.Rows[i].Cells["UserID"].Value != null &&
                            this.uGridQCNUser.Rows[i].Cells["UserID"].Value != DBNull.Value)
                        {
                            drRow = dtUserList.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["QCNNo"] = this.uTextQCNNo.Text;
                            drRow["Seq"] = this.uGridQCNUser.Rows[i].RowSelectorNumber;
                            drRow["UserID"] = this.uGridQCNUser.Rows[i].Cells["UserID"].Value.ToString();
                            drRow["EtcDesc"] = this.uGridQCNUser.Rows[i].Cells["EtcDesc"].Value.ToString();
                            dtUserList.Rows.Add(drRow);
                        }
                    }
                }

                return dtUserList;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUserList;
            }
            finally
            {
                dtUserList.Dispose();
            }
        }

        /// <summary>
        /// 컨트롤 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //ClearControls(uGroupBoxStdInfo);
                //ClearControls(uGroupBox3);
                this.uTextQCNNo.Clear();
                this.uTextLotNo.Clear();
                this.uTextCustomerName.Clear();
                this.uTextCustomerProductSpec.Clear();
                this.uTextProductCode.Clear();
                this.uTextProductName.Clear();
                this.uTextPackage.Clear();
                //this.uComboAriseProcessCode.ResetValueMember();
                //this.uComboAriseEquipCode.ResetValueMember();
                //this.uComboExpectProcessCode.ResetValueMember();
                //this.uComboExpectEquipCode.ResetValueMember();
                //this.uComboMultiEquipCode.ResetValueMember();
                this.uNumFaultQty.Value = 0;
                this.uNumInspectQty.Value = 0;
                this.uNumQty.Value = 0;
                this.uComboInspectFaultTypeCode.Value = "";
                this.uTextFaultImg.Clear();
                this.uCheckPublishFlag.Checked = false;

                this.uCheckActionFlag1.Checked = false;
                this.uCheckActionFlag2.Checked = false;
                this.uCheckActionFlag3.Checked = false;
                this.uCheckActionFlag4.Checked = false;
                this.uCheckActionFlag5.Checked = false;
                this.uCheckActionFlag6.Checked = false;
                this.uCheckActionFlag7.Checked = false;
                this.uCheckActionFlag8.Checked = false;
                this.uCheckActionFlag9.Checked = false;
                this.uTextWorkUserID.Clear();
                this.uTextWorkUserName.Clear();
                this.uTextInspectUserID.Clear();
                this.uTextInspectUserName.Clear();
                this.uTextEtcDesc.Clear();

                while (this.uGridAffectLotList.Rows.Count > 0)
                {
                    this.uGridAffectLotList.Rows[0].Delete(false);
                }

                while (this.uGridExpectEquipList.Rows.Count > 0)
                {
                    this.uGridExpectEquipList.Rows[0].Delete(false);
                }
                while (this.uGridQCNUser.Rows.Count > 0)
                {
                    this.uGridQCNUser.Rows[0].Delete(false);
                }

                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");

                this.uComboPlantCode.Enabled = true;
                this.uCheckPublishFlag.Enabled = true;

                this.uTextLotNo.ReadOnly = false;                //LotNo
                this.uComboAriseProcessCode.ReadOnly = false;    //발생공정
                this.uComboAriseEquipCode.ReadOnly = false;      //발생설비
                this.uDateAriseDate.ReadOnly = false;            //발행일
                this.uComboExpectProcessCode.ReadOnly = false;   //예상공정
                this.uNumQty.ReadOnly = false;                   //수량
                this.uNumInspectQty.ReadOnly = false;            //검사수량 
                this.uNumFaultQty.ReadOnly = false;              //불량수량
                this.uComboInspectFaultTypeCode.ReadOnly = false;//불량유형
                //this.uCheckWorkStepMaterialFlag.Enabled = false;//자재재검
                this.uCheckWorkStepMaterialFlag.Checked = true;//Default값은 Checked
                this.uCheckWorkStepEquipFlag.Enabled = true;   //장비재검
                this.uCheckWorkStepEquipFlag.Checked = false;
                this.uCheckActionFlag1.Enabled = true;         //작업 방법 점검
                this.uCheckActionFlag2.Enabled = true;         //부적합처리절차에의해 처리
                this.uCheckActionFlag3.Enabled = true;         //원/부자재상태 점검
                this.uCheckActionFlag4.Enabled = true;         //교육실시
                this.uCheckActionFlag5.Enabled = true;         //장비상태및가동조건점검
                this.uCheckActionFlag6.Enabled = true;         //표준발행유무점검
                this.uCheckActionFlag7.Enabled = true;         //기술부서 검토의뢰
                this.uCheckActionFlag8.Enabled = true;         //작업중지
                this.uCheckActionFlag9.Enabled = true;         //전수 선별QC의뢰
                //this.uTextWorkUserID.ReadOnly = false;           //작업자
                this.uTextInspectUserID.ReadOnly = false;        //검사자
                this.uTextEtcDesc.ReadOnly = false;              //비고
                this.uTextMESTFlag.Text = "F";
                this.uTextPublishComplete.Text = "F";

                this.uTextReqNo.Clear();
                this.uTextReqSeq.Clear();
                this.uTextLotProcessState.Clear();
                this.uTextLotHoldState.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 Clear Method
        /// 너무 느림;;;
        /// </summary>
        /// <param name="c"></param>
        private void ClearControls(Control c)
        {
            foreach (Control Ctrl in c.Controls)
            {
                if (Ctrl.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraTextEditor" || Ctrl.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraComboEditor" ||
                    Ctrl.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraDateTimeEditor" || Ctrl.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraNumericEditor" ||
                    Ctrl.GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraCheckEditor")
                {
                    switch (Ctrl.GetType().ToString())
                    {
                        case "Infragistics.Win.UltraWinEditors.UltraTextEditor":
                            ((Infragistics.Win.UltraWinEditors.UltraTextEditor)Ctrl).Clear();
                            break;
                        case "Infragistics.Win.UltraWinEditors.UltraComboEditor":
                            ((Infragistics.Win.UltraWinEditors.UltraComboEditor)Ctrl).Value = "";
                            break;
                        case "Infragistics.Win.UltraWinEditors.UltraDateTimeEditor":
                            ((Infragistics.Win.UltraWinEditors.UltraDateTimeEditor)Ctrl).Value = DateTime.Now;
                            break;
                        case "Infragistics.Win.UltraWinEditors.UltraNumericEditor":
                            ((Infragistics.Win.UltraWinEditors.UltraNumericEditor)Ctrl).Value = 0;
                            break;
                        case "Infragistics.Win.UltraWinEditors.UltraCheckEditor":
                            ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)Ctrl).Checked = false;
                            break;
                        //case "Infragistics.Win.UltraWinGrid.UltraGrid":
                        //    while (((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows.Count > 0)
                        //    {
                        //        ((Infragistics.Win.UltraWinGrid.UltraGrid)Ctrl).Rows[0].Delete(false);
                        //    }
                        //    break;
                        default:
                            //if (Ctrl.Controls.Count > 0)
                            //    ClearControls(Ctrl);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// 헤더정보 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">언어</param>
        private void Search_HeaderData(string strPlantCode, string strQCNNo, string strLang)
        {
            try
            {
                QRPINS.BL.INSPRC.ProcQCN clsQCN;
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                    clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                    brwChannel.mfCredentials(clsQCN);
                }
                else
                {
                    clsQCN = new QRPINS.BL.INSPRC.ProcQCN(m_strDBConn);
                }

                DataTable dtRtn = clsQCN.mfReadINSProcQCNDetail(strPlantCode, strQCNNo, strLang);

                if (dtRtn.Rows.Count > 0)
                {
                    this.uCheckPublishFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["PublishFlag"]);

                    this.uComboPlantCode.Value = dtRtn.Rows[0]["PlantCode"].ToString();
                    this.uTextQCNNo.Text = dtRtn.Rows[0]["QCNNo"].ToString();
                    this.uTextLotNo.Text = dtRtn.Rows[0]["LotNo"].ToString();
                    this.uTextCustomerCode.Text = dtRtn.Rows[0]["CustomerCode"].ToString();
                    this.uTextCustomerName.Text = dtRtn.Rows[0]["CustomerName"].ToString();
                    this.uTextProductCode.Text = dtRtn.Rows[0]["ProductCode"].ToString();
                    this.uTextProductName.Text = dtRtn.Rows[0]["ProductName"].ToString();
                    this.uTextCustomerProductSpec.Text = dtRtn.Rows[0]["CustomerProductSpec"].ToString();
                    this.uTextPackage.Text = dtRtn.Rows[0]["Package"].ToString();
                    if (dtRtn.Rows[0]["AriseProcessCode"].ToString() == "")
                    {
                        this.uComboAriseProcessCode.Text = "선택";
                    }
                    else
                    {
                        this.uComboAriseProcessCode.EventManager.AllEventsEnabled = false;
                        this.uComboAriseProcessCode.Value = dtRtn.Rows[0]["AriseProcessCode"].ToString();
                        this.uComboAriseProcessCode.EventManager.AllEventsEnabled = true;
                    }
                    if (dtRtn.Rows[0]["AriseEquipCode"].ToString() == "")
                    {
                        this.uComboAriseEquipCode.Text = "선택";
                    }
                    else
                    {
                        this.uComboAriseEquipCode.EventManager.AllEventsEnabled = false;
                        this.uComboAriseEquipCode.Value = dtRtn.Rows[0]["AriseEquipCode"].ToString();
                        this.uComboAriseEquipCode.EventManager.AllEventsEnabled = true;
                    }
                    this.uDateAriseDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["AriseDate"]).ToString("yyyy-MM-dd");
                    this.uDateAriseTime.Value = Convert.ToDateTime(dtRtn.Rows[0]["AriseTime"]).ToString("HH:mm:ss");
                    if (dtRtn.Rows[0]["ExpectProcessCode"].ToString() == "")
                    {
                        this.uComboExpectProcessCode.Text = "선택";
                    }
                    else
                    {
                        this.uComboExpectProcessCode.EventManager.AllEventsEnabled = false;
                        this.uComboExpectProcessCode.Value = dtRtn.Rows[0]["ExpectProcessCode"].ToString();
                        this.uComboExpectProcessCode.EventManager.AllEventsEnabled = true;
                    }
                    //this.uTextMESWorkUserID.Text = dtRtn.Rows[0]["MESWorkUserID"].ToString();
                    this.uNumQty.Value = Convert.ToDecimal(dtRtn.Rows[0]["Qty"]);
                    this.uNumInspectQty.Value = Convert.ToDecimal(dtRtn.Rows[0]["InspectQty"]);
                    this.uNumFaultQty.Value = Convert.ToDecimal(dtRtn.Rows[0]["FaultQty"]);
                    this.uComboInspectFaultTypeCode.Value = dtRtn.Rows[0]["InspectFaultTypeCode"].ToString();
                    this.uTextFaultImg.Text = dtRtn.Rows[0]["FaultFilePath"].ToString();
                    this.uCheckWorkStepMaterialFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["WorkStepMaterialFlag"]);
                    this.uCheckWorkStepEquipFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["WorkStepEquipFlag"]);
                    this.uCheckActionFlag1.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag1"]);
                    this.uCheckActionFlag2.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag2"]);
                    this.uCheckActionFlag3.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag3"]);
                    this.uCheckActionFlag4.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag4"]);
                    this.uCheckActionFlag5.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag5"]);
                    this.uCheckActionFlag6.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag6"]);
                    this.uCheckActionFlag7.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag7"]);
                    this.uCheckActionFlag8.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag8"]);
                    this.uCheckActionFlag9.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag9"]);
                    this.uTextWorkUserID.Text = dtRtn.Rows[0]["WorkUserID"].ToString();
                    this.uTextWorkUserName.Text = dtRtn.Rows[0]["WorkUserName"].ToString();
                    this.uTextInspectUserID.Text = dtRtn.Rows[0]["InspectUserID"].ToString();
                    this.uTextInspectUserName.Text = dtRtn.Rows[0]["InspectUserName"].ToString();
                    this.uTextEtcDesc.Text = dtRtn.Rows[0]["EtcDesc"].ToString();

                    this.uTextReqNo.Text = dtRtn.Rows[0]["ReqNo"].ToString();
                    this.uTextReqSeq.Text = dtRtn.Rows[0]["ReqSeq"].ToString();
                    this.uTextLotProcessState.Text = dtRtn.Rows[0]["LotProcessState"].ToString();
                    this.uTextLotHoldState.Text = dtRtn.Rows[0]["LotHoldState"].ToString();
                }
                this.uComboPlantCode.Enabled = false;

                if (Convert.ToBoolean(this.uCheckPublishFlag.CheckedValue))
                {
                    this.uCheckPublishFlag.Enabled = false;

                    //this.uGroupBox2.Enabled = false;
                    //this.uGroupBox3.Enabled = false;
                    //this.uGroupBoxStdInfo.Enabled = false;

                    //this.uGroupBox2.Appearance.BackColorDisabled = Color.White;
                    //this.uGroupBox2.Appearance.ForeColorDisabled = Color.Black;

                    //this.uGroupBox3.Appearance.BackColorDisabled = Color.White;
                    //this.uGroupBox3.Appearance.ForeColorDisabled = Color.Black;

                    //this.uGroupBoxStdInfo.Appearance.BackColorDisabled = Color.White;
                    //this.uGroupBoxStdInfo.Appearance.ForeColorDisabled = Color.Black;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// AffectLot 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCNNo</param>
        /// <param name="strLang">언어</param>
        private void Search_AffectLotData(string strPlantCode, string strQCNNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNAffectLot), "ProcQCNAffectLot");
                QRPINS.BL.INSPRC.ProcQCNAffectLot clsALot = new QRPINS.BL.INSPRC.ProcQCNAffectLot();
                brwChannel.mfCredentials(clsALot);

                DataTable dtRtn = clsALot.mfReadINSProcQCNAffectLot(strPlantCode, strQCNNo, strLang);

                this.uGridAffectLotList.DataSource = dtRtn;
                this.uGridAffectLotList.DataBind();

                ////// 바인딩된 행 수정불가
                ////for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                ////{
                ////    this.uGridAffectLotList.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit();
                ////    this.uGridAffectLotList.Rows[i].Appearance.BackColor = Color.Gainsboro;
                ////}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ExpectEquip List 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN 관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_ExpectEquipList(string strPlantCode, string strQCNNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNExpectEquip), "ProcQCNExpectEquip");
                QRPINS.BL.INSPRC.ProcQCNExpectEquip clsExEquip = new QRPINS.BL.INSPRC.ProcQCNExpectEquip();
                brwChannel.mfCredentials(clsExEquip);

                DataTable dtExEquipList = clsExEquip.mfReadINSProcQCNExpectEquip(strPlantCode, strQCNNo, strLang);

                this.uGridExpectEquipList.DataSource = dtExEquipList;
                this.uGridExpectEquipList.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 이메일 발송자명단 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN발행번호</param>
        /// <param name="strLang">언어</param>
        private void Search_QCNUser(string strPlantCode, string strQCNNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNUser), "ProcQCNUser");
                QRPINS.BL.INSPRC.ProcQCNUser clsQCNUser = new QRPINS.BL.INSPRC.ProcQCNUser();
                brwChannel.mfCredentials(clsQCNUser);

                DataTable dtQCNUser = clsQCNUser.mfReadINSProcQCNUser(strPlantCode, strQCNNo, strLang);

                this.uGridQCNUser.DataSource = dtQCNUser;
                this.uGridQCNUser.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 발행완료의 정보일경우 수정불가처리
        /// </summary>
        private void ReadOnlyControl()
        {
            try
            {
                if (this.uCheckPublishFlag.Checked == true)
                {
                    this.uTextLotNo.ReadOnly = true;                //LotNo
                    this.uComboAriseProcessCode.ReadOnly = true;    //발생공정
                    this.uComboAriseEquipCode.ReadOnly = true;      //발생설비
                    this.uDateAriseDate.ReadOnly = true;            //발행일
                    this.uComboExpectProcessCode.ReadOnly = true;   //예상공정
                    this.uNumQty.ReadOnly = true;                   //수량
                    this.uNumInspectQty.ReadOnly = true;            //검사수량 
                    this.uNumFaultQty.ReadOnly = true;              //불량수량
                    this.uComboInspectFaultTypeCode.ReadOnly = true;//불량유형
                    //this.uCheckWorkStepMaterialFlag.Enabled = false;//자재재검
                    this.uCheckWorkStepEquipFlag.Enabled = false;   //장비재검

                    SetCheckBoxEnable(this.uCheckActionFlag1);
                    SetCheckBoxEnable(this.uCheckActionFlag2);
                    SetCheckBoxEnable(this.uCheckActionFlag3);
                    SetCheckBoxEnable(this.uCheckActionFlag4);
                    SetCheckBoxEnable(this.uCheckActionFlag5);
                    SetCheckBoxEnable(this.uCheckActionFlag6);
                    SetCheckBoxEnable(this.uCheckActionFlag7);
                    SetCheckBoxEnable(this.uCheckActionFlag8);
                    SetCheckBoxEnable(this.uCheckActionFlag9);
                    
                    this.uTextWorkUserID.ReadOnly = true;           //작업자
                    this.uTextInspectUserID.ReadOnly = true;        //검사자
                    this.uTextEtcDesc.ReadOnly = true;              //비고

                    //AffectLot 그리드
                    for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                    {
                        this.uGridAffectLotList.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    
                }
                else
                {
                    if (this.uTextLotNo.ReadOnly == true)
                    {
                        this.uTextLotNo.ReadOnly = false;                //LotNo
                        this.uComboAriseProcessCode.ReadOnly = false;    //발생공정
                        this.uComboAriseEquipCode.ReadOnly = false;      //발생설비
                        this.uDateAriseDate.ReadOnly = false;            //발행일
                        this.uComboExpectProcessCode.ReadOnly = false;   //예상공정
                        this.uNumQty.ReadOnly = false;                   //수량
                        this.uNumInspectQty.ReadOnly = false;            //검사수량 
                        this.uNumFaultQty.ReadOnly = false;              //불량수량
                        this.uComboInspectFaultTypeCode.ReadOnly = false;//불량유형
                        //this.uCheckWorkStepMaterialFlag.Enabled = true;//자재재검
                        this.uCheckWorkStepEquipFlag.Enabled = true;   //장비재검

                        SetCheckBoxUnEnable(this.uCheckActionFlag1);
                        SetCheckBoxUnEnable(this.uCheckActionFlag2);
                        SetCheckBoxUnEnable(this.uCheckActionFlag3);
                        SetCheckBoxUnEnable(this.uCheckActionFlag4);
                        SetCheckBoxUnEnable(this.uCheckActionFlag5);
                        SetCheckBoxUnEnable(this.uCheckActionFlag6);
                        SetCheckBoxUnEnable(this.uCheckActionFlag7);
                        SetCheckBoxUnEnable(this.uCheckActionFlag8);
                        SetCheckBoxUnEnable(this.uCheckActionFlag9);

                        
                        this.uTextWorkUserID.ReadOnly = false;           //작업자
                        this.uTextInspectUserID.ReadOnly = false;        //검사자
                        this.uTextEtcDesc.ReadOnly = false;              //비고
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 체크박스 Enable 상태로 만들고 색지정하는 메소드
        /// </summary>
        /// <param name="c"></param>
        private void SetCheckBoxEnable(Control c)
        {
            try
            {
                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Enabled = false;
                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Appearance.BackColorDisabled = Color.White;
                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Appearance.ForeColorDisabled = Color.Black;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 체크박스 Enable 상태를 해제
        /// </summary>
        /// <param name="c"></param>
        private void SetCheckBoxUnEnable(Control c)
        {
            try
            {
                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Enabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 텍스트박스 이벤트
        // 작업자 팝업창 이벤트
        private void uTextWorkUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }            
                if (this.uTextWorkUserID.ReadOnly == false)
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                    , msg.GetMessge_Text("M001254", strLang) + uComboPlantCode.Text + msg.GetMessge_Text("M000001", strLang)
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextWorkUserID.Text = "";
                        this.uTextWorkUserName.Text = "";
                    }
                    else
                    {
                        this.uTextWorkUserID.Text = frmPOP.UserID;
                        this.uTextWorkUserName.Text = frmPOP.UserName;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사자 팝업창 이벤트
        private void uTextInspectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                if (this.uTextInspectUserID.ReadOnly == false)
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                    , msg.GetMessge_Text("M001254", strLang) + uComboPlantCode.Text + msg.GetMessge_Text("M000001", strLang)
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextInspectUserID.Text = "";
                        this.uTextInspectUserName.Text = "";
                    }
                    else
                    {
                        this.uTextInspectUserID.Text = frmPOP.UserID;
                        this.uTextInspectUserName.Text = frmPOP.UserName;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작업자 키다운 이벤트
        private void uTextWorkUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWorkUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        string strPlantCode = this.uComboPlantCode.Value.ToString();
                        String strWriteID = this.uTextWorkUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(strPlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextWorkUserID.Text = "";
                            this.uTextWorkUserName.Text = "";
                        }
                        else
                        {
                            this.uTextWorkUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWorkUserID.TextLength <= 1 || this.uTextWorkUserID.SelectedText == this.uTextWorkUserID.Text)
                    {
                        this.uTextWorkUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사자 키다운 이벤트
        private void uTextInspectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        string strPlantCode = this.uComboPlantCode.Value.ToString();
                        String strWriteID = this.uTextInspectUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(strPlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextInspectUserID.Text = "";
                            this.uTextInspectUserName.Text = "";
                        }
                        else
                        {
                            this.uTextInspectUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID.TextLength <= 1 || this.uTextInspectUserID.SelectedText == this.uTextInspectUserID.Text)
                    {
                        this.uTextInspectUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 제품코드 팝업 이벤트
        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uComboSearchPlantCode.Value.ToString() != "")
                {
                    frmPOP0002 frmPOP = new frmPOP0002();
                    frmPOP.PlantCode = uComboSearchPlantCode.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboSearchPlantCode.Value.ToString() != frmPOP.PlantCode)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000275", "M000658",
                                            Infragistics.Win.HAlign.Right);

                        this.uTextSearchProductCode.Clear();
                        this.uTextSearchProductName.Clear();
                    }
                    else
                    {
                        this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                        this.uTextSearchProductName.Text = frmPOP.ProductName;
                    }
                }
                else
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000276", "M000277",
                                        Infragistics.Win.HAlign.Right);
                    
                    this.uComboSearchPlantCode.DropDown();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

       
        #endregion

        #region Numeric에디터 버튼 이벤트
        // 수량 버튼 이벤트
        private void uNumQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //검사수량 버튼 이벤트
        private void uNumInspectQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량수량 버튼 이벤트
        private void uNumFaultQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 수량 스핀버튼 이벤트
        private void uNumQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumQty, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사수량 스핀버튼 이벤트
        private void uNumInspectQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumInspectQty, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량수량 스핀버튼 이벤트
        private void uNumFaultQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumFaultQty, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 공장 콤보박스 값 변경시 이벤트
        private void uComboPlantCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                string strPlantCode = uComboPlantCode.Value.ToString(); // 공장코드값

                
                DataTable dtProcess = new DataTable();
                dtProcess.Columns.Add("ProcessCode", typeof(string));
                dtProcess.Columns.Add("ProcessName", typeof(string));
                DataTable dtEquip = new DataTable();
                dtEquip.Columns.Add("EquipCode", typeof(string));
                dtEquip.Columns.Add("EquipName", typeof(string));
                DataTable dtIFType = new DataTable();

                if (strPlantCode != "")
                {
                    // 공정
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsAriseProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsAriseProcess);
                
                    dtProcess = clsAriseProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 설비
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    // Method 호출
                    dtEquip = clsEquip.mfReadEquipForPlantCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    
                    // 불량유형
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsIFault = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsIFault);

                    dtIFType = clsIFault.mfReadInspectFaultTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                this.uComboAriseEquipCode.DataSource = dtEquip;
                this.uComboAriseEquipCode.DataBind();

                this.uComboAriseProcessCode.DataSource = dtProcess;
                this.uComboAriseProcessCode.DataBind();

                this.uComboExpectProcessCode.DataSource = dtProcess;
                this.uComboExpectProcessCode.DataBind();

                // 불량유형
                wCombo.mfSetComboEditor(this.uComboInspectFaultTypeCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "InspectFaultTypeCode", "InspectFaultTypeName", dtIFType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            {        
            }
        }

        // 검색조건 공장콤보박스 값 변경시 이벤트
        private void uComboSearchPlantCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                
                DataTable dtProcess = new DataTable();

                // 전체가 아닐시만 데이터 조회 선택되었을때만 
                if (strPlantCode != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboSearchProcessCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그룹박스 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 110);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridQCNList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridQCNList.Height = 780;
                    for (int i = 0; i < this.uGridQCNList.Rows.Count; i++)
                    {
                        this.uGridQCNList.Rows[i].Fixed = false;
                    }

                    // Control Clear
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 헤더그리드 더블클릭시 상세정보 보여주는 이벤트
        private void uGridQCNList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 행고정
                e.Row.Fixed = true;

                // ContentsArea 펼침 상태로
                this.uGroupBoxContentsArea.Expanded = true;

                // 매개변수로 전달될 변수 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strQCNNo = e.Row.Cells["QCNNo"].Value.ToString();

                //MESTHoldFlag
                this.uTextMESTFlag.Text = e.Row.Cells["MESHoldTFlag"].Value.ToString();
                this.uTextPublishComplete.Text = e.Row.Cells["PublishFlagCode"].Value.ToString();

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 메소드 호출
                Search_HeaderData(strPlantCode, strQCNNo, m_resSys.GetString("SYS_LANG"));
                Search_AffectLotData(strPlantCode, strQCNNo, m_resSys.GetString("SYS_LANG"));
                Search_ExpectEquipList(strPlantCode, strQCNNo, m_resSys.GetString("SYS_LANG"));
                Search_QCNUser(strPlantCode, strQCNNo, m_resSys.GetString("SYS_LANG"));

                WinGrid wGrid = new WinGrid();
                wGrid.mfSetAutoResizeColWidth(this.uGridAffectLotList, 0);
                wGrid.mfSetAutoResizeColWidth(this.uGridExpectEquipList, 0);

                ReadOnlyControl();
                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 버튼 이벤트
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridAffectLotList.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridAffectLotList.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region MES IF

        // LotNo 텍스트 박스 키다운 이벤트
        private void uTextLotNo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Back || e.KeyData == Keys.Delete)
                {
                    Clear();
                }
                // 엔터키 입력시
                if (e.KeyData == Keys.Enter)
                {
                    // LotNo 가 공백이 아닐때 MES I/F
                    if (!this.uTextLotNo.Text.Equals(string.Empty))
                    {
                        // Clear
                        this.uTextProductCode.Text = "";
                        this.uTextProductName.Text = "";
                        this.uTextCustomerName.Text = "";
                        this.uTextCustomerProductSpec.Text = "";
                        this.uComboAriseEquipCode.Value = "";
                        this.uComboAriseProcessCode.Value = "";
                        this.uTextPackage.Clear();
                        this.uTextWorkUserID.Clear();
                        this.uTextWorkUserName.Clear();
                        this.uNumQty.Value = 0;
                        this.uTextLotProcessState.Clear();
                        this.uTextLotHoldState.Clear();
                        while (this.uGridAffectLotList.Rows.Count > 0)
                        {
                            this.uGridAffectLotList.Rows[0].Delete(false);
                        }

                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        // MES와 I/F
                        // LotNo 저장
                        string strLotNo = this.uTextLotNo.Text.ToUpper();

                        ////////MES서버경로 가져오기
                        //////QRPBrowser brwChannel = new QRPBrowser();
                        //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        //////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        //////brwChannel.mfCredentials(clsSysAcce);

                        ////////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
                        ////////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");
                        //////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                        //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);

                        //////// MES Lot정보 조회매서드 실행
                        //////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                        //////DataTable dtLotInfo = clsTibrv.LOT_INFO_REQ(strLotNo);
                        ////////clsTibrv.Dispose();

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                        QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                        brwChannel.mfCredentials(clsMES);

                        DataTable dtLotInfo = clsMES.mfRead_LOT_INFO_REQ(this.uComboPlantCode.Value.ToString(), this.uTextLotNo.Text.Trim().ToUpper());

                        //LotNo 정보가 있을 경우
                        if (dtLotInfo.Rows.Count > 0)
                        {
                            if (dtLotInfo.Rows[0]["returncode"].ToString().Equals("0"))
                            {
                                //제품코드 저장
                                string strProduct = dtLotInfo.Rows[0]["PRODUCTSPECNAME"].ToString();

                                this.uTextProductCode.Text = strProduct;                                               //제품코드
                                this.uTextProductName.Text = dtLotInfo.Rows[0]["PRODUCTSPECDESC"].ToString();           //제품명
                                this.uTextPackage.Text = dtLotInfo.Rows[0]["PACKAGE"].ToString();                       //Package
                                this.uTextCustomerProductSpec.Text = dtLotInfo.Rows[0]["CUSTOMPRODUCTSPECNAME"].ToString(); //고객사제품명
                                this.uComboAriseProcessCode.Value = dtLotInfo.Rows[0]["OPERID"].ToString();             //공정코드
                                this.uComboAriseEquipCode.Value = dtLotInfo.Rows[0]["EQPID"].ToString();                //설비코드
                                this.uTextWorkUserID.Text = dtLotInfo.Rows[0]["LASTEVENTUSER"].ToString();              //작업자ID
                                DataTable dtUserInfo = GetUserInfo(this.uComboPlantCode.Value.ToString(), this.uTextWorkUserID.Text);        // 작업자명
                                if (dtUserInfo.Rows.Count > 0)
                                    this.uTextWorkUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                                this.uNumQty.Value = dtLotInfo.Rows[0]["QTY"];                                          // 수량
                                this.uTextLotProcessState.Text = dtLotInfo.Rows[0]["LOTPROCESSSTATE"].ToString();
                                this.uTextLotHoldState.Text = dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString();

                                //제품정보 BL 호출
                                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                                brwChannel.mfCredentials(clsProduct);

                                //제품 고객사정보 조회 매서드 실행
                                DataTable dtProduct = clsProduct.mfReadMaterialCustomer("", strProduct, m_resSys.GetString("SYS_LANG"));
                                if (dtProduct.Rows.Count > 0)
                                    this.uTextCustomerName.Text = dtProduct.Rows[0]["CustomerName"].ToString();         //고객사명
                            }
                            else
                            {
                                if (dtLotInfo.Rows[0]["returnmessage"].ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001115", "M000091", Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001115", strLang)
                                        , dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                                }
                                this.uTextProductCode.Text = "";
                                this.uTextProductName.Text = "";
                                this.uTextCustomerName.Text = "";
                                this.uTextCustomerProductSpec.Text = "";
                                this.uComboAriseEquipCode.Value = "";
                                this.uComboAriseProcessCode.Value = "";
                                this.uTextPackage.Clear();
                                this.uTextWorkUserID.Clear();
                                this.uTextWorkUserName.Clear();
                                this.uNumQty.Value = 0;
                                this.uTextLotProcessState.Clear();
                                this.uTextLotHoldState.Clear();
                                while (this.uGridAffectLotList.Rows.Count > 0)
                                {
                                    this.uGridAffectLotList.Rows[0].Delete(false);
                                }
                            }
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000069", "M000091", Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //발생설비를 선택하기전 입력항목체크
        private void uComboAriseEquipCode_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //LotNo정보 , 발생공정정보 저장
                string strLotNo = this.uTextLotNo.Text.Trim();

                //LotNo 정보가 없는 경우 메세지 박스를 띄운다.
                if (strLotNo.Equals(string.Empty))// && this.uTextProductCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000962", "M000078", Infragistics.Win.HAlign.Right);
                    //Focus
                    this.uTextLotNo.Focus();
                    e.Cancel = true;
                    return;
                }
                // 발생공정 정보가 없을 경우 메세지 박스를 띄운다.
                if (this.uComboAriseProcessCode.Value == null)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000962", "M000432", Infragistics.Win.HAlign.Right);

                    //DropDown
                    //this.uComboAriseProcessCode.DropDown();
                    this.uComboAriseProcessCode.Focus();
                    e.Cancel = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //예상공정 선택전 입력항목체크
        private void uComboExpectProcessCode_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //LotNo 저장
                string strLotNo = this.uTextLotNo.Text.Trim();

                
                // LotNo 정보가 공백인경우 메세지박스를 입력하고 이동시킨다.
                if (strLotNo.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000962", "M000075", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextLotNo.Focus();
                    //이벤트 취소
                    e.Cancel = true;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //예상설비 선택전 입력항목체크
        private void uComboExpectEquipCode_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //LotNo정보 , 발생공정정보 저장
                string strLotNo = this.uTextLotNo.Text.Trim();
                string strProcess = this.uComboExpectProcessCode.Value.ToString();

                //LotNo 정보가 없는 경우 메세지 박스를 띄운다.
                if (strLotNo.Equals(string.Empty))// && this.uTextProductCode.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000962", "M000078", Infragistics.Win.HAlign.Right);
                    //Focus
                    this.uTextLotNo.Focus();
                    e.Cancel = true;
                    return;
                }
                // 예상공정 정보가 없을 경우 메세지 박스를 띄운다.
                if (strProcess.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000962", "M000822", Infragistics.Win.HAlign.Right);

                    //DropDown
                    this.uComboExpectProcessCode.Focus();
                    e.Cancel = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion                        

        #region 콤보박스 Value 임시 저장
        private void uComboAriseEquipCode_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                if (e.Row != null)
                {
                    if (e.Row.Cells["EquipCode"].Value != null)
                    {
                        //설비정보저장
                        string strEquipCode = e.Row.Cells["EquipCode"].Value.ToString();

                        //설비코드가 선택 되었을 때 Affect Lot정보를 조회하여 Grid에 바인드 시킨다.
                        if (strEquipCode != "")
                        {
                            if (this.uCheckPublishFlag.Enabled)
                            {
                                //System ResourceInfo
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                DialogResult Result = new DialogResult();
                                WinMessageBox msg = new WinMessageBox();

                                //LotNo정보 , 발생공정정보 저장
                                string strLotNo = this.uTextLotNo.Text.Trim().ToUpper();
                                string strProcess = this.uComboAriseProcessCode.Value.ToString();

                                //////MES서버경로 가져오기
                                ////QRPBrowser brwChannel = new QRPBrowser();
                                ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                ////brwChannel.mfCredentials(clsSysAcce);

                                //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
                                //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");
                                ////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                                ////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);

                                //////MES 로 설비정보 보냄
                                ////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);

                                //////Affect Lot정보조회 매서드 실행
                                ////DataTable dtAffect = clsTibrv.LOT_AFFECT_REQ(strLotNo, strProcess, strEquipCode);
                                ////clsTibrv.Dispose();

                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                                QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                                brwChannel.mfCredentials(clsMES);

                                DataTable dtAffect = clsMES.mfRead_LOT_AFFECT_REQ(this.uComboPlantCode.Value.ToString()
                                                                            , strLotNo, strProcess, strEquipCode);
                                
                                //Affect Lot정보가 있을 경우
                                if (dtAffect.Rows.Count > 0)
                                {
                                    if (dtAffect.Rows[0]["returncode"].ToString().Equals("0"))
                                    {
                                        //컬럼명 그리드에 맞게 설정
                                        dtAffect.Columns["LOTTYPE"].ColumnName = "LotType";             //Lot구분
                                        dtAffect.Columns["LOTID"].ColumnName = "LotNo";                 //LotNo
                                        dtAffect.Columns["PRODUCTSPECNAME"].ColumnName = "ProductCode"; //제품코드
                                        dtAffect.Columns["OPERID"].ColumnName = "ProcessCode";          //공정코드
                                        dtAffect.Columns["EQPID"].ColumnName = "EquipCode";             //설비코드
                                        dtAffect.Columns["QTY"].ColumnName = "Qty";                     //수량
                                        dtAffect.Columns["EVENTTIME"].ColumnName = "WorkDateTime";      //작업시간
                                        dtAffect.Columns["USERID"].ColumnName = "WorkUserID";           //작업자ID

                                        //작업자 컬럼추가
                                        dtAffect.Columns.Add("WorkUserName", typeof(string));       //작업자
                                        dtAffect.Columns.Add("MESHoldTFlag", typeof(string));       // MESHold 여부

                                        //사용자정보 BL 호출
                                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                                        brwChannel.mfCredentials(clsUser);


                                        // 0부터 MES에서 받아온 정보줄 수 까지 For문을 돌린다.
                                        for (int i = 0; i < dtAffect.Rows.Count; i++)
                                        {
                                            // WorkUserID가 있는경우
                                            if (dtAffect.Rows[i]["WorkUserID"].ToString() != "")
                                            {
                                                //유저정보 조회 매서드 실행을 한다.
                                                DataTable dtUser = clsUser.mfReadSYSUser("", dtAffect.Rows[i]["WorkUserID"].ToString(), m_resSys.GetString("SYS_LANG"));
                                                //해당 아이디의 정보가 있을 경우 WorkUserName 삽입한다.
                                                if (dtUser.Rows.Count > 0)
                                                    dtAffect.Rows[i]["WorkUserName"] = dtUser.Rows[0]["UserName"];
                                            }
                                            dtAffect.Rows[i]["MESHoldTFlag"] = "F";

                                            if (dtAffect.Rows[i]["WorkDateTime"].ToString().Length > 0)
                                            {
                                                char[] ch = dtAffect.Rows[i]["WorkDateTime"].ToString().ToCharArray();
                                                string strCon = ch[0].ToString() + ch[1].ToString() + ch[2].ToString() + ch[3].ToString() + "-" + ch[4].ToString() + ch[5].ToString() + "-" + ch[6].ToString() + ch[7].ToString()
                                                    + " " + ch[8].ToString() + ch[9].ToString() + ":" + ch[10].ToString() + ch[11].ToString() + ":" + ch[12].ToString() + ch[13].ToString();
                                                dtAffect.Rows[i]["WorkDateTime"] = strCon;
                                            }

                                            if (dtAffect.Rows[i]["LotNo"].ToString().Length.Equals(0))
                                            {
                                                dtAffect.Rows[i].Delete();
                                                i--;
                                            }
                                        }


                                        //그리드에 바인드
                                        this.uGridAffectLotList.DataSource = dtAffect;
                                        this.uGridAffectLotList.DataBind();
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001115", strLang)
                                            , dtAffect.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                                    }
                                }
                                else
                                {
                                    // 그리드 초기화
                                    while (this.uGridAffectLotList.Rows.Count > 0)
                                    {
                                        this.uGridAffectLotList.Rows[0].Delete(false);
                                    }
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001115", "M000027", Infragistics.Win.HAlign.Right);

                                    //this.uComboAriseEquipCode.Value = string.Empty;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        private void uComboExpectProcessCode_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            try
            {
                if (e.Row != null)
                {
                    if (this.uComboExpectProcessCode.Value != null)
                    {
                        //예상공정정보 저장
                        string strProcess = this.uComboExpectProcessCode.Value.ToString();
                        // 공정정보가 공백이 아닌경우 예상설비와 멀티설비를 조회한다.
                        if (strProcess != "")
                        {
                            //if (this.uCheckPublishFlag.Enabled == false)
                            if (this.uCheckPublishFlag.Enabled)
                            {
                                //System ResourceInfo
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                                //LotNo 저장
                                string strLotNo = this.uTextLotNo.Text.Trim().ToUpper();

                                //////MES서버경로 가져오기
                                ////QRPBrowser brwChannel = new QRPBrowser();
                                ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                ////brwChannel.mfCredentials(clsSysAcce);

                                //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
                                //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");
                                ////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                                ////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);

                                //////MES 설비정보 요청매서드 실행
                                ////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                                ////DataTable dtEquip = clsTibrv.LOT_PROCESSED_EQP_REQ(strLotNo, strProcess);
                                //////clsTibrv.Dispose();

                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                                QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                                brwChannel.mfCredentials(clsMES);

                                DataTable dtEquip = clsMES.mfRead_LOT_PROCESSED_EQP_REQ(this.uComboPlantCode.Value.ToString(), strLotNo, strProcess);

                                //설비정보가 있는경우
                                if (dtEquip.Rows.Count > 0)
                                {
                                    if (dtEquip.Rows[0]["returncode"].ToString().Equals("0")) //dtEquip.Rows[0]["returncode"].ToString().Equals(string.Empty) || )
                                    {
                                        //예상설비,멀티설비에 삽입
                                        //this.uComboExpectEquipCode.Value = dtEquip.Rows[0]["EQPID"].ToString();
                                        //this.uComboMultiEquipCode.Value = dtEquip.Rows[0]["EQPID"].ToString();

                                        // 설비명컬럼 추가
                                        dtEquip.Columns.Add("EquipName", typeof(string));
                                        dtEquip.Columns.Add("Seq", typeof(Int32));
                                        dtEquip.Columns.Add("EtcDesc", typeof(string));

                                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                                        brwChannel.mfCredentials(clsEquip);

                                        for (int i = 0; i < dtEquip.Rows.Count; i++)
                                        {
                                            DataTable dtEquipName = clsEquip.mfReadEquipName(this.uComboPlantCode.Value.ToString(), dtEquip.Rows[i]["EQPID"].ToString(), m_resSys.GetString("SYS_LANG"));
                                            if (dtEquipName.Rows.Count > 0)
                                            {
                                                dtEquip.Rows[i]["EquipName"] = dtEquipName.Rows[0]["EquipName"].ToString();
                                            }
                                            dtEquip.Rows[i]["Seq"] = 0;
                                            dtEquip.Rows[i]["EtcDesc"] = "";
                                        }

                                        // 필요없는 컬럼 제거
                                        dtEquip.Columns.Remove("returncode");
                                        dtEquip.Columns.Remove("returnmessage");

                                        this.uGridExpectEquipList.DataSource = dtEquip;
                                        this.uGridExpectEquipList.DataBind();
                                    }
                                    else
                                    {
                                        WinMessageBox msg = new WinMessageBox();
                                        DialogResult Result;
                                        if (dtEquip.Rows[0]["returnmessage"].ToString().Equals(string.Empty))
                                        {
                                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001264", "M000089", "M001403"
                                                                                    , Infragistics.Win.HAlign.Center);
                                        }
                                        else
                                        {
                                            string strLang = m_resSys.GetString("SYS_LANG");
                                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000089", strLang)
                                                                                    , dtEquip.Rows[0]["returnmessage"].ToString()
                                                                                    , Infragistics.Win.HAlign.Center);
                                        }

                                        // 그리드 초기화
                                        while (this.uGridExpectEquipList.Rows.Count > 0)
                                        {
                                            this.uGridExpectEquipList.Rows[0].Delete(false);
                                        }
                                    }
                                }
                                else
                                {
                                    WinMessageBox msg = new WinMessageBox();
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M001264", "M001099", "M001248"
                                                                            , Infragistics.Win.HAlign.Center);
                                    // 그리드 초기화
                                    while (this.uGridExpectEquipList.Rows.Count > 0)
                                    {
                                        this.uGridExpectEquipList.Rows[0].Delete(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridQCNUser_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("UserID"))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (this.uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    if (this.uTextInspectUserID.ReadOnly == false)
                    {
                        frmPOP0011 frmPOP = new frmPOP0011();
                        frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                        frmPOP.ShowDialog();

                        if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                        {
                            DialogResult Result = new DialogResult();
                            WinMessageBox msg = new WinMessageBox();
                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                , msg.GetMessge_Text("M001254" , strLang)+ this.uComboPlantCode.Text + msg.GetMessge_Text("M000001", strLang)
                                , Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Cells["UserID"].Value = "";
                            e.Cell.Row.Cells["UserName"].Value = "";
                            e.Cell.Row.Cells["DeptName"].Value = "";
                            e.Cell.Row.Cells["EMail"].Value = "";
                        }
                        else
                        {
                            if (frmPOP.UserID.Length != 0)
                            {
                                e.Cell.Row.Cells["UserID"].Value = frmPOP.UserID;
                                e.Cell.Row.Cells["UserName"].Value = frmPOP.UserName;
                                e.Cell.Row.Cells["DeptName"].Value = frmPOP.DeptName;
                                e.Cell.Row.Cells["EMail"].Value = frmPOP.EMail;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridQCNUser_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridQCNUser.ActiveCell;
                    if (uCell.Column.Key.Equals("UserID"))
                    {
                        if (!uCell.Text.Equals(string.Empty))
                        {
                            //System ResourceInfo
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                            QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                            brwChannel.mfCredentials(clsUser);

                            DataTable dtUserInfo = clsUser.mfReadSYSUser("", uCell.Text, m_resSys.GetString("SYS_LANG"));

                            if (dtUserInfo.Rows.Count > 0)
                            {
                                uCell.Row.Cells["UserName"].Value = dtUserInfo.Rows[0]["UserName"].ToString();
                                uCell.Row.Cells["DeptName"].Value = dtUserInfo.Rows[0]["DeptName"].ToString();
                                uCell.Row.Cells["EMail"].Value = dtUserInfo.Rows[0]["Email"].ToString();
                            }
                            else
                            {
                                DialogResult Result = new DialogResult();
                                WinMessageBox msg = new WinMessageBox();
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001099", "M000366", "M000368"
                                                            , Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uGridQCNUser.ActiveRow == null)
                    {
                        return;
                    }
                    else
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridQCNUser.ActiveCell;
                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (uCell.Text.Length <= 1 || uCell.SelText == uCell.Text)
                            {
                                uCell.Row.Delete(false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridQCNUser_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정시 RowSelector 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        private void uGridQCNUser_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridQCNUser, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Mail 전송시 메소드
        private void SendMail(string strQCNNo)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                if (this.uCheckPublishFlag.Checked && this.uCheckPublishFlag.Enabled)
                {
                    // 전 , 후 LotNo 검색
                    string strBeLotNo = ""; // 전 -Lot
                    string strAfLotNo = ""; // 후 -Lot

                    if (this.uGridAffectLotList.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                        {
                            if (this.uGridAffectLotList.Rows[i].GetCellValue("LotType").ToString().Equals("B")) // 전 Lot
                                strBeLotNo = this.uGridAffectLotList.Rows[i].GetCellValue("LotNo").ToString();
                            if (this.uGridAffectLotList.Rows[i].GetCellValue("LotType").ToString().Equals("A")) // 후 Lot
                                strAfLotNo = this.uGridAffectLotList.Rows[i].GetCellValue("LotNo").ToString();
                        }
                    }

                    QRPBrowser brwChannel = new QRPBrowser();
                    QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                    //메일에 첨부할 첨부파일명
                    ArrayList arrAttachFile = new ArrayList();
                    //메일 첨부파일에 복사 할 파일 정보
                    ArrayList arrFile = new ArrayList();    //복사할 파일 서버경로, 파일명
                    ArrayList arrAttach = new ArrayList();  //파일을 복사할 경로

                    
                    #region 메일서버에 첨부파일 복사

                        
                    //첨부파일을 위한 FileServer 연결정보
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlantCode.Value.ToString(), "S02");

                    //메일 발송 첨부파일 경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFileDestPath);
                    DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                    //QCN발행 첨부파일 저장경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysTargetFilePath);
                    DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0028");

                    //파일명변수
                    string strFileName = "";

                    if (!this.uTextFaultImg.Equals(string.Empty))
                    {
                        if (this.uTextFaultImg.Text.Contains(":\\")) //경로가 있을경우
                        {
                            FileInfo fileDoc = new FileInfo(this.uTextFaultImg.Text);
                            strFileName = this.uComboPlantCode.Value.ToString() + "-" + strQCNNo + "-F-" + fileDoc.Name;
                        }
                        else
                        {
                            strFileName = this.uTextFaultImg.Text;
                        }

                        string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName; //메일첨부파일 폴더 파일명지정
                        string TargetFile = "INSProcQCNFault/" + strFileName; // 복사할 폴더 경로
                        string NonPathFile = strFileName; //파일명



                        arrAttachFile.Add(TargetFile); // 복사할 폴더 경로
                        arrFile.Add(DestFile); //메일첨부파일 폴더 파일명지정
                        arrAttach.Add(NonPathFile);  //메일에 첨부할 파일명

                        //파일 복사
                        if (arrFile.Count > 0)
                        {
                            fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                                , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                , arrAttachFile
                                                                , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                                , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                                , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        }

                    }

                    if (arrFile.Count != 0)
                    {
                        fileAtt.mfFileUpload_NonAssync();
                    }


                    #endregion
                    
                    for (int i = 0; i < this.uGridQCNUser.Rows.Count; i++)
                    {
                        // Mail 받을사람의 정보 가져오기
                        DataTable dtUserInfo = GetUserInfo(strPlantCode, this.uGridQCNUser.Rows[i].Cells["UserID"].Value.ToString());

                        if (dtUserInfo.Rows.Count > 0)
                        {
                            if (!dtUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                            {
                                //메일보내기
                                string strMail = dtUserInfo.Rows[0]["EMail"].ToString();

                                brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                                QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                                brwChannel.mfCredentials(clsmail);
                                bool bolRtn = clsmail.mfSendSMTPMail(strPlantCode
                                                                , m_resSys.GetString("SYS_EMAIL")
                                                                , m_resSys.GetString("SYS_USERNAME")
                                                                , strMail//보내려는 사람 이메일주소
                                                                , "[QRP].QCN 발행되었습니다."
                                                                , "<HTML>" +
                                                                        "<HEAD>" +
                                                                        "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                        "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                        "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                        "</HEAD>" +
                                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** QCN발행정보 *****</SPAN></STRONG></P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 발행일시 : </SPAN>" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 고객사&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextCustomerName.Text + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. Package: </SPAN>" + this.uTextPackage.Text + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 발생공정 : </SPAN>" + this.uComboAriseProcessCode.Text + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 설비번호 : </SPAN>" + this.uComboAriseEquipCode.Value.ToString() + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. LotNo&nbsp&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextLotNo.Text + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 불량유형 : </SPAN>" + this.uComboInspectFaultTypeCode.Text + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 기생산&nbsp&nbsp&nbsp&nbsp: </SPAN> 전LOT - " + strBeLotNo + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 후LOT - </SPAN>" + strAfLotNo + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 작업자&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextWorkUserName.Text + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 검사자&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextInspectUserName.Text + "</P>" +
                                                                        "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                    "</BODY>" +
                                                                 "</HTML>"
                                                                , arrAttach);

                                if (!bolRtn)
                                {
                                    WinMessageBox msg = new WinMessageBox();
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M000083", strLang), msg.GetMessge_Text("M000106", strLang)
                                            , msg.GetMessge_Text("M000105", strLang) + dtUserInfo.Rows[i]["UserName"].ToString() + msg.GetMessge_Text("M000005", strLang)
                                            , Infragistics.Win.HAlign.Right);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 첨부파일 삭제
        private void uTextFaultImg_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uCheckPublishFlag.Enabled)
                {
                    if (this.uTextFaultImg.Text.Contains(this.uComboPlantCode.Value.ToString()) &&
                        this.uTextFaultImg.Text.Contains(this.uTextQCNNo.Text) &&
                        this.uTextFaultImg.Text.Contains("-F-"))
                    {
                        if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                        {
                            //화일서버 연결정보 가져오기
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                            //첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0028");

                            //첨부파일 삭제하기
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                            arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFaultImg.Text);

                            fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                , arrFile
                                                                , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.mfFileUploadNoProgView();

                            this.uTextFaultImg.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 텍스트박스 버튼 이벤트
        private void uTextFaultImg_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {

                if (e.Button.Key == "UP")
                {
                    if (this.uCheckPublishFlag.Enabled)
                    {
                        if (this.uTextFaultImg.Text.Contains(this.uComboPlantCode.Value.ToString()) &&
                        this.uTextFaultImg.Text.Contains(this.uTextQCNNo.Text) &&
                        this.uTextFaultImg.Text.Contains("-F-"))
                        {
                            FileDelete();
                        }
                        System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                        //openFile.Filter = "Image Files|*.jpg;*.jpeg;*.bmp;*.gif;*.png;*.tiff;*.ico;*.raw;*.pcx|Office Files|*.doc;*.xls;*.ppt|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                        openFile.Filter = "Image Files|*.jpg;*.jpeg;*.bmp;*.gif;*.png;*.tiff;*.ico;*.raw;*.pcx|All Files|*.*";
                        openFile.FilterIndex = 1;
                        openFile.RestoreDirectory = true;

                        if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            string strImageFile = openFile.FileName;
                            this.uTextFaultImg.Text = strImageFile;
                        }
                    }
                }
                else if (e.Button.Key == "DOWN")
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (this.uTextFaultImg.Text.Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000796",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            //화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                            //첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0028");

                            //첨부파일 Download하기
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                            arrFile.Add(this.uTextFaultImg.Text);

                            if (arrFile.Count > 0)
                            {
                                fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.ShowDialog();

                                // 파일 실행시키기
                                System.Diagnostics.Process.Start(strSaveFolder + this.uTextFaultImg.Text);
                            }
                        }

                        //////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();



                        ////////첨부파일 Download하기
                        //////frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        //////System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                        //////arrFile.Add(this.uTextFaultImg.Text);

                        ////////Download정보 설정
                        //////string strExePath = Application.ExecutablePath;
                        //////int intPos = strExePath.LastIndexOf(@"\");
                        //////strExePath = strExePath.Substring(0, intPos + 1);

                        //////fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                        //////                                       dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                        //////                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                        //////                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                        //////                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                        //////                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        //////fileAtt.ShowDialog();

                        //////// 파일 실행시키기
                        //////System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFaultImg.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 Upload 메소드
        /// </summary>
        /// <param name="strQCNNo">발행번호</param>
        private void FileUpload(string strQCNNo)
        {
            try
            {
                if (this.uCheckPublishFlag.Enabled)
                {
                    // 첨부파일 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                    // 불량Image파일
                    if (this.uTextFaultImg.Text.Contains(":\\"))
                    {
                        System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uTextFaultImg.Text);
                        string strUploadFile = fileDoc.DirectoryName + "\\" +
                                               this.uComboPlantCode.Value.ToString() + "-" + strQCNNo + "-F-" + fileDoc.Name;
                        //변경한 화일이 있으면 삭제하기
                        if (System.IO.File.Exists(strUploadFile))
                            System.IO.File.Delete(strUploadFile);
                        //변경한 화일이름으로 복사하기
                        System.IO.File.Copy(this.uTextFaultImg.Text, strUploadFile);
                        arrFile.Add(strUploadFile);
                    }

                    // 업로드할 파일이 있는경우 파일 업로드
                    if (arrFile.Count > 0)
                    {
                        // 화일서버 연결정보 가져오기
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                        // 첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0028");

                        //Upload정보 설정
                        fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 업로드 완료후 복사된 파일 삭제
                        for (int i = 0; i < arrFile.Count; i++)
                        {
                            System.IO.File.Delete(arrFile[i].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 삭제 메소드(전체)
        /// </summary>
        private void FileDelete()
        {
            try
            {
                if (this.uCheckPublishFlag.Enabled)
                {
                    // 첨부파일 저장경로정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0028");

                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                    // AttachmentFile
                    if (this.uTextFaultImg.Text.Contains(this.uComboPlantCode.Value.ToString()) &&
                        this.uTextFaultImg.Text.Contains(this.uTextQCNNo.Text) &&
                        !this.uTextFaultImg.Text.Contains(":\\"))
                    {
                        arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFaultImg.Text);
                    }

                    if (arrFile.Count > 0)
                    {
                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                            , arrFile
                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileUploadNoProgView();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridExpectEquipList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //설비정보저장
                string strEquipCode = e.Row.Cells["EQPID"].Value.ToString();

                //설비코드가 선택 되었을 때 Affect Lot정보를 조회하여 Grid에 바인드 시킨다.
                if (strEquipCode != "")
                {
                    if (this.uCheckPublishFlag.Enabled)
                    {
                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        DialogResult Result = new DialogResult();
                        WinMessageBox msg = new WinMessageBox();

                        //LotNo정보 , 발생공정정보 저장
                        string strLotNo = this.uTextLotNo.Text.Trim();
                        //string strProcess = this.uComboAriseProcessCode.Value.ToString();
                        string strProcess = this.uComboExpectProcessCode.Value.ToString();
                        
                        ////////MES서버경로 가져오기
                        //////QRPBrowser brwChannel = new QRPBrowser();
                        //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        //////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        //////brwChannel.mfCredentials(clsSysAcce);

                        ////////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
                        ////////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");
                        //////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                        //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);

                        ////////MES 로 설비정보 보냄
                        //////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);

                        ////////Affect Lot정보조회 매서드 실행
                        //////DataTable dtAffect = clsTibrv.LOT_AFFECT_REQ(strLotNo, strProcess, strEquipCode);
                        //////clsTibrv.Dispose();

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                        QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                        brwChannel.mfCredentials(clsMES);

                        DataTable dtAffect = clsMES.mfRead_LOT_AFFECT_REQ(this.uComboPlantCode.Value.ToString()
                                                                      , strLotNo, strProcess, strEquipCode);

                        // AffectLot정보가 있을 경우
                        if (dtAffect.Rows.Count > 0)
                        {
                            if (dtAffect.Rows[0]["returncode"].ToString().Equals("0"))
                            {
                                //컬럼명 그리드에 맞게 설정
                                dtAffect.Columns["LOTTYPE"].ColumnName = "LotType";             //Lot구분
                                dtAffect.Columns["LOTID"].ColumnName = "LotNo";                 //LotNo
                                dtAffect.Columns["PRODUCTSPECNAME"].ColumnName = "ProductCode"; //제품코드
                                dtAffect.Columns["OPERID"].ColumnName = "ProcessCode";          //공정코드
                                dtAffect.Columns["EQPID"].ColumnName = "EquipCode";             //설비코드
                                dtAffect.Columns["QTY"].ColumnName = "Qty";                     //수량
                                dtAffect.Columns["EVENTTIME"].ColumnName = "WorkDateTime";      //작업시간
                                dtAffect.Columns["USERID"].ColumnName = "WorkUserID";           //작업자ID

                                //작업자 컬럼추가
                                dtAffect.Columns.Add("WorkUserName", typeof(string));       //작업자
                                dtAffect.Columns.Add("MESHoldTFlag", typeof(string));       // MESHold 여부


                                //사용자정보 BL 호출
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                                brwChannel.mfCredentials(clsUser);

                                // 0부터 MES에서 받아온 정보줄 수 까지 For문을 돌린다.
                                for (int i = 0; i < dtAffect.Rows.Count; i++)
                                {
                                    // WorkUserID가 있는경우
                                    if (dtAffect.Rows[i]["WorkUserID"].ToString() != "")
                                    {
                                        //유저정보 조회 매서드 실행을 한다.
                                        DataTable dtUser = clsUser.mfReadSYSUser("", dtAffect.Rows[i]["WorkUserID"].ToString(), m_resSys.GetString("SYS_LANG"));
                                        //해당 아이디의 정보가 있을 경우 WorkUserName 삽입한다.
                                        if (dtUser.Rows.Count > 0)
                                            dtAffect.Rows[i]["WorkUserName"] = dtUser.Rows[0]["UserName"];
                                    }
                                    dtAffect.Rows[i]["MESHoldTFlag"] = "F";

                                    if (dtAffect.Rows[i]["WorkDateTime"].ToString().Length > 0)
                                    {
                                        char[] ch = dtAffect.Rows[i]["WorkDateTime"].ToString().ToCharArray();
                                        string strCon = ch[0].ToString() + ch[1].ToString() + ch[2].ToString() + ch[3].ToString() + "-" + ch[4].ToString() + ch[5].ToString() + "-" + ch[6].ToString() + ch[7].ToString()
                                            + " " + ch[8].ToString() + ch[9].ToString() + ":" + ch[10].ToString() + ch[11].ToString() + ":" + ch[12].ToString() + ch[13].ToString();
                                        dtAffect.Rows[i]["WorkDateTime"] = strCon;
                                    }

                                    if (dtAffect.Rows[i]["LotNo"].ToString().Length.Equals(0))
                                    {
                                        dtAffect.Rows[i].Delete();
                                        i--;
                                    }
                                }


                                //그리드에 바인드
                                this.uGridAffectLotList.DataSource = dtAffect;
                                this.uGridAffectLotList.DataBind();
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001115", strLang)
                                    , dtAffect.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            }
                        }
                        else
                        {
                            // 그리드 초기화
                            while (this.uGridAffectLotList.Rows.Count > 0)
                            {
                                this.uGridAffectLotList.Rows[0].Delete(false);
                            }
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001115", "M000027", Infragistics.Win.HAlign.Right);

                            //this.uComboAriseEquipCode.Value = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region Test
        private void TestSendMail(string strQCNNo, string strMail)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                //if (this.uCheckPublishFlag.Checked && this.uCheckPublishFlag.Enabled)
                //{
                    // 전 , 후 LotNo 검색
                    string strBeLotNo = ""; // 전 -Lot
                    string strAfLotNo = ""; // 후 -Lot

                    if (this.uGridAffectLotList.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                        {
                            if (this.uGridAffectLotList.Rows[i].GetCellValue("LotType").ToString().Equals("B")) // 전 Lot
                                strBeLotNo = this.uGridAffectLotList.Rows[i].GetCellValue("LotNo").ToString();
                            if (this.uGridAffectLotList.Rows[i].GetCellValue("LotType").ToString().Equals("A")) // 후 Lot
                                strAfLotNo = this.uGridAffectLotList.Rows[i].GetCellValue("LotNo").ToString();
                        }
                    }

                    QRPBrowser brwChannel = new QRPBrowser();
                    QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                    //메일에 첨부할 첨부파일명
                    ArrayList arrAttachFile = new ArrayList();
                    //메일 첨부파일에 복사 할 파일 정보
                    ArrayList arrFile = new ArrayList();    //복사할 파일 서버경로, 파일명
                    ArrayList arrAttach = new ArrayList();  //파일을 복사할 경로


                    #region 메일서버에 첨부파일 복사


                    //첨부파일을 위한 FileServer 연결정보
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlantCode.Value.ToString(), "S02");

                    //메일 발송 첨부파일 경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFileDestPath);
                    DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                    //QCN발행 첨부파일 저장경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysTargetFilePath);
                    DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0028");

                    //파일명변수
                    string strFileName = "";

                    if (!this.uTextFaultImg.Equals(string.Empty))
                    {
                        if (this.uTextFaultImg.Text.Contains(":\\")) //경로가 있을경우
                        {
                            FileInfo fileDoc = new FileInfo(this.uTextFaultImg.Text);
                            strFileName = this.uComboPlantCode.Value.ToString() + "-" + strQCNNo + "-F-" + fileDoc.Name;
                        }
                        else
                        {
                            strFileName = this.uTextFaultImg.Text;
                        }

                        string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName; //메일첨부파일 폴더 파일명지정
                        string TargetFile = "INSProcQCNFault/" + strFileName; // 복사할 폴더 경로
                        string NonPathFile = strFileName; //파일명



                        arrAttachFile.Add(TargetFile); // 복사할 폴더 경로
                        arrFile.Add(DestFile); //메일첨부파일 폴더 파일명지정
                        arrAttach.Add(NonPathFile);  //메일에 첨부할 파일명

                        //파일 복사
                        if (arrFile.Count > 0)
                        {
                            fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                                , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                , arrAttachFile
                                                                , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                                , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                                , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        }

                    }

                    if (arrFile.Count != 0)
                    {
                        fileAtt.mfFileUpload_NonAssync();
                    }


                    #endregion

                    brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                    QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                    brwChannel.mfCredentials(clsmail);
                    bool bolRtn = clsmail.mfSendSMTPMail(strPlantCode
                                                    , m_resSys.GetString("SYS_EMAIL")
                                                    , m_resSys.GetString("SYS_USERNAME")
                                                    , strMail//보내려는 사람 이메일주소
                                                    , "[QRP].QCN 발행되었습니다." 
                                                    , "<HTML>" +
                                                            "<HEAD>" +
                                                            "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                            "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                            "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                            "</HEAD>" +
                                                        "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** QCN발행정보 *****</SPAN></STRONG></P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 발행일시 : </SPAN>" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 고객사&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextCustomerName.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. Package: </SPAN>" + this.uTextPackage.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 발생공정 : </SPAN>" + this.uComboAriseProcessCode.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 설비번호 : </SPAN>" + this.uComboAriseEquipCode.Value.ToString() + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. LotNo&nbsp&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextLotNo.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 불량유형 : </SPAN>" + this.uComboInspectFaultTypeCode.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 기생산&nbsp&nbsp&nbsp&nbsp:  전 LOT - </SPAN>" + strBeLotNo + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp후 LOT - </SPAN>" + strAfLotNo + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 작업자&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextWorkUserName.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 검사자&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextInspectUserName.Text + "</P>" +
                                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                        "</BODY>" +
                                                     "</HTML>"
                                                    , arrAttach);

                    if (!bolRtn)
                    {
                        //WinMessageBox msg = new WinMessageBox();
                        //string strLang = m_resSys.GetString("SYS_LANG");
                        //msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        //        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //        , msg.GetMessge_Text("M000083", strLang), msg.GetMessge_Text("M000106", strLang)
                        //        , msg.GetMessge_Text("M000105", strLang) + dtUserInfo.Rows[i]["UserName"].ToString() + msg.GetMessge_Text("M000005", strLang)
                        //        , Infragistics.Win.HAlign.Right);
                        //return;

                    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            TestSendMail(this.uTextQCNNo.Text, "djinkim@bokwang.com"); //mespjt36@bokwang.com,djinkim@bokwang.com
        }

        //private void uComboExpectEquipCode_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        //{
        //    try
        //    {
        //        if (this.uComboExpectEquipCode.Value != null)
        //        {
        //            //설비정보저장
        //            string strEquipCode = this.uComboExpectEquipCode.Value.ToString();

        //            //설비코드가 선택 되었을 때 Affect Lot정보를 조회하여 Grid에 바인드 시킨다.
        //            if (strEquipCode != "")
        //            {
        //                if (this.uCheckPublishFlag.Checked == false)
        //                {
        //                    //if(this.uGridQCNList.DoubleClickRow == 
        //                    //System ResourceInfo
        //                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //                    WinMessageBox msg = new WinMessageBox();

        //                    //LotNo정보 , 예상공정정보 저장
        //                    string strLotNo = this.uTextLotNo.Text.Trim();
        //                    string strProcess = this.uComboExpectProcessCode.Value.ToString();

        //                    //MES서버경로 가져오기
        //                    QRPBrowser brwChannel = new QRPBrowser();
        //                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
        //                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
        //                    brwChannel.mfCredentials(clsSysAcce);

        //                    DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");

        //                    //MES 로 설비정보 보냄
        //                    QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);

        //                    //Affect Lot정보조회 매서드 실행
        //                    DataTable dtAffect = clsTibrv.LOT_AFFECT_REQ(strLotNo, strProcess, strEquipCode);

        //                    //Affect Lot정보가 있을 경우
        //                    if (dtAffect.Columns.Count > 0)
        //                    {
        //                        if (!dtAffect.Rows[0]["LOTID"].ToString().Equals(string.Empty))
        //                        {
        //                            //컬럼명 그리드에 맞게 설정
        //                            dtAffect.Columns["LOTTYPE"].ColumnName = "LotType";             //Lot구분
        //                            dtAffect.Columns["LOTID"].ColumnName = "LotNo";                 //LotNo
        //                            dtAffect.Columns["PRODUCTSPECNAME"].ColumnName = "ProductCode"; //제품코드
        //                            dtAffect.Columns["OPERID"].ColumnName = "ProcessCode";          //공정코드
        //                            dtAffect.Columns["EQPID"].ColumnName = "EquipCode";             //설비코드
        //                            dtAffect.Columns["QTY"].ColumnName = "Qty";                     //수량
        //                            dtAffect.Columns["EVENTTIME"].ColumnName = "WorkDateTime";      //작업시간
        //                            dtAffect.Columns["USERID"].ColumnName = "WorkUserID";           //작업자ID

        //                            //작업자 컬럼추가
        //                            dtAffect.Columns.Add("WorkUserName", typeof(string));           //작업자

        //                            // AffectLot정보가 있을 경우
        //                            if (dtAffect.Rows.Count > 0)
        //                            {
        //                                //사용자정보 BL 호출
        //                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
        //                                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
        //                                brwChannel.mfCredentials(clsUser);

        //                                // 0부터 MES에서 받아온 정보줄 수 까지 For문을 돌린다.
        //                                for (int i = 0; i < dtAffect.Rows.Count; i++)
        //                                {
        //                                    // WorkUserID가 있는경우
        //                                    if (dtAffect.Rows[i]["WorkUserID"].ToString() != "")
        //                                    {
        //                                        //유저정보 조회 매서드 실행을 한다.
        //                                        DataTable dtUser = clsUser.mfReadSYSUser("", dtAffect.Rows[i]["WorkUserID"].ToString(), m_resSys.GetString("SYS_LANG"));
        //                                        //해당 아이디의 정보가 있을 경우 WorkUserName 삽입한다.
        //                                        if (dtUser.Rows.Count > 0)
        //                                            dtAffect.Rows[i]["WorkUserName"] = dtUser.Rows[0]["UserName"];
        //                                    }
        //                                }
        //                            }

        //                            //그리드에 바인드
        //                            this.uGridAffectLotList.DataSource = dtAffect;
        //                            this.uGridAffectLotList.DataBind();
        //                        }
        //                        else
        //                        {
        //                            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        //                                , "확인창", "조회 처리결과 ", "Affect Lot정보가 없습니다.", Infragistics.Win.HAlign.Right);

        //                            //this.uComboExpectEquipCode.Value = string.Empty;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        // 그리드 초기화
        //                        while (this.uGridAffectLotList.Rows.Count > 0)
        //                        {
        //                            this.uGridAffectLotList.Rows[0].Delete(false);
        //                        }

        //                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        //                            , "확인창", "조회 처리결과 ", "Affect Lot정보가 없습니다.", Infragistics.Win.HAlign.Right);

        //                        //this.uComboExpectEquipCode.Value = string.Empty;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { 
        //    }
        //}

        ////#region 공정, 설비명 검색(검색시 콤보그리드값)
        /////// <summary>
        /////// 공정명 검색
        /////// </summary>
        /////// <param name="strPlantCode"></param>
        /////// <param name="strProcessCode"></param>
        /////// <returns></returns>
        ////public String GetProcessName(String strPlantCode, String strProcessCode)
        ////{
        ////    String strRtnName = "";
        ////    try
        ////    {
        ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
        ////        QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process();
        ////        brwChannel.mfCredentials(clsProc);

        ////        DataTable dtProcName = clsProc.mfReadProcessName(strPlantCode, strProcessCode, m_resSys.GetString("SYS_LANG"));
        ////        if(dtProcName.Rows.Count > 0)
        ////        {
        ////            strRtnName = dtProcName.Rows[0]["ProcessName"].ToString();
        ////        }
        ////        return strRtnName;
        ////    }
        ////    catch(Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////        return strRtnName;
        ////    }
        ////    finally
        ////    {}
        ////}
        /////// <summary>
        /////// 설비명 검색
        /////// </summary>
        /////// <param name="strPlantCode"></param>
        /////// <param name="strEquipCode"></param>
        /////// <returns></returns>
        ////public String GetEquipName(String strPlantCode, String strEquipCode)
        ////{
        ////    String strRtnName = "";
        ////    try
        ////    {
        ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        ////        QRPMAS.BL.MASEQU.Equip clsEqu = new QRPMAS.BL.MASEQU.Equip();
        ////        brwChannel.mfCredentials(clsEqu);

        ////        DataTable dtEquName = clsEqu.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));
        ////        if (dtEquName.Rows.Count > 0)
        ////        {
        ////            strRtnName = dtEquName.Rows[0]["EquipName"].ToString();
        ////        }
        ////        return strRtnName;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////        return strRtnName;
        ////    }
        ////    finally
        ////    { }
        ////}
        ////#endregion
        #endregion

    }
}
