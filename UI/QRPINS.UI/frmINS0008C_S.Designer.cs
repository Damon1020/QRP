﻿namespace QRPINS.UI
{
	partial class frmINS0008C_S
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINS0008C_S));
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridItem = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridSampling = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridFaultData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextQCNorITRCheck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonDelete2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridFault = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextMESTrackInTDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTestProcessFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProgram = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProgram = new Infragistics.Win.Misc.UltraLabel();
            this.uComboHandler = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelHandler = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCompleteCheck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTrackInTCheck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSPCNCheck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextOUTSOURCINGVENDOR = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOUTSOURCINGVENDOR = new Infragistics.Win.Misc.UltraLabel();
            this.uComboWorkProcess = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uComboEquip = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextProcessHoldState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProcessHoldState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelNowProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWokrUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerProductCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextNowProcessCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextNowProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotProcessState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotProcessState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWorkProcessCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESSPCNTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESHoldTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTrackOutTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTrackInTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboProcInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotSize = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextPCB_Layout = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboWafer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelWafer = new Infragistics.Win.Misc.UltraLabel();
            this.uNumLossQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelLossQty = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLossCheck = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckLossCheckFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uComboStack = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStack = new Infragistics.Win.Misc.UltraLabel();
            this.uDateInspectTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uButtonTrackIn = new Infragistics.Win.Misc.UltraButton();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uOptionPassFailFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelPassFailFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTextFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridItem)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSampling)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFaultData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQCNorITRCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTestProcessFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHandler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSPCNCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOUTSOURCINGVENDOR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWorkProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessHoldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESSPCNTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESHoldTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackOutTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCB_Layout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWafer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumLossQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckLossCheckFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFile)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridItem);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(907, 551);
            // 
            // uGridItem
            // 
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridItem.DisplayLayout.Appearance = appearance11;
            this.uGridItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.uGridItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItem.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.uGridItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridItem.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridItem.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.uGridItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridItem.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridItem.DisplayLayout.Override.CellPadding = 0;
            appearance16.BackColor = System.Drawing.SystemColors.Control;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.Override.GroupByRowAppearance = appearance16;
            appearance52.TextHAlignAsString = "Left";
            this.uGridItem.DisplayLayout.Override.HeaderAppearance = appearance52;
            this.uGridItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridItem.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGridItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridItem.Location = new System.Drawing.Point(0, 0);
            this.uGridItem.Name = "uGridItem";
            this.uGridItem.Size = new System.Drawing.Size(907, 551);
            this.uGridItem.TabIndex = 1;
            this.uGridItem.Text = "ultraGrid1";
            this.uGridItem.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridItem_InitializeRow);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridSampling);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(907, 551);
            // 
            // uGridSampling
            // 
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSampling.DisplayLayout.Appearance = appearance20;
            this.uGridSampling.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSampling.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSampling.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSampling.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGridSampling.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSampling.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.uGridSampling.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSampling.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSampling.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSampling.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.uGridSampling.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSampling.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSampling.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSampling.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridSampling.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSampling.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSampling.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.uGridSampling.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.uGridSampling.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSampling.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.uGridSampling.DisplayLayout.Override.RowAppearance = appearance32;
            this.uGridSampling.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSampling.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridSampling.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSampling.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSampling.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSampling.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSampling.Location = new System.Drawing.Point(0, 0);
            this.uGridSampling.Name = "uGridSampling";
            this.uGridSampling.Size = new System.Drawing.Size(907, 551);
            this.uGridSampling.TabIndex = 1;
            this.uGridSampling.Text = "ultraGrid1";
            this.uGridSampling.AfterExitEditMode += new System.EventHandler(this.uGridSampling_AfterExitEditMode);
            this.uGridSampling.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSampling_AfterCellUpdate);
            this.uGridSampling.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridSampling_KeyDown);
            this.uGridSampling.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridSampling_InitializeRow);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGroupBox3);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(907, 551);
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBox3.Controls.Add(this.ultraGroupBox5);
            this.uGroupBox3.Controls.Add(this.ultraGroupBox4);
            this.uGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(907, 551);
            this.uGroupBox3.TabIndex = 7;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.uGridFaultData);
            this.ultraGroupBox5.Controls.Add(this.ultraGroupBox6);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox5.Location = new System.Drawing.Point(1, 260);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(905, 290);
            this.ultraGroupBox5.TabIndex = 67;
            // 
            // uGridFaultData
            // 
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFaultData.DisplayLayout.Appearance = appearance74;
            this.uGridFaultData.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFaultData.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance75.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance75.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance75.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFaultData.DisplayLayout.GroupByBox.Appearance = appearance75;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFaultData.DisplayLayout.GroupByBox.BandLabelAppearance = appearance76;
            this.uGridFaultData.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance77.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance77.BackColor2 = System.Drawing.SystemColors.Control;
            appearance77.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance77.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFaultData.DisplayLayout.GroupByBox.PromptAppearance = appearance77;
            this.uGridFaultData.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFaultData.DisplayLayout.MaxRowScrollRegions = 1;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFaultData.DisplayLayout.Override.ActiveCellAppearance = appearance78;
            appearance79.BackColor = System.Drawing.SystemColors.Highlight;
            appearance79.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFaultData.DisplayLayout.Override.ActiveRowAppearance = appearance79;
            this.uGridFaultData.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFaultData.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFaultData.DisplayLayout.Override.CardAreaAppearance = appearance80;
            appearance81.BorderColor = System.Drawing.Color.Silver;
            appearance81.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFaultData.DisplayLayout.Override.CellAppearance = appearance81;
            this.uGridFaultData.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFaultData.DisplayLayout.Override.CellPadding = 0;
            appearance82.BackColor = System.Drawing.SystemColors.Control;
            appearance82.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance82.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance82.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFaultData.DisplayLayout.Override.GroupByRowAppearance = appearance82;
            appearance83.TextHAlignAsString = "Left";
            this.uGridFaultData.DisplayLayout.Override.HeaderAppearance = appearance83;
            this.uGridFaultData.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFaultData.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance84.BackColor = System.Drawing.SystemColors.Window;
            appearance84.BorderColor = System.Drawing.Color.Silver;
            this.uGridFaultData.DisplayLayout.Override.RowAppearance = appearance84;
            this.uGridFaultData.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance85.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFaultData.DisplayLayout.Override.TemplateAddRowAppearance = appearance85;
            this.uGridFaultData.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFaultData.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFaultData.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFaultData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFaultData.Location = new System.Drawing.Point(3, 40);
            this.uGridFaultData.Name = "uGridFaultData";
            this.uGridFaultData.Size = new System.Drawing.Size(899, 247);
            this.uGridFaultData.TabIndex = 9;
            this.uGridFaultData.Text = "ultraGrid1";
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.uTextQCNorITRCheck);
            this.ultraGroupBox6.Controls.Add(this.uButtonDelete2);
            this.ultraGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox6.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(899, 40);
            this.ultraGroupBox6.TabIndex = 0;
            // 
            // uTextQCNorITRCheck
            // 
            appearance95.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQCNorITRCheck.Appearance = appearance95;
            this.uTextQCNorITRCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQCNorITRCheck.Location = new System.Drawing.Point(89, 12);
            this.uTextQCNorITRCheck.Name = "uTextQCNorITRCheck";
            this.uTextQCNorITRCheck.ReadOnly = true;
            this.uTextQCNorITRCheck.Size = new System.Drawing.Size(123, 21);
            this.uTextQCNorITRCheck.TabIndex = 68;
            // 
            // uButtonDelete2
            // 
            this.uButtonDelete2.Location = new System.Drawing.Point(10, 8);
            this.uButtonDelete2.Name = "uButtonDelete2";
            this.uButtonDelete2.Size = new System.Drawing.Size(75, 28);
            this.uButtonDelete2.TabIndex = 67;
            this.uButtonDelete2.Text = "ultraButton1";
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.uGridFault);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox4.Location = new System.Drawing.Point(1, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(905, 260);
            this.ultraGroupBox4.TabIndex = 62;
            // 
            // uGridFault
            // 
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFault.DisplayLayout.Appearance = appearance65;
            this.uGridFault.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFault.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFault.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFault.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.uGridFault.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFault.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.uGridFault.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFault.DisplayLayout.MaxRowScrollRegions = 1;
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFault.DisplayLayout.Override.ActiveCellAppearance = appearance73;
            appearance68.BackColor = System.Drawing.SystemColors.Highlight;
            appearance68.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFault.DisplayLayout.Override.ActiveRowAppearance = appearance68;
            this.uGridFault.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFault.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFault.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance66.BorderColor = System.Drawing.Color.Silver;
            appearance66.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFault.DisplayLayout.Override.CellAppearance = appearance66;
            this.uGridFault.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFault.DisplayLayout.Override.CellPadding = 0;
            appearance70.BackColor = System.Drawing.SystemColors.Control;
            appearance70.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance70.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance70.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance70.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFault.DisplayLayout.Override.GroupByRowAppearance = appearance70;
            appearance72.TextHAlignAsString = "Left";
            this.uGridFault.DisplayLayout.Override.HeaderAppearance = appearance72;
            this.uGridFault.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFault.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.uGridFault.DisplayLayout.Override.RowAppearance = appearance71;
            this.uGridFault.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFault.DisplayLayout.Override.TemplateAddRowAppearance = appearance69;
            this.uGridFault.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFault.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFault.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFault.Location = new System.Drawing.Point(3, 0);
            this.uGridFault.Name = "uGridFault";
            this.uGridFault.Size = new System.Drawing.Size(899, 257);
            this.uGridFault.TabIndex = 62;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uTextMESTrackInTDate);
            this.ultraGroupBox1.Controls.Add(this.uTextTestProcessFlag);
            this.ultraGroupBox1.Controls.Add(this.uTextProgram);
            this.ultraGroupBox1.Controls.Add(this.uLabelProgram);
            this.ultraGroupBox1.Controls.Add(this.uComboHandler);
            this.ultraGroupBox1.Controls.Add(this.uLabelHandler);
            this.ultraGroupBox1.Controls.Add(this.uTextCompleteCheck);
            this.ultraGroupBox1.Controls.Add(this.uTextMESTrackInTCheck);
            this.ultraGroupBox1.Controls.Add(this.uTextSPCNCheck);
            this.ultraGroupBox1.Controls.Add(this.uTextOUTSOURCINGVENDOR);
            this.ultraGroupBox1.Controls.Add(this.uLabelOUTSOURCINGVENDOR);
            this.ultraGroupBox1.Controls.Add(this.uComboWorkProcess);
            this.ultraGroupBox1.Controls.Add(this.uComboEquip);
            this.ultraGroupBox1.Controls.Add(this.uTextProcessHoldState);
            this.ultraGroupBox1.Controls.Add(this.uLabelProcessHoldState);
            this.ultraGroupBox1.Controls.Add(this.uTextWorkUserID);
            this.ultraGroupBox1.Controls.Add(this.uTextEquipName);
            this.ultraGroupBox1.Controls.Add(this.uTextCustomerName);
            this.ultraGroupBox1.Controls.Add(this.uTextProductName);
            this.ultraGroupBox1.Controls.Add(this.uLabelNowProcess);
            this.ultraGroupBox1.Controls.Add(this.uTextWorkUserName);
            this.ultraGroupBox1.Controls.Add(this.uLabelWokrUser);
            this.ultraGroupBox1.Controls.Add(this.uLabelCustomerProductCode);
            this.ultraGroupBox1.Controls.Add(this.uTextCustomerProductCode);
            this.ultraGroupBox1.Controls.Add(this.uTextNowProcessCode);
            this.ultraGroupBox1.Controls.Add(this.uTextNowProcessName);
            this.ultraGroupBox1.Controls.Add(this.uLabelPackage);
            this.ultraGroupBox1.Controls.Add(this.uTextLotProcessState);
            this.ultraGroupBox1.Controls.Add(this.uLabelLotProcessState);
            this.ultraGroupBox1.Controls.Add(this.uTextEquipCode);
            this.ultraGroupBox1.Controls.Add(this.uTextWorkProcessCode);
            this.ultraGroupBox1.Controls.Add(this.uTextPackage);
            this.ultraGroupBox1.Controls.Add(this.uTextMESSPCNTFlag);
            this.ultraGroupBox1.Controls.Add(this.uTextMESHoldTFlag);
            this.ultraGroupBox1.Controls.Add(this.uTextMESTrackOutTFlag);
            this.ultraGroupBox1.Controls.Add(this.uTextMESTrackInTFlag);
            this.ultraGroupBox1.Controls.Add(this.uTextReqSeq);
            this.ultraGroupBox1.Controls.Add(this.uTextReqNo);
            this.ultraGroupBox1.Controls.Add(this.uTextCustomerCode);
            this.ultraGroupBox1.Controls.Add(this.uLabelCustomer);
            this.ultraGroupBox1.Controls.Add(this.uTextWorkProcessName);
            this.ultraGroupBox1.Controls.Add(this.uLabelWorkProcess);
            this.ultraGroupBox1.Controls.Add(this.uComboProcInspectType);
            this.ultraGroupBox1.Controls.Add(this.uLabelProcInspectType);
            this.ultraGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.ultraGroupBox1.Controls.Add(this.uLabelEtcDesc);
            this.ultraGroupBox1.Controls.Add(this.uTextLotNo);
            this.ultraGroupBox1.Controls.Add(this.uLabelEquip);
            this.ultraGroupBox1.Controls.Add(this.uTextLotSize);
            this.ultraGroupBox1.Controls.Add(this.uLabelLotSize);
            this.ultraGroupBox1.Controls.Add(this.uLabelLotNo);
            this.ultraGroupBox1.Controls.Add(this.uTextProductCode);
            this.ultraGroupBox1.Controls.Add(this.uLabelProduct);
            this.ultraGroupBox1.Controls.Add(this.uComboPlantCode);
            this.ultraGroupBox1.Controls.Add(this.uLabelPlant);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 40);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(917, 160);
            this.ultraGroupBox1.TabIndex = 2;
            // 
            // uTextMESTrackInTDate
            // 
            appearance96.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTDate.Appearance = appearance96;
            this.uTextMESTrackInTDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTDate.Location = new System.Drawing.Point(843, 48);
            this.uTextMESTrackInTDate.Name = "uTextMESTrackInTDate";
            this.uTextMESTrackInTDate.ReadOnly = true;
            this.uTextMESTrackInTDate.Size = new System.Drawing.Size(17, 21);
            this.uTextMESTrackInTDate.TabIndex = 124;
            this.uTextMESTrackInTDate.Visible = false;
            // 
            // uTextTestProcessFlag
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTestProcessFlag.Appearance = appearance53;
            this.uTextTestProcessFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTestProcessFlag.Location = new System.Drawing.Point(895, 4);
            this.uTextTestProcessFlag.Name = "uTextTestProcessFlag";
            this.uTextTestProcessFlag.ReadOnly = true;
            this.uTextTestProcessFlag.Size = new System.Drawing.Size(17, 21);
            this.uTextTestProcessFlag.TabIndex = 123;
            this.uTextTestProcessFlag.Visible = false;
            // 
            // uTextProgram
            // 
            this.uTextProgram.Location = new System.Drawing.Point(651, 108);
            this.uTextProgram.Name = "uTextProgram";
            this.uTextProgram.Size = new System.Drawing.Size(200, 21);
            this.uTextProgram.TabIndex = 122;
            // 
            // uLabelProgram
            // 
            this.uLabelProgram.Location = new System.Drawing.Point(562, 108);
            this.uLabelProgram.Name = "uLabelProgram";
            this.uLabelProgram.Size = new System.Drawing.Size(86, 20);
            this.uLabelProgram.TabIndex = 121;
            this.uLabelProgram.Text = "ultraLabel2";
            // 
            // uComboHandler
            // 
            this.uComboHandler.Location = new System.Drawing.Point(651, 84);
            this.uComboHandler.Name = "uComboHandler";
            this.uComboHandler.Size = new System.Drawing.Size(200, 21);
            this.uComboHandler.TabIndex = 120;
            this.uComboHandler.Text = "ultraComboEditor1";
            // 
            // uLabelHandler
            // 
            this.uLabelHandler.Location = new System.Drawing.Point(562, 84);
            this.uLabelHandler.Name = "uLabelHandler";
            this.uLabelHandler.Size = new System.Drawing.Size(86, 20);
            this.uLabelHandler.TabIndex = 119;
            this.uLabelHandler.Text = "ultraLabel1";
            // 
            // uTextCompleteCheck
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteCheck.Appearance = appearance57;
            this.uTextCompleteCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteCheck.Location = new System.Drawing.Point(891, 96);
            this.uTextCompleteCheck.Name = "uTextCompleteCheck";
            this.uTextCompleteCheck.ReadOnly = true;
            this.uTextCompleteCheck.Size = new System.Drawing.Size(17, 21);
            this.uTextCompleteCheck.TabIndex = 116;
            this.uTextCompleteCheck.Visible = false;
            // 
            // uTextMESTrackInTCheck
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTCheck.Appearance = appearance7;
            this.uTextMESTrackInTCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTCheck.Location = new System.Drawing.Point(891, 72);
            this.uTextMESTrackInTCheck.Name = "uTextMESTrackInTCheck";
            this.uTextMESTrackInTCheck.ReadOnly = true;
            this.uTextMESTrackInTCheck.Size = new System.Drawing.Size(17, 21);
            this.uTextMESTrackInTCheck.TabIndex = 117;
            this.uTextMESTrackInTCheck.Visible = false;
            // 
            // uTextSPCNCheck
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSPCNCheck.Appearance = appearance18;
            this.uTextSPCNCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSPCNCheck.Location = new System.Drawing.Point(891, 48);
            this.uTextSPCNCheck.Name = "uTextSPCNCheck";
            this.uTextSPCNCheck.ReadOnly = true;
            this.uTextSPCNCheck.Size = new System.Drawing.Size(17, 21);
            this.uTextSPCNCheck.TabIndex = 118;
            this.uTextSPCNCheck.Visible = false;
            // 
            // uTextOUTSOURCINGVENDOR
            // 
            this.uTextOUTSOURCINGVENDOR.Location = new System.Drawing.Point(867, 8);
            this.uTextOUTSOURCINGVENDOR.Name = "uTextOUTSOURCINGVENDOR";
            this.uTextOUTSOURCINGVENDOR.Size = new System.Drawing.Size(17, 21);
            this.uTextOUTSOURCINGVENDOR.TabIndex = 115;
            this.uTextOUTSOURCINGVENDOR.Visible = false;
            // 
            // uLabelOUTSOURCINGVENDOR
            // 
            this.uLabelOUTSOURCINGVENDOR.Location = new System.Drawing.Point(847, 8);
            this.uLabelOUTSOURCINGVENDOR.Name = "uLabelOUTSOURCINGVENDOR";
            this.uLabelOUTSOURCINGVENDOR.Size = new System.Drawing.Size(17, 20);
            this.uLabelOUTSOURCINGVENDOR.TabIndex = 114;
            this.uLabelOUTSOURCINGVENDOR.Text = "ultraLabel2";
            this.uLabelOUTSOURCINGVENDOR.Visible = false;
            // 
            // uComboWorkProcess
            // 
            this.uComboWorkProcess.CheckedListSettings.CheckStateMember = "";
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboWorkProcess.DisplayLayout.Appearance = appearance49;
            this.uComboWorkProcess.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboWorkProcess.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance58.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance58.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance58.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboWorkProcess.DisplayLayout.GroupByBox.Appearance = appearance58;
            appearance59.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboWorkProcess.DisplayLayout.GroupByBox.BandLabelAppearance = appearance59;
            this.uComboWorkProcess.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance60.BackColor2 = System.Drawing.SystemColors.Control;
            appearance60.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance60.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboWorkProcess.DisplayLayout.GroupByBox.PromptAppearance = appearance60;
            this.uComboWorkProcess.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboWorkProcess.DisplayLayout.MaxRowScrollRegions = 1;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboWorkProcess.DisplayLayout.Override.ActiveCellAppearance = appearance61;
            appearance99.BackColor = System.Drawing.SystemColors.Highlight;
            appearance99.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboWorkProcess.DisplayLayout.Override.ActiveRowAppearance = appearance99;
            this.uComboWorkProcess.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboWorkProcess.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            this.uComboWorkProcess.DisplayLayout.Override.CardAreaAppearance = appearance87;
            appearance88.BorderColor = System.Drawing.Color.Silver;
            appearance88.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboWorkProcess.DisplayLayout.Override.CellAppearance = appearance88;
            this.uComboWorkProcess.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboWorkProcess.DisplayLayout.Override.CellPadding = 0;
            appearance89.BackColor = System.Drawing.SystemColors.Control;
            appearance89.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance89.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance89.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance89.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboWorkProcess.DisplayLayout.Override.GroupByRowAppearance = appearance89;
            appearance90.TextHAlignAsString = "Left";
            this.uComboWorkProcess.DisplayLayout.Override.HeaderAppearance = appearance90;
            this.uComboWorkProcess.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboWorkProcess.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.BorderColor = System.Drawing.Color.Silver;
            this.uComboWorkProcess.DisplayLayout.Override.RowAppearance = appearance91;
            this.uComboWorkProcess.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance93.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboWorkProcess.DisplayLayout.Override.TemplateAddRowAppearance = appearance93;
            this.uComboWorkProcess.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboWorkProcess.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboWorkProcess.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboWorkProcess.Location = new System.Drawing.Point(651, 36);
            this.uComboWorkProcess.Name = "uComboWorkProcess";
            this.uComboWorkProcess.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboWorkProcess.Size = new System.Drawing.Size(200, 22);
            this.uComboWorkProcess.TabIndex = 111;
            this.uComboWorkProcess.Text = "ultraCombo1";
            this.uComboWorkProcess.Enter += new System.EventHandler(this.uComboWorkProcess_Enter);
            this.uComboWorkProcess.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboWorkProcess_BeforeDropDown);
            this.uComboWorkProcess.ValueChanged += new System.EventHandler(this.uComboWorkProcess_ValueChanged_1);
            // 
            // uComboEquip
            // 
            this.uComboEquip.Location = new System.Drawing.Point(651, 60);
            this.uComboEquip.Name = "uComboEquip";
            this.uComboEquip.Size = new System.Drawing.Size(200, 21);
            this.uComboEquip.TabIndex = 109;
            this.uComboEquip.Text = "ultraComboEditor1";
            // 
            // uTextProcessHoldState
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessHoldState.Appearance = appearance21;
            this.uTextProcessHoldState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessHoldState.Location = new System.Drawing.Point(357, 108);
            this.uTextProcessHoldState.Name = "uTextProcessHoldState";
            this.uTextProcessHoldState.ReadOnly = true;
            this.uTextProcessHoldState.Size = new System.Drawing.Size(197, 21);
            this.uTextProcessHoldState.TabIndex = 108;
            // 
            // uLabelProcessHoldState
            // 
            this.uLabelProcessHoldState.Location = new System.Drawing.Point(233, 108);
            this.uLabelProcessHoldState.Name = "uLabelProcessHoldState";
            this.uLabelProcessHoldState.Size = new System.Drawing.Size(120, 20);
            this.uLabelProcessHoldState.TabIndex = 107;
            this.uLabelProcessHoldState.Text = "ultraLabel2";
            // 
            // uTextWorkUserID
            // 
            appearance39.BackColor = System.Drawing.Color.White;
            this.uTextWorkUserID.Appearance = appearance39;
            this.uTextWorkUserID.BackColor = System.Drawing.Color.White;
            appearance92.Image = ((object)(resources.GetObject("appearance92.Image")));
            appearance92.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance92;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Visible = false;
            this.uTextWorkUserID.ButtonsRight.Add(editorButton1);
            this.uTextWorkUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkUserID.Location = new System.Drawing.Point(209, 132);
            this.uTextWorkUserID.Name = "uTextWorkUserID";
            this.uTextWorkUserID.Size = new System.Drawing.Size(18, 21);
            this.uTextWorkUserID.TabIndex = 104;
            this.uTextWorkUserID.Visible = false;
            this.uTextWorkUserID.ValueChanged += new System.EventHandler(this.uTextWorkUserID_ValueChanged);
            this.uTextWorkUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWorkUserID_KeyDown);
            this.uTextWorkUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWorkUserID_EditorButtonClick);
            // 
            // uTextEquipName
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance38;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(871, 96);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(18, 21);
            this.uTextEquipName.TabIndex = 42;
            this.uTextEquipName.Visible = false;
            // 
            // uTextCustomerName
            // 
            appearance50.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance50;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(531, 12);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(18, 21);
            this.uTextCustomerName.TabIndex = 51;
            this.uTextCustomerName.Visible = false;
            // 
            // uTextProductName
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance22;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(531, 36);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(18, 21);
            this.uTextProductName.TabIndex = 36;
            this.uTextProductName.Visible = false;
            // 
            // uLabelNowProcess
            // 
            this.uLabelNowProcess.Location = new System.Drawing.Point(562, 12);
            this.uLabelNowProcess.Name = "uLabelNowProcess";
            this.uLabelNowProcess.Size = new System.Drawing.Size(86, 20);
            this.uLabelNowProcess.TabIndex = 106;
            this.uLabelNowProcess.Text = "ultraLabel2";
            // 
            // uTextWorkUserName
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Appearance = appearance5;
            this.uTextWorkUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Location = new System.Drawing.Point(98, 132);
            this.uTextWorkUserName.Name = "uTextWorkUserName";
            this.uTextWorkUserName.ReadOnly = true;
            this.uTextWorkUserName.Size = new System.Drawing.Size(129, 21);
            this.uTextWorkUserName.TabIndex = 105;
            // 
            // uLabelWokrUser
            // 
            this.uLabelWokrUser.Location = new System.Drawing.Point(9, 132);
            this.uLabelWokrUser.Name = "uLabelWokrUser";
            this.uLabelWokrUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelWokrUser.TabIndex = 103;
            this.uLabelWokrUser.Text = "ultraLabel1";
            // 
            // uLabelCustomerProductCode
            // 
            this.uLabelCustomerProductCode.Location = new System.Drawing.Point(233, 60);
            this.uLabelCustomerProductCode.Name = "uLabelCustomerProductCode";
            this.uLabelCustomerProductCode.Size = new System.Drawing.Size(120, 20);
            this.uLabelCustomerProductCode.TabIndex = 67;
            this.uLabelCustomerProductCode.Text = "ultraLabel2";
            // 
            // uTextCustomerProductCode
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductCode.Appearance = appearance55;
            this.uTextCustomerProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductCode.Location = new System.Drawing.Point(357, 60);
            this.uTextCustomerProductCode.Name = "uTextCustomerProductCode";
            this.uTextCustomerProductCode.ReadOnly = true;
            this.uTextCustomerProductCode.Size = new System.Drawing.Size(197, 21);
            this.uTextCustomerProductCode.TabIndex = 66;
            // 
            // uTextNowProcessCode
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessCode.Appearance = appearance6;
            this.uTextNowProcessCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessCode.Location = new System.Drawing.Point(651, 12);
            this.uTextNowProcessCode.Name = "uTextNowProcessCode";
            this.uTextNowProcessCode.ReadOnly = true;
            this.uTextNowProcessCode.Size = new System.Drawing.Size(86, 21);
            this.uTextNowProcessCode.TabIndex = 62;
            // 
            // uTextNowProcessName
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessName.Appearance = appearance37;
            this.uTextNowProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessName.Location = new System.Drawing.Point(741, 12);
            this.uTextNowProcessName.Name = "uTextNowProcessName";
            this.uTextNowProcessName.ReadOnly = true;
            this.uTextNowProcessName.Size = new System.Drawing.Size(111, 21);
            this.uTextNowProcessName.TabIndex = 61;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(233, 84);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(120, 20);
            this.uLabelPackage.TabIndex = 65;
            this.uLabelPackage.Text = "ultraLabel2";
            // 
            // uTextLotProcessState
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Appearance = appearance47;
            this.uTextLotProcessState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Location = new System.Drawing.Point(98, 108);
            this.uTextLotProcessState.Name = "uTextLotProcessState";
            this.uTextLotProcessState.ReadOnly = true;
            this.uTextLotProcessState.Size = new System.Drawing.Size(129, 21);
            this.uTextLotProcessState.TabIndex = 64;
            // 
            // uLabelLotProcessState
            // 
            this.uLabelLotProcessState.Location = new System.Drawing.Point(9, 108);
            this.uLabelLotProcessState.Name = "uLabelLotProcessState";
            this.uLabelLotProcessState.Size = new System.Drawing.Size(86, 20);
            this.uLabelLotProcessState.TabIndex = 63;
            this.uLabelLotProcessState.Text = "ultraLabel2";
            // 
            // uTextEquipCode
            // 
            appearance97.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance97;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(850, 96);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(18, 21);
            this.uTextEquipCode.TabIndex = 60;
            this.uTextEquipCode.Visible = false;
            // 
            // uTextWorkProcessCode
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessCode.Appearance = appearance46;
            this.uTextWorkProcessCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessCode.Location = new System.Drawing.Point(850, 24);
            this.uTextWorkProcessCode.Name = "uTextWorkProcessCode";
            this.uTextWorkProcessCode.ReadOnly = true;
            this.uTextWorkProcessCode.Size = new System.Drawing.Size(15, 21);
            this.uTextWorkProcessCode.TabIndex = 59;
            this.uTextWorkProcessCode.Visible = false;
            // 
            // uTextPackage
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance43;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(357, 84);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(197, 21);
            this.uTextPackage.TabIndex = 58;
            // 
            // uTextMESSPCNTFlag
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESSPCNTFlag.Appearance = appearance35;
            this.uTextMESSPCNTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESSPCNTFlag.Location = new System.Drawing.Point(871, 72);
            this.uTextMESSPCNTFlag.Name = "uTextMESSPCNTFlag";
            this.uTextMESSPCNTFlag.ReadOnly = true;
            this.uTextMESSPCNTFlag.Size = new System.Drawing.Size(17, 21);
            this.uTextMESSPCNTFlag.TabIndex = 57;
            this.uTextMESSPCNTFlag.Visible = false;
            // 
            // uTextMESHoldTFlag
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESHoldTFlag.Appearance = appearance56;
            this.uTextMESHoldTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESHoldTFlag.Location = new System.Drawing.Point(850, 72);
            this.uTextMESHoldTFlag.Name = "uTextMESHoldTFlag";
            this.uTextMESHoldTFlag.ReadOnly = true;
            this.uTextMESHoldTFlag.Size = new System.Drawing.Size(17, 21);
            this.uTextMESHoldTFlag.TabIndex = 56;
            this.uTextMESHoldTFlag.Visible = false;
            // 
            // uTextMESTrackOutTFlag
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackOutTFlag.Appearance = appearance54;
            this.uTextMESTrackOutTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackOutTFlag.Location = new System.Drawing.Point(871, 48);
            this.uTextMESTrackOutTFlag.Name = "uTextMESTrackOutTFlag";
            this.uTextMESTrackOutTFlag.ReadOnly = true;
            this.uTextMESTrackOutTFlag.Size = new System.Drawing.Size(17, 21);
            this.uTextMESTrackOutTFlag.TabIndex = 55;
            this.uTextMESTrackOutTFlag.Visible = false;
            // 
            // uTextMESTrackInTFlag
            // 
            appearance98.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTFlag.Appearance = appearance98;
            this.uTextMESTrackInTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTFlag.Location = new System.Drawing.Point(850, 48);
            this.uTextMESTrackInTFlag.Name = "uTextMESTrackInTFlag";
            this.uTextMESTrackInTFlag.ReadOnly = true;
            this.uTextMESTrackInTFlag.Size = new System.Drawing.Size(17, 21);
            this.uTextMESTrackInTFlag.TabIndex = 54;
            this.uTextMESTrackInTFlag.Visible = false;
            // 
            // uTextReqSeq
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Appearance = appearance1;
            this.uTextReqSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Location = new System.Drawing.Point(876, 120);
            this.uTextReqSeq.Name = "uTextReqSeq";
            this.uTextReqSeq.ReadOnly = true;
            this.uTextReqSeq.Size = new System.Drawing.Size(14, 21);
            this.uTextReqSeq.TabIndex = 53;
            this.uTextReqSeq.Visible = false;
            // 
            // uTextReqNo
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Appearance = appearance2;
            this.uTextReqNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Location = new System.Drawing.Point(862, 120);
            this.uTextReqNo.Name = "uTextReqNo";
            this.uTextReqNo.ReadOnly = true;
            this.uTextReqNo.Size = new System.Drawing.Size(15, 21);
            this.uTextReqNo.TabIndex = 52;
            this.uTextReqNo.Visible = false;
            // 
            // uTextCustomerCode
            // 
            appearance51.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance51;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(357, 12);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(197, 21);
            this.uTextCustomerCode.TabIndex = 50;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(233, 12);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(120, 20);
            this.uLabelCustomer.TabIndex = 49;
            this.uLabelCustomer.Text = "ultraLabel2";
            // 
            // uTextWorkProcessName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessName.Appearance = appearance40;
            this.uTextWorkProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessName.Location = new System.Drawing.Point(861, 24);
            this.uTextWorkProcessName.Name = "uTextWorkProcessName";
            this.uTextWorkProcessName.ReadOnly = true;
            this.uTextWorkProcessName.Size = new System.Drawing.Size(15, 21);
            this.uTextWorkProcessName.TabIndex = 48;
            this.uTextWorkProcessName.Visible = false;
            // 
            // uLabelWorkProcess
            // 
            this.uLabelWorkProcess.Location = new System.Drawing.Point(562, 36);
            this.uLabelWorkProcess.Name = "uLabelWorkProcess";
            this.uLabelWorkProcess.Size = new System.Drawing.Size(86, 20);
            this.uLabelWorkProcess.TabIndex = 47;
            this.uLabelWorkProcess.Text = "ultraLabel2";
            // 
            // uComboProcInspectType
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcInspectType.Appearance = appearance4;
            this.uComboProcInspectType.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboProcInspectType.Location = new System.Drawing.Point(98, 36);
            this.uComboProcInspectType.Name = "uComboProcInspectType";
            this.uComboProcInspectType.Size = new System.Drawing.Size(129, 21);
            this.uComboProcInspectType.TabIndex = 46;
            // 
            // uLabelProcInspectType
            // 
            this.uLabelProcInspectType.Location = new System.Drawing.Point(9, 36);
            this.uLabelProcInspectType.Name = "uLabelProcInspectType";
            this.uLabelProcInspectType.Size = new System.Drawing.Size(86, 20);
            this.uLabelProcInspectType.TabIndex = 45;
            this.uLabelProcInspectType.Text = "공정검사구분";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(357, 132);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(495, 21);
            this.uTextEtcDesc.TabIndex = 44;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(233, 132);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(120, 20);
            this.uLabelEtcDesc.TabIndex = 43;
            this.uLabelEtcDesc.Text = "ultraLabel2";
            // 
            // uTextLotNo
            // 
            appearance34.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.Appearance = appearance34;
            this.uTextLotNo.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLotNo.Location = new System.Drawing.Point(98, 60);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(129, 21);
            this.uTextLotNo.TabIndex = 41;
            this.uTextLotNo.AfterEnterEditMode += new System.EventHandler(this.uTextLotNo_AfterEnterEditMode);
            this.uTextLotNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLotNo_KeyDown);
            // 
            // uLabelEquip
            // 
            this.uLabelEquip.Location = new System.Drawing.Point(562, 60);
            this.uLabelEquip.Name = "uLabelEquip";
            this.uLabelEquip.Size = new System.Drawing.Size(86, 20);
            this.uLabelEquip.TabIndex = 40;
            this.uLabelEquip.Text = "ultraLabel1";
            // 
            // uTextLotSize
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotSize.Appearance = appearance41;
            this.uTextLotSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotSize.Location = new System.Drawing.Point(98, 84);
            this.uTextLotSize.Name = "uTextLotSize";
            this.uTextLotSize.ReadOnly = true;
            this.uTextLotSize.Size = new System.Drawing.Size(129, 21);
            this.uTextLotSize.TabIndex = 39;
            // 
            // uLabelLotSize
            // 
            this.uLabelLotSize.Location = new System.Drawing.Point(9, 84);
            this.uLabelLotSize.Name = "uLabelLotSize";
            this.uLabelLotSize.Size = new System.Drawing.Size(86, 20);
            this.uLabelLotSize.TabIndex = 38;
            this.uLabelLotSize.Text = "ultraLabel2";
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(9, 60);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelLotNo.TabIndex = 37;
            this.uLabelLotNo.Text = "ultraLabel2";
            // 
            // uTextProductCode
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance36;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(357, 36);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(197, 21);
            this.uTextProductCode.TabIndex = 35;
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(233, 36);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(120, 20);
            this.uLabelProduct.TabIndex = 34;
            this.uLabelProduct.Text = "ultraLabel2";
            // 
            // uComboPlantCode
            // 
            this.uComboPlantCode.Location = new System.Drawing.Point(98, 12);
            this.uComboPlantCode.Name = "uComboPlantCode";
            this.uComboPlantCode.Size = new System.Drawing.Size(129, 21);
            this.uComboPlantCode.TabIndex = 33;
            this.uComboPlantCode.Text = "ultraComboEditor1";
            this.uComboPlantCode.ValueChanged += new System.EventHandler(this.uComboPlantCode_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(9, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(86, 20);
            this.uLabelPlant.TabIndex = 32;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uTextFile);
            this.ultraGroupBox2.Controls.Add(this.uLabelAttachFile1);
            this.ultraGroupBox2.Controls.Add(this.uTextPCB_Layout);
            this.ultraGroupBox2.Controls.Add(this.uComboWafer);
            this.ultraGroupBox2.Controls.Add(this.uLabelWafer);
            this.ultraGroupBox2.Controls.Add(this.uNumLossQty);
            this.ultraGroupBox2.Controls.Add(this.uLabelLossQty);
            this.ultraGroupBox2.Controls.Add(this.uLabelLossCheck);
            this.ultraGroupBox2.Controls.Add(this.uCheckLossCheckFlag);
            this.ultraGroupBox2.Controls.Add(this.uLabelCompleteFlag);
            this.ultraGroupBox2.Controls.Add(this.uComboStack);
            this.ultraGroupBox2.Controls.Add(this.uLabelStack);
            this.ultraGroupBox2.Controls.Add(this.uDateInspectTime);
            this.ultraGroupBox2.Controls.Add(this.uButtonTrackIn);
            this.ultraGroupBox2.Controls.Add(this.uTextInspectUserName);
            this.ultraGroupBox2.Controls.Add(this.uTextInspectUserID);
            this.ultraGroupBox2.Controls.Add(this.uLabelInspectUser);
            this.ultraGroupBox2.Controls.Add(this.uCheckCompleteFlag);
            this.ultraGroupBox2.Controls.Add(this.uOptionPassFailFlag);
            this.ultraGroupBox2.Controls.Add(this.uLabelPassFailFlag);
            this.ultraGroupBox2.Controls.Add(this.uDateInspectDate);
            this.ultraGroupBox2.Controls.Add(this.uLabelInspectDate);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 200);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(917, 70);
            this.ultraGroupBox2.TabIndex = 3;
            // 
            // uTextPCB_Layout
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPCB_Layout.Appearance = appearance3;
            this.uTextPCB_Layout.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPCB_Layout.Location = new System.Drawing.Point(189, 36);
            this.uTextPCB_Layout.Name = "uTextPCB_Layout";
            this.uTextPCB_Layout.ReadOnly = true;
            this.uTextPCB_Layout.Size = new System.Drawing.Size(18, 21);
            this.uTextPCB_Layout.TabIndex = 115;
            this.uTextPCB_Layout.Visible = false;
            // 
            // uComboWafer
            // 
            this.uComboWafer.Location = new System.Drawing.Point(99, 36);
            this.uComboWafer.Name = "uComboWafer";
            this.uComboWafer.Size = new System.Drawing.Size(86, 21);
            this.uComboWafer.TabIndex = 114;
            this.uComboWafer.Text = "전체";
            // 
            // uLabelWafer
            // 
            this.uLabelWafer.Location = new System.Drawing.Point(10, 36);
            this.uLabelWafer.Name = "uLabelWafer";
            this.uLabelWafer.Size = new System.Drawing.Size(86, 20);
            this.uLabelWafer.TabIndex = 113;
            this.uLabelWafer.Text = "ultraLabel1";
            // 
            // uNumLossQty
            // 
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Text = "0";
            this.uNumLossQty.ButtonsLeft.Add(editorButton4);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumLossQty.ButtonsRight.Add(spinEditorButton1);
            this.uNumLossQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumLossQty.Location = new System.Drawing.Point(552, 36);
            this.uNumLossQty.MaskInput = "nnnnnnnnnn";
            this.uNumLossQty.Name = "uNumLossQty";
            this.uNumLossQty.Size = new System.Drawing.Size(93, 21);
            this.uNumLossQty.TabIndex = 112;
            this.uNumLossQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumLossQty_EditorSpinButtonClick);
            this.uNumLossQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumLossQty_EditorButtonClick);
            // 
            // uLabelLossQty
            // 
            this.uLabelLossQty.Location = new System.Drawing.Point(463, 36);
            this.uLabelLossQty.Name = "uLabelLossQty";
            this.uLabelLossQty.Size = new System.Drawing.Size(86, 20);
            this.uLabelLossQty.TabIndex = 110;
            this.uLabelLossQty.Text = "ultraLabel2";
            this.uLabelLossQty.Visible = false;
            // 
            // uLabelLossCheck
            // 
            this.uLabelLossCheck.Location = new System.Drawing.Point(689, 36);
            this.uLabelLossCheck.Name = "uLabelLossCheck";
            this.uLabelLossCheck.Size = new System.Drawing.Size(86, 20);
            this.uLabelLossCheck.TabIndex = 109;
            this.uLabelLossCheck.Text = "ultraLabel1";
            this.uLabelLossCheck.Visible = false;
            // 
            // uCheckLossCheckFlag
            // 
            this.uCheckLossCheckFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckLossCheckFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckLossCheckFlag.Location = new System.Drawing.Point(778, 36);
            this.uCheckLossCheckFlag.Name = "uCheckLossCheckFlag";
            this.uCheckLossCheckFlag.Size = new System.Drawing.Size(17, 20);
            this.uCheckLossCheckFlag.TabIndex = 108;
            this.uCheckLossCheckFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelCompleteFlag
            // 
            this.uLabelCompleteFlag.Location = new System.Drawing.Point(689, 12);
            this.uLabelCompleteFlag.Name = "uLabelCompleteFlag";
            this.uLabelCompleteFlag.Size = new System.Drawing.Size(86, 20);
            this.uLabelCompleteFlag.TabIndex = 107;
            this.uLabelCompleteFlag.Text = "ultraLabel1";
            // 
            // uComboStack
            // 
            this.uComboStack.Location = new System.Drawing.Point(370, 36);
            this.uComboStack.Name = "uComboStack";
            this.uComboStack.Size = new System.Drawing.Size(86, 21);
            this.uComboStack.TabIndex = 106;
            this.uComboStack.Text = "전체";
            this.uComboStack.ValueChanged += new System.EventHandler(this.uComboStack_ValueChanged);
            this.uComboStack.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboStack_BeforeDropDown);
            // 
            // uLabelStack
            // 
            this.uLabelStack.Location = new System.Drawing.Point(281, 36);
            this.uLabelStack.Name = "uLabelStack";
            this.uLabelStack.Size = new System.Drawing.Size(86, 20);
            this.uLabelStack.TabIndex = 105;
            this.uLabelStack.Text = "ultraLabel1";
            // 
            // uDateInspectTime
            // 
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateInspectTime.ButtonsRight.Add(spinEditorButton2);
            this.uDateInspectTime.DateTime = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.uDateInspectTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateInspectTime.Location = new System.Drawing.Point(802, 8);
            this.uDateInspectTime.MaskInput = "{LOC}hh:mm:ss";
            this.uDateInspectTime.Name = "uDateInspectTime";
            this.uDateInspectTime.Size = new System.Drawing.Size(86, 21);
            this.uDateInspectTime.TabIndex = 104;
            this.uDateInspectTime.Value = null;
            this.uDateInspectTime.Visible = false;
            // 
            // uButtonTrackIn
            // 
            this.uButtonTrackIn.Location = new System.Drawing.Point(802, 36);
            this.uButtonTrackIn.Name = "uButtonTrackIn";
            this.uButtonTrackIn.Size = new System.Drawing.Size(64, 23);
            this.uButtonTrackIn.TabIndex = 103;
            this.uButtonTrackIn.Text = "TrackIn";
            this.uButtonTrackIn.Click += new System.EventHandler(this.uButtonTrackIn_Click);
            this.uButtonTrackIn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uButtonTrackIn_KeyDown);
            // 
            // uTextInspectUserName
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance44;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(187, 12);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextInspectUserName.TabIndex = 102;
            // 
            // uTextInspectUserID
            // 
            appearance94.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectUserID.Appearance = appearance94;
            this.uTextInspectUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance45.Image = ((object)(resources.GetObject("appearance45.Image")));
            appearance45.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance45;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton5);
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(99, 12);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextInspectUserID.TabIndex = 101;
            this.uTextInspectUserID.ValueChanged += new System.EventHandler(this.uTextInspectUserID_ValueChanged);
            this.uTextInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextInspectUserID_KeyDown);
            this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectUserID_EditorButtonClick);
            // 
            // uLabelInspectUser
            // 
            this.uLabelInspectUser.Location = new System.Drawing.Point(10, 12);
            this.uLabelInspectUser.Name = "uLabelInspectUser";
            this.uLabelInspectUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelInspectUser.TabIndex = 100;
            this.uLabelInspectUser.Text = "ultraLabel1";
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckCompleteFlag.Checked = true;
            this.uCheckCompleteFlag.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(778, 12);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(17, 20);
            this.uCheckCompleteFlag.TabIndex = 99;
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedValueChanged);
            // 
            // uOptionPassFailFlag
            // 
            appearance48.BackColorDisabled = System.Drawing.Color.White;
            appearance48.ForeColorDisabled = System.Drawing.Color.Black;
            this.uOptionPassFailFlag.Appearance = appearance48;
            this.uOptionPassFailFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionPassFailFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem6.DataValue = "OK";
            valueListItem6.DisplayText = "합격";
            valueListItem7.DataValue = "NG";
            valueListItem7.DisplayText = "불합격";
            this.uOptionPassFailFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem6,
            valueListItem7});
            this.uOptionPassFailFlag.Location = new System.Drawing.Point(552, 12);
            this.uOptionPassFailFlag.Name = "uOptionPassFailFlag";
            this.uOptionPassFailFlag.Size = new System.Drawing.Size(129, 20);
            this.uOptionPassFailFlag.TabIndex = 98;
            this.uOptionPassFailFlag.TextIndentation = 2;
            this.uOptionPassFailFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelPassFailFlag
            // 
            this.uLabelPassFailFlag.Location = new System.Drawing.Point(463, 12);
            this.uLabelPassFailFlag.Name = "uLabelPassFailFlag";
            this.uLabelPassFailFlag.Size = new System.Drawing.Size(86, 20);
            this.uLabelPassFailFlag.TabIndex = 97;
            this.uLabelPassFailFlag.Text = "ultraLabel1";
            // 
            // uDateInspectDate
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uDateInspectDate.Appearance = appearance42;
            this.uDateInspectDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uDateInspectDate.DateTime = new System.DateTime(2015, 5, 28, 0, 0, 0, 0);
            this.uDateInspectDate.Location = new System.Drawing.Point(370, 12);
            this.uDateInspectDate.Name = "uDateInspectDate";
            this.uDateInspectDate.ReadOnly = true;
            this.uDateInspectDate.Size = new System.Drawing.Size(86, 21);
            this.uDateInspectDate.TabIndex = 96;
            this.uDateInspectDate.Value = new System.DateTime(2015, 5, 28, 0, 0, 0, 0);
            // 
            // uLabelInspectDate
            // 
            this.uLabelInspectDate.Location = new System.Drawing.Point(281, 12);
            this.uLabelInspectDate.Name = "uLabelInspectDate";
            this.uLabelInspectDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelInspectDate.TabIndex = 95;
            this.uLabelInspectDate.Text = "ultraLabel2";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.uTab);
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 270);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(917, 580);
            this.ultraGroupBox3.TabIndex = 108;
            // 
            // uTab
            // 
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Controls.Add(this.ultraTabPageControl3);
            this.uTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uTab.Location = new System.Drawing.Point(3, 0);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(911, 577);
            this.uTab.TabIndex = 5;
            ultraTab1.Key = "0";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "검사결과";
            ultraTab2.Key = "1";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "샘플림검사";
            ultraTab3.Key = "2";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "불량유형";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(907, 551);
            // 
            // uTextFile
            // 
            appearance86.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance86.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance86;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Key = "Up";
            appearance106.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance106.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance106;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Key = "Down";
            editorButton3.Visible = false;
            this.uTextFile.ButtonsRight.Add(editorButton2);
            this.uTextFile.ButtonsRight.Add(editorButton3);
            this.uTextFile.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextFile.Location = new System.Drawing.Point(551, 36);
            this.uTextFile.Name = "uTextFile";
            this.uTextFile.ReadOnly = true;
            this.uTextFile.Size = new System.Drawing.Size(196, 21);
            this.uTextFile.TabIndex = 140;
            this.uTextFile.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFile_EditorButtonClick);
            // 
            // uLabelAttachFile1
            // 
            this.uLabelAttachFile1.Location = new System.Drawing.Point(463, 36);
            this.uLabelAttachFile1.Name = "uLabelAttachFile1";
            this.uLabelAttachFile1.Size = new System.Drawing.Size(86, 20);
            this.uLabelAttachFile1.TabIndex = 139;
            this.uLabelAttachFile1.Text = "上传文件";
            // 
            // frmINS0008C_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINS0008C_S";
            this.Load += new System.EventHandler(this.frmINS0008C_Load);
            this.Activated += new System.EventHandler(this.frmINS0008C_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINS0008C_FormClosing);
            this.Resize += new System.EventHandler(this.frmINS0008C_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridItem)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSampling)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFaultData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            this.ultraGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQCNorITRCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTestProcessFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHandler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSPCNCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOUTSOURCINGVENDOR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWorkProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessHoldState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESSPCNTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESHoldTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackOutTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPCB_Layout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboWafer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumLossQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckLossCheckFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextFile)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESSPCNTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESHoldTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTrackOutTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTrackInTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkProcessName;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelProcInspectType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotSize;
        private Infragistics.Win.Misc.UltraLabel uLabelLotSize;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectTime;
        private Infragistics.Win.Misc.UltraButton uButtonTrackIn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectUser;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionPassFailFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelPassFailFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStack;
        private Infragistics.Win.Misc.UltraLabel uLabelStack;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextNowProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextNowProcessName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotProcessState;
        private Infragistics.Win.Misc.UltraLabel uLabelLotProcessState;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProductCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelWokrUser;
        private Infragistics.Win.Misc.UltraLabel uLabelNowProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessHoldState;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessHoldState;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelLossCheck;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckLossCheckFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelLossQty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumLossQty;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquip;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboWorkProcess;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridItem;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSampling;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOUTSOURCINGVENDOR;
        private Infragistics.Win.Misc.UltraLabel uLabelOUTSOURCINGVENDOR;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboWafer;
        private Infragistics.Win.Misc.UltraLabel uLabelWafer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPCB_Layout;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCompleteCheck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTrackInTCheck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSPCNCheck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgram;
        private Infragistics.Win.Misc.UltraLabel uLabelProgram;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHandler;
        private Infragistics.Win.Misc.UltraLabel uLabelHandler;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTestProcessFlag;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFaultData;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextQCNorITRCheck;
        private Infragistics.Win.Misc.UltraButton uButtonDelete2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFault;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTrackInTDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFile;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile1;
	}
}