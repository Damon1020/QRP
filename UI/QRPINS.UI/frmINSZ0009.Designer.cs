﻿namespace QRPINS.UI
{
    partial class frmINSZ0009
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0009));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchStartToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchStartFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchStartDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridMonitoring = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchStartToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchStartFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMonitoring)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchStartToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel8);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchStartFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStartDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uDateSearchStartToDate
            // 
            this.uDateSearchStartToDate.Location = new System.Drawing.Point(776, 12);
            this.uDateSearchStartToDate.Name = "uDateSearchStartToDate";
            this.uDateSearchStartToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchStartToDate.TabIndex = 20;
            // 
            // ultraLabel8
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel8.Appearance = appearance4;
            this.ultraLabel8.Location = new System.Drawing.Point(766, 12);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(8, 20);
            this.ultraLabel8.TabIndex = 19;
            this.ultraLabel8.Text = "~";
            // 
            // uDateSearchStartFromDate
            // 
            this.uDateSearchStartFromDate.Location = new System.Drawing.Point(664, 12);
            this.uDateSearchStartFromDate.Name = "uDateSearchStartFromDate";
            this.uDateSearchStartFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchStartFromDate.TabIndex = 18;
            // 
            // uLabelSearchStartDate
            // 
            this.uLabelSearchStartDate.Location = new System.Drawing.Point(548, 12);
            this.uLabelSearchStartDate.Name = "uLabelSearchStartDate";
            this.uLabelSearchStartDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchStartDate.TabIndex = 17;
            this.uLabelSearchStartDate.Text = "모니터링 시작일";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(384, 12);
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchArea.TabIndex = 3;
            this.uComboSearchArea.Text = "ultraComboEditor1";
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(280, 12);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchArea.TabIndex = 2;
            this.uLabelSearchArea.Text = "위치";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uGridMonitoring
            // 
            this.uGridMonitoring.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMonitoring.DisplayLayout.Appearance = appearance2;
            this.uGridMonitoring.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMonitoring.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMonitoring.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMonitoring.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridMonitoring.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMonitoring.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridMonitoring.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMonitoring.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMonitoring.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMonitoring.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridMonitoring.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMonitoring.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMonitoring.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMonitoring.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridMonitoring.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMonitoring.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMonitoring.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridMonitoring.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridMonitoring.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMonitoring.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridMonitoring.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridMonitoring.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMonitoring.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridMonitoring.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMonitoring.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMonitoring.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMonitoring.Location = new System.Drawing.Point(0, 80);
            this.uGridMonitoring.Name = "uGridMonitoring";
            this.uGridMonitoring.Size = new System.Drawing.Size(1070, 764);
            this.uGridMonitoring.TabIndex = 2;
            // 
            // frmINSZ0009
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridMonitoring);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0009";
            this.Load += new System.EventHandler(this.frmINSZ0009_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0009_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchStartToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchStartFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMonitoring)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMonitoring;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchStartToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchStartFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStartDate;
    }
}