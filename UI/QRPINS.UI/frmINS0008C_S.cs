﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINS0008C.cs                                         */
/* 프로그램명   : 공정검사등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-10-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

// 파일첨부를 위한 Using 추가
using System.IO;
using System.Collections;

using System.Net;

namespace QRPINS.UI
{
    public partial class frmINS0008C_S : Form, IToolbar
    {
        #region Global Variable

        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        private string m_strPlantCode;
        private string m_strReqNo;
        private string m_strReqSeq;
        private string m_strMoveFormName;

        private string strfileName;

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string ReqNo
        {
            get { return m_strReqNo; }
            set { m_strReqNo = value; }
        }

        public string ReqSeq
        {
            get { return m_strReqSeq; }
            set { m_strReqSeq = value; }
        }

        private string m_strProcessCode;
        private string m_strEquipCode;

        public string ProcessCode
        {
            get { return m_strProcessCode; }
            set { m_strProcessCode = value; }
        }

        public string EquipCode
        {
            get { return m_strEquipCode; }
            set { m_strEquipCode = value; }
        }

        #endregion

        #region Form Events

        public frmINS0008C_S()
        {
            InitializeComponent();
        }

        private void frmINS0008C_Activated(object sender, EventArgs e)
        {
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, false, true, false, true, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINS0008C_Load(object sender, EventArgs e)
        {
            // 컨트롤 초기화
            SetToolAuth();
            InitTab();
            InitButton();
            InitComboBox();
            InitGrid();
            InitLabel();
            InitEtc();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmINS0008C_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        // 폼리사이즈
        private void frmINS0008C_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width < 1070 && this.Height < 850)
                {
                    this.ultraGroupBox3.Dock = DockStyle.None;                    
                }
                else if (this.Width < 1070 && this.Height > 850)
                {
                    this.ultraGroupBox3.Dock = DockStyle.None;
                    this.ultraGroupBox3.Height = this.Height - this.titleArea.Height - this.ultraGroupBox1.Height - this.ultraGroupBox2.Height
                                        - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                    this.ultraGroupBox3.Width = 1060;
                }
                else if (this.Width > 1070 && this.Height < 850)
                {
                    this.ultraGroupBox3.Dock = DockStyle.None;
                    this.ultraGroupBox3.Width = this.Width - this.uTab.Left - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    this.ultraGroupBox3.Height = 580;
                }
                else
                {
                    this.uGroupBox3.Dock = DockStyle.Fill;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤 초기화 Method

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 최대길이 설정
                this.uTextLotNo.MaxLength = 50;
                this.uTextEtcDesc.MaxLength = 100;

                // 기본값 설정
                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uDateInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateInspectTime.Value = DateTime.Now.ToString("HH:mm:ss");

                this.uCheckLossCheckFlag.Visible = false;
                this.uNumLossQty.Visible = false;
                this.uLabelLossQty.Visible = false;
                this.uLabelLossCheck.Visible = false;

                // 문자입력 대문자로만 입력되게 하기
                this.uTextLotNo.ImeMode = ImeMode.Disable;
                this.uTextLotNo.CharacterCasing = CharacterCasing.Upper;

                this.uTextInspectUserID.ImeMode = ImeMode.Disable;
                this.uTextInspectUserID.CharacterCasing = CharacterCasing.Upper;

                this.uTextLotSize.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                this.uLabelWafer.Hide();
                this.uComboWafer.Hide();

                this.uLabelHandler.Hide();
                this.uLabelProgram.Hide();
                this.uTextProgram.MaxLength = 100;
                this.uTextProgram.Hide();
                this.uComboHandler.Hide();
                this.uTextTestProcessFlag.Text = "F";
                this.uTextTestProcessFlag.Visible = false;

                this.uTextMESTrackInTFlag.Text = "F";

                this.uTextLotNo.Focus();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                this.uTab.Tabs[0].VisibleIndex = 2;
                this.uTab.Tabs[1].VisibleIndex = 0;
                this.uTab.Tabs[2].VisibleIndex = 1;

                this.uTab.SelectedTab = this.uTab.Tabs[1];
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWorkProcess, "작업공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProcInspectType, "공정검사구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLotSize, "Lot수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquip, "설비번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotProcessState, "Lot상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerProductCode, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWokrUser, "작업자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelNowProcess, "현재공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessHoldState, "Hold상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOUTSOURCINGVENDOR, "도금업체", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStack, "Stack", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteFlag, "작성완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLossCheck, "Loss여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLossQty, "Loss수량", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser, "검사자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPassFailFlag, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelWafer, "B/G여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgram, "PROGRAM", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelHandler, "HANDLER", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile1, "上传文件", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonDelete2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonTrackIn.Enabled = false;

                // Set Events...
                this.uButtonDelete2.Click += new EventHandler(uButtonDelete2_Click);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlantCode, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // 공정검사구분 코드 (공통코드에서 가져오기)
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode"); // 2
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode(); // 3
                brwChannel.mfCredentials(clsComCode); // 4

                DataTable dtProcInspectGubun = clsComCode.mfReadCommonCode("C0020", m_resSys.GetString("SYS_LANG")); // 5

                wCombo.mfSetComboEditor(this.uComboProcInspectType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "1", "", "선택"
                    , "ComCode", "ComCodeName", dtProcInspectGubun);

                DataTable dtStack = new DataTable();
                wCombo.mfSetComboEditor(this.uComboStack, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "StackSeq", "StackSeqName", dtStack);

                // 설비콤보박스 초기화
                DataTable dtEquip = new DataTable();
                wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "EquipCode", "EquipName", dtEquip);

                // BG여부 확인
                DataTable dtWafer = clsComCode.mfReadCommonCode("C0026", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboWafer, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dtWafer);

                // HANDLER 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtHANDLER = clsEquip.mfReadEquip_Combo(this.uComboPlantCode.Value.ToString()
                                                                , "", "", "", "", "TH", "", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboHandler, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "", "EquipCode", "EquipName", dtHANDLER);

                WinComboGrid wComboG = new WinComboGrid();
                wComboG.mfInitGeneralComboGrid(this.uComboWorkProcess, true, false, true, true, "ProcessCode", "ProcessCodeName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                                            , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                                            , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                                            , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wComboG.mfSetComboGridColumn(this.uComboWorkProcess, 0, "ProcessCode", "공정코드", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboWorkProcess, 0, "ProcessName", "공정명", false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                wComboG.mfSetComboGridColumn(this.uComboWorkProcess, 0, "ProcessCodeName", "공정명", false, 100, true, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                // 자동완성기능 설정
                this.uComboWorkProcess.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 검사결과 그리드
                // 검사결과 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridItem, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectIngFlag", "검사진행여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ReqItemSeq", "항목순번", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 60, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessCode", "공정코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessName", "공정", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectGroupName", "검사분류", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectTypeName", "검사유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "StackSeq", "StackSeq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Generation", "Generation", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "FaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SampleSize", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessSampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //     wGrid.mfSetGridColumn(this.uGrid1, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //         , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //         , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                ////////////////////////////////////////////////////////////////////////

                wGrid.mfSetGridColumn(this.uGridItem, 0, "MaxValue", "Max", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "StdDev", "StdDev.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "OCPCount", "OCPCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperRunCount", "UpperRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerRunCount", "LowerRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperTrendCount", "UpperTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerTrendCount", "LowerTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                //wGrid.mfSetGridColumn(this.uGridItem, 0, "CycleCount", "CycleCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");


                //////////////////////////////////////////////////////////////////////

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LCL", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "CL", "CL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UCL", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean1", "평균1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean2", "평균2", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean3", "평균3", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean4", "평균4", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean5", "평균5", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean6", "평균6", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");
                #endregion

                #region Sampling Grid
                //// 샘플링검사 그리드
                //InitSampleGrid();
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSampling, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectGropCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessSampleSize", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SampleSize", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                ///////////////하단 Hidden 컬럼

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LCL", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "CL", "CL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UCL", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean1", "평균1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean2", "평균2", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean3", "평균3", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean4", "평균4", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean5", "평균5", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean6", "평균6", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "MaxValue", "Max", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "StdDev", "StdDev.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "OCPCount", "OCPCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperRunCount", "UpperRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerRunCount", "LowerRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperTrendCount", "UpperTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerTrendCount", "LowerTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");


                // 그리드 헤더 체크박스 없애기
                this.uGridSampling.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                #endregion

                #region FaultGrid
                // 불량유형 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridFault, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                //wGrid.mfSetGridColumn(this.uGridFault, 0, "ReqLotSeq", "Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "1");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                //wGrid.mfSetGridColumn(this.uGridFault, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectTypeName", "유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 20
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid5, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "ProcessSampleSize", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "FaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region FatulData Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridFaultData, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ReqLotSeq", "Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "1");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ReqInspectSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "QCNFlag", "QCN대상여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "P_ITRFlag", "ITR대상여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "InspectQty", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:15.0}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.0}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ExpectProcessCode", "예상공정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "CauseEquipCode", "원인설비", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "WorkUserID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "WorkUserName", "작업자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 90, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "CheckFaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                this.uGridFaultData.DisplayLayout.Bands[0].Columns["P_ITRFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                #endregion

                #region Set Grid Font, DropDown, Etc...

                // Set FontSize
                this.uGridItem.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridItem.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridSampling.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSampling.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridFault.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridFault.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridFaultData.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridFaultData.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // DropDown 설정
                // 단위
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();

                wGrid.mfSetGridColumnValueList(this.uGridItem, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridFault, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                // 규격단위
                wGrid.mfSetGridColumnValueList(this.uGridItem, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridFault, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                // DataRange
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0032", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridItem, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridFault, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                // 검사결과
                dtCom = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridItem, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridFault, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);


                this.uGridItem.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridFault.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                this.uGridSampling.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGridSampling.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGridSampling.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                //Key Mapping Clear
                this.uGridSampling.KeyActionMappings.Clear();

                #endregion

                #region Set Events

                this.uGridFault.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridFault_DoubleClickRow);
                this.uGridFault.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGridFault_InitializeRow);

                this.uGridFaultData.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridFaultData_AfterCellUpdate);
                this.uGridFaultData.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(uGridFaultData_BeforeCellListDropDown);
                this.uGridFaultData.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridFaultData_ClickCellButton);
                this.uGridFaultData.KeyDown += new KeyEventHandler(uGridFaultData_KeyDown);

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 필수입력사항 확인
                if (this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlantCode.Focus();
                    this.uComboPlantCode.DropDown();
                    return;
                }
                else if (this.uComboProcInspectType.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000287", Infragistics.Win.HAlign.Right);

                    this.uComboProcInspectType.Focus();
                    this.uComboProcInspectType.DropDown();
                    return;
                }
                else if (this.uTextLotNo.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000078", Infragistics.Win.HAlign.Right);

                    this.uTextLotNo.Focus();
                    return;
                }
                else if (this.uTextInspectUserName.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M001364", Infragistics.Win.HAlign.Right);
                    this.uTextInspectUserID.Focus();
                    return;
                }
                //else if (this.uTextEquipCode.Text.Equals("-") || this.uTextEquipCode.Text.Equals(string.Empty))
                else if (this.uComboEquip.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000703", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (this.uComboEquip.SelectedIndex.Equals(-1))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000070", "M000702", Infragistics.Win.HAlign.Right);

                    this.uComboEquip.Focus();
                    this.uComboEquip.DropDown();
                    return;
                }
                else if (this.uComboEquip.Value.ToString().Equals("-"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000070", "M000702", Infragistics.Win.HAlign.Right);

                    this.uComboEquip.Focus();
                    this.uComboEquip.DropDown();
                    return;
                }
                else if (!(this.uGridSampling.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000198", "M000195", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (!(this.uGridSampling.DisplayLayout.Bands[0].Columns.Exists("1")))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000172", "M000171", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (!(this.uTextMESTrackInTFlag.Text.Equals("T")) && this.uButtonTrackIn.Enabled.Equals(true))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000154", "M000155", Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (this.uOptionPassFailFlag.CheckedIndex.Equals(-1))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000174", "M000175", Infragistics.Win.HAlign.Right);

                    this.uOptionPassFailFlag.Focus();
                    return;
                }
                else
                {
                    this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this)) return;

                    // 작업공정에 Matching 되어있는 설비인지 확인
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProccess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProccess);

                    DataTable dtEquipCheck = clsProccess.mfReadMASProcEquipGroup_Check(this.uComboPlantCode.Value.ToString()
                                                                                    , this.uComboWorkProcess.Value.ToString()
                                                                                    , this.uComboEquip.Value.ToString()
                                                                                    , m_resSys.GetString("SYS_LANG"));

                    //if (!(dtEquipCheck.Rows.Count > 0) && !this.uComboEquip.Value.ToString().Contains("VI"))
                    if (!(dtEquipCheck.Rows.Count > 0) && !this.uComboEquip.Value.ToString().Equals(this.uTextEquipCode.Text))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000723", "M001365", Infragistics.Win.HAlign.Right);

                        this.uComboEquip.Focus();
                        this.uComboEquip.DropDown();
                        return;
                    }

                    // SPCN발생여부 확인
                    bool bolCheckSPCN = false;

                    // 불량수량입력확인
                    bool bolCheckFaultCount = false;

                    // 검사유형이 특성인지 파악
                    bool bolCheckInspectType = false;


                    //2014-12-19 modified by shichao
                    if (this.uTextNowProcessCode.Text.Equals("T0500") && this.uGridSampling.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value).Equals(false))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                             , "M001264", "M000183", "M001569", Infragistics.Win.HAlign.Right);

                                return;
                            }
                        }
                    }
                    //-------------------------------------Begin--------------2015-07-15------------------------------------

                    //Wafer 수입검사(작업공정 = A1000 && Wafer검사여부 = "Y" && 합격)  2015-07-15
                    //如果未做以下这些项目其中一种的检查，QRP给MES的tib信息中就不包含dtInspectItemList---QUALEFLAG信息，MES中工程投入时会提示：该lot未通过QRP系统检查，请确认品质部门后在进行。
                    if (this.uTextPCB_Layout.Text.Equals("Y")
                        && this.uComboWorkProcess.Value != null
                        && this.uComboWorkProcess.Value.ToString().Equals("A1000")
                        && this.uOptionPassFailFlag.CheckedIndex == 0)
                    {

                        bool bolSECInspectItemFlag = false;
                        bool bolOtherInspectItemFlag = false;

                        for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                        {
                            // 검사 진행여부가 체크된 것만
                            if (Convert.ToBoolean(this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value))
                            {
                                // 고객사가 삼성인경우 그리드에 검사항목 II02031902, II02031903, II02010010, II02010009 가 있으면 같이 보낸다
                                if (this.uTextCustomerCode.Text.Equals("SEC"))
                                {
                                    if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010001"))
                                    {
                                        bolSECInspectItemFlag = true;
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02031902"))
                                    {
                                        bolSECInspectItemFlag = true;
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02031903"))
                                    {
                                        bolSECInspectItemFlag = true;
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010010"))
                                    {
                                        bolSECInspectItemFlag = true;
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010009"))
                                    {
                                        bolSECInspectItemFlag = true;
                                    }
                                }
                                else
                                {
                                    // 고객사가 삼성이 아닌경우 외관검사항목만 보낸다
                                    if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010001"))
                                    {
                                        bolOtherInspectItemFlag = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (this.uTextCustomerCode.Text.Equals("SEC") && bolSECInspectItemFlag == false) 
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                         , "M001264", "M000183", "M001579", Infragistics.Win.HAlign.Right);

                            return;
                        }

                        if ( !this.uTextCustomerCode.Text.Equals("SEC") && bolOtherInspectItemFlag == false)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                         , "M001264", "M000183", "M001580", Infragistics.Win.HAlign.Right);

                            return;
                        }

                    }
                    //------------------------------------End-----------------2015-07-15-----------------------------

                    if (this.uGridSampling.Rows.Count > 0)
                    {
                        // 검사진행여부 확인
                        bool bolCheckIngFlag = false;

                        for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value).Equals(true))
                            {
                                bolCheckIngFlag = true;
                                //break;
                            }

                            // 계량형인 경우에만 SPCN 여부 판단
                            if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString().Equals("1"))
                            {
                                if (Convert.ToInt32(this.uGridSampling.Rows[i].Cells["OCPCount"].Value) > 0 ||
                                    Convert.ToInt32(this.uGridSampling.Rows[i].Cells["UpperRunCount"].Value) > 0 ||
                                    Convert.ToInt32(this.uGridSampling.Rows[i].Cells["LowerRunCount"].Value) > 0 ||
                                    Convert.ToInt32(this.uGridSampling.Rows[i].Cells["UpperTrendCount"].Value) > 0 ||
                                    Convert.ToInt32(this.uGridSampling.Rows[i].Cells["LowerTrendCount"].Value) > 0)
                                {
                                    bolCheckSPCN = true;
                                    //break;
                                }
                            }

                            if (Convert.ToInt32(this.uGridSampling.Rows[i].Cells["FaultQty"].Value) > 0)
                            {
                                bolCheckFaultCount = true;
                            }

                            if (this.uGridSampling.Rows[i].Cells["InspectGroupCode"].Value.ToString().Equals("IG02") &&
                                this.uGridSampling.Rows[i].Cells["InspectTypeCode"].Value.ToString().Equals("IT0203") &&
                                Convert.ToBoolean(this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value))
                            {
                                bolCheckInspectType = true;
                            }
                        }

                        if (bolCheckIngFlag.Equals(false))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000183", "M001056", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }

                    // 테스트 공정이고, 검사항목중 치수검사진행여부가 체크되어있으면 HANDLER, PROGRAM 입력사항 확인
                    if (this.uTextTestProcessFlag.Text.Equals("T") && bolCheckInspectType)
                    {
                        if (this.uComboHandler.Value.ToString().Equals(string.Empty) ||
                            this.uComboHandler.SelectedIndex.Equals(-1))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001235", "M001482", Infragistics.Win.HAlign.Right);

                            this.uComboHandler.Focus();
                            this.uComboHandler.DropDown();
                            return;
                        }
                        else if (this.uTextProgram.Text.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001235", "M001483", Infragistics.Win.HAlign.Right);

                            this.uTextProgram.Focus();
                            return;
                        }
                    }
                    string strPlantCode = this.uComboPlantCode.Value.ToString();
                    string strWorkProcess = this.uComboWorkProcess.Value.ToString();
                    string strEquipCode = this.uComboEquip.Value == null ? string.Empty : this.uComboEquip.Value.ToString();

                    if (this.uGridFaultData.Rows.Count > 0)
                    {

                        //불량유형 콤보 정합성확인을 위한 불량유형정보 BL 호출 2012-10-17 
                        // 불량유형
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                        QRPMAS.BL.MASQUA.InspectFaultType clsFType = new QRPMAS.BL.MASQUA.InspectFaultType();
                        brwChannel.mfCredentials(clsFType);

                        // TrackIn 여부 상관없이 작업공정으로
                        DataTable dtFType = new DataTable();
                        if (this.uTextCustomerCode.Text.Equals("HYNIX") || this.uTextCustomerName.Text.Equals("HYNIX"))         // HYNIX 제품일경우 불량명 다르게 설정
                            dtFType = clsFType.mfReadInspectFaultTypeCombo_WithInspectFaultGubun(strPlantCode, "3", m_resSys.GetString("SYS_LANG"));
                        else
                            dtFType = clsFType.mfReadInspectFaultTypeCombo_ProcessGroup(strPlantCode, strWorkProcess, m_resSys.GetString("SYS_LANG"));

                        for (int i = 0; i < this.uGridFaultData.Rows.Count; i++)
                        {
                            // 불량수량이 입력이 안되어 있는경우
                            if (this.uGridFaultData.Rows[i].Cells["FaultQty"].Value == null ||
                                this.uGridFaultData.Rows[i].Cells["FaultQty"].Value.ToString().Equals(string.Empty) ||
                                Convert.ToInt32(this.uGridFaultData.Rows[i].Cells["FaultQty"].Value) == 0)
                            {
                                // 불량수량을 입력해주세요
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001264", "M000611", "M001407", Infragistics.Win.HAlign.Right);

                                this.uGridFaultData.Rows[i].Cells["FaultQty"].Activate();
                                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;


                            }

                            // 불량유형 체크 (선택된 불량유형정보가 제대로 입력이 되었는지 확인
                            if (this.uGridFaultData.Rows[i].Cells["FaultTypeCode"].Value == null ||
                                this.uGridFaultData.Rows[i].Cells["FaultTypeCode"].Value == DBNull.Value ||
                                this.uGridFaultData.Rows[i].Cells["FaultTypeCode"].Value.ToString().Equals(string.Empty) ||
                                dtFType.Select("InspectFaultTypeCode = '" + this.uGridFaultData.Rows[i].Cells["FaultTypeCode"].Value.ToString() + "'").Count() == 0) // 불량유형정보유형 정합성판단 아닌경우 2012-10-17
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000605", strLang)
                                    , (i + 1).ToString() + msg.GetMessge_Text("M000008", strLang)
                                    , Infragistics.Win.HAlign.Right);

                                this.uGridFaultData.Rows[i].Cells["FaultTypeCode"].Activate();
                                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                        }

                        clsFType.Dispose();
                        dtFType.Dispose();
                    }

                    // BG 여부 확인  2012-09-03
                    //if (this.uTextPCB_Layout.Text.Equals("Y"))
                    //{
                    //    if (this.uComboWafer.Value.ToString().Equals(string.Empty))
                    //    {
                    //        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                    //                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                , "M001264", "M001235", "M001478"
                    //                , Infragistics.Win.HAlign.Right);

                    //        this.uComboWafer.Focus();
                    //        this.uComboWafer.DropDown();
                    //        return;
                    //    }
                    //}

                    if (!Check_FaultData())
                        return;

                    ////////////////////////// 동일한 Lot+공정+설비 체크 2012-11-01 //////////////////////////////
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                    QRPINS.BL.INSPRC.ProcInspectReqLot clsProcLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                    brwChannel.mfCredentials(clsProcLot);

                    // 중복LotNo 검색 매서드 실행
                    DataTable dtLotCheck = clsProcLot.mfReadINSProcInspectReqLot_Check(strPlantCode, strWorkProcess, strEquipCode, this.uTextLotNo.Text, m_resSys.GetString("SYS_LANG"));

                    // 중복 LotNo 
                    if (dtLotCheck.Rows.Count > 0)
                    {
                        // 입력하신 정보와 동일한 정보가 존재 합니다. <br/> 등록 하시겠습니까?
                        Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000962", "M001530", Infragistics.Win.HAlign.Right);

                        if (Result == DialogResult.No)
                            return;

                    }

                    // 리소스해제
                    clsProcLot.Dispose();
                    dtLotCheck.Dispose();

                    //////////////////////////////////////////////////////////////////////////////////////////////////

                    // SPCN 발생시 저장여부 확인
                    if (bolCheckSPCN)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001360", "M001351", Infragistics.Win.HAlign.Right);

                        if (Result == DialogResult.No)
                            return;
                    }

                    if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                    {
                       if (this.uTextEtcDesc.Text.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                           , "M001264", "M000881", "M001559", Infragistics.Win.HAlign.Right);

                            this.uTextEtcDesc.Focus();
                            return;
                        }

                        if (bolCheckFaultCount)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000174", "M001363", Infragistics.Win.HAlign.Right);

                            if (Result == DialogResult.No)
                                return;
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000174", "M001356", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }

                    ////if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true)
                    ////{
                    ////    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                    "확인창", "작성완료 확인", "작성완료 체크후 저장시 이후 수정할 수 없습니다",
                    ////                    Infragistics.Win.HAlign.Right);
                    ////}
                    ////// 저장여부를 묻는다
                    ////if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                "확인창", "저장확인", "입력한 정보를 저장하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    ////{
                    // 저장정보 데이터 테이블 설정
                    // 검시일시 설정
                    if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                    {
                        this.uDateInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                        this.uDateInspectTime.Value = DateTime.Now.ToString("HH:mm:ss");
                    }

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    { 
                    DataTable dtHeader = Rtn_HeaderDataTable();
                    DataTable dtLot = Rtn_LotDataTable();
                    DataTable dtItem = Rtn_ItemDataTable();
                    DataSet dsResult = Rtn_ResultDataSet();
                    DataTable dtFault = Rtn_FaultDataTable();
                    DataTable dtCycle = Rtn_CycleDataTable();

                    // BL 연결
                    //QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                    QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                    brwChannel.mfCredentials(clsHeader);

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;
                    
                    //上传附件2015-05-28
                    //FileUpload_Header();

                    //FTP方式上传附件 2015-07-20
                    if (this.uTextFile.Text.Contains(":\\"))
                    {

                        FileInfo fileDoc = new FileInfo(this.uTextFile.Text);
                        string strUploadFile = fileDoc.DirectoryName + "\\" + strfileName;
                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);
                        File.Copy(this.uTextFile.Text, strUploadFile);

                        string strIp = "";
                        string serverPath = m_resSys.GetString("SYS_SERVERPATH");
                        if (serverPath == "http://10.61.61.71/QRP_PSTS_DEV/")
                        {
                             strIp = "10.61.61.71";
                        }

                        if (serverPath == "http://10.61.61.73/QRP_PSTS_DEV/")
                        {
                            strIp = "10.61.61.73";
                        }

                        string strFtpPath = "ftp://" + strIp + "/" + "UploadFile/INSProcInspectReqLotFile" + "/";
                        string strUserId = "qrp";
                        string strPwd = "logic12!@";

                        FileUpload_Header_Ftp(strUserId, strPwd, strUploadFile, strFtpPath);

                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);

                    }

                    // Method 호출
                    string strErrRtn = clsHeader.mfSaveINSProcInspectReqH_S(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                                                                        , dtLot, dtItem, dsResult, dtFault, dtCycle);

                    // 팦업창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리결과에 따른 메세지 박스
                    if (ErrRtn.ErrNum == 0)
                    {
                        // OUTPUT 값 저장

                        string strReqNo = ErrRtn.mfGetReturnValue(0);
                        string strReqSeq = ErrRtn.mfGetReturnValue(1);

                        this.uTextReqNo.Text = strReqNo;
                        this.uTextReqSeq.Text = strReqSeq;

                        // 첨부파일 업로드 메소드 호출
                        FileUpload(strReqNo, strReqSeq);


                        // 작성완료가 체크되어있는경우
                        if (this.uCheckCompleteFlag.Checked)
                        {
                            //MES I/F 진행
                            SaveMESInterFace(strReqNo, strReqSeq);

                            //Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                            //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                    "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                            //                    Infragistics.Win.HAlign.Right);

                            // QCN 자동이동
                            // MoveQCN(strReqNo, strReqSeq);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);
                            //Clear();
                        }
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                , ErrRtn.ErrMessage,
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                    ////}
                 }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            //try
            //{
            //    // SystemInfo ResourceSet
            //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //    DialogResult Result = new DialogResult();
            //    WinMessageBox msg = new WinMessageBox();

            //    if (this.uTextReqNo.Text == "" || this.uTextReqSeq.Text == "")
            //    {
            //        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //                            , "확인창", "입력사항 확인", "리스트에서 삭제할 정보를 선택해 주세요", Infragistics.Win.HAlign.Center);

            //        mfSearch();
            //        return;
            //    }
            //    else
            //    {
            //        // 삭제여부를 묻는다

            //        if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                        "확인창", "삭제확인", "입력한 정보를 삭제하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
            //        {
            //            // BL연결
            //            QRPBrowser brwChannel = new QRPBrowser();
            //            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
            //            QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
            //            brwChannel.mfCredentials(clsHeader);

            //            // 변수 설정
            //            string strPlantCode = this.uComboPlantCode.Value.ToString();
            //            string strReqNo = this.uTextReqNo.Text;
            //            string strReqSeq = this.uTextReqSeq.Text;

            //            // ProgressBar 생성
            //            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            //            Thread threadPop = m_ProgressPopup.mfStartThread();
            //            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
            //            this.MdiParent.Cursor = Cursors.WaitCursor;

            //            string strErrRtn = clsHeader.mfDeleteINSProcInspectReqALL(strPlantCode, strReqNo, strReqSeq);

            //            // ProgressBar Close
            //            this.MdiParent.Cursor = Cursors.Default;
            //            m_ProgressPopup.mfCloseProgressPopup(this);

            //            // 결과 검사

            //            TransErrRtn ErrRtn = new TransErrRtn();
            //            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
            //            if (ErrRtn.ErrNum == 0)
            //            {
            //                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            //                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                                            "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
            //                                            Infragistics.Win.HAlign.Right);
            //                // 리스트 갱신
            //                mfSearch();
            //            }
            //            else
            //            {
            //                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            //                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            //                                            "처리결과", "삭제처리결과", "입력한 정보를 삭제하지 못했습니다.",
            //                                            Infragistics.Win.HAlign.Right);
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //}
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridItem.Rows.Count > 0 || this.uGridSampling.Rows.Count > 0 || this.uGridFaultData.Rows.Count > 0)
                {
                    if (this.uGridItem.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridItem);
                    }

                    if (this.uGridSampling.Rows.Count > 0)
                        wGrid.mfDownLoadGridToExcel(this.uGridSampling);

                    if (this.uGridFaultData.Rows.Count > 0)
                        wGrid.mfDownLoadGridToExcel(this.uGridFaultData);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 메소드..

        #region 컬럼설정 메소드

        /// <summary>
        /// 지정된 Size만큼 컬럼추가하는 Method
        /// </summary>
        /// <param name="uGrid"> 그리드명 </param>
        /// <param name="intBandIndex"> Band인덱스 </param>
        /// <param name="strPoint">Point 컬럼</param>
        /// <param name="strSampleSizeColKey"> Size가 입력된 컬럼Key </param>
        /// <param name="strLastIndexColKey"> 생성된 컬럼보다 뒤에 위치해야할 컬럼들의 키가 저장된 배열 </param>
        private void CreateColumn(Infragistics.Win.UltraWinGrid.UltraGrid uGrid
                                , int intBandIndex
                                , String strPoint
                                , String strSampleSizeColKey
                                , String[] strLastIndexColKey
                                )
        {
            try
            {
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // SampleSize가 될 최대값 찾기
                int intSampleMax = 0;
                int intPoint = 0;
                int inttotMax = 0;

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value) * Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) > inttotMax)
                    {
                        inttotMax = Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value) * Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                        intSampleMax = Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value);
                        intPoint = Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    }
                }
                if (inttotMax > 99)
                    inttotMax = 99;

                ////// SampleSize 보다 큰 컬럼 제거
                ////for (int i = inttotMax+1; i <= 99; i++)
                ////{
                ////    if (uGrid.DisplayLayout.Bands[intBandIndex].Columns.Exists(i.ToString()))
                ////        uGrid.DisplayLayout.Bands[intBandIndex].Columns.Remove(i.ToString());
                ////}

                // 상세 SampleSize만큼 컬럼생성
                for (int i = 1; i <= inttotMax; i++)
                {
                    // 컬럼이 이미 존재하는경우(공정검사항목 테이블에 이미 저장되어 있는경우)
                    if (uGrid.DisplayLayout.Bands[intBandIndex].Columns.Exists(i.ToString()))
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Header.Caption = "X" + i.ToString();
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Hidden = false;
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].MaskInput = "{double:5.5}";
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Width = 70;
                    }
                    // 컬럼 신규생성(공정검사항목 테이블에 저장된 데이터가 아직없는경우)
                    else
                    {
                        wGrid.mfSetGridColumn(uGrid, intBandIndex, i.ToString(), "X" + i.ToString(), false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:5.5}", "0.0");
                    }
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].PromptChar = ' ';
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                }

                // 추가된 컬럼보다 뒤에 있어야 하는 컬럼들이 있으면 뒤로 보냄
                if (strLastIndexColKey.Length > 0)
                {
                    int LastIndex = uGrid.DisplayLayout.Bands[intBandIndex].Columns.Count;
                    for (int i = 0; i < strLastIndexColKey.Length; i++)
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[strLastIndexColKey[i]].Header.VisiblePosition = LastIndex + i;
                    }
                }

                // Size 만큼의 Cell만 입력 가능하도록나머지는 편집불가처리
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    int ActivateNum = Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value) * Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    if (ActivateNum > 99)
                        ActivateNum = 99;

                    for (int j = 1; j <= inttotMax; j++)
                    {
                        if (j <= ActivateNum)
                        {
                            uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.BackColor = Color.Empty;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                            uGrid.Rows[i].Cells[j.ToString()].Hidden = false;
                        }
                        else
                        {
                            uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.BackColor = Color.Gainsboro;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.ForeColor = Color.Empty;
                            uGrid.Rows[i].Cells[j.ToString()].Hidden = true;
                        }
                    }

                    ////for (int j = inttotMax; j > ActivateNum; j--)
                    ////{
                    ////    uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    ////    uGrid.Rows[i].Cells[j.ToString()].Appearance.BackColor = Color.White;
                    ////    uGrid.Rows[i].Cells[j.ToString()].Appearance.ForeColor = Color.White;
                    ////}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형에 맞게 Xn컬럼 설정
        /// </summary>
        private void SetSamplingGridColumn()
        {
            try
            {
                // 그리드 이벤트 헤제
                //this.uGridSampling.AfterCellUpdate -= new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSampling_AfterCellUpdate);
                this.uGridSampling.EventManager.AllEventsEnabled = false;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 선택 DropDown 적용용 DataTable
                int intStart = 0;
                if (this.uGridSampling.DisplayLayout.Bands[0].Columns.Exists("1"))
                    intStart = this.uGridSampling.DisplayLayout.Bands[0].Columns["1"].Index;
                else
                {
                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001099", "M000285"
                                                        , "M001366",
                                                        Infragistics.Win.HAlign.Right);
                    return;
                }
                int intSampleSize = 0;

                // 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                MaskString.DataType = typeof(String);
                MaskString.MaxLength = 50;
                MaskString.MaskInput = "";

                Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                // 합/부 데이터 테이블
                DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                {
                    intSampleSize = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value) * Convert.ToInt32(this.uGridSampling.Rows[i].Cells["SampleSize"].Value);
                    if (intSampleSize > 99)
                        intSampleSize = 99;
                    // 계량
                    if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "1")
                    {
                        this.uGridSampling.Rows[i].Cells["FaultQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSampling.Rows[i].Cells["FaultQty"].Appearance.BackColor = Color.Gainsboro;
                        this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                            if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // 계수
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // OK/NG
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "3")
                    {
                        //this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            wGrid.mfSetGridCellValueList(this.uGridSampling, i, this.uGridSampling.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "OK";
                            }
                        }
                    }
                    // 설명
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "4")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            this.uGridSampling.Rows[i].Cells[j].Editor = editorString;
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                    // 선택
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "5")
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                        QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                        brwChannel.mfCredentials(clsItem);

                        // 검사분류/유형/항목/DataType 에 따른 선택항목 조회 Method 호출
                        string strInspectItemCode = this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString();

                        DataTable dtSelect = clsItem.mfReadINSProcInspectItem_DataTypeSelect(this.uComboPlantCode.Value.ToString()
                                                                                            , strInspectItemCode
                                                                                            , this.uTextProductCode.Text
                                                                                            , this.uComboWorkProcess.Value.ToString()
                                                                                            , m_resSys.GetString("SYS_LANG"));

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            wGrid.mfSetGridCellValueList(this.uGridSampling, i, this.uGridSampling.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                    //else
                    //{
                    //    for (int j = intStart; j < intStart + intSampleSize; j++)
                    //    {
                    //        this.uGridSampling.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //        this.uGridSampling.Rows[i].Cells[j].Appearance.BackColor = Color.Gainsboro;
                    //        if (this.uGridSampling.Rows[i].Cells[j].Value == null)
                    //        {
                    //            if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    //                this.uGridSampling.Rows[i].Cells[j].Value = 0;
                    //            else
                    //                this.uGridSampling.Rows[i].Cells[j].Value = "";
                    //        }
                    //    }
                    //}
                }
                // 그리드 이벤트 등록
                //this.uGridSampling.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSampling_AfterCellUpdate);
                this.uGridSampling.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 저장정보 반환 메소드
        /// <summary>
        /// 헤더정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_HeaderDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                brwChannel.mfCredentials(clsHeader);

                dtRtn = clsHeader.mfSetDataInfo();

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                drRow["ReqNo"] = this.uTextReqNo.Text;
                drRow["ReqSeq"] = this.uTextReqSeq.Text;
                drRow["ReqDeptCode"] = m_resSys.GetString("SYS_DEPTCODE");
                drRow["ReqUserID"] = m_resSys.GetString("SYS_USERID");
                drRow["ReqDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                drRow["InspectReqTypeCode"] = "1";
                drRow["EmergencyTypeCode"] = "1";
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;

                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_LotDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                dtRtn = clsLot.mfSetDataInfo();

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                drRow["ReqNo"] = this.uTextReqNo.Text;
                drRow["ReqSeq"] = this.uTextReqSeq.Text;
                drRow["ReqLotSeq"] = 1;
                drRow["ProductCode"] = this.uTextProductCode.Text;
                //drRow["EquipCode"] = this.uTextEquipCode.Text;
                drRow["EquipCode"] = this.uComboEquip.Value.ToString();
                drRow["LotNo"] = this.uTextLotNo.Text.Trim();
                drRow["ProcInspectType"] = this.uComboProcInspectType.Value.ToString();
                drRow["ProcInspectPattern"] = "2";
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                drRow["LotSize"] = this.uTextLotSize.Text;
                drRow["InspectUserID"] = this.uTextInspectUserID.Text;
                drRow["InspectDate"] = Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd");
                drRow["InspectTime"] = Convert.ToDateTime(this.uDateInspectTime.Value).ToString("HH:mm:ss");
                if (this.uOptionPassFailFlag.CheckedIndex != -1)
                    drRow["PassFailFlag"] = this.uOptionPassFailFlag.Value.ToString();
                drRow["CompleteFlag"] = this.uCheckCompleteFlag.CheckedValue.ToString().Substring(0, 1);
                drRow["StackSeq"] = this.uComboStack.Value.ToString();
                drRow["WorkUserID"] = this.uTextWorkUserID.Text;
                //drRow["WorkProcessCode"] = this.uTextWorkProcessCode.Text;
                drRow["WorkProcessCode"] = this.uComboWorkProcess.Value.ToString();
                drRow["NowProcessCode"] = this.uTextNowProcessCode.Text;
                drRow["LotState"] = this.uTextLotProcessState.Text;
                drRow["HoldState"] = this.uTextProcessHoldState.Text;
                drRow["LossCheckFlag"] = this.uCheckLossCheckFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["LossQty"] = Convert.ToInt32(this.uNumLossQty.Value);
                //drRow["OUTSOURCINGVENDOR"] = this.uTextOUTSOURCINGVENDOR.Text;
                drRow["PCB_LAYOUT"] = this.uTextPCB_Layout.Text;
                drRow["WaferYN"] = this.uComboWafer.Value == null ? "" : this.uComboWafer.Value.ToString(); //2012-09-03
                drRow["HANDLER"] = this.uComboHandler.Value.ToString();
                drRow["PROGRAM"] = this.uTextProgram.Text;
                drRow["MESTrackInTFlag"] = this.uTextMESTrackInTFlag.Text;
                drRow["MESTrackInTDate"] = this.uTextMESTrackInTDate.Text;

                if (this.uTextFile.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextFile.Text);

                    strfileName = this.uTextLotNo.Text + "-" + this.uTextNowProcessCode.Text + "-" + DateTime.Now.ToString("yyyyMMddhhmmss") + "-" + fileDoc.Name;

                    drRow["FileName"] = strfileName;
                }
                else
                {
                    drRow["FileName"] = this.uTextFile.Text;
                }

                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item 정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_ItemDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                dtRtn = clsItem.mfSetDataInfo();

                if (this.uGridItem.Rows.Count > 0)
                {
                    this.uGridItem.ActiveCell = this.uGridItem.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridItem.Rows.Count; i++)
                    {
                        DataRow drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                        drRow["ReqLotSeq"] = 1;
                        drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                        drRow["Seq"] = this.uGridSampling.Rows[i].Cells["Seq"].Value.ToString();
                        drRow["ProcessCode"] = this.uGridSampling.Rows[i].Cells["ProcessCode"].Value.ToString();
                        //drRow["ProcessSeq"] = this.uGridSampling.Rows[i].Cells["ProcessSeq"].Value.ToString();
                        drRow["InspectGroupCode"] = this.uGridSampling.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                        drRow["InspectTypeCode"] = this.uGridSampling.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                        drRow["InspectItemCode"] = this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString();
                        if (this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value.Equals("True"))
                            drRow["InspectIngFlag"] = "T";
                        else
                            drRow["InspectIngFlag"] = "F";
                        drRow["StackSeq"] = this.uGridSampling.Rows[i].Cells["StackSeq"].Value.ToString();
                        drRow["Generation"] = this.uGridSampling.Rows[i].Cells["Generation"].Value.ToString();
                        //drRow["ProcessInspectFlag"] = this.uGridSampling.Rows[i].Cells["ProcessInspectFlag"].Value.ToString();
                        //drRow["ProductItemFlag"] = this.uGridSampling.Rows[i].Cells["ProductItemFlag"].Value.ToString();
                        //drRow["QualityItemFlag"] = this.uGridSampling.Rows[i].Cells["QualityItemFlag"].Value.ToString();
                        drRow["InspectCondition"] = this.uGridSampling.Rows[i].Cells["InspectCondition"].Value.ToString();
                        drRow["Method"] = this.uGridSampling.Rows[i].Cells["Method"].Value.ToString();
                        //drRow["SpecDesc"] = this.uGridSampling.Rows[i].Cells["SpecDesc"].Value.ToString();
                        //drRow["MeasureToolCode"] = this.uGridSampling.Rows[i].Cells["MeasureToolCode"].Value.ToString();
                        drRow["UpperSpec"] = this.uGridSampling.Rows[i].Cells["UpperSpec"].Value.ToString();
                        drRow["LowerSpec"] = this.uGridSampling.Rows[i].Cells["LowerSpec"].Value.ToString();
                        drRow["SpecRange"] = this.uGridSampling.Rows[i].Cells["SpecRange"].Value.ToString();
                        drRow["FaultQty"] = this.uGridSampling.Rows[i].Cells["FaultQty"].Value.ToString();
                        //drRow["SampleSize"] = this.uGridSampling.Rows[i].Cells["SampleSize"].Value.ToString();
                        drRow["ProcessSampleSize"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                        //drRow["ProductItemSampleSize"] = this.uGridSampling.Rows[i].Cells["ProductItemSampleSize"].Value.ToString();
                        //drRow["QualityItemSampleSize"] = this.uGridSampling.Rows[i].Cells["QualityItemSampleSize"].Value.ToString();
                        drRow["UnitCode"] = this.uGridSampling.Rows[i].Cells["UnitCode"].Value.ToString();
                        //drRow["InspectPeriod"] = this.uGridSampling.Rows[i].Cells["InspectPeriod"].Value.ToString();
                        //drRow["PeriodUnitCode"] = this.uGridSampling.Rows[i].Cells["PeriodUnitCode"].Value.ToString();
                        //drRow["CompareFlag"] = this.uGridSampling.Rows[i].Cells["CompareFlag"].Value.ToString();
                        drRow["DataType"] = this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString();
                        //drRow["EtcDesc"] = this.uGridSampling.Rows[i].Cells["EtcDesc"].Value.ToString();
                        drRow["InspectResultFlag"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString();
                        drRow["Mean"] = this.uGridSampling.Rows[i].Cells["Mean"].Value.ToString();
                        drRow["StdDev"] = this.uGridSampling.Rows[i].Cells["StdDev"].Value.ToString();
                        drRow["MaxValue"] = this.uGridSampling.Rows[i].Cells["MaxValue"].Value.ToString();
                        drRow["MinValue"] = this.uGridSampling.Rows[i].Cells["MinValue"].Value.ToString();
                        drRow["DataRange"] = this.uGridSampling.Rows[i].Cells["DataRange"].Value.ToString();
                        drRow["Cp"] = this.uGridSampling.Rows[i].Cells["Cp"].Value.ToString();
                        drRow["Cpk"] = this.uGridSampling.Rows[i].Cells["Cpk"].Value.ToString();
                        drRow["OCPCount"] = this.uGridSampling.Rows[i].Cells["OCPCount"].Value.ToString();
                        drRow["UpperRunCount"] = this.uGridSampling.Rows[i].Cells["UpperRunCount"].Value.ToString();
                        drRow["LowerRunCount"] = this.uGridSampling.Rows[i].Cells["LowerRunCount"].Value.ToString();
                        drRow["UpperTrendCount"] = this.uGridSampling.Rows[i].Cells["UpperTrendCount"].Value.ToString();
                        drRow["LowerTrendCount"] = this.uGridSampling.Rows[i].Cells["LowerTrendCount"].Value.ToString();
                        //drRow["CycleCount"] = this.uGridSampling.Rows[i].Cells["CycleCount"].Value.ToString();
                        drRow["LCL"] = this.uGridSampling.Rows[i].Cells["LCL"].Value.ToString();
                        drRow["CL"] = this.uGridSampling.Rows[i].Cells["CL"].Value.ToString();
                        drRow["UCL"] = this.uGridSampling.Rows[i].Cells["UCL"].Value.ToString();
                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사결과 데이터 Set으로 반환
        /// </summary>
        /// <returns></returns>
        private DataSet Rtn_ResultDataSet()
        {
            DataSet dsResult = new DataSet();
            try
            {
                //DataTable dtCount = new DataTable("Count");
                //DataTable dtMeasure = new DataTable("Measure");
                //DataTable dtOkNg = new DataTable("OkNg");
                //DataTable dtDesc = new DataTable("Desc");
                //DataTable dtSelect = new DataTable("Select");

                // 데이터 테이블 컬럼설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultCount), "ProcInspectResultCount");
                QRPINS.BL.INSPRC.ProcInspectResultCount clsCount = new QRPINS.BL.INSPRC.ProcInspectResultCount();
                brwChannel.mfCredentials(clsCount);
                DataTable dtCount = clsCount.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultMeasure), "ProcInspectResultMeasure");
                QRPINS.BL.INSPRC.ProcInspectResultMeasure clsMeasure = new QRPINS.BL.INSPRC.ProcInspectResultMeasure();
                brwChannel.mfCredentials(clsMeasure);
                DataTable dtMeasure = clsMeasure.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultOkNg), "ProcInspectResultOkNg");
                QRPINS.BL.INSPRC.ProcInspectResultOkNg clsOkNg = new QRPINS.BL.INSPRC.ProcInspectResultOkNg();
                brwChannel.mfCredentials(clsOkNg);
                DataTable dtOkNg = clsOkNg.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultDesc), "ProcInspectResultDesc");
                QRPINS.BL.INSPRC.ProcInspectResultDesc clsDesc = new QRPINS.BL.INSPRC.ProcInspectResultDesc();
                brwChannel.mfCredentials(clsDesc);
                DataTable dtDesc = clsDesc.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultSelect), "ProcInspectResultSelect");
                QRPINS.BL.INSPRC.ProcInspectResultSelect clsSelect = new QRPINS.BL.INSPRC.ProcInspectResultSelect();
                brwChannel.mfCredentials(clsSelect);
                DataTable dtSelect = clsSelect.mfSetDataInfo();

                // X1 시작지점 Index값 저장
                if (this.uGridSampling.DisplayLayout.Bands[0].Columns.Exists("1"))
                {
                    int intStart = this.uGridSampling.DisplayLayout.Bands[0].Columns["1"].Index;

                    if (this.uGridSampling.Rows.Count > 0)
                    {
                        this.uGridSampling.ActiveCell = this.uGridSampling.Rows[0].Cells[0];
                        DataRow drRow;
                        for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                        {
                            int intSampleSize = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value) * Convert.ToInt32(this.uGridSampling.Rows[i].Cells["SampleSize"].Value);
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "1")
                                {
                                    drRow = dtMeasure.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = 0.0;
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtMeasure.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "2")
                                {
                                    drRow = dtCount.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = 0.0;
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtCount.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "3")
                                {
                                    drRow = dtOkNg.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = "";
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtOkNg.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "4")
                                {
                                    drRow = dtDesc.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = "";
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtDesc.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "5")
                                {
                                    drRow = dtSelect.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = "";
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtSelect.Rows.Add(drRow);
                                }
                            }
                        }
                    }
                }
                dsResult.Tables.Add(dtCount);
                dsResult.Tables.Add(dtDesc);
                dsResult.Tables.Add(dtMeasure);
                dsResult.Tables.Add(dtOkNg);
                dsResult.Tables.Add(dtSelect);

                return dsResult;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dsResult;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형 정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_FaultDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                brwChannel.mfCredentials(clsFault);

                dtRtn = clsFault.mfSetDataInfo();
                DataRow drRow;

                //// 불합격인 경우만
                //if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                //{
                if (this.uGridFaultData.Rows.Count > 0)
                {
                    this.uGridFaultData.ActiveCell = this.uGridFaultData.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridFaultData.Rows.Count; i++)
                    {
                        if (this.uGridFaultData.Rows[i].Hidden == false)
                        {
                            drRow = dtRtn.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = this.uTextReqNo.Text;
                            drRow["ReqSeq"] = this.uTextReqSeq.Text;
                            drRow["ReqLotSeq"] = 1;
                            drRow["ReqItemSeq"] = Convert.ToInt32(this.uGridFaultData.Rows[i].Cells["ReqItemSeq"].Value);
                            drRow["ReqInspectSeq"] = this.uGridFaultData.Rows[i].RowSelectorNumber;
                            drRow["FaultTypeCode"] = this.uGridFaultData.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                            if (Convert.ToBoolean(this.uGridFaultData.Rows[i].Cells["QCNFlag"].Value) == true)
                                drRow["QCNFlag"] = "T";
                            else
                                drRow["QCNFlag"] = "F";

                            drRow["ITRFlag"] = Convert.ToBoolean(this.uGridFaultData.Rows[i].Cells["P_ITRFlag"].Value) == true ? "T" : "F";
                            drRow["InspectQty"] = Convert.ToDecimal(this.uGridFaultData.Rows[i].Cells["InspectQty"].Value);
                            drRow["FaultQty"] = Convert.ToDecimal(this.uGridFaultData.Rows[i].Cells["FaultQty"].Value);
                            drRow["UnitCode"] = this.uGridFaultData.Rows[i].Cells["UnitCode"].Value.ToString();

                            if (this.uGridFaultData.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo fileDoc = new FileInfo(this.uGridFaultData.Rows[i].Cells["FilePath"].Value.ToString());
                                drRow["FilePath"] = fileDoc.Name;
                            }
                            else
                                drRow["FilePath"] = this.uGridFaultData.Rows[i].Cells["FilePath"].Value.ToString();
                            drRow["ExpectProcessCode"] = this.uGridFaultData.Rows[i].Cells["ExpectProcessCode"].Value.ToString();
                            drRow["CauseEquipCode"] = this.uGridFaultData.Rows[i].Cells["CauseEquipCode"].Value.ToString();
                            drRow["WorkUserID"] = this.uGridFaultData.Rows[i].Cells["WorkUserID"].Value.ToString();
                            drRow["EtcDesc"] = this.uGridFaultData.Rows[i].Cells["EtcDesc"].Value.ToString();
                            dtRtn.Rows.Add(drRow);
                        }
                    }
                }
                //}
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 작성완료시 순회검사모니터링 저장용 데이터 테이블 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_CycleDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.Monitoring), "Monitoring");
                QRPINS.BL.INSPRC.Monitoring clsMot = new QRPINS.BL.INSPRC.Monitoring();
                brwChannel.mfCredentials(clsMot);

                dtRtn = clsMot.mfSetDataInfo();
                if (this.uCheckCompleteFlag.Checked)
                {
                    DataRow drRow = dtRtn.NewRow();
                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                    drRow["ReqNo"] = this.uTextReqNo.Text;
                    drRow["ReqSeq"] = this.uTextReqSeq.Text;
                    drRow["ReqLotSeq"] = 1;
                    //drRow["EquipCode"] = this.uTextEquipCode.Text;
                    drRow["EquipCode"] = this.uComboEquip.Value.ToString();
                    drRow["ProcessCode"] = ProcessCode;
                    drRow["ProductCode"] = this.uTextProductCode.Text;
                    dtRtn.Rows.Add(drRow);
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }
        #endregion

        #region 각테이블별 조회 메소드
        /// <summary>
        /// Lot정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_LotInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                // 메소드호출

                DataTable dtLot = clsLot.mfReadINSProcInspectReqLot(strPlantCode, strReqNo, strReqSeq, strLang);

                // 컨트롤에 값 적용
                this.uComboPlantCode.Value = dtLot.Rows[0]["PlantCode"].ToString();
                this.uTextCustomerCode.Text = dtLot.Rows[0]["CustomerCode"].ToString();
                this.uTextCustomerName.Text = dtLot.Rows[0]["CustomerName"].ToString();
                this.uComboProcInspectType.Value = dtLot.Rows[0]["ProcInspectType"].ToString();
                //EquipCode = dtLot.Rows[0]["EquipCode"].ToString();
                //this.uTextEquipCode.Text = dtLot.Rows[0]["EquipCode"].ToString();
                //this.uTextEquipName.Text = dtLot.Rows[0]["EquipName"].ToString();
                this.uComboEquip.Value = dtLot.Rows[0]["EquipCode"].ToString();
                this.uTextProductCode.Text = dtLot.Rows[0]["ProductCode"].ToString();
                this.uTextProductName.Text = dtLot.Rows[0]["ProductName"].ToString();
                this.uTextLotNo.Text = dtLot.Rows[0]["LotNo"].ToString();
                this.uTextLotSize.Text = Convert.ToInt32(dtLot.Rows[0]["QTY"].ToString()).ToString();
                this.uTextInspectUserID.Text = dtLot.Rows[0]["InspectUserID"].ToString();
                this.uTextInspectUserName.Text = dtLot.Rows[0]["InspectUserName"].ToString();
                this.uTextEtcDesc.Text = dtLot.Rows[0]["EtcDesc"].ToString();
                this.uDateInspectDate.Value = Convert.ToDateTime(dtLot.Rows[0]["InspectDate"]).ToString("yyyy-MM-dd");
                this.uDateInspectTime.Value = Convert.ToDateTime(dtLot.Rows[0]["InspectTime"]).ToString("HH:mm:ss");
                if (dtLot.Rows[0]["PassFailFlag"].ToString() != "")
                    this.uOptionPassFailFlag.Value = dtLot.Rows[0]["PassFailFlag"].ToString();
                this.uCheckCompleteFlag.Enabled = !Convert.ToBoolean(dtLot.Rows[0]["CompleteFlag"]);
                this.uCheckCompleteFlag.Checked = Convert.ToBoolean(dtLot.Rows[0]["CompleteFlag"]);
                this.uTextCompleteCheck.Text = dtLot.Rows[0]["CompleteFlag"].ToString();
                this.uComboStack.Value = dtLot.Rows[0]["StackSeq"].ToString();
                this.uTextCustomerProductCode.Text = dtLot.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                this.uTextPackage.Text = dtLot.Rows[0]["PACKAGE"].ToString();
                this.uTextNowProcessCode.Text = dtLot.Rows[0]["NowProcessCode"].ToString();
                this.uTextNowProcessName.Text = dtLot.Rows[0]["NowProcessName"].ToString();
                //this.uTextWorkProcessCode.Text = dtLot.Rows[0]["WorkProcessCode"].ToString();
                //this.uTextWorkProcessName.Text = dtLot.Rows[0]["WorkProcessName"].ToString();
                this.uComboWorkProcess.Value = dtLot.Rows[0]["WorkProcessCode"].ToString();
                this.uTextWorkUserID.Text = dtLot.Rows[0]["WorkUserID"].ToString();
                this.uTextWorkUserName.Text = dtLot.Rows[0]["WorkUserName"].ToString();

                //this.uTextLotProcessState.Text = dtLot.Rows[0]["LotState"].ToString();
                //this.uTextProcessHoldState.Text = dtLot.Rows[0]["HoldState"].ToString();

                this.uCheckLossCheckFlag.Checked = Convert.ToBoolean(dtLot.Rows[0]["LossCheckFlag"]);
                this.uNumLossQty.Value = Convert.ToInt32(dtLot.Rows[0]["LossQty"]);
                //this.uTextOUTSOURCINGVENDOR.Text = dtLot.Rows[0]["OUTSOURCINGVENDOR"].ToString();

                //this.uTextMESTrackInTCheck.Text = dtLot.Rows[0]["TrackInFlag"].ToString();

                // Lot 상태가 RUN 이면 TrackIn 처리한 상태??
                if (this.uTextLotProcessState.Text.Equals("RUN"))
                {
                    this.uButtonTrackIn.Enabled = false;
                    this.uTextMESTrackInTCheck.Text = dtLot.Rows[0]["TrackInFlag"].ToString();
                    //this.uCheckLossCheckFlag.Visible = true;
                    //this.uNumLossQty.Visible = true;
                }
                else if (this.uTextLotProcessState.Text.Equals("WAIT"))
                {
                    // TrackIn Button 활성화 여부
                    if (dtLot.Rows[0]["MESTrackInTFlag"].ToString().Equals("F") && dtLot.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                    {
                        this.uButtonTrackIn.Enabled = true;
                        this.uTextMESTrackInTCheck.Text = "T";
                    }
                    else
                    {
                        this.uButtonTrackIn.Enabled = false;
                        this.uTextMESTrackInTCheck.Text = dtLot.Rows[0]["TrackInFlag"].ToString();
                        //this.uCheckLossCheckFlag.Visible = true;
                        //this.uNumLossQty.Visible = true;
                        this.uLabelLossCheck.Visible = true;
                        this.uLabelLossQty.Visible = true;
                    }
                }

                this.uTextMESTrackInTFlag.Text = dtLot.Rows[0]["MESTrackInTFlag"].ToString();
                this.uTextMESTrackOutTFlag.Text = dtLot.Rows[0]["MESTrackOutTFlag"].ToString();
                this.uTextMESHoldTFlag.Text = dtLot.Rows[0]["MESHoldTFlag"].ToString();
                this.uTextMESSPCNTFlag.Text = dtLot.Rows[0]["MESSPCNTFlag"].ToString();

                //// 작성완료 상태면 체크박스 Enable로
                //if (this.uCheckCompleteFlag.Checked)
                //    this.uCheckCompleteFlag.Enabled = false;
                //else
                //    this.uCheckCompleteFlag.Enabled = true;

                // 필수입력사항 Enable 로
                this.uComboPlantCode.Enabled = false;
                this.uComboProcInspectType.Enabled = false;
                this.uTextLotNo.Enabled = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_ItemInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadINSProcInspectReqItem(strPlantCode, strReqNo, strReqSeq, strLang);

                this.uGridItem.DataSource = dtItem;
                this.uGridItem.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Sampling 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_SamplingInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                // 샘플링그리드 컬럼설정
                //InitSampleGrid();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadINSProcInspectReqItem_Sampling(strPlantCode, strReqNo, strReqSeq, strLang);

                this.uGridSampling.DataSource = dtItem;
                this.uGridSampling.DataBind();

                this.uGridSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

                if (this.uGridSampling.Rows.Count > 0)
                {
                    // SampleSize 만큼 컬럼생성 Method 호출
                    String[] strLastColKey = { "Mean", "SpecRange" };
                    CreateColumn(this.uGridSampling, 0, "SampleSize", "ProcessSampleSize", strLastColKey);

                    // 데이터 유형에 따른 그리드 설정 메소드 호출
                    SetSamplingGridColumn();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형정보 그리드 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_FaultInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtFault = clsItem.mfReadINSProcInspectReqItem_Fault(strPlantCode, strReqNo, strReqSeq, strLang);

                this.uGridFault.DataSource = dtFault;
                this.uGridFault.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 통계값, 합부판정
        /// <summary>
        /// 계수/계량형 결과값 검사 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementMeasureCount(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Instance 객체 생성
                QRPSTA.STABAS clsSTABAS = new QRPSTA.STABAS();

                QRPSTA.STASummary structSTA = new QRPSTA.STASummary();

                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;
                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                //int intLastIndex = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;
                int intSampleSize = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value));
                if (intSampleSize > 99)
                    intSampleSize = 99;
                int intLastIndex = intSampleSize + intStart;
                // Double형 배열 생성
                double[] dblXn = new double[intLastIndex - intStart];

                // Loop돌며 배열에 값 저장
                for (int i = 0; i < intLastIndex - intStart; i++)
                {
                    int intIndex = intStart + i;
                    dblXn[i] = Convert.ToDouble(e.Cell.Row.Cells[intIndex].Value);
                }

                ////// 관리 상/하한 한계썬 가져오는 메소드 호출
                ////QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticCL), "ProcessStaticCL");
                ////QRPSTA.BL.STAPRC.ProcessStaticCL clsStaticCL = new QRPSTA.BL.STAPRC.ProcessStaticCL();
                ////brwChannel.mfCredentials(clsStaticCL);

                ////DataTable dtCL = clsStaticCL.mfReadINSProcessStaticCL_XMR(this.uComboPlantCode.Value.ToString(), this.uTextProductCode.Text, ProcessCode
                ////                                                , e.Cell.Row.Cells["InspectItemCode"].Value.ToString()
                ////                                                , Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd"));

                ////if (dtCL.Rows.Count > 0)
                ////{
                ////    structSTA = clsSTABAS.mfCalcSummaryStat(dblXn, Convert.ToDouble(e.Cell.Row.Cells["LowerSpec"].Value)
                ////                                    , Convert.ToDouble(e.Cell.Row.Cells["UpperSpec"].Value), e.Cell.Row.Cells["SpecRange"].Value.ToString()
                ////                                    , true, Convert.ToDouble(dtCL.Rows[0]["XLCL"]), Convert.ToDouble(dtCL.Rows[0]["XUCL"]));
                ////}
                ////else
                ////{
                ////    structSTA = clsSTABAS.mfCalcSummaryStat(dblXn, Convert.ToDouble(e.Cell.Row.Cells["LowerSpec"].Value)
                ////                                    , Convert.ToDouble(e.Cell.Row.Cells["UpperSpec"].Value), e.Cell.Row.Cells["SpecRange"].Value.ToString()
                ////                                    , false, 0, 0);
                ////}

                structSTA = clsSTABAS.mfCalcSummaryStat(dblXn, Convert.ToDouble(e.Cell.Row.Cells["LowerSpec"].Value)
                                                , Convert.ToDouble(e.Cell.Row.Cells["UpperSpec"].Value), e.Cell.Row.Cells["SpecRange"].Value.ToString()
                                                , true, Convert.ToDouble(e.Cell.Row.Cells["LCL"].Value), Convert.ToDouble(e.Cell.Row.Cells["UCL"].Value));

                // 합부판정
                if (structSTA.AcceptFlagDA)
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                    e.Cell.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                    e.Cell.Row.Appearance.BackColor = Color.Salmon;
                }

                // 불량수량 입력
                e.Cell.Row.Cells["FaultQty"].Value = structSTA.SLOverCount;

                // 통계값
                e.Cell.Row.Cells["Mean"].Value = structSTA.MeanDA;
                e.Cell.Row.Cells["MaxValue"].Value = structSTA.MaxDA;
                e.Cell.Row.Cells["MinValue"].Value = structSTA.MinDA;
                e.Cell.Row.Cells["DataRange"].Value = structSTA.RangeDA;
                e.Cell.Row.Cells["StdDev"].Value = structSTA.StdDevDA;
                e.Cell.Row.Cells["Cp"].Value = structSTA.CpDA;
                e.Cell.Row.Cells["Cpk"].Value = structSTA.CpkDA;


                // SPCN 여부 판단
                int intOCPCount = 0;
                int intUpperRunCount = 0;
                int intLowerRunCount = 0;
                int intUpperTrendCount = 0;
                int intLowerTrendCount = 0;

                // 3개월 이전 데이터가 6개가 되는지 판단하는 변수
                bool bolCheck6Count = true;

                decimal dblUCL = Convert.ToDecimal(e.Cell.Row.Cells["UCL"].Value);
                decimal dblLCL = Convert.ToDecimal(e.Cell.Row.Cells["LCL"].Value);

                if (!dblUCL.Equals(0) && !dblLCL.Equals(0))
                {
                    // OCPCount
                    if (Convert.ToDecimal(e.Cell.Row.Cells["Mean"].Value) > dblUCL || Convert.ToDecimal(e.Cell.Row.Cells["Mean"].Value) < dblLCL)
                        intOCPCount++;
                }

                // 3개월 이전 데이터가 6개가 되는지 판단
                for (int i = 1; i <= 6; i++)
                {
                    if (e.Cell.Row.Cells["Mean" + i.ToString()].Value != DBNull.Value)
                    {
                    }
                    else
                    {
                        bolCheck6Count = false;
                        break;
                    }
                }

                // 상향Run, 하향Run, 상향Trend, 하향Trend
                // 평균값이 6개가 되지 않으면 수행하지 않는다.
                if (bolCheck6Count)
                {
                    // 상향 Trend
                    if ((Convert.ToDecimal(e.Cell.Row.Cells["Mean1"].Value) < Convert.ToDecimal(e.Cell.Row.Cells["Mean2"].Value)) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean2"].Value) < Convert.ToDecimal(e.Cell.Row.Cells["Mean3"].Value)) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean3"].Value) < Convert.ToDecimal(e.Cell.Row.Cells["Mean4"].Value)) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean4"].Value) < Convert.ToDecimal(e.Cell.Row.Cells["Mean5"].Value)) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean5"].Value) < Convert.ToDecimal(e.Cell.Row.Cells["Mean6"].Value)) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean6"].Value) < Convert.ToDecimal(e.Cell.Row.Cells["Mean"].Value)))
                    {
                        intUpperTrendCount++;
                    }
                    // 하향 Trend
                    else if ((Convert.ToDecimal(e.Cell.Row.Cells["Mean1"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["Mean2"].Value)) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean2"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["Mean3"].Value)) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean3"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["Mean4"].Value)) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean4"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["Mean5"].Value)) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean5"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["Mean6"].Value)) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean6"].Value) > Convert.ToDecimal(e.Cell.Row.Cells["Mean"].Value)))
                    {
                        intLowerTrendCount++;
                    }

                    // 하향 Run
                    if ((Convert.ToDecimal(e.Cell.Row.Cells["Mean1"].Value) < dblLCL) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean2"].Value) < dblLCL) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean3"].Value) < dblLCL) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean4"].Value) < dblLCL) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean5"].Value) < dblLCL) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean6"].Value) < dblLCL) &&
                        (Convert.ToDecimal(e.Cell.Row.Cells["Mean"].Value) < dblLCL))
                    {
                        intLowerRunCount++;
                    }
                    // 상향 Trend
                    else if ((Convert.ToDecimal(e.Cell.Row.Cells["Mean1"].Value) > dblUCL) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean2"].Value) > dblUCL) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean3"].Value) > dblUCL) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean4"].Value) > dblUCL) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean5"].Value) > dblUCL) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean6"].Value) > dblUCL) &&
                            (Convert.ToDecimal(e.Cell.Row.Cells["Mean6"].Value) > dblUCL))
                    {
                        intUpperRunCount++;
                    }
                }

                // SPCN값 대입
                e.Cell.Row.Cells["OCPCount"].Value = intOCPCount;
                e.Cell.Row.Cells["UpperRunCount"].Value = intUpperRunCount;
                e.Cell.Row.Cells["LowerRunCount"].Value = intLowerRunCount;
                e.Cell.Row.Cells["UpperTrendCount"].Value = intUpperTrendCount;
                e.Cell.Row.Cells["LowerTrendCount"].Value = intLowerTrendCount;

                ////for (int i = 0; i < this.uGridItem.Rows.Count; i++)
                ////{
                ////    if (Convert.ToInt32(this.uGridItem.Rows[i].Cells["Seq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["Seq"].Value))
                ////    {
                ////        // 평균
                ////        this.uGridItem.Rows[i].Cells["Mean"].Value = structSTA.MeanDA;
                ////        // MaxValue
                ////        this.uGridItem.Rows[i].Cells["MaxValue"].Value = structSTA.MaxDA;
                ////        // MinValue
                ////        this.uGridItem.Rows[i].Cells["MinValue"].Value = structSTA.MinDA;
                ////        // Range
                ////        this.uGridItem.Rows[i].Cells["DataRange"].Value = structSTA.RangeDA;
                ////        // StdDev
                ////        this.uGridItem.Rows[i].Cells["StdDev"].Value = structSTA.StdDevDA;
                ////        // Cp
                ////        this.uGridItem.Rows[i].Cells["Cp"].Value = structSTA.CpDA;
                ////        // Cpk
                ////        this.uGridItem.Rows[i].Cells["Cpk"].Value = structSTA.CpkDA;

                ////        // SPCN 여부는 수동으로 기존 현재평균과 3개월의 최근 6개 데이터를 비교해서 정한다.
                ////        //////// OCPCount
                ////        //////this.uGridItem.Rows[i].Cells["OCPCount"].Value = structSTA.SLOverCount;
                ////        //////// UpperRunCount
                ////        //////this.uGridItem.Rows[i].Cells["UpperRunCount"].Value = structSTA.UpwardRunCount;
                ////        //////// LowerRunCount
                ////        //////this.uGridItem.Rows[i].Cells["LowerRunCount"].Value = structSTA.DownwardRunCount;
                ////        //////// UpperTrendCount
                ////        //////this.uGridItem.Rows[i].Cells["UpperTrendCount"].Value = structSTA.UpwardTrendCount;
                ////        //////// LowerTrendCount
                ////        //////this.uGridItem.Rows[i].Cells["LowerTrendCount"].Value = structSTA.DownwardTrendCount;
                ////        //////break;
                ////    }
                ////}
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// OK/NG 합/부 판정 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementOkNg(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;
                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                //int intLastIndex = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;
                int intSampleSize = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value));
                if (intSampleSize > 99)
                    intSampleSize = 99;
                int intLastIndex = intSampleSize + intStart;

                bool bolCheck = true;
                int intFaultCount = 0;

                // Loop 돌며 결과값 검사
                for (int i = intStart; i < intLastIndex; i++)
                {
                    if (e.Cell.Row.Cells[i].Value.ToString() == "NG")
                    {
                        bolCheck = false;
                        intFaultCount += 1;
                    }
                }

                // 불량수량 Update
                e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;

                if (bolCheck)
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                    e.Cell.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                    e.Cell.Row.Appearance.BackColor = Color.Salmon;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형 선택 합부판정(Bom인경우 Bom테이블에 자재코드가 존재하면 OK아니면 NG BomCheckFlag가 false이면 기존방식대로 선택
        /// </summary>
        /// <param name="e"></param>
        private void JudgementSelect(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;
                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                //int intLastIndex = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;
                int intSampleSize = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value));
                if (intSampleSize > 99)
                    intSampleSize = 99;
                int intLastIndex = intSampleSize + intStart;

                // BomCheckFlag 확인
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType clsType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(clsType);

                string strPlantCode = this.uComboPlantCode.Value.ToString();
                string strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                string strInspectTypeCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();

                DataTable dtBomCheck = clsType.mfReadMASInspectType_BomChecFlag(strPlantCode, strInspectGroupCode, strInspectTypeCode);

                // BomCheckFlag 가 True 이면
                if (dtBomCheck.Rows[0]["BomCheckFlag"].ToString().Equals("T"))
                {
                    bool bolCheck = true;
                    int intFaultCount = 0;
                    // Loop 돌며 결과값 검사
                    for (int i = intStart; i < intLastIndex; i++)
                    {
                        if (!e.Cell.Row.Cells[i].Value.ToString().Equals(string.Empty))
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                            QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                            brwChannel.mfCredentials(clsItem);

                            DataTable dtInspectResult = clsItem.mfReadCCSInspectReqItem_DataTypeSelectInspectResult_MASBOM(strPlantCode, this.uTextProductCode.Text
                                                                                                                        , this.uComboWorkProcess.Value.ToString(), e.Cell.Row.Cells[i].Value.ToString());
                            if (dtInspectResult.Rows[0]["InspectResult"].ToString().Equals("NG"))
                            {
                                bolCheck = false;
                                //break;
                                intFaultCount += 1;
                            }
                        }
                    }

                    if (bolCheck)
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                        e.Cell.Row.Appearance.BackColor = Color.Empty;
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                        e.Cell.Row.Appearance.BackColor = Color.Salmon;
                    }

                    e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void Search_STAValue(string strGeneration)
        {
            try
            {
                // 그리드 이벤트 잠금
                this.uGridSampling.EventManager.AllEventsEnabled = false;

                // 이전 3개월 데이터 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DateTime dateInspectDate = Convert.ToDateTime(this.uDateInspectDate.Value);                                                     // 검사일
                DateTime dateFromDate = new DateTime(dateInspectDate.AddMonths(-3).Year, dateInspectDate.AddMonths(-3).Month, 1).AddDays(-1);   // 검사일의 전달로부터 3개월전 1일날짜
                DateTime dateToDate = new DateTime(dateInspectDate.Year, dateInspectDate.Month, 1).AddDays(-1);                                 // 검사일의 전달 마지막 날짜

                DataTable dtData = clsItem.mfReadINSProcInspectReqItem_Mean_Range_FromToDate(this.uComboPlantCode.Value.ToString(), this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                    , this.uComboStack.Value.ToString(), strGeneration
                                                                                    , this.uComboWorkProcess.Value.ToString(), this.uComboEquip.Value.ToString()
                                                                                    , dateFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                    , dateToDate.ToString("yyyy-MM-dd 21:59:59"));

                // 2012.03.09 분기별 날짜 구하는부분 추가
                DateTime datePrevFromDate = new DateTime();
                DateTime datePrevToDate = new DateTime();
                if (dateInspectDate.Month.Equals(1) || dateInspectDate.Month.Equals(2) || dateInspectDate.Month.Equals(3))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.AddYears(-1).Year, 9, 30);
                    datePrevToDate = new DateTime(dateInspectDate.AddYears(-1).Year, 12, 31);
                }
                else if (dateInspectDate.Month.Equals(4) || dateInspectDate.Month.Equals(5) || dateInspectDate.Month.Equals(6))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.AddYears(-1).Year, 12, 31);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 3, 31);
                }
                else if (dateInspectDate.Month.Equals(7) || dateInspectDate.Month.Equals(8) || dateInspectDate.Month.Equals(9))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.Year, 3, 31);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 6, 30);
                }
                else if (dateInspectDate.Month.Equals(10) || dateInspectDate.Month.Equals(11) || dateInspectDate.Month.Equals(12))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.Year, 6, 30);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 9, 30);
                }

                DataTable dtPrevData = clsItem.mfReadINSProcInspectReqItem_Mean_Range_FromToDate(this.uComboPlantCode.Value.ToString(), this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                    , this.uComboStack.Value.ToString(), strGeneration
                                                                                    , this.uComboWorkProcess.Value.ToString(), this.uComboEquip.Value.ToString()
                                                                                    , datePrevFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                    , datePrevToDate.ToString("yyyy-MM-dd 21:59:59"));

                DataRow[] drRow;
                QRPSTA.STASPC clsSTAPRC = new QRPSTA.STASPC();
                QRPSTA.STAControlLimit XBarCL = new QRPSTA.STAControlLimit();

                // 그리드 Loop돌며 LCL, CL UCL 계산
                for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                {
                    if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString().Equals("1"))
                    {
                        string strInspectItemCode = this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString();
                        int intSampleSize = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value);
                        if (intSampleSize.Equals(0))
                            intSampleSize = 5;
                        DataTable dtMean = new DataTable();

                        // 2012.03.09 추가
                        // 분기데이터가 있는경우 분기데이터로, 없는경우 3개월자료로
                        if (dtPrevData.Rows.Count > 0)
                        {
                            drRow = dtPrevData.Select("InspectItemCode = '" + strInspectItemCode + "'");
                            dtMean = dtPrevData.Clone();
                            foreach (DataRow dr in drRow)
                            {
                                dtMean.ImportRow(dr);
                            }
                        }
                        else
                        {
                            drRow = dtData.Select("InspectItemCode = '" + strInspectItemCode + "'");
                            dtMean = dtData.Clone();
                            foreach (DataRow dr in drRow)
                            {
                                dtMean.ImportRow(dr);
                            }
                        }

                        DataTable dtMean1 = dtMean.Copy();
                        dtMean1.DefaultView.Sort = "InspectDateTime ASC";
                        DataTable dtMeanValue = dtMean1.DefaultView.ToTable(false, "Mean");
                        DataTable dtRangeValue = dtMean1.DefaultView.ToTable(false, "DataRange");

                        XBarCL = clsSTAPRC.mfCalcControlLimitXBarWithR(dtMeanValue, dtRangeValue, intSampleSize);

                        // LCL, CL, UCL값 설정(소수점 6자리에서 반올림)
                        this.uGridSampling.Rows[i].Cells["LCL"].Value = Math.Round(XBarCL.XBRLCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridSampling.Rows[i].Cells["CL"].Value = Math.Round(XBarCL.XBCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridSampling.Rows[i].Cells["UCL"].Value = Math.Round(XBarCL.XBRUCL, 5, MidpointRounding.AwayFromZero);

                        this.uGridItem.Rows[i].Cells["LCL"].Value = Math.Round(XBarCL.XBRLCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridItem.Rows[i].Cells["CL"].Value = Math.Round(XBarCL.XBCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridItem.Rows[i].Cells["UCL"].Value = Math.Round(XBarCL.XBRUCL, 5, MidpointRounding.AwayFromZero);

                        // 상위 6개 평균값 설정
                        for (int j = 6; j > 0; j--)
                        {
                            if (6 < dtMean.Rows.Count)
                            {
                                this.uGridSampling.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[6 - j]["Mean"];
                                this.uGridItem.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[6 - j]["Mean"];
                            }
                            else
                            {
                                if (j > dtMean.Rows.Count)
                                {
                                    this.uGridSampling.Rows[i].Cells["Mean" + j.ToString()].Value = DBNull.Value;
                                    this.uGridItem.Rows[i].Cells["Mean" + j.ToString()].Value = DBNull.Value;
                                }
                                else
                                {
                                    this.uGridSampling.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[dtMean.Rows.Count - j]["Mean"];
                                    this.uGridItem.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[dtMean.Rows.Count - j]["Mean"];
                                }
                            }
                        }
                    }
                }

                // 그리드 이벤트 잠금해제
                this.uGridSampling.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region UCL, LCL 계산메소드 변경(SampleSize 추가)에 의한 주석처리
        /*
        private void Search_STAValue(string strGeneration)
        {
            try
            {
                // 그리드 이벤트 잠금
                this.uGridSampling.EventManager.AllEventsEnabled = false;

                // 이전 3개월 데이터 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DateTime dateInspectDate = Convert.ToDateTime(this.uDateInspectDate.Value);                                                     // 검사일
                DateTime dateFromDate = new DateTime(dateInspectDate.AddMonths(-3).Year, dateInspectDate.AddMonths(-3).Month, 1).AddDays(-1);   // 검사일의 전달로부터 3개월전 1일날짜
                DateTime dateToDate = new DateTime(dateInspectDate.Year, dateInspectDate.Month, 1).AddDays(-1);                                 // 검사일의 전달 마지막 날짜

                DataTable dtData = clsItem.mfReadINSProcInspectReqItem_Mean_Range_FromToDate(this.uComboPlantCode.Value.ToString(), this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                    , this.uComboStack.Value.ToString(), strGeneration
                                                                                    , this.uComboWorkProcess.Value.ToString(), this.uComboEquip.Value.ToString()
                                                                                    , dateFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                    , dateToDate.ToString("yyyy-MM-dd 21:59:59"));

                // 2012.03.09 분기별 날짜 구하는부분 추가
                DateTime datePrevFromDate = new DateTime();
                DateTime datePrevToDate = new DateTime();
                if (dateInspectDate.Month.Equals(1) || dateInspectDate.Month.Equals(2) || dateInspectDate.Month.Equals(3))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.AddYears(-1).Year, 9, 30);
                    datePrevToDate = new DateTime(dateInspectDate.AddYears(-1).Year, 12, 31);
                }
                else if (dateInspectDate.Month.Equals(4) || dateInspectDate.Month.Equals(5) || dateInspectDate.Month.Equals(6))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.AddYears(-1).Year, 12, 31);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 3, 31);
                }
                else if (dateInspectDate.Month.Equals(7) || dateInspectDate.Month.Equals(8) || dateInspectDate.Month.Equals(9))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.Year, 3, 31);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 6, 30);
                }
                else if (dateInspectDate.Month.Equals(10) || dateInspectDate.Month.Equals(11) || dateInspectDate.Month.Equals(12))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.Year, 6, 30);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 9, 30);
                }

                DataTable dtPrevData = clsItem.mfReadINSProcInspectReqItem_Mean_Range_FromToDate(this.uComboPlantCode.Value.ToString(), this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                    , this.uComboStack.Value.ToString(), strGeneration
                                                                                    , this.uComboWorkProcess.Value.ToString(), this.uComboEquip.Value.ToString()
                                                                                    , datePrevFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                    , datePrevToDate.ToString("yyyy-MM-dd 21:59:59"));

                DataRow[] drRow;
                QRPSTA.STASPC clsSTAPRC = new QRPSTA.STASPC();
                QRPSTA.STAControlLimit ControlLimit = new QRPSTA.STAControlLimit();

                // 그리드 Loop돌며 LCL, CL UCL 계산
                for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                {
                    if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString().Equals("1"))
                    {
                        string strInspectItemCode = this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString();
                        DataTable dtMean = new DataTable();

                        // 2012.03.09 추가
                        // 분기데이터가 있는경우 분기데이터로, 없는경우 3개월자료로
                        if (dtPrevData.Rows.Count > 0)
                        {
                            drRow = dtPrevData.Select("InspectItemCode = '" + strInspectItemCode + "'");
                            dtMean = dtPrevData.Clone();
                            foreach (DataRow dr in drRow)
                            {
                                dtMean.ImportRow(dr);
                            }
                        }
                        else
                        {
                            drRow = dtData.Select("InspectItemCode = '" + strInspectItemCode + "'");
                            dtMean = dtData.Clone();
                            foreach (DataRow dr in drRow)
                            {
                                dtMean.ImportRow(dr);
                            }
                        }

                        DataTable dtMean1 = dtMean.Copy();
                        dtMean1.DefaultView.Sort = "InspectDateTime ASC";
                        DataTable dtRawValue = new DataTable();
                        DataTable dtStdDevValue = new DataTable();
                        DataTable dtMeanValue = dtMean1.DefaultView.ToTable(false, "Mean");
                        DataTable dtRangeValue = dtMean1.DefaultView.ToTable(false, "DataRange");

                        // 초기화
                        ControlLimit.InitSTAControlLimit();

                        ControlLimit = clsSTAPRC.mfCalcControlLimit(dtRawValue, dtMeanValue, dtRangeValue, dtStdDevValue);

                        // LCL, CL, UCL값 설정(소수점 6자리에서 반올림)
                        this.uGridSampling.Rows[i].Cells["LCL"].Value = Math.Round(ControlLimit.XBRLCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridSampling.Rows[i].Cells["CL"].Value = Math.Round(ControlLimit.XBCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridSampling.Rows[i].Cells["UCL"].Value = Math.Round(ControlLimit.XBRUCL, 5, MidpointRounding.AwayFromZero);

                        this.uGridItem.Rows[i].Cells["LCL"].Value = Math.Round(ControlLimit.XBRLCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridItem.Rows[i].Cells["CL"].Value = Math.Round(ControlLimit.XBCL, 5, MidpointRounding.AwayFromZero);
                        this.uGridItem.Rows[i].Cells["UCL"].Value = Math.Round(ControlLimit.XBRUCL, 5, MidpointRounding.AwayFromZero);

                        // 상위 6개 평균값 설정
                        for (int j = 6; j > 0; j--)
                        {
                            if (6 < dtMean.Rows.Count)
                            {
                                this.uGridSampling.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[6 - j]["Mean"];
                                this.uGridItem.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[6 - j]["Mean"];
                            }
                            else
                            {
                                if (j > dtMean.Rows.Count)
                                {
                                    this.uGridSampling.Rows[i].Cells["Mean" + j.ToString()].Value = DBNull.Value;
                                    this.uGridItem.Rows[i].Cells["Mean" + j.ToString()].Value = DBNull.Value;
                                }
                                else
                                {
                                    this.uGridSampling.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[dtMean.Rows.Count - j]["Mean"];
                                    this.uGridItem.Rows[i].Cells["Mean" + j.ToString()].Value = dtMean.Rows[dtMean.Rows.Count - j]["Mean"];
                                }
                            }
                        }
                    }
                }

                // 그리드 이벤트 잠금해제
                this.uGridSampling.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
         * */
        #endregion
        #endregion

        #region MES I/F 기존소스
        /////////// <summary>
        /////////// MES I/F 저장처리부분
        /////////// </summary>
        /////////// <param name="strReqNo">관리번호</param>
        /////////// <param name="strReqSeq">관리번호순번</param>
        ////////private void SaveMESInterFace(string strReqNo, string strReqSeq)
        ////////{
        ////////    try
        ////////    {
        ////////        // SystemInfo ResourceSet
        ////////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        ////////        DialogResult Result = new DialogResult();
        ////////        WinMessageBox msg = new WinMessageBox();
        ////////        QRPBrowser brwChannel = new QRPBrowser();
        ////////        string strErrRtn = string.Empty;
        ////////        TransErrRtn ErrRtn = new TransErrRtn();

        ////////        bool bolSaveCheck = true;

        ////////        // QCN 불량인지 QC 불량인지 판단
        ////////        // BL 연결
        ////////        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
        ////////        QRPINS.BL.INSPRC.ProcInspectResultFault clsFault1 = new QRPINS.BL.INSPRC.ProcInspectResultFault();
        ////////        brwChannel.mfCredentials(clsFault1);

        ////////        DataTable dtCheck = clsFault1.mfReadINSProcInspectResultFault_MoveQCN(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq);

        ////////        #region TracnIn 공정이 아닌경우
        ////////        // TrackIn 공정인지 아닌지 구분
        ////////        if (this.uTextMESTrackInTCheck.Text.Equals("F"))
        ////////        {
        ////////            // TrackIn 공정이 아닌경우
        ////////            // 검사결과 확인
        ////////            #region 불합격
        ////////            if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))        // 불합격인 경우
        ////////            {
        ////////                // Hold 상태 확인
        ////////                if (this.uTextProcessHoldState.Text.Equals("NotOnHold"))     // Hold가 아닌경우
        ////////                {
        ////////                    // LotHold 요청
        ////////                    DataTable dtLotList = new DataTable();
        ////////                    dtLotList.Columns.Add("LOTID", typeof(string));
        ////////                    DataRow drRow = dtLotList.NewRow();
        ////////                    drRow["LOTID"] = this.uTextLotNo.Text;
        ////////                    dtLotList.Rows.Add(drRow);

        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                    brwChannel.mfCredentials(clsLot);

        ////////                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                    drRow = dtStdInfo.NewRow();
        ////////                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                    drRow["ReqNo"] = strReqNo;
        ////////                    drRow["ReqSeq"] = strReqSeq;
        ////////                    drRow["ReqLotSeq"] = 1;
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    // HoldCode 가져오는 메소드 호출
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
        ////////                    QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
        ////////                    brwChannel.mfCredentials(clsReason);
        ////////                    DataTable dtReason = new DataTable();
        ////////                    // QCN 불량인경우 QCN Hold 코드로 Hold 요청한다
        ////////                    if (dtCheck.Rows.Count > 0)
        ////////                    {
        ////////                        dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), "frmINSZ0010");
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);
        ////////                    }

        ////////                    string strHoldCode = string.Empty;
        ////////                    if (dtReason.Rows.Count > 0)
        ////////                        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();
        ////////                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESHold(this.Name,
        ////////                                                            this.uTextInspectUserID.Text,
        ////////                                                            this.uTextEtcDesc.Text,     // Comment
        ////////                                                            strHoldCode,     // HoldCode
        ////////                                                            dtLotList,
        ////////                                                            m_resSys.GetString("SYS_USERIP"),
        ////////                                                            dtStdInfo);
        ////////                    // 결과검사
        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                    {
        ////////                        bolSaveCheck = false;

        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES Lot Hold요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        //// 조회 Method 호출
        ////////                        //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        return;
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        //////// 조회 Method 호출
        ////////                        //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                        ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Hold 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                }
        ////////            }
        ////////            #endregion

        ////////            #region 합격
        ////////            else if (this.uOptionPassFailFlag.CheckedIndex.Equals(0))     // 합격인 경우
        ////////            {
        ////////                // Hold 상태 확인
        ////////                if (this.uTextProcessHoldState.Text.Equals("OnHold"))     // Hold 인경우
        ////////                {
        ////////                    // Realease 코드 전송
        ////////                    DataTable dtLotInfo = new DataTable();
        ////////                    dtLotInfo.Columns.Add("LOTID", typeof(string));
        ////////                    dtLotInfo.Columns.Add("HOLDCODE", typeof(string));

        ////////                    // HoldCode 가져오는 메소드 호출
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
        ////////                    QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
        ////////                    brwChannel.mfCredentials(clsReason);

        ////////                    DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

        ////////                    string strHoldCode = string.Empty;
        ////////                    if (dtReason.Rows.Count > 0)
        ////////                        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

        ////////                    // dtLotInfo 테이블 설정
        ////////                    DataRow drLot = dtLotInfo.NewRow();
        ////////                    drLot["LOTID"] = this.uTextLotNo.Text.Trim();
        ////////                    drLot["HOLDCODE"] = strHoldCode;
        ////////                    dtLotInfo.Rows.Add(drLot);

        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                    brwChannel.mfCredentials(clsLot);

        ////////                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                    DataRow drRow = dtStdInfo.NewRow();
        ////////                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                    drRow["ReqNo"] = strReqNo;
        ////////                    drRow["ReqSeq"] = strReqSeq;
        ////////                    drRow["ReqLotSeq"] = 1;
        ////////                    drRow["FormName"] = this.Name;
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    // Release 메소드 호출
        ////////                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESRelease(dtStdInfo
        ////////                                                                            , dtLotInfo
        ////////                                                                            , this.uTextInspectUserID.Text
        ////////                                                                            , m_resSys.GetString("SYS_USERID")
        ////////                                                                            , m_resSys.GetString("SYS_USERIP"));

        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                    {
        ////////                        bolSaveCheck = false;
        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES Release요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        //// 조회 Method 호출
        ////////                        //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        return;
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        //////// 조회 Method 호출
        ////////                        //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                        ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Release 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                }
        ////////            }
        ////////            #endregion
        ////////        }
        ////////        #endregion

        ////////        #region TrackIn 공정
        ////////        else if (this.uTextMESTrackInTCheck.Text.Equals("T"))
        ////////        {
        ////////            // TrackIn 공정인 경우
        ////////            // 검사결과 확인
        ////////            #region 합격
        ////////            if (this.uOptionPassFailFlag.CheckedIndex.Equals(0))     // 합격인 경우
        ////////            {
        ////////                // Hold 상태 확인
        ////////                #region Release 처리
        ////////                if (this.uTextProcessHoldState.Text.Equals("OnHold"))     // Hold 인경우
        ////////                {
        ////////                    // Realease 코드 전송
        ////////                    DataTable dtLotInfo = new DataTable();
        ////////                    dtLotInfo.Columns.Add("LOTID", typeof(string));
        ////////                    dtLotInfo.Columns.Add("HOLDCODE", typeof(string));

        ////////                    // HoldCode 가져오는 메소드 호출
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
        ////////                    QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
        ////////                    brwChannel.mfCredentials(clsReason);

        ////////                    DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

        ////////                    string strHoldCode = string.Empty;
        ////////                    if (dtReason.Rows.Count > 0)
        ////////                        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

        ////////                    // dtLotInfo 테이블 설정
        ////////                    DataRow drLot = dtLotInfo.NewRow();
        ////////                    drLot["LOTID"] = this.uTextLotNo.Text.Trim();
        ////////                    drLot["HOLDCODE"] = strHoldCode;
        ////////                    dtLotInfo.Rows.Add(drLot);

        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                    brwChannel.mfCredentials(clsLot);

        ////////                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                    DataRow drRow = dtStdInfo.NewRow();
        ////////                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                    drRow["ReqNo"] = strReqNo;
        ////////                    drRow["ReqSeq"] = strReqSeq;
        ////////                    drRow["ReqLotSeq"] = 1;
        ////////                    drRow["FormName"] = this.Name;
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    // Release 메소드 호출
        ////////                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESRelease(dtStdInfo
        ////////                                                                            , dtLotInfo
        ////////                                                                            , this.uTextInspectUserID.Text
        ////////                                                                            , m_resSys.GetString("SYS_USERID")
        ////////                                                                            , m_resSys.GetString("SYS_USERIP"));

        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                    {
        ////////                        bolSaveCheck = false;
        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES Release요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        //// 조회 Method 호출
        ////////                        //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        return;
        ////////                    }
        ////////                }
        ////////                #endregion

        ////////                // TrackOut 처리
        ////////                #region TrackOut
        ////////                // 설비테이블에서 AREACODE 조회
        ////////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        ////////                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
        ////////                brwChannel.mfCredentials(clsEquip);

        ////////                //DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));
        ////////                DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));

        ////////                if (dtEquip.Rows.Count > 0)
        ////////                {
        ////////                    // Sampling 갯수(외관검사 검사수량)
        ////////                    int intSampleCount = 0;
        ////////                    for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
        ////////                    {
        ////////                        if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010001"))
        ////////                        {
        ////////                            intSampleCount = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value);
        ////////                            break;
        ////////                        }
        ////////                    }

        ////////                    // 데이터 테이블 설정
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                    brwChannel.mfCredentials(clsLot);

        ////////                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackOUT();
        ////////                    DataRow drRow = dtStdInfo.NewRow();
        ////////                    drRow["ReqNo"] = strReqNo;
        ////////                    drRow["ReqSeq"] = strReqSeq;
        ////////                    drRow["ReqLotSeq"] = 1;
        ////////                    drRow["FormName"] = this.Name;
        ////////                    drRow["FACTORYID"] = this.uComboPlantCode.Value.ToString();          // 공장코드
        ////////                    drRow["USERID"] = this.uTextInspectUserID.Text;                      // 사용자ID
        ////////                    drRow["LOTID"] = this.uTextLotNo.Text.Trim();                        // Lot 번호
        ////////                    //drRow["EQPID"] = this.uTextEquipCode.Text;                                      // 설비코드
        ////////                    drRow["EQPID"] = this.uComboEquip.Value.ToString();                                      // 설비코드
        ////////                    drRow["RECIPEID"] = "-";                                            // RecipeID 사용하지 않으면'-'
        ////////                    drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();           // AreaID
        ////////                    drRow["SPLITFLAG"] = "N";                                               // SPLIT 여부 N 고정
        ////////                    if (this.uCheckLossCheckFlag.Checked)
        ////////                        drRow["SCRAPFLAG"] = "Y";                                           // SCRAP 여부
        ////////                    else
        ////////                        drRow["SCRAPFLAG"] = "N";                                           // SCRAP 여부
        ////////                    drRow["REPAIRFLAG"] = "N";                                              // REPAIR 여부
        ////////                    drRow["RWFLAG"] = "N";                                                  // REWORK 여부
        ////////                    drRow["CONSUMEFLAG"] = "N";                                             // 자재소모여부 'N' 고정
        ////////                    drRow["COMMENT"] = "";                                                  // Comment
        ////////                    drRow["QCFLAG"] = "Y";                                                  // QCFlag 'Y' 고정
        ////////                    drRow["SAMPLECOUNT"] = intSampleCount.ToString();                       // Sampling 갯수
        ////////                    drRow["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();         // 판정결과
        ////////                    ////// DataTable 1:n
        ////////                    //////drRow["REASONQTY"] = "";                                             // 불량개수
        ////////                    //////drRow["REASONCODE"] = "";                                             // 불량코드
        ////////                    //////drRow["CAUSEEQPID"] = "";                                             // 불량원인설비
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    // Scrap 데이터 테이블(SCRAPCODE, SCRAPQTY)
        ////////                    DataTable dtScrapList = new DataTable();
        ////////                    dtScrapList.Columns.Add("SCRAPCODE", typeof(string));
        ////////                    dtScrapList.Columns.Add("SCRAPQTY", typeof(Int32));

        ////////                    DataRow drRow1 = dtScrapList.NewRow();
        ////////                    drRow1["SCRAPCODE"] = "610";
        ////////                    drRow1["SCRAPQTY"] = Convert.ToInt32(this.uNumLossQty.Value);
        ////////                    dtScrapList.Rows.Add(drRow1);

        ////////                    // 해당공정 불량정보 가져오기
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
        ////////                    QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
        ////////                    brwChannel.mfCredentials(clsFault);

        ////////                    DataTable dtFaultList = clsFault.mfReadINSProcInspectResultFault_MESTrackOut(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, 1, "II02010001");

        ////////                    // TrackOUT 처리 메소드 호출
        ////////                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESTrackOUT(dtStdInfo, dtScrapList, dtFaultList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

        ////////                    // 결과검사
        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                    {
        ////////                        bolSaveCheck = false;
        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        ////////                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES TrackOut요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요."
        ////////                                                        , Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        ////////                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////////                                                        , "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage
        ////////                                                        , Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        ////////                        ////                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////////                        ////                                , "처리결과", "MES처리결과"
        ////////                        ////                                , "입력한 정보를 저장하고 MES TrackOut 요청했습니다."
        ////////                        ////                                , Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                }
        ////////                else
        ////////                {
        ////////                    bolSaveCheck = false;
        ////////                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////////                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                                "확인창", "설비정보조회 결과", "MES I/F에 필요한 설비정보를 조회할 수 없습니다",
        ////////                                                Infragistics.Win.HAlign.Right);
        ////////                }
        ////////                #endregion
        ////////            }
        ////////            #endregion

        ////////            #region 불합격
        ////////            else if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))       // 불합격인 경우
        ////////            {
        ////////                // Hold 상태 아닐때만 Lot Hold 요청
        ////////                if (this.uTextProcessHoldState.Text.Equals("NotOnHold"))
        ////////                {
        ////////                    // Lot Hold 요청
        ////////                    DataTable dtLotList = new DataTable();
        ////////                    dtLotList.Columns.Add("LOTID", typeof(string));
        ////////                    DataRow drRow = dtLotList.NewRow();
        ////////                    drRow["LOTID"] = this.uTextLotNo.Text;
        ////////                    dtLotList.Rows.Add(drRow);

        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                    brwChannel.mfCredentials(clsLot);

        ////////                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                    drRow = dtStdInfo.NewRow();
        ////////                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                    drRow["ReqNo"] = strReqNo;
        ////////                    drRow["ReqSeq"] = strReqSeq;
        ////////                    drRow["ReqLotSeq"] = 1;
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    // HoldCode 가져오는 메소드 호출
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
        ////////                    QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
        ////////                    brwChannel.mfCredentials(clsReason);
        ////////                    DataTable dtReason = new DataTable();
        ////////                    // QCN 불량인경우 QCN Hold 코드로 Hold 요청한다
        ////////                    if (dtCheck.Rows.Count > 0)
        ////////                    {
        ////////                        dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), "frmINSZ0010");
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);
        ////////                    }

        ////////                    string strHoldCode = string.Empty;
        ////////                    if (dtReason.Rows.Count > 0)
        ////////                        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();
        ////////                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESHold(this.Name,
        ////////                                                            this.uTextInspectUserID.Text,
        ////////                                                            this.uTextEtcDesc.Text,     // Comment
        ////////                                                            strHoldCode,     // HoldCode
        ////////                                                            dtLotList,
        ////////                                                            m_resSys.GetString("SYS_USERIP"),
        ////////                                                            dtStdInfo);
        ////////                    // 결과검사
        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                    {
        ////////                        bolSaveCheck = false;
        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES Lot Hold요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        //// 조회 Method 호출
        ////////                        //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        return;
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        //////// 조회 Method 호출
        ////////                        //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                        ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Hold 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                }
        ////////            }
        ////////            #endregion
        ////////        }
        ////////        #endregion

        ////////        #region SPCN
        ////////        // SPCN
        ////////        if (!this.uTextMESSPCNTFlag.Text.Equals("T"))
        ////////        {
        ////////            for (int i = 0; i < this.uGridItem.Rows.Count; i++)
        ////////            {
        ////////                if (Convert.ToInt32(this.uGridItem.Rows[i].Cells["OCPCount"].Value) > 0)
        ////////                {
        ////////                    this.uTextMESSPCNTFlag.Text = "F";
        ////////                    // MES I/F 처리
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                    brwChannel.mfCredentials(clsLot);

        ////////                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                    DataRow drRow = dtStdInfo.NewRow();
        ////////                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                    drRow["ReqNo"] = strReqNo;
        ////////                    drRow["ReqSeq"] = strReqSeq;
        ////////                    drRow["ReqLotSeq"] = 1;
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    //strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESSPCN(this.Name, m_resSys.GetString("SYS_USERID"), this.uTextEquipCode.Text, m_resSys.GetString("SYS_USERIP"), dtStdInfo);
        ////////                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESSPCN(this.Name, this.uTextInspectUserID.Text, this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_USERIP"), dtStdInfo);

        ////////                    // 결과검사
        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

        ////////                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                    {
        ////////                        bolSaveCheck = false;
        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES SPCN ALARM 요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                        ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES SPCN ALARM 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                    break;
        ////////                }
        ////////                else
        ////////                {
        ////////                    this.uTextMESSPCNTFlag.Text = "T";
        ////////                }
        ////////            }
        ////////        }
        ////////        #endregion

        ////////        #region QCN 자동이동
        ////////        if (dtCheck.Rows.Count > 0)
        ////////        {
        ////////            #region Hold 이면 Release
        ////////            // Hold 상태 확인
        ////////            if (this.uTextProcessHoldState.Text.Equals("OnHold"))     // Hold 인경우
        ////////            {
        ////////                // Realease 코드 전송
        ////////                DataTable dtLotInfo = new DataTable();
        ////////                dtLotInfo.Columns.Add("LOTID", typeof(string));
        ////////                dtLotInfo.Columns.Add("HOLDCODE", typeof(string));

        ////////                // HoldCode 가져오는 메소드 호출
        ////////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
        ////////                QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
        ////////                brwChannel.mfCredentials(clsReason);

        ////////                DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

        ////////                string strHoldCode = string.Empty;
        ////////                if (dtReason.Rows.Count > 0)
        ////////                    strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

        ////////                // dtLotInfo 테이블 설정
        ////////                DataRow drLot = dtLotInfo.NewRow();
        ////////                drLot["LOTID"] = this.uTextLotNo.Text.Trim();
        ////////                drLot["HOLDCODE"] = strHoldCode;
        ////////                dtLotInfo.Rows.Add(drLot);

        ////////                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                brwChannel.mfCredentials(clsLot);

        ////////                DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                DataRow drRow = dtStdInfo.NewRow();
        ////////                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                drRow["ReqNo"] = strReqNo;
        ////////                drRow["ReqSeq"] = strReqSeq;
        ////////                drRow["ReqLotSeq"] = 1;
        ////////                drRow["FormName"] = this.Name;
        ////////                dtStdInfo.Rows.Add(drRow);

        ////////                // Release 메소드 호출
        ////////                strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESRelease(dtStdInfo
        ////////                                                                        , dtLotInfo
        ////////                                                                        , this.uTextInspectUserID.Text
        ////////                                                                        , m_resSys.GetString("SYS_USERID")
        ////////                                                                        , m_resSys.GetString("SYS_USERIP"));

        ////////                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                {
        ////////                    bolSaveCheck = false;
        ////////                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                    {
        ////////                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                    "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES Release요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                   "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                    //// 조회 Method 호출
        ////////                    //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    //return;
        ////////                }
        ////////                else
        ////////                {
        ////////                    //////// 조회 Method 호출
        ////////                    //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                    ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Release 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
        ////////                }
        ////////            }
        ////////            #endregion

        ////////            #region 화면이동
        ////////            // QCN 대상이면 QCN 화면으로 이동
        ////////            frmINSZ0010 frmINS = new frmINSZ0010();
        ////////            frmINS.PlantCode = dtCheck.Rows[0]["PlantCode"].ToString();
        ////////            frmINS.ReqNo = dtCheck.Rows[0]["ReqNo"].ToString();
        ////////            frmINS.ReqSeq = dtCheck.Rows[0]["ReqSeq"].ToString();
        ////////            frmINS.LotNo = this.uTextLotNo.Text;
        ////////            frmINS.ReqLotSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqLotSeq"]);
        ////////            frmINS.ReqItemSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqItemSeq"]);
        ////////            frmINS.ReqInspectSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqInspectSeq"]);
        ////////            frmINS.FormName = this.Name;

        ////////            CommonControl cControl = new CommonControl();
        ////////            Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
        ////////            Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
        ////////            if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINSZ0010"))
        ////////            {
        ////////                uTabMenu.Tabs["QRPINS" + "," + "frmINSZ0010"].Close();
        ////////            }
        ////////            //탭에 추가
        ////////            uTabMenu.Tabs.Add("QRPINS" + "," + "frmINSZ0010", "QCN 발행");
        ////////            uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINSZ0010"];

        ////////            //호출시 화면 속성 설정
        ////////            frmINS.AutoScroll = true;
        ////////            frmINS.MdiParent = this.MdiParent;
        ////////            frmINS.ControlBox = false;
        ////////            frmINS.Dock = DockStyle.Fill;
        ////////            frmINS.FormBorderStyle = FormBorderStyle.None;
        ////////            frmINS.WindowState = FormWindowState.Normal;
        ////////            frmINS.Text = "QCN 발행";
        ////////            frmINS.Show();
        ////////            #endregion
        ////////        }
        ////////        #endregion

        ////////        if (bolSaveCheck.Equals(true))
        ////////        {
        ////////            ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////            ////                       "저장결과", "MES I/F & 저장 결과", "입력한 정보를 저장하고 MES I/F 요청에 성공하였습니다.", Infragistics.Win.HAlign.Right);
        ////////            Clear();
        ////////        }

        ////////        #region 기존소스
        ////////        /* 기존소스 주석처리
        ////////            // TrackOut 이 진행되지 않은경우
        ////////            //if (!this.uTextMESTrackOutTFlag.Text.Equals("T") && (!this.uTextMESTrackInTCheck.Text.Equals("T") || !this.uTextMESTrackInTFlag.Text.Equals("F")))
        ////////            if (!this.uTextMESTrackOutTFlag.Text.Equals("T") && (this.uTextMESTrackInTCheck.Text.Equals("T") || this.uTextMESTrackInTFlag.Text.Equals("T")))
        ////////            {
        ////////                // 설비테이블에서 AREACODE 조회
        ////////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        ////////                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
        ////////                brwChannel.mfCredentials(clsEquip);

        ////////                DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

        ////////                if (dtEquip.Rows.Count > 0)
        ////////                {
        ////////                    // 데이터 테이블 설정
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                    brwChannel.mfCredentials(clsLot);

        ////////                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackOUT();
        ////////                    DataRow drRow = dtStdInfo.NewRow();
        ////////                    drRow["ReqNo"] = strReqNo;
        ////////                    drRow["ReqSeq"] = strReqSeq;
        ////////                    drRow["ReqLotSeq"] = 1;
        ////////                    drRow["FormName"] = this.Name;
        ////////                    drRow["FACTORYID"] = this.uComboPlantCode.Value.ToString();          // 공장코드
        ////////                    drRow["USERID"] = m_resSys.GetString("SYS_USERID");               // 사용자ID
        ////////                    drRow["LOTID"] = this.uTextLotNo.Text.Trim();                    // Lot 번호
        ////////                    drRow["EQPID"] = this.uTextEquipCode.Text;                                      // 설비코드
        ////////                    drRow["RECIPEID"] = "-";                                            // RecipeID 사용하지 않으면'-'
        ////////                    drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();         // AreaID
        ////////                    drRow["SPLITFLAG"] = "N";                                            // SPLIT 여부 N 고정
        ////////                    drRow["SCRAPFLAG"] = "N";                                            // SCRAP 여부
        ////////                    drRow["REPAIRFLAG"] = "N";                                            // REPAIR 여부
        ////////                    drRow["RWFLAG"] = "N";                                            // REWORK 여부
        ////////                    drRow["CONSUMEFLAG"] = "N";                                            // 자재소모여부 'N' 고정
        ////////                    drRow["COMMENT"] = "";                                             // Comment
        ////////                    drRow["QCFLAG"] = "Y";                                            // QCFlag 'Y' 고정
        ////////                    drRow["SAMPLECOUNT"] = "";                                          // Sampling 갯수
        ////////                    drRow["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();      // 판정결과
        ////////                    ////// DataTable 1:n
        ////////                    //////drRow["REASONQTY"] = "";                                             // 불량개수
        ////////                    //////drRow["REASONCODE"] = "";                                             // 불량코드
        ////////                    //////drRow["CAUSEEQPID"] = "";                                             // 불량원인설비
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    // Scrap 데이터 테이블(SCRAPCODE, SCRAPQTY)
        ////////                    DataTable dtScrapList = new DataTable();

        ////////                    // 해당공정 불량정보 가져오기

        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
        ////////                    QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
        ////////                    brwChannel.mfCredentials(clsFault);

        ////////                    DataTable dtFaultList = clsFault.mfReadINSProcInspectResultFault_MESTrackOut(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, 1, "II02010001");

        ////////                    // TrackOUT 처리 메소드 호출
        ////////                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESTrackOUT(dtStdInfo, dtScrapList, dtFaultList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

        ////////                    // 결과검사

        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                    if (ErrRtn.ErrNum.Equals(0) && !ErrRtn.InterfaceResultCode.Equals(string.Empty) && !ErrRtn.InterfaceResultCode.Equals(string.Empty))
        ////////                    {
        ////////                        bolSaveCheck = false;
        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES TrackOut요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);

        ////////                            // 조회 Method 호출
        ////////                            Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                            Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                            Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                            Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);

        ////////                            // 조회 Method 호출
        ////////                            Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                            Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                            Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                            Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        }
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        // 조회 Method 호출
        ////////                        Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    }
        ////////                }
        ////////                else
        ////////                {
        ////////                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////////                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                                "확인창", "설비정보조회 결과", "MES I/F에 필요한 설비정보를 조회할 수 없습니다",
        ////////                                                Infragistics.Win.HAlign.Right);

        ////////                    // 조회 Method 호출
        ////////                    Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                }
        ////////            }
        ////////            else if (!this.uTextMESTrackInTCheck.Text.Equals("T"))
        ////////            {
        ////////                this.uTextMESTrackOutTFlag.Text = "T";
        ////////            }

        ////////            // TrackOut이 처리되고 검사결과가 불합격인데 Hold요청이 T가 아닌인 경우
        ////////            if (this.uOptionPassFailFlag.CheckedIndex == 1 && !this.uTextMESHoldTFlag.Text.Equals("T")) //&& this.uTextMESTrackOutTFlag.Text.Equals("T"))
        ////////            {
        ////////                DataTable dtLotList = new DataTable();
        ////////                dtLotList.Columns.Add("LOTID", typeof(string));
        ////////                DataRow drRow = dtLotList.NewRow();
        ////////                drRow["LOTID"] = this.uTextLotNo.Text;
        ////////                dtLotList.Rows.Add(drRow);

        ////////                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                brwChannel.mfCredentials(clsLot);

        ////////                DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                drRow = dtStdInfo.NewRow();
        ////////                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                drRow["ReqNo"] = strReqNo;
        ////////                drRow["ReqSeq"] = strReqSeq;
        ////////                drRow["ReqLotSeq"] = 1;
        ////////                dtStdInfo.Rows.Add(drRow);

        ////////                // HoldCode 가져오는 메소드 호출
        ////////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
        ////////                QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
        ////////                brwChannel.mfCredentials(clsReason);

        ////////                DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

        ////////                string strHoldCode = string.Empty;
        ////////                if (dtReason.Rows.Count > 0)
        ////////                    strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();
        ////////                strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESHold(this.Name,
        ////////                                                        m_resSys.GetString("SYS_USERID"),
        ////////                                                        "",     // Comment
        ////////                                                        strHoldCode,     // HoldCode
        ////////                                                        dtLotList,
        ////////                                                        m_resSys.GetString("SYS_USERIP"),
        ////////                                                        dtStdInfo);
        ////////                // 결과검사

        ////////                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                if (ErrRtn.ErrNum.Equals(0) && !ErrRtn.InterfaceResultCode.Equals(string.Empty) && !ErrRtn.InterfaceResultCode.Equals(string.Empty))
        ////////                {
        ////////                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                    {
        ////////                        bolSaveCheck = false;
        ////////                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                    "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES Lot Hold요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);

        ////////                        // 조회 Method 호출
        ////////                        Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                   "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);

        ////////                        // 조회 Method 호출
        ////////                        Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    }
        ////////                }
        ////////                else
        ////////                {
        ////////                    // 조회 Method 호출
        ////////                    Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                }
        ////////            }
        ////////            else if (!this.uOptionPassFailFlag.CheckedIndex.Equals(1))
        ////////            {
        ////////                this.uTextMESHoldTFlag.Text = "T";
        ////////            }
        ////////            // TrackIn, TrackOut, Hold 요청이 모두 끝난후 검사결과가 관리한계선을 벗어난 경우
        ////////            //if (this.uTextMESHoldTFlag.Text.Equals("T") && this.uTextMESSPCNTFlag.Text.Equals("F") &&
        ////////            //    this.uTextMESTrackInTFlag.Text.Equals("T") && this.uTextMESTrackOutTFlag.Text.Equals("T"))
        ////////            //{
        ////////            if (!this.uTextMESSPCNTFlag.Text.Equals("T"))
        ////////            {
        ////////                for (int i = 0; i < this.uGridItem.Rows.Count; i++)
        ////////                {
        ////////                    if (Convert.ToInt32(this.uGridItem.Rows[i].Cells["OCPCount"].Value) > 0)
        ////////                    {
        ////////                        this.uTextMESSPCNTFlag.Text = "F";
        ////////                        // MES I/F 처리
        ////////                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////                        QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////                        brwChannel.mfCredentials(clsLot);

        ////////                        DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
        ////////                        DataRow drRow = dtStdInfo.NewRow();
        ////////                        drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
        ////////                        drRow["ReqNo"] = strReqNo;
        ////////                        drRow["ReqSeq"] = strReqSeq;
        ////////                        drRow["ReqLotSeq"] = 1;
        ////////                        dtStdInfo.Rows.Add(drRow);

        ////////                        strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESSPCN(this.Name, m_resSys.GetString("SYS_USERID"), this.uTextEquipCode.Text, m_resSys.GetString("SYS_USERIP"), dtStdInfo);

        ////////                        // 결과검사

        ////////                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                        break;
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        this.uTextMESSPCNTFlag.Text = "T";
        ////////                    }
        ////////                }
        ////////                if (ErrRtn.ErrNum.Equals(0) && !ErrRtn.InterfaceResultCode.Equals(string.Empty) && !ErrRtn.InterfaceResultCode.Equals(string.Empty))
        ////////                {
        ////////                    bolSaveCheck = false;
        ////////                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                    {
        ////////                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                    "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES SPCN ALARM 요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);

        ////////                        // 조회 Method 호출
        ////////                        Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                   "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);

        ////////                        // 조회 Method 호출
        ////////                        Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                        Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                    }
        ////////                }
        ////////                else
        ////////                {
        ////////                    // 조회 Method 호출
        ////////                    Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
        ////////                }
        ////////            }
        ////////            if (bolSaveCheck)
        ////////            {
        ////////                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////////                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장하고, MES I/F 요청을 성공하였습니다.",
        ////////                                            Infragistics.Win.HAlign.Right);

        ////////                Clear();
        ////////            }
        ////////            */
        ////////        #endregion
        ////////    }
        ////////    catch (Exception ex)
        ////////    {
        ////////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////////        frmErr.ShowDialog();
        ////////    }
        ////////    finally
        ////////    {
        ////////    }
        ////////}
        #endregion

        #region MES I/F
        /// <summary>
        /// MES I/F 저장처리부분
        /// </summary>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        private void SaveMESInterFace(string strReqNo, string strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                QRPBrowser brwChannel = new QRPBrowser();
                string strErrRtn = string.Empty;
                TransErrRtn ErrRtn = new TransErrRtn();

                string strPlantCode = this.uComboPlantCode.Value.ToString();
                bool bolSaveCheck = true;

                // QCN 불량인지 QC 불량인지 판단  
                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                QRPINS.BL.INSPRC.ProcInspectResultFault clsFault1 = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                brwChannel.mfCredentials(clsFault1);

                DataTable dtCheck = clsFault1.mfReadINSProcInspectResultFault_MoveQCN(strPlantCode, strReqNo, strReqSeq);
                clsFault1.Dispose();

                //PRODUCTOPERATIONTYPE 정보 조회 QCN,ITR정보가 X - > QCN 불량 ASSEMBLY인경우 QCN 발행, TEST일경우 ITR발행 판단
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsPrc = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsPrc);

                DataTable dtProc = clsPrc.mfReadMASProcess_Detail(strPlantCode, this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));
                clsPrc.Dispose();

                string strProcOperationType = "";
                if (dtProc.Rows.Count > 0)
                    strProcOperationType = dtProc.Rows[0]["PRODUCTOPERATIONTYPE"].ToString();

                #region TrackIn 공정이 아닌경우
                // TrackIn 공정인지 아닌지 구분
                if (this.uTextMESTrackInTCheck.Text.Equals("F"))
                {
                    // TrackIn 공정이 아닌경우
                    // 검사결과 확인
                    if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))        // 불합격인 경우
                    {
                        // Hold 상태 확인
                        if (this.uTextProcessHoldState.Text.Equals("NotOnHold"))     // Hold가 아닌경우
                        {
                            // LotHold 요청
                            DataTable dtLotList = new DataTable();
                            dtLotList.Columns.Add("LOTID", typeof(string));
                            DataRow drRow = dtLotList.NewRow();
                            drRow["LOTID"] = this.uTextLotNo.Text;
                            dtLotList.Rows.Add(drRow);

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                            drRow = dtStdInfo.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            dtStdInfo.Rows.Add(drRow);

                            // HoldCode 가져오는 메소드 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                            QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                            brwChannel.mfCredentials(clsReason);
                            DataTable dtReason = new DataTable();
                            string strHoldCode = string.Empty;

                            // QCN 불량인경우 QCN Hold 코드로 Hold 요청한다. 
                            if (dtCheck.Rows.Count > 0)
                            {

                                //2012-08-21 QCN,ITR구분 추가
                                if (this.uTextQCNorITRCheck.Text.ToUpper().Equals("QCN"))
                                    dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), "frmINSZ0010_S");
                                else if (this.uTextQCNorITRCheck.Text.ToUpper().Equals("ITR"))
                                    strHoldCode = "H003";
                                else
                                {
                                    if (strProcOperationType.ToUpper().Equals("ASSEMBLY"))
                                        dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), "frmINSZ0010_S");
                                    else if (strProcOperationType.ToUpper().Equals("TEST"))
                                        strHoldCode = "H003";
                                }
                            }
                            else
                                dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

                            clsReason.Dispose();

                            if (dtReason.Rows.Count > 0)
                                strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESHold(this.Name,
                                                                    this.uTextInspectUserID.Text,
                                                                    this.uTextEtcDesc.Text,     // Comment
                                                                    strHoldCode,     // HoldCode
                                                                    dtLotList,
                                                                    m_resSys.GetString("SYS_USERIP"),
                                                                    dtStdInfo);
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                            {
                                bolSaveCheck = false;

                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000095", "M000946", Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                        , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                }
                                //// 조회 Method 호출
                                //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                return;
                            }
                            else
                            {
                                //////// 조회 Method 호출
                                //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Hold 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                    else if (this.uOptionPassFailFlag.CheckedIndex.Equals(0))     // 합격인 경우
                    {
                        // Hold 상태 확인
                        if (this.uTextProcessHoldState.Text.Equals("OnHold"))     // Hold 인경우
                        {
                            // Realease 코드 전송
                            DataTable dtLotInfo = new DataTable();
                            dtLotInfo.Columns.Add("LOTID", typeof(string));
                            dtLotInfo.Columns.Add("HOLDCODE", typeof(string));

                            // HoldCode 가져오는 메소드 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                            QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                            brwChannel.mfCredentials(clsReason);

                            DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

                            string strHoldCode = string.Empty;
                            if (dtReason.Rows.Count > 0)
                                strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                            // dtLotInfo 테이블 설정
                            DataRow drLot = dtLotInfo.NewRow();
                            drLot["LOTID"] = this.uTextLotNo.Text.Trim();
                            drLot["HOLDCODE"] = strHoldCode;
                            dtLotInfo.Rows.Add(drLot);

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                            DataRow drRow = dtStdInfo.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            drRow["FormName"] = this.Name;
                            dtStdInfo.Rows.Add(drRow);

                            // Release 메소드 호출
                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESRelease(dtStdInfo
                                                                                    , dtLotInfo
                                                                                    , this.uTextInspectUserID.Text
                                                                                    , m_resSys.GetString("SYS_USERID")
                                                                                    , m_resSys.GetString("SYS_USERIP"));

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                            {
                                bolSaveCheck = false;
                                if (!ErrRtn.InterfaceResultCode.Equals("UnexpectedError"))
                                {
                                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000095", "M000948", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                    }
                                    //// 조회 Method 호출
                                    //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                }
                                return;
                            }
                            else
                            {
                                //////// 조회 Method 호출
                                //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Release 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                #endregion

                #region TrackIn 공정인경우
                else if (this.uTextMESTrackInTCheck.Text.Equals("T"))
                {
                    // TrackIn 공정인 경우
                    // 검사결과 확인
                    if (this.uOptionPassFailFlag.CheckedIndex.Equals(0))     // 합격인 경우
                    {
                        // Hold 상태 확인
                        if (this.uTextProcessHoldState.Text.Equals("OnHold"))     // Hold 인경우
                        {
                            // Realease 코드 전송
                            DataTable dtLotInfo = new DataTable();
                            dtLotInfo.Columns.Add("LOTID", typeof(string));
                            dtLotInfo.Columns.Add("HOLDCODE", typeof(string));

                            // HoldCode 가져오는 메소드 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                            QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                            brwChannel.mfCredentials(clsReason);

                            DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

                            string strHoldCode = string.Empty;
                            if (dtReason.Rows.Count > 0)
                                strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                            // dtLotInfo 테이블 설정
                            DataRow drLot = dtLotInfo.NewRow();
                            drLot["LOTID"] = this.uTextLotNo.Text.Trim();
                            drLot["HOLDCODE"] = strHoldCode;
                            dtLotInfo.Rows.Add(drLot);

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                            DataRow drRow = dtStdInfo.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            drRow["FormName"] = this.Name;
                            dtStdInfo.Rows.Add(drRow);

                            // Release 메소드 호출
                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESRelease(dtStdInfo
                                                                                    , dtLotInfo
                                                                                    , this.uTextInspectUserID.Text
                                                                                    , m_resSys.GetString("SYS_USERID")
                                                                                    , m_resSys.GetString("SYS_USERIP"));

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                            {
                                bolSaveCheck = false;
                                if (!ErrRtn.InterfaceResultCode.Equals("UnexpectedError"))
                                {
                                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000095", "M000948", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                    }
                                }
                                //// 조회 Method 호출
                                //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                return;
                            }
                        }
                        // TrackOut 처리
                        // 설비테이블에서 AREACODE 조회
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                        brwChannel.mfCredentials(clsEquip);

                        //DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));
                        DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                        if (dtEquip.Rows.Count > 0)
                        {
                            #region Body
                            // 데이터 테이블 설정
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackOUT();
                            DataRow drRow = dtStdInfo.NewRow();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            drRow["FormName"] = this.Name;
                            drRow["FACTORYID"] = this.uComboPlantCode.Value.ToString();          // 공장코드
                            drRow["USERID"] = this.uTextInspectUserID.Text;                      // 사용자ID
                            drRow["LOTID"] = this.uTextLotNo.Text.Trim();                        // Lot 번호
                            //drRow["EQPID"] = this.uTextEquipCode.Text;                                      // 설비코드
                            drRow["EQPID"] = this.uComboEquip.Value.ToString();                                      // 설비코드
                            drRow["RECIPEID"] = "-";                                            // RecipeID 사용하지 않으면'-'
                            drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();           // AreaID
                            drRow["SPLITFLAG"] = "N";                                               // SPLIT 여부 N 고정
                            //if (this.uCheckLossCheckFlag.Checked)
                            //    drRow["SCRAPFLAG"] = "Y";                                           // SCRAP 여부
                            //else
                            //    drRow["SCRAPFLAG"] = "N";                                           // SCRAP 여부
                            drRow["SCRAPFLAG"] = "N";                                               // 2012-11-20 불량발생시 SCRAP처리안함.
                            drRow["REPAIRFLAG"] = "N";                                              // REPAIR 여부
                            drRow["RWFLAG"] = "N";                                                  // REWORK 여부
                            drRow["CONSUMEFLAG"] = "N";                                             // 자재소모여부 'N' 고정
                            drRow["COMMENT"] = this.uTextEtcDesc.Text;                              // Comment
                            drRow["QCFLAG"] = "Y";                                                  // QCFlag 'Y' 고정
                            dtStdInfo.Rows.Add(drRow);
                            #endregion

                            #region ScrapList
                            // Scrap 데이터 테이블(SCRAPCODE, SCRAPQTY)
                            DataTable dtScrapList = new DataTable();
                            dtScrapList.Columns.Add("SCRAPCODE", typeof(string));
                            dtScrapList.Columns.Add("SCRAPQTY", typeof(Int32));

                            DataRow drRow1 = dtScrapList.NewRow();
                            drRow1["SCRAPCODE"] = "610";
                            drRow1["SCRAPQTY"] = Convert.ToInt32(this.uNumLossQty.Value);
                            dtScrapList.Rows.Add(drRow1);
                            #endregion

                            #region InspectItemList
                            // 검사항목 데이터 테이블
                            // Sampling 갯수(외관검사 검사수량)
                            DataTable dtInspectItemList = new DataTable();
                            dtInspectItemList.Columns.Add("INSPECTID", typeof(string));
                            dtInspectItemList.Columns.Add("SAMPLECOUNT", typeof(string));
                            dtInspectItemList.Columns.Add("QUALEFLAG", typeof(string));
                            for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                            {
                                // 검사 진행여부가 체크된 것만
                                if (Convert.ToBoolean(this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value))
                                {
                                    // 고객사가 삼성인경우 그리드에 검사항목 II02031902, II02031903, II02010010, II02010009 가 있으면 같이 보낸다
                                    if (this.uTextCustomerCode.Text.Equals("SEC"))
                                    {
                                        if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010001"))
                                        {
                                            drRow1 = dtInspectItemList.NewRow();
                                            drRow1["INSPECTID"] = "PI_VISUAL";
                                            drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                            //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                            drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                            dtInspectItemList.Rows.Add(drRow1);
                                        }
                                        else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02031902"))
                                        {
                                            drRow1 = dtInspectItemList.NewRow();
                                            drRow1["INSPECTID"] = "AE";
                                            drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                            //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                            drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                            dtInspectItemList.Rows.Add(drRow1);
                                        }
                                        else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02031903"))
                                        {
                                            drRow1 = dtInspectItemList.NewRow();
                                            drRow1["INSPECTID"] = "AO";
                                            drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                            //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                            drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                            dtInspectItemList.Rows.Add(drRow1);
                                        }
                                        else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010010"))
                                        {
                                            drRow1 = dtInspectItemList.NewRow();
                                            drRow1["INSPECTID"] = "AV";
                                            drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                            //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                            drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                            dtInspectItemList.Rows.Add(drRow1);
                                        }
                                        else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010009"))
                                        {
                                            drRow1 = dtInspectItemList.NewRow();
                                            drRow1["INSPECTID"] = "AP";
                                            drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                            //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                            drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                            dtInspectItemList.Rows.Add(drRow1);
                                        }
                                    }
                                    else
                                    {
                                        // 고객사가 삼성이 아닌경우 외관검사항목만 보낸다
                                        if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010001"))
                                        {
                                            //intSampleCount = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value);
                                            drRow1 = dtInspectItemList.NewRow();
                                            drRow1["INSPECTID"] = "PI_VISUAL";
                                            drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                            //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                            drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                            dtInspectItemList.Rows.Add(drRow1);
                                            break;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region ReasonList
                            // 불량리스트 데이터 테이블
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                            QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                            brwChannel.mfCredentials(clsFault);

                            DataTable dtFaultList = clsFault.mfReadINSProcInspectResultFault_MESTrackOut(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, 1, "II02010001");
                            #endregion

                            // TrackOUT 처리 메소드 호출
                            //strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESTrackOUT(dtStdInfo, dtScrapList, dtInspectItemList, dtFaultList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESTrackOUT(dtStdInfo, dtScrapList, dtInspectItemList, dtFaultList, "Y",m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                            {
                                bolSaveCheck = false;
                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    //입력한 정보를 저장하였으나 MES TrackOut요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "M001135", "M000095", "M000950"
                                                                , Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                                                , ErrRtn.InterfaceResultMessage
                                                                , Infragistics.Win.HAlign.Right);
                                }
                            }
                            else
                            {
                                ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                ////                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                ////                                , "처리결과", "MES처리결과"
                                ////                                , "입력한 정보를 저장하고 MES TrackOut 요청했습니다."
                                ////                                , Infragistics.Win.HAlign.Right);
                            }
                        }
                        else
                        {
                            //MES I/F에 필요한 설비정보를 조회할 수 없습니다
                            bolSaveCheck = false;
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000717", "M000090",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                    else if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))       // 불합격인 경우
                    {
                        // Hold 상태 아닐때만 Lot Hold 요청
                        if (this.uTextProcessHoldState.Text.Equals("NotOnHold"))
                        {
                            // Lot Hold 요청
                            DataTable dtLotList = new DataTable();
                            dtLotList.Columns.Add("LOTID", typeof(string));
                            DataRow drRow = dtLotList.NewRow();
                            drRow["LOTID"] = this.uTextLotNo.Text;
                            dtLotList.Rows.Add(drRow);

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                            drRow = dtStdInfo.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            dtStdInfo.Rows.Add(drRow);

                            // HoldCode 가져오는 메소드 호출
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                            QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                            brwChannel.mfCredentials(clsReason);
                            DataTable dtReason = new DataTable();
                            string strHoldCode = string.Empty;
                            // QCN 불량인경우 QCN Hold 코드로 Hold 요청한다
                            if (dtCheck.Rows.Count > 0)
                            {  
                                //2012-08-21 QCN,ITR구분 추가
                                if (this.uTextQCNorITRCheck.Text.ToUpper().Equals("QCN"))
                                    dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), "frmINSZ0010_S");
                                else if (this.uTextQCNorITRCheck.Text.ToUpper().Equals("ITR"))
                                    strHoldCode = "H003";
                                else
                                {
                                    if( strProcOperationType.ToUpper().Equals("ASSEMBLY"))
                                        dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), "frmINSZ0010_S");
                                    else if(strProcOperationType.ToUpper().Equals("TEST"))
                                        strHoldCode = "H003";
                                }
                            }
                            else
                            {
                                dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);
                            }

                            if (dtReason.Rows.Count > 0)
                                strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESHold(this.Name,
                                                                    this.uTextInspectUserID.Text,
                                                                    this.uTextEtcDesc.Text,     // Comment
                                                                    strHoldCode,     // HoldCode
                                                                    dtLotList,
                                                                    m_resSys.GetString("SYS_USERIP"),
                                                                    dtStdInfo);
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                            {
                                bolSaveCheck = false;
                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    //입력한 정보를 저장하였으나 MES Lot Hold요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000095", "M000946", Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                        , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                }
                                //// 조회 Method 호출
                                //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                return;
                            }
                            else
                            {
                                //////// 조회 Method 호출
                                //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Hold 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                #endregion

                #region SPCN
                // SPCN
                if (!this.uTextMESSPCNTFlag.Text.Equals("T"))
                {
                    for (int i = 0; i < this.uGridItem.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(this.uGridItem.Rows[i].Cells["OCPCount"].Value) > 0 ||
                            Convert.ToInt32(this.uGridItem.Rows[i].Cells["UpperRunCount"].Value) > 0 ||
                            Convert.ToInt32(this.uGridItem.Rows[i].Cells["LowerRunCount"].Value) > 0 ||
                            Convert.ToInt32(this.uGridItem.Rows[i].Cells["UpperTrendCount"].Value) > 0 ||
                            Convert.ToInt32(this.uGridItem.Rows[i].Cells["LowerTrendCount"].Value) > 0)
                        {
                            //this.uTextMESSPCNTFlag.Text = "F";
                            // MES I/F 처리
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                            DataRow drRow = dtStdInfo.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            dtStdInfo.Rows.Add(drRow);

                            //strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESSPCN(this.Name, m_resSys.GetString("SYS_USERID"), this.uTextEquipCode.Text, m_resSys.GetString("SYS_USERIP"), dtStdInfo);
                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESSPCN(this.Name, this.uTextInspectUserID.Text, this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_USERIP"), dtStdInfo);

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                bolSaveCheck = false;
                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    //입력한 정보를 저장하였으나 MES SPCN ALARM 요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000095", "M000949", Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                        , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                }
                            }
                            else
                            {
                                ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES SPCN ALARM 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
                            }
                            break;
                        }
                        else
                        {
                            //this.uTextMESSPCNTFlag.Text = "T";
                        }
                    }
                }
                #endregion

                #region WAFER 수입검사여부

                //if (this.uTextPCB_Layout.Text.Equals("Y") && this.uOptionPassFailFlag.CheckedIndex.Equals(0)) -- 주석처리 2012-09-03
                //{
                //    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                //    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                //    brwChannel.mfCredentials(clsLot);

                //    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_LOT_WAFERBG4QC_REQ(this.Name, this.uComboPlantCode.Value.ToString()
                //                                                                , strReqNo, strReqSeq, 1, this.uTextInspectUserID.Text
                //                                                                , this.uTextLotNo.Text.ToUpper().Trim()
                //                                                                , this.uComboWafer.Value.ToString()
                //                                                                , this.uTextEtcDesc.Text
                //                                                                , m_resSys.GetString("SYS_USERID")
                //                                                                , m_resSys.GetString("SYS_USERIP"));

                //    if (!ErrRtn.ErrNum.Equals(0) || ErrRtn.InterfaceResultCode.Equals("0"))
                //    {
                //        bolSaveCheck = false;
                //        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                //        {
                //            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                        "M001135", "M000095", "M001479", Infragistics.Win.HAlign.Right);
                //        }
                //        else
                //        {
                //            string strLang = m_resSys.GetString("SYS_LANG");
                //            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                //                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                //                , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                //        }
                //    }
                //}
                //Wafer 수입검사(작업공정 = A1000 && Wafer검사여부 = "Y" && 합격)  2012-09-03
                if (this.uTextPCB_Layout.Text.Equals("Y")
                    && this.uComboWorkProcess.Value != null
                    && this.uComboWorkProcess.Value.ToString().Equals("A1000")
                    && this.uOptionPassFailFlag.CheckedIndex == 0)   
                {
                    // TrackOut 처리
                    // 설비테이블에서 AREACODE 조회
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    if (dtEquip.Rows.Count > 0)
                    {
                        #region Body
                        // 데이터 테이블 설정
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                        QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackOUT();
                        DataRow drRow = dtStdInfo.NewRow();
                        drRow["ReqNo"] = strReqNo;
                        drRow["ReqSeq"] = strReqSeq;
                        drRow["ReqLotSeq"] = 1;
                        drRow["FormName"] = this.Name;
                        drRow["FACTORYID"] = this.uComboPlantCode.Value.ToString();          // 공장코드
                        drRow["USERID"] = this.uTextInspectUserID.Text;                      // 사용자ID
                        drRow["LOTID"] = this.uTextLotNo.Text.Trim();                        // Lot 번호
                        //drRow["EQPID"] = this.uTextEquipCode.Text;                        // 설비코드
                        drRow["EQPID"] = this.uComboEquip.Value.ToString();                 // 설비코드
                        drRow["RECIPEID"] = "-";                                            // RecipeID 사용하지 않으면'-'
                        drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();           // AreaID
                        drRow["SPLITFLAG"] = "N";                                           // SPLIT 여부 N 고정
                        //if (this.uCheckLossCheckFlag.Checked)
                        //    drRow["SCRAPFLAG"] = "Y";                                       // SCRAP 여부
                        //else
                        //    drRow["SCRAPFLAG"] = "N";                                       // SCRAP 여부
                        drRow["SCRAPFLAG"] = "N";
                        drRow["REPAIRFLAG"] = "N";                                          // REPAIR 여부
                        drRow["RWFLAG"] = "N";                                              // REWORK 여부
                        drRow["CONSUMEFLAG"] = "N";                                         // 자재소모여부 'N' 고정
                        drRow["COMMENT"] = this.uTextEtcDesc.Text;                          // Comment
                        drRow["QCFLAG"] = "Y";                                              // QCFlag 'Y' 고정
                        dtStdInfo.Rows.Add(drRow);
                        #endregion

                        #region ScrapList
                        // Scrap 데이터 테이블(SCRAPCODE, SCRAPQTY)
                        DataTable dtScrapList = new DataTable();
                        dtScrapList.Columns.Add("SCRAPCODE", typeof(string));
                        dtScrapList.Columns.Add("SCRAPQTY", typeof(Int32));

                        DataRow drRow1 = dtScrapList.NewRow();
                        drRow1["SCRAPCODE"] = "610";
                        drRow1["SCRAPQTY"] = Convert.ToInt32(this.uNumLossQty.Value);
                        dtScrapList.Rows.Add(drRow1);
                        #endregion

                        #region InspectItemList
                        // 검사항목 데이터 테이블
                        // Sampling 갯수(외관검사 검사수량)
                        DataTable dtInspectItemList = new DataTable();
                        dtInspectItemList.Columns.Add("INSPECTID", typeof(string));
                        dtInspectItemList.Columns.Add("SAMPLECOUNT", typeof(string));
                        dtInspectItemList.Columns.Add("QUALEFLAG", typeof(string));
                        for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                        {
                            // 검사 진행여부가 체크된 것만
                            if (Convert.ToBoolean(this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value))
                            {
                                // 고객사가 삼성인경우 그리드에 검사항목 II02031902, II02031903, II02010010, II02010009 가 있으면 같이 보낸다
                                if (this.uTextCustomerCode.Text.Equals("SEC"))
                                {
                                    if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010001"))
                                    {
                                        drRow1 = dtInspectItemList.NewRow();
                                        drRow1["INSPECTID"] = "PI_VISUAL";
                                        drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                        //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                        drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                        dtInspectItemList.Rows.Add(drRow1);
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02031902"))
                                    {
                                        drRow1 = dtInspectItemList.NewRow();
                                        drRow1["INSPECTID"] = "AE";
                                        drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                        //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                        drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                        dtInspectItemList.Rows.Add(drRow1);
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02031903"))
                                    {
                                        drRow1 = dtInspectItemList.NewRow();
                                        drRow1["INSPECTID"] = "AO";
                                        drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                        //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                        drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                        dtInspectItemList.Rows.Add(drRow1);
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010010"))
                                    {
                                        drRow1 = dtInspectItemList.NewRow();
                                        drRow1["INSPECTID"] = "AV";
                                        drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                        //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                        drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                        dtInspectItemList.Rows.Add(drRow1);
                                    }
                                    else if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010009"))
                                    {
                                        drRow1 = dtInspectItemList.NewRow();
                                        drRow1["INSPECTID"] = "AP";
                                        drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                        //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                        drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                        dtInspectItemList.Rows.Add(drRow1);
                                    }
                                }
                                else
                                {
                                    // 고객사가 삼성이 아닌경우 외관검사항목만 보낸다
                                    if (this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II02010001"))
                                    {
                                        //intSampleCount = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value);
                                        drRow1 = dtInspectItemList.NewRow();
                                        drRow1["INSPECTID"] = "PI_VISUAL";
                                        drRow1["SAMPLECOUNT"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                                        //drRow1["QUALEFLAG"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value;
                                        drRow1["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();
                                        dtInspectItemList.Rows.Add(drRow1);
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        #region ReasonList
                        // 불량리스트 데이터 테이블
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                        QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                        brwChannel.mfCredentials(clsFault);

                        DataTable dtFaultList = clsFault.mfReadINSProcInspectResultFault_MESTrackOut(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, 1, "II02010001");
                        #endregion

                        // TrackOUT 처리 메소드 호출  TrackOut(TKOUTFLAG = "N")
                        strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESTrackOUT(dtStdInfo, dtScrapList, dtInspectItemList, dtFaultList, "N",m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                        {
                            bolSaveCheck = false;
                            if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            {
                                //입력한 정보를 저장하였으나 MES TrackOut요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000095", "M000950"
                                                            , Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                //MES I/F ErrorMessage
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                                            , ErrRtn.InterfaceResultMessage
                                                            , Infragistics.Win.HAlign.Right);
                            }
                        }
                        else
                        {
                            ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            ////                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////                                , "처리결과", "MES처리결과"
                            ////                                , "입력한 정보를 저장하고 MES TrackOut 요청했습니다."
                            ////                                , Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        //MES I/F에 필요한 설비정보를 조회할 수 없습니다
                        bolSaveCheck = false;
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000717", "M000090",
                                                    Infragistics.Win.HAlign.Right);
                    }
                }
                #endregion

                #region QCN 자동이동
                if (dtCheck.Rows.Count > 0)
                {
                    // Hold 상태 확인
                    if (this.uTextProcessHoldState.Text.Equals("OnHold"))     // Hold 인경우
                    {
                        // Realease 코드 전송
                        DataTable dtLotInfo = new DataTable();
                        dtLotInfo.Columns.Add("LOTID", typeof(string));
                        dtLotInfo.Columns.Add("HOLDCODE", typeof(string));

                        // HoldCode 가져오는 메소드 호출
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                        QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                        brwChannel.mfCredentials(clsReason);

                        DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(this.uComboPlantCode.Value.ToString(), this.Name);

                        string strHoldCode = string.Empty;
                        if (dtReason.Rows.Count > 0)
                            strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                        // dtLotInfo 테이블 설정
                        DataRow drLot = dtLotInfo.NewRow();
                        drLot["LOTID"] = this.uTextLotNo.Text.Trim();
                        drLot["HOLDCODE"] = strHoldCode;
                        dtLotInfo.Rows.Add(drLot);

                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                        QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                        DataRow drRow = dtStdInfo.NewRow();
                        drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                        drRow["ReqNo"] = strReqNo;
                        drRow["ReqSeq"] = strReqSeq;
                        drRow["ReqLotSeq"] = 1;
                        drRow["FormName"] = this.Name;
                        dtStdInfo.Rows.Add(drRow);

                        // Release 메소드 호출
                        strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESRelease(dtStdInfo
                                                                                , dtLotInfo
                                                                                , this.uTextInspectUserID.Text
                                                                                , m_resSys.GetString("SYS_USERID")
                                                                                , m_resSys.GetString("SYS_USERIP"));

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                        {
                            bolSaveCheck = false;
                            if (!ErrRtn.InterfaceResultCode.Equals("UnexpectedError"))
                            {
                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    //입력한 정보를 저장하였으나 MES Release요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000095", "M000948", Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                        , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                }
                            }
                            //// 조회 Method 호출
                            //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            //return;
                        }
                        else
                        {
                            //////// 조회 Method 호출
                            //////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            ////               "처리결과", "MES처리결과", "입력한 정보를 저장하고 MES Lot Release 요청 성공하였습니다.", Infragistics.Win.HAlign.Right);
                        }
                    }

                    // QCN 대상이면 QCN 화면으로 이동
                    frmINSZ0010_S frmINS = new frmINSZ0010_S();

                    //호출시 화면 속성 설정
                    frmINS.AutoScroll = true;
                    frmINS.MdiParent = this.MdiParent;
                    frmINS.ControlBox = false;
                    frmINS.Dock = DockStyle.Fill;
                    frmINS.FormBorderStyle = FormBorderStyle.None;
                    frmINS.WindowState = FormWindowState.Normal;
                    frmINS.Text = "QCN 발행";

                    frmINS.PlantCode = dtCheck.Rows[0]["PlantCode"].ToString();
                    frmINS.ReqNo = dtCheck.Rows[0]["ReqNo"].ToString();
                    frmINS.ReqSeq = dtCheck.Rows[0]["ReqSeq"].ToString();
                    frmINS.LotNo = this.uTextLotNo.Text;
                    frmINS.ReqLotSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqLotSeq"]);
                    frmINS.ReqItemSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqItemSeq"]);
                    frmINS.ReqInspectSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqInspectSeq"]);
                    frmINS.FormName = this.Name;

                    CommonControl cControl = new CommonControl();
                    Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                    Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                    if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINSZ0010_S"))
                    {
                        uTabMenu.Tabs["QRPINS" + "," + "frmINSZ0010_S"].Close();
                    }
                    //탭에 추가
                    uTabMenu.Tabs.Add("QRPINS" + "," + "frmINSZ0010_S", "QCN 발행");
                    uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINSZ0010_S"];

                    frmINS.Show();
                }
                #endregion

                // 모두 성공시 Clear
                if (bolSaveCheck.Equals(true))
                {
                    ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                       "저장결과", "MES I/F & 저장 결과", "입력한 정보를 저장하고 MES I/F 요청에 성공하였습니다.", Infragistics.Win.HAlign.Right);
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// LotInfo 요청후 처리 메소드
        /// </summary>
        private void Rtn_MES_LotInfo()
        {
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 초기화                                                        
                ProcessCode = string.Empty;
                this.uTextNowProcessCode.Clear();
                this.uTextNowProcessName.Clear();
                this.uTextWorkProcessCode.Clear();
                this.uTextProductCode.Clear();
                this.uTextProductName.Clear();
                EquipCode = string.Empty;
                this.uTextEquipCode.Clear();
                this.uTextEquipName.Clear();
                this.uTextLotSize.Clear();
                this.uTextPackage.Clear();
                this.uTextLotProcessState.Clear();
                this.uTextCustomerProductCode.Clear();
                this.uTextProcessHoldState.Clear();
                this.uTextWorkUserID.Clear();
                this.uTextOUTSOURCINGVENDOR.Clear();

                this.uTextPCB_Layout.Clear();
                this.uComboWafer.Value = string.Empty;
                this.uLabelWafer.Hide();
                this.uComboWafer.Hide();

                this.uLabelHandler.Hide();
                this.uLabelProgram.Hide();
                this.uComboHandler.Hide();
                this.uTextProgram.Hide();
                this.uComboHandler.Value = string.Empty;
                this.uTextProgram.Clear();

                while (this.uGridItem.Rows.Count > 0)
                {
                    this.uGridItem.Rows[0].Delete(false);
                }
                while (this.uGridSampling.Rows.Count > 0)
                {
                    this.uGridSampling.Rows[0].Delete(false);
                }
                while (this.uGridFault.Rows.Count > 0)
                {
                    this.uGridFault.Rows[0].Delete(false);
                }
                while (this.uGridFaultData.Rows.Count > 0)
                {
                    this.uGridFaultData.Rows[0].Delete(false);
                }

                //this.uComboStack.Items.Clear();
                this.uComboWorkProcess.EventManager.AllEventsEnabled = false;
                this.uComboWorkProcess.Value = "";
                this.uComboWorkProcess.EventManager.AllEventsEnabled = true;

                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                QRPINS.BL.INSPRC.ProcMESInterface clsMes = new QRPINS.BL.INSPRC.ProcMESInterface();
                brwChannel.mfCredentials(clsMes);

                string strLotNo = this.uTextLotNo.Text.ToUpper().Trim();
                DataTable dtLotInfo = clsMes.mfRead_LOT_INFO_REQ(this.uComboPlantCode.Value.ToString(), strLotNo);

                // 데이터가 있는경우
                if (dtLotInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 합부판정 기본값 합격
                    this.uOptionPassFailFlag.CheckedIndex = 0;

                    this.uTextLotNo.ReadOnly = true;
                    this.uTextLotNo.Appearance.BackColor = Color.Gainsboro;

                    ProcessCode = dtLotInfo.Rows[0]["OPERID"].ToString();
                    this.uTextNowProcessCode.Text = dtLotInfo.Rows[0]["OPERID"].ToString();
                    this.uTextNowProcessName.Text = dtLotInfo.Rows[0]["OPERDESC"].ToString();
                    this.uTextWorkProcessCode.Text = dtLotInfo.Rows[0]["WORKOPERID"].ToString();
                    this.uTextProductCode.Text = dtLotInfo.Rows[0]["PRODUCTSPECNAME"].ToString();
                    this.uTextProductName.Text = dtLotInfo.Rows[0]["PRODUCTSPECDESC"].ToString();
                    EquipCode = dtLotInfo.Rows[0]["EQPID"].ToString();
                    this.uTextEquipCode.Text = dtLotInfo.Rows[0]["EQPID"].ToString();
                    this.uTextEquipName.Text = dtLotInfo.Rows[0]["EQPDESC"].ToString();
                    this.uTextLotSize.Text = Convert.ToInt32(dtLotInfo.Rows[0]["QTY"].ToString()).ToString();
                    this.uTextPackage.Text = dtLotInfo.Rows[0]["PACKAGE"].ToString();
                    this.uTextLotProcessState.Text = dtLotInfo.Rows[0]["LOTPROCESSSTATE"].ToString();
                    this.uTextCustomerProductCode.Text = dtLotInfo.Rows[0]["CUSTOMPRODUCTSPECNAME"].ToString(); //고객사제품코드
                    this.uTextProcessHoldState.Text = dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString();
                    this.uTextWorkUserID.Text = dtLotInfo.Rows[0]["LASTEVENTUSER"].ToString();
                    //this.uTextOUTSOURCINGVENDOR.Text = dtLotInfo.Rows[0]["OUTSOURCINGVENDOR"].ToString();

                    

                    // 작업자명 설정
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);
                    DataTable dtUser = clsUser.mfReadSYSUser(this.uComboPlantCode.Value.ToString(), this.uTextWorkUserID.Text, m_resSys.GetString("SYS_LANG"));
                    if (dtUser.Rows.Count > 0 && dtUser.Columns.Contains("UserName"))
                        this.uTextWorkUserName.Text = dtUser.Rows[0]["UserName"].ToString();

                    // 고객정보 조회
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);
                    DataTable dtProductInfo = clsProduct.mfReadProductInfo_frmINS0008C_PSTS(this.uComboPlantCode.Value.ToString(), this.uTextProductCode.Text, m_resSys.GetString("SYS_LANG"));
                    if (dtProductInfo.Rows.Count > 0 &&
                        dtProductInfo.Columns.Contains("CustomerCode") &&
                        dtProductInfo.Columns.Contains("CustomerName"))
                    {
                        this.uTextCustomerCode.Text = dtProductInfo.Rows[0]["CustomerCode"].ToString();
                        this.uTextCustomerName.Text = dtProductInfo.Rows[0]["CustomerName"].ToString();
                        this.uTextPCB_Layout.Text = dtProductInfo.Rows[0]["PCB_LAYOUT"].ToString();
                        //2012-09-03 권종구
                        //if (this.uTextPCB_Layout.Text.Equals("Y"))
                        //{
                        //    this.uComboWafer.Show();
                        //    this.uLabelWafer.Show();
                        //}
                        //else
                        //{
                        //    this.uComboWafer.Hide();
                        //    this.uLabelWafer.Hide();
                        //}
                    }

                    // LotNo에 대한 양산품, 개발품 구분 확인
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLotProductionType = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                    brwChannel.mfCredentials(clsLotProductionType);
                    DataTable dtLotProductionType = clsLotProductionType.mfReadINSProcInspectReq_Check_MP(this.uComboPlantCode.Value.ToString(), this.uTextLotNo.Text);
                    string strProductionType = "";
                    if (dtLotProductionType.Rows.Count > 0)
                    {
                        strProductionType = dtLotProductionType.Rows[0]["ProductionType"].ToString();
                    }

                    // SBL 체크 적용 기준 : SEC 고객사, T0500 공정, LotType MP(양산품)인 경우
                    if (this.uTextCustomerCode.Text.Equals("SEC") && this.uTextWorkProcessCode.Text.Equals("T0500") && strProductionType.Equals("MP"))
                    //if (this.uTextCustomerCode.Text.Equals("SEC") && this.uTextWorkProcessCode.Text.Equals("T0500"))
                    {
                        // SBL_PASS 여부 확인
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                        QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        DataTable dtSBLPASS = clsLot.mfReadINSProcInspectReq_Check_SBLPASS(this.uComboPlantCode.Value.ToString(), this.uTextProductCode.Text, this.uTextLotNo.Text);
                        if (dtSBLPASS.Rows.Count > 0)
                        {
                            if (dtSBLPASS.Rows[0]["SBL_PASS"].ToString().Equals("N"))
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001505", "M001504"
                                    , Infragistics.Win.HAlign.Right);

                                return;
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001347", "M001346"
                                , Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }

                    // Package 번호 MES I/F 통해 넘어오는것 사용, 현재공정코드로 TrackIn 여부만 가져온다
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                    QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                    DataTable dtCheck = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlantCode.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_FONTNAME"));

                    // Lot 상태가 RUN 이면 TrackIn 처리한 상태??
                    if (this.uTextLotProcessState.Text.Equals("RUN"))
                    {
                        this.uButtonTrackIn.Enabled = false;
                        this.uTextMESTrackInTCheck.Text = dtCheck.Rows[0]["TrackInFlag"].ToString();

                        if (dtCheck.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                        {
                            //this.uCheckLossCheckFlag.Visible = true;
                            //this.uNumLossQty.Visible = true;
                            this.uLabelLossCheck.Visible = true;
                            this.uLabelLossQty.Visible = true;
                        }
                        else
                        {
                            //this.uCheckLossCheckFlag.Visible = false;
                            //this.uNumLossQty.Visible = false;
                            this.uLabelLossCheck.Visible = false;
                            this.uLabelLossQty.Visible = false;
                        }
                    }
                    else if (this.uTextLotProcessState.Text.Equals("WAIT"))
                    {
                        // TrackInFlag에 따라서 TrackIn 버튼 활성화 설정
                        if (dtCheck.Rows.Count > 0)
                        {
                            if (dtCheck.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                            {
                                this.uButtonTrackIn.Enabled = true;
                                this.uTextMESTrackInTCheck.Text = "T";

                                //2012-10-03 주석 Track In 설비시 설비번호 : PV0001 지정
                                //if (this.uTextEquipCode.Text.Equals(string.Empty) || this.uTextEquipCode.Text.Equals("-"))
                                //{
                                    //EquipCode = "VI0001";
                                //T0500 공정이 아닌경우 설비번호 : PV0001 지정
                                if (!ProcessCode.Equals("T0500"))
                                {
                                    this.uTextEquipCode.Text = "PV0001";
                                    this.uTextEquipName.Text = "VISUAL INSPECTION";
                                }
                                //}
                                // 그리드 편집불가 상태로
                                this.uGridItem.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                this.uGridSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                this.uGridFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            }
                            else
                            {
                                this.uTextMESTrackInTCheck.Text = "F";
                                this.uButtonTrackIn.Enabled = false;

                                //this.uCheckLossCheckFlag.Visible = false;
                                //this.uNumLossQty.Visible = false;
                                this.uLabelLossCheck.Visible = false;
                                this.uLabelLossQty.Visible = false;
                            }
                        }
                    }

                    // 기본정보 모두 받은후에 이벤트 걸기 위해 제일 마지막에
                    this.uComboWorkProcess.Value = dtLotInfo.Rows[0]["WORKOPERID"].ToString();


                    //2012-08-22 TrackIn 공정 중 T0500제외 설비번호 고정
                    if (this.uTextMESTrackInTCheck.Text.Equals("T"))
                    {
                        if (ProcessCode.Equals("T0500"))
                            this.uComboEquip.Enabled = true;
                        else
                            this.uComboEquip.Enabled = false;
                    }
                    else
                        this.uComboEquip.Enabled = true;

                    //2012-08-22 TrackIn공정중 "T0500인경우 설비콤보박스 설정" 그외 TrackIn공정 설비번호고정
                    if (this.uComboEquip.Enabled)
                    {
                        DataTable dtEquipGroup = new DataTable();
                        // 2012.05.16 LotNo받아온 직후는 현재공정으로

                        // 설비그룹콤보박스 설정 2012-10-04 T0500 공정이 아닌 경우 설비그룹별 설비조회
                        if (!ProcessCode.Equals("T0500"))
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                            QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                            brwChannel.mfCredentials(clsEquipGroup);

                            dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlantCode.Value.ToString()
                                                                                                , this.uTextNowProcessCode.Text
                                                                                                , m_resSys.GetString("SYS_LANG"));

                            //설비콤보 Item Clear
                            this.uComboEquip.Items.Clear();
                            WinComboEditor wCombo = new WinComboEditor();
                            wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                                , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                                , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                        }
                        else
                        {
                            //T0500 공정 T0500 공정 설비 삽입 2012-10-04
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                            QRPMAS.BL.MASEQU.Equip clsEquipGroup = new QRPMAS.BL.MASEQU.Equip();
                            brwChannel.mfCredentials(clsEquipGroup);

                            dtEquipGroup = clsEquipGroup.mfReadEquip_Combo(this.uComboPlantCode.Value.ToString()
                                                                                                , "TEST"                        // Hard Coding
                                                                                                , m_resSys.GetString("SYS_LANG"));

                            //설비콤보 Item Clear
                            this.uComboEquip.Items.Clear();
                            WinComboEditor wCombo = new WinComboEditor();
                            wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                                , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                                , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "ComboName", dtEquipGroup);
                        }
                       
                    }
                    //else
                    //{

                    //    //this.uComboEquip.Items.Clear();
                    //    this.uTextEquipCode.Text = "PV0001";
                    //    this.uTextEquipName.Text = "VISUAL INSPECTION";
                    //    this.uComboEquip.Value = this.uTextEquipCode.Text;

                    //}


                    this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                    

                    //// 테스트 공정이면 HANDLER, PROGRAM 보이게 설정 --> 2012-10-05 T0500공정만 적용
                    string strWorkProcessCode = this.uComboWorkProcess.SelectedRow.Cells["ProcessCode"].Value == null ? string.Empty : this.uComboWorkProcess.SelectedRow.Cells["ProcessCode"].Value.ToString();
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    //QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    //brwChannel.mfCredentials(clsProcess);

                    //DataTable dtProcessInfo = clsProcess.mfReadMASProcess_Detail(this.uComboPlantCode.Value.ToString()
                    //                                                            , this.uComboWorkProcess.SelectedRow.Cells["ProcessCode"].Value.ToString()
                    //                                                            , m_resSys.GetString("SYS_LANG"));
                    //if (dtProcessInfo.Rows.Count > 0)
                    //{
                    //    if (dtProcessInfo.Rows[0]["PRODUCTOPERATIONTYPE"].ToString().ToUpper().Equals("TEST"))
                    //    {
                    //        this.uLabelHandler.Show();
                    //        this.uLabelProgram.Show();
                    //        this.uComboHandler.Show();
                    //        this.uTextProgram.Show();
                    //        this.uTextTestProcessFlag.Text = "T";
                    //    }
                    //    else
                    //    {
                    //        this.uLabelHandler.Hide();
                    //        this.uLabelProgram.Hide();
                    //        this.uComboHandler.Hide();
                    //        this.uTextProgram.Hide();
                    //        this.uTextTestProcessFlag.Text = "F";
                    //    }
                    //}
                    //2012-10-05 T0500공정인 경우 HANDLER, PROGRAM 보이게 설정
                    if (strWorkProcessCode.Equals("T0500"))
                    {
                        this.uLabelHandler.Show();
                        this.uLabelProgram.Show();
                        this.uComboHandler.Show();
                        this.uTextProgram.Show();
                        this.uTextTestProcessFlag.Text = "T";
                    }
                    else
                    {
                        this.uLabelHandler.Hide();
                        this.uLabelProgram.Hide();
                        this.uComboHandler.Hide();
                        this.uTextProgram.Hide();
                        this.uTextTestProcessFlag.Text = "F";
                    }

                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000089", strLang), msg.GetMessge_Text("M000082", strLang)
                        , dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                    Clear();
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 기타..

        /// <summary>
        /// 불량정보 조회
        /// </summary>
        /// <returns></returns>
        private bool Check_FaultData()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // 불량정보가 저장되었는지 안되었는지 확인
                if (this.uTextReqNo.Text.Equals(string.Empty) && this.uTextReqSeq.Text.Equals(string.Empty))
                {
                    string strInspectItemName = string.Empty;
                    int intCurrentReqItemSeq = -1;
                    // 불량정보그리드에 데이터가 있으면 현재 검사항목 순번 저장
                    if (this.uGridFaultData.Rows.Count > 0)
                    {
                        intCurrentReqItemSeq = Convert.ToInt32(this.uGridFaultData.Rows[0].Cells["ReqItemSeq"].Value);
                    }

                    for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                    {
                        // 검사항목이 불량인경우
                        if (this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                        {
                            // 현재 검사항목이 아닌경우
                            if (!this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.Equals(intCurrentReqItemSeq))
                            {
                                strInspectItemName += this.uGridSampling.Rows[i].Cells["InspectItemName"].Value.ToString() + "<br/>";
                            }
                        }
                    }

                    if (strInspectItemName.Length > 0)
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000614", strLang)
                            , strInspectItemName + msg.GetMessge_Text("M000194", strLang)
                            , Infragistics.Win.HAlign.Center);

                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    // 관리번호가 저장된 경우
                    // 현재 불량정보 그리드에 데이터가 있을경우 검사항목순번 저장
                    int intCurrentReqItemSeq = -99; // 디비에서 불량인대 불량정보 저장 안된경우 -1로 Return 해주기 때문에 -99 로 설정
                    if (this.uGridFaultData.Rows.Count > 0)
                    {
                        intCurrentReqItemSeq = Convert.ToInt32(this.uGridFaultData.Rows[0].Cells["ReqItemSeq"].Value);
                    }

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                    QRPINS.BL.INSPRC.ProcInspectResultFault clsRFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                    brwChannel.mfCredentials(clsRFault);

                    DataTable dtFaultCheck = clsRFault.mfReadINSProcInspectResultFault_InspectItem(this.uComboPlantCode.Value.ToString()
                                                                                                , this.uTextReqNo.Text
                                                                                                , this.uTextReqSeq.Text
                                                                                                , 1
                                                                                                , m_resSys.GetString("SYS_LANG"));

                    string strInspectItemName = string.Empty;

                    for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtFaultCheck.Rows.Count; j++)
                        {
                            if (Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value) == Convert.ToInt32(dtFaultCheck.Rows[j]["ReqItemSeq"]))
                            {
                                // 그리드의 검사결과가 불합격인 경우
                                if (this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                                {
                                    // 디비에서 조회된 검사항목 순번이 -1(불량정보 저장 안된경우)이면서 현재 불량정보 그리드에 있는 검사항목순번과 같지 않은경우는 불량정보 저장 안된 정보
                                    if (dtFaultCheck.Rows[j]["FaultReqItemSeq"].ToString().Equals("-1") &&
                                        !dtFaultCheck.Rows[j]["ReqItemSeq"].ToString().Equals(intCurrentReqItemSeq.ToString()))
                                    {
                                        strInspectItemName += dtFaultCheck.Rows[j]["InspectItemName"].ToString() + "<br/>";
                                    }
                                }
                                else if (this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString().Equals("OK"))
                                {
                                    // 합격인대 불합격정보가 저장된경우 삭제
                                    if (!dtFaultCheck.Rows[j]["FaultReqItemSeq"].ToString().Equals("-1"))
                                    {
                                        string strErrRtn = clsRFault.mfDeleteINSProcInspectResultFault(this.uComboPlantCode.Value.ToString()
                                                                                                    , this.uTextReqNo.Text
                                                                                                    , this.uTextReqSeq.Text
                                                                                                    , 1
                                                                                                    , Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value));
                                    }
                                }
                            }
                        }
                    }

                    if (strInspectItemName.Length > 0)
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000614", strLang)
                            , strInspectItemName + msg.GetMessge_Text("M000194", strLang)
                            , Infragistics.Win.HAlign.Center);

                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }
        /// <summary>
        /// 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //this.uButtonTrackIn.Visible = false;
                this.uButtonTrackIn.Enabled = false;

                this.uGridItem.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                this.uGridFault.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                while (this.uGridFaultData.Rows.Count > 0)
                {
                    this.uGridFaultData.Rows[0].Delete(false);
                }
                while (this.uGridFault.Rows.Count > 0)
                {
                    this.uGridFault.Rows[0].Delete(false);
                }
                while (this.uGridSampling.Rows.Count > 0)
                {
                    this.uGridSampling.Rows[0].Delete(false);
                }
                while (this.uGridItem.Rows.Count > 0)
                {
                    this.uGridItem.Rows[0].Delete(false);
                }

                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboProcInspectType.Value = "1";
                this.uTextLotNo.Clear();
                this.uTextWorkProcessName.Clear();
                this.uTextEquipName.Clear();
                this.uTextLotSize.Clear();
                this.uTextCustomerCode.Clear();
                this.uTextCustomerName.Clear();
                this.uTextEtcDesc.Clear();
                this.uTextFile.Clear();
                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextProductCode.Clear();
                this.uTextProductName.Clear();
                this.uTextReqNo.Clear();
                this.uTextReqSeq.Clear();
                this.uDateInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateInspectTime.Value = DateTime.Now.ToString("HH:mm:ss");
                this.uOptionPassFailFlag.CheckedIndex = -1;
                this.uCheckCompleteFlag.CheckedValue = true;
                this.uTextProcessHoldState.Clear();

                this.uComboPlantCode.Enabled = true;
                this.uComboProcInspectType.Enabled = true;
                this.uTextLotNo.Enabled = true;
                this.uCheckCompleteFlag.Enabled = true;

                this.uComboStack.Value = "";
                this.uTextWorkProcessCode.Clear();
                this.uTextWorkProcessName.Clear();
                this.uTextWorkUserID.Clear();
                this.uTextWorkUserName.Clear();
                this.uTextPackage.Clear();
                this.uTextNowProcessName.Clear();
                this.uTextNowProcessCode.Clear();
                this.uTextMESTrackOutTFlag.Clear();
                this.uTextMESTrackInTFlag.Text = "F";
                this.uTextMESTrackInTDate.Clear();
                this.uTextMESTrackInTCheck.Clear();
                this.uTextMESSPCNTFlag.Clear();
                this.uTextMESHoldTFlag.Clear();
                this.uTextEquipCode.Clear();
                this.uTextCustomerProductCode.Clear();
                this.uTextLotProcessState.Clear();

                this.uTextOUTSOURCINGVENDOR.Clear();

                this.uCheckLossCheckFlag.Checked = false;
                this.uNumLossQty.Value = 0;
                this.uCheckLossCheckFlag.Visible = false;
                this.uNumLossQty.Visible = false;
                this.uLabelLossQty.Visible = false;
                this.uLabelLossCheck.Visible = false;

                this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                this.uGridItem.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                // 설비콤보박스 초기화
                this.uComboEquip.Enabled = true;
                DataTable dtEquip = new DataTable();
                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "EquipCode", "EquipName", dtEquip);

                //this.uComboStack.Items.Clear();
                this.uComboWorkProcess.EventManager.AllEventsEnabled = false;
                this.uComboWorkProcess.Value = "";
                this.uComboWorkProcess.EventManager.AllEventsEnabled = true;

                this.uTextLotNo.ReadOnly = false;
                this.uTextLotNo.Appearance.BackColor = Color.PowderBlue;

                

                // BG 여부
                this.uLabelWafer.Hide();
                this.uComboWafer.Hide();
                this.uComboWafer.Value = string.Empty;
                this.uTextPCB_Layout.Clear();

                // Test 공정의 HANDLER, PROGRAM
                this.uComboHandler.Value = string.Empty;
                this.uTextProgram.Clear();
                this.uLabelProgram.Hide();
                this.uLabelHandler.Hide();
                this.uComboHandler.Hide();
                this.uTextProgram.Hide();
                this.uTextTestProcessFlag.Text = "F";

                this.uTextQCNorITRCheck.Clear();

                this.uTextLotNo.Focus();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
            }
        }

        /// <summary>
        /// QCN 자동이동 메소드
        /// </summary>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        private void MoveQCN(string strReqNo, string strReqSeq)
        {
            try
            {
                string strPlantCode = this.uComboPlantCode.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                brwChannel.mfCredentials(clsFault);

                DataTable dtCheck = clsFault.mfReadINSProcInspectResultFault_MoveQCN(strPlantCode, strReqNo, strReqSeq);

                // QCN 자동이동 여부 판단
                if (dtCheck.Rows.Count > 0)
                {
                    frmINSZ0010_S frmINS = new frmINSZ0010_S();
                    frmINS.PlantCode = dtCheck.Rows[0]["PlantCode"].ToString();
                    frmINS.ReqNo = dtCheck.Rows[0]["ReqNo"].ToString();
                    frmINS.ReqSeq = dtCheck.Rows[0]["ReqSeq"].ToString();
                    frmINS.ReqLotSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqLotSeq"]);
                    frmINS.ReqItemSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqItemSeq"]);
                    frmINS.ReqInspectSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqInspectSeq"]);
                    frmINS.FormName = this.Name;

                    CommonControl cControl = new CommonControl();
                    Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                    Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                    if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINSZ0010_S"))
                    {
                        uTabMenu.Tabs["QRPINS" + "," + "frmINSZ0010_S"].Close();
                    }
                    //탭에 추가
                    uTabMenu.Tabs.Add("QRPINS" + "," + "frmINSZ0010_S", "QCN 발행");
                    uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINSZ0010_S"];

                    //호출시 화면 속성 설정
                    frmINS.AutoScroll = true;
                    frmINS.MdiParent = this.MdiParent;
                    frmINS.ControlBox = false;
                    frmINS.Dock = DockStyle.Fill;
                    frmINS.FormBorderStyle = FormBorderStyle.None;
                    frmINS.WindowState = FormWindowState.Normal;
                    frmINS.Text = "QCN 발행";
                    frmINS.Show();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 스택콤보설정 메소드
        private void ComboStackSeq()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 콤보박스 클리어
                this.uComboStack.Items.Clear();

                // 변수 설정
                string strPlantCode = this.uComboPlantCode.Value.ToString();
                string strProductCode = this.uTextProductCode.Text;
                //string strProcessCode = this.uTextWorkProcessCode.Text;
                string strProcessCode = this.uComboWorkProcess.Value.ToString();

                if (!strProcessCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                    QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                    brwChannel.mfCredentials(clsLot);

                    DataTable dtStack = clsLot.mfReadCCSInspectReq_ForStackCombo(strPlantCode, strProductCode, strProcessCode, "C", "");

                    wCombo.mfSetComboEditor(this.uComboStack, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_LANG")
                                                , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                                                , "", "", "전체", "StackCode", "StackName", dtStack);

                    if (dtStack.Rows.Count > 0)
                    {
                        //int intItemCount = this.uComboStack.Items.Count;
                        // (Key 가 ""인 것 제거)
                        for (int i = 1; i < this.uComboStack.Items.Count; i++)
                        {
                            if (this.uComboStack.Items[0].DataValue.Equals(this.uComboStack.Items[i].DataValue))
                            {
                                this.uComboStack.Items.Remove(i);
                                i--;
                            }
                        }

                        // 키 중복 제거하고 콤보Item 갯수가 여러개이면 차수확인하라는 메세지창 띄운다.
                        if (this.uComboStack.Items.Count > 1)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M001264", "M001129", "M001130"
                                                                            , Infragistics.Win.HAlign.Center);
                        }

                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M001264", "M001001", "M001002"
                                                                    , Infragistics.Win.HAlign.Center);
                    //this.uComboWorkProcess.dropd
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비콤보박스 설정 메소드
        /// </summary>
        private bool SetEquipCombo()
        {
            bool bolDataCheck = true;
            try
            {
                string strLotNo = this.uTextLotNo.Text.Trim().ToUpper();
                string strProcessCode = this.uComboWorkProcess.Value.ToString();
                if (!strLotNo.Equals(string.Empty) && !strProcessCode.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor wCombo = new WinComboEditor();

                    //////MES서버경로 가져오기
                    ////QRPBrowser brwChannel = new QRPBrowser();
                    ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    ////brwChannel.mfCredentials(clsSysAcce);

                    ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.MESCodeReturn), "MESCodeReturn");
                    ////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                    ////brwChannel.mfCredentials(clsMESCode);

                    ////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);
                    //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
                    //////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");

                    //////MES 설비정보 요청매서드 실행
                    ////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                    ////DataTable dtEquip = clsTibrv.LOT_PROCESSED_EQP_REQ(strLotNo, strProcessCode);
                    //////clsTibrv.Dispose();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                    QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                    brwChannel.mfCredentials(clsMES);

                    DataTable dtEquip = clsMES.mfRead_LOT_PROCESSED_EQP_REQ(this.uComboPlantCode.Value.ToString(), strLotNo, strProcessCode);

                    //설비정보가 있는경우
                    if (dtEquip.Rows.Count > 0)
                    {
                        if (dtEquip.Rows[0]["returncode"].ToString().Equals("0")) //dtEquip.Rows[0]["returncode"].ToString().Equals(string.Empty) || )
                        {
                            // 필요없는 컬럼 제거
                            dtEquip.Columns.Remove("returncode");
                            dtEquip.Columns.Remove("returnmessage");

                            // 설비명컬럼 추가
                            dtEquip.Columns.Add("EquipName", typeof(string));

                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                            QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                            brwChannel.mfCredentials(clsEquip);

                            for (int i = 0; i < dtEquip.Rows.Count; i++)
                            {
                                DataTable dtEquipName = clsEquip.mfReadEquipName(this.uComboPlantCode.Value.ToString(), dtEquip.Rows[i]["EQPID"].ToString(), m_resSys.GetString("SYS_LANG"));
                                if (dtEquipName.Rows.Count > 0)
                                {
                                    dtEquip.Rows[i]["EquipName"] = dtEquipName.Rows[0]["EquipName"].ToString();
                                }
                            }

                            wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                                                    , "EquipCode", "EquipName", dtEquip);
                            this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;
                            bolDataCheck = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            string strLang = m_resSys.GetString("SYS_LANG");
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000089", strLang)
                                                                    , dtEquip.Rows[0]["returnmessage"].ToString()
                                                                    , Infragistics.Win.HAlign.Center);

                            dtEquip = new DataTable();
                            wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                                                    , "EquipCode", "EquipName", dtEquip);

                            bolDataCheck = false;
                        }
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001264", "M001099", "M001248"
                                                                , Infragistics.Win.HAlign.Center);

                        dtEquip = new DataTable();
                        wCombo.mfSetComboEditor(this.uComboEquip, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                                , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                                                , "EquipCode", "EquipName", dtEquip);
                        bolDataCheck = false;
                    }
                }
                return bolDataCheck;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        private void FileUpload(string strReqNo, string strReqSeq)
        {
            try
            {
                ArrayList arrFile = new ArrayList();

                for (int i = 0; i < this.uGridFaultData.Rows.Count; i++)
                {
                    if (!this.uGridFaultData.Rows[i].Cells["FilePath"].Value.Equals(null) && !this.uGridFaultData.Rows[i].Cells["FilePath"].Value.ToString().Equals(string.Empty))
                    {
                        if (this.uGridFaultData.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridFaultData.Rows[i].Cells["FilePath"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                    this.uComboPlantCode.Value.ToString() + "-" +
                                                    strReqNo + strReqSeq + "-1-" +
                                                    this.uGridFaultData.Rows[i].Cells["ReqItemSeq"].Value.ToString() + "-" +
                                                    this.uGridFaultData.Rows[i].RowSelectorNumber.ToString() + "-" +
                                                    fileDoc.Name;

                            // 변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            // 변경한 화일이름으로 복사하기
                            File.Copy(this.uGridFaultData.Rows[i].Cells["FilePath"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                    }
                }

                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0026");

                    // 첨부파일 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();

                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion

        #region Events..

        #region 기타 이벤트..
        // 행삭제버튼 이벤트
        private void uButtonDelete2_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridFaultData.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridFaultData.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridFaultData.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사시간 스핀버튼 이벤트
        private void uDateInspectTime_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.EditorButtonEventArgs ed = sender as Infragistics.Win.UltraWinEditors.EditorButtonEventArgs;
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uDateInspectTime.Focus();
                    uDateInspectTime.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uDateInspectTime.Focus();
                    uDateInspectTime.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성완료 체크시 이벤트
        private void uCheckCompleteFlag_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                // CheckEditor 가 활성화 상태일때
                if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true)
                {
                    // TrackIn 버튼이 활성화 상태일때
                    if (this.uButtonTrackIn.Enabled == true)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000149", "M000151",
                                        Infragistics.Win.HAlign.Right);

                        this.uCheckCompleteFlag.Checked = false;
                    }
                    //else
                    //{
                    //    WinMessageBox msg = new WinMessageBox();
                    //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                    "확인창", "작성완료 확인", "작성완료 체크후 저장시 이후 수정할 수 없습니다",
                    //                    Infragistics.Win.HAlign.Right);
                    //}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Loss 수량 버튼클릭이벤트
        private void uNumLossQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Loss 수량 Spin버튼 이벤트
        private void uNumLossQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    this.uNumLossQty.Focus();
                    this.uNumLossQty.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    this.uNumLossQty.Focus();
                    this.uNumLossQty.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region TrackIn 버튼 이벤트
        // TrackIn 버튼 클릭시 MES I/F Lot TrackIn 요청
        private void uButtonTrackIn_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 필수입력사항 확인
                if (this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                {
                    //공장을 선택해 주세요.
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlantCode.DropDown();
                    return;
                }
                else if (this.uTextLotNo.Text.Equals(string.Empty))
                {
                    //LotNo을 입력해 주세요
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000078", Infragistics.Win.HAlign.Center);

                    this.uTextLotNo.Focus();
                    return;
                }
                else if (this.uComboEquip.Value.ToString().Equals(string.Empty))
                {
                    //설비번호를 선택해 주세요.
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000703", Infragistics.Win.HAlign.Center);

                    this.uComboEquip.DropDown();
                    return;
                }
                else if (!this.uTextLotProcessState.Text.Equals("WAIT"))
                {
                    //RUN 상태인 Lot은 TrackIn 할 수 없습니다.
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000080", "M000117", Infragistics.Win.HAlign.Center);

                    this.uComboEquip.DropDown();
                    return;
                }
                else if (!this.uTextProcessHoldState.Text.Equals("NotOnHold"))
                {
                    //Hold 상태인 Lot은 TrackIn 할 수 없습니다.
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000067", "M000053", Infragistics.Win.HAlign.Center);

                    this.uComboEquip.DropDown();
                    return;
                }
                else
                {
                    // 설비테이블에서 AREACODE 조회
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    if (dtEquip.Rows.Count > 0)
                    {
                        // LotList 설정
                        DataTable dtLotList = new DataTable();
                        // 컬럼설정
                        dtLotList.Columns.Add("LOTID", typeof(string));
                        dtLotList.Columns.Add("EQPID", typeof(string));
                        dtLotList.Columns.Add("RECIPEID", typeof(string));
                        dtLotList.Columns.Add("AREAID", typeof(string));
                        dtLotList.Columns.Add("IQASAMPLINGFLAG", typeof(string));
                        dtLotList.Columns.Add("COMMENT", typeof(string));

                        DataRow drRow = dtLotList.NewRow();
                        drRow["LOTID"] = this.uTextLotNo.Text;
                        //drRow["EQPID"] = this.uTextEquipCode.Text;
                        drRow["EQPID"] = this.uComboEquip.Value.ToString();                     // 콤보에서 선택한걸로
                        drRow["RECIPEID"] = "-";
                        drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();
                        drRow["IQASAMPLINGFLAG"] = "";
                        drRow["COMMENT"] = this.uTextEtcDesc.Text;
                        dtLotList.Rows.Add(drRow);

                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                        QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsLot.mfSaveINSProcINspectReqLot_MESTrackIn(this.uComboPlantCode.Value.ToString()
                                                                                    , this.Name
                                                                                    , dtLotList
                                                                                    , this.uTextInspectUserID.Text
                                                                                    , m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.InterfaceResultCode.Equals("0"))
                        {
                            this.uButtonTrackIn.Enabled = false;

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000150",
                                        Infragistics.Win.HAlign.Right);

                            this.uGridSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                            this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                            // 작업자 저장
                            string strWorkUserName = this.uTextWorkUserName.Text;
                            string strWorkUserID = this.uTextWorkUserID.Text;
                            // LotInfo 요청후 처리 메소드 호출
                            Rtn_MES_LotInfo();
                            // TrackIn 시 작업자가 현재 검사자로 변경되서 TrackIn 이전 작업자로 Display 하기 위한 구문
                            this.uTextWorkUserName.Text = strWorkUserName;
                            this.uTextWorkUserID.Text = strWorkUserID;

                            this.uCheckCompleteFlag.Checked = true;
                            this.uTextMESTrackInTFlag.Text = "T";
                            this.uTextMESTrackInTDate.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                            // 검사수량으로 커서 이동
                            if (this.uGridSampling.Rows.Count > 0)
                            {
                                this.uGridSampling.Rows[0].Cells["ProcessSampleSize"].Activate();
                                this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                        else
                        {
                            if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M000095", "M000951", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                    , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                            }

                            this.uCheckCompleteFlag.Checked = false;
                            this.uTextMESTrackInTFlag.Text = "F";
                            this.uTextMESTrackInTDate.Text = string.Empty;
                        }
                    }
                    else
                    {
                        //설비정보를 조회할 수 없습니다
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M000717", "M000716",
                            Infragistics.Win.HAlign.Right);

                        this.uCheckCompleteFlag.Checked = false;
                        this.uTextMESTrackInTFlag.Text = "F";
                        this.uTextMESTrackInTDate.Text = string.Empty;
                        return;
                    }
                }

                #region TrackIn 시 데이터 저장없이 MES I/F 만 실행
                /*
                // 필수입력사항 확인
                if (this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlantCode.DropDown();
                    return;
                }
                else if (this.uComboProcInspectType.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000287", Infragistics.Win.HAlign.Center);

                    this.uComboProcInspectType.DropDown();
                    return;
                }
                else if (this.uTextLotNo.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000078", Infragistics.Win.HAlign.Center);

                    this.uTextLotNo.Focus();
                    return;
                }
                else if (this.uComboEquip.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000703", Infragistics.Win.HAlign.Center);

                    this.uComboEquip.DropDown();
                    return;
                }
                else if (!this.uTextLotProcessState.Text.Equals("WAIT"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000080", "M000117", Infragistics.Win.HAlign.Center);

                    this.uComboEquip.DropDown();
                    return;
                }
                else if (!this.uTextProcessHoldState.Text.Equals("NotOnHold"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000067", "M000053", Infragistics.Win.HAlign.Center);

                    this.uComboEquip.DropDown();
                    return;
                }
                //else if (!(this.uGridSampling.Rows.Count > 0))
                //{
                //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                    , "확인창", "검사항목 확인", "검사항목이 존재하지 않아 TrackIn 할 수 없습니다.", Infragistics.Win.HAlign.Center);

                //    return;
                //}
                else
                {
                    // 저장여부를 묻는다

                    //if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                "확인창", "TrackIn 확인", "TrackIn 처리 하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    //{
                    this.uCheckCompleteFlag.Checked = false;
                    // 저장정보 데이터 테이블 설정
                    DataTable dtHeader = Rtn_HeaderDataTable();
                    DataTable dtLot = Rtn_LotDataTable();
                    //DataTable dtItem = Rtn_ItemDataTable();
                    //DataSet dsResult = Rtn_ResultDataSet();
                    //DataTable dtFault = Rtn_FaultDataTable();
                    //DataTable dtCycle = Rtn_CycleDataTable();

                    DataTable dtItem = new DataTable();
                    DataSet dsResult = new DataSet();
                    DataTable dtFault = new DataTable();
                    DataTable dtCycle = new DataTable();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                    QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                    brwChannel.mfCredentials(clsHeader);

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // Method 호출
                    string strErrRtn = clsHeader.mfSaveINSProcInspectReqH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                                                                        , dtLot, dtItem, dsResult, dtFault, dtCycle);

                    // 팦업창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리결과에 따른 메세지 박스
                    if (ErrRtn.ErrNum == 0)
                    {
                        string strReqNo = ErrRtn.mfGetReturnValue(0);
                        string strReqSeq = ErrRtn.mfGetReturnValue(1);

                        this.uTextReqNo.Text = strReqNo;
                        this.uTextReqSeq.Text = strReqSeq;

                        // 저장 성공시 MESTrackIn 처리
                        // 설비테이블에서 AREACODE 조회
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                        brwChannel.mfCredentials(clsEquip);

                        // TrackIn 공정인경우 설비콤보에서 선택한 코드를 보내지 않고 LotInfo 받아온 설비코드로 보낸다 (MES 요청사항)
                        //DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));
                        // 설비콤보에서 선택한것으로
                        DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), this.uComboEquip.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                        if (dtEquip.Rows.Count > 0)
                        {
                            // LotList 설정
                            DataTable dtLotList = new DataTable();
                            // 컬럼설정
                            dtLotList.Columns.Add("LOTID", typeof(string));
                            dtLotList.Columns.Add("EQPID", typeof(string));
                            dtLotList.Columns.Add("RECIPEID", typeof(string));
                            dtLotList.Columns.Add("AREAID", typeof(string));
                            dtLotList.Columns.Add("IQASAMPLINGFLAG", typeof(string));
                            dtLotList.Columns.Add("COMMENT", typeof(string));

                            DataRow drRow = dtLotList.NewRow();
                            drRow["LOTID"] = this.uTextLotNo.Text;
                            //drRow["EQPID"] = this.uTextEquipCode.Text;
                            drRow["EQPID"] = this.uComboEquip.Value.ToString();                     // 콤보에서 선택한걸로
                            drRow["RECIPEID"] = "-";
                            drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();
                            drRow["IQASAMPLINGFLAG"] = "";
                            drRow["COMMENT"] = "";
                            dtLotList.Rows.Add(drRow);

                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            // 기본정보 테이블 설정
                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                            drRow = dtStdInfo.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            drRow["FormName"] = this.Name;
                            dtStdInfo.Rows.Add(drRow);

                            strErrRtn = clsLot.mfSaveINSProcINspectReqLot_MESTrackIn(dtStdInfo, dtLotList, this.uTextInspectUserID.Text, m_resSys.GetString("SYS_USERIP"));

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.InterfaceResultCode.Equals("0") && ErrRtn.ErrNum.Equals(0))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000150",
                                            Infragistics.Win.HAlign.Right);

                                //this.uGridItem.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                this.uGridSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                //this.uGridFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                                ////// LotInfo 정보를 다시 받아온다
                                ////// 화일서버 연결정보 가져오기
                                ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                ////brwChannel.mfCredentials(clsSysAccess);
                                ////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");

                                ////// MES I/F
                                ////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);
                                ////DataTable dtLotInfo = clsTibrv.LOT_INFO_REQ(this.uTextLotNo.Text.Trim());
                                //////clsTibrv.Dispose();

                                ////// 데이터가 있는경우
                                ////if (dtLotInfo.Rows[0]["returncode"].ToString().Equals("0"))
                                ////{
                                ////    this.uTextNowProcessCode.Text = dtLotInfo.Rows[0]["OPERID"].ToString();
                                ////    this.uTextNowProcessName.Text = dtLotInfo.Rows[0]["OPERDESC"].ToString();
                                ////    this.uTextWorkProcessCode.Text = dtLotInfo.Rows[0]["WORKOPERID"].ToString();
                                ////    this.uTextLotSize.Text = dtLotInfo.Rows[0]["QTY"].ToString();
                                ////    this.uTextPackage.Text = dtLotInfo.Rows[0]["PACKAGE"].ToString();
                                ////    this.uTextLotProcessState.Text = dtLotInfo.Rows[0]["LOTPROCESSSTATE"].ToString();
                                ////    this.uTextProcessHoldState.Text = dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString();
                                ////    this.uTextWorkUserID.Text = dtLotInfo.Rows[0]["LASTEVENTUSER"].ToString();

                                ////    // 작업자명 설정
                                ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                                ////    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                                ////    brwChannel.mfCredentials(clsUser);

                                ////    DataTable dtUser = clsUser.mfReadSYSUser(this.uComboPlantCode.Value.ToString(), this.uTextWorkUserID.Text, m_resSys.GetString("SYS_LANG"));
                                ////    if (dtUser.Rows.Count > 0)
                                ////        this.uTextWorkUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                                ////}

                                ////// 데이터 다시 조회
                                ////Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                ////Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                ////Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                                // Key Down 이벤트 발생
                                System.Windows.Forms.KeyEventArgs aa = new KeyEventArgs(Keys.Enter);
                                this.uTextLotNo_KeyDown(this.uTextLotNo, aa);

                                // 검사수량으로 커서 이동
                                if (this.uGridSampling.Rows.Count > 0)
                                {
                                    this.uGridSampling.Rows[0].Cells["ProcessSampleSize"].Activate();
                                    this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                }

                                this.uCheckCompleteFlag.Checked = true;
                                this.uTextMESTrackInTFlag.Text = "T";
                            }
                            else
                            {
                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000095", "M000951", Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                        , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                }

                                this.uCheckCompleteFlag.Checked = false;
                            }
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000717", "M000716",
                                Infragistics.Win.HAlign.Right);

                            this.uCheckCompleteFlag.Checked = false;
                            return;
                        }


                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000953",
                                            Infragistics.Win.HAlign.Right);

                        this.uCheckCompleteFlag.Checked = false;
                    }
                    //}
                }
                */
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonTrackIn_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
                {
                    // 클릭이벤트 호출
                    System.EventArgs aa = new EventArgs();
                    this.uButtonTrackIn_Click(this.uButtonTrackIn, aa);

                    // 검사수량으로 커서 이동
                    if (this.uGridSampling.Rows.Count > 0)
                    {
                        this.uGridSampling.Rows[0].Cells["ProcessSampleSize"].Activate();
                        this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
        #endregion

        #region ComboBox Event

        // 콤보박스 선택시 Text 전체 선택되게
        private void uComboWorkProcess_Enter(object sender, EventArgs e)
        {
            this.uComboWorkProcess.SelectAll();
        }

        // Stack 콤보박스 선택시 해당하는 검사항목 불러오는 이벤트
        private void uComboStack_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboWorkProcess.Value == null)
                    return;
                else if (this.uComboStack.Value == null)
                    return;

                if (!this.uComboWorkProcess.Value.ToString().Equals(string.Empty) && !this.uTextProductCode.Text.Equals(string.Empty))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    string strStackSeq = this.uComboStack.Value.ToString();

                    // 샘플링그리드 컬럼설정
                    //InitSampleGrid();

                    // 공정검사 규격서에서 Data Load
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                    QRPINS.BL.INSPRC.ProcInspectReqItem clsReqItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                    brwChannel.mfCredentials(clsReqItem);

                    // AQL SampleSize 구하는 부분   /////////////////////////////////////////////////////////////////////////////
                    QRPSTA.STAINS.JOJUNG clsJoJung = new QRPSTA.STAINS.JOJUNG();
                    // Lot 수량
                    int intLotCount = Convert.ToInt32(this.uTextLotSize.Text);
                    // 검사단계
                    QRPSTA.STAINS.JojungInspectionStep InspectStep = QRPSTA.STAINS.JojungInspectionStep.NormalInspection;
                    // 검사비율
                    double dblInspectRate = 0.065;
                    // 검사수준
                    QRPSTA.STAINS.InspectLevel InspectLevel = QRPSTA.STAINS.InspectLevel.G2;

                    QRPSTA.STAINS.AQLTable AQLTable = new QRPSTA.STAINS.AQLTable();
                    AQLTable = clsJoJung.mfSetAQLTable(intLotCount, InspectLevel, InspectStep, dblInspectRate);

                    //DataTable dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), this.uTextWorkProcessCode.Text
                    //    , this.uTextProductCode.Text, strStackSeq, m_resSys.GetString("SYS_LANG"));
                    DataTable dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), this.uComboWorkProcess.Value.ToString()
                        , this.uTextProductCode.Text, strStackSeq, m_resSys.GetString("SYS_LANG"));

                    ////// 공정이 A8850일경우 AQL 적용
                    ////if (this.uComboWorkProcess.Value.ToString().Equals("A8850"))
                    ////{
                    ////    for (int i = 0; i < dtItem.Rows.Count; i++)
                    ////    {
                    ////        dtItem.Rows[i]["ProcessSampleSize"] = AQLTable.SampleSize;
                    ////    }
                    ////}

                    // 해당검사항목이 AQL적용 일때 SampleSize 변경
                    for (int i = 0; i < dtItem.Rows.Count; i++)
                    {
                        if (dtItem.Rows[i]["AQLFlag"].ToString().Equals("T"))
                        {
                            dtItem.Rows[i]["ProcessSampleSize"] = AQLTable.SampleSize;
                        }
                    }

                    this.uGridItem.DataSource = dtItem;
                    this.uGridItem.DataBind();

                    this.uGridSampling.DataSource = dtItem;
                    this.uGridSampling.DataBind();

                    this.uGridFault.DataSource = dtItem;
                    this.uGridFault.DataBind();

                    if (dtItem.Rows.Count > 0)
                    {
                        // Unbount 컬럼 삭제(기존 Xn 컬럼 삭제하기 위해)
                        this.uGridSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

                        // 컬럼생성 메소드 호출
                        String[] strLastColKey = { "Mean", "SpecRange" };
                        CreateColumn(this.uGridSampling, 0, "SampleSize", "ProcessSampleSize", strLastColKey);

                        // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                        SetSamplingGridColumn();

                        // OCAP에 등록된 정보가 있는지 조회
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.OCAP), "OCAP");
                        QRPINS.BL.INSSTS.OCAP clsOCAP = new QRPINS.BL.INSSTS.OCAP();
                        brwChannel.mfCredentials(clsOCAP);

                        DataTable dtOCAP = clsOCAP.mfReadINSProcOCAP_ForINSProcReq(this.uComboPlantCode.Value.ToString(), this.uTextLotNo.Text
                                                                                , this.uComboWorkProcess.Value.ToString(), this.uTextPackage.Text);
                        if (dtOCAP.Rows.Count > 0)
                        {
                            this.uTextEtcDesc.Text = dtOCAP.Rows[0]["ActionDesc"].ToString();

                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();

                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001367", strLang), msg.GetMessge_Text("M001368", strLang)
                                , dtOCAP.Rows[0]["ActionDesc"].ToString()
                                , Infragistics.Win.HAlign.Right);
                        }

                        // Generation 설정
                        DataTable dtGeneration = dtItem.DefaultView.ToTable(true, "Generation");

                        string strGeneration = string.Empty;

                        if (dtGeneration.Rows.Count > 1)
                            strGeneration = string.Empty;
                        else
                            strGeneration = dtGeneration.Rows[0]["Generation"].ToString();

                        // 메소드 호출
                        Search_STAValue(strGeneration);
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M000325", "M000285", "M000326", Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Stack 콤보박스 선택전 MES I/F 를 통해 정보를 받아왔는지 검사
        private void uComboStack_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uTextLotNo.Text.Equals(string.Empty) || this.uTextProductCode.Text.Equals(string.Empty))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M000070", "M000069", "M000072", Infragistics.Win.HAlign.Right);

                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장콤보 값 변할때 공정콤보박스 설정
        private void uComboPlantCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = this.uComboPlantCode.Value.ToString();
                DataTable dtProcess = new DataTable();

                if (!strPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));


                    dtProcess.Columns.Add("ProcessCodeName", typeof(string));
                    for (int i = 0; i < dtProcess.Rows.Count; i++)
                    {
                        dtProcess.Rows[i]["ProcessCodeName"] = dtProcess.Rows[i]["ProcessCode"].ToString() + " | " + dtProcess.Rows[i]["ProcessName"].ToString();

                    }
                }

                this.uComboWorkProcess.DataSource = dtProcess;
                this.uComboWorkProcess.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboWorkProcess_ValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboWorkProcess.SelectedRow == null)
                    return;
                else if (this.uComboWorkProcess.SelectedRow.Index.Equals(-1))
                    return;
                else if (this.uComboWorkProcess.Value == null)
                    return;
                else if (this.uComboWorkProcess.Value.ToString() == string.Empty)
                    return;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();

                string strStackSeq = this.uComboStack.Value.ToString();

                ComboStackSeq();

                // 설비그룹콤보박스 설정
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);

                DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlantCode.Value.ToString()
                                                                                    , this.uComboWorkProcess.Value.ToString()
                                                                                    , m_resSys.GetString("SYS_LANG"));
                this.uComboEquip.Items.Clear();
                WinComboEditor wCombo = new WinComboEditor();
                wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;

                // 테스트 공정이면 HANDLER, PROGRAM 보이게 설정 --> 2012-10-05 T0500공정만 적용
                string strWorkProcessCode = this.uComboWorkProcess.SelectedRow.Cells["ProcessCode"].Value == null ? string.Empty : this.uComboWorkProcess.SelectedRow.Cells["ProcessCode"].Value.ToString();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                //QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                //brwChannel.mfCredentials(clsProcess);

                //DataTable dtProcessInfo = clsProcess.mfReadMASProcess_Detail(this.uComboPlantCode.Value.ToString()
                //                                                            , this.uComboWorkProcess.SelectedRow.Cells["ProcessCode"].Value.ToString()
                //                                                            , m_resSys.GetString("SYS_LANG"));

                //if (dtProcessInfo.Rows.Count > 0)
                //{
                //    if (dtProcessInfo.Rows[0]["PRODUCTOPERATIONTYPE"].ToString().ToUpper().Equals("TEST"))
                //    {
                //        this.uLabelHandler.Show();
                //        this.uLabelProgram.Show();
                //        this.uComboHandler.Show();
                //        this.uTextProgram.Show();
                //        this.uTextTestProcessFlag.Text = "T";
                //    }
                //    else
                //    {
                //        this.uLabelHandler.Hide();
                //        this.uLabelProgram.Hide();
                //        this.uComboHandler.Hide();
                //        this.uTextProgram.Hide();
                //        this.uTextTestProcessFlag.Text = "F";
                //    }
                //}

                //2012-10-05 T0500공정인 경우 HANDLER, PROGRAM 보이게 설정
                if (strWorkProcessCode.Equals("T0500"))
                {
                    this.uLabelHandler.Show();
                    this.uLabelProgram.Show();
                    this.uComboHandler.Show();
                    this.uTextProgram.Show();
                    this.uTextTestProcessFlag.Text = "T";
                }
                else
                {
                    this.uLabelHandler.Hide();
                    this.uLabelProgram.Hide();
                    this.uComboHandler.Hide();
                    this.uTextProgram.Hide();
                    this.uTextTestProcessFlag.Text = "F";
                }

                if (strStackSeq.Equals(string.Empty))
                {
                    ////// 설비그룹콤보박스 설정
                    ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    ////QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    ////brwChannel.mfCredentials(clsEquipGroup);

                    ////DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlantCode.Value.ToString()
                    ////                                                                    , this.uComboWorkProcess.Value.ToString()
                    ////                                                                    , m_resSys.GetString("SYS_LANG"));
                    ////this.uComboEquip.Items.Clear();
                    ////WinComboEditor wCombo = new WinComboEditor();
                    ////wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    ////                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    ////                    , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                    ////this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;

                    // 공정검사 규격서에 데이터가 있는지 조회
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                    QRPISO.BL.ISOPRC.ProcessInspectSpecH clsInsSpecH = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                    brwChannel.mfCredentials(clsInsSpecH);

                    DataTable dtSpecCheck = clsInsSpecH.mfReadISOProcessInspectSpecCheck(this.uComboPlantCode.Value.ToString(), this.uTextPackage.Text, this.uTextCustomerCode.Text);

                    if (dtSpecCheck.Rows.Count > 0)
                    {
                        // 공정검사 규격서에서 Data Load
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                        QRPINS.BL.INSPRC.ProcInspectReqItem clsReqItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                        brwChannel.mfCredentials(clsReqItem);
                        DataTable dtItem = new DataTable();

                        // AQL SampleSize 구하는 부분   /////////////////////////////////////////////////////////////////////////////
                        QRPSTA.STAINS.JOJUNG clsJoJung = new QRPSTA.STAINS.JOJUNG();
                        // Lot 수량
                        int intLotCount = Convert.ToInt32(this.uTextLotSize.Text);
                        // 검사단계
                        QRPSTA.STAINS.JojungInspectionStep InspectStep = QRPSTA.STAINS.JojungInspectionStep.NormalInspection;
                        // 검사비율
                        double dblInspectRate = 0.065;
                        // 검사수준
                        QRPSTA.STAINS.InspectLevel InspectLevel = QRPSTA.STAINS.InspectLevel.G2;

                        QRPSTA.STAINS.AQLTable AQLTable = new QRPSTA.STAINS.AQLTable();
                        AQLTable = clsJoJung.mfSetAQLTable(intLotCount, InspectLevel, InspectStep, dblInspectRate);

                        // TrackIn 여부 상관없이 모두 작업공정으로 처리
                        //dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), this.uTextWorkProcessCode.Text
                        //    , this.uTextProductCode.Text, this.uComboStack.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                        dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), this.uComboWorkProcess.Value.ToString()
                            , this.uTextProductCode.Text, this.uComboStack.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                        ////// 공정이 A8850일경우 AQL 적용
                        ////if (this.uComboWorkProcess.Value.ToString().Equals("A8850"))
                        ////{
                        ////    for (int i = 0; i < dtItem.Rows.Count; i++)
                        ////    {
                        ////        dtItem.Rows[i]["ProcessSampleSize"] = AQLTable.SampleSize;
                        ////    }
                        ////}

                        // 해당검사항목이 AQL적용 일때 SampleSize 변경
                        for (int i = 0; i < dtItem.Rows.Count; i++)
                        {
                            if (dtItem.Rows[i]["AQLFlag"].ToString().Equals("T"))
                            {
                                dtItem.Rows[i]["ProcessSampleSize"] = AQLTable.SampleSize;
                            }
                        }

                        this.uGridItem.DataSource = dtItem;
                        this.uGridItem.DataBind();

                        this.uGridSampling.DataSource = dtItem;
                        this.uGridSampling.DataBind();

                        // Unbount 컬럼 삭제(기존 Xn 컬럼 삭제하기 위해)
                        this.uGridSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

                        this.uGridFault.DataSource = dtItem;
                        this.uGridFault.DataBind();

                        if (dtItem.Rows.Count > 0)
                        {
                            // 컬럼생성 메소드 호출
                            String[] strLastColKey = { "Mean", "SpecRange" };
                            CreateColumn(this.uGridSampling, 0, "SampleSize", "ProcessSampleSize", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn();

                            // OCAP에 등록된 정보가 있는지 조회
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.OCAP), "OCAP");
                            QRPINS.BL.INSSTS.OCAP clsOCAP = new QRPINS.BL.INSSTS.OCAP();
                            brwChannel.mfCredentials(clsOCAP);

                            DataTable dtOCAP = clsOCAP.mfReadINSProcOCAP_ForINSProcReq(this.uComboPlantCode.Value.ToString(), this.uTextLotNo.Text
                                                                                    , this.uComboWorkProcess.Value.ToString(), this.uTextPackage.Text);
                            if (dtOCAP.Rows.Count > 0)
                            {
                                this.uTextEtcDesc.Text = dtOCAP.Rows[0]["ActionDesc"].ToString();

                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001367", strLang), msg.GetMessge_Text("M001368", strLang)
                                    , dtOCAP.Rows[0]["ActionDesc"].ToString()
                                    , Infragistics.Win.HAlign.Right);
                            }

                            // Generation 설정
                            DataTable dtGeneration = dtItem.DefaultView.ToTable(true, "Generation");

                            string strGeneration = string.Empty;

                            if (dtGeneration.Rows.Count > 1)
                                strGeneration = string.Empty;
                            else
                                strGeneration = dtGeneration.Rows[0]["Generation"].ToString();

                            // 메소드 호출
                            Search_STAValue(strGeneration);
                        }
                        else
                        {
                            // TrackIn 공정의 경우 항목 없다는 메세지 안나오게 
                            if (!this.uButtonTrackIn.Enabled)
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M000325", "M000285", "M000326", Infragistics.Win.HAlign.Right);

                                this.uComboWorkProcess.Focus();
                                this.uComboWorkProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                            }
                            //else
                            //{
                            //    this.uButtonTrackIn.Focus();
                            //}
                        }

                        this.uTextInspectUserID.Focus();

                        ////// 설비그룹콤보박스 설정
                        ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                        ////QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                        ////brwChannel.mfCredentials(clsEquipGroup);

                        ////DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlantCode.Value.ToString()
                        ////                                                                    , this.uComboWorkProcess.Value.ToString()
                        ////                                                                    , m_resSys.GetString("SYS_LANG"));
                        ////this.uComboEquip.Items.Clear();
                        ////WinComboEditor wCombo = new WinComboEditor();
                        ////wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        ////                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                        ////                    , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                        ////this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;

                        // 검사자로 커서 이동
                        //this.uTextInspectUserID.Focus();
                        //this.uComboWorkProcess.Focus();
                        //this.uComboWorkProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                    }
                    else
                    {
                        // TrackIn 공정의 경우 항목 없다는 메세지 안나오게 
                        if (!this.uButtonTrackIn.Enabled)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M000325", "M000285", "M000381", Infragistics.Win.HAlign.Right);

                            //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                            //QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                            //brwChannel.mfCredentials(clsEquipGroup);

                            //DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlantCode.Value.ToString()
                            //                                                                    , this.uComboWorkProcess.Value.ToString()
                            //                                                                    , m_resSys.GetString("SYS_LANG"));
                            //this.uComboEquip.Items.Clear();
                            //WinComboEditor wCombo = new WinComboEditor();
                            //wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            //                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                            //                    , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                            //this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;

                            // 검사자로 커서 이동
                            //this.uTextInspectUserID.Focus();
                            this.uComboWorkProcess.Focus();
                            this.uComboWorkProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.Dropdown);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboWorkProcess_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uTextLotNo.Text.Trim().Equals(string.Empty) || this.uTextPackage.Text.Trim().Equals(string.Empty) || this.uTextProductCode.Text.Trim().Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M000060", "M000061", "M000077", Infragistics.Win.HAlign.Right);

                    this.uComboWorkProcess.PerformAction(Infragistics.Win.UltraWinGrid.UltraComboAction.CloseDropdown);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region TextBox Events

        #region KeyDown
        // LotNo 텍스트박스 엔터키입력시 MES I/F 통해 기본정보를 받아온다
        private void uTextLotNo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextLotNo.ReadOnly)
                    return;

                if (e.KeyCode == Keys.Enter)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (!this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                    {
                        // LotNo 텍스트박스가 공백이 아닐경우
                        if (!this.uTextLotNo.Text.ToString().Equals(string.Empty))
                        {
                            // LotInfo 요청후 처리 메소드 호출
                            Rtn_MES_LotInfo();

                            #region 메소드로 이동에 의한 주석처리
                            ////// 초기화                                                        
                            ////ProcessCode = string.Empty;
                            ////this.uTextNowProcessCode.Clear();
                            ////this.uTextNowProcessName.Clear();
                            ////this.uTextWorkProcessCode.Clear();
                            ////this.uTextProductCode.Clear();
                            ////this.uTextProductName.Clear();
                            ////EquipCode = string.Empty;
                            ////this.uTextEquipCode.Clear();
                            ////this.uTextEquipName.Clear();
                            ////this.uTextLotSize.Clear();
                            ////this.uTextPackage.Clear();
                            ////this.uTextLotProcessState.Clear();
                            ////this.uTextCustomerProductCode.Clear();
                            ////this.uTextProcessHoldState.Clear();
                            ////this.uTextWorkUserID.Clear();
                            ////this.uTextOUTSOURCINGVENDOR.Clear();

                            ////while (this.uGridItem.Rows.Count > 0)
                            ////{
                            ////    this.uGridItem.Rows[0].Delete(false);
                            ////}
                            ////while (this.uGridSampling.Rows.Count > 0)
                            ////{
                            ////    this.uGridSampling.Rows[0].Delete(false);
                            ////}
                            ////while (this.uGridFault.Rows.Count > 0)
                            ////{
                            ////    this.uGridFault.Rows[0].Delete(false);
                            ////}
                            ////while (this.uGridFaultData.Rows.Count > 0)
                            ////{
                            ////    this.uGridFaultData.Rows[0].Delete(false);
                            ////}

                            //////this.uComboStack.Items.Clear();
                            ////this.uComboWorkProcess.EventManager.AllEventsEnabled = false;
                            ////this.uComboWorkProcess.Value = "";
                            ////this.uComboWorkProcess.EventManager.AllEventsEnabled = true;

                            ////////// 화일서버 연결정보 가져오기
                            ////////QRPBrowser brwChannel = new QRPBrowser();
                            ////////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            ////////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            ////////brwChannel.mfCredentials(clsSysAccess);

                            ////////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                            ////////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);
                            //////////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
                            //////////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");

                            ////////// MES I/F
                            ////////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);
                            ////////DataTable dtLotInfo = clsTibrv.LOT_INFO_REQ(this.uTextLotNo.Text.ToUpper().Trim());
                            ////////clsTibrv.Dispose();

                            ////QRPBrowser brwChannel = new QRPBrowser();
                            ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                            ////QRPINS.BL.INSPRC.ProcMESInterface clsMes = new QRPINS.BL.INSPRC.ProcMESInterface();
                            ////brwChannel.mfCredentials(clsMes);

                            ////string strLotNo = this.uTextLotNo.Text.ToUpper().Trim();
                            ////DataTable dtLotInfo = clsMes.mfRead_LOT_INFO_REQ(this.uComboPlantCode.Value.ToString(), strLotNo);

                            ////// 데이터가 있는경우
                            ////if (dtLotInfo.Rows[0]["returncode"].ToString().Equals("0"))
                            ////{
                            ////    // 합부판정 기본값 합격
                            ////    this.uOptionPassFailFlag.CheckedIndex = 0;

                            ////    this.uTextLotNo.ReadOnly = true;
                            ////    this.uTextLotNo.Appearance.BackColor = Color.Gainsboro;

                            ////    ProcessCode = dtLotInfo.Rows[0]["OPERID"].ToString();
                            ////    this.uTextNowProcessCode.Text = dtLotInfo.Rows[0]["OPERID"].ToString();
                            ////    this.uTextNowProcessName.Text = dtLotInfo.Rows[0]["OPERDESC"].ToString();
                            ////    this.uTextWorkProcessCode.Text = dtLotInfo.Rows[0]["WORKOPERID"].ToString();
                            ////    this.uTextProductCode.Text = dtLotInfo.Rows[0]["PRODUCTSPECNAME"].ToString();
                            ////    this.uTextProductName.Text = dtLotInfo.Rows[0]["PRODUCTSPECDESC"].ToString();
                            ////    EquipCode = dtLotInfo.Rows[0]["EQPID"].ToString();
                            ////    this.uTextEquipCode.Text = dtLotInfo.Rows[0]["EQPID"].ToString();
                            ////    this.uTextEquipName.Text = dtLotInfo.Rows[0]["EQPDESC"].ToString();
                            ////    this.uTextLotSize.Text = Convert.ToInt32(dtLotInfo.Rows[0]["QTY"].ToString()).ToString();
                            ////    this.uTextPackage.Text = dtLotInfo.Rows[0]["PACKAGE"].ToString();
                            ////    this.uTextLotProcessState.Text = dtLotInfo.Rows[0]["LOTPROCESSSTATE"].ToString();
                            ////    this.uTextCustomerProductCode.Text = dtLotInfo.Rows[0]["CUSTOMPRODUCTSPECNAME"].ToString(); //고객사제품코드
                            ////    this.uTextProcessHoldState.Text = dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString();
                            ////    this.uTextWorkUserID.Text = dtLotInfo.Rows[0]["LASTEVENTUSER"].ToString();
                            ////    //this.uTextOUTSOURCINGVENDOR.Text = dtLotInfo.Rows[0]["OUTSOURCINGVENDOR"].ToString();

                            ////    // 작업자명 설정
                            ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                            ////    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                            ////    brwChannel.mfCredentials(clsUser);

                            ////    DataTable dtUser = clsUser.mfReadSYSUser(this.uComboPlantCode.Value.ToString(), this.uTextWorkUserID.Text, m_resSys.GetString("SYS_LANG"));
                            ////    if (dtUser.Rows.Count > 0)
                            ////        this.uTextWorkUserName.Text = dtUser.Rows[0]["UserName"].ToString();

                            ////    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                            ////    QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                            ////    brwChannel.mfCredentials(clsHeader);

                            ////    //DataTable dtProcess = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlantCode.Value.ToString(), this.uTextWorkProcessCode.Text, m_resSys.GetString("SYS_FONTNAME"));
                            ////    //if (dtProcess.Rows.Count > 0)
                            ////    //    this.uTextWorkProcessName.Text = dtProcess.Rows[0]["ProcessName"].ToString();


                            ////    // 고객정보 조회
                            ////    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                            ////    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                            ////    brwChannel.mfCredentials(clsProduct);

                            ////    DataTable dtCustomer = clsProduct.mfReadMaterialCustomer(this.uComboPlantCode.Value.ToString(), this.uTextProductCode.Text, m_resSys.GetString("SYS_LANG"));

                            ////    if (dtCustomer.Rows.Count > 0)
                            ////    {
                            ////        this.uTextCustomerCode.Text = dtCustomer.Rows[0]["CustomerCode"].ToString();
                            ////        this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();
                            ////    }

                            ////    // Package 번호 MES I/F 통해 넘어오는것 사용, 현재공정코드로 TrackIn 여부만 가져온다
                            ////    DataTable dtCheck = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlantCode.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_FONTNAME"));

                            ////    // Lot 상태가 RUN 이면 TrackIn 처리한 상태??
                            ////    if (this.uTextLotProcessState.Text.Equals("RUN"))
                            ////    {
                            ////        this.uButtonTrackIn.Enabled = false;
                            ////        this.uTextMESTrackInTCheck.Text = dtCheck.Rows[0]["TrackInFlag"].ToString();

                            ////        if (dtCheck.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                            ////        {
                            ////            this.uCheckLossCheckFlag.Visible = true;
                            ////            this.uNumLossQty.Visible = true;
                            ////            this.uLabelLossCheck.Visible = true;
                            ////            this.uLabelLossQty.Visible = true;
                            ////        }
                            ////        else
                            ////        {
                            ////            this.uCheckLossCheckFlag.Visible = false;
                            ////            this.uNumLossQty.Visible = false;
                            ////            this.uLabelLossCheck.Visible = false;
                            ////            this.uLabelLossQty.Visible = false;
                            ////        }
                            ////    }
                            ////    else if (this.uTextLotProcessState.Text.Equals("WAIT"))
                            ////    {
                            ////        // TrackInFlag에 따라서 TrackIn 버튼 활성화 설정
                            ////        if (dtCheck.Rows.Count > 0)
                            ////        {
                            ////            if (dtCheck.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                            ////            {
                            ////                this.uButtonTrackIn.Enabled = true;
                            ////                this.uTextMESTrackInTCheck.Text = "T";
                            ////                if (this.uTextEquipCode.Text.Equals(string.Empty) || this.uTextEquipCode.Text.Equals("-"))
                            ////                {
                            ////                    //EquipCode = "VI0001";
                            ////                    this.uTextEquipCode.Text = "VI0001";
                            ////                    this.uTextEquipName.Text = "VISUAL INSPECTION";
                            ////                }

                            ////                // 그리드 편집불가 상태로
                            ////                this.uGridItem.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////                this.uGridSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////                this.uGridFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////                this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////            }
                            ////            else
                            ////            {
                            ////                this.uTextMESTrackInTCheck.Text = "F";
                            ////                this.uButtonTrackIn.Enabled = false;

                            ////                this.uCheckLossCheckFlag.Visible = false;
                            ////                this.uNumLossQty.Visible = false;
                            ////                this.uLabelLossCheck.Visible = false;
                            ////                this.uLabelLossQty.Visible = false;
                            ////            }
                            ////        }
                            ////    }

                            ////    // 기본정보 모두 받은후에 이벤트 걸기 위해 제일 마지막에
                            ////    this.uComboWorkProcess.Value = dtLotInfo.Rows[0]["WORKOPERID"].ToString();


                            ////    // 2012.05.16 LotNo받아온 직후는 현재공정으로
                            ////    // 설비그룹콤보박스 설정
                            ////    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                            ////    QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                            ////    brwChannel.mfCredentials(clsEquipGroup);

                            ////    DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroupCombo_Process(this.uComboPlantCode.Value.ToString()
                            ////                                                                        , this.uTextNowProcessCode.Text
                            ////                                                                        , m_resSys.GetString("SYS_LANG"));
                            ////    this.uComboEquip.Items.Clear();
                            ////    WinComboEditor wCombo = new WinComboEditor();
                            ////    wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            ////                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                            ////                        , this.uTextEquipCode.Text, this.uTextEquipCode.Text, this.uTextEquipName.Text, "EquipCode", "EquipName", dtEquipGroup);
                            ////    this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;




                            ////    #region 공정콤보박스 변경으로 인한 주석처리 2011.12.01
                            ////    //////////// 설비콤보박스 설정
                            ////    //////////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                            ////    //////////QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                            ////    //////////brwChannel.mfCredentials(clsEquip);

                            ////    //////////DataTable dtEquip = clsEquip.mfReadMASEquip_EquipType(this.uComboPlantCode.Value.ToString(), this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                            ////    //////////WinComboEditor wCombo = new WinComboEditor();
                            ////    //////////wCombo.mfSetComboEditor(this.uComboEquip, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                            ////    //////////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                            ////    //////////    , this.uTextEquipCode.Text, "", "선택", "EquipCode", "EquipName", dtEquip);

                            ////    //////////this.uComboEquip.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;                                

                            ////    //////////// 설비콤보 설정 메소드 호출
                            ////    //////////if (SetEquipCombo())
                            ////    //////////{
                            ////    ////////    //// 공정검사 규격서에 데이터가 있는지 조회
                            ////    ////////    //brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                            ////    ////////    //QRPISO.BL.ISOPRC.ProcessInspectSpecH clsInsSpecH = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                            ////    ////////    //brwChannel.mfCredentials(clsInsSpecH);

                            ////    ////////    //DataTable dtSpecCheck = clsInsSpecH.mfReadISOProcessInspectSpecCheck(this.uComboPlantCode.Value.ToString(), this.uTextPackage.Text, this.uTextCustomerCode.Text);

                            ////    ////////    //if (dtSpecCheck.Rows.Count > 0)
                            ////    ////////    //{

                            ////    ////////    //    #region 위쪽으로 자리이동으로 인한 주석처리
                            ////    ////////    //    // 위쪽으로 자리이동으로 인한 주석처리
                            ////    ////////    //    // Package 번호 MES I/F 통해 넘어오는것 사용, 현재공정코드로 TrackIn 여부만 가져온다
                            ////    ////////    //    ////DataTable dtCheck = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlantCode.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_FONTNAME"));

                            ////    ////////    //    ////// Lot 상태가 RUN 이면 TrackIn 처리한 상태??
                            ////    ////////    //    ////if (this.uTextLotProcessState.Text.Equals("RUN"))
                            ////    ////////    //    ////{
                            ////    ////////    //    ////    this.uButtonTrackIn.Enabled = false;
                            ////    ////////    //    ////    this.uTextMESTrackInTCheck.Text = dtCheck.Rows[0]["TrackInFlag"].ToString();

                            ////    ////////    //    ////    if (dtCheck.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                            ////    ////////    //    ////    {
                            ////    ////////    //    ////        this.uCheckLossCheckFlag.Visible = true;
                            ////    ////////    //    ////        this.uNumLossQty.Visible = true;
                            ////    ////////    //    ////        this.uLabelLossCheck.Visible = true;
                            ////    ////////    //    ////        this.uLabelLossQty.Visible = true;
                            ////    ////////    //    ////    }
                            ////    ////////    //    ////    else
                            ////    ////////    //    ////    {
                            ////    ////////    //    ////        this.uCheckLossCheckFlag.Visible = false;
                            ////    ////////    //    ////        this.uNumLossQty.Visible = false;
                            ////    ////////    //    ////        this.uLabelLossCheck.Visible = false;
                            ////    ////////    //    ////        this.uLabelLossQty.Visible = false;
                            ////    ////////    //    ////    }
                            ////    ////////    //    ////}
                            ////    ////////    //    ////else if (this.uTextLotProcessState.Text.Equals("WAIT"))
                            ////    ////////    //    ////{
                            ////    ////////    //    ////    // TrackInFlag에 따라서 TrackIn 버튼 활성화 설정
                            ////    ////////    //    ////    if (dtCheck.Rows.Count > 0)
                            ////    ////////    //    ////    {
                            ////    ////////    //    ////        if (dtCheck.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                            ////    ////////    //    ////        {
                            ////    ////////    //    ////            this.uButtonTrackIn.Enabled = true;
                            ////    ////////    //    ////            this.uTextMESTrackInTCheck.Text = "T";
                            ////    ////////    //    ////            if (EquipCode.Equals(string.Empty) || EquipCode.Equals("-"))
                            ////    ////////    //    ////            {
                            ////    ////////    //    ////                EquipCode = "VI0001";
                            ////    ////////    //    ////                this.uTextEquipCode.Text = "VI0001";
                            ////    ////////    //    ////                this.uTextEquipName.Text = "VISUAL INSPECTION";
                            ////    ////////    //    ////            }

                            ////    ////////    //    ////            // 그리드 편집불가 상태로
                            ////    ////////    //    ////            this.uGridItem.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////    ////////    //    ////            this.uGridSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////    ////////    //    ////            this.uGridFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////    ////////    //    ////            this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            ////    ////////    //    ////        }
                            ////    ////////    //    ////        else
                            ////    ////////    //    ////        {
                            ////    ////////    //    ////            this.uTextMESTrackInTCheck.Text = "F";
                            ////    ////////    //    ////            this.uButtonTrackIn.Enabled = false;

                            ////    ////////    //    ////            this.uCheckLossCheckFlag.Visible = false;
                            ////    ////////    //    ////            this.uNumLossQty.Visible = false;
                            ////    ////////    //    ////            this.uLabelLossCheck.Visible = false;
                            ////    ////////    //    ////            this.uLabelLossQty.Visible = false;
                            ////    ////////    //    ////        }
                            ////    ////////    //    ////    }
                            ////    ////////    //    ////}
                            ////    ////////    //    #endregion
                            ////    ////////    //    // 샘플링그리드 컬럼설정
                            ////    ////////    //    //InitSampleGrid();

                            ////    ////////    //    // 공정검사 규격서에서 Data Load
                            ////    ////////    //    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                            ////    ////////    //    QRPINS.BL.INSPRC.ProcInspectReqItem clsReqItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                            ////    ////////    //    brwChannel.mfCredentials(clsReqItem);
                            ////    ////////    //    DataTable dtItem = new DataTable();

                            ////    ////////    //    // AQL SampleSize 구하는 부분   /////////////////////////////////////////////////////////////////////////////
                            ////    ////////    //    QRPSTA.STAINS.JOJUNG clsJoJung = new QRPSTA.STAINS.JOJUNG();
                            ////    ////////    //    // Lot 수량
                            ////    ////////    //    int intLotCount = Convert.ToInt32(this.uTextLotSize.Text);
                            ////    ////////    //    // 검사단계
                            ////    ////////    //    QRPSTA.STAINS.JojungInspectionStep InspectStep = QRPSTA.STAINS.JojungInspectionStep.NormalInspection;
                            ////    ////////    //    // 검사비율
                            ////    ////////    //    double dblInspectRate = 0.065;
                            ////    ////////    //    // 검사수준
                            ////    ////////    //    QRPSTA.STAINS.InspectLevel InspectLevel = QRPSTA.STAINS.InspectLevel.G2;

                            ////    ////////    //    QRPSTA.STAINS.AQLTable AQLTable = new QRPSTA.STAINS.AQLTable();
                            ////    ////////    //    AQLTable = clsJoJung.mfSetAQLTable(intLotCount, InspectLevel, InspectStep, dblInspectRate);
                            ////    ////////    //    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                            ////    ////////    //    // TrackIn 공정일경우 현재공정으로 규격서를 불러온다
                            ////    ////////    //    ////if (uTextMESTrackInTCheck.Text.Equals("T"))
                            ////    ////////    //    ////{
                            ////    ////////    //    ////    dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), this.uTextNowProcessCode.Text
                            ////    ////////    //    ////    , this.uTextProductCode.Text, this.uComboStack.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                            ////    ////////    //    ////    // 공정이 A8850일경우 AQL 적용
                            ////    ////////    //    ////    if (this.uTextNowProcessCode.Text.Equals("A8850"))
                            ////    ////////    //    ////    {
                            ////    ////////    //    ////        for (int i = 0; i < dtItem.Rows.Count; i++)
                            ////    ////////    //    ////        {
                            ////    ////////    //    ////            dtItem.Rows[i]["ProcessSampleSize"] = AQLTable.SampleSize;
                            ////    ////////    //    ////        }
                            ////    ////////    //    ////    }

                            ////    ////////    //    ////}
                            ////    ////////    //    ////else if (uTextMESTrackInTCheck.Text.Equals("F"))
                            ////    ////////    //    ////{
                            ////    ////////    //    ////    dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), this.uTextWorkProcessCode.Text
                            ////    ////////    //    ////    , this.uTextProductCode.Text, this.uComboStack.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                            ////    ////////    //    ////    // 공정이 A8850일경우 AQL 적용
                            ////    ////////    //    ////    if (this.uTextWorkProcessCode.Text.Equals("A8850"))
                            ////    ////////    //    ////    {
                            ////    ////////    //    ////        for (int i = 0; i < dtItem.Rows.Count; i++)
                            ////    ////////    //    ////        {
                            ////    ////////    //    ////            dtItem.Rows[i]["ProcessSampleSize"] = AQLTable.SampleSize;
                            ////    ////////    //    ////        }
                            ////    ////////    //    ////    }
                            ////    ////////    //    ////}

                            ////    ////////    //    // TrackIn 여부 상관없이 모두 작업공정으로 처리
                            ////    ////////    //    dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), this.uTextWorkProcessCode.Text
                            ////    ////////    //        , this.uTextProductCode.Text, this.uComboStack.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                            ////    ////////    //    // 공정이 A8850일경우 AQL 적용
                            ////    ////////    //    if (this.uTextWorkProcessCode.Text.Equals("A8850"))
                            ////    ////////    //    {
                            ////    ////////    //        for (int i = 0; i < dtItem.Rows.Count; i++)
                            ////    ////////    //        {
                            ////    ////////    //            dtItem.Rows[i]["ProcessSampleSize"] = AQLTable.SampleSize;
                            ////    ////////    //        }
                            ////    ////////    //    }

                            ////    ////////    //    this.uGridItem.DataSource = dtItem;
                            ////    ////////    //    this.uGridItem.DataBind();

                            ////    ////////    //    this.uGridSampling.DataSource = dtItem;
                            ////    ////////    //    this.uGridSampling.DataBind();

                            ////    ////////    //    // Unbount 컬럼 삭제(기존 Xn 컬럼 삭제하기 위해)
                            ////    ////////    //    this.uGridSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            ////    ////////    //    this.uGridFault.DataSource = dtItem;
                            ////    ////////    //    this.uGridFault.DataBind();

                            ////    ////////    //    //// 그리드 AutoSize
                            ////    ////////    //    //WinGrid grd = new WinGrid();
                            ////    ////////    //    //grd.mfSetAutoResizeColWidth(this.uGridItem, 0);
                            ////    ////////    //    //grd.mfSetAutoResizeColWidth(this.uGridSampling, 0);
                            ////    ////////    //    //grd.mfSetAutoResizeColWidth(this.uGridFault, 0);

                            ////    ////////    //    if (dtItem.Rows.Count > 0)
                            ////    ////////    //    {
                            ////    ////////    //        // Stack ComboBox 설정
                            ////    ////////    //        this.uComboStack.ValueChanged -= new EventHandler(uComboStack_ValueChanged);
                            ////    ////////    //        ComboStackSeq();
                            ////    ////////    //        this.uComboStack.ValueChanged -= new EventHandler(uComboStack_ValueChanged);

                            ////    ////////    //        // 컬럼생성 메소드 호출
                            ////    ////////    //        String[] strLastColKey = { "Mean", "SpecRange" };
                            ////    ////////    //        CreateColumn(this.uGridSampling, 0, "SampleSize", "ProcessSampleSize", strLastColKey);

                            ////    ////////    //        // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            ////    ////////    //        SetSamplingGridColumn();

                            ////    ////////    //        // OCAP에 등록된 정보가 있는지 조회
                            ////    ////////    //        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.OCAP), "OCAP");
                            ////    ////////    //        QRPINS.BL.INSSTS.OCAP clsOCAP = new QRPINS.BL.INSSTS.OCAP();
                            ////    ////////    //        brwChannel.mfCredentials(clsOCAP);

                            ////    ////////    //        DataTable dtOCAP = clsOCAP.mfReadINSProcOCAP_ForINSProcReq(this.uComboPlantCode.Value.ToString(), this.uTextLotNo.Text
                            ////    ////////    //                                                                , this.uTextWorkProcessCode.Text, dtLotInfo.Rows[0]["PACKAGE"].ToString());
                            ////    ////////    //        if (dtOCAP.Rows.Count > 0)
                            ////    ////////    //        {
                            ////    ////////    //            this.uTextEtcDesc.Text = dtOCAP.Rows[0]["ActionDesc"].ToString();
                            ////    ////////    //        }

                            ////    ////////    //        // 검사자로 커서 이동
                            ////    ////////    //        this.uTextInspectUserID.Focus();
                            ////    ////////    //    }
                            ////    ////////    //    else
                            ////    ////////    //    {
                            ////    ////////    //        WinMessageBox msg = new WinMessageBox();
                            ////    ////////    //        DialogResult Result = new DialogResult();

                            ////    ////////    //        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////    ////////    //            , "규격서확인", "공정검사 규격서 조회 결과", "규격서에 등록된 공정검사항목이 없습니다.", Infragistics.Win.HAlign.Right);
                            ////    ////////    //    }
                            ////    ////////    //}
                            ////    ////////    //else
                            ////    ////////    //{
                            ////    ////////    //    WinMessageBox msg = new WinMessageBox();
                            ////    ////////    //    DialogResult Result = new DialogResult();

                            ////    ////////    //    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////    ////////    //        , "규격서확인", "공정검사 규격서 조회 결과", "등록된 공정검사 규격서가 없습니다", Infragistics.Win.HAlign.Right);
                            ////    ////////    //}
                            ////    //////////}
                            ////    #endregion
                            ////}
                            ////else
                            ////{
                            ////    WinMessageBox msg = new WinMessageBox();
                            ////    DialogResult Result = new DialogResult();
                            ////    string strLang = m_resSys.GetString("SYS_LANG");
                            ////    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////        , msg.GetMessge_Text("M000089", strLang), msg.GetMessge_Text("M000082", strLang)
                            ////        , dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            ////    Clear();
                            ////}
                            #endregion
                        }
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        //공장을 선택해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M000962", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                        this.uComboPlantCode.DropDown();
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사자 키다운 이벤트
        private void uTextInspectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlantCode.Value.ToString() != "")
                        {
                            String strWriteID = this.uTextInspectUserID.Text;

                            // UserName 검색 함수 호출
                            DataTable dtUserInfo = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                            if (dtUserInfo.Rows.Count > 0)
                            {
                                this.uTextInspectUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();

                                if (this.uButtonTrackIn.Enabled)
                                {
                                    //TrackIn 버튼으로 커서 이동
                                    this.uButtonTrackIn.Focus();
                                }
                                else
                                {
                                    // 검사수량으로 커서 이동
                                    if (this.uGridSampling.Rows.Count > 0)
                                    {
                                        this.uGridSampling.Rows[0].Cells["ProcessSampleSize"].Activate();
                                        this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    }
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextInspectUserID.Clear();
                                this.uTextInspectUserName.Clear();
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlantCode.DropDown();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID.TextLength <= 1 || this.uTextInspectUserID.SelectedText == this.uTextInspectUserID.Text)
                    {
                        this.uTextInspectUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextInspectUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextInspectUserName.Text.Equals(string.Empty))
                this.uTextInspectUserName.Clear();
        }

        private void uTextWorkUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWorkUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlantCode.Value.ToString() != "")
                        {
                            String strWriteID = this.uTextWorkUserID.Text;

                            // UserName 검색 함수 호출
                            DataTable dtUserInfo = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                            if (dtUserInfo.Rows.Count > 0)
                            {
                                this.uTextWorkUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextWorkUserID.Clear();
                                this.uTextWorkUserName.Clear();
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlantCode.DropDown();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWorkUserID.TextLength <= 1 || this.uTextWorkUserID.SelectedText == this.uTextWorkUserID.Text)
                    {
                        this.uTextWorkUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWorkUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextWorkUserName.Text.Equals(string.Empty))
                this.uTextWorkUserName.Clear();
        }

        #endregion

        #region 팝업창
        private void uTextWorkUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextWorkUserID.Text = frmPOP.UserID;
                this.uTextWorkUserName.Text = frmPOP.UserName;
                this.uComboPlantCode.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사자 버튼클릭시 팝업창 이벤트
        private void uTextInspectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uComboPlantCode.Value.ToString() != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                    , msg.GetMessge_Text("M001254", strLang) + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001", strLang)
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextInspectUserID.Text = "";
                        this.uTextInspectUserName.Text = "";
                    }
                    else
                    {
                        this.uTextInspectUserID.Text = frmPOP.UserID;
                        this.uTextInspectUserName.Text = frmPOP.UserName;
                    }
                }
                else
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M000266"
                                                , Infragistics.Win.HAlign.Right);

                    this.uComboPlantCode.DropDown();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 기타..

        private void uTextLotNo_AfterEnterEditMode(object sender, EventArgs e)
        {
            this.uTextLotNo.SelectAll();
        }

        #endregion

        #endregion

        #region 그리드 관련 이벤트들..
        #region 불량유형, Sampling 그리드
        // Sampling 그리드 결과값 입력시 통계값 및 합부판정 구하는 이벤트
        private void uGridSampling_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                // 그리드 이벤트 헤제
                this.uGridSampling.EventManager.SetEnabled(Infragistics.Win.UltraWinGrid.EventGroups.AfterEvents, false);

                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우
                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")
                        {
                            ////if (e.Cell.Value == DBNull.Value)
                            ////{
                            ////    e.Cell.Value = e.Cell.OriginalValue;
                            ////    return;
                            ////}

                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e);
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }

                    // 검사결과 컬럼 Update시 헤더 자동 합부판정 
                    else if (e.Cell.Column.Key == "InspectResultFlag")
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                        }
                        else
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = false;
                        }
                    }

                    // 불량수량이 0보다 크면 검사결과 불량판정
                    else if (e.Cell.Column.Key.Equals("FaultQty"))
                    {
                        int intSampleSize = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value));
                        //if (intSampleSize > 99)
                        //    intSampleSize = 99;
                        if (Convert.ToInt32(e.Cell.Value) <= intSampleSize)
                        {
                            if (e.Cell.Value.Equals(null))
                            {
                                e.Cell.Value = 0;
                            }
                            else if (Convert.ToInt32(e.Cell.Value) > 0)
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                                this.uOptionPassFailFlag.CheckedIndex = 1;
                            }
                            else if (Convert.ToInt32(e.Cell.Value).Equals(0))
                            {
                                e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                                e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            }

                            if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                            {
                                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                                if (intSampleSize > 99)
                                    intSampleSize = 99;
                                // Xn컬럼의 마지막 컬럼 Index저장
                                int intLastIndex = intSampleSize + intStart;

                                for (int j = intStart; j < intLastIndex; j++)
                                {
                                    if (e.Cell.Row.Cells[j].Column.Index < Convert.ToInt32(e.Cell.Value) + intStart)
                                        e.Cell.Row.Cells[j].Value = "NG";
                                    else
                                        e.Cell.Row.Cells[j].Value = "OK";
                                }
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000610", "M000896"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }
                    else if (e.Cell.Column.Key.Equals("ProcessSampleSize"))
                    {
                        if (Convert.ToInt32(e.Cell.Value) >= Convert.ToInt32(e.Cell.Row.Cells["FaultQty"].Value))
                        {
                            //this.uGridSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

                            // 기존 Xn 컬럼 삭제
                            // 원래 SampleSize 값으로 최대 Xn 을 구한다
                            int intOldSampleSize = Convert.ToInt32(e.Cell.OriginalValue) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value);
                            int intNewSampleSize = Convert.ToInt32(e.Cell.Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value);

                            if (intOldSampleSize > 99)
                                intOldSampleSize = 99;

                            if (intNewSampleSize > 99)
                                intNewSampleSize = 99;

                            for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                            {
                                if (this.uGridSampling.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                    this.uGridSampling.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                            }

                            // 컬럼생성 메소드 호출
                            String[] strLastColKey = { "Mean", "SpecRange" };
                            CreateColumn(this.uGridSampling, 0, "SampleSize", "ProcessSampleSize", strLastColKey);

                            // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                            SetSamplingGridColumn();

                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500 //입력하신 검사수량이 불량수량보다 작습니다.
                                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , "M000879", "M000182", "M000888"
                                                                    , Infragistics.Win.HAlign.Center);
                            e.Cell.Value = e.Cell.OriginalValue;
                        }
                    }

                    // 상단 검사결과 업데이트
                    int intCount = 0;
                    for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount++;
                        }
                    }
                    // 하나라도 불량일경우 헤더 합부판정 불합격
                    if (intCount > 0)
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 1;
                    }
                    else
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }

                    // 검사결과 그리드
                    for (int i = 0; i < this.uGridItem.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(this.uGridItem.Rows[i].Cells["Seq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["Seq"].Value))
                        {
                            // 합부판정
                            this.uGridItem.Rows[i].Cells["InspectResultFlag"].Value = e.Cell.Row.Cells["InspectResultFlag"].Value;
                            this.uGridItem.Rows[i].Cells["InspectIngFlag"].Value = e.Cell.Row.Cells["InspectIngFlag"].Value;
                            this.uGridItem.Rows[i].Cells["ProcessSampleSize"].Value = e.Cell.Row.Cells["ProcessSampleSize"].Value;
                            this.uGridItem.Rows[i].Cells["FaultQty"].Value = e.Cell.Row.Cells["FaultQty"].Value;
                            break;
                        }
                    }

                    // 불량그리드
                    for (int i = 0; i < this.uGridFault.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(this.uGridFault.Rows[i].Cells["Seq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["Seq"].Value))
                        {
                            this.uGridFault.Rows[i].Cells["InspectResultFlag"].Value = e.Cell.Row.Cells["InspectResultFlag"].Value;
                            this.uGridFault.Rows[i].Cells["ProcessSampleSize"].Value = e.Cell.Row.Cells["ProcessSampleSize"].Value;
                            this.uGridFault.Rows[i].Cells["FaultQty"].Value = e.Cell.Row.Cells["FaultQty"].Value;
                            break;
                        }
                    }
                }

                // 이벤트 등록
                this.uGridSampling.EventManager.SetEnabled(Infragistics.Win.UltraWinGrid.EventGroups.AfterEvents, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        // 불량수량 0 입력시 검사진행여부 체크하기 위한 이벤트
        private void uGridSampling_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridSampling.ActiveCell == null)
                    return;

                if (this.uGridSampling.ActiveCell.Column.Key.Equals("FaultQty"))
                {
                    int intIndex = this.uGridSampling.ActiveCell.Row.Index;
                    if (this.uGridSampling.ActiveCell.Value.Equals(0))
                    {
                        this.uGridSampling.EventManager.AllEventsEnabled = false;
                        this.uGridSampling.Rows[intIndex].Cells["InspectIngFlag"].Value = true;

                        if (this.uGridSampling.Rows[intIndex].Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                            int intStart = this.uGridSampling.Rows[intIndex].Cells["1"].Column.Index;
                            //// Xn컬럼의 마지막 컬럼 Index저장
                            //int intLastIndex = (Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                            //                    Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value)) + intStart;
                            // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                            int intSampleSize = (Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["ProcessSampleSize"].Value) *
                                                Convert.ToInt32(this.uGridSampling.Rows[intIndex].Cells["SampleSize"].Value));
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                this.uGridSampling.Rows[intIndex].Cells[j].Value = "OK";
                            }
                        }

                        this.uGridSampling.Rows[intIndex].Cells["InspectResultFlag"].Value = "OK";
                        this.uGridSampling.EventManager.AllEventsEnabled = true;
                    }
                }
                else if (this.uGridSampling.ActiveCell.Column.Key.Equals("ProcessSampleSize"))
                {
                    ////if (this.uGridSampling.ActiveCell.Value.Equals(0))
                    ////{
                    int intIndex = this.uGridSampling.ActiveCell.Row.Index;
                    if (!intIndex.Equals(-1))
                        this.uGridSampling.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                    ////}
                }
                else if (this.uGridSampling.ActiveCell.Column.Header.Caption.Contains("X") && !this.uGridSampling.ActiveCell.Column.Header.Caption.Contains("MAX"))
                {
                    int intIndex = this.uGridSampling.ActiveCell.Row.Index;
                    if (!intIndex.Equals(-1))
                        this.uGridSampling.Rows[intIndex].Cells["InspectIngFlag"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Sampling 그리드 초기화 이벤트
        private void uGridSampling_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                    //cellError = "InspectResultFlag";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경
                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = Convert.ToInt32(e.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량유형 그리드 검사결과 불량인경우만 그리드에 보이고 아닌경우는 히든처리하는 이벤트
        private void uGridFault_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (!e.Row.Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                {
                    e.Row.Hidden = true;
                    e.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Row.Hidden = false;
                    e.Row.Appearance.BackColor = Color.Salmon;
                }
                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridItem_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["InspectIngFlag"].Value.Equals("True"))
                {
                    e.Row.Hidden = false;
                }
                else
                {
                    e.Row.Hidden = true;
                }

                if (!e.Row.Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                {
                    e.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Row.Appearance.BackColor = Color.Salmon;
                }

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량유형그리드 더블클릭시 상세 그리드
        private void uGridFault_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                e.Row.Selected = true;

                // 그리드에 내용이 있는경우
                if (this.uGridFaultData.Rows.Count > 0)
                {
                    // 헤더정보가 저장된 상황이면 FaultData 만 저장
                    if (this.uTextReqNo.Text != "" && this.uTextReqSeq.Text != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                        QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                        brwChannel.mfCredentials(clsFault);

                        DataTable dtFault = Rtn_FaultDataTable();

                        string strErrRtn = clsFault.mfSaveINSProcInspectResultFault_S(dtFault, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            //입력한 정보를 저장하지 못했습니다.
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else
                        {
                            string strReqNo = ErrRtn.mfGetReturnValue(0);
                            string strReqSeq = ErrRtn.mfGetReturnValue(1);

                            // 첨부파일 업로드 메소드 호출
                            FileUpload(strReqNo, strReqSeq);
                        }
                    }
                    else
                    {
                        DialogResult Result = new DialogResult();
                        WinMessageBox msg = new WinMessageBox();

                        // 필수입력사항 확인
                        if (this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                            this.uComboPlantCode.DropDown();
                            return;
                        }
                        else if (this.uComboProcInspectType.Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M000287", Infragistics.Win.HAlign.Center);

                            this.uComboProcInspectType.DropDown();
                            return;
                        }
                        else if (this.uTextLotNo.Text.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M000078", Infragistics.Win.HAlign.Center);

                            this.uTextLotNo.Focus();
                            return;
                        }
                        //else if (this.uTextEquipCode.Text.Equals("-") || this.uTextEquipCode.Text.Equals(string.Empty))
                        else if (this.uComboEquip.Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000070", "M000071", Infragistics.Win.HAlign.Center);

                            return;
                        }
                        else if (this.uComboEquip.Value.ToString().Equals("-"))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000070", "M000702", Infragistics.Win.HAlign.Center);

                            return;
                        }
                        else
                        {
                            // 저장정보 데이터 테이블 설정
                            DataTable dtHeader = Rtn_HeaderDataTable();
                            DataTable dtLot = Rtn_LotDataTable();
                            DataTable dtItem = Rtn_ItemDataTable();
                            DataSet dsResult = Rtn_ResultDataSet();
                            DataTable dtFault = Rtn_FaultDataTable();
                            DataTable dtCycle = Rtn_CycleDataTable();

                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                            QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                            brwChannel.mfCredentials(clsHeader);

                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // Method 호출
                            string strErrRtn = clsHeader.mfSaveINSProcInspectReqH_S(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                                                                                , dtLot, dtItem, dsResult, dtFault, dtCycle);

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // 처리결과에 따른 메세지 박스
                            if (ErrRtn.ErrNum != 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strReqNo = ErrRtn.mfGetReturnValue(0);
                                string strReqSeq = ErrRtn.mfGetReturnValue(1);
                                this.uTextReqNo.Text = strReqNo;
                                this.uTextReqSeq.Text = strReqSeq;

                                // 첨부파일 업로드 메소드 호출
                                FileUpload(strReqNo, strReqSeq);

                                //Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                // BL 연결
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                                brwChannel.mfCredentials(clsLot);

                                // 메소드호출

                                dtLot = clsLot.mfReadINSProcInspectReqLot(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                                // 컨트롤에 값 적용
                                //this.uComboPlantCode.Value = dtLot.Rows[0]["PlantCode"].ToString();
                                //this.uTextCustomerCode.Text = dtLot.Rows[0]["CustomerCode"].ToString();
                                //this.uTextCustomerName.Text = dtLot.Rows[0]["CustomerName"].ToString();
                                //this.uComboProcInspectType.Value = dtLot.Rows[0]["ProcInspectType"].ToString();
                                //EquipCode = dtLot.Rows[0]["EquipCode"].ToString();
                                //this.uTextEquipCode.Text = dtLot.Rows[0]["EquipCode"].ToString();
                                //this.uTextEquipName.Text = dtLot.Rows[0]["EquipName"].ToString();
                                //this.uTextProductCode.Text = dtLot.Rows[0]["ProductCode"].ToString();
                                //this.uTextProductName.Text = dtLot.Rows[0]["ProductName"].ToString();
                                //this.uTextLotNo.Text = dtLot.Rows[0]["LotNo"].ToString();
                                //this.uTextLotSize.Text = dtLot.Rows[0]["LotSize"].ToString();
                                //this.uTextInspectUserID.Text = dtLot.Rows[0]["InspectUserID"].ToString();
                                //this.uTextInspectUserName.Text = dtLot.Rows[0]["InspectUserName"].ToString();
                                //this.uTextEtcDesc.Text = dtLot.Rows[0]["EtcDesc"].ToString();
                                //this.uDateInspectDate.Value = Convert.ToDateTime(dtLot.Rows[0]["InspectDate"]).ToString("yyyy-MM-dd");
                                //this.uDateInspectTime.Value = Convert.ToDateTime(dtLot.Rows[0]["InspectTime"]).ToString("HH:mm:ss");
                                //if (dtLot.Rows[0]["PassFailFlag"].ToString() != "")
                                //    this.uOptionPassFailFlag.Value = dtLot.Rows[0]["PassFailFlag"].ToString();
                                ////this.uCheckCompleteFlag.Enabled = !Convert.ToBoolean(dtLot.Rows[0]["CompleteFlag"]);
                                //this.uCheckCompleteFlag.Checked = Convert.ToBoolean(dtLot.Rows[0]["CompleteFlag"]);
                                //this.uTextCompleteCheck.Text = dtLot.Rows[0]["CompleteFlag"].ToString();
                                //this.uComboStack.Value = dtLot.Rows[0]["StackSeq"].ToString();
                                //this.uTextCustomerProductCode.Text = dtLot.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                                //this.uTextPackage.Text = dtLot.Rows[0]["PACKAGE"].ToString();
                                //this.uTextNowProcessCode.Text = dtLot.Rows[0]["NowProcessCode"].ToString();
                                //this.uTextWorkProcessCode.Text = dtLot.Rows[0]["WorkProcessCode"].ToString();
                                //this.uTextWorkProcessName.Text = dtLot.Rows[0]["WorkProcessName"].ToString();
                                //this.uTextWorkUserID.Text = dtLot.Rows[0]["WorkUserID"].ToString();
                                //this.uTextWorkUserName.Text = dtLot.Rows[0]["WorkUserName"].ToString();

                                // TrackIn Button 활성화 여부
                                // Lot 상태가 RUN 이면 TrackIn 처리한 상태??
                                if (this.uTextLotProcessState.Text.Equals("RUN"))
                                {
                                    this.uButtonTrackIn.Enabled = false;
                                    this.uTextMESTrackInTCheck.Text = dtLot.Rows[0]["TrackInFlag"].ToString();
                                    //this.uCheckLossCheckFlag.Visible = true;
                                    //this.uNumLossQty.Visible = true;
                                }
                                else if (this.uTextLotProcessState.Text.Equals("WAIT"))
                                {
                                    // TrackIn Button 활성화 여부
                                    if (dtLot.Rows[0]["MESTrackInTFlag"].ToString().Equals("F") && dtLot.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                                    {
                                        this.uButtonTrackIn.Enabled = true;
                                        this.uTextMESTrackInTCheck.Text = "T";
                                    }
                                    else
                                    {
                                        this.uButtonTrackIn.Enabled = false;
                                        this.uTextMESTrackInTCheck.Text = dtLot.Rows[0]["TrackInFlag"].ToString();
                                       // this.uCheckLossCheckFlag.Visible = true;
                                        this.uNumLossQty.Visible = true;
                                        this.uLabelLossCheck.Visible = true;
                                        this.uLabelLossQty.Visible = true;
                                    }
                                }

                                this.uTextMESTrackInTFlag.Text = dtLot.Rows[0]["MESTrackInTFlag"].ToString();
                                this.uTextMESTrackOutTFlag.Text = dtLot.Rows[0]["MESTrackOutTFlag"].ToString();
                                this.uTextMESHoldTFlag.Text = dtLot.Rows[0]["MESHoldTFlag"].ToString();
                                this.uTextMESSPCNTFlag.Text = dtLot.Rows[0]["MESSPCNTFlag"].ToString();

                                //// 작성완료 상태면 체크박스 Enable로

                                //if (this.uCheckCompleteFlag.Checked)
                                //    this.uCheckCompleteFlag.Enabled = false;
                                //else
                                //    this.uCheckCompleteFlag.Enabled = true;

                                // 필수입력사항 Enable 로

                                this.uComboPlantCode.Enabled = false;
                                this.uComboProcInspectType.Enabled = false;
                                this.uTextLotNo.Enabled = false;
                            }
                        }
                    }
                }

                // 검사결과가 불량이고 TrackIn이 진행된경우 경우만 작성가능
                if (e.Row.Cells["InspectResultFlag"].Value.ToString().Equals("NG") && this.uButtonTrackIn.Enabled.Equals(false))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                    QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                    brwChannel.mfCredentials(clsFault);

                    // DropDown 설정
                    WinGrid wGrid = new WinGrid();

                    // 불량유형
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsFType = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsFType);
                    //DataTable dtFType = clsFType.mfReadInspectFaultTypeCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    // 불량명 공정별로 묶어서 출력
                    //DataTable dtFType = new DataTable();
                    //if (this.uTextMESTrackInTCheck.Text.Equals("T") || this.uTextMESTrackInTFlag.Text.Equals("T"))
                    //{
                    //    dtFType = clsFType.mfReadInspectFaultTypeCombo_ProcessGroup(this.uComboPlantCode.Value.ToString(), this.uTextNowProcessCode.Text, m_resSys.GetString("SYS_LANG"));
                    //}
                    //else
                    //{
                    //    dtFType = clsFType.mfReadInspectFaultTypeCombo_ProcessGroup(this.uComboPlantCode.Value.ToString(), this.uTextWorkProcessCode.Text, m_resSys.GetString("SYS_LANG"));
                    //}

                    // TrackIn 여부 상관없이 작업공정으로
                    DataTable dtFType = new DataTable();
                    if (this.uTextCustomerCode.Text.Equals("HYNIX") || this.uTextCustomerName.Text.Equals("HYNIX"))         // HYNIX 제품일경우 불량명 다르게 설정
                        dtFType = clsFType.mfReadInspectFaultTypeCombo_WithInspectFaultGubun(this.uComboPlantCode.Value.ToString(), "3", m_resSys.GetString("SYS_LANG"));
                    else
                        dtFType = clsFType.mfReadInspectFaultTypeCombo_ProcessGroup(this.uComboPlantCode.Value.ToString(), this.uComboWorkProcess.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    //wGrid.mfSetGridColumnValueGridList(this.uGridFaultData, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    //    , "InspectFaultTypeCode,InspectFaultTypeName", "불량코드,불량명", "InspectFaultTypeCode", "InspectFaultTypeName", dtFType);
                    wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFType);

                    // 예상공정
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    DataTable dtProcess = clsProcess.mfReadProcessForCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "ExpectProcessCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcess);

                    // 원인설비
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    DataTable dtEquip = clsEquip.mfReadEquipForPlantCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "CauseEquipCode", Infragistics.Win.ValueListDisplayStyle.DataValue, "", "선택", dtEquip);

                    DataTable dtRtnFault = clsFault.mfReadINSProcInspectResultFault(this.uComboPlantCode.Value.ToString(), this.uTextReqNo.Text, this.uTextReqSeq.Text
                                                                        , 1, Convert.ToInt32(e.Row.Cells["ReqItemSeq"].Value));

                    // 작업자 ID 설정 (TrackIn 공정이 아닌경우만 작업자로 기본값 설정)
                    if (!this.uTextMESTrackInTFlag.Text.Equals("T") && !this.uTextMESTrackInTCheck.Text.Equals("T"))
                    {
                        this.uGridFaultData.DisplayLayout.Bands[0].Columns["WorkUserID"].DefaultCellValue = this.uTextWorkUserID.Text;
                        this.uGridFaultData.DisplayLayout.Bands[0].Columns["WorkUserName"].DefaultCellValue = this.uTextWorkUserName.Text;
                    }

                    this.uGridFaultData.DataSource = dtRtnFault;
                    this.uGridFaultData.DataBind();

                    // 그리드 입력가능하게 변경
                    this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    // PK 키값 설정
                    //this.uGridFaultData.DisplayLayout.Bands[0].Columns["ReqLotSeq"].DefaultCellValue = e.Row.Cells["ReqLotSeq"].Value.ToString();
                    this.uGridFaultData.DisplayLayout.Bands[0].Columns["ReqItemSeq"].DefaultCellValue = e.Row.Cells["ReqItemSeq"].Value.ToString();
                    this.uGridFaultData.DisplayLayout.Bands[0].Columns["InspectQty"].DefaultCellValue = e.Row.Cells["ProcessSampleSize"].Value.ToString();
                    this.uGridFaultData.DisplayLayout.Bands[0].Columns["CheckFaultQty"].DefaultCellValue = e.Row.Cells["FaultQty"].Value.ToString();
                }
                else
                {
                    while (this.uGridFaultData.Rows.Count > 0)
                    {
                        this.uGridFaultData.Rows[0].Delete(false);
                    }

                    // 그리드 입력불가능하게 변경
                    this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 불량 데이터 그리드
        // 불량 검사데이터 그리드 작업자 ID 셀버튼 클릭시 이벤트
        private void uGridFaultData_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (e.Cell.Column.Key == "WorkUserID")
                {
                    if (this.uComboPlantCode.Value.ToString() != "")
                    {
                        frmPOP0011 frmPOP = new frmPOP0011();
                        frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                        frmPOP.ShowDialog();

                        if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                        {
                            // SystemInfo ResourceSet
                            DialogResult Result = new DialogResult();
                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                        , msg.GetMessge_Text("M001254", strLang) + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Cells["WorkUserID"].Value = "";
                            e.Cell.Row.Cells["WorkUserName"].Value = "";
                        }
                        else
                        {
                            e.Cell.Row.Cells["WorkUserID"].Value = frmPOP.UserID;
                            e.Cell.Row.Cells["WorkUserName"].Value = frmPOP.UserName;
                        }
                    }
                    else
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M000798", "M000275", "M000266"
                                                    , Infragistics.Win.HAlign.Right);

                        this.uComboPlantCode.DropDown();
                    }
                }
                else if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        e.Cell.Value = openFile.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작업자 ID 컬럼에서 사번입력하고 엔터버튼 클릭시 이름 가져오는 이벤트
        private void uGridFaultData_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridFaultData.ActiveCell == null)
                    return;

                if (this.uGridFaultData.ActiveCell.Column.Key == "WorkUserID")
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uGridFaultData.ActiveCell.Text != "")
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            if (this.uComboPlantCode.Value.ToString() != "")
                            {
                                String strWriteID = this.uGridFaultData.ActiveCell.Text;

                                // UserName 검색 함수 호출
                                DataTable dtUserInfo = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                                if (dtUserInfo.Rows.Count > 0)
                                {
                                    this.uGridFaultData.ActiveCell.Row.Cells["WorkUserName"].Value = dtUserInfo.Rows[0]["UserName"].ToString();

                                    //this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode
                                    //                                | Infragistics.Win.UltraWinGrid.UltraGridAction.BelowCell);
                                }
                                else
                                {
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000621",
                                                Infragistics.Win.HAlign.Right);

                                    this.uGridFaultData.ActiveCell.Row.Cells["WorkUserName"].Value = "";
                                    this.uGridFaultData.ActiveCell.Value = "";
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000266",
                                                Infragistics.Win.HAlign.Right);

                                this.uComboPlantCode.Focus();
                                this.uComboPlantCode.DropDown();
                            }
                        }
                    }

                    if (e.KeyCode == Keys.Back)
                    {
                        this.uGridFaultData.ActiveCell.Value = "";
                        this.uGridFaultData.ActiveCell.Row.Cells["WorkUserName"].Value = "";
                    }
                }
                //else
                //{
                //    int intRowIndex = this.uGridFaultData.ActiveCell.Row.Index;
                //    if (e.KeyCode == Keys.Up)
                //    {
                //        if (intRowIndex - 1 >= 0)
                //        {
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.AboveRow);
                //            if(this.uGridFaultData.ActiveCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown)
                //                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                //            else
                //                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                //        }
                //    }
                //    else if (e.KeyCode == Keys.Down)
                //    {
                //        if (intRowIndex < this.uGridFaultData.Rows.Count)
                //        {
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.BelowRow);
                //            if (this.uGridFaultData.ActiveCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown)
                //                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                //            else
                //                this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                //        }
                //    }
                //    else if (e.KeyCode == Keys.Left)
                //    {
                //        this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                //        this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.AboveCell);
                //        if (this.uGridFaultData.ActiveCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown)
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                //        else
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                //    }
                //    else if (e.KeyCode == Keys.Right)
                //    {
                //        this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                //        this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.BelowCell);
                //        if (this.uGridFaultData.ActiveCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown)
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                //        else
                //            this.uGridFaultData.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                //    }
                //}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량유형 상세 그리드 셀 업데이트시 이벤트들
        private void uGridFaultData_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                // 예상공정 선택시 설비, 작업자ID, 작업자 명 가져오는 MES I/F
                if (e.Cell.Column.Key.Equals("ExpectProcessCode"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        if (this.uButtonTrackIn.Enabled.Equals(false))
                        {
                            ////// 화일서버 연결정보 가져오기
                            ////QRPBrowser brwChannel = new QRPBrowser();
                            ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            ////brwChannel.mfCredentials(clsSysAccess);

                            ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.MESCodeReturn), "MESCodeReturn");
                            ////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                            ////brwChannel.mfCredentials(clsMESCode);

                            ////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);
                            //////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
                            //////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");

                            ////// MES I/F
                            ////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);
                            ////DataTable dtEquipInfo = clsTibrv.LOT_PROCESSED_EQP_REQ(this.uTextLotNo.Text.Trim(), e.Cell.Value.ToString());
                            //////clsTibrv.Dispose();

                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                            QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                            brwChannel.mfCredentials(clsMES);

                            DataTable dtEquipInfo = clsMES.mfRead_LOT_PROCESSED_EQP_REQ(this.uComboPlantCode.Value.ToString()
                                                                                        , this.uTextLotNo.Text.Trim().ToUpper()
                                                                                        , e.Cell.Value.ToString());

                            if (dtEquipInfo.Rows.Count > 0)
                            {
                                if (dtEquipInfo.Rows[0]["returncode"].ToString().Equals("0"))
                                {
                                    e.Cell.Row.Cells["CauseEquipCode"].Value = dtEquipInfo.Rows[0]["EQPID"].ToString();
                                    e.Cell.Row.Cells["WorkUserID"].Value = dtEquipInfo.Rows[0]["USERID"].ToString();
                                    e.Cell.Row.Cells["WorkUserName"].Value = dtEquipInfo.Rows[0]["USERNAME"].ToString();
                                }
                                else
                                {
                                    // SystemInfo ResourceSet
                                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                    WinMessageBox msg = new WinMessageBox();
                                    DialogResult Result = new DialogResult();
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M000089", strLang), msg.GetMessge_Text("M000082", strLang)
                                        , dtEquipInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);

                                    e.Cell.Value = "";
                                }
                            }
                            else
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M000089", "M000082", "M000377", Infragistics.Win.HAlign.Right);

                                e.Cell.Value = "";
                            }
                        }
                    }
                }
                // 불량유형 선택시 기준정보에서 QCNFlag 가져오는 부분
                else if (e.Cell.Column.Key.Equals("FaultTypeCode"))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsFT);

                    DataTable dtQCNFlag = clsFT.mfReadInspectFaultType_QCNFlag(this.uComboPlantCode.Value.ToString(), e.Cell.Value.ToString());

                    if (dtQCNFlag.Rows.Count > 0)
                    {
                        e.Cell.Row.Cells["QCNFlag"].Value = Convert.ToBoolean(dtQCNFlag.Rows[0]["QCNFlag"]);

                        if (dtQCNFlag.Rows[0]["QCNFlagCode"].ToString().Equals("I"))  // ITR 대상인 불량유형을 선택하였을 경우 ITRFlag대상여부체크
                            e.Cell.Row.Cells["P_ITRFlag"].Value = true;
                    }
                    else
                    { e.Cell.Row.Cells["QCNFlag"].Value = false; e.Cell.Row.Cells["P_ITRFlag"].Value = false; }

                }
                // 불량수량 입력시 검사수량보다 많으면 에러메세지 띄우는 부분
                else if (e.Cell.Column.Key.Equals("FaultQty"))
                {
                    if (!Convert.ToDecimal(e.Cell.Value).Equals(0))
                    {
                        if (Convert.ToDecimal(e.Cell.Value) > Convert.ToDecimal(e.Cell.Row.Cells["CheckFaultQty"].Value))
                        {
                            // SystemInfo ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            if (msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M000962", "M000611", "M000609", Infragistics.Win.HAlign.Right) == DialogResult.OK)
                            {
                                e.Cell.CancelUpdate();
                                //e.Cell.Value = 0;
                                e.Cell.Activate();
                            }
                        }
                    }
                }
                // ITR or QCN 중 하나만 체크 가능하도록
                else if (e.Cell.Column.Key.Equals("QCNFlag"))
                {
                    this.uGridFaultData.UpdateData();
                    DataTable dtFault = (DataTable)this.uGridFaultData.DataSource;

                    var query = from check in dtFault.AsEnumerable()
                                select new
                                {
                                    QCNFlag = Convert.ToBoolean(check.Field<string>("QCNFlag")),
                                    ITRFlag = Convert.ToBoolean(check.Field<string>("P_ITRFlag"))
                                };

                    bool bolQCN = false;
                    bool bolITR = false;

                    foreach (var result in query)
                    {
                        if (result.QCNFlag)
                            bolQCN = result.QCNFlag;
                        if (result.ITRFlag)
                            bolITR = result.ITRFlag;
                    }

                    if (Convert.ToBoolean(e.Cell.Value))
                    {
                        if (bolQCN && !bolITR)
                            this.uTextQCNorITRCheck.Text = "QCN";
                        else if (!bolQCN && !bolITR)
                            this.uTextQCNorITRCheck.Clear();
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000882", "M001486", Infragistics.Win.HAlign.Right);


                            e.Cell.Value = false;
                        }
                    }
                    else
                    {
                        if (!bolQCN && !bolITR)
                            this.uTextQCNorITRCheck.Clear();
                    }
                }
                else if (e.Cell.Column.Key.Equals("P_ITRFlag"))
                {
                    this.uGridFaultData.UpdateData();
                    DataTable dtFault = (DataTable)this.uGridFaultData.DataSource;

                    var query = from check in dtFault.AsEnumerable()
                                select new
                                {
                                    QCNFlag = Convert.ToBoolean(check.Field<string>("QCNFlag")),
                                    ITRFlag = Convert.ToBoolean(check.Field<string>("P_ITRFlag"))
                                };

                    bool bolQCN = false;
                    bool bolITR = false;

                    foreach (var result in query)
                    {
                        if (result.QCNFlag)
                            bolQCN = result.QCNFlag;
                        if (result.ITRFlag)
                            bolITR = result.ITRFlag;
                    }

                    if (Convert.ToBoolean(e.Cell.Value))
                    {
                        if (!bolQCN && bolITR)
                            this.uTextQCNorITRCheck.Text = "ITR";
                        else if (!bolQCN && !bolITR)
                            this.uTextQCNorITRCheck.Clear();
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000882", "M001486", Infragistics.Win.HAlign.Right);


                            e.Cell.Value = false;
                        }
                    }
                    else
                    {
                        if (!bolQCN && !bolITR)
                            this.uTextQCNorITRCheck.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // TrackIn 이 진행되어야 하는 상황에서 설비정보 요청 방지
        private void uGridFaultData_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("ExpectProcessCode"))
                {
                    if (this.uButtonTrackIn.Enabled.Equals(true))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M000089", "M000714", "M000155", Infragistics.Win.HAlign.Right);

                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 샘플링검사 그리드 Key 이벤트 처리

        #region KeyDown 이벤트로 변경에 따른 주석처리
        //샘플링검사 엔터치면 바로 아래로 이동하도록 처리
        //private void uGridSampling_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //try
        //    //{
        //    //    Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridSampling == null ? null : uGridSampling.ActiveCell;
        //    //    if (activeCell == null) return;

        //    //    int intRowIndex = uGridSampling.ActiveCell.Row.Index;

        //    //    //검사수 입력하고 엔터누르면 불량수 란으로 이동
        //    //    if (activeCell.Column.Key.Equals("ProcessSampleSize"))
        //    //    {
        //    //        if (e.KeyCode == Keys.Enter)
        //    //        {
        //    //            Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[activeCell.Row.Index].Cells["FaultQty"];
        //    //            this.uGridSampling.ActiveCell = nextCell;
        //    //            this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        //    //        }
        //    //    }

        //    //    //불량수 입력하면 X1으로 이동
        //    //    else if (activeCell.Column.Key.Equals("FaultQty"))
        //    //    {
        //    //        if (e.KeyCode == Keys.Enter)
        //    //        {
        //    //            Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[activeCell.Row.Index].Cells["1"];
        //    //            this.uGridSampling.ActiveCell = nextCell;
        //    //            this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        //    //            if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown)
        //    //                this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
        //    //            else
        //    //                this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        //    //        }
        //    //    }

        //    //    //X1~Xn 셀 입력하면 옆란 이동 또는 아래란 이동.
        //    //    else if (activeCell.Column.Header.Caption.Contains("X"))
        //    //    {
        //    //        if (e.KeyCode == Keys.Enter)
        //    //        {
        //    //            //엔터를 치면 다음 셀로 이동
        //    //            int intNextCellKey = Convert.ToInt32(activeCell.Column.Key) + 1;
        //    //            string strNextCellKey = intNextCellKey.ToString();

        //    //            //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
        //    //            int intSampleSize = (Convert.ToInt32(uGridSampling.Rows[intRowIndex].Cells["ProcessSampleSize"].Value) * Convert.ToInt32(uGridSampling.Rows[intRowIndex].Cells["SampleSize"].Value));
        //    //            if (intSampleSize > 99)
        //    //                intSampleSize = 99;
        //    //            if (intNextCellKey > intSampleSize)
        //    //            {
        //    //                intRowIndex = intRowIndex + 1;
        //    //                //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
        //    //                if (intRowIndex > this.uGridSampling.Rows.Count - 1)
        //    //                    intRowIndex = 0;
        //    //                intNextCellKey = 1;
        //    //                strNextCellKey = "ProcessSampleSize";
        //    //            }

        //    //            //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
        //    //            Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[strNextCellKey];

        //    //            //다음 셀이 편집불가면 다음 행 검사수로 이동처리
        //    //            if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
        //    //            {
        //    //                if (this.uGridSampling.ActiveCell.Row.Index < this.uGridSampling.Rows.Count - 1)
        //    //                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
        //    //                    nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
        //    //                else
        //    //                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
        //    //                    nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
        //    //            }

        //    //            //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
        //    //            this.uGridSampling.ActiveCell = nextCell;
        //    //            if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown)
        //    //                this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
        //    //            else
        //    //                this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        //    //        }
        //    //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //    //    frmErr.ShowDialog();
        //    //}
        //    //finally
        //    //{
        //    //}
        //}
        #endregion

        private void uGridSampling_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridSampling == null ? null : uGridSampling.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGridSampling.ActiveCell.Row.Index;

                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                if (activeCell.Column.Key.Equals("ProcessSampleSize"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGridSampling.ActiveCell = nextCell;
                        this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGridSampling.ActiveCell = nextCell;
                        this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //X1~Xn 셀 입력하면 옆란 이동 또는 아래란 이동.
                else if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = Convert.ToInt32(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        int intSampleSize = (Convert.ToInt32(uGridSampling.Rows[intRowIndex].Cells["ProcessSampleSize"].Value) * Convert.ToInt32(uGridSampling.Rows[intRowIndex].Cells["SampleSize"].Value));
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGridSampling.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGridSampling.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "ProcessSampleSize";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridSampling.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            if (this.uGridSampling.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGridSampling.ActiveCell.Row.Index < this.uGridSampling.Rows.Count - 1)
                                    nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                //nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                            else
                            {
                                if (this.uGridSampling.ActiveCell.Row.Index < this.uGridSampling.Rows.Count - 1)
                                    //nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["1"];
                                    nextCell = this.uGridSampling.Rows[intRowIndex + 1].Cells["ProcessSampleSize"];
                                else
                                    //nextCell = this.uGridSampling.Rows[0].Cells["1"];
                                    nextCell = this.uGridSampling.Rows[0].Cells["ProcessSampleSize"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGridSampling.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uTextFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextFile.Text = strImageFile;
                    }
                }
                //else if (e.Button.Key == "Down")
                //{
                //    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                //    //화일서버 연결정보 가져오기
                //    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                //    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //    brwChannel.mfCredentials(clsSysAccess);
                //    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                //    //첨부파일 저장경로정보 가져오기
                //    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                //    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                //    brwChannel.mfCredentials(clsSysFilePath);
                //    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0042");

                //    //첨부파일 Download하기
                //    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                //    ArrayList arrFile = new ArrayList();
                //    arrFile.Add(this.uTextFile.Text);

                //    //Download정보 설정
                //    string strExePath = Application.ExecutablePath;
                //    int intPos = strExePath.LastIndexOf(@"\");
                //    strExePath = strExePath.Substring(0, intPos + 1);

                //    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                //                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                //                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                //                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                //                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                //                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                //    fileAtt.mfFileDownloadNoProgView();

                //    // 파일 실행시키기
                //    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFile.Text);
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //ftp的上传功能   
        public void FileUpload_Header_Ftp(string userId, string pwd, string filename, string ftpPath)
        {
            FileInfo fileInf = new FileInfo(filename);
            FtpWebRequest reqFTP;
            // 根据uri创建FtpWebRequest对象   
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpPath + fileInf.Name));
            // ftp用户名和密码  
            reqFTP.Credentials = new NetworkCredential(userId, pwd);

            reqFTP.UsePassive = false;
            // 默认为true，连接不会被关闭  
            // 在一个命令之后被执行  
            reqFTP.KeepAlive = false;
            // 指定执行什么命令  
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
            // 指定数据传输类型  
            reqFTP.UseBinary = true;
            // 上传文件时通知服务器文件的大小  
            reqFTP.ContentLength = fileInf.Length;
            // 缓冲大小设置为2kb  
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;
            // 打开一个文件流 (System.IO.FileStream) 去读上传的文件  
            FileStream fs = fileInf.OpenRead();
            try
            {
                // 把上传的文件写入流  
                Stream strm = reqFTP.GetRequestStream();
                // 每次读文件流的2kb  
                contentLen = fs.Read(buff, 0, buffLength);
                // 流内容没有结束  
                while (contentLen != 0)
                {
                    // 把内容从file stream 写入 upload stream  
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }
                // 关闭两个流  
                strm.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
        }

        private void FileUpload_Header()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                if (this.uTextFile.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextFile.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" + strfileName;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextFile.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }
                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0042");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion
    }
}