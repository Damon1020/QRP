﻿namespace QRPINS.UI
{
    partial class frmINSZ0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0005));
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchMaterialType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMaterialType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterialGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMaterialGrade = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridAMLHistoryList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDel = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextDrawing_No = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDrawing_No = new Infragistics.Win.Misc.UltraLabel();
            this.uNumShipCount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uCheckHistoryInfo = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelHistoryInfo = new Infragistics.Win.Misc.UltraLabel();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uCheckAQLFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelAQLFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectRate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInspectRate = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInspectLevel = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInspectLevel = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboMaterialGrade = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialGrade = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCustomerAdmitDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpecNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerAdmitDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSpecNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGridAMLList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAMLHistoryList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDrawing_No)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumShipCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckHistoryInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAQLFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCustomerAdmitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAMLList)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterialType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialType);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterialGrade);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uComboSearchMaterialType
            // 
            this.uComboSearchMaterialType.Location = new System.Drawing.Point(692, 12);
            this.uComboSearchMaterialType.Name = "uComboSearchMaterialType";
            this.uComboSearchMaterialType.Size = new System.Drawing.Size(152, 21);
            this.uComboSearchMaterialType.TabIndex = 237;
            // 
            // uLabelSearchMaterialType
            // 
            this.uLabelSearchMaterialType.Location = new System.Drawing.Point(588, 12);
            this.uLabelSearchMaterialType.Name = "uLabelSearchMaterialType";
            this.uLabelSearchMaterialType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialType.TabIndex = 236;
            this.uLabelSearchMaterialType.Text = "자재유형";
            // 
            // uTextSearchMaterialCode
            // 
            appearance23.BackColor = System.Drawing.Color.White;
            this.uTextSearchMaterialCode.Appearance = appearance23;
            this.uTextSearchMaterialCode.BackColor = System.Drawing.Color.White;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(470, 12);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialCode.TabIndex = 235;
            // 
            // uLabelSearchMaterialCode
            // 
            this.uLabelSearchMaterialCode.Location = new System.Drawing.Point(356, 12);
            this.uLabelSearchMaterialCode.Name = "uLabelSearchMaterialCode";
            this.uLabelSearchMaterialCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchMaterialCode.TabIndex = 234;
            this.uLabelSearchMaterialCode.Text = "자재코드";
            // 
            // uDateSearchToWriteDate
            // 
            this.uDateSearchToWriteDate.Location = new System.Drawing.Point(1011, 4);
            this.uDateSearchToWriteDate.Name = "uDateSearchToWriteDate";
            this.uDateSearchToWriteDate.Size = new System.Drawing.Size(20, 21);
            this.uDateSearchToWriteDate.TabIndex = 233;
            this.uDateSearchToWriteDate.Visible = false;
            // 
            // uDateSearchFromWriteDate
            // 
            this.uDateSearchFromWriteDate.Location = new System.Drawing.Point(996, 4);
            this.uDateSearchFromWriteDate.Name = "uDateSearchFromWriteDate";
            this.uDateSearchFromWriteDate.Size = new System.Drawing.Size(20, 21);
            this.uDateSearchFromWriteDate.TabIndex = 233;
            this.uDateSearchFromWriteDate.Visible = false;
            // 
            // uTextSearchVendorName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance21;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(237, 12);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorName.TabIndex = 232;
            // 
            // uTextSearchVendorCode
            // 
            appearance16.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance16.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance16;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(124, 12);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorCode.TabIndex = 232;
            // 
            // uLabelSearchMaterialGrade
            // 
            this.uLabelSearchMaterialGrade.Location = new System.Drawing.Point(852, 12);
            this.uLabelSearchMaterialGrade.Name = "uLabelSearchMaterialGrade";
            this.uLabelSearchMaterialGrade.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchMaterialGrade.TabIndex = 229;
            this.uLabelSearchMaterialGrade.Text = "3";
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(8, 12);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchVendor.TabIndex = 227;
            this.uLabelSearchVendor.Text = "1";
            // 
            // uComboSearchMaterialGrade
            // 
            this.uComboSearchMaterialGrade.Location = new System.Drawing.Point(968, 12);
            this.uComboSearchMaterialGrade.Name = "uComboSearchMaterialGrade";
            this.uComboSearchMaterialGrade.Size = new System.Drawing.Size(84, 21);
            this.uComboSearchMaterialGrade.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1048, 4);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1036, 4);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(8, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 2;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 680);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 680);
            this.uGroupBoxContentsArea.TabIndex = 5;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 660);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGridAMLHistoryList);
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox3);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 132);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1064, 528);
            this.ultraGroupBox1.TabIndex = 238;
            // 
            // uGridAMLHistoryList
            // 
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAMLHistoryList.DisplayLayout.Appearance = appearance31;
            this.uGridAMLHistoryList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAMLHistoryList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAMLHistoryList.DisplayLayout.GroupByBox.Appearance = appearance28;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAMLHistoryList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance29;
            this.uGridAMLHistoryList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance30.BackColor2 = System.Drawing.SystemColors.Control;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAMLHistoryList.DisplayLayout.GroupByBox.PromptAppearance = appearance30;
            this.uGridAMLHistoryList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAMLHistoryList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAMLHistoryList.DisplayLayout.Override.ActiveCellAppearance = appearance39;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAMLHistoryList.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.uGridAMLHistoryList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAMLHistoryList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAMLHistoryList.DisplayLayout.Override.CardAreaAppearance = appearance33;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAMLHistoryList.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGridAMLHistoryList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAMLHistoryList.DisplayLayout.Override.CellPadding = 0;
            appearance36.BackColor = System.Drawing.SystemColors.Control;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAMLHistoryList.DisplayLayout.Override.GroupByRowAppearance = appearance36;
            appearance38.TextHAlignAsString = "Left";
            this.uGridAMLHistoryList.DisplayLayout.Override.HeaderAppearance = appearance38;
            this.uGridAMLHistoryList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAMLHistoryList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            this.uGridAMLHistoryList.DisplayLayout.Override.RowAppearance = appearance37;
            this.uGridAMLHistoryList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAMLHistoryList.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridAMLHistoryList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAMLHistoryList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAMLHistoryList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAMLHistoryList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridAMLHistoryList.Location = new System.Drawing.Point(3, 40);
            this.uGridAMLHistoryList.Name = "uGridAMLHistoryList";
            this.uGridAMLHistoryList.Size = new System.Drawing.Size(1058, 485);
            this.uGridAMLHistoryList.TabIndex = 3;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.uButtonDel);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox3.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1058, 40);
            this.ultraGroupBox3.TabIndex = 2;
            // 
            // uButtonDel
            // 
            this.uButtonDel.Location = new System.Drawing.Point(8, 8);
            this.uButtonDel.Name = "uButtonDel";
            this.uButtonDel.Size = new System.Drawing.Size(88, 28);
            this.uButtonDel.TabIndex = 1;
            this.uButtonDel.Visible = false;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uTextDrawing_No);
            this.ultraGroupBox2.Controls.Add(this.uLabelDrawing_No);
            this.ultraGroupBox2.Controls.Add(this.uNumShipCount);
            this.ultraGroupBox2.Controls.Add(this.uCheckHistoryInfo);
            this.ultraGroupBox2.Controls.Add(this.uLabelHistoryInfo);
            this.ultraGroupBox2.Controls.Add(this.uDateWriteDate);
            this.ultraGroupBox2.Controls.Add(this.uCheckAQLFlag);
            this.ultraGroupBox2.Controls.Add(this.uLabelAQLFlag);
            this.ultraGroupBox2.Controls.Add(this.uTextMoldSeq);
            this.ultraGroupBox2.Controls.Add(this.uLabelMoldSeq);
            this.ultraGroupBox2.Controls.Add(this.uLabelInspectRate);
            this.ultraGroupBox2.Controls.Add(this.uComboInspectRate);
            this.ultraGroupBox2.Controls.Add(this.uLabelInspectLevel);
            this.ultraGroupBox2.Controls.Add(this.uComboInspectLevel);
            this.ultraGroupBox2.Controls.Add(this.uComboMaterialGrade);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorName);
            this.ultraGroupBox2.Controls.Add(this.uLabelMaterialGrade);
            this.ultraGroupBox2.Controls.Add(this.uLabelEtcDesc);
            this.ultraGroupBox2.Controls.Add(this.uDateCustomerAdmitDate);
            this.ultraGroupBox2.Controls.Add(this.uTextEtcDesc);
            this.ultraGroupBox2.Controls.Add(this.uTextSpecNo);
            this.ultraGroupBox2.Controls.Add(this.uTextSpec);
            this.ultraGroupBox2.Controls.Add(this.uLabelWriteDate);
            this.ultraGroupBox2.Controls.Add(this.uLabelWriteUser);
            this.ultraGroupBox2.Controls.Add(this.uTextWriteUserName);
            this.ultraGroupBox2.Controls.Add(this.uTextCustomerName);
            this.ultraGroupBox2.Controls.Add(this.uTextMaterialName);
            this.ultraGroupBox2.Controls.Add(this.uLabelCustomerAdmitDate);
            this.ultraGroupBox2.Controls.Add(this.uTextWriteUserID);
            this.ultraGroupBox2.Controls.Add(this.uTextCustomerCode);
            this.ultraGroupBox2.Controls.Add(this.uTextMaterialCode);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorCode);
            this.ultraGroupBox2.Controls.Add(this.uLabelCustomer);
            this.ultraGroupBox2.Controls.Add(this.uLabelPlant);
            this.ultraGroupBox2.Controls.Add(this.uLabelVendor);
            this.ultraGroupBox2.Controls.Add(this.uLabelSpec);
            this.ultraGroupBox2.Controls.Add(this.uLabelSpecNo);
            this.ultraGroupBox2.Controls.Add(this.uLabelMaterial);
            this.ultraGroupBox2.Controls.Add(this.uComboPlant);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1064, 132);
            this.ultraGroupBox2.TabIndex = 237;
            // 
            // uTextDrawing_No
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDrawing_No.Appearance = appearance25;
            this.uTextDrawing_No.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDrawing_No.Location = new System.Drawing.Point(708, 8);
            this.uTextDrawing_No.Name = "uTextDrawing_No";
            this.uTextDrawing_No.ReadOnly = true;
            this.uTextDrawing_No.Size = new System.Drawing.Size(148, 21);
            this.uTextDrawing_No.TabIndex = 294;
            // 
            // uLabelDrawing_No
            // 
            this.uLabelDrawing_No.Location = new System.Drawing.Point(592, 8);
            this.uLabelDrawing_No.Name = "uLabelDrawing_No";
            this.uLabelDrawing_No.Size = new System.Drawing.Size(110, 20);
            this.uLabelDrawing_No.TabIndex = 293;
            this.uLabelDrawing_No.Text = "12";
            // 
            // uNumShipCount
            // 
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Text = "0";
            this.uNumShipCount.ButtonsLeft.Add(editorButton2);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumShipCount.ButtonsRight.Add(spinEditorButton1);
            this.uNumShipCount.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumShipCount.Location = new System.Drawing.Point(976, 80);
            this.uNumShipCount.MaskInput = "nnnnn";
            this.uNumShipCount.Name = "uNumShipCount";
            this.uNumShipCount.PromptChar = ' ';
            this.uNumShipCount.Size = new System.Drawing.Size(68, 21);
            this.uNumShipCount.TabIndex = 292;
            // 
            // uCheckHistoryInfo
            // 
            this.uCheckHistoryInfo.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckHistoryInfo.Location = new System.Drawing.Point(952, 80);
            this.uCheckHistoryInfo.Name = "uCheckHistoryInfo";
            this.uCheckHistoryInfo.Size = new System.Drawing.Size(20, 20);
            this.uCheckHistoryInfo.TabIndex = 291;
            // 
            // uLabelHistoryInfo
            // 
            this.uLabelHistoryInfo.Location = new System.Drawing.Point(816, 80);
            this.uLabelHistoryInfo.Name = "uLabelHistoryInfo";
            this.uLabelHistoryInfo.Size = new System.Drawing.Size(130, 20);
            this.uLabelHistoryInfo.TabIndex = 290;
            this.uLabelHistoryInfo.Text = "3";
            // 
            // uDateWriteDate
            // 
            this.uDateWriteDate.Location = new System.Drawing.Point(124, 80);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDate.TabIndex = 289;
            // 
            // uCheckAQLFlag
            // 
            this.uCheckAQLFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckAQLFlag.Location = new System.Drawing.Point(952, 104);
            this.uCheckAQLFlag.Name = "uCheckAQLFlag";
            this.uCheckAQLFlag.Size = new System.Drawing.Size(20, 20);
            this.uCheckAQLFlag.TabIndex = 288;
            // 
            // uLabelAQLFlag
            // 
            this.uLabelAQLFlag.Location = new System.Drawing.Point(816, 104);
            this.uLabelAQLFlag.Name = "uLabelAQLFlag";
            this.uLabelAQLFlag.Size = new System.Drawing.Size(130, 20);
            this.uLabelAQLFlag.TabIndex = 287;
            this.uLabelAQLFlag.Text = "3";
            // 
            // uTextMoldSeq
            // 
            appearance45.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMoldSeq.Appearance = appearance45;
            this.uTextMoldSeq.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMoldSeq.Location = new System.Drawing.Point(348, 32);
            this.uTextMoldSeq.Name = "uTextMoldSeq";
            this.uTextMoldSeq.Size = new System.Drawing.Size(120, 21);
            this.uTextMoldSeq.TabIndex = 286;
            // 
            // uLabelMoldSeq
            // 
            this.uLabelMoldSeq.Location = new System.Drawing.Point(232, 32);
            this.uLabelMoldSeq.Name = "uLabelMoldSeq";
            this.uLabelMoldSeq.Size = new System.Drawing.Size(110, 20);
            this.uLabelMoldSeq.TabIndex = 285;
            this.uLabelMoldSeq.Text = "12";
            // 
            // uLabelInspectRate
            // 
            this.uLabelInspectRate.Location = new System.Drawing.Point(592, 80);
            this.uLabelInspectRate.Name = "uLabelInspectRate";
            this.uLabelInspectRate.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectRate.TabIndex = 284;
            this.uLabelInspectRate.Text = "3";
            // 
            // uComboInspectRate
            // 
            this.uComboInspectRate.Location = new System.Drawing.Point(708, 80);
            this.uComboInspectRate.Name = "uComboInspectRate";
            this.uComboInspectRate.Size = new System.Drawing.Size(100, 21);
            this.uComboInspectRate.TabIndex = 283;
            // 
            // uLabelInspectLevel
            // 
            this.uLabelInspectLevel.Location = new System.Drawing.Point(592, 104);
            this.uLabelInspectLevel.Name = "uLabelInspectLevel";
            this.uLabelInspectLevel.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectLevel.TabIndex = 282;
            this.uLabelInspectLevel.Text = "3";
            // 
            // uComboInspectLevel
            // 
            this.uComboInspectLevel.Location = new System.Drawing.Point(708, 104);
            this.uComboInspectLevel.Name = "uComboInspectLevel";
            this.uComboInspectLevel.Size = new System.Drawing.Size(100, 21);
            this.uComboInspectLevel.TabIndex = 281;
            // 
            // uComboMaterialGrade
            // 
            this.uComboMaterialGrade.Location = new System.Drawing.Point(124, 32);
            this.uComboMaterialGrade.Name = "uComboMaterialGrade";
            this.uComboMaterialGrade.Size = new System.Drawing.Size(100, 21);
            this.uComboMaterialGrade.TabIndex = 280;
            // 
            // uTextVendorName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance18;
            this.uTextVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Location = new System.Drawing.Point(472, 8);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(110, 21);
            this.uTextVendorName.TabIndex = 279;
            // 
            // uLabelMaterialGrade
            // 
            this.uLabelMaterialGrade.Location = new System.Drawing.Point(8, 32);
            this.uLabelMaterialGrade.Name = "uLabelMaterialGrade";
            this.uLabelMaterialGrade.Size = new System.Drawing.Size(110, 20);
            this.uLabelMaterialGrade.TabIndex = 278;
            this.uLabelMaterialGrade.Text = "18";
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(8, 104);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(110, 20);
            this.uLabelEtcDesc.TabIndex = 277;
            this.uLabelEtcDesc.Text = "17";
            // 
            // uDateCustomerAdmitDate
            // 
            this.uDateCustomerAdmitDate.Location = new System.Drawing.Point(936, 32);
            this.uDateCustomerAdmitDate.Name = "uDateCustomerAdmitDate";
            this.uDateCustomerAdmitDate.Size = new System.Drawing.Size(28, 21);
            this.uDateCustomerAdmitDate.TabIndex = 276;
            this.uDateCustomerAdmitDate.Visible = false;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(124, 104);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(460, 21);
            this.uTextEtcDesc.TabIndex = 275;
            // 
            // uTextSpecNo
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSpecNo.Appearance = appearance26;
            this.uTextSpecNo.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSpecNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSpecNo.Location = new System.Drawing.Point(708, 32);
            this.uTextSpecNo.MaxLength = 1000;
            this.uTextSpecNo.Name = "uTextSpecNo";
            this.uTextSpecNo.Size = new System.Drawing.Size(148, 21);
            this.uTextSpecNo.TabIndex = 273;
            // 
            // uTextSpec
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance14;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(708, 56);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(312, 21);
            this.uTextSpec.TabIndex = 274;
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(8, 80);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelWriteDate.TabIndex = 257;
            this.uLabelWriteDate.Text = "15";
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(232, 80);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(110, 20);
            this.uLabelWriteUser.TabIndex = 265;
            this.uLabelWriteUser.Text = "16";
            // 
            // uTextWriteUserName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Appearance = appearance40;
            this.uTextWriteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Location = new System.Drawing.Point(472, 80);
            this.uTextWriteUserName.Name = "uTextWriteUserName";
            this.uTextWriteUserName.ReadOnly = true;
            this.uTextWriteUserName.Size = new System.Drawing.Size(112, 21);
            this.uTextWriteUserName.TabIndex = 267;
            // 
            // uTextCustomerName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance17;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(992, 32);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(36, 21);
            this.uTextCustomerName.TabIndex = 266;
            this.uTextCustomerName.Visible = false;
            // 
            // uTextMaterialName
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance44;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(256, 56);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(328, 21);
            this.uTextMaterialName.TabIndex = 268;
            // 
            // uLabelCustomerAdmitDate
            // 
            this.uLabelCustomerAdmitDate.Location = new System.Drawing.Point(860, 32);
            this.uLabelCustomerAdmitDate.Name = "uLabelCustomerAdmitDate";
            this.uLabelCustomerAdmitDate.Size = new System.Drawing.Size(36, 20);
            this.uLabelCustomerAdmitDate.TabIndex = 260;
            this.uLabelCustomerAdmitDate.Text = "14";
            this.uLabelCustomerAdmitDate.Visible = false;
            // 
            // uTextWriteUserID
            // 
            appearance20.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance20.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance20;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
            this.uTextWriteUserID.ButtonsRight.Add(editorButton3);
            this.uTextWriteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteUserID.Location = new System.Drawing.Point(348, 80);
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.Size = new System.Drawing.Size(120, 21);
            this.uTextWriteUserID.TabIndex = 271;
            // 
            // uTextCustomerCode
            // 
            appearance19.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance19;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCustomerCode.ButtonsRight.Add(editorButton4);
            this.uTextCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCustomerCode.Location = new System.Drawing.Point(968, 32);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.Size = new System.Drawing.Size(20, 21);
            this.uTextCustomerCode.TabIndex = 272;
            this.uTextCustomerCode.Visible = false;
            // 
            // uTextMaterialCode
            // 
            appearance24.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMaterialCode.Appearance = appearance24;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance15.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance15;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
            this.uTextMaterialCode.ButtonsRight.Add(editorButton5);
            this.uTextMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMaterialCode.Location = new System.Drawing.Point(125, 56);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.Size = new System.Drawing.Size(127, 21);
            this.uTextMaterialCode.TabIndex = 269;
            // 
            // uTextVendorCode
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextVendorCode.Appearance = appearance22;
            this.uTextVendorCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance5.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance5;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
            this.uTextVendorCode.ButtonsRight.Add(editorButton6);
            this.uTextVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextVendorCode.Location = new System.Drawing.Point(348, 8);
            this.uTextVendorCode.Name = "uTextVendorCode";
            this.uTextVendorCode.Size = new System.Drawing.Size(120, 21);
            this.uTextVendorCode.TabIndex = 270;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(912, 32);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(20, 20);
            this.uLabelCustomer.TabIndex = 262;
            this.uLabelCustomer.Text = "13";
            this.uLabelCustomer.Visible = false;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(8, 8);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 258;
            this.uLabelPlant.Text = "6";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(232, 8);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelVendor.TabIndex = 259;
            this.uLabelVendor.Text = "7";
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(592, 56);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpec.TabIndex = 264;
            this.uLabelSpec.Text = "10";
            // 
            // uLabelSpecNo
            // 
            this.uLabelSpecNo.Location = new System.Drawing.Point(592, 32);
            this.uLabelSpecNo.Name = "uLabelSpecNo";
            this.uLabelSpecNo.Size = new System.Drawing.Size(110, 20);
            this.uLabelSpecNo.TabIndex = 261;
            this.uLabelSpecNo.Text = "12";
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(8, 56);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(110, 20);
            this.uLabelMaterial.TabIndex = 263;
            this.uLabelMaterial.Text = "9";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(124, 8);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(100, 21);
            this.uComboPlant.TabIndex = 256;
            // 
            // uGridAMLList
            // 
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            appearance42.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAMLList.DisplayLayout.Appearance = appearance42;
            this.uGridAMLList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAMLList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAMLList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAMLList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridAMLList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAMLList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridAMLList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAMLList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAMLList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAMLList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridAMLList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAMLList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAMLList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAMLList.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridAMLList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAMLList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAMLList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridAMLList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridAMLList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAMLList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridAMLList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridAMLList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAMLList.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridAMLList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAMLList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAMLList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAMLList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridAMLList.Location = new System.Drawing.Point(0, 80);
            this.uGridAMLList.Name = "uGridAMLList";
            this.uGridAMLList.Size = new System.Drawing.Size(1070, 90);
            this.uGridAMLList.TabIndex = 6;
            // 
            // frmINSZ0005
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.Controls.Add(this.uGridAMLList);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0005";
            this.Load += new System.EventHandler(this.frmINSZ0005_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0005_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0005_FormClosing);
            this.Resize += new System.EventHandler(this.frmINSZ0005_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAMLHistoryList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDrawing_No)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumShipCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckHistoryInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckAQLFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCustomerAdmitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAMLList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialGrade;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialType;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDrawing_No;
        private Infragistics.Win.Misc.UltraLabel uLabelDrawing_No;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumShipCount;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckHistoryInfo;
        private Infragistics.Win.Misc.UltraLabel uLabelHistoryInfo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckAQLFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelAQLFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectRate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectRate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectLevel;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectLevel;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMaterialGrade;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialGrade;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCustomerAdmitDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerAdmitDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecNo;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAMLList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAMLHistoryList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraButton uButtonDel;
    }
}