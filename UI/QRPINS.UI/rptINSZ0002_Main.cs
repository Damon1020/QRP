﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : rptINSZ0002.cs                                        */
/* 프로그램명   : 신제품 인증등록(수입검사 성적서)                      */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-09-18                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : 기능 추가 (홍길동)                       */
/*----------------------------------------------------------------------*/

using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

//Using 추가
using System.Data;

namespace QRPINS.UI
{
    /// <summary>
    /// Summary description for rptINSZ0002_Main.
    /// </summary>
    public partial class rptINSZ0002_Main : DataDynamics.ActiveReports.ActiveReport
    {

        public rptINSZ0002_Main()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        public rptINSZ0002_Main(DataTable dtResult, DataTable dtData)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            
            this.subResult.Report = new rptINSZ0002_S01(dtResult);

            CheckData(dtData);
        }

        void CheckData(DataTable dtData)
        {
            if (dtData.Rows.Count == 0)
                return;

            // Columns Count
            int intColumnCount = dtData.Columns.Count;
            // 9 <- TextBox Count(InspectData SubReport)
            int intTextCount = 9;
            // 8 <- SubReport Count(Data SubReport)
            int intSubReportCnt = 8;
            int intSubCnt = 0;


            if (intColumnCount <= intTextCount)
            {
                this.subData.Visible = true;
                this.subData.Report = new rptINSZ0002_S02(dtData,true);
                return;
            }

            
            for (int i = 1; i < intSubReportCnt; i++)
            {
                int _int = i * intTextCount;

                
                if (intColumnCount <= _int)
                {
                    intSubCnt = i-1;
                    break;
                }
            }

            SubReportData(dtData, intSubCnt);
           
        }

        DataTable DeleteColumns(DataTable dtData)
        {
            // 8 <- TextBox Count(InspectData SubReport)
            int intTextCount = 9;

            if (dtData.Rows.Count <= intTextCount)
                return dtData;

            // intCnt-1 : Text Count - Title TextCount (InspectData SubReport)
            for (int i = 0; i < intTextCount - 1; i++)
            {
                // Xn 정보가 없다면 PASS
                if(!dtData.Columns.Contains("#"+i.ToString()))
                    continue;

                // X1 컬럼삭제
                dtData.Columns.Remove("#"+i.ToString());

                if(!dtData.Columns.Contains("#"+ (i+7).ToString()))
                    continue;

                dtData.Columns["#" + (i+7).ToString()].ColumnName = "#" + i.ToString();
            }


            return dtData;
        }

        void SubReportData(DataTable dtData, int intSub)
        {
            DataTable dtCopy = new DataTable();
            try
            {
                for (int i = 0; i <= intSub; i++)
                {
                    switch (i)
                    {
                        case 0:
                            dtCopy = dtData;
                            subData.Visible = true;
                            subData.Report = new rptINSZ0002_S02(dtCopy, true);
                            break;
                        case 1:
                            dtCopy = DeleteColumns(dtData);
                            subData1.Visible = true;
                            subData1.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        case 2:
                            dtCopy = DeleteColumns(dtData);
                            subData2.Visible = true;
                            subData2.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        case 3:
                            dtCopy = DeleteColumns(dtData);
                            subData3.Visible = true;
                            subData3.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        case 4:
                            dtCopy = DeleteColumns(dtData);
                            subData4.Visible = true;
                            subData4.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        case 5:
                            dtCopy = DeleteColumns(dtData);
                            subData5.Visible = true;
                            subData5.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        case 6:
                            dtCopy = DeleteColumns(dtData);
                            subData6.Visible = true;
                            subData6.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        case 7:
                            dtCopy = DeleteColumns(dtData);
                            subData7.Visible = true;
                            subData7.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        case 8:
                            dtCopy = DeleteColumns(dtData);
                            subData8.Visible = true;
                            subData8.Report = new rptINSZ0002_S02(dtCopy, false);
                            break;
                        default:
                            break;

                    }
                    dtCopy.Clear();
                }
            }
            catch (Exception ex)
            { }
            finally
            {
                dtData.Dispose();
                dtCopy.Dispose();
            }
        }

        private void reportHeader_Format(object sender, EventArgs e)
        {
            if (txtICP.Text.Trim().Equals("P"))
                this.checkPass.Checked = true;
            else if (txtICP.Text.Trim().Equals("F"))
                this.checkFail.Checked = true;

        }


        void ContrlSize()
        {
            for (int i = 0; i < 2; i++)
            {
                float dbDefalut = 0;
                float dbChange = 0;
                float dbHeight = 0;
                string strName = string.Empty;

                if (i==0)
                {
                    
                    DataDynamics.ActiveReports.TextBox[] txt = { txtVendor, txtSpec, txtSpecNo, txtLotQty, txtLotNo, txtGRQty, txtGradeName };
                    
                    dbDefalut = txt[0].Size.Height;

                    foreach (DataDynamics.ActiveReports.TextBox c in txt)
                    {
                        if (c.Size.Height <= dbDefalut)
                            continue;
                        else
                        {
                            dbChange = c.Size.Height;
                            strName = c.Name;
                        }
                    }

                    if (dbChange == 0)
                        continue;

                    foreach (DataDynamics.ActiveReports.TextBox c in txt)
                    {
                        if (c.Name.Equals(strName))
                            continue;

                        dbHeight = dbChange - c.Size.Height;

                        SizeF size = new SizeF(c.Size.Width, c.Size.Height + dbHeight);
                        c.Size = size;
                        
                    }
                }
                else
                {
                    DataDynamics.ActiveReports.TextBox[] txt = { txtGRDate, txtReceipt, txtCompleteDate, txtLot, txtFaultContents, txtConfirm, txtEtcDesc };

                    dbDefalut = txt[0].Size.Height;

                    foreach (DataDynamics.ActiveReports.TextBox c in txt)
                    {
                        if (c.Size.Height <= dbDefalut)
                            continue;
                        else
                        {
                            dbChange = c.Size.Height;
                            strName = c.Name;
                        }
                    }

                    if (dbChange == 0)
                        continue;

                    foreach (DataDynamics.ActiveReports.TextBox c in txt)
                    {
                        if (c.Name.Equals(strName))
                            continue;

                        dbHeight = dbChange - c.Size.Height;

                        SizeF size = new SizeF(c.Size.Width, c.Size.Height + dbHeight);
                        c.Size = size;

                    }
                }
            }
        }
    }
}
