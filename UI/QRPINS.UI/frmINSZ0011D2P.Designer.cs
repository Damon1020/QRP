﻿namespace QRPINS.UI
{
    partial class frmINSZ0011D2P
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0011D2P));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxBottom = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSave = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxBottom)).BeginInit();
            this.uGroupBoxBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(298, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxBottom
            // 
            this.uGroupBoxBottom.Controls.Add(this.uButtonDelete);
            this.uGroupBoxBottom.Controls.Add(this.uButtonSave);
            this.uGroupBoxBottom.Controls.Add(this.uButtonClose);
            this.uGroupBoxBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxBottom.Location = new System.Drawing.Point(0, 368);
            this.uGroupBoxBottom.Name = "uGroupBoxBottom";
            this.uGroupBoxBottom.Size = new System.Drawing.Size(298, 30);
            this.uGroupBoxBottom.TabIndex = 2;
            // 
            // uGridList
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridList.DisplayLayout.Appearance = appearance13;
            this.uGridList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridList.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridList.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGridList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridList.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridList.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGridList.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGridList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridList.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridList.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridList.Location = new System.Drawing.Point(0, 40);
            this.uGridList.Name = "uGridList";
            this.uGridList.Size = new System.Drawing.Size(298, 328);
            this.uGridList.TabIndex = 3;
            this.uGridList.Text = "ultraGrid1";
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(184, 1);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 0;
            this.uButtonClose.Text = "닫기";
            // 
            // uButtonSave
            // 
            this.uButtonSave.Location = new System.Drawing.Point(96, 1);
            this.uButtonSave.Name = "uButtonSave";
            this.uButtonSave.Size = new System.Drawing.Size(88, 28);
            this.uButtonSave.TabIndex = 1;
            this.uButtonSave.Text = "저장";
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(8, 1);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 2;
            this.uButtonDelete.Text = "행삭제";
            // 
            // frmINSZ0011D2P
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(298, 398);
            this.ControlBox = false;
            this.Controls.Add(this.uGridList);
            this.Controls.Add(this.uGroupBoxBottom);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0011D2P";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmINSZ0011D2P_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0011D2P_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxBottom)).EndInit();
            this.uGroupBoxBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxBottom;
        private Infragistics.Win.Misc.UltraButton uButtonSave;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridList;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
    }
}