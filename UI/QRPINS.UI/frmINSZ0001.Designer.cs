﻿namespace QRPINS.UI
{
    partial class frmINSZ0001
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0001));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchGRToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchGRFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchGRDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchGRNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxFile = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridFile = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextSpecNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecNo = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextGRDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateReqDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextICP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelICP = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMSDS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMSDS = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextFloorPlanNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFloorPlanNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApplyDevice = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApplyDevice = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialSpec1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialSpec1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShipmentQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelShipmentQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqPurpose = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReqPurpose = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelReqDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReqUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelGRDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStdNumber = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonFileDelete = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchGRNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFile)).BeginInit();
            this.uGroupBoxFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextICP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMSDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyDevice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialSpec1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchGRToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchGRFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchGRNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelGRNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uDateSearchGRToDate
            // 
            this.uDateSearchGRToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchGRToDate.Location = new System.Drawing.Point(232, 36);
            this.uDateSearchGRToDate.Name = "uDateSearchGRToDate";
            this.uDateSearchGRToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchGRToDate.TabIndex = 15;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(216, 36);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 14;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchGRFromDate
            // 
            this.uDateSearchGRFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchGRFromDate.Location = new System.Drawing.Point(116, 36);
            this.uDateSearchGRFromDate.Name = "uDateSearchGRFromDate";
            this.uDateSearchGRFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchGRFromDate.TabIndex = 13;
            // 
            // uLabelSearchGRDate
            // 
            this.uLabelSearchGRDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchGRDate.Name = "uLabelSearchGRDate";
            this.uLabelSearchGRDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchGRDate.TabIndex = 12;
            this.uLabelSearchGRDate.Text = "ultraLabel2";
            // 
            // uTextSearchVendorName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance21;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(818, 36);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchVendorName.TabIndex = 11;
            // 
            // uTextSearchVendorCode
            // 
            appearance22.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance22;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(706, 36);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorCode.TabIndex = 10;
            this.uTextSearchVendorCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchVendorCode_KeyDown);
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(602, 36);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchVendor.TabIndex = 9;
            this.uLabelSearchVendor.Text = "ultraLabel2";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(444, 36);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchLotNo.TabIndex = 8;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(340, 36);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 7;
            this.uLabelSearchLotNo.Text = "ultraLabel2";
            // 
            // uTextSearchMaterialName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Appearance = appearance4;
            this.uTextSearchMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(818, 12);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.ReadOnly = true;
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchMaterialName.TabIndex = 6;
            // 
            // uTextSearchMaterialCode
            // 
            appearance40.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance40.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance40;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(706, 12);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialCode.TabIndex = 5;
            this.uTextSearchMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMaterialCode_KeyDown);
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick);
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(602, 12);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterial.TabIndex = 4;
            this.uLabelSearchMaterial.Text = "ultraLabel2";
            // 
            // uTextSearchGRNo
            // 
            this.uTextSearchGRNo.Location = new System.Drawing.Point(444, 12);
            this.uTextSearchGRNo.Name = "uTextSearchGRNo";
            this.uTextSearchGRNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchGRNo.TabIndex = 3;
            // 
            // uLabelGRNo
            // 
            this.uLabelGRNo.Location = new System.Drawing.Point(340, 12);
            this.uLabelGRNo.Name = "uLabelGRNo";
            this.uLabelGRNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRNo.TabIndex = 2;
            this.uLabelGRNo.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance6;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance16;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance17;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1060, 650);
            this.uGrid1.TabIndex = 2;
            this.uGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid1_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxFile);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSpecNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSpecNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGrid2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextGRDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateReqDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextICP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelICP);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMSDS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMSDS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextFloorPlanNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFloorPlanNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApplyDevice);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelApplyDevice);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMoldSeq);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMoldSeq);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialSpec1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialSpec1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextShipmentQty);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelShipmentQty);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterial);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCustomer);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReqPurpose);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReqPurpose);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReqDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReqName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReqID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReqUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelGRDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStdNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStdNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxFile
            // 
            this.uGroupBoxFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxFile.Controls.Add(this.uButtonFileDelete);
            this.uGroupBoxFile.Controls.Add(this.uGridFile);
            this.uGroupBoxFile.Location = new System.Drawing.Point(12, 156);
            this.uGroupBoxFile.Name = "uGroupBoxFile";
            this.uGroupBoxFile.Size = new System.Drawing.Size(1036, 264);
            this.uGroupBoxFile.TabIndex = 51;
            this.uGroupBoxFile.Text = "ultraGroupBox1";
            // 
            // uGridFile
            // 
            this.uGridFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFile.DisplayLayout.Appearance = appearance33;
            this.uGridFile.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFile.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.GroupByBox.Appearance = appearance36;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFile.DisplayLayout.GroupByBox.BandLabelAppearance = appearance37;
            this.uGridFile.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFile.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.uGridFile.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFile.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFile.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFile.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.uGridFile.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFile.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFile.DisplayLayout.Override.CellAppearance = appearance56;
            this.uGridFile.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFile.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.uGridFile.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.uGridFile.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFile.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.uGridFile.DisplayLayout.Override.RowAppearance = appearance59;
            this.uGridFile.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFile.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.uGridFile.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFile.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFile.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFile.Location = new System.Drawing.Point(12, 56);
            this.uGridFile.Name = "uGridFile";
            this.uGridFile.Size = new System.Drawing.Size(1016, 200);
            this.uGridFile.TabIndex = 0;
            this.uGridFile.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFile_ClickCellButton);
            // 
            // uTextSpecNo
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Appearance = appearance26;
            this.uTextSpecNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Location = new System.Drawing.Point(808, 84);
            this.uTextSpecNo.Name = "uTextSpecNo";
            this.uTextSpecNo.ReadOnly = true;
            this.uTextSpecNo.Size = new System.Drawing.Size(40, 21);
            this.uTextSpecNo.TabIndex = 50;
            // 
            // uLabelSpecNo
            // 
            this.uLabelSpecNo.Location = new System.Drawing.Point(916, 140);
            this.uLabelSpecNo.Name = "uLabelSpecNo";
            this.uLabelSpecNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecNo.TabIndex = 49;
            this.uLabelSpecNo.Text = "ultraLabel2";
            this.uLabelSpecNo.Visible = false;
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance24;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 424);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1036, 220);
            this.uGrid2.TabIndex = 48;
            // 
            // uTextGRDate
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Appearance = appearance2;
            this.uTextGRDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Location = new System.Drawing.Point(696, 12);
            this.uTextGRDate.Name = "uTextGRDate";
            this.uTextGRDate.ReadOnly = true;
            this.uTextGRDate.Size = new System.Drawing.Size(100, 21);
            this.uTextGRDate.TabIndex = 47;
            // 
            // uDateReqDate
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReqDate.Appearance = appearance39;
            this.uDateReqDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReqDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateReqDate.Location = new System.Drawing.Point(432, 36);
            this.uDateReqDate.Name = "uDateReqDate";
            this.uDateReqDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReqDate.TabIndex = 46;
            // 
            // uTextICP
            // 
            appearance29.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance29.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance29;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextICP.ButtonsRight.Add(editorButton3);
            this.uTextICP.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextICP.Location = new System.Drawing.Point(1024, 28);
            this.uTextICP.Name = "uTextICP";
            this.uTextICP.ReadOnly = true;
            this.uTextICP.Size = new System.Drawing.Size(21, 21);
            this.uTextICP.TabIndex = 45;
            this.uTextICP.Visible = false;
            this.uTextICP.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextICP_EditorButtonClick);
            // 
            // uLabelICP
            // 
            this.uLabelICP.Location = new System.Drawing.Point(920, 28);
            this.uLabelICP.Name = "uLabelICP";
            this.uLabelICP.Size = new System.Drawing.Size(100, 20);
            this.uLabelICP.TabIndex = 44;
            this.uLabelICP.Text = "ultraLabel2";
            this.uLabelICP.Visible = false;
            // 
            // uTextMSDS
            // 
            appearance23.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance23.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance23;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMSDS.ButtonsRight.Add(editorButton4);
            this.uTextMSDS.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMSDS.Location = new System.Drawing.Point(1024, 4);
            this.uTextMSDS.Name = "uTextMSDS";
            this.uTextMSDS.ReadOnly = true;
            this.uTextMSDS.Size = new System.Drawing.Size(21, 21);
            this.uTextMSDS.TabIndex = 43;
            this.uTextMSDS.Visible = false;
            this.uTextMSDS.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMSDS_EditorButtonClick);
            // 
            // uLabelMSDS
            // 
            this.uLabelMSDS.Location = new System.Drawing.Point(920, 4);
            this.uLabelMSDS.Name = "uLabelMSDS";
            this.uLabelMSDS.Size = new System.Drawing.Size(100, 20);
            this.uLabelMSDS.TabIndex = 42;
            this.uLabelMSDS.Text = "ultraLabel2";
            this.uLabelMSDS.Visible = false;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 132);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(468, 21);
            this.uTextEtcDesc.TabIndex = 41;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 132);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtcDesc.TabIndex = 40;
            this.uLabelEtcDesc.Text = "ultraLabel2";
            // 
            // uTextFloorPlanNo
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Appearance = appearance32;
            this.uTextFloorPlanNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Location = new System.Drawing.Point(696, 84);
            this.uTextFloorPlanNo.Name = "uTextFloorPlanNo";
            this.uTextFloorPlanNo.ReadOnly = true;
            this.uTextFloorPlanNo.Size = new System.Drawing.Size(108, 21);
            this.uTextFloorPlanNo.TabIndex = 39;
            // 
            // uLabelFloorPlanNo
            // 
            this.uLabelFloorPlanNo.Location = new System.Drawing.Point(592, 84);
            this.uLabelFloorPlanNo.Name = "uLabelFloorPlanNo";
            this.uLabelFloorPlanNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelFloorPlanNo.TabIndex = 38;
            this.uLabelFloorPlanNo.Text = "ultraLabel2";
            // 
            // uTextApplyDevice
            // 
            appearance27.BackColor = System.Drawing.Color.White;
            this.uTextApplyDevice.Appearance = appearance27;
            this.uTextApplyDevice.BackColor = System.Drawing.Color.White;
            this.uTextApplyDevice.Location = new System.Drawing.Point(432, 84);
            this.uTextApplyDevice.Name = "uTextApplyDevice";
            this.uTextApplyDevice.Size = new System.Drawing.Size(150, 21);
            this.uTextApplyDevice.TabIndex = 37;
            // 
            // uLabelApplyDevice
            // 
            this.uLabelApplyDevice.Location = new System.Drawing.Point(328, 84);
            this.uLabelApplyDevice.Name = "uLabelApplyDevice";
            this.uLabelApplyDevice.Size = new System.Drawing.Size(100, 20);
            this.uLabelApplyDevice.TabIndex = 36;
            this.uLabelApplyDevice.Text = "ultraLabel2";
            // 
            // uTextMoldSeq
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Appearance = appearance38;
            this.uTextMoldSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Location = new System.Drawing.Point(696, 60);
            this.uTextMoldSeq.Name = "uTextMoldSeq";
            this.uTextMoldSeq.ReadOnly = true;
            this.uTextMoldSeq.Size = new System.Drawing.Size(152, 21);
            this.uTextMoldSeq.TabIndex = 35;
            // 
            // uLabelMoldSeq
            // 
            this.uLabelMoldSeq.Location = new System.Drawing.Point(592, 60);
            this.uLabelMoldSeq.Name = "uLabelMoldSeq";
            this.uLabelMoldSeq.Size = new System.Drawing.Size(100, 20);
            this.uLabelMoldSeq.TabIndex = 34;
            this.uLabelMoldSeq.Text = "ultraLabel2";
            // 
            // uTextMaterialSpec1
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialSpec1.Appearance = appearance34;
            this.uTextMaterialSpec1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialSpec1.Location = new System.Drawing.Point(1012, 112);
            this.uTextMaterialSpec1.Name = "uTextMaterialSpec1";
            this.uTextMaterialSpec1.ReadOnly = true;
            this.uTextMaterialSpec1.Size = new System.Drawing.Size(40, 21);
            this.uTextMaterialSpec1.TabIndex = 33;
            this.uTextMaterialSpec1.Visible = false;
            // 
            // uLabelMaterialSpec1
            // 
            this.uLabelMaterialSpec1.Location = new System.Drawing.Point(908, 112);
            this.uLabelMaterialSpec1.Name = "uLabelMaterialSpec1";
            this.uLabelMaterialSpec1.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterialSpec1.TabIndex = 32;
            this.uLabelMaterialSpec1.Text = "ultraLabel2";
            this.uLabelMaterialSpec1.Visible = false;
            // 
            // uTextShipmentQty
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentQty.Appearance = appearance35;
            this.uTextShipmentQty.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentQty.Location = new System.Drawing.Point(696, 36);
            this.uTextShipmentQty.Name = "uTextShipmentQty";
            this.uTextShipmentQty.ReadOnly = true;
            this.uTextShipmentQty.Size = new System.Drawing.Size(100, 21);
            this.uTextShipmentQty.TabIndex = 29;
            // 
            // uLabelShipmentQty
            // 
            this.uLabelShipmentQty.Location = new System.Drawing.Point(592, 36);
            this.uLabelShipmentQty.Name = "uLabelShipmentQty";
            this.uLabelShipmentQty.Size = new System.Drawing.Size(100, 20);
            this.uLabelShipmentQty.TabIndex = 28;
            this.uLabelShipmentQty.Text = "ultraLabel2";
            // 
            // uTextMaterialName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance18;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(218, 60);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(362, 21);
            this.uTextMaterialName.TabIndex = 25;
            // 
            // uTextMaterialCode
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Appearance = appearance25;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Location = new System.Drawing.Point(116, 60);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.ReadOnly = true;
            this.uTextMaterialCode.Size = new System.Drawing.Size(100, 21);
            this.uTextMaterialCode.TabIndex = 24;
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(12, 60);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterial.TabIndex = 23;
            this.uLabelMaterial.Text = "ultraLabel2";
            // 
            // uTextVendor
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance30;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(116, 84);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(200, 21);
            this.uTextVendor.TabIndex = 22;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(12, 84);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomer.TabIndex = 21;
            this.uLabelCustomer.Text = "ultraLabel2";
            // 
            // uTextReqPurpose
            // 
            this.uTextReqPurpose.Location = new System.Drawing.Point(116, 108);
            this.uTextReqPurpose.Name = "uTextReqPurpose";
            this.uTextReqPurpose.Size = new System.Drawing.Size(468, 21);
            this.uTextReqPurpose.TabIndex = 20;
            // 
            // uLabelReqPurpose
            // 
            this.uLabelReqPurpose.Location = new System.Drawing.Point(12, 108);
            this.uLabelReqPurpose.Name = "uLabelReqPurpose";
            this.uLabelReqPurpose.Size = new System.Drawing.Size(100, 20);
            this.uLabelReqPurpose.TabIndex = 19;
            this.uLabelReqPurpose.Text = "ultraLabel2";
            // 
            // uLabelReqDate
            // 
            this.uLabelReqDate.Location = new System.Drawing.Point(328, 36);
            this.uLabelReqDate.Name = "uLabelReqDate";
            this.uLabelReqDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelReqDate.TabIndex = 17;
            this.uLabelReqDate.Text = "ultraLabel2";
            // 
            // uTextReqName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqName.Appearance = appearance19;
            this.uTextReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqName.Location = new System.Drawing.Point(218, 36);
            this.uTextReqName.Name = "uTextReqName";
            this.uTextReqName.ReadOnly = true;
            this.uTextReqName.Size = new System.Drawing.Size(100, 21);
            this.uTextReqName.TabIndex = 16;
            // 
            // uTextReqID
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReqID.Appearance = appearance20;
            this.uTextReqID.BackColor = System.Drawing.Color.PowderBlue;
            appearance3.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance3;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReqID.ButtonsRight.Add(editorButton5);
            this.uTextReqID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReqID.Location = new System.Drawing.Point(116, 36);
            this.uTextReqID.Name = "uTextReqID";
            this.uTextReqID.Size = new System.Drawing.Size(100, 21);
            this.uTextReqID.TabIndex = 15;
            this.uTextReqID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReqID_KeyDown);
            this.uTextReqID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReqID_EditorButtonClick);
            // 
            // uLabelReqUser
            // 
            this.uLabelReqUser.Location = new System.Drawing.Point(12, 36);
            this.uLabelReqUser.Name = "uLabelReqUser";
            this.uLabelReqUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelReqUser.TabIndex = 14;
            this.uLabelReqUser.Text = "ultraLabel2";
            // 
            // uLabelGRDate
            // 
            this.uLabelGRDate.Location = new System.Drawing.Point(592, 12);
            this.uLabelGRDate.Name = "uLabelGRDate";
            this.uLabelGRDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRDate.TabIndex = 12;
            this.uLabelGRDate.Text = "ultraLabel2";
            // 
            // uTextStdNumber
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance31;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(432, 12);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(150, 21);
            this.uTextStdNumber.TabIndex = 11;
            // 
            // uLabelStdNumber
            // 
            this.uLabelStdNumber.Location = new System.Drawing.Point(328, 12);
            this.uLabelStdNumber.Name = "uLabelStdNumber";
            this.uLabelStdNumber.Size = new System.Drawing.Size(100, 20);
            this.uLabelStdNumber.TabIndex = 10;
            this.uLabelStdNumber.Text = "ultraLabel2";
            // 
            // uTextPlant
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance28;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(116, 12);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(150, 21);
            this.uTextPlant.TabIndex = 9;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 8;
            this.uLabelPlant.Text = "ultraLabel2";
            // 
            // uButtonFileDelete
            // 
            this.uButtonFileDelete.Location = new System.Drawing.Point(12, 28);
            this.uButtonFileDelete.Name = "uButtonFileDelete";
            this.uButtonFileDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDelete.TabIndex = 1;
            this.uButtonFileDelete.Text = "행삭제";
            this.uButtonFileDelete.Click += new System.EventHandler(this.uButtonFileDelete_Click);
            // 
            // frmINSZ0001
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0001";
            this.Load += new System.EventHandler(this.frmINSZ0001_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0001_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0001_FormClosing);
            this.Resize += new System.EventHandler(this.frmINSZ0001_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchGRNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFile)).EndInit();
            this.uGroupBoxFile.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextICP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMSDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyDevice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialSpec1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelGRNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchGRNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchGRToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchGRFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchGRDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqID;
        private Infragistics.Win.Misc.UltraLabel uLabelReqUser;
        private Infragistics.Win.Misc.UltraLabel uLabelGRDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqPurpose;
        private Infragistics.Win.Misc.UltraLabel uLabelReqPurpose;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShipmentQty;
        private Infragistics.Win.Misc.UltraLabel uLabelShipmentQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFloorPlanNo;
        private Infragistics.Win.Misc.UltraLabel uLabelFloorPlanNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApplyDevice;
        private Infragistics.Win.Misc.UltraLabel uLabelApplyDevice;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialSpec1;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialSpec1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextICP;
        private Infragistics.Win.Misc.UltraLabel uLabelICP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMSDS;
        private Infragistics.Win.Misc.UltraLabel uLabelMSDS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecNo;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxFile;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFile;
        private Infragistics.Win.Misc.UltraButton uButtonFileDelete;
    }
}