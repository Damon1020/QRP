﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0009.cs                                        */
/* 프로그램명   : 순회검사 Monitoring                                   */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0009 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmINSZ0009()
        {
            InitializeComponent();
        }

        private void frmINSZ0009_Activated(object sender, EventArgs e)
        {
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0009_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("순회검사 Monitoring", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            //ChangeGridColumn(0);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStartDate, "모니터링 시작일", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridMonitoring, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "MonitoringNo", "MonitoringNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipLocCode", "위치코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipName", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "PACKAGE", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "GobyTime", "경과시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProductName", "제품", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "InspectPeriod", "검사주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "PeriodUnitName", "검사주기단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "StartDate", "모니터링 시작일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "InspectCount", "검사횟수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                // Set FontSize
                this.uGridMonitoring.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridMonitoring.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void ChangeGridColumn(int intBandIndex)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                //intBandIndex = 0 : Area정보
                if (intBandIndex == 0)
                {
                    wGrid.mfInitGeneralGrid(this.uGridMonitoring, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                        , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                        , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                        , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaCode", "Area코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaName", "Area명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                }

                //intBandIndex = 1 : 위치정보
                else if (intBandIndex == 1)
                {
                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaCode", "Area코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaName", "Area명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipLocCode", "위치코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipLocName", "위치명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                }

                //intBandIndex = 2 : 설비그룹정보
                else if (intBandIndex == 2)
                {
                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaCode", "Area코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaName", "Area명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipLocCode", "위치코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipLocName", "위치명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipGroupCode", "설비그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipGroupName", "설비그룹명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                }
                //intBandIndex = 3 : 모니터링정보
                else if (intBandIndex == 3)
                {
                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaCode", "Area코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "AreaName", "Area명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipLocCode", "위치코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipLocName", "위치명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipGroupCode", "설비그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, intBandIndex, "EquipGroupName", "설비그룹명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    // 컬럼설정
                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "MonitoringNo", "MonitoringNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipLocCode", "위치코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "EquipName", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 40
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 50
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "CustomerCode", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "PACKAGE", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "GobyTime", "경과시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 50
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "ProductName", "제품", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, true, 50
                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "InspectPeriod", "검사주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 50
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "PeriodUnitName", "검사주기단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 50
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "StartDate", "모니터링 시작일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                    wGrid.mfSetGridColumn(this.uGridMonitoring, 0, "InspectCount", "검사횟수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 50
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method

        // 조회
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건 변수 생성
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strEquipLocCode = this.uComboSearchArea.Value.ToString();
                String strStartFromDate = this.uDateSearchStartFromDate.Value.ToString();
                String strStartToDate = this.uDateSearchStartToDate.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();                
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.Monitoring), "Monitoring");
                QRPINS.BL.INSPRC.Monitoring clsMonitoring = new QRPINS.BL.INSPRC.Monitoring();
                brwChannel.mfCredentials(clsMonitoring);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();

                //-- 기존 모니터링 검색 및 Binding 방법 --//
                // 검색 Method 호출
                DataTable dtSearch = clsMonitoring.mfReadCycleMonitoring(strPlantCode, strEquipLocCode, strStartFromDate, strStartToDate, m_resSys.GetString("SYS_LANG"));

                this.uGridMonitoring.DataSource = dtSearch;
                this.uGridMonitoring.DataBind();
                //----------------------------------------//

                //-- 계층형태 모니터링 검색 및 Binding 방법 --//
                ////DataTable dtSearch = clsMonitoring.mfReadCycleMonitoringHierarchy(strPlantCode, strEquipLocCode, strStartFromDate, strStartToDate, m_resSys.GetString("SYS_LANG"));
                ////DataSet ds = grd.mfCreateHierarchicalDataSet(dtSearch, "PKCode", "ParentCode");
                ////this.uGridMonitoring.DataSource = ds;

                //for (int i = 0; i < this.uGridMonitoring.DisplayLayout.Bands.Count; i++)
                //{
                //    ChangeGridColumn(i);
                //    this.uGridMonitoring.DisplayLayout.Bands[i].ColHeadersVisible = true;
                //}
                //----------------------------------------//

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
                else
                {
                    grd.mfSetAutoResizeColWidth(this.uGridMonitoring, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 1.검색조건 Event, Method

        // 검색조건 : 공장선택시 위치정보를 보여준다.
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                DataTable dtEquipLoc = new DataTable();

                this.uComboSearchArea.Items.Clear();

                if (strPlantCode != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsEquipLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsEquipLoc);

                    dtEquipLoc = clsEquipLoc.mfReadLocationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "EquipLocCode", "EquipLocName", dtEquipLoc);                                          
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

    }
}
