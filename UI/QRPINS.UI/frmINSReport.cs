﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질검사관리                                          */
/* 프로그램ID   : frmINSReport.cs                                       */
/* 프로그램명   : 품질검사관리 Report                                   */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-23                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Microsoft.VisualBasic;
using System.Collections;
using System.IO;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export;
using DataDynamics.ActiveReports.Export.Pdf;

namespace QRPINS.UI
{
    public partial class frmINSReport : Form
    {
        private string strFileChk;

        public string FileChk
        {
            get { return strFileChk; }
            set { strFileChk = value; }
        }


        public frmINSReport()
        {
            InitializeComponent();
        }
        // 수입검사성적서(Inspection)
        public frmINSReport(DataTable dtHeader, DataTable dtResult, DataTable dtData)
        {
            InitializeComponent();

            InitReport(dtHeader, dtResult, dtData);
        }
        // Abornomal
        public frmINSReport(DataTable dtHeader, DataTable dtHistory, DataTable dtContents, DataTable dtState)
        {
            InitializeComponent();

            InitReport(dtHeader, dtHistory, dtContents, dtState);
        }

        /// <summary>
        /// 수입검사성적서
        /// </summary>
        /// <param name="dtHeader"></param>
        /// <param name="dtResult"></param>
        /// <param name="dtData"></param>
        private void InitReport(DataTable dtHeader, DataTable dtResult, DataTable dtData)
        {
            try
            {
                string strStdNumber = string.Empty;
                if (dtHeader.Rows.Count > 0)
                    strStdNumber = dtHeader.Rows[0]["ReqNumber"].ToString(); //관리번호 저장


                #region Viewer 초기화
                rptViewDoc.Width = this.Width;
                rptViewDoc.Height = this.Height;
                rptViewDoc.Anchor = AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                #endregion

                #region 리포트 그리기


                // 수입검사이상Resport
                rptINSZ0002_Main rptSetUpMain = new rptINSZ0002_Main(dtResult, dtData);
                rptSetUpMain.DataSource = dtHeader;

                rptSetUpMain.Run();
                rptViewDoc.Document = rptSetUpMain.Document;

                SaveReport_Export("S", strStdNumber, "Inspection.pdf");

                #endregion

            }
            catch (Exception ex)
            {
            }
            finally
            { }
        }

        private void InitReport(DataTable dtHeader, DataTable dtHistory, DataTable dtContents, DataTable dtState)
        {
            try
            {
                string strStdNumber = "";
                if (dtHeader.Rows.Count > 0)
                    strStdNumber = dtHeader.Rows[0]["StdNumber"].ToString(); //관리번호 저장


                #region Viewer 초기화
                rptViewDoc.Width = this.Width;
                rptViewDoc.Height = this.Height;
                rptViewDoc.Anchor = AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                #endregion

                #region 리포트 그리기


                //자재이상발생 리포트 그리기
                //rptINS0017_Main rptSetUpMain = new rptINS0017_Main(dtHistory, dtContents, dtState);
                //rptSetUpMain.DataSource = dtHeader;

                //rptSetUpMain.Run();
                //rptViewDoc.Document = rptSetUpMain.Document;

                SaveReport_Export("S", strStdNumber, "Report.pdf");

                #endregion

            }
            catch (Exception ex)
            {
            }
            finally
            { }
        }

        /// <summary>
        /// 리포트 PDF로 변환하여 지정된 경로에 저장시킨다.
        /// </summary>
        private void SaveReport_Export(string strGubun,string strStdNumber,string strName)
        {
            try
            {
                //
                object export = null;
                export = new PdfExport();

                //Report디렉토리지정
                string strBrowserPath = Application.ExecutablePath;
                int intPos = strBrowserPath.LastIndexOf(@"\");
                string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "GWpdf\\";

                //디렉토리 유무확인
                DirectoryInfo di = new DirectoryInfo(m_strQRPBrowserPath);

                //지정된경로에 폴더가 없을 경우 폴더생성
                if (di.Exists.Equals(false))
                {
                    di.Create();
                }

                if (strGubun.Equals("S"))
                {
                    //파일여부 체크 존재한다면 
                    if (File.Exists(m_strQRPBrowserPath + strStdNumber + "-" + strName))
                    {
                        File.Delete(m_strQRPBrowserPath + strStdNumber + "-" + strName);
                    }
                    //((PdfExport)export).Export(this.rptViewDoc.Document, m_strQRPBrowserPath + "Setup.pdf");

                    PdfExport pd = new PdfExport();
                    pd.Security.Permissions = PdfPermissions.AllowModifyAnnotations;
                    pd.Security.Permissions = PdfPermissions.AllowAssembly;
                    pd.Security.Permissions = PdfPermissions.AllowCopy;
                    pd.Security.Permissions = PdfPermissions.AllowModifyContents;
                    pd.Security.Permissions = PdfPermissions.AllowAccessibleReaders;

                    pd.Export(this.rptViewDoc.Document, m_strQRPBrowserPath + strStdNumber + "-" + strName);
                    pd.Dispose();

                    //변환완료
                    strFileChk = strStdNumber + "-" + strName;

                }
               
                //엑셀변환
                //export = new XlsExport();
                //((XlsExport)export).Export(this.rptViewDoc.Document, "C:\\Users\\Jong\\Desktop\\버림\\넴.xls");

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

    }
}
