﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINS0004P1.cs                                       */
/* 프로그램명   : 수입검사등록 / 조회 COCFilePopUp                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-03-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPINS.UI
{
    public partial class frmINS0004P1 : Form
    {
        // 다국어 지원을 위한 전역변수
        QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();

        #region 전역변수

        private string m_strPlantCode;
        private string m_strReqNo;
        private string m_strReqSeq;
        private int m_intReqLotSeq;
        private int m_intCOCFileCount;
        private bool bolCompleteFlag;

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string ReqNo
        {
            get { return m_strReqNo; }
            set { m_strReqNo = value; }
        }

        public string ReqSeq
        {
            get { return m_strReqSeq; }
            set { m_strReqSeq = value; }
        }

        public int ReqLotSeq
        {
            get { return m_intReqLotSeq; }
            set { m_intReqLotSeq = value; }
        }

        public int COCFileCount
        {
            get { return m_intCOCFileCount; }
            set { m_intCOCFileCount = value; }
        }

        public bool CompleteFlag
        {
            get { return bolCompleteFlag; }
            set { bolCompleteFlag = value; }
        }

        #endregion

        public frmINS0004P1()
        {
            InitializeComponent();
        }

        private void frmINS0004P1_Load(object sender, EventArgs e)
        {
            System.Resources.ResourceSet m_SysRes = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            this.titleArea.mfSetLabelText("수입검사 COCFile", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 초기화 메소드 호출
            InitButton();
            InitGrid();

            Search();

            if (CompleteFlag)
            {
                this.uButtonSave.Enabled = false;
                this.uButtonDelete.Enabled = false;
                this.uGridCOCFileList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridCOCFileList.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            }
        }

        private void frmINS0004P1_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new QRPCOM.QRPUI.WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        #region 컨트롤 초기화 Method

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinButton wButton = new QRPCOM.QRPUI.WinButton();

                wButton.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonSave, "저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonFileDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                wGrid.mfInitGeneralGrid(this.uGridCOCFileList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridCOCFileList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridCOCFileList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridCOCFileList, 0, "COCFileName", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCOCFileList, 0, "COCFilePath", "파일경로", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridCOCFileList, 0, "DeleteFlag", "삭제여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");

                this.uGridCOCFileList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridCOCFileList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridCOCFileList, 0);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 메소드

        /// <summary>
        /// 조회
        /// </summary>
        private void Search()
        {
            try
            {
                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLotCOCFile), "MatInspectReqLotCOCFile");
                QRPINS.BL.INSIMP.MatInspectReqLotCOCFile clsCOCFile = new QRPINS.BL.INSIMP.MatInspectReqLotCOCFile();
                brwChannel.mfCredentials(clsCOCFile);

                DataTable dtCOCFileList = clsCOCFile.mfReadINSMatInspectReqLotCOCFile(PlantCode, ReqNo, ReqSeq, ReqLotSeq);

                this.uGridCOCFileList.SetDataBinding(dtCOCFileList, string.Empty);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 갯수 반환메소드
        /// </summary>
        /// <returns></returns>
        private int returnCOCFileCount()
        {
            int intCOCFileCount = 0;
            try
            {
                for (int i = 0; i < this.uGridCOCFileList.Rows.Count; i++)
                {
                    if (this.uGridCOCFileList.Rows[i].Cells["DeleteFlag"].Value.ToString().Equals("F"))
                        intCOCFileCount++;
                }
                return intCOCFileCount;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return intCOCFileCount;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 파일을 Byte[] 로 변환하여 반환하는 메소드
        /// </summary>
        /// <param name="strFileFullPath">파일경로</param>
        /// <returns></returns>
        private byte[] fun_ConvertFileToBinray(string strFileFullPath)
        {
            // FileStream 자동 소멸을 위한 Using 구문
            using (System.IO.FileStream fs = new System.IO.FileStream(@strFileFullPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                int intLength = Convert.ToInt32(fs.Length);
                System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                byte[] btBuffer = br.ReadBytes(intLength);
                fs.Close();

                return btBuffer;
            }
        }

        #endregion

        #region 버튼 클릭 이벤트

        // 행삭제버튼
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridCOCFileList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridCOCFileList.Rows[i].Cells["Check"].Value))
                    {
                        this.uGridCOCFileList.Rows[i].Hidden = true;
                        this.uGridCOCFileList.Rows[i].Cells["DeleteFlag"].Value = "T";
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 파일다운로드 버튼
        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLotCOCFile), "MatInspectReqLotCOCFile");
                QRPINS.BL.INSIMP.MatInspectReqLotCOCFile clsCOCFile = new QRPINS.BL.INSIMP.MatInspectReqLotCOCFile();
                brwChannel.mfCredentials(clsCOCFile);

                int intCheck = 0;
                for (int i = 0; i < this.uGridCOCFileList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridCOCFileList.Rows[i].Cells["Check"].Value) &&    // 선택된 상태
                        !this.uGridCOCFileList.Rows[i].Cells["COCFilePath"].Value.ToString().Contains("\\") && // 새로 등록한 첨부파일이 아닐때
                        !this.uGridCOCFileList.Rows[i].Cells["COCFileName"].Value.ToString().Equals(string.Empty))  // 파일명이 존재할때
                    {
                        intCheck++;
                    }

                }

                if (intCheck > 0)
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";

                        for (int i = 0; i < this.uGridCOCFileList.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridCOCFileList.Rows[i].Cells["Check"].Value) &&    // 선택된 상태
                                !this.uGridCOCFileList.Rows[i].Cells["COCFilePath"].Value.ToString().Contains("\\") && // 새로 등록한 첨부파일이 아닐때
                                !this.uGridCOCFileList.Rows[i].Cells["COCFileName"].Value.ToString().Equals(string.Empty))  // 파일명이 존재할때
                            {
                                DataTable dtCOCFile = clsCOCFile.mfReadINSMatInspectReqLotCOCFile_Value(PlantCode, ReqNo, ReqSeq, ReqLotSeq
                                                                                                    , Convert.ToInt32(this.uGridCOCFileList.Rows[i].Cells["Seq"].Value));

                                if (dtCOCFile.Rows.Count > 0)
                                {
                                    string strFileName = dtCOCFile.Rows[0]["COCFileName"].ToString();
                                    byte[] btFileByte = (byte[])dtCOCFile.Rows[0]["COCFileValue"];

                                    System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                                    fs.Write(btFileByte, 0, btFileByte.Length);
                                    fs.Close();
                                }
                            }
                        }

                        // 저장폴더 열기
                        System.Diagnostics.Process p = new System.Diagnostics.Process();
                        p.StartInfo.FileName = strSaveFolder;
                        p.Start();
                    }
                }
                else
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    DialogResult result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Warning, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                    return;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장버튼
        private void uButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLotCOCFile), "MatInspectReqLotCOCFile");
                QRPINS.BL.INSIMP.MatInspectReqLotCOCFile clsCOCFile = new QRPINS.BL.INSIMP.MatInspectReqLotCOCFile();
                brwChannel.mfCredentials(clsCOCFile);

                DataTable dtDeleteCOCFileList = clsCOCFile.mfSetDataInfo();
                DataTable dtSaveCOCFileList = clsCOCFile.mfSetDataInfo();

                for (int i = 0; i < this.uGridCOCFileList.Rows.Count; i++)
                {
                    if (this.uGridCOCFileList.Rows[i].Hidden.Equals(false) &&
                        this.uGridCOCFileList.Rows[i].Cells["COCFilePath"].Value.ToString().Contains("\\") &&
                        this.uGridCOCFileList.Rows[i].Cells["DeleteFlag"].Value.ToString().Equals("F"))
                    {
                        DataRow drRow = dtSaveCOCFileList.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["ReqNo"] = ReqNo;
                        drRow["ReqSeq"] = ReqSeq;
                        drRow["ReqLotSeq"] = ReqLotSeq;
                        drRow["Seq"] = this.uGridCOCFileList.Rows[i].Cells["Seq"].Value;
                        drRow["COCFileName"] = this.uGridCOCFileList.Rows[i].Cells["COCFileName"].Value.ToString();
                        drRow["COCFileValue"] = fun_ConvertFileToBinray(this.uGridCOCFileList.Rows[i].Cells["COCFIlePath"].Value.ToString());
                        drRow["DeleteFlag"] = this.uGridCOCFileList.Rows[i].Cells["DeleteFlag"].Value.ToString();
                        dtSaveCOCFileList.Rows.Add(drRow);
                    }
                    else if (this.uGridCOCFileList.Rows[i].Hidden.Equals(true) &&
                            !this.uGridCOCFileList.Rows[i].Cells["COCFilePath"].Value.ToString().Contains("\\") &&
                        //!this.uGridCOCFileList.Rows[i].Cells["COCFileName"].Value.ToString().Equals(string.Empty) &&
                            this.uGridCOCFileList.Rows[i].Cells["DeleteFlag"].Value.ToString().Equals("T") &&
                            !this.uGridCOCFileList.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                    {
                        byte[] bt = { };
                        DataRow drRow = dtDeleteCOCFileList.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["ReqNo"] = ReqNo;
                        drRow["ReqSeq"] = ReqSeq;
                        drRow["ReqLotSeq"] = ReqLotSeq;
                        drRow["Seq"] = this.uGridCOCFileList.Rows[i].Cells["Seq"].Value;
                        //drRow["COCFileName"] = this.uGridCOCFileList.Rows[i].Cells["COCFileName"].Value.ToString();
                        //drRow["COCFileValue"] = bt;
                        //drRow["DeleteFlag"] = this.uGridCOCFileList.Rows[i].Cells["DeleteFlag"].Value.ToString();
                        dtDeleteCOCFileList.Rows.Add(drRow);
                    }
                }

                if (dtDeleteCOCFileList.Rows.Count > 0 || dtSaveCOCFileList.Rows.Count > 0)
                {
                    string strErrRtn = clsCOCFile.mfSaveINSMatInspectReqLotCOCFile(dtSaveCOCFileList, dtDeleteCOCFileList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    DialogResult Result = new DialogResult();

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        Result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001023"
                            , "M001409"
                            , Infragistics.Win.HAlign.Right);

                        COCFileCount = returnCOCFileCount();
                        this.Close();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001023"
                            , "M001410"
                            , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001023", m_resSys.GetString("SYS_LANG"))
                            , ErrRtn.ErrMessage
                            , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 닫기버튼
        private void uButtonClose_Click(object sender, EventArgs e)
        {
            try
            {
                // 첨부파일 갯수 저장
                COCFileCount = returnCOCFileCount();

                this.Close();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uGridCOCFileList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("COCFileName"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "Word Documents|*.doc;*.docx|Excel Worksheets|*.xls;*.xlsx|PowerPoint Presentations|*.ppt;*.pptx|Office Files|*.doc;*.xls;*.ppt;*.docx;*.xlsx;*.pptx|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                    openFile.FilterIndex = 7;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strFilePath = openFile.FileName;

                        if (strFilePath.Contains("\\"))
                        {
                            System.IO.FileInfo fileDoc = new System.IO.FileInfo(@strFilePath);

                            e.Cell.Value = fileDoc.Name;
                            e.Cell.Row.Cells["COCFilePath"].Value = @strFilePath;

                            QRPCOM.QRPGLO.QRPGlobal grdImg = new QRPCOM.QRPGLO.QRPGlobal();
                            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                            this.uGridCOCFileList.DisplayLayout.Bands[0].AddNew();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridCOCFileList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                if (!e.Row.Cells["COCFilePath"].Value.ToString().Contains("\\") && // 새로 등록한 첨부파일이 아닐때
                    !e.Row.Cells["COCFileName"].Value.ToString().Equals(string.Empty))  // 파일명이 존재할때
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = @saveFolder.SelectedPath + "\\";

                        // BL 연결
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLotCOCFile), "MatInspectReqLotCOCFile");
                        QRPINS.BL.INSIMP.MatInspectReqLotCOCFile clsCOCFile = new QRPINS.BL.INSIMP.MatInspectReqLotCOCFile();
                        brwChannel.mfCredentials(clsCOCFile);
                        DataTable dtCOCFile = clsCOCFile.mfReadINSMatInspectReqLotCOCFile_Value(PlantCode, ReqNo, ReqSeq, ReqLotSeq
                                                                                            , Convert.ToInt32(e.Row.Cells["Seq"].Value));
                        string strFileName = string.Empty;
                        if (dtCOCFile.Rows.Count > 0)
                        {
                            strFileName = dtCOCFile.Rows[0]["COCFileName"].ToString();
                            byte[] btFileByte = (byte[])dtCOCFile.Rows[0]["COCFileValue"];

                            System.IO.FileStream fs = new System.IO.FileStream(@strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                            fs.Write(btFileByte, 0, btFileByte.Length);
                            if (fs.Length.Equals(fs.Position))
                            {
                                fs.Close();
                            }
                        }

                        // 저장폴더 열기
                        if (System.IO.File.Exists(@strSaveFolder + strFileName))
                        {
                            System.Diagnostics.Process p = new System.Diagnostics.Process();
                            p.StartInfo.WorkingDirectory = @strSaveFolder;
                            p.StartInfo.FileName = @strFileName;
                            p.Start();
                        }
                        else
                        {
                            System.Diagnostics.Process p = new System.Diagnostics.Process();
                            p.StartInfo.FileName = @strSaveFolder;
                            p.Start();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
