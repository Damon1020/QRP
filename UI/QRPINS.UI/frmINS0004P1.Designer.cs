﻿namespace QRPINS.UI
{
    partial class frmINS0004P1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINS0004P1));
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridCOCFileList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSave = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCOCFileList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(598, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridCOCFileList
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCOCFileList.DisplayLayout.Appearance = appearance4;
            this.uGridCOCFileList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCOCFileList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCOCFileList.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCOCFileList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGridCOCFileList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCOCFileList.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.uGridCOCFileList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCOCFileList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCOCFileList.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCOCFileList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridCOCFileList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCOCFileList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCOCFileList.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCOCFileList.DisplayLayout.Override.CellAppearance = appearance5;
            this.uGridCOCFileList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCOCFileList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCOCFileList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.uGridCOCFileList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridCOCFileList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCOCFileList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGridCOCFileList.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGridCOCFileList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCOCFileList.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.uGridCOCFileList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCOCFileList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCOCFileList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCOCFileList.Location = new System.Drawing.Point(0, 40);
            this.uGridCOCFileList.Name = "uGridCOCFileList";
            this.uGridCOCFileList.Size = new System.Drawing.Size(596, 298);
            this.uGridCOCFileList.TabIndex = 1;
            this.uGridCOCFileList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridCOCFileList_DoubleClickRow);
            this.uGridCOCFileList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCOCFileList_ClickCellButton);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(508, 338);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 2;
            this.uButtonClose.Text = "닫기";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uButtonSave
            // 
            this.uButtonSave.Location = new System.Drawing.Point(420, 338);
            this.uButtonSave.Name = "uButtonSave";
            this.uButtonSave.Size = new System.Drawing.Size(88, 28);
            this.uButtonSave.TabIndex = 3;
            this.uButtonSave.Text = "저장";
            this.uButtonSave.Click += new System.EventHandler(this.uButtonSave_Click);
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(244, 338);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 4;
            this.uButtonDelete.Text = "행삭제";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(332, 338);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDown.TabIndex = 5;
            this.uButtonFileDown.Text = "파일다운";
            this.uButtonFileDown.Click += new System.EventHandler(this.uButtonFileDown_Click);
            // 
            // frmINS0004P1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(598, 366);
            this.ControlBox = false;
            this.Controls.Add(this.uButtonFileDown);
            this.Controls.Add(this.uButtonDelete);
            this.Controls.Add(this.uButtonSave);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uGridCOCFileList);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINS0004P1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmINS0004P1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINS0004P1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCOCFileList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCOCFileList;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.Misc.UltraButton uButtonSave;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
    }
}