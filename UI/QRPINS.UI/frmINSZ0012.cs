﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0012.cs                                        */
/* 프로그램명   : SPCN LIST                                             */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0012 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmINSZ0012()
        {
            InitializeComponent();
        }

        private void frmINSZ0012_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0012_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;

            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("SPCN LIST", m_SysRes.GetString("SYS_FONTNAME"), 12);

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            // 컨트롤 초기화
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            initEtc();
            InitComboBox();
            InitGrid();

            this.uGroupBoxContentsArea.Expanded = false;
        }

        private void frmINSZ0012_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        #region 공통 Event
        // ContestsArea 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                ////if (this.uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    Point point = new Point(0, 145);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridSPCNList.Height = 60;
                ////}
                ////else
                ////{
                ////    Point point = new Point(0, 825);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridSPCNList.Height = 740;

                ////    for (int i = 0; i < this.uGridSPCNList.Rows.Count; i++)
                ////    {
                ////        this.uGridSPCNList.Rows[i].Fixed = false;
                ////    }

                ////    // 초기화
                ////    InitClear();
                ////}

                if (this.uGroupBoxContentsArea.Expanded)
                {
                    this.uGridSPCNList.Rows.FixedRows.Clear();

                    InitClear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "측정Data", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "조치사항", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.INFO, "SPCN Lot 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.INFO, "SPCN 항목 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.ultraGroupBox1, GroupBoxType.INFO, "X-Bar 관리도", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                
                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;

                this.ultraGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.ultraGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);

                ////Size size = new Size(1070, 680);
                ////this.uGroupBoxContentsArea.MaximumSize = size;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void initEtc()
        {
            try
            {
                //Desc TextBox 스크롤바 생성
                this.uTextProblemDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextProblemDesc.SelectionStart = uTextProblemDesc.Text.Length;
                this.uTextProblemDesc.ScrollToCaret();

                this.uTextCauseDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextCauseDesc.SelectionStart = uTextCauseDesc.Text.Length;
                this.uTextCauseDesc.ScrollToCaret();

                this.uTextPreventionDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextPreventionDesc.SelectionStart = uTextPreventionDesc.Text.Length;
                this.uTextPreventionDesc.ScrollToCaret();

                this.uTextResultDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextResultDesc.SelectionStart = uTextResultDesc.Text.Length;
                this.uTextResultDesc.ScrollToCaret();

                this.uTextProblemDesc.MaxLength = 200;
                this.uTextProblemUserID.MaxLength = 20;
                this.uTextCauseDesc.MaxLength = 200;
                this.uTextCauseUserID.MaxLength = 20;
                this.uTextPreventionDesc.MaxLength = 200;
                this.uTextPreventionUserID.MaxLength = 20;
                this.uTextResultDesc.MaxLength = 200;
                this.uTextResultUserID.MaxLength = 20;

                this.uCheckCycle.Hide();
                this.uTextProductCode.Hide();
                this.uTextProductName.Hide();
                this.uTextCustomerName.Hide();

                //작성시간 현재 시간으로
                this.uDateWrieteTime.Value = DateTime.Now.ToString("HH:mm:ss");

                this.uCheckComplete.CheckedChanged += new EventHandler(uCheckComplete_CheckedChanged);
                this.uTextCauseUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextCauseUserID_EditorButtonClick);
                this.uTextCauseUserID.KeyDown += new KeyEventHandler(uTextCauseUserID_KeyDown);
                this.uTextPreventionUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextPreventionUserID_EditorButtonClick);
                this.uTextPreventionUserID.KeyDown += new KeyEventHandler(uTextPreventionUserID_KeyDown);
                this.uTextProblemUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextProblemUserID_EditorButtonClick);
                this.uTextProblemUserID.KeyDown += new KeyEventHandler(uTextProblemUserID_KeyDown);
                this.uTextResultUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextResultUserID_EditorButtonClick);
                this.uTextResultUserID.KeyDown += new KeyEventHandler(uTextResultUserID_KeyDown);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCompleteFlag, "완료여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRegisterDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRegister, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSPCUnfitType, "SPC부적합 유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRegisterDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerProductSpec, "고객사제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessCode, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPACKAGE, "PACKAGE", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectItem, "검사항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLSL, "LSL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUSL, "USL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMax, "Max", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMin, "Min", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAvg, "Avg", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelstdDeviation, "표준편차", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRange, "Range", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCp, "Cp", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCpk, "Cpk", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLCL, "LCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUCL, "UCL", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCL, "CL", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //조치사항 Label
                wLabel.mfSetLabel(this.uLabel11, "문제점", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel12, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel13, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel14, "원인분석", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel15, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel16, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel17, "재발방지대책", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel18, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel19, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel20, "처리결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel21, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel22, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCopleteFlag, "작성완료", m_resSys.GetString("SYS_FONTNAME"), true, false);               
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 검색조건 : 공장 콤보박스
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommon = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommon);

                DataTable dtCommon = clsCommon.mfReadCommonCode("C0056", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchCompleteFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCommon);

                this.uComboSearchPlant.ValueChanged += new EventHandler(uComboSearchPlant_ValueChanged);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSPCNList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ReqNo", "의뢰번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ReqSeq", "의뢰순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 40
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "SampleSize", "S/S", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "Mean", "Avg", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "MaxValue", "Max", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "CauseDesc", "발생원인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 200
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ResultDesc", "조치사항", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 200
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "CompleteFlag", "완료여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "WriteUserName", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "WriteDate", "등록일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "OCP", "관리한계선이탈", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "UpperRun", "상향런", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "LowerRun", "하향런", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "UpperTrend", "상향경향", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "LowerTrend", "하향경향", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //순서에 없는 항목들은 Hidden처리
                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ReqLotSeq", "의뢰Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "EquipName", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");                

                wGrid.mfSetGridColumn(this.uGridSPCNList, 0, "MESTFlag", "MES전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region 측정Data 그리드
                // 측정 Data 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridValueData, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                ////wGrid.mfSetGridColumn(this.uGridValueData, 0, "ReqResultSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridValueData, 0, "InspectValue", "측정값", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueData, 0, "ReqNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueData, 0, "ReqSeq", "관리번호순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 4
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueData, 0, "ReqLotSeq", "Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueData, 0, "ReqItemSeq", "Item순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueData, 0, "InspectDateTime", "등록일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueData, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueData, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 편집불가 상태로
                this.uGridValueData.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridValueData.DisplayLayout.Bands[0].Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
                #endregion
                // 그리드 Sort 방지
                this.uGridValueData.DisplayLayout.Bands[0].Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
                // Set FontSize
                this.uGridSPCNList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridSPCNList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // Set FontSize
                this.uGridValueData.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridValueData.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridSPCNList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridSPCNList_DoubleClickRow);
                this.uGridValueData.DoubleClickRow +=new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridValueData_DoubleClickRow);

                Size size = new Size(1070, 90);
                this.uGridSPCNList.MinimumSize = size;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화면 초기화
        /// </summary>
        private void InitClear()
        {
            try
            {
                this.uTextPlantCode.Text = "";
                this.uTextPlantName.Text = "";
                this.uTextProductCode.Text = "";
                this.uTextProductName.Clear();
                this.uTextCustomerProductSpec.Text = "";
                this.uTextEquipCode.Clear();
                this.uTextEquipName.Text = "";
                this.uTextProcessCode.Clear();
                this.uTextProcessName.Text = "";
                this.uTextCustomerCode.Text = "";
                this.uTextCustomerName.Text = "";
                this.uTextLotNo.Text = "";
                this.uCheckOCP.Checked = false;
                this.uCheckUpperRun.Checked = false;
                this.uCheckLowerRun.Checked = false;
                this.uCheckUpperTrend.Checked = false;
                this.uCheckLowerTrend.Checked = false;
                this.uCheckCycle.Checked = false;
                this.uTextPackage.Text = "";
                this.uTextReqNo.Text = "";
                this.uTextReqSeq.Text = "";
                this.uTextReqLotSeq.Text = "";
                this.uTextReqItemSeq.Text = "";
                this.uTextWriteDate.Text = "";
                this.uTextWriteUserID.Text = "";
                this.uTextWriteUserName.Text = "";
                this.uTextInspectItemCode.Clear();
                this.uTextInspectItemName.Text = "";
                this.uTextStdDev.Text = "";
                this.uTextAvg.Text = "";
                this.uTextLSL.Text = "";
                this.uTextUSL.Text = "";
                this.uTextSpecRange.Text = "";
                this.uTextRange.Text = "";
                this.uTextMax.Text = "";
                this.uTextMin.Text = "";
                this.uTextCp.Text = "";
                this.uTextCpk.Text = "";
                this.uTextProblemDesc.Text = "";
                this.uTextProblemUserID.Text = "";
                this.uTextProblemUserName.Text = "";
                this.uDateProblemDate.Value = DateTime.Today;
                this.uTextCauseDesc.Text = "";
                this.uTextCauseUserID.Text = "";
                this.uTextCauseUserName.Text = "";
                this.uDateCauseDate.Value = DateTime.Today; ;
                this.uTextPreventionDesc.Text = "";
                this.uTextPreventionUserID.Text = "";
                this.uTextPreventionUserName.Text = "";
                this.uDatePreventionDate.Value = DateTime.Today; ;
                this.uTextResultDesc.Text = "";
                this.uTextResultUserID.Text = "";
                this.uTextResultUserName.Text = "";
                this.uDateResultDate.Value = DateTime.Today;
                this.uCheckComplete.Checked = false;

                while (this.uGridValueData.Rows.Count > 0)
                {
                    this.uGridValueData.Rows[0].Delete(false);
                }

                QRPSTA.STASPC clsXchart = new QRPSTA.STASPC();
                clsXchart.mfInitControlChart(this.uChartX);

                this.uTextCL.Clear();
                this.uTextUCL.Clear();
                this.uTextLCL.Clear();
                this.uTextPackage.Clear();
                this.uTextGeneration.Clear();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        #endregion
                
        #region ToolBar Method
        // 검색
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건 변수 생성
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strCompleteFlag = this.uComboSearchCompleteFlag.Value.ToString();                
                String strProcessCode = this.uComboSearchProcess.Value.ToString();
                String strWriteFromDate = Convert.ToDateTime(this.uDateSearchWriteFromDate.Value).ToString("yyyy-MM-dd");
                String strWriteToDate = Convert.ToDateTime(this.uDateSearchWriteToDate.Value).ToString("yyyy-MM-dd");                

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.SPCN), "SPCN");
                QRPINS.BL.INSPRC.SPCN clsSPCN = new QRPINS.BL.INSPRC.SPCN();
                brwChannel.mfCredentials(clsSPCN);
                
                
                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 검색 Method 호출
                //DataTable dtSearch = clsSPCN.mfReadSPCNList(strPlantCode, strProductCode, strLotNo, strProcessCode, strWriteFromDate, strWriteToDate, m_resSys.GetString("SYS_LANG"));

                DataTable dtSearch = clsSPCN.mfReadSPCNList(strPlantCode, strCompleteFlag, strProcessCode, strWriteFromDate, strWriteToDate, m_resSys.GetString("SYS_LANG"));

                this.uGridSPCNList.DataSource = dtSearch;
                this.uGridSPCNList.DataBind();

                for (int i = 0; i < this.uGridSPCNList.Rows.Count; i++)
                {
                    if (this.uGridSPCNList.Rows[i].Cells["CompleteFlag"].Value.ToString().Equals("T") && this.uGridSPCNList.Rows[i].Cells["MESTFlag"].Value.ToString().Equals("F"))
                    {
                        this.uGridSPCNList.Rows[i].Appearance.BackColor = Color.Salmon;
                    }                    
                }

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSPCNList, 0);
                }

                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 헤더 필수사항 확인
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M001042", Infragistics.Win.HAlign.Right);
                    return;
                }

                // 작성완료 체크 후 저장시 처리결과 작성자 필요 입력 체크
                if (this.uCheckComplete.Checked == true && this.uTextResultUserName.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    "M001264", "M001053", "M001136",
                    Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uCheckComplete.Enabled == false && this.uTextMESTFlag.Text.Equals("T"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000984", "M000991", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uCheckComplete.Enabled == true && this.uCheckComplete.Checked == true)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000984", "M000982",
                                    Infragistics.Win.HAlign.Right);
                }

                if (this.uTextPlantCode.Text.Equals(string.Empty) ||this.uTextReqNo.Text.Equals(string.Empty) ||
                    this.uTextReqSeq.Text.Equals(string.Empty) || this.uTextReqLotSeq.Text.Equals(string.Empty) ||
                    this.uTextReqItemSeq.Text.Equals(string.Empty))
                    return;

                 // 저장여부 확인 메세지
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 작성완료 되어 저장되었으면 MESI/F 만 진행
                    if (this.uCheckComplete.Enabled.Equals(true))
                    {

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.SPCN), "SPCN");
                        QRPINS.BL.INSPRC.SPCN clsSPCN = new QRPINS.BL.INSPRC.SPCN();
                        brwChannel.mfCredentials(clsSPCN);

                        // DataTabel에 저장할 항목을 담는다.
                        DataTable dtSave = clsSPCN.mfSetDataInfo();
                        DataRow drRow = dtSave.NewRow();
                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["ReqNo"] = this.uTextReqNo.Text;
                        drRow["ReqSeq"] = this.uTextReqSeq.Text;
                        drRow["ReqLotSeq"] = this.uTextReqLotSeq.Text;
                        drRow["ReqItemSeq"] = this.uTextReqItemSeq.Text;
                        drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                        drRow["WriteDate"] = this.uTextWriteDate.Text;
                        drRow["WriteTime"] = Convert.ToDateTime(this.uDateWrieteTime.Value).ToString("HH:mm:ss");
                        if (this.uCheckOCP.Checked)
                            drRow["OCP"] = "T";
                        else
                            drRow["OCP"] = "F";

                        if (this.uCheckUpperRun.Checked)
                            drRow["UpperRun"] = "T";
                        else
                            drRow["UpperRun"] = "F";

                        if (this.uCheckLowerRun.Checked)
                            drRow["LowerRun"] = "T";
                        else
                            drRow["LowerRun"] = "F";

                        if (this.uCheckUpperTrend.Checked)
                            drRow["UpperTrend"] = "T";
                        else
                            drRow["UpperTrend"] = "F";

                        if (this.uCheckOCP.Checked)
                            drRow["LowerTrend"] = "T";
                        else
                            drRow["LowerTrend"] = "F";

                        if (this.uCheckCycle.Checked)
                            drRow["Cycle"] = "T";
                        else
                            drRow["Cycle"] = "F";
                        drRow["ProblemDesc"] = this.uTextProblemDesc.Text;
                        drRow["ProblemUserID"] = this.uTextProblemUserID.Text;
                        drRow["ProblemDate"] = this.uDateProblemDate.Value.ToString();
                        drRow["CauseDesc"] = this.uTextCauseDesc.Text;
                        drRow["CauseUserID"] = this.uTextCauseUserID.Text;
                        drRow["CauseDate"] = this.uDateCauseDate.Value.ToString();
                        drRow["PreventionDesc"] = this.uTextPreventionDesc.Text;
                        drRow["PreventionUserID"] = this.uTextPreventionUserID.Text;
                        drRow["PreventionDate"] = this.uDatePreventionDate.Value.ToString();
                        drRow["ResultDesc"] = this.uTextResultDesc.Text;
                        drRow["ResultUserID"] = this.uTextResultUserID.Text;
                        drRow["ResultDate"] = this.uDateResultDate.Value.ToString();
                        if (this.uCheckComplete.Checked)
                            drRow["CompleteFlag"] = "T";
                        else
                            drRow["CompleteFlag"] = "F";

                        dtSave.Rows.Add(drRow);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장 Method 호출                    
                        string strErrRtn = clsSPCN.mfSaveSPCNSave(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            // 작성완료 상태에서 MES I/F가 진행안되었으면 MES I/F진행
                            if (this.uCheckComplete.Checked.Equals(true) && !this.uTextMESTFlag.Text.Equals("T"))
                            {
                                MESInterFace();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000930",
                                                        Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        // 작성완료인 경우 MES I/F만 진행
                        MESInterFace();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 삭제
        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        // 생성
        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        // 출력
        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        // 엑셀
        public void mfExcel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridSPCNList.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridSPCNList);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M000803", "M000809", "M000375",
                                Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 1.검색조건 Event, Method
        // 공장선택시 공정정보 Display
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                DataTable dtProcess = new DataTable();

                this.uComboSearchProcess.Items.Clear();

                if (strPlantCode != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //// 제품코드 팝업창

        //private void uTextSearchProductID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        //{
        //    WinMessageBox msg = new WinMessageBox();
        //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //    try
        //    {
        //        if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
        //        {
        //            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        //                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                            "확인창", "입력확인", "공장을 선택해주세요.",
        //                            Infragistics.Win.HAlign.Right);
        //            return;
        //        }
        //        frmPOP0002 frmPOP = new frmPOP0002();
        //        frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
        //        frmPOP.ShowDialog();

        //        this.uTextSearchProductCode.Text = frmPOP.ProductCode;
        //        this.uTextSearchProductName.Text = frmPOP.ProductName;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}

        //// 제품코드 입력 후 엔터 시 제품명을 가지고 온다.
        //private void uTextSearchProductCode_KeyDown(object sender, KeyEventArgs e)
        //{
        //    try
        //    {
        //        if (e.KeyCode == Keys.Enter)
        //        {
        //            if (this.uTextSearchProductCode.Text == "")
        //            {
        //                this.uTextSearchProductName.Text = "";
        //            }
        //            else
        //            {
        //                // SystemInfo ResourceSet
        //                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //                WinMessageBox msg = new WinMessageBox();

        //                if (this.uComboSearchPlant.Value.ToString() == "")
        //                {
        //                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                                    "확인창", "입력확인", "공장을 선택해주세요.",
        //                                    Infragistics.Win.HAlign.Right);

        //                    this.uComboSearchPlant.DropDown();
        //                }
        //                else
        //                {
        //                    String strPlantCode = this.uComboSearchPlant.Value.ToString();
        //                    String strProductCode = this.uTextSearchProductCode.Text;

        //                    //제품명 조회 메소드 호출하여 처리결과 정보를 리턴 받는다.
        //                    DataTable dtProduct = GetProductInfo(strPlantCode, strProductCode);

        //                    if (dtProduct.Rows.Count > 0)
        //                    {
        //                        for (int i = 0; i < dtProduct.Rows.Count; i++)
        //                        {
        //                            this.uTextSearchProductName.Text = dtProduct.Rows[i]["ProductName"].ToString();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                                    "확인창", "입력확인", "제품을 찾을수 없습니다",
        //                                    Infragistics.Win.HAlign.Right);

        //                        this.uTextSearchProductName.Text = "";
        //                        this.uTextSearchProductCode.Text = "";
        //                    }
        //                }
        //            }
        //        }

        //        if (e.KeyCode == Keys.Back)
        //        {
        //            if (this.uTextSearchProductCode.TextLength <= 1 || this.uTextSearchProductCode.Text == this.uTextSearchProductCode.SelectedText)
        //            {
        //                this.uTextSearchProductName.Text = "";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}

        // 제품명 조회 
        private DataTable GetProductInfo(String strPlantCode, String strProductCode)
        {
            DataTable dtProduct = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                dtProduct = clsProduct.mfReadMASMaterialDetail(strPlantCode, strProductCode, m_resSys.GetString("SYS_LANG"));

                return dtProduct;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtProduct;
            }
            finally
            {
            }
        }

        #endregion

        #region 2.상세정보 작성자 Event, Method
        // 1.문제점 작성자 팝업창
        private void uTextProblemUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                // SystemInfor ResourceSet

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlantCode.Text;
                frmPOP.ShowDialog();

                if (this.uTextPlantCode.Text != frmPOP.PlantCode)
                {
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                , msg.GetMessge_Text("M001254", strLang) + this.uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextProblemUserID.Text = "";
                    this.uTextProblemUserName.Text = "";
                }
                else
                {
                    this.uTextProblemUserID.Text = frmPOP.UserID;
                    this.uTextProblemUserName.Text = frmPOP.UserName;
                }                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 1.문제점 작성자 엔터시 작성자명을 가지고 온다.
        private void uTextProblemUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextProblemUserID.Text == "")
                    {
                        this.uTextProblemUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strPlantCode = this.uTextPlantCode.Text;
                        String strUserID = this.uTextProblemUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextProblemUserID.Text = "";
                            this.uTextProblemUserName.Text = "";
                        }
                        else
                        {
                            this.uTextProblemUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextProblemUserID.TextLength <= 1 || this.uTextProblemUserID.Text == this.uTextProblemUserID.SelectedText)
                    {
                        this.uTextProblemUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 2.원인분석 작성자 팝업창
        private void uTextCauseUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlantCode.Text;
                frmPOP.ShowDialog();

                if (this.uTextPlantCode.Text != frmPOP.PlantCode)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                , msg.GetMessge_Text("M001254", strLang) + this.uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextCauseUserID.Text = "";
                    this.uTextCauseUserName.Text = "";
                }
                else
                {
                    this.uTextCauseUserID.Text = frmPOP.UserID;
                    this.uTextCauseUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 2.원인분석 작성자 엔터시 작성자명을 가지고 온다.
        private void uTextCauseUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextCauseUserID.Text == "")
                    {
                        this.uTextCauseUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strPlantCode = this.uTextPlantCode.Text;
                        String strUserID = this.uTextCauseUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextCauseUserID.Text = "";
                            this.uTextCauseUserName.Text = "";
                        }
                        else
                        {
                            this.uTextCauseUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextCauseUserID.TextLength <= 1 || this.uTextCauseUserID.Text == this.uTextCauseUserID.SelectedText)
                    {
                        this.uTextCauseUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 3.재발방지대책 작성자 팝업창
        private void uTextPreventionUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {               
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlantCode.Text;
                frmPOP.ShowDialog();

                if (this.uTextPlantCode.Text != frmPOP.PlantCode)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                , msg.GetMessge_Text("M001254", strLang) + this.uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextPreventionUserID.Text = "";
                    this.uTextPreventionUserName.Text = "";
                }
                else
                {
                    this.uTextPreventionUserID.Text = frmPOP.UserID;
                    this.uTextPreventionUserName.Text = frmPOP.UserName;
                }                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 3.재발방지대책 작성자 엔터시 작성자명을 가지고 온다.
        private void uTextPreventionUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextPreventionUserID.Text == "")
                    {
                        this.uTextPreventionUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strPlantCode = this.uTextPlantCode.Text;
                        String strUserID = this.uTextPreventionUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextPreventionUserID.Text = "";
                            this.uTextPreventionUserName.Text = "";
                        }
                        else
                        {
                            this.uTextPreventionUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextPreventionUserID.TextLength <= 1 || this.uTextPreventionUserID.Text == this.uTextPreventionUserID.SelectedText)
                    {
                        this.uTextPreventionUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 4.처리결과 작성자 팝업창
        private void uTextResultUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlantCode.Text;
                frmPOP.ShowDialog();

                if (this.uTextPlantCode.Text != frmPOP.PlantCode)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                , msg.GetMessge_Text("M001254", strLang) + this.uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextResultUserID.Text = "";
                    this.uTextResultUserName.Text = "";
                }
                else
                {
                    this.uTextResultUserID.Text = frmPOP.UserID;
                    this.uTextResultUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 4.처리결과 작성자 엔터시 작성자명을 가지고 온다.
        private void uTextResultUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextResultUserID.Text == "")
                    {
                        this.uTextResultUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strPlantCode = this.uTextPlantCode.Text;
                        String strUserID = this.uTextResultUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserName(strPlantCode, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextResultUserID.Text = "";
                            this.uTextResultUserName.Text = "";
                        }
                        else
                        {
                            this.uTextResultUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextResultUserID.TextLength <= 1 || this.uTextResultUserID.Text == this.uTextResultUserID.SelectedText)
                    {
                        this.uTextResultUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // UserName 조회
        private String GetUserName(String strPlantCode, String strCreateUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strCreateUserID, m_resSys.GetString("SYS_LANG"));

                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }

                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        #endregion

        #region 3.List 더블클릭Event
        private void uGridSPCNList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //////상세정보 초기화
                ////InitClear();

                // 더블클릭된 행 고정
                e.Row.Fixed = true;
                // ContentsArea 펼침 상태로
                this.uGroupBoxContentsArea.Expanded = true;

                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strReqNo = e.Row.Cells["ReqNo"].Value.ToString();
                String strReqSeq = e.Row.Cells["ReqSeq"].Value.ToString();
                int intReqLotSeq = Convert.ToInt32(e.Row.Cells["ReqLotSeq"].Value.ToString());
                int intReqItemSeq = Convert.ToInt32(e.Row.Cells["ReqItemSeq"].Value.ToString());

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.SPCN), "SPCN");
                QRPINS.BL.INSPRC.SPCN clsDetail = new QRPINS.BL.INSPRC.SPCN();
                brwChannel.mfCredentials(clsDetail);

                #region 3.1 상세정보 조회 Method 호출
                // BL 연결                                                              
                DataTable dtDetail = clsDetail.mfReadSPCNDetail(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, intReqItemSeq, m_resSys.GetString("SYS_LANG"));

                this.uTextPlantCode.Text = dtDetail.Rows[0]["PlantCode"].ToString();
                this.uTextPlantName.Text = dtDetail.Rows[0]["PlantName"].ToString();
                this.uTextProductCode.Text = dtDetail.Rows[0]["ProductCode"].ToString();
                this.uTextProductName.Text = dtDetail.Rows[0]["ProductName"].ToString();
                this.uTextCustomerProductSpec.Text = dtDetail.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                this.uTextEquipCode.Text = dtDetail.Rows[0]["EquipCode"].ToString();
                this.uTextEquipName.Text = dtDetail.Rows[0]["EquipName"].ToString();
                this.uTextProcessCode.Text = dtDetail.Rows[0]["ProcessCode"].ToString();
                this.uTextProcessName.Text = dtDetail.Rows[0]["ProcessName"].ToString();
                this.uTextCustomerCode.Text = dtDetail.Rows[0]["CustomerCode"].ToString();
                this.uTextCustomerName.Text = dtDetail.Rows[0]["CustomerName"].ToString();
                this.uTextLotNo.Text = dtDetail.Rows[0]["LotNo"].ToString();
                this.uTextPackage.Text = dtDetail.Rows[0]["PACKAGE"].ToString();

                if (dtDetail.Rows[0]["OCP"].ToString() == "T")
                {
                    this.uCheckOCP.Checked = true;
                }
                else
                {
                    this.uCheckOCP.Checked = false;
                }
                this.uCheckOCP.Enabled = false;
                this.uCheckOCP.Appearance.BackColorDisabled = Color.White ;
                this.uCheckOCP.Appearance.ForeColorDisabled= Color.Black;

                if (dtDetail.Rows[0]["UpperRun"].ToString() == "T")
                {
                    this.uCheckUpperRun.Checked = true;
                }
                else
                {
                    this.uCheckUpperRun.Checked = false;
                }
                this.uCheckUpperRun.Enabled = false;
                this.uCheckUpperRun.Appearance.BackColorDisabled = Color.White;
                this.uCheckUpperRun.Appearance.ForeColorDisabled = Color.Black;

                if (dtDetail.Rows[0]["LowerRun"].ToString() == "T")
                {
                    this.uCheckLowerRun.Checked = true;
                }
                else
                {
                    this.uCheckLowerRun.Checked = false;
                }
                this.uCheckLowerRun.Enabled = false;
                this.uCheckLowerRun.Appearance.BackColorDisabled = Color.White;
                this.uCheckLowerRun.Appearance.ForeColorDisabled = Color.Black;

                if (dtDetail.Rows[0]["UpperTrend"].ToString() == "T")
                {
                    this.uCheckUpperTrend.Checked = true;
                }
                else
                {
                    this.uCheckUpperTrend.Checked = false;
                }
                this.uCheckUpperTrend.Enabled = false;
                this.uCheckUpperTrend.Appearance.BackColorDisabled = Color.White;
                this.uCheckUpperTrend.Appearance.ForeColorDisabled = Color.Black;

                if (dtDetail.Rows[0]["LowerTrend"].ToString() == "T")
                {
                    this.uCheckLowerTrend.Checked = true;
                }
                else
                {
                    this.uCheckLowerTrend.Checked = false;
                }
                this.uCheckLowerTrend.Enabled = false;
                this.uCheckLowerTrend.Appearance.BackColorDisabled = Color.White;
                this.uCheckLowerTrend.Appearance.ForeColorDisabled = Color.Black;

                if (dtDetail.Rows[0]["Cycle"].ToString() == "T")
                {
                    this.uCheckCycle.Checked = true;
                }
                else
                {
                    this.uCheckCycle.Checked = false;
                }
                this.uCheckCycle.Enabled = false;
                this.uCheckCycle.Appearance.BackColorDisabled = Color.White;
                this.uCheckCycle.Appearance.ForeColorDisabled = Color.Black;

                this.uTextReqNo.Text = dtDetail.Rows[0]["ReqNo"].ToString();
                this.uTextReqSeq.Text = dtDetail.Rows[0]["ReqSeq"].ToString();
                this.uTextReqLotSeq.Text = dtDetail.Rows[0]["ReqLotSeq"].ToString();
                this.uTextReqItemSeq.Text = dtDetail.Rows[0]["ReqItemSeq"].ToString();
                this.uTextWriteDate.Text = dtDetail.Rows[0]["WriteDate"].ToString();
                this.uDateWrieteTime.Value = dtDetail.Rows[0]["WriteTime"].ToString();
                this.uTextWriteUserID.Text = dtDetail.Rows[0]["WriteUserID"].ToString();
                this.uTextWriteUserName.Text = dtDetail.Rows[0]["WriteUserName"].ToString();
                this.uTextInspectItemCode.Text = dtDetail.Rows[0]["InspectItemCode"].ToString();
                this.uTextInspectItemName.Text = dtDetail.Rows[0]["InspectItemName"].ToString();
                this.uTextStdDev.Text = dtDetail.Rows[0]["StdDev"].ToString();
                this.uTextAvg.Text = dtDetail.Rows[0]["Mean"].ToString();                
                this.uTextLSL.Text = dtDetail.Rows[0]["LSL"].ToString();                
                this.uTextUSL.Text = dtDetail.Rows[0]["USL"].ToString();
                this.uTextSpecRange.Text = dtDetail.Rows[0]["SpecRange"].ToString();
                this.uTextRange.Text = dtDetail.Rows[0]["DataRange"].ToString();
                this.uTextMax.Text = dtDetail.Rows[0]["MaxValue"].ToString();
                this.uTextMin.Text = dtDetail.Rows[0]["MinValue"].ToString();
                this.uTextCp.Text = dtDetail.Rows[0]["Cp"].ToString();
                this.uTextCpk.Text = dtDetail.Rows[0]["Cpk"].ToString();                
                this.uTextProblemDesc.Text = dtDetail.Rows[0]["ProblemDesc"].ToString();
                this.uTextProblemUserID.Text = dtDetail.Rows[0]["ProblemUserID"].ToString();
                this.uTextProblemUserName.Text = dtDetail.Rows[0]["ProblemUserName"].ToString();
                if (dtDetail.Rows[0]["ProblemDate"].ToString() == "")
                    this.uDateProblemDate.Value = DateTime.Today;                
                else
                    this.uDateProblemDate.Value = dtDetail.Rows[0]["ProblemDate"].ToString();                
                this.uTextCauseDesc.Text = dtDetail.Rows[0]["CauseDesc"].ToString();
                this.uTextCauseUserID.Text = dtDetail.Rows[0]["CauseUserID"].ToString();
                this.uTextCauseUserName.Text = dtDetail.Rows[0]["CauseUserName"].ToString();
                if (dtDetail.Rows[0]["CauseDate"].ToString() == "")
                    this.uDateCauseDate.Value = DateTime.Today;
                else
                    this.uDateCauseDate.Value = dtDetail.Rows[0]["CauseDate"].ToString();
                this.uTextPreventionDesc.Text = dtDetail.Rows[0]["PreventionDesc"].ToString();
                this.uTextPreventionUserID.Text = dtDetail.Rows[0]["PreventionUserID"].ToString();
                this.uTextPreventionUserName.Text = dtDetail.Rows[0]["PreventionUserName"].ToString();
                if (dtDetail.Rows[0]["PreventionDate"].ToString() == "")
                    this.uDatePreventionDate.Value = DateTime.Today;
                else
                    this.uDatePreventionDate.Value = dtDetail.Rows[0]["PreventionDate"].ToString();
                this.uTextResultDesc.Text = dtDetail.Rows[0]["ResultDesc"].ToString();
                this.uTextResultUserID.Text = dtDetail.Rows[0]["ResultUserID"].ToString();
                this.uTextResultUserName.Text = dtDetail.Rows[0]["ResultUserName"].ToString();
                if (dtDetail.Rows[0]["ResultDate"].ToString() == "")
                    this.uDateResultDate.Value = DateTime.Today;
                else
                    this.uDateResultDate.Value = dtDetail.Rows[0]["ResultDate"].ToString();
                if (dtDetail.Rows[0]["CompleteFlag"].ToString() == "T")
                {
                    this.uCheckComplete.Checked = true;
                    this.uCheckComplete.Enabled = false;
                }
                else
                {
                    this.uCheckComplete.Checked = false;
                    this.uCheckComplete.Enabled = true;
                }
                this.uTextMESTFlag.Text = dtDetail.Rows[0]["MESTFlag"].ToString();

                this.uTextUCL.Text = dtDetail.Rows[0]["UCL"].ToString();
                this.uTextLCL.Text = dtDetail.Rows[0]["LCL"].ToString();
                this.uTextCL.Text = dtDetail.Rows[0]["CL"].ToString();
                this.uTextPackage.Text = dtDetail.Rows[0]["Package"].ToString();
                this.uTextWriteTime.Text = dtDetail.Rows[0]["InspectTime"].ToString();
                this.uTextStackSeq.Text = dtDetail.Rows[0]["StackSeq"].ToString();
                this.uTextGeneration.Text = dtDetail.Rows[0]["Generation"].ToString();
                #endregion

                #region 3.2 측정Data값 조회 Method 호출

                ////// 조회 Method 호출
                ////DataTable dtValue = clsDetail.mfReadSPCNDetailValue(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, intReqItemSeq);
                
                ////// 그리드 Databind
                ////this.uGridValueData.DataSource = dtValue;
                ////this.uGridValueData.DataBind();

                ////if (dtValue.Rows.Count > 0)
                ////{
                ////    WinGrid grd = new WinGrid();
                ////    grd.mfSetAutoResizeColWidth(this.uGridValueData, 0);
                ////}

                #endregion

                #region 3.3 X관리도
                #region 기존소스 주석처리 - 1
                /*
                double dblLSL;
                double dblUSL;

                if (this.uTextLSL.Text == "")
                    dblLSL = Convert.ToDouble("0");    
                else
                    dblLSL = Convert.ToDouble(this.uTextLSL.Text);

                if (this.uTextUSL.Text == "")
                    dblUSL = Convert.ToDouble("0");
                else
                    dblUSL = Convert.ToDouble(this.uTextUSL.Text);   
 
                QRPSTA.STASPC clsXchart = new QRPSTA.STASPC();

                // 관리 상/하한 한계썬 가져오는 메소드 호출
                brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticCL), "ProcessStaticCL");
                QRPSTA.BL.STAPRC.ProcessStaticCL clsStaticCL = new QRPSTA.BL.STAPRC.ProcessStaticCL();
                brwChannel.mfCredentials(clsStaticCL);

                DataTable dtCL = clsStaticCL.mfReadINSProcessStaticCL_XMR(this.uTextPlantCode.Text, this.uTextProductCode.Text, this.uTextProcessCode.Text
                                                                , this.uTextInspectItemCode.Text
                                                                , this.uTextWriteDate.Text);

                if (dtCL.Rows.Count > 0)
                {
                    QRPSTA.STASummary clsXSum = clsXchart.mfDrawXChart(this.uChartX, dtValue, dblLSL, dblUSL, this.uTextSpecRange.Text
                                                                    , true, Convert.ToDouble(dtCL.Rows[0]["XLCL"]), Convert.ToDouble(dtCL.Rows[0]["XUCL"]), true, true, "", ""
                                                                    , "", m_resSys.GetString("SYS_FONTNAME"), 20, 10, 0);
                    uTextLCL.Text = dtCL.Rows[0]["XLCL"].ToString();
                    uTextUCL.Text = dtCL.Rows[0]["XUCL"].ToString();
                }
                else
                {
                    QRPSTA.STASummary clsXSum = clsXchart.mfDrawXChart(this.uChartX, dtValue, dblLSL, dblUSL, this.uTextSpecRange.Text, false, 0, 0, true, true, "", ""
                                                                    , "", m_resSys.GetString("SYS_FONTNAME"), 20, 10, 0);
                    uTextLCL.Text = clsXSum.LCLDA.ToString();
                    uTextUCL.Text = clsXSum.UCLDA.ToString();
                }
                */
                #endregion

                #region 기존소스 주석처리 - 2

                /*
                // 이전 3개월 데이터 조회(평균값)
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DateTime dateInspectDate = Convert.ToDateTime(this.uTextWriteDate.Text);                                                     // 검사일
                DateTime dateFromDate = new DateTime(dateInspectDate.AddMonths(-3).Year, dateInspectDate.AddMonths(-3).Month, 1).AddDays(-1);   // 검사일의 전달로부터 3개월전 1일날짜
                DateTime dateToDate = new DateTime(dateInspectDate.Year, dateInspectDate.Month, 1).AddDays(-1);                                 // 검사일의 전달 마지막 날짜


                // 2012.03.07 분기별 날짜 구하는부분 추가
                DateTime datePrevFromDate = new DateTime();
                DateTime datePrevToDate = new DateTime();
                if (dateInspectDate.Month.Equals(1) || dateInspectDate.Month.Equals(2) || dateInspectDate.Month.Equals(3))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.AddYears(-1).Year, 9, 30);
                    datePrevToDate = new DateTime(dateInspectDate.AddYears(-1).Year, 12, 31);
                }
                else if (dateInspectDate.Month.Equals(4) || dateInspectDate.Month.Equals(5) || dateInspectDate.Month.Equals(6))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.AddYears(-1).Year, 12, 31);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 3, 31);
                }
                else if (dateInspectDate.Month.Equals(7) || dateInspectDate.Month.Equals(8) || dateInspectDate.Month.Equals(9))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.Year, 3, 31);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 6, 30);
                }
                else if (dateInspectDate.Month.Equals(10) || dateInspectDate.Month.Equals(11) || dateInspectDate.Month.Equals(12))
                {
                    datePrevFromDate = new DateTime(dateInspectDate.Year, 6, 30);
                    datePrevToDate = new DateTime(dateInspectDate.Year, 9, 30);
                }


                DataTable dtData = clsItem.mfReadINSProcInspectReqItem_Mean_Range_FromToDate(this.uTextPlantCode.Text, this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                    , this.uTextStackSeq.Text, this.uTextGeneration.Text
                                                                                    , this.uTextProcessCode.Text, this.uTextEquipCode.Text
                                                                                    , dateFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                    , dateToDate.ToString("yyyy-MM-dd 21:59:59"));

                DataTable dtPrevData = clsItem.mfReadINSProcInspectReqItem_Mean_Range_FromToDate(this.uTextPlantCode.Text, this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                    , this.uTextStackSeq.Text, this.uTextGeneration.Text
                                                                                    , this.uTextProcessCode.Text, this.uTextEquipCode.Text
                                                                                    , datePrevFromDate.ToString("yyyy-MM-dd 22:00:00")
                                                                                    , datePrevToDate.ToString("yyyy-MM-dd 21:59:59"));

                QRPSTA.STASPC clsXchart = new QRPSTA.STASPC();

                if (dtPrevData.Rows.Count > 0)
                {
                    // 현재 줄의 검사항목만 Select
                    string strInspectItemCode = this.uTextInspectItemCode.Text;

                    DataRow[] drRow = dtPrevData.Select("InspectItemCode = '" + strInspectItemCode + "'", "InspectDate ASC, InspectTime ASC");

                    DataTable dtMean = dtPrevData.Clone();

                    foreach (DataRow dr in drRow)
                    {
                        dtMean.ImportRow(dr);
                    }

                    // 현재 등록값 추가
                    DataRow drCur = dtMean.NewRow();
                    drCur["ReqNo"] = this.uTextReqNo.Text;
                    drCur["ReqSeq"] = this.uTextReqSeq.Text;
                    drCur["ReqLotSeq"] = this.uTextReqLotSeq.Text;
                    drCur["ReqItemSeq"] = this.uTextReqItemSeq.Text;
                    drCur["LotNo"] = this.uTextLotNo.Text;
                    drCur["InspectDate"] = this.uTextWriteDate.Text;
                    drCur["InspectTime"] = this.uTextWriteTime.Text;
                    drCur["InspectDateTime"] = this.uTextWriteDate.Text + " " + this.uTextWriteTime.Text;
                    drCur["InspectItemCode"] = this.uTextInspectItemCode.Text;
                    drCur["Mean"] = this.uTextAvg.Text;
                    drCur["DataRange"] = this.uTextRange.Text;
                    dtMean.Rows.Add(drCur);
                    

                    // 평균, 범위 값별로 데이터 테이블로 만든다
                    DataTable dtMeanValue = dtMean.DefaultView.ToTable(false, "Mean");
                    DataTable dtRangeValue = dtMean.DefaultView.ToTable(false, "DataRange");

                    DataTable dtDetailValue = dtMean.DefaultView.ToTable(false, "ReqNo", "ReqSeq", "ReqLotSeq", "ReqItemSeq", "LotNo", "InspectDateTime", "Mean");

                    // 변수 설정
                    double dblLSL = Convert.ToDouble(this.uTextLSL.Text);
                    double dblUSL = Convert.ToDouble(this.uTextUSL.Text);
                    double dblUCL = Convert.ToDouble(this.uTextUCL.Text);
                    double dblLCL = Convert.ToDouble(this.uTextLCL.Text);
                    double dblCL = Convert.ToDouble(this.uTextCL.Text);
                    string strSpecRange = this.uTextSpecRange.Text;

                    QRPSTA.STASummary clsXSum = clsXchart.mfDrawXBarChartWithRChart4SubGroupMean(this.uChartX, dtMeanValue, dtRangeValue
                                                                                                , dblLSL, dblUSL, strSpecRange
                                                                                                , true, dblLCL, dblUCL
                                                                                                , true, true, "", ""
                                                                                                , "", m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                    this.uGridValueData.DataSource = dtDetailValue;
                    this.uGridValueData.DataBind();
                }
                else if (dtData.Rows.Count > 0)
                {
                    // 현재 줄의 검사항목만 Select
                    string strInspectItemCode = this.uTextInspectItemCode.Text;

                    DataRow[] drRow = dtData.Select("InspectItemCode = '" + strInspectItemCode + "'", "InspectDate ASC, InspectTime ASC");

                    DataTable dtMean = dtData.Clone();

                    foreach (DataRow dr in drRow)
                    {
                        dtMean.ImportRow(dr);
                    }

                    // 현재 등록값 추가
                    DataRow drCur = dtMean.NewRow();
                    drCur["ReqNo"] = this.uTextReqNo.Text;
                    drCur["ReqSeq"] = this.uTextReqSeq.Text;
                    drCur["ReqLotSeq"] = this.uTextReqLotSeq.Text;
                    drCur["ReqItemSeq"] = this.uTextReqItemSeq.Text;
                    drCur["LotNo"] = this.uTextLotNo.Text;
                    drCur["InspectDate"] = this.uTextWriteDate.Text;
                    drCur["InspectTime"] = this.uTextWriteTime.Text;
                    drCur["InspectDateTime"] = this.uTextWriteDate.Text + " " + this.uTextWriteTime.Text;
                    drCur["InspectItemCode"] = this.uTextInspectItemCode.Text;
                    drCur["Mean"] = this.uTextAvg.Text;
                    drCur["DataRange"] = this.uTextRange.Text;
                    dtMean.Rows.Add(drCur);

                    // 평균, 범위 값별로 데이터 테이블로 만든다
                    DataTable dtMeanValue = dtMean.DefaultView.ToTable(false, "Mean");
                    DataTable dtRangeValue = dtMean.DefaultView.ToTable(false, "DataRange");

                    DataTable dtDetailValue = dtMean.DefaultView.ToTable(false, "ReqNo", "ReqSeq", "ReqLotSeq", "ReqItemSeq", "LotNo", "InspectDateTime", "Mean");

                    // 변수 설정
                    double dblLSL = Convert.ToDouble(this.uTextLSL.Text);
                    double dblUSL = Convert.ToDouble(this.uTextUSL.Text);
                    double dblUCL = Convert.ToDouble(this.uTextUCL.Text);
                    double dblLCL = Convert.ToDouble(this.uTextLCL.Text);
                    double dblCL = Convert.ToDouble(this.uTextCL.Text);
                    string strSpecRange = this.uTextSpecRange.Text;

                    QRPSTA.STASummary clsXSum = clsXchart.mfDrawXBarChartWithRChart4SubGroupMean(this.uChartX, dtMeanValue, dtRangeValue
                                                                                                , dblLSL, dblUSL, strSpecRange
                                                                                                , true, dblLCL, dblUCL
                                                                                                , true, true, "", ""
                                                                                                , "", m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                    this.uGridValueData.DataSource = dtDetailValue;
                    this.uGridValueData.DataBind();
                }
                else
                {
                    clsXchart.mfInitControlChart(this.uChartX);

                    while (this.uGridValueData.Rows.Count > 0)
                        this.uGridValueData.Rows[0].Delete(false);
                }
                 * */
                #endregion

                // 2012.03.09 추가
                // 측정 Data 및 그래프에 표시될 측정데이터는 최근 30개를 보여준다
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtMean = clsItem.mfReadINSProcInspectReqItem_Mean_Range_Top30(this.uTextPlantCode.Text, this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                    , this.uTextStackSeq.Text, this.uTextGeneration.Text
                                                                                    , this.uTextProcessCode.Text, this.uTextEquipCode.Text
                                                                                    , this.uTextWriteDate.Text, this.uTextWriteTime.Text
                                                                                    , this.uTextInspectItemCode.Text);

                // 내림차순으로 정렬
                DataView dvMean = dtMean.DefaultView;
                dvMean.Sort = "InspectDate ASC, InspectTime ASC";
                dtMean = dvMean.ToTable();
                
                QRPSTA.STASPC clsXchart = new QRPSTA.STASPC();

                if (dtMean.Rows.Count > 0)
                {
                    ////// 현재 줄의 검사항목만 Select
                    ////string strInspectItemCode = this.uTextInspectItemCode.Text;

                    ////DataRow[] drRow = dtData.Select("InspectItemCode = '" + strInspectItemCode + "'", "InspectDate ASC, InspectTime ASC");

                    ////DataTable dtMean = dtData.Clone();

                    ////foreach (DataRow dr in drRow)
                    ////{
                    ////    dtMean.ImportRow(dr);
                    ////}

                    //// 현재 등록값 추가
                    //DataRow drCur = dtMean.NewRow();
                    //drCur["ReqNo"] = this.uTextReqNo.Text;
                    //drCur["ReqSeq"] = this.uTextReqSeq.Text;
                    //drCur["ReqLotSeq"] = this.uTextReqLotSeq.Text;
                    //drCur["ReqItemSeq"] = this.uTextReqItemSeq.Text;
                    //drCur["LotNo"] = this.uTextLotNo.Text;
                    //drCur["InspectDate"] = this.uTextWriteDate.Text;
                    //drCur["InspectTime"] = this.uTextWriteTime.Text;
                    //drCur["InspectDateTime"] = this.uTextWriteDate.Text + " " + this.uTextWriteTime.Text;
                    //drCur["InspectItemCode"] = this.uTextInspectItemCode.Text;
                    //drCur["Mean"] = this.uTextAvg.Text;
                    //drCur["DataRange"] = this.uTextRange.Text;
                    //dtMean.Rows.Add(drCur);

                    // 평균, 범위 값별로 데이터 테이블로 만든다
                    DataTable dtMeanValue = dtMean.DefaultView.ToTable(false, "Mean");
                    DataTable dtRangeValue = dtMean.DefaultView.ToTable(false, "DataRange");

                    DataTable dtDetailValue = dtMean.DefaultView.ToTable(false, "ReqNo", "ReqSeq", "ReqLotSeq", "ReqItemSeq", "LotNo", "InspectDateTime", "Mean");

                    // 변수 설정
                    double dblLSL = Convert.ToDouble(this.uTextLSL.Text);
                    double dblUSL = Convert.ToDouble(this.uTextUSL.Text);
                    double dblUCL = Convert.ToDouble(this.uTextUCL.Text);
                    double dblLCL = Convert.ToDouble(this.uTextLCL.Text);
                    double dblCL = Convert.ToDouble(this.uTextCL.Text);
                    string strSpecRange = this.uTextSpecRange.Text;

                    QRPSTA.STASummary clsXSum = clsXchart.mfDrawXBarChartWithRChart4SubGroupMean(this.uChartX, dtMeanValue, dtRangeValue
                                                                                                , dblLSL, dblUSL, strSpecRange
                                                                                                , true, dblLCL, dblCL, dblUCL, 5
                                                                                                , true, true, "", ""
                                                                                                , "", m_resSys.GetString("SYS_FONTNAME"), 50, 10, 0);

                    this.uGridValueData.DataSource = dtDetailValue;
                    this.uGridValueData.DataBind();
                }
                else
                {
                    clsXchart.mfInitControlChart(this.uChartX);

                    while (this.uGridValueData.Rows.Count > 0)
                        this.uGridValueData.Rows[0].Delete(false);
                }

                #endregion

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }            

        #endregion                

        #region 4.일반 Event, Method
        private void uCheckComplete_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //// CheckEditor 가 활성화 상태일때
                //if (this.uCheckComplete.Enabled == true && this.uCheckComplete.Checked == true)
                //{
                //    WinMessageBox msg = new WinMessageBox();
                //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "작성완료 확인", "작성완료 체크후 저장시 이후 수정할 수 없습니다",
                //                    Infragistics.Win.HAlign.Right);
                //}
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// MES I/F 메소드
        /// </summary>
        private void MESInterFace()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                TransErrRtn ErrRtn = new TransErrRtn();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.SPCN), "SPCN");
                QRPINS.BL.INSPRC.SPCN clsSPCN = new QRPINS.BL.INSPRC.SPCN();
                brwChannel.mfCredentials(clsSPCN);

                string strErrRtn = clsSPCN.mfSaveSPCNMESIF(this.Name, m_resSys.GetString("SYS_USERID"), this.uTextEquipCode.Text, m_resSys.GetString("SYS_USERIP"),
                                                            this.uTextPlantCode.Text, this.uTextReqNo.Text, this.uTextReqSeq.Text,
                                                            Convert.ToInt32(this.uTextReqLotSeq.Text), Convert.ToInt32(this.uTextReqItemSeq.Text));

                if (!ErrRtn.ErrNum.Equals(0))
                {
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001008",
                                                        "M000940",
                                                        Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001008", strLang)
                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001008"
                                                        , "M000939",
                                                        Infragistics.Win.HAlign.Right);

                    // 리스트 갱신
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINSZ0012_Resize(object sender, EventArgs e)
        {
            ////try
            ////{
            ////    if (this.Width > 1070)
            ////    {
            ////        uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
            ////    }
            ////    else
            ////    {
            ////        uGroupBoxContentsArea.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            ////    }

            ////}
            ////catch (System.Exception ex)
            ////{
            ////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////    frmErr.ShowDialog();
            ////}
            ////finally
            ////{
            ////}
        }

        // 평균값 더블클릭시 Raw Data 상세조회
        private void uGridValueData_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                frmINSZ0012P frmINS = new frmINSZ0012P();
                frmINS.PlantCode = this.uTextPlantCode.Text;
                frmINS.ReqNo = e.Row.Cells["ReqNo"].Value.ToString();
                frmINS.ReqSeq = e.Row.Cells["ReqSeq"].Value.ToString();
                frmINS.ReqLotSeq = Convert.ToInt32(e.Row.Cells["ReqLotSeq"].Value);
                frmINS.ReqItemSeq = Convert.ToInt32(e.Row.Cells["ReqItemSeq"].Value);
                
                frmINS.Text = "측정값 조회";
                frmINS.Location = new Point(Control.MousePosition.X, Control.MousePosition.Y - frmINS.Height);

                frmINS.ShowDialog();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


    }
}
