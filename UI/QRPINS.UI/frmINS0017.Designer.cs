﻿namespace QRPINS.UI
{
    partial class frmINS0017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINS0017));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton9 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton10 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uTextShortFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextShortConfirmDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelShortConfirmUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShortConfirmUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextShortConfirmUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateShortConfirmDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelShortConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uOptShortConfirm = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelShortConfirm = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelShortEmail = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShortEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelShortUserName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShortUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelShortActionDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateShortAcceptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAttachDate1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShortActionDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uTextLongFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLongConfirmDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLongConfirmUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLongConfirmUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLongConfirmUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateLongConfirmDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelLongConfirmDate = new Infragistics.Win.Misc.UltraLabel();
            this.uOptLongConfirm = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelLongConfirm = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLongEmail = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLongEmail = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLongActionDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLongUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLongUserName = new Infragistics.Win.Misc.UltraLabel();
            this.uDateLongAcceptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAttachDate2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLongActionDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonMoveDisplay = new Infragistics.Win.Misc.UltraButton();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete1 = new Infragistics.Win.Misc.UltraButton();
            this.uGridMatInspectFault = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridMatInspect = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboAriseProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonDelete2 = new Infragistics.Win.Misc.UltraButton();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridProcInspectFault = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridProcInspect = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckSearchFinalCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelSearchFinalCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMaterialGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMaterialGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMgnNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchAbnomalType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchFaultSeparation = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchRegistDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTabAction = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage3 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTextMaterialTreate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialTreate = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckFinalCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelFinalCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectDesc = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextWMSTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uComboAbnormalType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboStatus = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelStatus = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAbnormalType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTotalQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWriteUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLalbeTotalQty = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelGRDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGRDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialSpec1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialSpec1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMgnNum = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortConfirmDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortConfirmUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortConfirmUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShortConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptShortConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShortAcceptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortActionDesc)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongConfirmDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongConfirmUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongConfirmUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateLongConfirmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptLongConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateLongAcceptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongActionDesc)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatInspectFault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatInspect)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcInspectFault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcInspect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchFinalCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchAbnomalType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabAction)).BeginInit();
            this.uTabAction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialTreate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinalCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWMSTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAbnormalType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotalQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialSpec1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uTextShortFile);
            this.ultraTabPageControl3.Controls.Add(this.uTextShortConfirmDesc);
            this.ultraTabPageControl3.Controls.Add(this.uLabelShortConfirmUser);
            this.ultraTabPageControl3.Controls.Add(this.uTextShortConfirmUserID);
            this.ultraTabPageControl3.Controls.Add(this.uTextShortConfirmUserName);
            this.ultraTabPageControl3.Controls.Add(this.uDateShortConfirmDate);
            this.ultraTabPageControl3.Controls.Add(this.uLabelShortConfirmDate);
            this.ultraTabPageControl3.Controls.Add(this.uOptShortConfirm);
            this.ultraTabPageControl3.Controls.Add(this.uLabelShortConfirm);
            this.ultraTabPageControl3.Controls.Add(this.uLabelShortEmail);
            this.ultraTabPageControl3.Controls.Add(this.uTextShortEmail);
            this.ultraTabPageControl3.Controls.Add(this.uLabelShortUserName);
            this.ultraTabPageControl3.Controls.Add(this.uTextShortUserName);
            this.ultraTabPageControl3.Controls.Add(this.uLabelShortActionDesc);
            this.ultraTabPageControl3.Controls.Add(this.uDateShortAcceptDate);
            this.ultraTabPageControl3.Controls.Add(this.uLabelAttachDate1);
            this.ultraTabPageControl3.Controls.Add(this.uTextShortActionDesc);
            this.ultraTabPageControl3.Controls.Add(this.uLabelAttachFile1);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(901, 78);
            // 
            // uTextShortFile
            // 
            appearance86.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance86.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance86;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Key = "Up";
            appearance106.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance106.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance106;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Key = "Down";
            this.uTextShortFile.ButtonsRight.Add(editorButton1);
            this.uTextShortFile.ButtonsRight.Add(editorButton2);
            this.uTextShortFile.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextShortFile.Location = new System.Drawing.Point(95, 4);
            this.uTextShortFile.Name = "uTextShortFile";
            this.uTextShortFile.Size = new System.Drawing.Size(330, 21);
            this.uTextShortFile.TabIndex = 127;
            this.uTextShortFile.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextShortFile_EditorButtonClick);
            // 
            // uTextShortConfirmDesc
            // 
            appearance103.BackColor = System.Drawing.Color.White;
            this.uTextShortConfirmDesc.Appearance = appearance103;
            this.uTextShortConfirmDesc.BackColor = System.Drawing.Color.White;
            this.uTextShortConfirmDesc.Location = new System.Drawing.Point(185, 52);
            this.uTextShortConfirmDesc.Name = "uTextShortConfirmDesc";
            this.uTextShortConfirmDesc.Size = new System.Drawing.Size(240, 21);
            this.uTextShortConfirmDesc.TabIndex = 126;
            // 
            // uLabelShortConfirmUser
            // 
            this.uLabelShortConfirmUser.Location = new System.Drawing.Point(442, 52);
            this.uLabelShortConfirmUser.Name = "uLabelShortConfirmUser";
            this.uLabelShortConfirmUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelShortConfirmUser.TabIndex = 125;
            this.uLabelShortConfirmUser.Text = "승인자";
            // 
            // uTextShortConfirmUserID
            // 
            appearance42.BackColor = System.Drawing.Color.White;
            this.uTextShortConfirmUserID.Appearance = appearance42;
            this.uTextShortConfirmUserID.BackColor = System.Drawing.Color.White;
            appearance67.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance67.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance67;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextShortConfirmUserID.ButtonsRight.Add(editorButton3);
            this.uTextShortConfirmUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextShortConfirmUserID.Location = new System.Drawing.Point(531, 52);
            this.uTextShortConfirmUserID.Name = "uTextShortConfirmUserID";
            this.uTextShortConfirmUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextShortConfirmUserID.TabIndex = 124;
            this.uTextShortConfirmUserID.ValueChanged += new System.EventHandler(this.uTextShortConfirmUserID_ValueChanged);
            this.uTextShortConfirmUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextShortConfirmUserID_KeyDown);
            this.uTextShortConfirmUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextShortConfirmUserID_EditorButtonClick);
            // 
            // uTextShortConfirmUserName
            // 
            appearance74.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortConfirmUserName.Appearance = appearance74;
            this.uTextShortConfirmUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortConfirmUserName.Location = new System.Drawing.Point(621, 52);
            this.uTextShortConfirmUserName.Name = "uTextShortConfirmUserName";
            this.uTextShortConfirmUserName.ReadOnly = true;
            this.uTextShortConfirmUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextShortConfirmUserName.TabIndex = 123;
            // 
            // uDateShortConfirmDate
            // 
            appearance99.BackColor = System.Drawing.Color.White;
            this.uDateShortConfirmDate.Appearance = appearance99;
            this.uDateShortConfirmDate.BackColor = System.Drawing.Color.White;
            this.uDateShortConfirmDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateShortConfirmDate.Location = new System.Drawing.Point(809, 52);
            this.uDateShortConfirmDate.Name = "uDateShortConfirmDate";
            this.uDateShortConfirmDate.Size = new System.Drawing.Size(86, 21);
            this.uDateShortConfirmDate.TabIndex = 122;
            // 
            // uLabelShortConfirmDate
            // 
            this.uLabelShortConfirmDate.Location = new System.Drawing.Point(720, 52);
            this.uLabelShortConfirmDate.Name = "uLabelShortConfirmDate";
            this.uLabelShortConfirmDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelShortConfirmDate.TabIndex = 121;
            this.uLabelShortConfirmDate.Text = "승인일";
            // 
            // uOptShortConfirm
            // 
            appearance104.BackColor = System.Drawing.Color.White;
            this.uOptShortConfirm.Appearance = appearance104;
            this.uOptShortConfirm.BackColor = System.Drawing.Color.White;
            this.uOptShortConfirm.BackColorInternal = System.Drawing.Color.White;
            this.uOptShortConfirm.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem3.DataValue = "T";
            valueListItem3.DisplayText = "Pass";
            valueListItem4.DataValue = "F";
            valueListItem4.DisplayText = "Fail";
            this.uOptShortConfirm.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.uOptShortConfirm.Location = new System.Drawing.Point(96, 52);
            this.uOptShortConfirm.Name = "uOptShortConfirm";
            this.uOptShortConfirm.Size = new System.Drawing.Size(86, 20);
            this.uOptShortConfirm.TabIndex = 120;
            // 
            // uLabelShortConfirm
            // 
            this.uLabelShortConfirm.Location = new System.Drawing.Point(7, 52);
            this.uLabelShortConfirm.Name = "uLabelShortConfirm";
            this.uLabelShortConfirm.Size = new System.Drawing.Size(86, 20);
            this.uLabelShortConfirm.TabIndex = 119;
            this.uLabelShortConfirm.Text = "승인";
            // 
            // uLabelShortEmail
            // 
            this.uLabelShortEmail.Location = new System.Drawing.Point(442, 28);
            this.uLabelShortEmail.Name = "uLabelShortEmail";
            this.uLabelShortEmail.Size = new System.Drawing.Size(86, 20);
            this.uLabelShortEmail.TabIndex = 117;
            this.uLabelShortEmail.Text = "E-Mail";
            // 
            // uTextShortEmail
            // 
            appearance101.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortEmail.Appearance = appearance101;
            this.uTextShortEmail.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortEmail.Location = new System.Drawing.Point(531, 28);
            this.uTextShortEmail.Name = "uTextShortEmail";
            this.uTextShortEmail.ReadOnly = true;
            this.uTextShortEmail.Size = new System.Drawing.Size(175, 21);
            this.uTextShortEmail.TabIndex = 116;
            // 
            // uLabelShortUserName
            // 
            this.uLabelShortUserName.Location = new System.Drawing.Point(442, 4);
            this.uLabelShortUserName.Name = "uLabelShortUserName";
            this.uLabelShortUserName.Size = new System.Drawing.Size(86, 20);
            this.uLabelShortUserName.TabIndex = 115;
            this.uLabelShortUserName.Text = "접수자";
            // 
            // uTextShortUserName
            // 
            appearance102.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortUserName.Appearance = appearance102;
            this.uTextShortUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortUserName.Location = new System.Drawing.Point(531, 4);
            this.uTextShortUserName.Name = "uTextShortUserName";
            this.uTextShortUserName.ReadOnly = true;
            this.uTextShortUserName.Size = new System.Drawing.Size(175, 21);
            this.uTextShortUserName.TabIndex = 113;
            // 
            // uLabelShortActionDesc
            // 
            this.uLabelShortActionDesc.Location = new System.Drawing.Point(7, 28);
            this.uLabelShortActionDesc.Name = "uLabelShortActionDesc";
            this.uLabelShortActionDesc.Size = new System.Drawing.Size(86, 20);
            this.uLabelShortActionDesc.TabIndex = 112;
            this.uLabelShortActionDesc.Text = "Comment";
            // 
            // uDateShortAcceptDate
            // 
            appearance73.BackColor = System.Drawing.Color.White;
            this.uDateShortAcceptDate.Appearance = appearance73;
            this.uDateShortAcceptDate.BackColor = System.Drawing.Color.White;
            this.uDateShortAcceptDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateShortAcceptDate.Enabled = false;
            this.uDateShortAcceptDate.Location = new System.Drawing.Point(809, 4);
            this.uDateShortAcceptDate.Name = "uDateShortAcceptDate";
            this.uDateShortAcceptDate.ReadOnly = true;
            this.uDateShortAcceptDate.Size = new System.Drawing.Size(86, 21);
            this.uDateShortAcceptDate.TabIndex = 111;
            // 
            // uLabelAttachDate1
            // 
            this.uLabelAttachDate1.Location = new System.Drawing.Point(720, 4);
            this.uLabelAttachDate1.Name = "uLabelAttachDate1";
            this.uLabelAttachDate1.Size = new System.Drawing.Size(86, 20);
            this.uLabelAttachDate1.TabIndex = 110;
            this.uLabelAttachDate1.Text = "접수일";
            // 
            // uTextShortActionDesc
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortActionDesc.Appearance = appearance34;
            this.uTextShortActionDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShortActionDesc.Location = new System.Drawing.Point(96, 28);
            this.uTextShortActionDesc.Name = "uTextShortActionDesc";
            this.uTextShortActionDesc.ReadOnly = true;
            this.uTextShortActionDesc.Size = new System.Drawing.Size(329, 21);
            this.uTextShortActionDesc.TabIndex = 109;
            // 
            // uLabelAttachFile1
            // 
            this.uLabelAttachFile1.Location = new System.Drawing.Point(7, 4);
            this.uLabelAttachFile1.Name = "uLabelAttachFile1";
            this.uLabelAttachFile1.Size = new System.Drawing.Size(86, 20);
            this.uLabelAttachFile1.TabIndex = 107;
            this.uLabelAttachFile1.Text = "첨부파일";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uTextLongFile);
            this.ultraTabPageControl4.Controls.Add(this.uTextLongConfirmDesc);
            this.ultraTabPageControl4.Controls.Add(this.uLabelLongConfirmUser);
            this.ultraTabPageControl4.Controls.Add(this.uTextLongConfirmUserID);
            this.ultraTabPageControl4.Controls.Add(this.uTextLongConfirmUserName);
            this.ultraTabPageControl4.Controls.Add(this.uDateLongConfirmDate);
            this.ultraTabPageControl4.Controls.Add(this.uLabelLongConfirmDate);
            this.ultraTabPageControl4.Controls.Add(this.uOptLongConfirm);
            this.ultraTabPageControl4.Controls.Add(this.uLabelLongConfirm);
            this.ultraTabPageControl4.Controls.Add(this.uLabelLongEmail);
            this.ultraTabPageControl4.Controls.Add(this.uTextLongEmail);
            this.ultraTabPageControl4.Controls.Add(this.uLabelLongActionDesc);
            this.ultraTabPageControl4.Controls.Add(this.uTextLongUserName);
            this.ultraTabPageControl4.Controls.Add(this.uLabelLongUserName);
            this.ultraTabPageControl4.Controls.Add(this.uDateLongAcceptDate);
            this.ultraTabPageControl4.Controls.Add(this.uLabelAttachDate2);
            this.ultraTabPageControl4.Controls.Add(this.uTextLongActionDesc);
            this.ultraTabPageControl4.Controls.Add(this.uLabelAttachFile2);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(901, 78);
            // 
            // uTextLongFile
            // 
            appearance128.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance128.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance128;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Key = "Up";
            appearance129.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance129.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance129;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "Down";
            this.uTextLongFile.ButtonsRight.Add(editorButton4);
            this.uTextLongFile.ButtonsRight.Add(editorButton5);
            this.uTextLongFile.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLongFile.Location = new System.Drawing.Point(96, 4);
            this.uTextLongFile.Name = "uTextLongFile";
            this.uTextLongFile.Size = new System.Drawing.Size(329, 21);
            this.uTextLongFile.TabIndex = 135;
            this.uTextLongFile.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextLongFile_EditorButtonClick);
            // 
            // uTextLongConfirmDesc
            // 
            appearance100.BackColor = System.Drawing.Color.White;
            this.uTextLongConfirmDesc.Appearance = appearance100;
            this.uTextLongConfirmDesc.BackColor = System.Drawing.Color.White;
            this.uTextLongConfirmDesc.Location = new System.Drawing.Point(185, 52);
            this.uTextLongConfirmDesc.Name = "uTextLongConfirmDesc";
            this.uTextLongConfirmDesc.Size = new System.Drawing.Size(240, 21);
            this.uTextLongConfirmDesc.TabIndex = 134;
            // 
            // uLabelLongConfirmUser
            // 
            this.uLabelLongConfirmUser.Location = new System.Drawing.Point(442, 52);
            this.uLabelLongConfirmUser.Name = "uLabelLongConfirmUser";
            this.uLabelLongConfirmUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelLongConfirmUser.TabIndex = 133;
            this.uLabelLongConfirmUser.Text = "승인자";
            // 
            // uTextLongConfirmUserID
            // 
            appearance31.BackColor = System.Drawing.Color.White;
            this.uTextLongConfirmUserID.Appearance = appearance31;
            this.uTextLongConfirmUserID.BackColor = System.Drawing.Color.White;
            appearance32.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance32.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance32;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextLongConfirmUserID.ButtonsRight.Add(editorButton6);
            this.uTextLongConfirmUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLongConfirmUserID.Location = new System.Drawing.Point(531, 52);
            this.uTextLongConfirmUserID.Name = "uTextLongConfirmUserID";
            this.uTextLongConfirmUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextLongConfirmUserID.TabIndex = 132;
            this.uTextLongConfirmUserID.ValueChanged += new System.EventHandler(this.uTextLongConfirmUserID_ValueChanged);
            this.uTextLongConfirmUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLongConfirmUserID_KeyDown);
            this.uTextLongConfirmUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextLongConfirmUserID_EditorButtonClick);
            // 
            // uTextLongConfirmUserName
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongConfirmUserName.Appearance = appearance33;
            this.uTextLongConfirmUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongConfirmUserName.Location = new System.Drawing.Point(621, 52);
            this.uTextLongConfirmUserName.Name = "uTextLongConfirmUserName";
            this.uTextLongConfirmUserName.ReadOnly = true;
            this.uTextLongConfirmUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextLongConfirmUserName.TabIndex = 131;
            // 
            // uDateLongConfirmDate
            // 
            appearance37.BackColor = System.Drawing.Color.White;
            this.uDateLongConfirmDate.Appearance = appearance37;
            this.uDateLongConfirmDate.BackColor = System.Drawing.Color.White;
            this.uDateLongConfirmDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateLongConfirmDate.Location = new System.Drawing.Point(809, 52);
            this.uDateLongConfirmDate.Name = "uDateLongConfirmDate";
            this.uDateLongConfirmDate.Size = new System.Drawing.Size(86, 21);
            this.uDateLongConfirmDate.TabIndex = 130;
            // 
            // uLabelLongConfirmDate
            // 
            this.uLabelLongConfirmDate.Location = new System.Drawing.Point(720, 52);
            this.uLabelLongConfirmDate.Name = "uLabelLongConfirmDate";
            this.uLabelLongConfirmDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelLongConfirmDate.TabIndex = 129;
            this.uLabelLongConfirmDate.Text = "승인일";
            // 
            // uOptLongConfirm
            // 
            appearance105.BackColor = System.Drawing.Color.White;
            this.uOptLongConfirm.Appearance = appearance105;
            this.uOptLongConfirm.BackColor = System.Drawing.Color.White;
            this.uOptLongConfirm.BackColorInternal = System.Drawing.Color.White;
            this.uOptLongConfirm.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "T";
            valueListItem1.DisplayText = "Pass";
            valueListItem2.DataValue = "F";
            valueListItem2.DisplayText = "Fail";
            this.uOptLongConfirm.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptLongConfirm.Location = new System.Drawing.Point(96, 52);
            this.uOptLongConfirm.Name = "uOptLongConfirm";
            this.uOptLongConfirm.Size = new System.Drawing.Size(86, 20);
            this.uOptLongConfirm.TabIndex = 128;
            // 
            // uLabelLongConfirm
            // 
            this.uLabelLongConfirm.Location = new System.Drawing.Point(7, 52);
            this.uLabelLongConfirm.Name = "uLabelLongConfirm";
            this.uLabelLongConfirm.Size = new System.Drawing.Size(86, 20);
            this.uLabelLongConfirm.TabIndex = 127;
            this.uLabelLongConfirm.Text = "승인";
            // 
            // uLabelLongEmail
            // 
            this.uLabelLongEmail.Location = new System.Drawing.Point(442, 28);
            this.uLabelLongEmail.Name = "uLabelLongEmail";
            this.uLabelLongEmail.Size = new System.Drawing.Size(86, 20);
            this.uLabelLongEmail.TabIndex = 124;
            this.uLabelLongEmail.Text = "E-Mail";
            // 
            // uTextLongEmail
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongEmail.Appearance = appearance35;
            this.uTextLongEmail.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongEmail.Location = new System.Drawing.Point(531, 28);
            this.uTextLongEmail.Name = "uTextLongEmail";
            this.uTextLongEmail.ReadOnly = true;
            this.uTextLongEmail.Size = new System.Drawing.Size(175, 21);
            this.uTextLongEmail.TabIndex = 123;
            // 
            // uLabelLongActionDesc
            // 
            this.uLabelLongActionDesc.Location = new System.Drawing.Point(7, 28);
            this.uLabelLongActionDesc.Name = "uLabelLongActionDesc";
            this.uLabelLongActionDesc.Size = new System.Drawing.Size(86, 20);
            this.uLabelLongActionDesc.TabIndex = 122;
            this.uLabelLongActionDesc.Text = "Comment";
            // 
            // uTextLongUserName
            // 
            appearance75.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongUserName.Appearance = appearance75;
            this.uTextLongUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongUserName.Location = new System.Drawing.Point(531, 4);
            this.uTextLongUserName.Name = "uTextLongUserName";
            this.uTextLongUserName.ReadOnly = true;
            this.uTextLongUserName.Size = new System.Drawing.Size(175, 21);
            this.uTextLongUserName.TabIndex = 121;
            // 
            // uLabelLongUserName
            // 
            this.uLabelLongUserName.Location = new System.Drawing.Point(442, 4);
            this.uLabelLongUserName.Name = "uLabelLongUserName";
            this.uLabelLongUserName.Size = new System.Drawing.Size(86, 20);
            this.uLabelLongUserName.TabIndex = 120;
            this.uLabelLongUserName.Text = "접수자";
            // 
            // uDateLongAcceptDate
            // 
            appearance85.BackColor = System.Drawing.Color.White;
            this.uDateLongAcceptDate.Appearance = appearance85;
            this.uDateLongAcceptDate.BackColor = System.Drawing.Color.White;
            this.uDateLongAcceptDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateLongAcceptDate.Enabled = false;
            this.uDateLongAcceptDate.Location = new System.Drawing.Point(809, 4);
            this.uDateLongAcceptDate.Name = "uDateLongAcceptDate";
            this.uDateLongAcceptDate.Size = new System.Drawing.Size(86, 21);
            this.uDateLongAcceptDate.TabIndex = 113;
            // 
            // uLabelAttachDate2
            // 
            this.uLabelAttachDate2.Location = new System.Drawing.Point(720, 4);
            this.uLabelAttachDate2.Name = "uLabelAttachDate2";
            this.uLabelAttachDate2.Size = new System.Drawing.Size(86, 20);
            this.uLabelAttachDate2.TabIndex = 112;
            this.uLabelAttachDate2.Text = "접수일";
            // 
            // uTextLongActionDesc
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongActionDesc.Appearance = appearance41;
            this.uTextLongActionDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLongActionDesc.Location = new System.Drawing.Point(96, 28);
            this.uTextLongActionDesc.Name = "uTextLongActionDesc";
            this.uTextLongActionDesc.ReadOnly = true;
            this.uTextLongActionDesc.Size = new System.Drawing.Size(329, 21);
            this.uTextLongActionDesc.TabIndex = 111;
            // 
            // uLabelAttachFile2
            // 
            this.uLabelAttachFile2.Location = new System.Drawing.Point(7, 4);
            this.uLabelAttachFile2.Name = "uLabelAttachFile2";
            this.uLabelAttachFile2.Size = new System.Drawing.Size(86, 20);
            this.uLabelAttachFile2.TabIndex = 109;
            this.uLabelAttachFile2.Text = "첨부파일";
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGroupBox1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(901, 338);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uButtonMoveDisplay);
            this.uGroupBox1.Controls.Add(this.uButtonFileDown);
            this.uGroupBox1.Controls.Add(this.uButtonDelete1);
            this.uGroupBox1.Controls.Add(this.uGridMatInspectFault);
            this.uGroupBox1.Controls.Add(this.uGridMatInspect);
            this.uGroupBox1.Location = new System.Drawing.Point(3, 4);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(894, 328);
            this.uGroupBox1.TabIndex = 69;
            // 
            // uButtonMoveDisplay
            // 
            this.uButtonMoveDisplay.Location = new System.Drawing.Point(754, 28);
            this.uButtonMoveDisplay.Name = "uButtonMoveDisplay";
            this.uButtonMoveDisplay.Size = new System.Drawing.Size(124, 28);
            this.uButtonMoveDisplay.TabIndex = 117;
            this.uButtonMoveDisplay.Click += new System.EventHandler(this.uButtonMoveDisplay_Click);
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(7, 28);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(75, 28);
            this.uButtonFileDown.TabIndex = 116;
            this.uButtonFileDown.Text = "ultraButton1";
            this.uButtonFileDown.Visible = false;
            // 
            // uButtonDelete1
            // 
            this.uButtonDelete1.Location = new System.Drawing.Point(350, 28);
            this.uButtonDelete1.Name = "uButtonDelete1";
            this.uButtonDelete1.Size = new System.Drawing.Size(75, 28);
            this.uButtonDelete1.TabIndex = 115;
            this.uButtonDelete1.Text = "ultraButton1";
            this.uButtonDelete1.Click += new System.EventHandler(this.uButtonDelete1_Click);
            // 
            // uGridMatInspectFault
            // 
            this.uGridMatInspectFault.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMatInspectFault.DisplayLayout.Appearance = appearance19;
            this.uGridMatInspectFault.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMatInspectFault.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatInspectFault.DisplayLayout.GroupByBox.Appearance = appearance20;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatInspectFault.DisplayLayout.GroupByBox.BandLabelAppearance = appearance21;
            this.uGridMatInspectFault.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance22.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance22.BackColor2 = System.Drawing.SystemColors.Control;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatInspectFault.DisplayLayout.GroupByBox.PromptAppearance = appearance22;
            this.uGridMatInspectFault.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMatInspectFault.DisplayLayout.MaxRowScrollRegions = 1;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMatInspectFault.DisplayLayout.Override.ActiveCellAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.Highlight;
            appearance24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMatInspectFault.DisplayLayout.Override.ActiveRowAppearance = appearance24;
            this.uGridMatInspectFault.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMatInspectFault.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMatInspectFault.DisplayLayout.Override.CardAreaAppearance = appearance25;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            appearance26.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMatInspectFault.DisplayLayout.Override.CellAppearance = appearance26;
            this.uGridMatInspectFault.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMatInspectFault.DisplayLayout.Override.CellPadding = 0;
            appearance27.BackColor = System.Drawing.SystemColors.Control;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatInspectFault.DisplayLayout.Override.GroupByRowAppearance = appearance27;
            appearance28.TextHAlignAsString = "Left";
            this.uGridMatInspectFault.DisplayLayout.Override.HeaderAppearance = appearance28;
            this.uGridMatInspectFault.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMatInspectFault.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            this.uGridMatInspectFault.DisplayLayout.Override.RowAppearance = appearance29;
            this.uGridMatInspectFault.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMatInspectFault.DisplayLayout.Override.TemplateAddRowAppearance = appearance30;
            this.uGridMatInspectFault.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMatInspectFault.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMatInspectFault.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMatInspectFault.Location = new System.Drawing.Point(350, 60);
            this.uGridMatInspectFault.Name = "uGridMatInspectFault";
            this.uGridMatInspectFault.Size = new System.Drawing.Size(534, 256);
            this.uGridMatInspectFault.TabIndex = 66;
            this.uGridMatInspectFault.Text = "ultraGrid1";
            this.uGridMatInspectFault.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridMatInspectFault_KeyDown);
            this.uGridMatInspectFault.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMatInspectFault_ClickCellButton);
            this.uGridMatInspectFault.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridMatInspectFault_DoubleClickCell);
            // 
            // uGridMatInspect
            // 
            this.uGridMatInspect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMatInspect.DisplayLayout.Appearance = appearance87;
            this.uGridMatInspect.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMatInspect.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance88.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance88.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance88.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatInspect.DisplayLayout.GroupByBox.Appearance = appearance88;
            appearance89.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatInspect.DisplayLayout.GroupByBox.BandLabelAppearance = appearance89;
            this.uGridMatInspect.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance90.BackColor2 = System.Drawing.SystemColors.Control;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatInspect.DisplayLayout.GroupByBox.PromptAppearance = appearance90;
            this.uGridMatInspect.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMatInspect.DisplayLayout.MaxRowScrollRegions = 1;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMatInspect.DisplayLayout.Override.ActiveCellAppearance = appearance91;
            appearance92.BackColor = System.Drawing.SystemColors.Highlight;
            appearance92.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMatInspect.DisplayLayout.Override.ActiveRowAppearance = appearance92;
            this.uGridMatInspect.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMatInspect.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMatInspect.DisplayLayout.Override.CardAreaAppearance = appearance93;
            appearance94.BorderColor = System.Drawing.Color.Silver;
            appearance94.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMatInspect.DisplayLayout.Override.CellAppearance = appearance94;
            this.uGridMatInspect.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMatInspect.DisplayLayout.Override.CellPadding = 0;
            appearance95.BackColor = System.Drawing.SystemColors.Control;
            appearance95.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance95.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatInspect.DisplayLayout.Override.GroupByRowAppearance = appearance95;
            appearance96.TextHAlignAsString = "Left";
            this.uGridMatInspect.DisplayLayout.Override.HeaderAppearance = appearance96;
            this.uGridMatInspect.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMatInspect.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.Color.Silver;
            this.uGridMatInspect.DisplayLayout.Override.RowAppearance = appearance97;
            this.uGridMatInspect.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMatInspect.DisplayLayout.Override.TemplateAddRowAppearance = appearance98;
            this.uGridMatInspect.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMatInspect.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMatInspect.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMatInspect.Location = new System.Drawing.Point(7, 60);
            this.uGridMatInspect.Name = "uGridMatInspect";
            this.uGridMatInspect.Size = new System.Drawing.Size(343, 256);
            this.uGridMatInspect.TabIndex = 64;
            this.uGridMatInspect.Text = "ultraGrid1";
            this.uGridMatInspect.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridMatInspect_DoubleClickRow);
            this.uGridMatInspect.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMatInspect_ClickCellButton);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGroupBox2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(901, 338);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uDateAriseDate);
            this.uGroupBox2.Controls.Add(this.uComboAriseProcess);
            this.uGroupBox2.Controls.Add(this.uButtonDelete2);
            this.uGroupBox2.Controls.Add(this.uLabelLotNo);
            this.uGroupBox2.Controls.Add(this.uTextLotNo);
            this.uGroupBox2.Controls.Add(this.ultraLabel6);
            this.uGroupBox2.Controls.Add(this.ultraLabel3);
            this.uGroupBox2.Controls.Add(this.uGridProcInspectFault);
            this.uGroupBox2.Controls.Add(this.uGridProcInspect);
            this.uGroupBox2.Location = new System.Drawing.Point(3, 4);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(894, 328);
            this.uGroupBox2.TabIndex = 70;
            // 
            // uDateAriseDate
            // 
            this.uDateAriseDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateAriseDate.Location = new System.Drawing.Point(518, 28);
            this.uDateAriseDate.Name = "uDateAriseDate";
            this.uDateAriseDate.Size = new System.Drawing.Size(86, 21);
            this.uDateAriseDate.TabIndex = 100;
            // 
            // uComboAriseProcess
            // 
            this.uComboAriseProcess.Location = new System.Drawing.Point(336, 28);
            this.uComboAriseProcess.Name = "uComboAriseProcess";
            this.uComboAriseProcess.Size = new System.Drawing.Size(86, 21);
            this.uComboAriseProcess.TabIndex = 99;
            this.uComboAriseProcess.Text = "ultraComboEditor1";
            // 
            // uButtonDelete2
            // 
            this.uButtonDelete2.Location = new System.Drawing.Point(813, 24);
            this.uButtonDelete2.Name = "uButtonDelete2";
            this.uButtonDelete2.Size = new System.Drawing.Size(75, 28);
            this.uButtonDelete2.TabIndex = 98;
            this.uButtonDelete2.Text = "ultraButton1";
            this.uButtonDelete2.Click += new System.EventHandler(this.uButtonDelete2_Click);
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(10, 28);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelLotNo.TabIndex = 97;
            this.uLabelLotNo.Text = "LotNo";
            // 
            // uTextLotNo
            // 
            this.uTextLotNo.Location = new System.Drawing.Point(103, 28);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(137, 21);
            this.uTextLotNo.TabIndex = 95;
            this.uTextLotNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLotNo_KeyDown);
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(429, 28);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(86, 20);
            this.ultraLabel6.TabIndex = 94;
            this.ultraLabel6.Text = "발생일";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(247, 28);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(86, 20);
            this.ultraLabel3.TabIndex = 92;
            this.ultraLabel3.Text = "발생공정";
            // 
            // uGridProcInspectFault
            // 
            this.uGridProcInspectFault.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcInspectFault.DisplayLayout.Appearance = appearance43;
            this.uGridProcInspectFault.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcInspectFault.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance44.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspectFault.DisplayLayout.GroupByBox.Appearance = appearance44;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcInspectFault.DisplayLayout.GroupByBox.BandLabelAppearance = appearance45;
            this.uGridProcInspectFault.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance46.BackColor2 = System.Drawing.SystemColors.Control;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance46.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcInspectFault.DisplayLayout.GroupByBox.PromptAppearance = appearance46;
            this.uGridProcInspectFault.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcInspectFault.DisplayLayout.MaxRowScrollRegions = 1;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcInspectFault.DisplayLayout.Override.ActiveCellAppearance = appearance47;
            appearance48.BackColor = System.Drawing.SystemColors.Highlight;
            appearance48.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcInspectFault.DisplayLayout.Override.ActiveRowAppearance = appearance48;
            this.uGridProcInspectFault.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcInspectFault.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspectFault.DisplayLayout.Override.CardAreaAppearance = appearance49;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            appearance50.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcInspectFault.DisplayLayout.Override.CellAppearance = appearance50;
            this.uGridProcInspectFault.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcInspectFault.DisplayLayout.Override.CellPadding = 0;
            appearance51.BackColor = System.Drawing.SystemColors.Control;
            appearance51.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance51.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance51.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspectFault.DisplayLayout.Override.GroupByRowAppearance = appearance51;
            appearance52.TextHAlignAsString = "Left";
            this.uGridProcInspectFault.DisplayLayout.Override.HeaderAppearance = appearance52;
            this.uGridProcInspectFault.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcInspectFault.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcInspectFault.DisplayLayout.Override.RowAppearance = appearance53;
            this.uGridProcInspectFault.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcInspectFault.DisplayLayout.Override.TemplateAddRowAppearance = appearance54;
            this.uGridProcInspectFault.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcInspectFault.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcInspectFault.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcInspectFault.Location = new System.Drawing.Point(264, 56);
            this.uGridProcInspectFault.Name = "uGridProcInspectFault";
            this.uGridProcInspectFault.Size = new System.Drawing.Size(620, 264);
            this.uGridProcInspectFault.TabIndex = 65;
            this.uGridProcInspectFault.Text = "ultraGrid1";
            this.uGridProcInspectFault.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridProcInspectFault_AfterCellUpdate);
            this.uGridProcInspectFault.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridProcInspectFault_KeyDown);
            this.uGridProcInspectFault.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridProcInspectFault_ClickCellButton);
            this.uGridProcInspectFault.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridProcInspectFault_DoubleClickCell);
            // 
            // uGridProcInspect
            // 
            this.uGridProcInspect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcInspect.DisplayLayout.Appearance = appearance55;
            this.uGridProcInspect.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcInspect.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspect.DisplayLayout.GroupByBox.Appearance = appearance56;
            appearance57.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcInspect.DisplayLayout.GroupByBox.BandLabelAppearance = appearance57;
            this.uGridProcInspect.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance58.BackColor2 = System.Drawing.SystemColors.Control;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcInspect.DisplayLayout.GroupByBox.PromptAppearance = appearance58;
            this.uGridProcInspect.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcInspect.DisplayLayout.MaxRowScrollRegions = 1;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcInspect.DisplayLayout.Override.ActiveCellAppearance = appearance59;
            appearance60.BackColor = System.Drawing.SystemColors.Highlight;
            appearance60.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcInspect.DisplayLayout.Override.ActiveRowAppearance = appearance60;
            this.uGridProcInspect.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcInspect.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspect.DisplayLayout.Override.CardAreaAppearance = appearance61;
            appearance62.BorderColor = System.Drawing.Color.Silver;
            appearance62.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcInspect.DisplayLayout.Override.CellAppearance = appearance62;
            this.uGridProcInspect.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcInspect.DisplayLayout.Override.CellPadding = 0;
            appearance63.BackColor = System.Drawing.SystemColors.Control;
            appearance63.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance63.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance63.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcInspect.DisplayLayout.Override.GroupByRowAppearance = appearance63;
            appearance64.TextHAlignAsString = "Left";
            this.uGridProcInspect.DisplayLayout.Override.HeaderAppearance = appearance64;
            this.uGridProcInspect.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcInspect.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcInspect.DisplayLayout.Override.RowAppearance = appearance65;
            this.uGridProcInspect.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance66.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcInspect.DisplayLayout.Override.TemplateAddRowAppearance = appearance66;
            this.uGridProcInspect.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcInspect.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcInspect.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcInspect.Location = new System.Drawing.Point(7, 56);
            this.uGridProcInspect.Name = "uGridProcInspect";
            this.uGridProcInspect.Size = new System.Drawing.Size(257, 264);
            this.uGridProcInspect.TabIndex = 64;
            this.uGridProcInspect.Text = "ultraGrid1";
            this.uGridProcInspect.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridProcInspect_DoubleClickRow);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 2;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckSearchFinalCompleteFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchFinalCompleteFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterialGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStdNumber);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMgnNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchAbnomalType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchFaultSeparation);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromWriteDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRegistDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 60);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uCheckSearchFinalCompleteFlag
            // 
            this.uCheckSearchFinalCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckSearchFinalCompleteFlag.Location = new System.Drawing.Point(871, 36);
            this.uCheckSearchFinalCompleteFlag.Name = "uCheckSearchFinalCompleteFlag";
            this.uCheckSearchFinalCompleteFlag.Size = new System.Drawing.Size(14, 20);
            this.uCheckSearchFinalCompleteFlag.TabIndex = 241;
            // 
            // uLabelSearchFinalCompleteFlag
            // 
            this.uLabelSearchFinalCompleteFlag.Location = new System.Drawing.Point(782, 36);
            this.uLabelSearchFinalCompleteFlag.Name = "uLabelSearchFinalCompleteFlag";
            this.uLabelSearchFinalCompleteFlag.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchFinalCompleteFlag.TabIndex = 240;
            this.uLabelSearchFinalCompleteFlag.Text = "최종완료포함";
            // 
            // uComboSearchMaterialGroup
            // 
            this.uComboSearchMaterialGroup.Location = new System.Drawing.Point(99, 12);
            this.uComboSearchMaterialGroup.Name = "uComboSearchMaterialGroup";
            this.uComboSearchMaterialGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchMaterialGroup.TabIndex = 239;
            // 
            // uLabelSearchMaterialGroup
            // 
            this.uLabelSearchMaterialGroup.Location = new System.Drawing.Point(10, 12);
            this.uLabelSearchMaterialGroup.Name = "uLabelSearchMaterialGroup";
            this.uLabelSearchMaterialGroup.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchMaterialGroup.TabIndex = 238;
            this.uLabelSearchMaterialGroup.Text = "자재그룹";
            // 
            // uDateSearchToWriteDate
            // 
            this.uDateSearchToWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToWriteDate.Location = new System.Drawing.Point(783, 12);
            this.uDateSearchToWriteDate.Name = "uDateSearchToWriteDate";
            this.uDateSearchToWriteDate.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchToWriteDate.TabIndex = 52;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(769, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(14, 20);
            this.ultraLabel1.TabIndex = 51;
            this.ultraLabel1.Text = "~";
            // 
            // uTextSearchMaterialCode
            // 
            appearance3.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance3;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton7);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(327, 36);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchMaterialCode.TabIndex = 50;
            this.uTextSearchMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMaterialCode_KeyDown);
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick);
            // 
            // uTextSearchMaterialName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Appearance = appearance4;
            this.uTextSearchMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(414, 36);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.ReadOnly = true;
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(171, 21);
            this.uTextSearchMaterialName.TabIndex = 49;
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(237, 36);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchMaterial.TabIndex = 48;
            this.uLabelSearchMaterial.Text = "ultraLabel2";
            // 
            // uTextSearchStdNumber
            // 
            this.uTextSearchStdNumber.Location = new System.Drawing.Point(99, 36);
            this.uTextSearchStdNumber.Name = "uTextSearchStdNumber";
            this.uTextSearchStdNumber.Size = new System.Drawing.Size(129, 21);
            this.uTextSearchStdNumber.TabIndex = 47;
            // 
            // uLabelSearchMgnNo
            // 
            this.uLabelSearchMgnNo.Location = new System.Drawing.Point(10, 36);
            this.uLabelSearchMgnNo.Name = "uLabelSearchMgnNo";
            this.uLabelSearchMgnNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchMgnNo.TabIndex = 46;
            this.uLabelSearchMgnNo.Text = "ultraLabel1";
            // 
            // uComboSearchAbnomalType
            // 
            this.uComboSearchAbnomalType.Location = new System.Drawing.Point(681, 36);
            this.uComboSearchAbnomalType.Name = "uComboSearchAbnomalType";
            this.uComboSearchAbnomalType.Size = new System.Drawing.Size(86, 21);
            this.uComboSearchAbnomalType.TabIndex = 45;
            this.uComboSearchAbnomalType.Text = "ultraComboEditor1";
            // 
            // uLabelSearchFaultSeparation
            // 
            this.uLabelSearchFaultSeparation.Location = new System.Drawing.Point(593, 36);
            this.uLabelSearchFaultSeparation.Name = "uLabelSearchFaultSeparation";
            this.uLabelSearchFaultSeparation.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchFaultSeparation.TabIndex = 44;
            this.uLabelSearchFaultSeparation.Text = "ultraLabel1";
            // 
            // uTextSearchVendorName
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance5;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(414, 12);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(171, 21);
            this.uTextSearchVendorName.TabIndex = 40;
            // 
            // uTextSearchVendorCode
            // 
            appearance6.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance6.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton8.Appearance = appearance6;
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton8);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(326, 12);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchVendorCode.TabIndex = 39;
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(237, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCustomer.TabIndex = 38;
            this.uLabelSearchCustomer.Text = "ultraLabel2";
            // 
            // uDateSearchFromWriteDate
            // 
            this.uDateSearchFromWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromWriteDate.Location = new System.Drawing.Point(681, 12);
            this.uDateSearchFromWriteDate.Name = "uDateSearchFromWriteDate";
            this.uDateSearchFromWriteDate.Size = new System.Drawing.Size(86, 21);
            this.uDateSearchFromWriteDate.TabIndex = 37;
            // 
            // uLabelSearchRegistDate
            // 
            this.uLabelSearchRegistDate.Location = new System.Drawing.Point(593, 12);
            this.uLabelSearchRegistDate.Name = "uLabelSearchRegistDate";
            this.uLabelSearchRegistDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchRegistDate.TabIndex = 36;
            this.uLabelSearchRegistDate.Text = "ultraLabel2";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(895, 4);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(17, 21);
            this.uComboSearchPlant.TabIndex = 5;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(874, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(17, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridHeader
            // 
            this.uGridHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHeader.DisplayLayout.Appearance = appearance7;
            this.uGridHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.uGridHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.uGridHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHeader.DisplayLayout.Override.ActiveCellAppearance = appearance11;
            appearance12.BackColor = System.Drawing.SystemColors.Highlight;
            appearance12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHeader.DisplayLayout.Override.ActiveRowAppearance = appearance12;
            this.uGridHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            appearance14.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHeader.DisplayLayout.Override.CellAppearance = appearance14;
            this.uGridHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHeader.DisplayLayout.Override.CellPadding = 0;
            appearance15.BackColor = System.Drawing.SystemColors.Control;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.GroupByRowAppearance = appearance15;
            appearance16.TextHAlignAsString = "Left";
            this.uGridHeader.DisplayLayout.Override.HeaderAppearance = appearance16;
            this.uGridHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridHeader.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance18;
            this.uGridHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHeader.Location = new System.Drawing.Point(0, 100);
            this.uGridHeader.Name = "uGridHeader";
            this.uGridHeader.Size = new System.Drawing.Size(917, 650);
            this.uGridHeader.TabIndex = 4;
            this.uGridHeader.Text = "ultraGrid1";
            this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridHeader_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(917, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(917, 675);
            this.uGroupBoxContentsArea.TabIndex = 5;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabAction);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialTreate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialTreate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckFinalCompleteFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFinalCompleteFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTabControl1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(911, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTabAction
            // 
            this.uTabAction.Controls.Add(this.ultraTabSharedControlsPage3);
            this.uTabAction.Controls.Add(this.ultraTabPageControl3);
            this.uTabAction.Controls.Add(this.ultraTabPageControl4);
            this.uTabAction.Location = new System.Drawing.Point(0, 516);
            this.uTabAction.Name = "uTabAction";
            this.uTabAction.SharedControlsPage = this.ultraTabSharedControlsPage3;
            this.uTabAction.Size = new System.Drawing.Size(905, 104);
            this.uTabAction.TabIndex = 121;
            ultraTab3.Key = "Short";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "단기조치사항";
            ultraTab4.Key = "Long";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "장기조치사항";
            this.uTabAction.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab3,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage3
            // 
            this.ultraTabSharedControlsPage3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage3.Name = "ultraTabSharedControlsPage3";
            this.ultraTabSharedControlsPage3.Size = new System.Drawing.Size(901, 78);
            // 
            // uTextMaterialTreate
            // 
            this.uTextMaterialTreate.Location = new System.Drawing.Point(93, 628);
            this.uTextMaterialTreate.Name = "uTextMaterialTreate";
            this.uTextMaterialTreate.Size = new System.Drawing.Size(686, 21);
            this.uTextMaterialTreate.TabIndex = 120;
            // 
            // uLabelMaterialTreate
            // 
            this.uLabelMaterialTreate.Location = new System.Drawing.Point(3, 627);
            this.uLabelMaterialTreate.Name = "uLabelMaterialTreate";
            this.uLabelMaterialTreate.Size = new System.Drawing.Size(86, 20);
            this.uLabelMaterialTreate.TabIndex = 119;
            this.uLabelMaterialTreate.Text = "자재처리결과";
            // 
            // uCheckFinalCompleteFlag
            // 
            this.uCheckFinalCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckFinalCompleteFlag.Location = new System.Drawing.Point(881, 628);
            this.uCheckFinalCompleteFlag.Name = "uCheckFinalCompleteFlag";
            this.uCheckFinalCompleteFlag.Size = new System.Drawing.Size(14, 20);
            this.uCheckFinalCompleteFlag.TabIndex = 118;
            // 
            // uLabelFinalCompleteFlag
            // 
            this.uLabelFinalCompleteFlag.Location = new System.Drawing.Point(792, 628);
            this.uLabelFinalCompleteFlag.Name = "uLabelFinalCompleteFlag";
            this.uLabelFinalCompleteFlag.Size = new System.Drawing.Size(86, 20);
            this.uLabelFinalCompleteFlag.TabIndex = 117;
            this.uLabelFinalCompleteFlag.Text = "최종작성완료";
            // 
            // uTextInspectDesc
            // 
            this.uTextInspectDesc.Location = new System.Drawing.Point(93, 489);
            this.uTextInspectDesc.Name = "uTextInspectDesc";
            this.uTextInspectDesc.Size = new System.Drawing.Size(809, 21);
            this.uTextInspectDesc.TabIndex = 116;
            // 
            // uLabelInspectDesc
            // 
            this.uLabelInspectDesc.Location = new System.Drawing.Point(3, 488);
            this.uLabelInspectDesc.Name = "uLabelInspectDesc";
            this.uLabelInspectDesc.Size = new System.Drawing.Size(86, 20);
            this.uLabelInspectDesc.TabIndex = 115;
            this.uLabelInspectDesc.Text = "이상발생내용";
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage2);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(0, 120);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.ultraTabControl1.Size = new System.Drawing.Size(905, 364);
            this.ultraTabControl1.TabIndex = 107;
            ultraTab1.Key = "Mat";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "수입이상";
            ultraTab2.Key = "Proc";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "공정이상";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(901, 338);
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uTextWMSTFlag);
            this.uGroupBox3.Controls.Add(this.uTextMESTFlag);
            this.uGroupBox3.Controls.Add(this.uTextCompleteFlag);
            this.uGroupBox3.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBox3.Controls.Add(this.uLabelCompleteFlag);
            this.uGroupBox3.Controls.Add(this.uComboAbnormalType);
            this.uGroupBox3.Controls.Add(this.uComboStatus);
            this.uGroupBox3.Controls.Add(this.uLabelStatus);
            this.uGroupBox3.Controls.Add(this.uLabelAbnormalType);
            this.uGroupBox3.Controls.Add(this.uTextTotalQty);
            this.uGroupBox3.Controls.Add(this.uDateWriteDate);
            this.uGroupBox3.Controls.Add(this.uLabelWriteDate);
            this.uGroupBox3.Controls.Add(this.uLabelWriteUser);
            this.uGroupBox3.Controls.Add(this.uTextWriteUserID);
            this.uGroupBox3.Controls.Add(this.uTextWriteName);
            this.uGroupBox3.Controls.Add(this.uLalbeTotalQty);
            this.uGroupBox3.Controls.Add(this.uLabelGRDate);
            this.uGroupBox3.Controls.Add(this.uTextMoldSeq);
            this.uGroupBox3.Controls.Add(this.uLabelMoldSeq);
            this.uGroupBox3.Controls.Add(this.uTextMaterialCode);
            this.uGroupBox3.Controls.Add(this.uTextVendorCode);
            this.uGroupBox3.Controls.Add(this.uTextGRDate);
            this.uGroupBox3.Controls.Add(this.uTextMaterialSpec1);
            this.uGroupBox3.Controls.Add(this.uLabelMaterialSpec1);
            this.uGroupBox3.Controls.Add(this.uTextMaterialName);
            this.uGroupBox3.Controls.Add(this.uLabelMaterial);
            this.uGroupBox3.Controls.Add(this.uTextVendorName);
            this.uGroupBox3.Controls.Add(this.uLabelCustomer);
            this.uGroupBox3.Controls.Add(this.uTextStdNumber);
            this.uGroupBox3.Controls.Add(this.uLabelMgnNum);
            this.uGroupBox3.Controls.Add(this.uTextPlant);
            this.uGroupBox3.Controls.Add(this.uLabelPlant);
            this.uGroupBox3.Location = new System.Drawing.Point(0, 4);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(904, 112);
            this.uGroupBox3.TabIndex = 89;
            // 
            // uTextWMSTFlag
            // 
            this.uTextWMSTFlag.Location = new System.Drawing.Point(881, 56);
            this.uTextWMSTFlag.Name = "uTextWMSTFlag";
            this.uTextWMSTFlag.ReadOnly = true;
            this.uTextWMSTFlag.Size = new System.Drawing.Size(18, 21);
            this.uTextWMSTFlag.TabIndex = 104;
            this.uTextWMSTFlag.Visible = false;
            // 
            // uTextMESTFlag
            // 
            this.uTextMESTFlag.Location = new System.Drawing.Point(881, 32);
            this.uTextMESTFlag.Name = "uTextMESTFlag";
            this.uTextMESTFlag.ReadOnly = true;
            this.uTextMESTFlag.Size = new System.Drawing.Size(18, 21);
            this.uTextMESTFlag.TabIndex = 103;
            this.uTextMESTFlag.Text = "MESTFlag";
            this.uTextMESTFlag.Visible = false;
            // 
            // uTextCompleteFlag
            // 
            this.uTextCompleteFlag.Location = new System.Drawing.Point(861, 32);
            this.uTextCompleteFlag.Name = "uTextCompleteFlag";
            this.uTextCompleteFlag.ReadOnly = true;
            this.uTextCompleteFlag.Size = new System.Drawing.Size(18, 21);
            this.uTextCompleteFlag.TabIndex = 102;
            this.uTextCompleteFlag.Text = "CompleteFlag";
            this.uTextCompleteFlag.Visible = false;
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(552, 80);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(14, 20);
            this.uCheckCompleteFlag.TabIndex = 101;
            // 
            // uLabelCompleteFlag
            // 
            this.uLabelCompleteFlag.Location = new System.Drawing.Point(463, 80);
            this.uLabelCompleteFlag.Name = "uLabelCompleteFlag";
            this.uLabelCompleteFlag.Size = new System.Drawing.Size(86, 20);
            this.uLabelCompleteFlag.TabIndex = 100;
            this.uLabelCompleteFlag.Text = "작성완료";
            // 
            // uComboAbnormalType
            // 
            this.uComboAbnormalType.Location = new System.Drawing.Point(552, 56);
            this.uComboAbnormalType.Name = "uComboAbnormalType";
            this.uComboAbnormalType.Size = new System.Drawing.Size(86, 21);
            this.uComboAbnormalType.TabIndex = 99;
            this.uComboAbnormalType.Text = "ultraComboEditor1";
            // 
            // uComboStatus
            // 
            this.uComboStatus.Location = new System.Drawing.Point(552, 32);
            this.uComboStatus.Name = "uComboStatus";
            this.uComboStatus.Size = new System.Drawing.Size(113, 21);
            this.uComboStatus.TabIndex = 93;
            this.uComboStatus.Text = "ultraComboEditor1";
            // 
            // uLabelStatus
            // 
            this.uLabelStatus.Location = new System.Drawing.Point(463, 32);
            this.uLabelStatus.Name = "uLabelStatus";
            this.uLabelStatus.Size = new System.Drawing.Size(86, 20);
            this.uLabelStatus.TabIndex = 92;
            this.uLabelStatus.Text = "상태";
            // 
            // uLabelAbnormalType
            // 
            this.uLabelAbnormalType.Location = new System.Drawing.Point(463, 56);
            this.uLabelAbnormalType.Name = "uLabelAbnormalType";
            this.uLabelAbnormalType.Size = new System.Drawing.Size(86, 20);
            this.uLabelAbnormalType.TabIndex = 90;
            this.uLabelAbnormalType.Text = "이상발생구분";
            // 
            // uTextTotalQty
            // 
            appearance72.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTotalQty.Appearance = appearance72;
            this.uTextTotalQty.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTotalQty.Location = new System.Drawing.Point(802, 84);
            this.uTextTotalQty.Name = "uTextTotalQty";
            this.uTextTotalQty.ReadOnly = true;
            this.uTextTotalQty.Size = new System.Drawing.Size(14, 21);
            this.uTextTotalQty.TabIndex = 89;
            this.uTextTotalQty.Visible = false;
            // 
            // uDateWriteDate
            // 
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(99, 80);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(86, 21);
            this.uDateWriteDate.TabIndex = 88;
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(10, 80);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelWriteDate.TabIndex = 87;
            this.uLabelWriteDate.Text = "등록일";
            // 
            // uLabelWriteUser
            // 
            this.uLabelWriteUser.Location = new System.Drawing.Point(192, 80);
            this.uLabelWriteUser.Name = "uLabelWriteUser";
            this.uLabelWriteUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelWriteUser.TabIndex = 85;
            this.uLabelWriteUser.Text = "등록자";
            // 
            // uTextWriteUserID
            // 
            appearance38.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteUserID.Appearance = appearance38;
            this.uTextWriteUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance39.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance39.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton9.Appearance = appearance39;
            editorButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteUserID.ButtonsRight.Add(editorButton9);
            this.uTextWriteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteUserID.Location = new System.Drawing.Point(281, 80);
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextWriteUserID.TabIndex = 84;
            this.uTextWriteUserID.ValueChanged += new System.EventHandler(this.uTextWriteUserID_ValueChanged);
            this.uTextWriteUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteUserID_KeyDown);
            this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteUserID_EditorButtonClick);
            // 
            // uTextWriteName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance40;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(370, 80);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(86, 21);
            this.uTextWriteName.TabIndex = 83;
            // 
            // uLalbeTotalQty
            // 
            this.uLalbeTotalQty.Location = new System.Drawing.Point(775, 84);
            this.uLalbeTotalQty.Name = "uLalbeTotalQty";
            this.uLalbeTotalQty.Size = new System.Drawing.Size(21, 20);
            this.uLalbeTotalQty.TabIndex = 76;
            this.uLalbeTotalQty.Text = "입고수량";
            this.uLalbeTotalQty.Visible = false;
            // 
            // uLabelGRDate
            // 
            this.uLabelGRDate.Location = new System.Drawing.Point(665, 56);
            this.uLabelGRDate.Name = "uLabelGRDate";
            this.uLabelGRDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelGRDate.TabIndex = 74;
            this.uLabelGRDate.Text = "입고일";
            // 
            // uTextMoldSeq
            // 
            appearance76.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Appearance = appearance76;
            this.uTextMoldSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Location = new System.Drawing.Point(843, 32);
            this.uTextMoldSeq.Name = "uTextMoldSeq";
            this.uTextMoldSeq.ReadOnly = true;
            this.uTextMoldSeq.Size = new System.Drawing.Size(24, 21);
            this.uTextMoldSeq.TabIndex = 73;
            this.uTextMoldSeq.Visible = false;
            // 
            // uLabelMoldSeq
            // 
            this.uLabelMoldSeq.Location = new System.Drawing.Point(819, 32);
            this.uLabelMoldSeq.Name = "uLabelMoldSeq";
            this.uLabelMoldSeq.Size = new System.Drawing.Size(24, 20);
            this.uLabelMoldSeq.TabIndex = 72;
            this.uLabelMoldSeq.Text = "자재규격2";
            this.uLabelMoldSeq.Visible = false;
            // 
            // uTextMaterialCode
            // 
            appearance77.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Appearance = appearance77;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Location = new System.Drawing.Point(99, 56);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.ReadOnly = true;
            this.uTextMaterialCode.Size = new System.Drawing.Size(86, 21);
            this.uTextMaterialCode.TabIndex = 71;
            // 
            // uTextVendorCode
            // 
            appearance78.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorCode.Appearance = appearance78;
            this.uTextVendorCode.BackColor = System.Drawing.Color.Gainsboro;
            appearance36.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance36.TextHAlignAsString = "Center";
            editorButton10.Appearance = appearance36;
            editorButton10.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextVendorCode.ButtonsRight.Add(editorButton10);
            this.uTextVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextVendorCode.Location = new System.Drawing.Point(281, 32);
            this.uTextVendorCode.Name = "uTextVendorCode";
            this.uTextVendorCode.ReadOnly = true;
            this.uTextVendorCode.Size = new System.Drawing.Size(86, 21);
            this.uTextVendorCode.TabIndex = 70;
            this.uTextVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextVendorCode_EditorButtonClick);
            // 
            // uTextGRDate
            // 
            appearance79.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Appearance = appearance79;
            this.uTextGRDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Location = new System.Drawing.Point(754, 56);
            this.uTextGRDate.Name = "uTextGRDate";
            this.uTextGRDate.ReadOnly = true;
            this.uTextGRDate.Size = new System.Drawing.Size(86, 21);
            this.uTextGRDate.TabIndex = 63;
            // 
            // uTextMaterialSpec1
            // 
            appearance80.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialSpec1.Appearance = appearance80;
            this.uTextMaterialSpec1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialSpec1.Location = new System.Drawing.Point(850, 80);
            this.uTextMaterialSpec1.Name = "uTextMaterialSpec1";
            this.uTextMaterialSpec1.ReadOnly = true;
            this.uTextMaterialSpec1.Size = new System.Drawing.Size(21, 21);
            this.uTextMaterialSpec1.TabIndex = 61;
            this.uTextMaterialSpec1.Visible = false;
            // 
            // uLabelMaterialSpec1
            // 
            this.uLabelMaterialSpec1.Location = new System.Drawing.Point(826, 80);
            this.uLabelMaterialSpec1.Name = "uLabelMaterialSpec1";
            this.uLabelMaterialSpec1.Size = new System.Drawing.Size(21, 20);
            this.uLabelMaterialSpec1.TabIndex = 60;
            this.uLabelMaterialSpec1.Text = "자재규격1";
            this.uLabelMaterialSpec1.Visible = false;
            // 
            // uTextMaterialName
            // 
            appearance81.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance81;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(189, 56);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(267, 21);
            this.uTextMaterialName.TabIndex = 58;
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(10, 56);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(86, 20);
            this.uLabelMaterial.TabIndex = 57;
            this.uLabelMaterial.Text = "자재코드";
            this.uLabelMaterial.Click += new System.EventHandler(this.frmINS0017_Activated);
            // 
            // uTextVendorName
            // 
            appearance82.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance82;
            this.uTextVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Location = new System.Drawing.Point(370, 32);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(86, 21);
            this.uTextVendorName.TabIndex = 56;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(192, 32);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelCustomer.TabIndex = 54;
            this.uLabelCustomer.Text = "거래처";
            // 
            // uTextStdNumber
            // 
            appearance83.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance83;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(99, 32);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(86, 21);
            this.uTextStdNumber.TabIndex = 51;
            // 
            // uLabelMgnNum
            // 
            this.uLabelMgnNum.Location = new System.Drawing.Point(10, 32);
            this.uLabelMgnNum.Name = "uLabelMgnNum";
            this.uLabelMgnNum.Size = new System.Drawing.Size(86, 20);
            this.uLabelMgnNum.TabIndex = 50;
            this.uLabelMgnNum.Text = "관리번호";
            // 
            // uTextPlant
            // 
            appearance84.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance84;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(850, 56);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(27, 21);
            this.uTextPlant.TabIndex = 49;
            this.uTextPlant.Visible = false;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(819, 60);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(27, 20);
            this.uLabelPlant.TabIndex = 48;
            this.uLabelPlant.Text = "공장";
            this.uLabelPlant.Visible = false;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(1, 20);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(368, 217);
            // 
            // frmINS0017
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINS0017";
            this.Load += new System.EventHandler(this.frmINS0017_Load);
            this.Activated += new System.EventHandler(this.frmINS0017_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINS0017_FormClosing);
            this.Resize += new System.EventHandler(this.frmINS0017_Resize);
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortConfirmDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortConfirmUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortConfirmUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShortConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptShortConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateShortAcceptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShortActionDesc)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.ultraTabPageControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongConfirmDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongConfirmUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongConfirmUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateLongConfirmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptLongConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateLongAcceptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLongActionDesc)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatInspectFault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatInspect)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcInspectFault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcInspect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchFinalCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchAbnomalType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabAction)).EndInit();
            this.uTabAction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialTreate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFinalCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWMSTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAbnormalType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotalQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialSpec1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRegistDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchAbnomalType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchFaultSeparation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMgnNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHeader;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelMgnNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialSpec1;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialSpec1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.Misc.UltraLabel uLalbeTotalQty;
        private Infragistics.Win.Misc.UltraLabel uLabelGRDate;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTotalQty;
        private Infragistics.Win.Misc.UltraLabel uLabelAbnormalType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStatus;
        private Infragistics.Win.Misc.UltraLabel uLabelStatus;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAbnormalType;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToWriteDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWMSTFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSearchFinalCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchFinalCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialGroup;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMatInspectFault;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMatInspect;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAriseProcess;
        private Infragistics.Win.Misc.UltraButton uButtonDelete2;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcInspectFault;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcInspect;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialGroup;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
        private Infragistics.Win.Misc.UltraButton uButtonDelete1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialTreate;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialTreate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFinalCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelFinalCompleteFlag;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabAction;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraLabel uLabelShortUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShortUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelShortActionDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateShortAcceptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachDate1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShortActionDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile1;
        private Infragistics.Win.Misc.UltraLabel uLabelShortEmail;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShortEmail;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptShortConfirm;
        private Infragistics.Win.Misc.UltraLabel uLabelShortConfirm;
        private Infragistics.Win.Misc.UltraLabel uLabelShortConfirmUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShortConfirmUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShortConfirmUserName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateShortConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelShortConfirmDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShortConfirmDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateLongAcceptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachDate2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLongActionDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLongConfirmDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelLongConfirmUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLongConfirmUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLongConfirmUserName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateLongConfirmDate;
        private Infragistics.Win.Misc.UltraLabel uLabelLongConfirmDate;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptLongConfirm;
        private Infragistics.Win.Misc.UltraLabel uLabelLongConfirm;
        private Infragistics.Win.Misc.UltraLabel uLabelLongEmail;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLongEmail;
        private Infragistics.Win.Misc.UltraLabel uLabelLongActionDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLongUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelLongUserName;
        private Infragistics.Win.Misc.UltraButton uButtonMoveDisplay;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShortFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLongFile;
    }
}