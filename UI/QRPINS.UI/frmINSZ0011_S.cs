﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0011.cs                                        */
/* 프로그램명   : QCN LIST                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-16 : 자재재검/장비재검 팝업창 추가 (이종호)   */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;
using System.IO;

namespace QRPINS.UI
{
    public partial class frmINSZ0011_S : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string m_strPlantCode = "";

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public frmINSZ0011_S()
        {
            InitializeComponent();
        }

        private void frmINSZ0011_Activated(object sender, EventArgs e)
        {
            // 툴바 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0011_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;

            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("QCN LIST", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitTab();
            InitEtc();
            InitGroupBox();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmINSZ0011_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method

        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                System.Resources.ResourceSet m_resSys = new System.Resources.ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinTabControl wTab = new QRPCOM.QRPUI.WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabGubun, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                this.uDateSearchAriseDateFrom.Value = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                this.uDateSerachAriseDateTo.Value = DateTime.Now.ToString("yyyy-MM-dd");

                // Top부분 체크박스 Enable 상태로
                SetCheckBoxEnable(this.uCheckWorkStepEquipFlag);
                SetCheckBoxEnable(this.uCheckWorkStepMaterialFlag);
                SetCheckBoxEnable(this.uCheckActionFlag1);
                SetCheckBoxEnable(this.uCheckActionFlag2);
                SetCheckBoxEnable(this.uCheckActionFlag3);
                SetCheckBoxEnable(this.uCheckActionFlag4);
                SetCheckBoxEnable(this.uCheckActionFlag5);
                SetCheckBoxEnable(this.uCheckActionFlag6);
                SetCheckBoxEnable(this.uCheckActionFlag7);
                SetCheckBoxEnable(this.uCheckActionFlag8);
                SetCheckBoxEnable(this.uCheckActionFlag9);

                //사용자ID 텍스트 박스는 대문자만
                this.uTextFirCauseUserID.ImeMode = ImeMode.Disable;
                this.uTextFirCauseUserID.CharacterCasing = CharacterCasing.Upper;
                this.uTextFirCorrectUserID.ImeMode = ImeMode.Disable;
                this.uTextFirCorrectUserID.CharacterCasing = CharacterCasing.Upper;
                this.uTextFirMeasureUserID.ImeMode = ImeMode.Disable;
                this.uTextFirMeasureUserID.CharacterCasing = CharacterCasing.Upper;
                this.uTextSecActionUserID.ImeMode = ImeMode.Disable;
                this.uTextSecActionUserID.CharacterCasing = CharacterCasing.Upper;
                this.uTextQCConfirmUserID.ImeMode = ImeMode.Disable;
                this.uTextQCConfirmUserID.CharacterCasing = CharacterCasing.Upper;

                // 텍스트박스 MaxLength 지정
                this.uTextSearchQCNNo.MaxLength = 20;
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextFirCauseDesc.MaxLength = 200;
                this.uTextFirCauseUserID.MaxLength = 20;
                this.uTextFirCorrectDesc.MaxLength = 200;
                this.uTextFirCorrectUserID.MaxLength = 20;
                this.uTextFirMeasureDesc.MaxLength = 200;
                this.uTextFirMeasureUserID.MaxLength = 20;
                this.uTextSecActionDesc.MaxLength = 200;
                this.uTextSecActionUserID.MaxLength = 20;
                this.uTextEtcDesc.MaxLength = 500;

                this.uNumThiFCostPerson.MaskInput = "nnnnnnnn";
                this.uNumThiFCostQty.MaskInput = "nnnnnnnn";
                this.uNumThiFCostScrap.MaskInput = "nnnnnnnn";
                this.uNumThiFCostTime.MaskInput = "nnnnnnnn";

                // 작성 시간 편집불가, 시간 지정
                //완료가 체크되지 않은 항목에는 현재 시간을 받아온다.
                this.uDateFirCauseTime.ReadOnly = true;
                this.uDateFirCauseTime.Appearance.BackColor = Color.Gainsboro;
                if (this.uCheckFirCauseCompleteFlag.Checked == false)
                {
                    this.uDateFirCauseTime.Value = DateTime.Now;
                }

                this.uDateFirCorrectTime.ReadOnly = true;
                this.uDateFirCorrectTime.Appearance.BackColor = Color.Gainsboro;
                if (this.uCheckFirCorrectCompleteFlag.Checked == false)
                {
                    this.uDateFirCorrectTime.Value = DateTime.Now;
                }


                this.uDateFirMeasureTime.ReadOnly = true;
                this.uDateFirMeasureTime.Appearance.BackColor = Color.Gainsboro;
                if (this.uCheckFirMeasureCompleteFlag.Checked == false)
                {
                    this.uDateFirMeasureTime.Value = DateTime.Now;
                }


                this.uDateSecActionTime.ReadOnly = true;
                this.uDateSecActionTime.Appearance.BackColor = Color.Gainsboro;
                if (this.uCheckSecActionCompleteFlag.Checked == false)
                {
                    this.uDateSecActionTime.Value = DateTime.Now;
                }

                //최종 합격 여부
                this.uDateQCConfirmDate.ReadOnly = true;
                this.uDateQCConfirmDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateQCConfirmTime.ReadOnly = true;
                this.uDateQCConfirmTime.Appearance.BackColor = Color.Gainsboro;
                if (this.uCheckQCConfirmFlag.Checked == false)
                {
                    //this.uDateQCConfirmTime.Value = DateTime.Now;
                    //this.uDateQCConfirmTime.Value = DateTime.Now;
                    this.uDateQCConfirmTime.Value = null;
                    this.uDateQCConfirmTime.Value = null;
                }

                
                //Desc TextBox 스크롤바 생성
                this.uTextFirCauseDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextFirCauseDesc.SelectionStart = uTextFirCauseDesc.Text.Length;
                this.uTextFirCauseDesc.ScrollToCaret();

                this.uTextFirCorrectDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextFirCorrectDesc.SelectionStart = uTextFirCauseDesc.Text.Length;
                this.uTextFirCorrectDesc.ScrollToCaret();

                this.uTextFirMeasureDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextFirMeasureDesc.SelectionStart = uTextFirCauseDesc.Text.Length;
                this.uTextFirMeasureDesc.ScrollToCaret();

                this.uTextSecActionDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextSecActionDesc.SelectionStart = uTextFirCauseDesc.Text.Length;
                this.uTextSecActionDesc.ScrollToCaret();

                // 숫자 오른쪽 정렬
                this.uTextQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextFaultQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextInspectQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                this.uTextRetestFaultQty1.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextRetestFaultQty2.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextRetestFaultQty3.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextRetestInspectQty1.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextRetestInspectQty2.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextRetestInspectQty3.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                this.uTextProgram.MaxLength = 100;

                this.uTextFirReturnDesc.Clear();
                this.uTextFirReturnDesc.MaxLength = 500;
                this.uTextSecReturnDesc.Clear();
                this.uTextSecReturnDesc.MaxLength = 500;
                this.uTextThiReturnDesc.Clear();
                this.uTextThiReturnDesc.MaxLength = 500;

                // 이벤트 설정
                this.uNumThiFCostPerson.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uNumThiFCostPerson_EditorButtonClick);
                this.uNumThiFCostPerson.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(uNumThiFCostPerson_EditorSpinButtonClick);
                this.uNumThiFCostQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uNumThiFCostQty_EditorButtonClick);
                this.uNumThiFCostQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(uNumThiFCostQty_EditorSpinButtonClick);
                this.uNumThiFCostScrap.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uNumThiFCostScrap_EditorButtonClick);
                this.uNumThiFCostScrap.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(uNumThiFCostScrap_EditorSpinButtonClick);
                this.uNumThiFCostTime.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uNumThiFCostTime_EditorButtonClick);
                this.uNumThiFCostTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(uNumThiFCostTime_EditorSpinButtonClick);
                this.uTextFaultImg.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextFaultImg_EditorButtonClick);
                this.uTextFaultImg.KeyDown +=new KeyEventHandler(uTextFaultImg_KeyDown);
                this.uTextFirCauseUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextFirCauseUserID_EditorButtonClick);
                this.uTextFirCauseUserID.KeyDown += new KeyEventHandler(uTextFirCauseUserID_KeyDown);
                this.uTextFirCorrectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextFirCorrectUserID_EditorButtonClick);
                this.uTextFirCorrectUserID.KeyDown += new KeyEventHandler(uTextFirCorrectUserID_KeyDown);
                this.uTextFirMeasureUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextFirMeasureUserID_EditorButtonClick);
                this.uTextFirMeasureUserID.KeyDown += new KeyEventHandler(uTextFirMeasureUserID_KeyDown);
                this.uTextQCConfirmUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextQCConfirmUserID_EditorButtonClick);
                this.uTextQCConfirmUserID.KeyDown += new KeyEventHandler(uTextQCConfirmUserID_KeyDown);
                this.uTextSearchProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSearchProductCode_EditorButtonClick);
                this.uTextSecActionUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSecActionUserID_EditorButtonClick);
                this.uTextSecActionUserID.KeyDown += new KeyEventHandler(uTextSecActionUserID_KeyDown);
                this.uCheckQCConfirmFlag.Click +=new EventHandler(uCheckQCConfirmFlag_Click);
                this.uComboSearchPlantCode.ValueChanged += new EventHandler(uComboSearchPlant_ValueChanged);
                this.uOptionSetConfirmResultFlag.ValueChanged += new EventHandler(uOptionSetConfirmResultFlag_ValueChanged);
                this.uCheckQCConfirmFlag.CheckedChanged += new System.EventHandler(this.uCheckQCConfirmFlag_CheckedChanged);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxExpectEquip, GroupBoxType.LIST, "예상공정", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxExpectEquip.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxExpectEquip.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "Affect Lot 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBoxFault, GroupBoxType.LIST, "불량유형정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxFault.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxFault.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "조치사항", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.DETAIL, "1차 확인", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.DETAIL, "2차 확인", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBox5, GroupBoxType.DETAIL, "3차 확인_F-COST 현황(재작업:선별포함)", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox5.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox5.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBoxQCConfirm, GroupBoxType.DETAIL, "QCConfirm", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxQCConfirm.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxQCConfirm.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxQCConfirm.Visible = false;

                ////Size size = new Size(1070, 660);
                ////this.uGroupBoxContentsArea.MaximumSize = size;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchQCNNo, "QCNNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchAriseDate, "발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchGubun, "구분", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelQCNNo, "QCNNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGubun, "구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerProductSpec, "고객사 제품명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseProcess, "발생공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquip, "발생설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelExpectProcess, "예상공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOccurDate, "발행일시", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelQty, "수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectQty, "검사수량", m_resSys.GetString("SYS_FONTNAME"), true, false);           
                wLabel.mfSetLabel(this.uLabelFaultType, "불량유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFaultImg, "불량Image", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWorkSetup, "작업Setup", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUserName, "발행자", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //wLabel.mfSetLabel(this.uLabelCause, "원인 및 검토", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCause, "基本信息及发生原因", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFirCauseUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFirCauseDate, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);                                
                //wLabel.mfSetLabel(this.uLabelCurrect, "시정조치(장비/기타)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCurrect, "改正措施（设备/其他）", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFirCorrectUserID, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelIfrCorrectDate, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelMeasure, "대책 사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFirMeasureUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFirMeasureDate, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFirResult, "1차결과", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSecAction, "기 생산 자재 조치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSecActionUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSecActionDate, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSecResult, "2차결과", m_resSys.GetString("SYS_FONTNAME"), true, false);                

                wLabel.mfSetLabel(this.uLabelThiFCostQty, "수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelThiFCostTime, "시간(HR)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLablThiFCostPerson, "인원", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelThiFCostScrap, "Scrap", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelThiResult, "3차결과", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelImputeDept, "귀책부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelQCConfirmUser, "최종 Confirm User", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelResultFlag, "최종결과", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelQCNCancel, "QCN 취소여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelQCConfirmFlag, "QC 최종 Confirm", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelRetest1, "Retest #1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRetest2, "Retest #2", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRetest3, "Retest #3", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProgram, "PROGRAM", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSYSTEM, "SYSTEM", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelHandler, "HANDLER", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelQAENGR, "预防再发生措施", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelFirReturnDesc, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSecReturnDesc, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelThiReturnDesc, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelAttachFile1, "附件", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                this.uLabelQAENGR.Visible = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                //wButton.mfSetButton(this.uButton1stReturn, "1차 Return", m_resSys.GetString("SYS_FONTNAME"), null);
                wButton.mfSetButton(this.uButtonEquip, "장비재검", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonMaterial, "자재재검", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                wButton.mfSetButton(this.uButtonQCNCancel, "QCN 취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);

                this.uButtonMaterial.Visible = false;
                this.uButtonEquip.Visible = false;

                this.uButtonEquip.Click += new EventHandler(uButtonEquip_Click);
                this.uButtonMaterial.Click += new EventHandler(uButtonMaterial_Click);
                this.uButtonQCNCancel.Click += new EventHandler(uButtonQCNCancel_Click_1);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                clsPlant.Dispose();

                wCombo.mfSetComboEditor(this.uComboSearchPlantCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtCom = clsCommonCode.mfReadCommonCode("C0068", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchP_QCNITR, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "All"
                    , "ComCode", "ComCodeName", dtCom);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 그리드
                // QCN LIST GRID
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridQCNList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "QCNNo", "QCNNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "P_QcnItrType", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "CustomerCode", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseDate", "발행일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseTime", "발행시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 90, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseProcessCode", "발생공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseProcessName", "발생공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseEquipCode", "설비 번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "InspectUserID", "발행자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "InspectUserName", "발행자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "InspectFaultTypeName", "불량명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //진행step 추가

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "Step", "진행Step", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridQCNList, 0, "리턴여부", "리턴여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "FirResultFlag", "1차결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "SecResultFlag", "2차결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ThiResultFlag", "3차결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "AriseEquipName", "발생설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "MESReleaseTFlag", "MESReleaseTFlag", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "MESTrackOutTFlag", "MESTrackOutTFlag", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "QCNConfirmResultFlag", "최종결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "QCConfirmFlag", "최종Confirm", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "QCNCancelFlag", "취소여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80,false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ImputationDeptName", "귀책부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly,100, false, false, 30
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "FirCauseDesc", "원인 및 검토", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "FirCorrectDesc", "시정조치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "FirMeasureDesc", "대책사항", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "SecActionDesc", "기생산 자재조치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ThiFCostQty", "F-Cost 수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ThiFCostTime", "F-Cost 시간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ThiFCostPerson", "F-Cost 인원", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "ThiFCostScrap", "F-Cost Scrap", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "InspectQty", "InspectQty", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridQCNList, 0, "FaultQty", "FaultQty", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region AffectLot 그리드

                // Affect Lot 정보 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridAffectLotList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "LotType", "Lot구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "Qty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "WorkDateTime", "TrackIn일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", DateTime.Now.ToString("yyyy-MM-dd HH:mm:tt"));

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "WorkUserID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAffectLotList, 0, "WorkUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region FaultType

                // 설비 그리드
                wGrid.mfInitGeneralGrid(this.uGridFaulty, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "PlantCode", "선택", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 30, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "QCNNo", "발행번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 30, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "InspectFaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaulty, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 설비 List

                // 설비 그리드
                wGrid.mfInitGeneralGrid(this.uGridExpectEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "CheckFlag", "선택", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "EQPID", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "USERID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "USERNAME", "작업자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridExpectEquipList, 0, "EtcDes", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                this.uGridExpectEquipList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                #endregion

                // Affect Lot정보 Lot구분 콤보 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtCom = clsComCode.mfReadCommonCode("C0044", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridAffectLotList, 0, "LotType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                // SetFontSize
                this.uGridQCNList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridQCNList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridAffectLotList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridAffectLotList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridExpectEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridExpectEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridQCNList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridQCNList_DoubleClickRow);
                
                Size size = new Size(1070, 90);
                this.uGridQCNList.MinimumSize = size;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 검색조건 변수 설정
                string strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                string strProductCode = this.uTextSearchProductCode.Text;
                string strProcessCode = this.uComboSearchProcessCode.Value.ToString();
                string strQCNNo = this.uTextSearchQCNNo.Text;
                string strLotNo = this.uTextSearchLotNo.Text;
                string strQCConfirmFlag = "";
                string strAriseDateFrom = Convert.ToDateTime(this.uDateSearchAriseDateFrom.Value).ToString("yyyy-MM-dd");
                string strAriseDateTo = Convert.ToDateTime(this.uDateSerachAriseDateTo.Value).ToString("yyyy-MM-dd");
                if (this.uCheckConfirm.Checked == true)
                {
                    strQCConfirmFlag = "T";
                }
                else
                {
                    strQCConfirmFlag = "F";
                }

                string strP_QNCITR = this.uComboSearchP_QCNITR.Value == null ? "" : this.uComboSearchP_QCNITR.Value.ToString();


                // BL연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQCN);

                WinMessageBox msg = new WinMessageBox();
                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 메소드 호출
                DataTable dtSearch = clsQCN.mfReadInsProcQCN_List_S(strPlantCode, strProcessCode, strQCNNo, strP_QNCITR, strLotNo, strProductCode, strQCConfirmFlag, strAriseDateFrom, strAriseDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGridQCNList.DataSource = dtSearch;
                this.uGridQCNList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                {
                    
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridQCNList, 0);
                }
                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001041", Infragistics.Win.HAlign.Center);
                    return;
                }
                else if (PlantCode == null)
                {
                    return;
                }
                else if (PlantCode.Equals(string.Empty) || this.uTextQCNNo.Text.Equals(string.Empty))
                {
                    return;
                }
                else
                {
                    //if (dtSave.Rows[0]["QCConfirmComplete"].ToString().Equals("T") && dtSave.Rows[0]["MESReleaseTFlag"].ToString().Equals("T"))
                    ////if (dtSave.Rows[0]["QCConfirmComplete"].ToString().Equals("T") && this.uTextMESReleaseTFlag.Text.Equals("T"))
                    ////{
                    ////    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                "확인창", "처리여부 확인", "QC 최종 Confirm 완료,MES전송이 완료된 데이터는 수정할수 없습니다.", Infragistics.Win.HAlign.Right);
                    ////    return;
                    ////}

                    if (this.uTextQCConfirmComplete.Text.ToUpper().Equals("T"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001137", "M001404", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (this.uCheckQCConfirmFlag.Checked == true && this.uTextQCConfirmUserName.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000437", "M001155",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (this.uCheckQCConfirmFlag.Checked == true && this.uOptionSetConfirmResultFlag.CheckedIndex.Equals(-1))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000437", "M001157",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (this.uCheckQCConfirmFlag.Checked == true && this.uOptionSetConfirmResultFlag.CheckedIndex.Equals(0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000437", "M001159",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (this.uCheckQCConfirmFlag.Enabled == true && this.uCheckQCConfirmFlag.Checked == true)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000437", "M000104",
                                    Infragistics.Win.HAlign.Right);
                    }

                    //DialogResult dirResult = new DialogResult();

                    //if (dtSave.Rows[0]["QCConfirmComplete"].ToString().Equals("T"))// && dtSave.Rows[0]["MESReleaseTFlag"].ToString().Equals("F"))
                    //{
                    //    dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                "확인창", "저장확인", "QC 최종 Confirm이 완료되어 수정이 불가합니다. MES처리를 하시겠습니까?", Infragistics.Win.HAlign.Right);
                    //}
                    //else
                    //{
                    //    dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                "확인창", "저장확인", "입력한 정보를 저장하겠습니까?", Infragistics.Win.HAlign.Right);
                    //}

                    //// 저장여부를 묻는다
                    //if (dirResult == DialogResult.Yes)
                    //{
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 헤더정보 데이터테이블에 저장하는 메소드 호출
                        DataTable dtSave = Rtn_SaveData();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                        QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                        brwChannel.mfCredentials(clsQCN);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", m_resSys.GetString("SYS_LANG")));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsQCN.mfSaveINSProcQCN_List_PSTS(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));  //, this.Name, dtAffectLotNo);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            FileUpload_Header(uTextQCNNo.Text);
                            // TrackOut 메소드 호출
                            MES_TrackOut();

                            //// 발행완료가 체크되어있고, MESHoldTFlag가 F 일경우 MES처리 정보
                            //if (dtSave.Rows[0]["QCConfirmFlag"].ToString().Equals("True") && dtSave.Rows[0]["MESReleaseTFlag"].ToString().Equals("F"))
                            //{
                            //    //MES 처리정보가 실패면
                            //    if (ErrRtn.InterfaceResultCode != "0")
                            //    {
                            //        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            //        {
                            //            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);
                            //        }
                            //        else
                            //        {
                            //            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                       "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                            //        }
                            //    }
                            //    else
                            //    {
                            //        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                      "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장하고, MES Lot Release정보를 전송하였습니다.", Infragistics.Win.HAlign.Right);

                            //        // 공정검사에서 넘어온 정보가 TrackOut 처리 필요한지 판단하여 처리하는 메소드 호출
                            //        MES_TrackOut();
                            //    }
                            //}
                            //else
                            //{
                            //Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.", Infragistics.Win.HAlign.Right);

                            //}
                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region MES TrackOut 처리
        /// <summary>
        /// 공정검사에서 넘어온경우 TrackIn 공정이면 TrackOut MES I/F 하는 메소드
        /// </summary>
        private void MES_TrackOut()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                bool bolSaveCheck = true;

                // 공정검사에서 넘어온 정보일때
                if (!this.uTextReqNo.Text.Equals(string.Empty) && !this.uTextReqSeq.Text.Equals(string.Empty) &&
                    this.uTextMESTrackOutTFlag.Text.Equals("F") && this.uTextMESReleaseTFlag.Text.Equals("T"))
                {
                    string strErrRtn = string.Empty;
                    TransErrRtn ErrRtn = new TransErrRtn();

                    // DB정보 조회하여 TrackIn 한 정보인지 판단
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                    brwChannel.mfCredentials(clsLot);

                    DataTable dtLot = clsLot.mfReadINSProcInspectReqLot(PlantCode, this.uTextReqNo.Text, this.uTextReqSeq.Text, m_resSys.GetString("SYS_LANG"));

                    // TrackIn 처리한 자료이면
                    if (dtLot.Rows[0]["MESTrackInTFlag"].ToString().Equals("T") || dtLot.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                        brwChannel.mfCredentials(clsEquip);

                        DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(PlantCode, this.uTextAriseEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                        if (dtEquip.Rows.Count > 0)
                        {
                            // Sampling 갯수(외관검사 검사수량)
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                            QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                            brwChannel.mfCredentials(clsItem);

                            DataTable dtItem = clsItem.mfReadINSProcInspectReqItem(PlantCode, this.uTextReqNo.Text, this.uTextReqSeq.Text, m_resSys.GetString("SYS_LANG"));

                            // DataTable 형태 복제
                            DataTable dtInspectItemList = new DataTable();
                            dtInspectItemList.Columns.Add("INSPECTID", typeof(string));
                            dtInspectItemList.Columns.Add("SAMPLECOUNT", typeof(string));
                            dtInspectItemList.Columns.Add("QUALEFLAG", typeof(string));
                            DataRow drRow;

                            for (int i = 0; i < dtItem.Rows.Count; i++)
                            {
                                // 고객사가 삼성인경우 그리드에 검사항목 II02031902, II02031903, II02010010, II02010009 가 있으면 같이 보낸다
                                if (this.uTextCustomerCode.Text.Equals("SEC"))
                                {
                                    if (dtItem.Rows[i]["InspectItemCode"].Equals("II02010001"))
                                    {
                                        drRow = dtInspectItemList.NewRow();
                                        drRow["INSPECTID"] = "PI_VISUAL";
                                        drRow["SAMPLECOUNT"] = dtItem.Rows[i]["ProcessSampleSize"].ToString();
                                        drRow["QUALEFLAG"] = "OK";
                                        dtInspectItemList.Rows.Add(drRow);
                                    }
                                    else if (dtItem.Rows[i]["InspectItemCode"].Equals("II02031902"))
                                    {
                                        drRow = dtInspectItemList.NewRow();
                                        drRow["INSPECTID"] = "AE";
                                        drRow["SAMPLECOUNT"] = dtItem.Rows[i]["ProcessSampleSize"].ToString();
                                        drRow["QUALEFLAG"] = "OK";
                                        dtInspectItemList.Rows.Add(drRow);
                                    }
                                    else if (dtItem.Rows[i]["InspectItemCode"].Equals("II02031903"))
                                    {
                                        drRow = dtInspectItemList.NewRow();
                                        drRow["INSPECTID"] = "AO";
                                        drRow["SAMPLECOUNT"] = dtItem.Rows[i]["ProcessSampleSize"].ToString();
                                        drRow["QUALEFLAG"] = "OK";
                                        dtInspectItemList.Rows.Add(drRow);
                                    }
                                    else if (dtItem.Rows[i]["InspectItemCode"].Equals("II02010010"))
                                    {
                                        drRow = dtInspectItemList.NewRow();
                                        drRow["INSPECTID"] = "AV";
                                        drRow["SAMPLECOUNT"] = dtItem.Rows[i]["ProcessSampleSize"].ToString();
                                        drRow["QUALEFLAG"] = "OK";
                                        dtInspectItemList.Rows.Add(drRow);
                                    }
                                    else if (dtItem.Rows[i]["InspectItemCode"].Equals("II02010009"))
                                    {
                                        drRow = dtInspectItemList.NewRow();
                                        drRow["INSPECTID"] = "AP";
                                        drRow["SAMPLECOUNT"] = dtItem.Rows[i]["ProcessSampleSize"].ToString();
                                        drRow["QUALEFLAG"] = "OK";
                                        dtInspectItemList.Rows.Add(drRow);
                                    }
                                }
                                else
                                {
                                    drRow = dtInspectItemList.NewRow();
                                    drRow["INSPECTID"] = "PI_VISUAL";
                                    drRow["SAMPLECOUNT"] = dtItem.Rows[i]["ProcessSampleSize"].ToString();
                                    drRow["QUALEFLAG"] = "OK";
                                    dtInspectItemList.Rows.Add(drRow);
                                }
                            }

                            // 데이터 테이블 설정
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                            QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                            brwChannel.mfCredentials(clsQCN);

                            DataTable dtStdInfo = clsQCN.mfSetDataInfo_MESTrackOUT();
                            drRow = dtStdInfo.NewRow();
                            drRow["QCNNo"] = this.uTextQCNNo.Text;
                            drRow["FormName"] = this.Name;
                            drRow["FACTORYID"] = PlantCode;                                    // 공장코드
                            drRow["USERID"] = m_resSys.GetString("SYS_USERID");               // 사용자ID
                            drRow["LOTID"] = this.uTextLotNo.Text.Trim();                    // Lot 번호
                            drRow["EQPID"] = this.uTextAriseEquipCode.Text;                                      // 설비코드
                            drRow["RECIPEID"] = "-";                                            // RecipeID 사용하지 않으면'-'
                            drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();         // AreaID
                            drRow["SPLITFLAG"] = "N";                                            // SPLIT 여부 N 고정
                            drRow["SCRAPFLAG"] = "N";                                            // SCRAP 여부
                            drRow["REPAIRFLAG"] = "N";                                            // REPAIR 여부
                            drRow["RWFLAG"] = "N";                                            // REWORK 여부
                            drRow["CONSUMEFLAG"] = "N";                                            // 자재소모여부 'N' 고정
                            drRow["COMMENT"] = this.uTextEtcDesc.Text;                                             // Comment
                            drRow["QCFLAG"] = "Y";                                            // QCFlag 'Y' 고정
                            dtStdInfo.Rows.Add(drRow);

                            // Scrap 데이터 테이블(SCRAPCODE, SCRAPQTY)
                            DataTable dtScrapList = new DataTable();

                            // 해당공정 불량정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                            QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                            brwChannel.mfCredentials(clsFault);

                            DataTable dtFaultList = clsFault.mfReadINSProcInspectResultFault_MESTrackOut(PlantCode, this.uTextReqNo.Text, this.uTextReqSeq.Text, 1, "II02010001");

                            // TrackOUT 처리 메소드 호출 
                            strErrRtn = clsQCN.mfSaveINSProcQCN_MESTrackOUT(dtStdInfo, dtScrapList, dtInspectItemList, dtFaultList, "Y", m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                            {
                                bolSaveCheck = false;
                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "M001135", "M000095"
                                                                , "M000950"
                                                                , Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                                                , ErrRtn.InterfaceResultMessage
                                                                , Infragistics.Win.HAlign.Right);
                                }
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001135", "M000095"
                                                                , "M000937"
                                                                , Infragistics.Win.HAlign.Right);

                                this.uTextMESTrackOutTFlag.Text = "T";
                            }
                        }
                        else
                        {
                            bolSaveCheck = false;
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000717", "M000090",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                }

                if (bolSaveCheck)
                {
                    ////Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                            "M001135", "M001037", "M000930", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        ////////#region 기존TrackOut 소스 주석처리
        /////////// <summary>
        /////////// 공정검사에서 넘어온경우 TrackIn 공정이면 TrackOut MES I/F 하는 메소드
        /////////// </summary>
        ////////private void MES_TrackOut()
        ////////{
        ////////    try
        ////////    {
        ////////        // 공정검사에서 넘어온 정보일때
        ////////        if (!this.uTextReqNo.Text.Equals(string.Empty) && !this.uTextReqSeq.Text.Equals(string.Empty))
        ////////        {
        ////////            // SystemInfo ResourceSet
        ////////            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        ////////            string strErrRtn = string.Empty;
        ////////            TransErrRtn ErrRtn = new TransErrRtn();
        ////////            DialogResult Result = new DialogResult();
        ////////            WinMessageBox msg = new WinMessageBox();

        ////////            // DB정보 조회하여 TrackIn 한 정보인지 판단
        ////////            QRPBrowser brwChannel = new QRPBrowser();
        ////////            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
        ////////            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
        ////////            brwChannel.mfCredentials(clsLot);

        ////////            DataTable dtLot = clsLot.mfReadINSProcInspectReqLot(PlantCode, this.uTextReqNo.Text, this.uTextReqSeq.Text, m_resSys.GetString("SYS_LANG"));

        ////////            // TrackIn 처리한 자료이면
        ////////            if (dtLot.Rows[0]["MESTrackInTFlag"].ToString().Equals("T") || dtLot.Rows[0]["TrackInFlag"].ToString().Equals("T"))
        ////////            {
        ////////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        ////////                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
        ////////                brwChannel.mfCredentials(clsEquip);

        ////////                DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(PlantCode, this.uTextAriseEquipCode.Text, m_resSys.GetString("SYS_LANG"));

        ////////                if (dtEquip.Rows.Count > 0)
        ////////                {
        ////////                    // Sampling 갯수(외관검사 검사수량)
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
        ////////                    QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
        ////////                    brwChannel.mfCredentials(clsItem);

        ////////                    DataTable dtItem = clsItem.mfReadINSProcInspectReqItem(PlantCode, this.uTextReqNo.Text, this.uTextReqSeq.Text, m_resSys.GetString("SYS_LANG"));

        ////////                    int intSampleCount = 0;
        ////////                    for (int i = 0; i < dtItem.Rows.Count; i++)
        ////////                    {
        ////////                        if (dtItem.Rows[i]["InspectItemCode"].ToString().Equals("II02010001"))
        ////////                        {
        ////////                            intSampleCount = Convert.ToInt32(dtItem.Rows[i]["ProcessSampleSize"]);
        ////////                            break;
        ////////                        }
        ////////                    }

        ////////                    // 데이터 테이블 설정
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
        ////////                    QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
        ////////                    brwChannel.mfCredentials(clsQCN);

        ////////                    DataTable dtStdInfo = clsQCN.mfSetDataInfo_MESTrackOUT();
        ////////                    DataRow drRow = dtStdInfo.NewRow();
        ////////                    drRow["QCNNo"] = this.uTextQCNNo.Text;
        ////////                    drRow["FormName"] = this.Name;
        ////////                    drRow["FACTORYID"] = PlantCode;                                    // 공장코드
        ////////                    drRow["USERID"] = m_resSys.GetString("SYS_USERID");               // 사용자ID
        ////////                    drRow["LOTID"] = this.uTextLotNo.Text.Trim();                    // Lot 번호
        ////////                    drRow["EQPID"] = this.uTextAriseEquipCode.Text;                                      // 설비코드
        ////////                    drRow["RECIPEID"] = "-";                                            // RecipeID 사용하지 않으면'-'
        ////////                    drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();         // AreaID
        ////////                    drRow["SPLITFLAG"] = "N";                                            // SPLIT 여부 N 고정
        ////////                    drRow["SCRAPFLAG"] = "N";                                            // SCRAP 여부
        ////////                    drRow["REPAIRFLAG"] = "N";                                            // REPAIR 여부
        ////////                    drRow["RWFLAG"] = "N";                                            // REWORK 여부
        ////////                    drRow["CONSUMEFLAG"] = "N";                                            // 자재소모여부 'N' 고정
        ////////                    drRow["COMMENT"] = "";                                             // Comment
        ////////                    drRow["QCFLAG"] = "Y";                                            // QCFlag 'Y' 고정
        ////////                    drRow["SAMPLECOUNT"] = intSampleCount.ToString();                       // Sampling 갯수
        ////////                    drRow["QUALEFLAG"] = "OK";                                            // 판정결과
        ////////                    //if (this.uOptionSetConfirmResultFlag.CheckedIndex.Equals(1))
        ////////                    //    drRow["QUALEFLAG"] = "OK";                                            // 판정결과
        ////////                    //else if(this.uOptionSetConfirmResultFlag.CheckedIndex.Equals(0))
        ////////                    //    drRow["QUALEFLAG"] = "NG";
        ////////                    ////// DataTable 1:n
        ////////                    //////drRow["REASONQTY"] = "";                                             // 불량개수
        ////////                    //////drRow["REASONCODE"] = "";                                             // 불량코드
        ////////                    //////drRow["CAUSEEQPID"] = "";                                             // 불량원인설비
        ////////                    dtStdInfo.Rows.Add(drRow);

        ////////                    // Scrap 데이터 테이블(SCRAPCODE, SCRAPQTY)
        ////////                    DataTable dtScrapList = new DataTable();

        ////////                    // 해당공정 불량정보 가져오기
        ////////                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
        ////////                    QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
        ////////                    brwChannel.mfCredentials(clsFault);

        ////////                    DataTable dtFaultList = clsFault.mfReadINSProcInspectResultFault_MESTrackOut(PlantCode, this.uTextReqNo.Text, this.uTextReqSeq.Text, 1, "II02010001");

        ////////                    // TrackOUT 처리 메소드 호출
        ////////                    strErrRtn = clsQCN.mfSaveINSProcQCN_MESTrackOUT(dtStdInfo, dtScrapList, dtFaultList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

        ////////                    // 결과검사
        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
        ////////                    {
        ////////                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        ////////                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                                        "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES TrackOut요청을 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요."
        ////////                                                        , Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                        else
        ////////                        {
        ////////                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        ////////                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////////                                                        , "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage
        ////////                                                        , Infragistics.Win.HAlign.Right);
        ////////                        }
        ////////                    }
        ////////                    else
        ////////                    {
        ////////                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
        ////////                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////////                                                        , "처리결과", "MES처리결과"
        ////////                                                        , "입력한 정보를 저장하고 MES TrackOut 요청했습니다."
        ////////                                                        , Infragistics.Win.HAlign.Right);
        ////////                    }
        ////////                }
        ////////                else
        ////////                {
        ////////                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////////                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////////                                                "확인창", "설비정보조회 결과", "MES I/F에 필요한 설비정보를 조회할 수 없습니다",
        ////////                                                Infragistics.Win.HAlign.Right);
        ////////                }
        ////////            }
        ////////        }
        ////////    }
        ////////    catch (Exception ex)
        ////////    {
        ////////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////////        frmErr.ShowDialog();
        ////////    }
        ////////    finally
        ////////    {
        ////////    }
        ////////}
        ////////#endregion

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextQCNNo.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000882", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uCheckQCConfirmFlag.Checked && !this.uCheckQCConfirmFlag.Enabled)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001156", "M001154", Infragistics.Win.HAlign.Center);

                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                        QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                        brwChannel.mfCredentials(clsQCN);

                        // 매개변수로 전달할 변수 설정
                        string strQCNNo = this.uTextQCNNo.Text;

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", m_resSys.GetString("SYS_LANG")));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsQCN.mfDeleteINSProcQCN_PSTS(PlantCode, strQCNNo);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000923",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridQCNList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridQCNList);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000803", "M000809", "M000804",
                                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 메소드..
        /// <summary>
        /// 체크박스 Enable 상태로 만들고 색지정하는 메소드
        /// </summary>
        /// <param name="c"></param>
        private void SetCheckBoxEnable(Control c)
        {
            try
            {
                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Enabled = false;
                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Appearance.BackColorDisabled = Color.White;
                ((Infragistics.Win.UltraWinEditors.UltraCheckEditor)c).Appearance.ForeColorDisabled = Color.Black;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
            }
        }

        // 리스트 선택시 귀책부서 
        private void SetImputeDeptCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                DataTable dtDept = clsDept.mfReadSYSDeptForCombo(PlantCode, m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCombo = new WinComboEditor();

                wCombo.mfSetComboEditor(this.uComboImputeDept, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ""
                    , "DeptCode", "DeptName", dtDept);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 상단부분 조회 Method
        /// </summary>
        /// <param name="strQCNNo">Qcn번호</param>
        /// <param name="strLang">언어</param>
        private void Search_HeaderTopData(string strQCNNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQCN);

                DataTable dtRtn = clsQCN.mfReadINSProcQCNDetail_List_PSTS(PlantCode, strQCNNo, strLang);

                if (dtRtn.Rows.Count == 0)
                    return;

                this.uTextPlantName.Text = dtRtn.Rows[0]["PlantName"].ToString();
                this.uTextQCNNo.Text = dtRtn.Rows[0]["QCNNo"].ToString();
                this.uTextP_QCNITR.Text = dtRtn.Rows[0]["P_QcnItrType"].ToString();
                this.uTextLotNo.Text = dtRtn.Rows[0]["LotNo"].ToString();
                this.uTextCustomerCode.Text = dtRtn.Rows[0]["CustomerCode"].ToString();
                this.uTextCustomerName.Text = dtRtn.Rows[0]["CustomerName"].ToString();
                this.uTextProductCode.Text = dtRtn.Rows[0]["ProductCode"].ToString();
                this.uTextProductName.Text = dtRtn.Rows[0]["ProductName"].ToString();
                this.uTextCustomerProductSpec.Text = dtRtn.Rows[0]["CustomerProductSpec"].ToString();
                this.uTextAriseProcessName.Text = dtRtn.Rows[0]["AriseProcessName"].ToString();
                this.uTextAriseEquipName.Text = dtRtn.Rows[0]["AriseEquipName"].ToString();
                this.uTextAriseDate.Text = dtRtn.Rows[0]["AriseDate"].ToString();
                this.uTextAriseTime.Text = dtRtn.Rows[0]["AriseTime"].ToString();
                this.uTextExpectProcessName.Text = dtRtn.Rows[0]["ExpectProcessName"].ToString();
                this.uTextQty.Text = Math.Floor(Convert.ToDecimal(dtRtn.Rows[0]["Qty"])).ToString();
                this.uTextInspectQty.Text = Math.Floor(Convert.ToDecimal(dtRtn.Rows[0]["InspectQty"])).ToString();
                this.uTextFaultQty.Text = Math.Floor(Convert.ToDecimal(dtRtn.Rows[0]["FaultQty"])).ToString();
                this.uTextInspectFaultTypeName.Text = dtRtn.Rows[0]["InspectFaultTypeName"].ToString();
                this.uTextFaultImg.Text = dtRtn.Rows[0]["FaultFilePath"].ToString();
                this.uCheckWorkStepMaterialFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["WorkStepMaterialFlag"]);
                this.uCheckWorkStepEquipFlag.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["WorkStepEquipFlag"]);
                this.uCheckActionFlag1.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag1"]);
                this.uCheckActionFlag2.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag2"]);
                this.uCheckActionFlag3.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag3"]);
                this.uCheckActionFlag4.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag4"]);
                this.uCheckActionFlag5.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag5"]);
                this.uCheckActionFlag6.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag6"]);
                this.uCheckActionFlag7.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag7"]);
                this.uCheckActionFlag8.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag8"]);
                this.uCheckActionFlag9.CheckedValue = Convert.ToBoolean(dtRtn.Rows[0]["ActionFlag9"]);

                this.uTextAriseProcessCode.Text = dtRtn.Rows[0]["AriseProcessCode"].ToString();
                this.uTextAriseEquipCode.Text = dtRtn.Rows[0]["AriseEquipCode"].ToString();
                this.uTextExpectProcessCode.Text = dtRtn.Rows[0]["ExpectProcessCode"].ToString();

                this.uTextReqNo.Text = dtRtn.Rows[0]["ReqNo"].ToString();
                this.uTextReqSeq.Text = dtRtn.Rows[0]["ReqSeq"].ToString();

                this.uTextInspectUserName.Text = dtRtn.Rows[0]["InspectUserName"].ToString();

                this.uTextRetestFaultQty1.Text = dtRtn.Rows[0]["RetestFaultQty1"].ToString();
                this.uTextRetestFaultQty2.Text = dtRtn.Rows[0]["RetestFaultQty2"].ToString();
                this.uTextRetestFaultQty3.Text = dtRtn.Rows[0]["RetestFaultQty3"].ToString();
                this.uTextRetestInspectQty1.Text = dtRtn.Rows[0]["RetestInspectQty1"].ToString();
                this.uTextRetestInspectQty2.Text = dtRtn.Rows[0]["RetestInspectQty2"].ToString();
                this.uTextRetestInspectQty3.Text = dtRtn.Rows[0]["RetestInspectQty3"].ToString();
                this.uComboSYSTEM.Value = dtRtn.Rows[0]["TestSYSTEM"].ToString();
                this.uComboHandler.Value = dtRtn.Rows[0]["TestHANDLER"].ToString();
                this.uTextProgram.Text = dtRtn.Rows[0]["TestPROGRAM"].ToString();

                if (this.uTextP_QCNITR.Text.Equals("ITR"))
                {
                    this.uLabelQAENGR.Visible = true;
                    this.uLabelMeasure.Visible = false;
                }
                else
                {
                    this.uLabelQAENGR.Visible = false;
                    this.uLabelMeasure.Visible = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// AffectLot 정보 조회 메소드
        /// </summary>
        /// <param name="strQCNNo">QCNNo</param>
        /// <param name="strLang">언어</param>
        private void Search_AffectLotData(string strQCNNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNAffectLot), "ProcQCNAffectLot");
                QRPINS.BL.INSPRC.ProcQCNAffectLot clsALot = new QRPINS.BL.INSPRC.ProcQCNAffectLot();
                brwChannel.mfCredentials(clsALot);

                DataTable dtRtn = clsALot.mfReadINSProcQCNAffectLot(PlantCode, strQCNNo, strLang);

                this.uGridAffectLotList.DataSource = dtRtn;
                this.uGridAffectLotList.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridAffectLotList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형정보 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQCNNo"></param>
        /// <param name="strLang"></param>
        private void Search_FaultType(string strPlantCode, string strQCNNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNFaultType), "ProcQCNFaultType");
                QRPINS.BL.INSPRC.ProcQCNFaultType clsQcn = new QRPINS.BL.INSPRC.ProcQCNFaultType();
                brwChannel.mfCredentials(clsQcn);

                DataTable dtFaulty = clsQcn.mfReadProcQCNFaultType(strPlantCode, strQCNNo, strLang);

                this.uGridFaulty.DataSource = dtFaulty;
                this.uGridFaulty.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ExpectEquip List 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN 관리번호</param>
        /// <param name="strLang">언어</param>
        private void Search_ExpectEquipList(string strPlantCode, string strQCNNo, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNExpectEquip), "ProcQCNExpectEquip");
                QRPINS.BL.INSPRC.ProcQCNExpectEquip clsExEquip = new QRPINS.BL.INSPRC.ProcQCNExpectEquip();
                brwChannel.mfCredentials(clsExEquip);

                DataTable dtExEquipList = clsExEquip.mfReadINSProcQCNExpectEquip(strPlantCode, strQCNNo, strLang);

                this.uGridExpectEquipList.DataSource = dtExEquipList;
                this.uGridExpectEquipList.DataBind();

                if (dtExEquipList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridExpectEquipList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 하단부분 조회하는 메소드
        /// </summary>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">언어</param>
        private void Search_HeaderBottomData(string strQCNNo, string strLang)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQCN);

                //DataTable dtHBottom = clsQCN.mfReadINSProcQCNDetail_ListBottom(PlantCode, strQCNNo, strLang);
                DataTable dtHBottom = clsQCN.mfReadINSProcQCNDetail_ListBottom_PSTS(PlantCode, strQCNNo, strLang);

                if (dtHBottom.Rows.Count > 0)
                {
                    this.uTextEtcDesc.Text = dtHBottom.Rows[0]["EtcDesc"].ToString();
                    // 1차 확인
                    this.uTextFirCauseDesc.Text = dtHBottom.Rows[0]["FirCauseDesc"].ToString();
                    this.uTextFirCauseUserID.Text = dtHBottom.Rows[0]["FirCauseUserID"].ToString();
                    this.uTextFirCauseUserName.Text = dtHBottom.Rows[0]["FirCauseUserName"].ToString();
                    this.uTextFirCauseDeptName.Text = dtHBottom.Rows[0]["FirCauseDeptName"].ToString();
                    if (dtHBottom.Rows[0]["FirCauseDate"].ToString() != "")
                    {
                        this.uDateFirCauseDate.Value = Convert.ToDateTime(dtHBottom.Rows[0]["FirCauseDate"]).ToString("yyyy-MM-dd");
                        //String strFirCauseDatetime = dtHBottom.Rows[0]["FirCauseDate"].ToString();
                        //String[] splitFirCaouseDatetime = strFirCauseDatetime.Split(new char[] { ' ' });
                        //this.uDateFirCauseDate.Value = Convert.ToDateTime(splitFirCaouseDatetime[0]).ToString("yyyy-MM-dd");
                        //this.uDateFirCauseTime.Value = Convert.ToDateTime(splitFirCaouseDatetime[2]).ToString();
                    }
                                            
                    this.uCheckFirCauseCompleteFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["FirCauseCompleteFlag"]);

                    //완료 체크에 따라 편집 가능, 불가 설정
                    //if (this.uCheckFirCauseCompleteFlag.Checked == true)
                    //{
                    //    this.uTextFirCauseDesc.ReadOnly = true;
                    //    this.uTextFirCauseDesc.Appearance.BackColor = Color.Gainsboro;
                    //    this.uTextFirCauseUserID.ReadOnly = true;
                    //    this.uTextFirCauseUserID.Appearance.BackColor = Color.Gainsboro;
                    //    this.uTextFirCauseUserName.Appearance.BackColor = Color.Gainsboro;
                    //    this.uTextFirCauseDeptName.Appearance.BackColor = Color.Gainsboro;
                    //    this.uDateFirCauseDate.ReadOnly = true;
                    //    this.uDateFirCauseDate.Appearance.BackColor = Color.Gainsboro;
                    //    this.uCheckFirCauseCompleteFlag.Enabled = false;
                    //}
                    //else
                    //{
                    //    this.uTextFirCauseDesc.ReadOnly = false;
                    //    this.uTextFirCauseDesc.Appearance.BackColor = Color.White;
                    //    this.uTextFirCauseUserID.ReadOnly = false;
                    //    this.uTextFirCauseUserID.Appearance.BackColor = Color.White;
                    //    this.uTextFirCauseUserName.Appearance.BackColor = Color.PowderBlue;
                    //    this.uTextFirCauseDeptName.Appearance.BackColor = Color.PowderBlue;
                    //    this.uDateFirCauseDate.ReadOnly = false;
                    //    this.uDateFirCauseDate.Appearance.BackColor = Color.White;
                    //    this.uCheckFirCauseCompleteFlag.Enabled = true;
                    //}
                    
                    this.uTextFirCorrectDesc.Text = dtHBottom.Rows[0]["FirCorrectDesc"].ToString();
                    this.uTextFirCorrectUserID.Text = dtHBottom.Rows[0]["FirCorrectUserID"].ToString();
                    this.uTextFirCorrectUserName.Text = dtHBottom.Rows[0]["FirCorrectUserName"].ToString();
                    this.uTextFirCorrectDeptName.Text = dtHBottom.Rows[0]["FirCorrectDeptName"].ToString();
                    if (dtHBottom.Rows[0]["FirCorrectDate"].ToString() != "")
                    {
                        this.uDateFirCorrectDate.Value = Convert.ToDateTime(dtHBottom.Rows[0]["FirCorrectDate"]).ToString("yyyy-MM-dd");
                        //String strFirCorrectDate = dtHBottom.Rows[0]["FirCorrectDate"].ToString();
                        //String[] splitFirCorrectDate = strFirCorrectDate.Split(new char[] { ' ' });
                        //this.uDateFirCorrectDate.Value = Convert.ToDateTime(splitFirCorrectDate[0]).ToString("yyyy-MM-dd");
                        //this.uDateFirCorrectTime.Value = Convert.ToDateTime(splitFirCorrectDate[2]).ToString();
                    }
                    this.uCheckFirCorrectCompleteFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["FirCorrectCompleteFlag"]);

                    //if (this.uCheckFirCorrectCompleteFlag.Checked == true)
                    //{
                    //    this.uTextFirCorrectDesc.ReadOnly = true;
                    //    this.uTextFirCorrectDesc.Appearance.BackColor = Color.Gainsboro;
                    //    this.uTextFirCorrectUserID.ReadOnly = true;
                    //    this.uTextFirCorrectUserID.Appearance.BackColor = Color.Gainsboro;
                    //    this.uDateFirCorrectDate.ReadOnly = true;
                    //    this.uDateFirCorrectDate.Appearance.BackColor = Color.Gainsboro;
                    //    this.uTextFirCorrectUserName.Appearance.BackColor = Color.Gainsboro;
                    //    this.uTextFirCorrectDeptName.Appearance.BackColor = Color.Gainsboro;
                    //    this.uCheckFirCorrectCompleteFlag.Enabled = false;
                    //}
                    //else
                    //{
                    //    this.uTextFirCorrectDesc.ReadOnly = false;
                    //    this.uTextFirCorrectDesc.Appearance.BackColor = Color.White;
                    //    this.uTextFirCorrectUserID.ReadOnly = false;
                    //    this.uTextFirCorrectUserID.Appearance.BackColor = Color.White;
                    //    this.uDateFirCorrectDate.ReadOnly = false;
                    //    this.uDateFirCorrectDate.Appearance.BackColor = Color.White;
                    //    this.uTextFirCorrectUserName.Appearance.BackColor = Color.PowderBlue;
                    //    this.uTextFirCorrectDeptName.Appearance.BackColor = Color.PowderBlue;
                    //    this.uCheckFirCorrectCompleteFlag.Enabled = true;
                    //}

                    this.uTextFirMeasureDesc.Text = dtHBottom.Rows[0]["FirMeasureDesc"].ToString();
                    this.uTextFirMeasureUserID.Text = dtHBottom.Rows[0]["FirMeasureUserID"].ToString();
                    this.uTextFirMeasureUserName.Text = dtHBottom.Rows[0]["FirMeasureUserName"].ToString();
                    this.uTextFirMeasureDeptName.Text = dtHBottom.Rows[0]["FirMeasureDeptName"].ToString();
                    if (dtHBottom.Rows[0]["FirMeasureDate"].ToString() != "")
                    {
                        this.uDateFirMeasureDate.Value = Convert.ToDateTime(dtHBottom.Rows[0]["FirMeasureDate"]).ToString("yyyy-MM-dd");
                        //String strFirMeasureDate = dtHBottom.Rows[0]["FirMeasureDate"].ToString();
                        //String[] splitFirMeasureDate = strFirMeasureDate.Split(new char[] { ' ' });
                        //this.uDateFirMeasureDate.Value = Convert.ToDateTime(splitFirMeasureDate[0]).ToString("yyyy-MM-dd");
                        //this.uDateFirMeasureTime.Value = Convert.ToDateTime(splitFirMeasureDate[2]).ToString();
                    }
                    this.uCheckFirMeasureCompleteFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["FirMeasureCompleteFlag"]);

                    this.uCheckFir4MManFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["Fir4MManFlag"]);
                    this.uCheckFir4MEquipFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["Fir4MEquipFlag"]);
                    this.uCheckFir4MMethodFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["Fir4MMethodFlag"]);
                    this.uCheckFir4MEnviroFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["Fir4MEnviroFlag"]);
                    this.uCheckFir4MMaterialFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["Fir4MMaterialFlag"]);

                    if (dtHBottom.Rows[0]["FirResultFlag"].ToString() != "")
                        this.uOptionFirResultFlag.Value = dtHBottom.Rows[0]["FirResultFlag"].ToString();

                    // 2차 확인
                    this.uTextSecActionDesc.Text = dtHBottom.Rows[0]["SecActionDesc"].ToString();
                    this.uTextSecActionUserID.Text = dtHBottom.Rows[0]["SecActionUserID"].ToString();
                    this.uTextSecActionUserName.Text = dtHBottom.Rows[0]["SecActionUserName"].ToString();
                    this.uTextSecActionDeptName.Text = dtHBottom.Rows[0]["SecActionDeptName"].ToString();
                    if (dtHBottom.Rows[0]["SecActionDate"].ToString() != "")              
                        this.uDateSecActionDate.Value = Convert.ToDateTime(dtHBottom.Rows[0]["SecActionDate"]).ToString("yyyy-MM-dd");
                    this.uCheckSecActionCompleteFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["SecActionCompleteFlag"]);
                    if (dtHBottom.Rows[0]["SecResultFlag"].ToString() != "")
                        this.uOptionSecResultFlag.Value = dtHBottom.Rows[0]["SecResultFlag"].ToString();

                    // 3차 확인
                    this.uNumThiFCostQty.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostQty"]);
                    this.uNumThiFCostTime.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostTime"]);
                    this.uNumThiFCostPerson.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostPerson"]);
                    this.uNumThiFCostScrap.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostScrap"]);
                    //if (dtHBottom.Rows[0]["ThiFCostQty"].Equals(null))
                    //{
                    //    this.uNumThiFCostQty.Value = 0;
                    //}
                    //else
                    //{
                    //    this.uNumThiFCostQty.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostQty"]);
                    //}
                    
                    //if (dtHBottom.Rows[0]["ThiFCostTime"].Equals(null))
                    //{
                    //    this.uNumThiFCostTime.Value = 0;
                    //}
                    //else
                    //{
                    //    this.uNumThiFCostTime.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostTime"]);
                    //}
                   
                    //if (dtHBottom.Rows[0]["ThiFCostPerson"].Equals(null))
                    //{
                    //    this.uNumThiFCostPerson.Value = 0;
                    //}
                    //else
                    //{
                    //    this.uNumThiFCostPerson.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostPerson"]);
                    //}
                    
                    //if (dtHBottom.Rows[0]["ThiFCostScrap"].Equals(null))
                    //{
                    //    this.uNumThiFCostScrap.Value = 0;
                    //}
                    //else
                    //{
                    //    this.uNumThiFCostScrap.Value = Convert.ToDecimal(dtHBottom.Rows[0]["ThiFCostScrap"]);
                    //}
                    
                    if (dtHBottom.Rows[0]["ThiFCostCompleteFlag"].ToString() != "")           
                        this.uCheckThiFCostCompleteFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["ThiFCostCompleteFlag"]);
                    if (dtHBottom.Rows[0]["ThiResultFlag"].ToString() != "")
                        this.uOptionThiResultFlag.Value = dtHBottom.Rows[0]["ThiResultFlag"].ToString();

                    this.uComboImputeDept.Value = dtHBottom.Rows[0]["ImputationDeptCode"].ToString();
                    this.uTextQCConfirmUserID.Text = dtHBottom.Rows[0]["QCConfirmUserID"].ToString();
                    this.uTextQCConfirmUserName.Text = dtHBottom.Rows[0]["QCConfirmUserName"].ToString();
                    this.uCheckQCConfirmFlag.CheckedValue = Convert.ToBoolean(dtHBottom.Rows[0]["QCConfirmFlag"]);
                    if (dtHBottom.Rows[0]["QCConfirmResultFlag"].ToString() != "")
                    {
                        this.uOptionSetConfirmResultFlag.Value = dtHBottom.Rows[0]["QCConfirmResultFlag"].ToString();
                    }

                    this.uCheckQCNCancelFlag.Checked = Convert.ToBoolean(dtHBottom.Rows[0]["QCNCancelFlag"]);
                    this.uTextQCNCancelFlag.Text = dtHBottom.Rows[0]["QCNCancelFlag"].ToString().ToUpper().Substring(0, 1);

                    // QC 최종 Confirm이 체크되었을때 작업Step결과에 따라 자재재검 정비재검 버튼을 보여주기
                    // --> 최종Confrim에 상관없이 자재재검, 장비재검 버튼을 보여준다.
                    if (this.uCheckQCConfirmFlag.Checked)
                    {
                        SetCheckBoxEnable(this.uCheckQCConfirmFlag);

                        //최종 컨펌 날짜, 시간 가져오기
                        if (!dtHBottom.Rows[0]["QCConfirmDate"].ToString().Equals(string.Empty))
                        {
                            String strDate = string.Format("{0:yyyy-MM-dd}", dtHBottom.Rows[0]["QCConfirmDate"]);
                            this.uDateQCConfirmDate.Value = Convert.ToDateTime(strDate);

                            String strTime = string.Format("{0:HH:mm:ss}", dtHBottom.Rows[0]["QCConfirmDate"]);
                            this.uDateQCConfirmTime.Value = Convert.ToDateTime(strTime);
                        }
                        else
                        {
                            this.uDateQCConfirmDate.Value = null;
                            this.uDateQCConfirmTime.Value = null;
                        }

                    }
                    else
                    {
                        this.uDateQCConfirmDate.Value = null;
                        this.uDateQCConfirmTime.Value = null;
                    }

                    this.uTextFirReturnDesc.Text = dtHBottom.Rows[0]["FirReturnDesc"].ToString();
                    this.uTextSecReturnDesc.Text = dtHBottom.Rows[0]["SecReturnDesc"].ToString();
                    this.uTextThiReturnDesc.Text = dtHBottom.Rows[0]["ThiReturnDesc"].ToString();

                    this.uTextFile.Text = dtHBottom.Rows[0]["UploadFileName"].ToString();

                    ////if (m_resSys.GetString("SYS_DEPTCODE").Equals("1010"))
                    ////{
                    ////    this.uButtonEquip.Visible = this.uCheckWorkStepEquipFlag.Checked;
                    ////    this.uButtonMaterial.Visible = this.uCheckWorkStepMaterialFlag.Checked;
                    ////}
                    ////else
                    ////{
                    ////    this.uButtonEquip.Visible = false;
                    ////    this.uButtonMaterial.Visible = false;
                    ////}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
        /// <summary>
        /// 저장정보 데이터 테이블로 반환하는 메소드
        /// </summary>
        private DataTable Rtn_SaveData()
        {
            DataTable dtRtn = new DataTable();
            //시간을 현재 시간으로 변경
            this.uDateQCConfirmTime.Value = DateTime.Now;
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQCN);

                dtRtn = clsQCN.mfSetDataInfo_List();
                
                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = PlantCode;
                drRow["QCNNo"] = this.uTextQCNNo.Text;
                drRow["P_QCNITR"] = this.uTextP_QCNITR.Text;
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                drRow["FirCauseDesc"] = this.uTextFirCauseDesc.Text;
                drRow["FirCauseUserID"] = this.uTextFirCauseUserID.Text;
                drRow["FirCauseDate"] = Convert.ToDateTime(this.uDateFirCauseDate.Value).ToString("yyyy-MM-dd");
                drRow["FirCauseCompleteFlag"] = this.uCheckFirCauseCompleteFlag.CheckedValue;
                drRow["FirCorrectDesc"] = this.uTextFirCorrectDesc.Text;
                drRow["FirCorrectUserID"] = this.uTextFirCorrectUserID.Text;
                drRow["FirCorrectDate"] = Convert.ToDateTime(this.uDateFirCorrectDate.Value).ToString("yyyy-MM-dd");
                drRow["FirCorrectCompleteFlag"] = this.uCheckFirCorrectCompleteFlag.CheckedValue;
                drRow["FirMeasureDesc"] = this.uTextFirMeasureDesc.Text;
                drRow["FirMeasureUserID"] = this.uTextFirMeasureUserID.Text;
                drRow["FirMeasureDate"] = Convert.ToDateTime(this.uDateFirMeasureDate.Value).ToString("yyyy-MM-dd");
                drRow["FirMeasureCompleteFlag"] = this.uCheckFirMeasureCompleteFlag.CheckedValue;
                drRow["Fir4MManFlag"] = this.uCheckFir4MManFlag.CheckedValue;
                drRow["Fir4MEquipFlag"] = this.uCheckFir4MEquipFlag.CheckedValue;
                drRow["Fir4MMethodFlag"] = this.uCheckFir4MMethodFlag.CheckedValue;
                drRow["Fir4MEnviroFlag"] = this.uCheckFir4MEnviroFlag.CheckedValue;
                drRow["Fir4MMaterialFlag"] = this.uCheckFir4MMaterialFlag.CheckedValue;
                if(this.uOptionFirResultFlag.CheckedIndex != -1)
                    drRow["FirResultFlag"] = this.uOptionFirResultFlag.CheckedItem.DataValue.ToString();
                drRow["SecActionDesc"] = this.uTextSecActionDesc.Text;
                drRow["SecActionUserID"] = this.uTextSecActionUserID.Text;
                drRow["SecActionDate"] = Convert.ToDateTime(this.uDateSecActionDate.Value).ToString("yyyy-MM-dd");
                drRow["SecActionCompleteFlag"] = this.uCheckSecActionCompleteFlag.CheckedValue;
                if(this.uOptionSecResultFlag.CheckedIndex != -1)
                    drRow["SecResultFlag"] = this.uOptionSecResultFlag.CheckedItem.DataValue.ToString();
                drRow["ThiFCostQty"] = Convert.ToDecimal(this.uNumThiFCostQty.Value);
                drRow["ThiFCostTime"] = Convert.ToDecimal(this.uNumThiFCostTime.Value);
                drRow["ThiFCostPerson"] = Convert.ToDecimal(this.uNumThiFCostPerson.Value);
                drRow["ThiFCostScrap"] = Convert.ToDecimal(this.uNumThiFCostScrap.Value);
                drRow["ThiFCostCompleteFlag"] = this.uCheckThiFCostCompleteFlag.CheckedValue;
                if(this.uOptionThiResultFlag.CheckedIndex != -1)
                    drRow["ThiResultFlag"] = this.uOptionThiResultFlag.CheckedItem.DataValue.ToString();
                drRow["ImputationDeptCode"] = this.uComboImputeDept.Value.ToString();
                drRow["QCConfirmUserID"] = this.uTextQCConfirmUserID.Text;
                drRow["QCConfirmFlag"] = this.uCheckQCConfirmFlag.CheckedValue;
                drRow["MESReleaseTFlag"] = this.uTextMESReleaseTFlag.Text;
                drRow["QCConfirmComplete"] = this.uTextQCConfirmComplete.Text;
                if (this.uOptionSetConfirmResultFlag.CheckedIndex != -1)
                    drRow["QCConfirmResultFlag"] = this.uOptionSetConfirmResultFlag.CheckedItem.DataValue.ToString();

                drRow["TestSYSTEM"] = this.uComboSYSTEM.Value.ToString();
                drRow["TestHANDLER"] = this.uComboHandler.Value.ToString();
                drRow["TestPROGRAM"] = this.uTextProgram.Text;

                drRow["FirReturnDesc"] = this.uTextFirReturnDesc.Text;
                drRow["SecReturnDesc"] = this.uTextSecReturnDesc.Text;
                drRow["ThiReturnDesc"] = this.uTextThiReturnDesc.Text;

                ////String strConfirmDate = this.uDateQCConfirmDate.Value.ToString();
                ////String[] splitDate = strConfirmDate.Split(new char[] { ' ' });

                
                ////String strConffirmTime = Convert.ToDateTime(this.uDateQCConfirmTime.Value).ToString("HH:mm:ss");
                ////String[] splitTime = strConffirmTime.Split(new char[] { ' ' });

                ////String strDate = splitDate[0];

                if (this.uCheckQCConfirmFlag.Checked && this.uCheckQCConfirmFlag.Enabled)
                    drRow["QCConfirmDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                if (this.uTextFile.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextFile.Text);

                    drRow["File"] = PlantCode + "-" + this.uTextQCNNo.Text + "-" + fileDoc.Name;
                }
                else
                {
                    drRow["File"] = this.uTextFile.Text;
                }

                dtRtn.Rows.Add(drRow);
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// AffectLotList LotNo정보 저장
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_MESData()
        {
            DataTable dtMES = new DataTable();
            try
            {
                //컬럼설정
                dtMES.Columns.Add("LOTID", typeof(string));
                dtMES.Columns.Add("HOLDCODE", typeof(string));

                // HoldCode 가져오는 메소드 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                brwChannel.mfCredentials(clsReason);
                DataTable dtReason = new DataTable();
                string strHoldCode = string.Empty;

                //if (this.uTextReqNo.Text.Equals(string.Empty) && this.uTextReqSeq.Text.Equals(string.Empty))
                //{
                    dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(PlantCode, this.Name);

                    if (dtReason.Rows.Count > 0)
                        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                    if (this.uGridAffectLotList.Rows.Count > 0)
                    {
                        //MES 로 전송할 LotNo정보를 저장한다.
                        for (int i = 0; i < this.uGridAffectLotList.Rows.Count; i++)
                        {
                            if (!this.uGridAffectLotList.Rows[i].Cells["LotNo"].Value.ToString().Equals(string.Empty))
                            {
                                DataRow drLotNo = dtMES.NewRow();
                                drLotNo["LOTID"] = this.uGridAffectLotList.Rows[i].Cells["LotNo"].Value.ToString(); //LotNo
                                //drLotNo["HOLDCODE"] = ""; //정해진코드가 없음
                                drLotNo["HOLDCODE"] = "HQ11"
                                    ;
                                dtMES.Rows.Add(drLotNo);
                            }
                        }
                    }
                //}
                //else
                //{
                //    dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(PlantCode, "frmINS0008C");
                //    if (dtReason.Rows.Count > 0)
                //        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();
                //}

                //추가 LotNo 삽입
                DataRow drLot = dtMES.NewRow();
                drLot["LOTID"] = this.uTextLotNo.Text.Trim();
                drLot["HOLDCODE"] = strHoldCode;
                dtMES.Rows.Add(drLot);

                return dtMES;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMES;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uTextPlantName.Clear();
                this.uTextQCNNo.Clear();
                this.uTextP_QCNITR.Clear();
                this.uTextLotNo.Clear();
                this.uTextCustomerName.Clear();
                this.uTextProductCode.Clear();
                this.uTextProductName.Clear();
                this.uTextCustomerProductSpec.Clear();
                this.uTextAriseProcessName.Clear();
                this.uTextAriseEquipName.Clear();
                this.uTextAriseDate.Clear();
                this.uTextAriseTime.Clear();
                this.uTextExpectProcessName.Clear();
                this.uTextQty.Clear();
                this.uTextInspectQty.Clear();
                this.uTextFaultQty.Clear();
                this.uTextInspectFaultTypeName.Clear();
                this.uTextInspectUserName.Clear();
                this.uTextFaultImg.Clear();
                this.uCheckWorkStepMaterialFlag.Checked = false;
                this.uCheckWorkStepEquipFlag.Checked = false;
                this.uCheckActionFlag1.Checked = false;
                this.uCheckActionFlag2.Checked = false;
                this.uCheckActionFlag3.Checked = false;
                this.uCheckActionFlag4.Checked = false;
                this.uCheckActionFlag5.Checked = false;
                this.uCheckActionFlag6.Checked = false;
                this.uCheckActionFlag7.Checked = false;
                this.uCheckActionFlag8.Checked = false;
                this.uCheckActionFlag9.Checked = false;

                while (this.uGridAffectLotList.Rows.Count > 0)
                {
                    this.uGridAffectLotList.Rows[0].Delete(false);
                }
                while (this.uGridFaulty.Rows.Count > 0)
                {
                    this.uGridFaulty.Rows[0].Delete(false);
                }
                while (this.uGridExpectEquipList.Rows.Count > 0)
                {
                    this.uGridExpectEquipList.Rows[0].Delete(false);
                }

                this.uTextFirCauseDesc.Clear();
                this.uTextFirCauseUserID.Clear();
                this.uTextFirCauseUserName.Clear();
                this.uTextFirCauseDeptName.Clear();
                this.uDateFirCauseDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uCheckFirCauseCompleteFlag.Checked = false;

                this.uTextFirCorrectDesc.Clear();
                this.uTextFirCorrectUserID.Clear();
                this.uTextFirCorrectUserName.Clear();
                this.uTextFirCorrectDeptName.Clear();
                this.uDateFirCorrectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uCheckFirCorrectCompleteFlag.Checked = false;

                this.uTextFirMeasureDesc.Clear();
                this.uTextFirMeasureUserID.Clear();
                this.uTextFirMeasureUserName.Clear();
                this.uTextFirMeasureDeptName.Clear();
                this.uDateFirMeasureDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uCheckFirMeasureCompleteFlag.Checked = false;

                this.uCheckFir4MManFlag.Checked = false;
                this.uCheckFir4MEquipFlag.Checked = false;
                this.uCheckFir4MMethodFlag.Checked = false;
                this.uCheckFir4MEnviroFlag.Checked = false;
                this.uCheckFir4MMaterialFlag.Checked = false;
                this.uOptionFirResultFlag.CheckedIndex = -1;

                this.uTextSecActionDesc.Clear();
                this.uTextSecActionUserID.Clear();
                this.uTextSecActionUserName.Clear();
                this.uTextSecActionDeptName.Clear();
                this.uDateSecActionDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uCheckSecActionCompleteFlag.Checked = false;
                this.uOptionSecResultFlag.CheckedIndex = -1;

                this.uNumThiFCostQty.Value = 0;
                this.uNumThiFCostTime.Value = 0;
                this.uNumThiFCostPerson.Value = 0;
                this.uNumThiFCostScrap.Value = 0;
                this.uCheckThiFCostCompleteFlag.Checked = false;
                this.uOptionThiResultFlag.CheckedIndex = -1;

                this.uComboImputeDept.Value = "";
                this.uTextQCConfirmUserID.Clear();
                this.uTextQCConfirmUserName.Clear();
                this.uCheckQCConfirmFlag.Checked = false;

                this.uCheckQCConfirmFlag.Enabled = true;
                this.uButtonMaterial.Visible = false;
                this.uButtonEquip.Visible = false;

                this.uTextMESTrackOutTFlag.Text = "F";
                this.uTextMESReleaseTFlag.Text = "F";
                this.uTextQCConfirmComplete.Text = "F";
                this.uCheckQCConfirmFlag.Checked = false;
                this.uOptionSetConfirmResultFlag.CheckedIndex = -1;
                this.uDateQCConfirmDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateQCConfirmTime.Value = DateTime.Now.ToString("HH:mm:ss");

                this.uTextAriseProcessCode.Clear();
                this.uTextAriseEquipCode.Clear();
                this.uTextExpectProcessCode.Clear();

                this.uTextReqNo.Clear();
                this.uTextReqSeq.Clear();

                this.uLabelFirResult.Visible = false;
                this.uOptionFirResultFlag.Visible = false;

                this.uLabelSecResult.Visible = false;
                this.uOptionSecResultFlag.Visible = false;

                this.uLabelThiResult.Visible = false;
                this.uOptionThiResultFlag.Visible = false;

                ////this.uLabelQCConfirmUser.Visible = false;
                ////this.uTextQCConfirmUserID.Visible = false;
                ////this.uTextQCConfirmUserName.Visible = false;
                ////this.uCheckQCConfirmFlag.Visible = false;
                ////this.uDateQCConfirmDate.Visible = false;
                ////this.uDateQCConfirmTime.Visible = false;
                ////this.uLabelResultFlag.Visible = false;
                ////this.uOptionSetConfirmResultFlag.Visible = false;

                ////this.uLabelQCConfirmFlag.Hide();
                ////this.uLabelQCNCancel.Hide();
                ////this.uCheckQCNCancelFlag.Hide();
                ////this.uButtonQCNCancel.Hide();

                this.uGroupBoxQCConfirm.Visible = false;

                this.uCheckQCNCancelFlag.Checked = false;
                this.uTextQCNCancelFlag.Clear();

                this.uComboSYSTEM.Value = string.Empty;
                this.uComboHandler.Value = string.Empty;
                this.uTextProgram.Clear();
                this.uTextRetestFaultQty1.Clear();
                this.uTextRetestFaultQty2.Clear();
                this.uTextRetestFaultQty3.Clear();
                this.uTextRetestInspectQty1.Clear();
                this.uTextRetestInspectQty2.Clear();
                this.uTextRetestInspectQty3.Clear();

                this.uTextFirReturnDesc.Clear();
                this.uTextSecReturnDesc.Clear();
                this.uTextThiReturnDesc.Clear();

                this.uLabelQAENGR.Visible = false;
                this.uLabelMeasure.Visible = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// NumericEditor 왼쪽버튼(0) 클릭시 Value 값 0으로 만드는 Method
        /// </summary>
        /// <param name="sender"></param>
        private void SetNumericEditorZeroButton(object sender)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// NumericEditor SpinButton Method
        /// </summary>
        /// <param name="uNumEditor">NumericEditor 이름</param>
        /// <param name="e"></param>
        private void NumericEditorSpinButton(Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumEditor, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uNumEditor.Focus();
                    uNumEditor.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uNumEditor.Focus();
                    uNumEditor.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // Contents GroupBox 펼침상태 변화 이벤트'
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 150);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridQCNList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridQCNList.Height = 720;
                    for (int i = 0; i < this.uGridQCNList.Rows.Count; i++)
                    {
                        this.uGridQCNList.Rows[i].Fixed = false;
                    }

                    Clear();
                }

                //if (this.uGroupBoxContentsArea.Expanded)
                //{
                //    this.uGridQCNList.Rows.FixedRows.Clear();

                //    Clear();
                //}
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 공장콤보박스값이 변할때 공정콤보박스 설정하는 이벤트
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                String strPlantCode = this.uComboSearchPlantCode.Value.ToString();
                DataTable dtProcess = new DataTable();

                this.uComboSearchProcessCode.Items.Clear();

                if (strPlantCode != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcessCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 장비재검 버튼 이벤트
        private void uButtonEquip_Click(object sender, EventArgs e)
        {
            try
            {
                frmINSZ0011D1 frmPOP = new frmINSZ0011D1();

                // 폼간 매개변수 전달
                frmPOP.PlantCode = PlantCode;
                frmPOP.QCNNo = this.uTextQCNNo.Text;
                //frmPOP.MdiParent = this.ParentForm;

                frmPOP.ShowDialog();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQcn = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQcn);

                String strPlantCode = PlantCode;
                String strQCNNo = this.uTextQCNNo.Text;

                DataTable dtCheckMat = clsQcn.mfReadINSProcQCNMatReInspectCheck(strPlantCode, strQCNNo);
                DataTable dtCheckEqu = clsQcn.mfReadINSProcQCNEquReInspectCheck(strPlantCode, strQCNNo);

                if (!frmPOP.Equals("-1"))
                {
                    Int32 intMatFaultCount = Convert.ToInt32(dtCheckMat.Rows[0]["FaultCount"]);
                    Int32 intEquFaultCount = Convert.ToInt32(dtCheckEqu.Rows[0]["FaultCount"]);

                    if (intMatFaultCount + intEquFaultCount > 0)
                        this.uOptionSetConfirmResultFlag.CheckedIndex = 0;
                    else
                        this.uOptionSetConfirmResultFlag.CheckedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 자재재검 버튼 이벤트
        private void uButtonMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                if (!PlantCode.Equals(string.Empty) && !this.uTextQCNNo.Text.Equals(string.Empty) && !this.uTextLotNo.Text.Equals(string.Empty))
                {
                    frmINSZ0011D2 frmPOP = new frmINSZ0011D2();

                    // 폼간 매개변수 전달
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.QCNNo = this.uTextQCNNo.Text;
                    frmPOP.LotNo = this.uTextLotNo.Text;
                    //frmPOP.MdiParent = this.ParentForm;

                    frmPOP.ShowDialog();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                    QRPINS.BL.INSPRC.ProcQCN clsQcn = new QRPINS.BL.INSPRC.ProcQCN();
                    brwChannel.mfCredentials(clsQcn);

                    String strPlantCode = PlantCode;
                    String strQCNNo = this.uTextQCNNo.Text;

                    DataTable dtCheckMat = clsQcn.mfReadINSProcQCNMatReInspectCheck(strPlantCode, strQCNNo);
                    DataTable dtCheckEqu = clsQcn.mfReadINSProcQCNEquReInspectCheck(strPlantCode, strQCNNo);

                    Int32 intMatFaultCount = Convert.ToInt32(dtCheckMat.Rows[0]["FaultCount"]);
                    Int32 intEquFaultCount = Convert.ToInt32(dtCheckEqu.Rows[0]["FaultCount"]);

                    if (intMatFaultCount + intEquFaultCount > 0)
                        this.uOptionSetConfirmResultFlag.CheckedIndex = 0;
                    else
                        this.uOptionSetConfirmResultFlag.CheckedIndex = 1;

                    if (!this.uTextReqNo.Text.Equals(string.Empty) && !this.uTextReqSeq.Text.Equals(string.Empty) && frmPOP.SaveCheck)
                    {
                        // 헤더LotNo 가 Release 되었으면 TrackOut 메소드 호출
                        this.uTextMESReleaseTFlag.Text = frmPOP.MESReleaseTFlag_Header;
                        MES_TrackOut();
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 텍스트박스 버튼/키다운 이벤트
        // 1차 원인 및 검토 작성자 버튼 이벤트
        private void uTextFirCauseUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (PlantCode != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    if (PlantCode != frmPOP.PlantCode)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                            , msg.GetMessge_Text("M001254", strLang) + uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                            , Infragistics.Win.HAlign.Right);

                        this.uTextFirCauseUserID.Clear();
                        this.uTextFirCauseUserName.Clear();
                        this.uTextFirCauseDeptName.Clear();
                    }
                    else
                    {
                        this.uTextFirCauseUserID.Text = frmPOP.UserID;
                        this.uTextFirCauseUserName.Text = frmPOP.UserName;
                        this.uTextFirCauseDeptName.Text = frmPOP.DeptName;
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 1차 원인 및 검토 작성자 키다운 이벤트
        private void uTextFirCauseUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextFirCauseUserID.Text != "")
                    {
                        String strWriteID = this.uTextFirCauseUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtRtn = GetUserInfo(PlantCode, strWriteID);

                        if (dtRtn.Rows.Count > 0)
                        {
                            this.uTextFirCauseUserName.Text = dtRtn.Rows[0]["UserName"].ToString();
                            this.uTextFirCauseDeptName.Text = dtRtn.Rows[0]["DeptName"].ToString();
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextFirCauseUserID.Clear();
                            this.uTextFirCauseUserName.Clear();
                            this.uTextFirCauseDeptName.Clear();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextFirCauseUserID.TextLength <= 1 || this.uTextFirCauseUserID.SelectedText == this.uTextFirCauseUserID.Text)
                    {
                        this.uTextFirCauseUserName.Clear();
                        this.uTextFirCauseDeptName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 1차 시정조치 작성자 버튼이벤트
        private void uTextFirCorrectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (PlantCode != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    if (PlantCode != frmPOP.PlantCode)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                            , msg.GetMessge_Text("M001254", strLang) + uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                            , Infragistics.Win.HAlign.Right);

                        this.uTextFirCorrectUserID.Clear();
                        this.uTextFirCorrectUserName.Clear();
                        this.uTextFirCorrectDeptName.Clear();
                    }
                    else
                    {
                        this.uTextFirCorrectUserID.Text = frmPOP.UserID;
                        this.uTextFirCorrectUserName.Text = frmPOP.UserName;
                        this.uTextFirCorrectDeptName.Text = frmPOP.DeptName;
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 1차 시정조치 작성자 키다운 이벤트
        private void uTextFirCorrectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextFirCorrectUserID.Text != "")
                    {
                        String strWriteID = this.uTextFirCorrectUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtRtn = GetUserInfo(PlantCode, strWriteID);

                        if (dtRtn.Rows.Count > 0)
                        {
                            this.uTextFirCorrectUserName.Text = dtRtn.Rows[0]["UserName"].ToString();
                            this.uTextFirCorrectDeptName.Text = dtRtn.Rows[0]["DeptName"].ToString();
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextFirCorrectUserID.Clear();
                            this.uTextFirCauseUserName.Clear();
                            this.uTextFirCauseDeptName.Clear();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextFirCauseUserID.TextLength <= 1 || this.uTextFirCauseUserID.SelectedText == this.uTextFirCauseUserID.Text)
                    {
                        this.uTextFirCorrectUserName.Clear();
                        this.uTextFirCorrectDeptName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 1차 대책사항 작성자 버튼이벤트
        private void uTextFirMeasureUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (PlantCode != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    if (PlantCode != frmPOP.PlantCode)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                            , msg.GetMessge_Text("M001254", strLang) + uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                            , Infragistics.Win.HAlign.Right);

                        this.uTextFirMeasureUserID.Clear();
                        this.uTextFirMeasureUserName.Clear();
                        this.uTextFirMeasureDeptName.Clear();
                    }
                    else
                    {
                        this.uTextFirMeasureUserID.Text = frmPOP.UserID;
                        this.uTextFirMeasureUserName.Text = frmPOP.UserName;
                        this.uTextFirMeasureDeptName.Text = frmPOP.DeptName;
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 1차 대책사항 작성자 키다운 이벤트
        private void uTextFirMeasureUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextFirMeasureUserID.Text != "")
                    {
                        String strWriteID = this.uTextFirMeasureUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtRtn = GetUserInfo(PlantCode, strWriteID);

                        if (dtRtn.Rows.Count > 0)
                        {
                            this.uTextFirMeasureUserName.Text = dtRtn.Rows[0]["UserName"].ToString();
                            this.uTextFirMeasureDeptName.Text = dtRtn.Rows[0]["DeptName"].ToString();
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextFirMeasureUserID.Clear();
                            this.uTextFirMeasureUserName.Clear();
                            this.uTextFirMeasureDeptName.Clear();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextFirMeasureUserID.TextLength <= 1 || this.uTextFirMeasureUserID.SelectedText == this.uTextFirMeasureUserID.Text)
                    {
                        this.uTextFirMeasureUserName.Clear();
                        this.uTextFirMeasureDeptName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 2차 작성자 버튼 이벤트
        private void uTextSecActionUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (PlantCode != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    if (PlantCode != frmPOP.PlantCode)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                            , msg.GetMessge_Text("M001254", strLang) + uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                            , Infragistics.Win.HAlign.Right);

                        this.uTextSecActionUserID.Clear();
                        this.uTextSecActionUserName.Clear();
                        this.uTextSecActionDeptName.Clear();
                    }
                    else
                    {
                        this.uTextSecActionUserID.Text = frmPOP.UserID;
                        this.uTextSecActionUserName.Text = frmPOP.UserName;
                        this.uTextSecActionDeptName.Text = frmPOP.DeptName;
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 2차 작성자 키다운 이벤트
        private void uTextSecActionUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSecActionUserID.Text != "")
                    {
                        String strWriteID = this.uTextSecActionUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtRtn = GetUserInfo(PlantCode, strWriteID);

                        if (dtRtn.Rows.Count > 0)
                        {
                            this.uTextSecActionUserName.Text = dtRtn.Rows[0]["UserName"].ToString();
                            this.uTextSecActionDeptName.Text = dtRtn.Rows[0]["DeptName"].ToString();
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextSecActionUserID.Clear();
                            this.uTextSecActionUserName.Clear();
                            this.uTextSecActionDeptName.Clear();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSecActionUserID.TextLength <= 1 || this.uTextSecActionUserID.SelectedText == this.uTextSecActionUserID.Text)
                    {
                        this.uTextSecActionUserName.Clear();
                        this.uTextSecActionDeptName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 최종 Confirm 작성자 버튼 이벤트
        private void uTextQCConfirmUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (PlantCode != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    if (PlantCode != frmPOP.PlantCode)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                            , msg.GetMessge_Text("M001254", strLang) + uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                            , Infragistics.Win.HAlign.Right);

                        this.uTextQCConfirmUserID.Clear();
                        this.uTextQCConfirmUserName.Clear();
                    }
                    else
                    {
                        this.uTextQCConfirmUserID.Text = frmPOP.UserID;
                        this.uTextQCConfirmUserName.Text = frmPOP.UserName;
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M000392"
                                                , Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 최종 Confirm 작성자 키다운 이벤트
        private void uTextQCConfirmUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextQCConfirmUserID.Text != "")
                    {
                        String strWriteID = this.uTextQCConfirmUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtRtn = GetUserInfo(PlantCode, strWriteID);

                        if (dtRtn.Rows.Count > 0)
                        {
                            this.uTextQCConfirmUserName.Text = dtRtn.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextQCConfirmUserID.Clear();
                            this.uTextQCConfirmUserName.Clear();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextQCConfirmUserID.TextLength <= 1 || this.uTextQCConfirmUserID.SelectedText == this.uTextQCConfirmUserID.Text)
                    {
                        this.uTextQCConfirmUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 제품코드 팝업창 이벤트
        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.ShowDialog();

                this.uComboSearchPlantCode.Value = frmPOP.PlantCode;
                this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                this.uTextSearchProductName.Text = frmPOP.ProductName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 헤더 그리드 더블글릭시 상세데이터 보여주는 이벤트
        private void uGridQCNList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                Clear();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uGroupBoxContentsArea.Expanded = true;

                // 행고정
                e.Row.Fixed = true;

                //MESReleaseTFlag,QCConfirmFlag 저장                
                this.uTextMESReleaseTFlag.Text = e.Row.Cells["MESReleaseTFlag"].Value.ToString();
                this.uTextMESTrackOutTFlag.Text = e.Row.Cells["MESTrackOutTFlag"].Value.ToString();
                this.uTextQCConfirmComplete.Text = e.Row.Cells["QCConfirmFlag"].Value.ToString();

                PlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strQCNNo = e.Row.Cells["QCNNo"].Value.ToString();

                // 귀책부서 콤보박스 설정
                SetImputeDeptCombo();

                // SYSTEM, HANDLER 콤보박스 설정
                SetTESTComboBox(PlantCode);

                // 프로그래스바 생성
                WinMessageBox msg = new WinMessageBox();
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                GridFaultyCombo(PlantCode, m_resSys.GetString("SYS_LANG"));
                Search_HeaderTopData(strQCNNo, m_resSys.GetString("SYS_LANG"));
                Search_AffectLotData(strQCNNo, m_resSys.GetString("SYS_LANG"));
                Search_FaultType(PlantCode, strQCNNo, m_resSys.GetString("SYS_LANG")); // 2012-09-03 불량유형조회
                Search_ExpectEquipList(PlantCode, strQCNNo, m_resSys.GetString("SYS_LANG"));
                Search_HeaderBottomData(strQCNNo, m_resSys.GetString("SYS_LANG"));

                // 품질부서에서만 결과입력가능
                // 로그인유저의 부서가 품질부서일 경우만 입력가능
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCom);
                DataTable dtQualityDept = clsUserCom.mfReadUserCommonCode("QUA", "S0003", m_resSys.GetString("SYS_LANG"));
                if (dtQualityDept.Rows.Count > 0)
                {
                    string strDeptCheck = string.Empty;
                    var varQualityDept = from getRow in dtQualityDept.AsEnumerable()
                                         //where getRow["ComCode"].ToString() != string.Empty//m_resSys.GetString("SYS_DEPTCODE")
                                         select getRow["ComCode"].ToString();

                    if (varQualityDept.Contains(m_resSys.GetString("SYS_DEPTCODE")))
                    {
                        if (this.uCheckFirMeasureCompleteFlag.Checked == true)
                        {
                            this.uLabelFirResult.Visible = true;
                            this.uOptionFirResultFlag.Visible = true;
                            this.uOptionFirResultFlag.Value = "C";
                        }

                        if (this.uCheckSecActionCompleteFlag.Checked == true)
                        {
                            this.uLabelSecResult.Visible = true;
                            this.uOptionSecResultFlag.Visible = true;
                            this.uOptionSecResultFlag.Value = "C";
                        }

                        if (this.uCheckThiFCostCompleteFlag.Checked == true)
                        {
                            this.uLabelThiResult.Visible = true;
                            this.uOptionThiResultFlag.Visible = true;
                            this.uOptionThiResultFlag.Value = "C";
                        }

                        this.uGroupBoxQCConfirm.Visible = true;

                        this.uButtonEquip.Visible = this.uCheckWorkStepEquipFlag.Checked;
                        this.uButtonMaterial.Visible = this.uCheckWorkStepMaterialFlag.Checked;
                    }
                    else
                    {
                        this.uLabelFirResult.Visible = false;
                        this.uOptionFirResultFlag.Visible = false;

                        this.uLabelSecResult.Visible = false;
                        this.uOptionSecResultFlag.Visible = false;

                        this.uLabelThiResult.Visible = false;
                        this.uOptionThiResultFlag.Visible = false;

                        this.uGroupBoxQCConfirm.Visible = false;

                        this.uButtonEquip.Visible = false;
                        this.uButtonMaterial.Visible = false;
                    }
                }
                else
                {
                    this.uLabelFirResult.Visible = false;
                    this.uOptionFirResultFlag.Visible = false;

                    this.uLabelSecResult.Visible = false;
                    this.uOptionSecResultFlag.Visible = false;

                    this.uLabelThiResult.Visible = false;
                    this.uOptionThiResultFlag.Visible = false;

                    this.uLabelQCConfirmUser.Visible = false;
                    this.uTextQCConfirmUserID.Visible = false;
                    this.uTextQCConfirmUserName.Visible = false;
                    this.uCheckQCConfirmFlag.Visible = false;
                    this.uDateQCConfirmDate.Visible = false;
                    this.uDateQCConfirmTime.Visible = false;
                    this.uLabelResultFlag.Visible = false;
                    this.uOptionSetConfirmResultFlag.Visible = false;

                    this.uButtonEquip.Visible = false;
                    this.uButtonMaterial.Visible = false;
                }

                ////// 품질부서에서만 결과입력가능
                ////if (m_resSys.GetString("SYS_DEPTCODE").Equals("1010"))
                ////{
                ////    if (this.uCheckFirMeasureCompleteFlag.Checked == true)
                ////    {
                ////        this.uLabelFirResult.Visible = true;
                ////        this.uOptionFirResultFlag.Visible = true;
                ////        this.uOptionFirResultFlag.Value = "C";
                ////    }

                ////    if (this.uCheckSecActionCompleteFlag.Checked == true)
                ////    {
                ////        this.uLabelSecResult.Visible = true;
                ////        this.uOptionSecResultFlag.Visible = true;
                ////        this.uOptionSecResultFlag.Value = "C";
                ////    }

                ////    if (this.uCheckThiFCostCompleteFlag.Checked == true)
                ////    {
                ////        this.uLabelThiResult.Visible = true;
                ////        this.uOptionThiResultFlag.Visible = true;
                ////        this.uOptionThiResultFlag.Value = "C";
                ////    }

                ////    ////this.uLabelQCConfirmUser.Visible = true;
                ////    ////this.uTextQCConfirmUserID.Visible = true;
                ////    ////this.uTextQCConfirmUserName.Visible = true;
                ////    ////this.uCheckQCConfirmFlag.Visible = true;
                ////    ////this.uDateQCConfirmDate.Visible = true;
                ////    ////this.uDateQCConfirmTime.Visible = true;
                ////    ////this.uLabelResultFlag.Visible = true;
                ////    ////this.uOptionSetConfirmResultFlag.Visible = true;

                ////    ////this.uLabelQCConfirmFlag.Show();
                ////    ////this.uLabelQCNCancel.Show();
                ////    ////this.uCheckQCNCancelFlag.Show();
                ////    ////this.uButtonQCNCancel.Show();

                ////    this.uGroupBoxQCConfirm.Visible = true;
                ////}
                ////else
                ////{
                ////    this.uLabelFirResult.Visible = false;
                ////    this.uOptionFirResultFlag.Visible = false;

                ////    this.uLabelSecResult.Visible = false;
                ////    this.uOptionSecResultFlag.Visible = false;

                ////    this.uLabelThiResult.Visible = false;
                ////    this.uOptionThiResultFlag.Visible = false;

                ////    ////this.uLabelQCConfirmUser.Visible = false;
                ////    ////this.uTextQCConfirmUserID.Visible = false;
                ////    ////this.uTextQCConfirmUserName.Visible = false;
                ////    ////this.uCheckQCConfirmFlag.Visible = false;
                ////    ////this.uDateQCConfirmDate.Visible = false;
                ////    ////this.uDateQCConfirmTime.Visible = false;
                ////    ////this.uLabelResultFlag.Visible = false;
                ////    ////this.uOptionSetConfirmResultFlag.Visible = false;

                ////    this.uGroupBoxQCConfirm.Visible = false;
                ////}

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 스핀버튼 이벤트
        // 수량 0버튼
        private void uNumThiFCostQty_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 시간 0버튼
        private void uNumThiFCostTime_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 인원 0버튼
        private void uNumThiFCostPerson_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Scrap 0버튼
        private void uNumThiFCostScrap_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                SetNumericEditorZeroButton(sender);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 수량 스핀버튼
        private void uNumThiFCostQty_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumThiFCostQty, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 시간 스핀버튼
        private void uNumThiFCostTime_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumThiFCostTime, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 인원 스핀버튼
        private void uNumThiFCostPerson_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumThiFCostPerson, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Scrap 스핀버튼
        private void uNumThiFCostScrap_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                NumericEditorSpinButton(this.uNumThiFCostScrap, e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// ConfirmResult값이 retrun인 경우 최종승인 불가
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckQCConfirmFlag_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (!this.uOptionSetConfirmResultFlag.CheckedIndex.Equals(1))
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M001159",
                                        Infragistics.Win.HAlign.Right);

                    this.uCheckQCConfirmFlag.Checked = false;
                    
                    return;                    
                }

                ////// 공정검사에서 넘어온 정보일때
                ////if (!this.uTextReqNo.Text.Equals(string.Empty) && !this.uTextReqSeq.Text.Equals(string.Empty))
                ////{
                ////    if (this.uTextMESReleaseTFlag.Text.Equals("F"))
                ////    {
                ////        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                ////                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                            "확인창", "Lot Release 여부 확인", "자재재검을 통해 Lot Release 처리를 해주십시오.",
                ////                            Infragistics.Win.HAlign.Right);

                ////        this.uCheckQCConfirmFlag.Checked = false;

                ////        return;
                ////    }
                ////    else if (this.uTextMESTrackOutTFlag.Text.Equals("F") && this.uTextMESReleaseTFlag.Text.Equals("T"))
                ////    {
                ////        // DB정보 조회하여 TrackIn 한 정보인지 판단
                ////        QRPBrowser brwChannel = new QRPBrowser();
                ////        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                ////        QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                ////        brwChannel.mfCredentials(clsLot);

                ////        DataTable dtLot = clsLot.mfReadINSProcInspectReqLot(PlantCode, this.uTextReqNo.Text, this.uTextReqSeq.Text, m_resSys.GetString("SYS_LANG"));

                ////        // TrackIn 처리한 자료이면
                ////        if (dtLot.Rows[0]["MESTrackInTFlag"].ToString().Equals("T") || dtLot.Rows[0]["TrackInFlag"].ToString().Equals("T"))
                ////        {
                ////            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                ////                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                            "확인창", "Track Out 여부 확인", "Track Out처리가 필요합니다. 저장을 통해 TrackOut처리를 해주십시오.",
                ////                            Infragistics.Win.HAlign.Right);

                ////            this.uCheckQCConfirmFlag.Checked = false;

                ////            return;
                ////        }
                ////    }
                ////}

                if (string.IsNullOrEmpty(this.uComboImputeDept.Value.ToString()) || this.uComboImputeDept.Value == DBNull.Value)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000882", "M000322",
                                            Infragistics.Win.HAlign.Right);

                    this.uCheckQCConfirmFlag.Checked = false;
                    this.uComboImputeDept.DropDown();

                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// ConfirmResult값이 Return으로 변하면 최동승인 체크 풀림
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uOptionSetConfirmResultFlag_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uOptionSetConfirmResultFlag.CheckedIndex.Equals(1) && this.uCheckQCConfirmFlag.Enabled == true)
            {
                this.uCheckQCConfirmFlag.Checked = false;
            }
        }

        // QCN 취소 버튼 이벤트
        private void uButtonQCNCancel_Click_1(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQCN);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                string strErrRtn = clsQCN.mfSaveINSProcQCN_List_QCNCancelFlag(PlantCode
                                                                            , this.uTextQCNNo.Text
                                                                            , this.uCheckQCNCancelFlag.Checked.ToString().ToUpper().Substring(0, 1)
                                                                            , m_resSys.GetString("SYS_USERIP")
                                                                            , m_resSys.GetString("SYS_USERID"));

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리결과에 따른 메세지 박스
                if (!ErrRtn.ErrNum.Equals(0))
                {
                    
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000107"
                                                , "M000109",
                                                Infragistics.Win.HAlign.Right);
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000107", strLang)
                                                , ErrRtn.ErrMessage,
                                                Infragistics.Win.HAlign.Right);
                    }
                }
                else
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000107"
                                                , "M000108",
                                                Infragistics.Win.HAlign.Right);

                    // 리스트 갱신
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextFaultImg_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ////if (this.uCheckQCNCancelFlag.Enabled)
                ////{
                ////    if (this.uTextFaultImg.Text.Contains(PlantCode) &&
                ////        this.uTextFaultImg.Text.Contains(this.uTextQCNNo.Text) &&
                ////        this.uTextFaultImg.Text.Contains("-F-"))
                ////    {
                ////        if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                ////        {
                ////            //화일서버 연결정보 가져오기
                ////            QRPBrowser brwChannel = new QRPBrowser();
                ////            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                ////            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                ////            brwChannel.mfCredentials(clsSysAccess);
                ////            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                ////            //첨부파일 저장경로정보 가져오기
                ////            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                ////            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                ////            brwChannel.mfCredentials(clsSysFilePath);
                ////            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0028");

                ////            //첨부파일 삭제하기
                ////            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ////            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                ////            arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFaultImg.Text);

                ////            fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                ////                                                , arrFile
                ////                                                , dtSysAccess.Rows[0]["AccessID"].ToString()
                ////                                                , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                ////            fileAtt.mfFileUploadNoProgView();

                ////            this.uTextFaultImg.Clear();
                ////        }
                ////    }
                ////}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextFaultImg_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            if (this.uTextFaultImg.Text.Equals(string.Empty))
                return;

            if (e.Button.Key == "DOWN")
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 파일서버에서 불러올수 있는 파일인지 체크
                if (this.uTextFaultImg.Text.Contains(":\\"))
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000796",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0028");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                        arrFile.Add(this.uTextFaultImg.Text);

                        if (arrFile.Count > 0)
                        {
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + this.uTextFaultImg.Text);
                        }
                    }
                }
            }
        }

        private void SetTESTComboBox(string strPlantCode)
        {
            try
            {
                if (strPlantCode.Equals(string.Empty))
                    return;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                // SYSTEM
                DataTable dtEquip = clsEquip.mfReadEquipForPlantCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                // HANDLER
                DataTable dtHANDLER = clsEquip.mfReadEquip_Combo(strPlantCode, "", "", "", "", "TH", "", m_resSys.GetString("SYS_LANG"));

                // SYSTEM 설비 콤보박스
                WinComboEditor wComboe = new WinComboEditor();
                wComboe.mfSetComboEditor(this.uComboSYSTEM, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 500, Infragistics.Win.HAlign.Left, "", "", ""
                    , "EquipCode", "EquipName", dtEquip);

                this.uComboSYSTEM.ValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DataValue;

                // HANDLER 콤보박스
                wComboe.mfSetComboEditor(this.uComboHandler, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, string.Empty, true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 500, Infragistics.Win.HAlign.Left, "", "", ""
                    , "EquipCode", "EquipName", dtHANDLER);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형그리드콤보박스
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        private void GridFaultyCombo(string strPlantCode,string strLang)
        {
            try
            {
                // 불량유형
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsIFault = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsIFault);

                DataTable dtIFType = clsIFault.mfReadInspectFaultTypeCombo(strPlantCode, strLang);
                clsIFault.Dispose();
                this.uGridFaulty.DisplayLayout.Bands[0].Columns["InspectFaultTypeCode"].Layout.ValueLists.Clear();
                WinGrid grd = new WinGrid();
                // 불량유형콤보설정
                grd.mfSetGridColumnValueList(this.uGridFaulty, 0, "InspectFaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtIFType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// QC 최종완료 체크시 자재재검 등록여부에 따라서 메세지출력 후 상태변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckQCConfirmFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uCheckQCConfirmFlag.Checked)
                    return;

                // QC 최종완료 체크가 False || QC최종완료가 저장이 안되는
                if (!this.uCheckQCConfirmFlag.Enabled && this.uCheckQCConfirmFlag.Checked)
                    return;

                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 자재재검 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNMatReInspect), "ProcQCNMatReInspect");
                QRPINS.BL.INSPRC.ProcQCNMatReInspect clsQcn = new QRPINS.BL.INSPRC.ProcQCNMatReInspect();
                brwChannel.mfCredentials(clsQcn);

                // 자재재검 여부 검색 매서드 실행
                DataTable dtQcnCheck = clsQcn.mfReadINSProcQCNMatReInspect_Check(PlantCode, this.uTextQCNNo.Text, m_resSys.GetString("SYS_LANG"));

                // 자재재검 했을 경우 Return
                int intCnt = 0;
                if (dtQcnCheck.Rows.Count > 0)
                    intCnt = Convert.ToInt32(dtQcnCheck.Rows[0]["CheckCNT"]);
                
                if (intCnt != 0)
                    return;

                WinMessageBox msg = new WinMessageBox();

                DialogResult result;

                // 자재재검을 통해 Lot Release 처리를 해주십시오.
                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000969"
                                                , Infragistics.Win.HAlign.Right);


                this.uCheckQCConfirmFlag.CheckedChanged -= new EventHandler(uCheckQCConfirmFlag_CheckedChanged);
                this.uCheckQCConfirmFlag.Checked = false;
                this.uCheckQCConfirmFlag.CheckedChanged += new EventHandler(uCheckQCConfirmFlag_CheckedChanged);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextFile.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0040");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextFile.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFile.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void FileUpload_Header(string strQCNNo) 
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                if (this.uTextFile.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextFile.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + strQCNNo + "-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextFile.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }
                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0040");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
