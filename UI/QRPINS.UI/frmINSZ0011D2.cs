﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0011D2.cs                                      */
/* 프로그램명   : QCN LIST 자재재검 POPUP 창                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-29 : 기능구현 추가 (이종호)                   */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0011D2 : Form
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        #region 전역변수
        // 전역변수
        private string m_strPlantCode;
        private string m_strQCNNo;
        private string m_strLotNo;
        private string m_strMESReleaseTFlag_Header = "F";
        private bool m_bolSaveCheck = false;

        public bool SaveCheck
        {
            get { return m_bolSaveCheck; }
            set { m_bolSaveCheck = value; }
        }

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string QCNNo
        {
            get { return m_strQCNNo; }
            set { m_strQCNNo = value; }
        }

        public string LotNo
        {
            get { return m_strLotNo; }
            set { m_strLotNo = value; }
        }

        public string MESReleaseTFlag_Header
        {
            get { return m_strMESReleaseTFlag_Header; }
            set { m_strMESReleaseTFlag_Header = value; }
        }
        #endregion

        public frmINSZ0011D2()
        {
            InitializeComponent();
        }

        private void frmINSZ0011D2_Load(object sender, EventArgs e)
        {
            InitGrid();
            InitGroupBox();

            QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);

            // 검색메소드
            Search();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmINSZ0011D2_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                // 1차 검사
                wGroupBox.mfSetGroupBox(this.uGroupBox1st, GroupBoxType.LIST, "1차 검사(자재 재검사)", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1st.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1st.HeaderAppearance.FontData.SizeInPoints = 9;

                // 2차 검사
                wGroupBox.mfSetGroupBox(this.uGroupBox2nd, GroupBoxType.LIST, "2차 검사(자재 재검사)", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox2nd.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2nd.HeaderAppearance.FontData.SizeInPoints = 9;

                // 3차 검사
                wGroupBox.mfSetGroupBox(this.uGroupBox3th, GroupBoxType.LIST, "3차 검사(자재 재검사)", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3th.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3th.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 1차 검사

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1st, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1st, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectResult", "검사내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectQty", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                //wGrid.mfSetGridColumn(this.uGrid1st, 0, "FaultDesc", "불량내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "ResultFlag", "결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", m_resSys.GetString("SYS_USERID"));

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectUserName", "검사자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", m_resSys.GetString("SYS_USERNAME"));

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectDate", "검사일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "MESReleaseTFlag", "MES전송Flag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "MESHoldTFlag", "HOLDFlag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 2차 검사
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2nd, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectResult", "검사내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectQty", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                //wGrid.mfSetGridColumn(this.uGrid2nd, 0, "FaultDesc", "불량내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "ResultFlag", "결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", m_resSys.GetString("SYS_USERID"));

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectUserName", "검사자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", m_resSys.GetString("SYS_USERNAME"));

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectDate", "검사일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "MESReleaseTFlag", "MES전송Flag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "MESHoldTFlag", "HOLDFlag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 3차 검사
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid3th, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3th, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectResult", "검사내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectQty", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                //wGrid.mfSetGridColumn(this.uGrid3th, 0, "FaultDesc", "불량내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "ResultFlag", "결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", m_resSys.GetString("SYS_USERID"));

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectUserName", "검사자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", m_resSys.GetString("SYS_USERNAME"));

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectDate", "검사일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "MESReleaseTFlag", "MES전송Flag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "MESHoldTFlag", "HOLDFlag", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid1st.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1st.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2nd.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2nd.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid3th.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid3th.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 결과 컬럼 DropDown 설정
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1st, 0, "ResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGrid2nd, 0, "ResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGrid3th, 0, "ResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);


                // 불량유형                
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFType = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFType);
                DataTable dtFType = clsFType.mfReadInspectFaultTypeCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1st, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFType);
                wGrid.mfSetGridColumnValueList(this.uGrid2nd, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFType);
                wGrid.mfSetGridColumnValueList(this.uGrid3th, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFType);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 자재재검 정보 조회 Method
        /// </summary>
        private void Search()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNMatReInspect), "ProcQCNMatReInspect");
                QRPINS.BL.INSPRC.ProcQCNMatReInspect clsQCNMat = new QRPINS.BL.INSPRC.ProcQCNMatReInspect();
                brwChannel.mfCredentials(clsQCNMat);

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQcn = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQcn);

                // 다음 차수 데이터 조회여부 판단하기 위한 변수들
                int intBlankCount1 = 0;                
                int intOkCount1 = 0;
                int intMESCount1 = 0;
                int intBlankCount2 = 0;
                int intOkCount2 = 0;
                int intMESCount2 = 0;

                // 1차
                DataTable dtFir = clsQCNMat.mfReadINSProcQCNMatReInspect(PlantCode, QCNNo, 1);

                this.uGrid1st.DataSource = dtFir;
                this.uGrid1st.DataBind();

                for (int i = 0; i < this.uGrid1st.Rows.Count; i++)
                {
                    if (this.uGrid1st.Rows[i].Cells["InspectUserID"].Value.ToString().Equals(string.Empty) &&
                        this.uGrid1st.Rows[i].Cells["InspectUserName"].Value.ToString().Equals(string.Empty))
                    {
                        this.uGrid1st.Rows[i].Cells["InspectUserID"].Value = m_resSys.GetString("SYS_USERID");
                        this.uGrid1st.Rows[i].Cells["InspectUserName"].Value = m_resSys.GetString("SYS_USERNAME");
                    }

                    if (this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(""))
                    {
                        intBlankCount1 += 1;
                    }
                    else if (this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("OK"))
                    {
                        intOkCount1 += 1;
                        this.uGrid1st.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                        this.uGrid1st.Rows[i].Cells["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                    }
                    else if (this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                    {
                        this.uGrid1st.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                        this.uGrid1st.Rows[i].Cells["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                    }

                    if (this.uGrid1st.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                    {
                        intMESCount1 += 1;
                    }
                }

                // 2차
                // 검사결과가 모두 선택이 된 상태에서 모두 합격이 아니고 2012-11-14 주석
                // 그리드의 행이 존재하고, 합격인 Lot에 MES I/F 가 진행되었으면 2차 데이터 조회
                if (intBlankCount1.Equals(0) && !intOkCount1.Equals(dtFir.Rows.Count) &&
                   this.uGrid1st.Rows.Count > 0 && intMESCount1.Equals(intOkCount1))
                // 2012-11-14 
                // 그리드의 행이 존재하고, 합격된 Lot이 존재하고 ,합격인 Lot에 MES I/F 가 진행되었으면 2차 데이터 조회
                //if (this.uGrid1st.Rows.Count > 0
                //    && intOkCount1 > 0
                //    && intMESCount1.Equals(intOkCount1))
                {
                    DataTable dtSec = clsQCNMat.mfReadINSProcQCNMatReInspect(PlantCode, QCNNo, 2);

                    this.uGrid2nd.DataSource = dtSec;
                    this.uGrid2nd.DataBind();

                    for (int i = 0; i < this.uGrid2nd.Rows.Count; i++)
                    {
                        if (this.uGrid2nd.Rows[i].Cells["InspectUserID"].Value.ToString().Equals(string.Empty) &&
                            this.uGrid2nd.Rows[i].Cells["InspectUserName"].Value.ToString().Equals(string.Empty))
                        {
                            this.uGrid2nd.Rows[i].Cells["InspectUserID"].Value = m_resSys.GetString("SYS_USERID");
                            this.uGrid2nd.Rows[i].Cells["InspectUserName"].Value = m_resSys.GetString("SYS_USERNAME");
                        }

                        if (this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(""))
                        {
                            intBlankCount2 += 1;
                        }
                        else if (this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("OK"))
                        {
                            intOkCount2 += 1;
                            this.uGrid2nd.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                            this.uGrid2nd.Rows[i].Cells["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                        }
                        else if (this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                        {
                            this.uGrid2nd.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                            this.uGrid2nd.Rows[i].Cells["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                        }

                        if (this.uGrid2nd.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                        {
                            intMESCount2 += 1;
                        }
                    }

                    // 3차
                    if (intBlankCount2.Equals(0) && !intOkCount2.Equals(dtSec.Rows.Count) &&
                        this.uGrid2nd.Rows.Count > 0 && intMESCount2.Equals(intOkCount2))
                    {
                    // 2012-11-14 
                    // 그리드의 행이 존재하고, 합격된 Lot이 존재하고 ,합격인 Lot에 MES I/F 가 진행되었으면 3차 데이터 조회
                    //if (this.uGrid2nd.Rows.Count > 0
                    //    && intOkCount2 > 0
                    //    && intMESCount2 == intOkCount2)
                    //{
                        DataTable dtThi = clsQCNMat.mfReadINSProcQCNMatReInspect(PlantCode, QCNNo, 3);

                        this.uGrid3th.DataSource = dtThi;
                        this.uGrid3th.DataBind();

                        for (int i = 0; i < this.uGrid3th.Rows.Count; i++)
                        {
                            if (this.uGrid3th.Rows[i].Cells["InspectUserID"].Value.ToString().Equals(string.Empty) &&
                                this.uGrid3th.Rows[i].Cells["InspectUserName"].Value.ToString().Equals(string.Empty))
                            {
                                this.uGrid3th.Rows[i].Cells["InspectUserID"].Value = m_resSys.GetString("SYS_USERID");
                                this.uGrid3th.Rows[i].Cells["InspectUserName"].Value = m_resSys.GetString("SYS_USERNAME");
                            }

                            if (this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("OK"))
                            {
                                this.uGrid3th.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                                this.uGrid3th.Rows[i].Cells["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            }
                            else if (this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                            {
                                this.uGrid3th.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                                this.uGrid3th.Rows[i].Cells["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            }
                        }
                    }
                }

                ////완료 체크박스 로드
                //DataTable dtComplete = clsQcn.mfReadINSProcQCNDetail_List(PlantCode, QCNNo, m_resSys.GetString("SYS_LANG"));
                //if (dtComplete.Rows.Count > 0)
                //{
                //    if (dtComplete.Rows[0]["MaterialReInspectCompleteFlag"].ToString().Equals("T"))
                //    {
                //        this.uCheckComplete.Checked = true;
                //        this.uCheckComplete.Enabled = false;
                //    }
                //    else
                //    {
                //        this.uCheckComplete.Checked = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사자 ID 팝업창 메소드
        /// </summary>
        /// <param name="e"></param>
        private void PopUpUser(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "InspectUserID")
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    if (PlantCode != frmPOP.PlantCode)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M000798", "M000275", "M001254"
                                                    , Infragistics.Win.HAlign.Right);

                        e.Cell.Row.Cells["InspectUserID"].Value = "";
                        e.Cell.Row.Cells["InspectUserName"].Value = "";
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectUserID"].Value = frmPOP.UserID;
                        e.Cell.Row.Cells["InspectUserName"].Value = frmPOP.UserName;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        ///  저장버튼 클릭이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //if (this.uCheckComplete.Enabled.Equals(true))
                //{
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    // BL연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNMatReInspect), "ProcQCNMatReInspect");
                    QRPINS.BL.INSPRC.ProcQCNMatReInspect clsMat = new QRPINS.BL.INSPRC.ProcQCNMatReInspect();
                    brwChannel.mfCredentials(clsMat);

                    // 데이터 테이블 컬럼설정
                    DataTable dtSave = clsMat.mfSetDataInfo();
                    DataRow drRow;


                    // 불량유형 코드 검사            
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsFType = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsFType);
                    DataTable dtFType = clsFType.mfReadInspectFaultTypeCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));


                    #region 검사수량, 불량수량 체크
                    //검사수량은 0보다 커야하고, 수량 테이블에 null값이 들어갈수 없음
                    if (this.uGrid1st.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGrid1st.Rows.Count; i++)
                        {
                            if (this.uGrid1st.Rows[i].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            {
                                if (!this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty))
                                {
                                    if (!this.uGrid1st.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                                    {
                                        if (this.uGrid1st.Rows[i].Cells["InspectQty"].Value.ToString() == "0" || this.uGrid1st.Rows[i].Cells["InspectQty"].Value.ToString() == "")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M000374", "M001405", Infragistics.Win.HAlign.Right);

                                            this.uGrid1st.ActiveCell = this.uGrid1st.Rows[i].Cells["InspectQty"];
                                            this.uGrid1st.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        else if (Convert.ToInt32(this.uGrid1st.Rows[i].Cells["InspectQty"].Value) < Convert.ToInt32(this.uGrid1st.Rows[i].Cells["FaultQty"].Value))
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M000182", "M001406", Infragistics.Win.HAlign.Right);

                                            this.uGrid1st.ActiveCell = this.uGrid1st.Rows[i].Cells["InspectQty"];
                                            this.uGrid1st.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                            return;
                                        }
                                        if (this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                                        {
                                            if (this.uGrid1st.Rows[i].Cells["FaultQty"].Value.ToString().Equals(string.Empty)
                                                || this.uGrid1st.Rows[i].Cells["FaultQty"].Value.ToString().Equals("0"))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001407", Infragistics.Win.HAlign.Right);

                                                this.uGrid1st.ActiveCell = this.uGrid1st.Rows[i].Cells["FaultQty"];
                                                this.uGrid1st.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                                return;
                                            }
                                            else if (this.uGrid1st.Rows[i].Cells["FaultTypeCode"].Value.ToString().Equals(string.Empty)
                                                || dtFType.Select("InspectFaultTypeCode ='" + this.uGrid1st.Rows[i].Cells["FaultTypeCode"].Value.ToString() + "'").Count() == 0)
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001408", Infragistics.Win.HAlign.Right);

                                                this.uGrid1st.ActiveCell = this.uGrid1st.Rows[i].Cells["FaultTypeCode"];
                                                this.uGrid1st.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (this.uGrid2nd.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGrid2nd.Rows.Count; i++)
                        {
                            if (!this.uGrid2nd.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                            {
                                if (this.uGrid2nd.Rows[i].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                {
                                    if (!this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty))
                                    {
                                        if (this.uGrid2nd.Rows[i].Cells["InspectQty"].Value == null || this.uGrid2nd.Rows[i].Cells["InspectQty"].Value.ToString() == "" || this.uGrid2nd.Rows[i].Cells["InspectQty"].Value.ToString() == "0")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001405", Infragistics.Win.HAlign.Right);

                                            this.uGrid2nd.ActiveCell = this.uGrid2nd.Rows[i].Cells["InspectQty"];
                                            this.uGrid2nd.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                        if (this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                                        {
                                            if (this.uGrid2nd.Rows[i].Cells["FaultQty"].Value == null
                                                || this.uGrid2nd.Rows[i].Cells["FaultQty"].Value.ToString().Equals(string.Empty)
                                                || this.uGrid2nd.Rows[i].Cells["FaultQty"].Value.ToString().Equals("0"))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001407", Infragistics.Win.HAlign.Right);

                                                this.uGrid2nd.ActiveCell = this.uGrid2nd.Rows[i].Cells["FaultQty"];
                                                this.uGrid2nd.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                                return;
                                            }
                                            else if (Convert.ToInt32(this.uGrid2nd.Rows[i].Cells["InspectQty"].Value) < Convert.ToInt32(this.uGrid2nd.Rows[i].Cells["FaultQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M000182", "M001406", Infragistics.Win.HAlign.Right);

                                                this.uGrid2nd.ActiveCell = this.uGrid2nd.Rows[i].Cells["InspectQty"];
                                                this.uGrid2nd.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            }
                                            else if (this.uGrid2nd.Rows[i].Cells["FaultTypeCode"].Value.ToString().Equals(string.Empty)
                                                || dtFType.Select("InspectFaultTypeCode ='" + this.uGrid2nd.Rows[i].Cells["FaultTypeCode"].Value.ToString() + "'").Count() == 0)
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001408", Infragistics.Win.HAlign.Right);

                                                this.uGrid2nd.ActiveCell = this.uGrid2nd.Rows[i].Cells["FaultTypeCode"];
                                                this.uGrid2nd.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (this.uGrid3th.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGrid3th.Rows.Count; i++)
                        {
                            if (!this.uGrid3th.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                            {
                                if (this.uGrid3th.Rows[i].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                {
                                    if (!this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty))
                                    {
                                        if (this.uGrid3th.Rows[i].Cells["InspectQty"].Value == null || this.uGrid3th.Rows[i].Cells["InspectQty"].Value.ToString() == "" || this.uGrid3th.Rows[i].Cells["InspectQty"].Value.ToString() == "0")
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001405", Infragistics.Win.HAlign.Right);

                                            this.uGrid3th.ActiveCell = this.uGrid3th.Rows[i].Cells["InspectQty"];
                                            this.uGrid3th.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }

                                        if (this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                                        {
                                            if (this.uGrid3th.Rows[i].Cells["FaultQty"].Value.ToString() == null
                                                || this.uGrid3th.Rows[i].Cells["FaultQty"].Value.ToString().Equals(string.Empty)
                                                || this.uGrid3th.Rows[i].Cells["FaultQty"].Value.ToString().Equals("0"))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001407", Infragistics.Win.HAlign.Right);

                                                this.uGrid3th.ActiveCell = this.uGrid3th.Rows[i].Cells["FaultQty"];
                                                this.uGrid3th.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                                return;
                                            }
                                            else if (Convert.ToInt32(this.uGrid3th.Rows[i].Cells["InspectQty"].Value) < Convert.ToInt32(this.uGrid3th.Rows[i].Cells["FaultQty"].Value))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M000182", "M001406", Infragistics.Win.HAlign.Right);

                                                this.uGrid3th.ActiveCell = this.uGrid3th.Rows[i].Cells["InspectQty"];
                                                this.uGrid3th.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            }
                                            else if (this.uGrid3th.Rows[i].Cells["FaultTypeCode"].Value.ToString().Equals(string.Empty)
                                                || dtFType.Select("InspectFaultTypeCode ='" + this.uGrid3th.Rows[i].Cells["FaultTypeCode"].Value.ToString() + "'").Count() == 0)
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001223", "M001408", Infragistics.Win.HAlign.Right);

                                                this.uGrid3th.ActiveCell = this.uGrid3th.Rows[i].Cells["FaultTypeCode"];
                                                this.uGrid3th.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                                return;
                                            }
                                        }
                                    }
                                    ////else
                                    ////{
                                    ////    // 3차 검사시 HOLD된 정보 결과 입력하도록 설정 2012-11-13
                                    ////    // 검사결과가 판정나지 않은 정보는 저장할 수 없습니다.
                                    ////    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    ////                                               , "M001135", "M001223", "M000175", Infragistics.Win.HAlign.Right);

                                    ////    this.uGrid3th.ActiveCell = this.uGrid3th.Rows[i].Cells["ResultFlag"];
                                    ////    this.uGrid3th.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    ////    return;
                                    ////}
                                }
                            }
                        }
                    }
                    #endregion

                    // 1차검사 데이터 저장
                    if (this.uGrid1st.Rows.Count > 0)
                    {
                        this.uGrid1st.ActiveCell = this.uGrid1st.Rows[0].Cells[0];
                        for (int i = 0; i < this.uGrid1st.Rows.Count; i++)
                        {
                            //if (!this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty) &&
                            //    !this.uGrid1st.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                            if (!this.uGrid1st.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T") && !this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty))
                            {
                                drRow = dtSave.NewRow();
                                drRow["PlantCode"] = PlantCode;
                                drRow["QCNNo"] = QCNNo;
                                drRow["InspectSeq"] = 1;
                                drRow["Seq"] = this.uGrid1st.Rows[i].RowSelectorNumber;
                                drRow["LotNo"] = this.uGrid1st.Rows[i].Cells["LotNo"].Value.ToString().Trim().ToUpper();
                                drRow["InspectResult"] = this.uGrid1st.Rows[i].Cells["InspectResult"].Value.ToString();
                                drRow["InspectQty"] = this.uGrid1st.Rows[i].Cells["InspectQTy"].Value.ToString();
                                drRow["FaultTypeCode"] = this.uGrid1st.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                                drRow["FaultQty"] = this.uGrid1st.Rows[i].Cells["FaultQty"].Value.ToString();
                                drRow["FaultDesc"] = this.uGrid1st.Rows[i].Cells["FaultDesc"].Value.ToString();
                                drRow["ResultFlag"] = this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString();
                                drRow["InspectUserID"] = this.uGrid1st.Rows[i].Cells["InspectUserID"].Value.ToString().Trim().ToUpper();
                                //drRow["InspectDate"] = Convert.ToDateTime(this.uGrid1st.Rows[i].Cells["InspectDate"].Value).ToString("yyyy-MM-dd");
                                drRow["MESReleaseTFlag"] = this.uGrid1st.Rows[i].Cells["MESReleaseTFlag"].Value.ToString();
                                drRow["MESHoldTFlag"] = this.uGrid1st.Rows[i].Cells["MESHoldTFlag"].Value.ToString();
                                dtSave.Rows.Add(drRow);
                            }
                        }
                    }

                    // 2차 검사 데이터 저장
                    if (this.uGrid2nd.Rows.Count > 0)
                    {
                        this.uGrid2nd.ActiveCell = this.uGrid2nd.Rows[0].Cells[0];
                        for (int i = 0; i < this.uGrid2nd.Rows.Count; i++)
                        {
                            //if (!this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty) &&
                            //    !this.uGrid2nd.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                            if (!this.uGrid2nd.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T") && !this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty))
                            {
                                drRow = dtSave.NewRow();
                                drRow["PlantCode"] = PlantCode;
                                drRow["QCNNo"] = QCNNo;
                                drRow["InspectSeq"] = 2;
                                drRow["Seq"] = this.uGrid2nd.Rows[i].RowSelectorNumber;
                                drRow["LotNo"] = this.uGrid2nd.Rows[i].Cells["LotNo"].Value.ToString();
                                drRow["InspectResult"] = this.uGrid2nd.Rows[i].Cells["InspectResult"].Value.ToString();
                                drRow["InspectQty"] = this.uGrid2nd.Rows[i].Cells["InspectQTy"].Value.ToString();
                                drRow["FaultTypeCode"] = this.uGrid2nd.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                                drRow["FaultQty"] = this.uGrid2nd.Rows[i].Cells["FaultQty"].Value.ToString();
                                drRow["FaultDesc"] = this.uGrid2nd.Rows[i].Cells["FaultDesc"].Value.ToString();
                                drRow["ResultFlag"] = this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString();
                                drRow["InspectUserID"] = this.uGrid2nd.Rows[i].Cells["InspectUserID"].Value.ToString().Trim().ToUpper();
                                //drRow["InspectDate"] = Convert.ToDateTime(this.uGrid2nd.Rows[i].Cells["InspectDate"].Value).ToString("yyyy-MM-dd");
                                drRow["MESReleaseTFlag"] = this.uGrid2nd.Rows[i].Cells["MESReleaseTFlag"].Value.ToString();
                                drRow["MESHoldTFlag"] = this.uGrid2nd.Rows[i].Cells["MESHoldTFlag"].Value.ToString();
                                dtSave.Rows.Add(drRow);
                            }
                        }
                    }

                    // 3차 검사 데이터 저장
                    if (this.uGrid3th.Rows.Count > 0)
                    {
                        this.uGrid3th.ActiveCell = this.uGrid3th.Rows[0].Cells[0];
                        for (int i = 0; i < this.uGrid3th.Rows.Count; i++)
                        {
                            //if (!this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty) &&
                            //    !this.uGrid3th.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T"))
                            if (!this.uGrid3th.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T") && !this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString().Equals(string.Empty))
                            {
                                drRow = dtSave.NewRow();
                                drRow["PlantCode"] = PlantCode;
                                drRow["QCNNo"] = QCNNo;
                                drRow["InspectSeq"] = 3;
                                drRow["Seq"] = this.uGrid3th.Rows[i].RowSelectorNumber;
                                drRow["LotNo"] = this.uGrid3th.Rows[i].Cells["LotNo"].Value.ToString();
                                drRow["InspectResult"] = this.uGrid3th.Rows[i].Cells["InspectResult"].Value.ToString();
                                drRow["InspectQty"] = this.uGrid3th.Rows[i].Cells["InspectQTy"].Value.ToString();
                                drRow["FaultTypeCode"] = this.uGrid3th.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                                drRow["FaultQty"] = this.uGrid3th.Rows[i].Cells["FaultQty"].Value.ToString();
                                drRow["FaultDesc"] = this.uGrid3th.Rows[i].Cells["FaultDesc"].Value.ToString();
                                drRow["ResultFlag"] = this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString();
                                drRow["InspectUserID"] = this.uGrid3th.Rows[i].Cells["InspectUserID"].Value.ToString().Trim().ToUpper();
                                //drRow["InspectDate"] = Convert.ToDateTime(this.uGrid3th.Rows[i].Cells["InspectDate"].Value).ToString("yyyy-MM-dd");
                                drRow["MESReleaseTFlag"] = this.uGrid3th.Rows[i].Cells["MESReleaseTFlag"].Value.ToString();
                                drRow["MESHoldTFlag"] = this.uGrid3th.Rows[i].Cells["MESHoldTFlag"].Value.ToString();
                                dtSave.Rows.Add(drRow);
                            }
                        }
                    }
                    if (dtSave.Rows.Count > 0)
                    {
                        // 프로그래스 팝업창 생성
                        //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        //Thread t1 = m_ProgressPopup.mfStartThread();
                        //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.Cursor = Cursors.WaitCursor;
                        //this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장 메소드 실행
                        string strErrRtn = clsMat.mfSaveINSProcQCNMatReInspect(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        //this.MdiParent.Cursor = Cursors.Default;
                        this.Cursor = Cursors.Default;
                        //m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        //TransErrRtn ErrRtn2 = new TransErrRtn();
                        //ErrRtn2 = ErrRtn2.mfDecodingErrMessage(strComplete);

                        // 처리결과에 따른 메세지 박스
                        //if (ErrRtn.ErrNum == 0 && ErrRtn2.ErrNum == 0)
                        if (ErrRtn.ErrNum == 0)
                        {                            
                            SaveMES_Release(dtSave);
                        }
                        else
                        {
                            DialogResult Result = new DialogResult();
                           // WinMessageBox msg = new WinMessageBox();

                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        ////DialogResult Result = new DialogResult();
                        //////WinMessageBox msg = new WinMessageBox();

                        ////Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        ////                    "처리결과", "저장 데이터 조회 결과", "저장 할 정보가 없습니다.",
                        ////                    Infragistics.Win.HAlign.Right);
                        
                        // 헤더 LotNo MES Release 요청 성공시 MESReleaseTFlag_Header "T"로 설정
                        MESReleaseTFlag_Header = "T";

                        this.Close();
                    }
                //}
                //else
                //{
                //    this.Close();
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// MES Release I/F 메소드
        /// </summary>
        /// <param name="dtSave"></param>
        private void SaveMES_Release(DataTable dtSave)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                bool bolSaveCheck = true;
                bool bolIFCheck = false;

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNMatReInspect), "ProcQCNMatReInspect");
                QRPINS.BL.INSPRC.ProcQCNMatReInspect clsMatReInspect = new QRPINS.BL.INSPRC.ProcQCNMatReInspect();
                brwChannel.mfCredentials(clsMatReInspect);



                string strHoldCode = string.Empty;
                string strQCNITR = Search_QCNITRData(m_resSys.GetString("SYS_LANG"));
                if (strQCNITR.ToUpper().Equals("QCN"))
                {
                    //// HoldCode 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                    QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                    brwChannel.mfCredentials(clsReason);

                    DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(PlantCode, "frmINSZ0011");

                    if (dtReason.Rows.Count > 0)
                        strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                }
                else if (strQCNITR.ToUpper().Equals("ITR")) // 2012-08-21 하드코드
                    strHoldCode = "H003";
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M000095"
                                            , "M000942"
                                            , Infragistics.Win.HAlign.Right);
                    SaveCheck = true;
                    this.Close();
                }
               

                // LotList DataTable 컬럼설정
                DataTable dtLotList = new DataTable();
                dtLotList.Columns.Add("LOTID", typeof(string));
                dtLotList.Columns.Add("HOLDCODE", typeof(string));

                DataRow drRow;
                string strErrRtn = string.Empty;
                TransErrRtn ErrRtn = new TransErrRtn();

                // for문 돌며 Lot 하나씩 MES I/F
                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 검사결과 합격이고, MES Flag가 "T"가 아닌경우 MES I/F 진행
                    if (!dtSave.Rows[i]["MESReleaseTFlag"].ToString().Equals("T") 
                        && dtSave.Rows[i]["ResultFlag"].ToString().Equals("OK")) 
                    {
                        // QCN발행 LotNo가 아니고 MESHoldFlag 도 T 가아닌경우 PASS (Affect Lot은 HOLD X)
                        if (!dtSave.Rows[i]["LotNo"].ToString().Equals(LotNo)
                            && !dtSave.Rows[i]["MESHoldTFlag"].ToString().Equals("T"))
                            continue;

                        bolIFCheck = true;
                        // AffectLot 의 경우 HoldCode "HQ11"로 하드코딩 - > 2012-08-20 QCN,ITR HoldCode로 변경
                        drRow = dtLotList.NewRow();
                        drRow["LOTID"] = dtSave.Rows[i]["LotNo"].ToString();
                        if (dtSave.Rows[i]["LotNo"].ToString().Equals(LotNo))
                            drRow["HOLDCODE"] = strHoldCode;
                        else
                            drRow["HOLDCODE"] = strHoldCode;//"HQ11";
                        dtLotList.Rows.Add(drRow);

                        strErrRtn = clsMatReInspect.mfSaveINSProcQCNMatReInspect_MESRelease(this.Name
                                                                                            , dtSave.Rows[i]["InspectUserID"].ToString()
                                                                                            , dtSave.Rows[i]["InspectResult"].ToString()
                                                                                            , dtLotList
                                                                                            , dtSave.Rows[i]["PlantCode"].ToString()
                                                                                            , dtSave.Rows[i]["QCNNo"].ToString()
                                                                                            , Convert.ToInt32(dtSave.Rows[i]["InspectSeq"])
                                                                                            , Convert.ToInt32(dtSave.Rows[i]["Seq"])
                                                                                            , m_resSys.GetString("SYS_USERID")
                                                                                            , m_resSys.GetString("SYS_USERIP"));

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();

                            bolSaveCheck = false;
                            if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M000095"
                                            , "M000947"
                                            , Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                    , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                            }
                            Search();
                            break;
                        }
                        else
                        {
                            if (dtSave.Rows[i]["LotNo"].ToString().Equals(LotNo))
                            {
                                // 헤더 LotNo MES Release 요청 성공시 MESReleaseTFlag_Header "T"로 설정
                                MESReleaseTFlag_Header = "T";
                            }
                        }
                    }
                }
                if (bolSaveCheck && bolIFCheck)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M000095"
                                            , "M000942"
                                            , Infragistics.Win.HAlign.Right);
                    SaveCheck = true;
                    this.Close();
                }
                else if (bolSaveCheck && !bolIFCheck)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M001023"
                                            , "M000930"
                                            , Infragistics.Win.HAlign.Right);
                    SaveCheck = true;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 1차 검사 검사자 ID 버튼클릭 이벤트
        private void uGrid1st_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                PopUpUser(e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 2차 검사 검사자 ID 버튼 클릭 이벤트
        private void uGrid2nd_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                PopUpUser(e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 3차 검사자 ID버튼 클릭 이벤트
        private void uGrid3th_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                PopUpUser(e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 그리드 키다운 이벤트 / 관련 메소드
        private void uGrid1st_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && uGrid1st.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    SearchUserName(this.uGrid1st);
                }
                else if (e.KeyCode == Keys.Back && uGrid1st.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    string strInspectUserID = this.uGrid1st.ActiveCell.Text;
                    if (strInspectUserID.Length <= 1)
                    {
                        this.uGrid1st.ActiveRow.Cells["InspectUserName"].Value = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2nd_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && uGrid2nd.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    SearchUserName(this.uGrid2nd);
                }
                else if (e.KeyCode == Keys.Back && uGrid2nd.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    string strInspectUserID = this.uGrid2nd.ActiveCell.Text;
                    if (strInspectUserID.Length <= 1)
                    {
                        this.uGrid2nd.ActiveRow.Cells["InspectUserName"].Value = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3th_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && uGrid3th.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    SearchUserName(this.uGrid3th);
                }
                else if (e.KeyCode == Keys.Back && uGrid3th.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    string strInspectUserID = this.uGrid3th.ActiveCell.Text;
                    if (strInspectUserID.Length <= 1)
                    {
                        this.uGrid3th.ActiveRow.Cells["InspectUserName"].Value = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사자 아이디 입력시 명 조회해서 그리드에 Display하는 메소드
        /// </summary>
        /// <param name="uGrid"></param>
        private void SearchUserName(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            try
            {
                if (!uGrid.ActiveCell.Text.Equals(string.Empty))
                {
                    // SystemInfor ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    string strUserID = uGrid.ActiveCell.Text.Trim();

                    DataTable dtUser = GetUserInfo(PlantCode, strUserID);

                    if (dtUser.Rows.Count > 0)
                    {
                        uGrid.ActiveRow.Cells["InspectUserName"].Value = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000621",
                                    Infragistics.Win.HAlign.Right);

                        uGrid.ActiveCell.Row.Cells["WorkUserName"].Value = "";
                        uGrid.ActiveCell.Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 모든 Lot에 대해 MES Release 전송여부 확인 메소드
        /// </summary>
        /// <returns></returns>
        private bool CheckedRelease_All()
        {
            try
            {
                for (int i = 0; i < this.uGrid3th.Rows.Count; i++)
                {
                    if (this.uGrid3th.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("F"))
                    {
                        return false;
                    }
                }
                for (int i = 0; i < this.uGrid2nd.Rows.Count; i++)
                {
                    if (this.uGrid2nd.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("F"))
                    {
                        return false;
                    }
                }
                for (int i = 0; i < this.uGrid1st.Rows.Count; i++)
                {
                    if (this.uGrid1st.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("F"))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
            }
        }

        private void uGrid1st_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("InspectQty") || e.Cell.Column.Key.Equals("FaultQty"))
                {
                    frmINSZ0011D2P frmPOP = new frmINSZ0011D2P();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.QCNNo = QCNNo;
                    frmPOP.InspectSeq = 1;
                    frmPOP.Seq = e.Cell.Row.RowSelectorNumber;

                    if (e.Cell.Row.Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                        frmPOP.SaveCheck = true;
                    ////else if (e.Cell.Row.Activation == Infragistics.Win.UltraWinGrid.Activation.ActivateOnly)
                    ////    frmPOP.SaveCheck = false;
                    else
                        frmPOP.SaveCheck = false;

                    frmPOP.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2nd_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("InspectQty") || e.Cell.Column.Key.Equals("FaultQty"))
                {
                    frmINSZ0011D2P frmPOP = new frmINSZ0011D2P();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.QCNNo = QCNNo;
                    frmPOP.InspectSeq = 2;
                    frmPOP.Seq = e.Cell.Row.RowSelectorNumber;

                    if (e.Cell.Row.Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                        frmPOP.SaveCheck = true;
                    ////else if (e.Cell.Row.Activation == Infragistics.Win.UltraWinGrid.Activation.ActivateOnly)
                    ////    frmPOP.SaveCheck = false;
                    else
                        frmPOP.SaveCheck = false;

                    frmPOP.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3th_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("InspectQty") || e.Cell.Column.Key.Equals("FaultQty"))
                {
                    frmINSZ0011D2P frmPOP = new frmINSZ0011D2P();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.QCNNo = QCNNo;
                    frmPOP.InspectSeq = 3;
                    frmPOP.Seq = e.Cell.Row.RowSelectorNumber;

                    if (e.Cell.Row.Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                        frmPOP.SaveCheck = true;
                    ////else if (e.Cell.Row.Activation == Infragistics.Win.UltraWinGrid.Activation.ActivateOnly)
                    ////    frmPOP.SaveCheck = false;
                    else
                        frmPOP.SaveCheck = false;

                    frmPOP.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">언어</param>
        private string Search_QCNITRData(string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQCN = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQCN);

                DataTable dtRtn = clsQCN.mfReadINSProcQCNDetail(m_strPlantCode, m_strQCNNo, strLang);
                clsQCN.Dispose();

                string strRtn = "";
                if (dtRtn.Rows.Count > 0)
                    strRtn = dtRtn.Rows[0]["P_QcnItrType"].ToString();

                
                return strRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            {
            }
        }

    }
}
