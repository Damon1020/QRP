﻿namespace QRPINS.UI
{
    /// <summary>
    /// Summary description for rptINSZ0002_S02.
    /// </summary>
    partial class rptINSZ0002_S02
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptINSZ0002_S02));
            this.lbllTitle = new DataDynamics.ActiveReports.Label();
            this.txtTopTitle = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX1 = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX2 = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX3 = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX4 = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX5 = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX6 = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX7 = new DataDynamics.ActiveReports.TextBox();
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.txtTitle = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX1 = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX2 = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX3 = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX4 = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX5 = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX6 = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX7 = new DataDynamics.ActiveReports.TextBox();
            this.txtInpectX8 = new DataDynamics.ActiveReports.TextBox();
            this.txtTopX8 = new DataDynamics.ActiveReports.TextBox();
            this.groupHeader = new DataDynamics.ActiveReports.GroupHeader();
            this.groupFooter = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lbllTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // lbllTitle
            // 
            this.lbllTitle.Height = 0.2F;
            this.lbllTitle.HyperLink = null;
            this.lbllTitle.Left = 0.004F;
            this.lbllTitle.Name = "lbllTitle";
            this.lbllTitle.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.lbllTitle.Text = "3. 검사 DATA";
            this.lbllTitle.Top = 0.01F;
            this.lbllTitle.Width = 1.146F;
            // 
            // txtTopTitle
            // 
            this.txtTopTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopTitle.Height = 0.645F;
            this.txtTopTitle.Left = 0.004001024F;
            this.txtTopTitle.Name = "txtTopTitle";
            this.txtTopTitle.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopTitle.Text = null;
            this.txtTopTitle.Top = 0.231F;
            this.txtTopTitle.Width = 0.747999F;
            // 
            // txtTopX1
            // 
            this.txtTopX1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX1.Height = 0.645F;
            this.txtTopX1.Left = 0.748F;
            this.txtTopX1.Name = "txtTopX1";
            this.txtTopX1.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX1.Text = null;
            this.txtTopX1.Top = 0.231F;
            this.txtTopX1.Width = 0.815F;
            // 
            // txtTopX2
            // 
            this.txtTopX2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX2.Height = 0.645F;
            this.txtTopX2.Left = 1.563F;
            this.txtTopX2.Name = "txtTopX2";
            this.txtTopX2.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX2.Text = null;
            this.txtTopX2.Top = 0.231F;
            this.txtTopX2.Width = 0.815F;
            // 
            // txtTopX3
            // 
            this.txtTopX3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX3.Height = 0.645F;
            this.txtTopX3.Left = 2.378F;
            this.txtTopX3.Name = "txtTopX3";
            this.txtTopX3.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX3.Text = null;
            this.txtTopX3.Top = 0.231F;
            this.txtTopX3.Width = 0.815F;
            // 
            // txtTopX4
            // 
            this.txtTopX4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX4.Height = 0.645F;
            this.txtTopX4.Left = 3.193F;
            this.txtTopX4.Name = "txtTopX4";
            this.txtTopX4.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX4.Text = null;
            this.txtTopX4.Top = 0.231F;
            this.txtTopX4.Width = 0.815F;
            // 
            // txtTopX5
            // 
            this.txtTopX5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX5.Height = 0.645F;
            this.txtTopX5.Left = 4.008F;
            this.txtTopX5.Name = "txtTopX5";
            this.txtTopX5.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX5.Text = null;
            this.txtTopX5.Top = 0.231F;
            this.txtTopX5.Width = 0.8270001F;
            // 
            // txtTopX6
            // 
            this.txtTopX6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX6.Height = 0.645F;
            this.txtTopX6.Left = 4.835001F;
            this.txtTopX6.Name = "txtTopX6";
            this.txtTopX6.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX6.Text = null;
            this.txtTopX6.Top = 0.231F;
            this.txtTopX6.Width = 0.815F;
            // 
            // txtTopX7
            // 
            this.txtTopX7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX7.Height = 0.645F;
            this.txtTopX7.Left = 5.646F;
            this.txtTopX7.Name = "txtTopX7";
            this.txtTopX7.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX7.Text = null;
            this.txtTopX7.Top = 0.231F;
            this.txtTopX7.Width = 0.8109999F;
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtTitle,
            this.txtInpectX1,
            this.txtInpectX2,
            this.txtInpectX3,
            this.txtInpectX4,
            this.txtInpectX5,
            this.txtInpectX6,
            this.txtInpectX7,
            this.txtInpectX8});
            this.detail.Height = 0.3010001F;
            this.detail.Name = "detail";
            // 
            // txtTitle
            // 
            this.txtTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTitle.DataField = "Title";
            this.txtTitle.Height = 0.3010001F;
            this.txtTitle.Left = 0F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtTitle.Text = null;
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 0.752F;
            // 
            // txtInpectX1
            // 
            this.txtInpectX1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX1.DataField = "#1";
            this.txtInpectX1.Height = 0.301F;
            this.txtInpectX1.Left = 0.748F;
            this.txtInpectX1.Name = "txtInpectX1";
            this.txtInpectX1.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX1.Text = null;
            this.txtInpectX1.Top = 0F;
            this.txtInpectX1.Width = 0.8150001F;
            // 
            // txtInpectX2
            // 
            this.txtInpectX2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX2.DataField = "#2";
            this.txtInpectX2.Height = 0.301F;
            this.txtInpectX2.Left = 1.563F;
            this.txtInpectX2.Name = "txtInpectX2";
            this.txtInpectX2.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX2.Text = null;
            this.txtInpectX2.Top = 0F;
            this.txtInpectX2.Width = 0.815F;
            // 
            // txtInpectX3
            // 
            this.txtInpectX3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX3.DataField = "#3";
            this.txtInpectX3.Height = 0.301F;
            this.txtInpectX3.Left = 2.378F;
            this.txtInpectX3.Name = "txtInpectX3";
            this.txtInpectX3.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX3.Text = null;
            this.txtInpectX3.Top = 0F;
            this.txtInpectX3.Width = 0.815F;
            // 
            // txtInpectX4
            // 
            this.txtInpectX4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX4.DataField = "#4";
            this.txtInpectX4.Height = 0.301F;
            this.txtInpectX4.Left = 3.193F;
            this.txtInpectX4.Name = "txtInpectX4";
            this.txtInpectX4.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX4.Text = null;
            this.txtInpectX4.Top = 0F;
            this.txtInpectX4.Width = 0.815F;
            // 
            // txtInpectX5
            // 
            this.txtInpectX5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX5.DataField = "#5";
            this.txtInpectX5.Height = 0.301F;
            this.txtInpectX5.Left = 4.008F;
            this.txtInpectX5.Name = "txtInpectX5";
            this.txtInpectX5.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX5.Text = null;
            this.txtInpectX5.Top = 0F;
            this.txtInpectX5.Width = 0.8269992F;
            // 
            // txtInpectX6
            // 
            this.txtInpectX6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX6.DataField = "#6";
            this.txtInpectX6.Height = 0.301F;
            this.txtInpectX6.Left = 4.835F;
            this.txtInpectX6.Name = "txtInpectX6";
            this.txtInpectX6.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX6.Text = null;
            this.txtInpectX6.Top = 0F;
            this.txtInpectX6.Width = 0.8110003F;
            // 
            // txtInpectX7
            // 
            this.txtInpectX7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX7.DataField = "#7";
            this.txtInpectX7.Height = 0.301F;
            this.txtInpectX7.Left = 5.646F;
            this.txtInpectX7.Name = "txtInpectX7";
            this.txtInpectX7.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX7.Text = null;
            this.txtInpectX7.Top = 0F;
            this.txtInpectX7.Width = 0.8110003F;
            // 
            // txtInpectX8
            // 
            this.txtInpectX8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInpectX8.DataField = "#8";
            this.txtInpectX8.Height = 0.301F;
            this.txtInpectX8.Left = 6.457F;
            this.txtInpectX8.Name = "txtInpectX8";
            this.txtInpectX8.Style = "font-size: 8pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInpectX8.Text = null;
            this.txtInpectX8.Top = 0F;
            this.txtInpectX8.Width = 0.815F;
            // 
            // txtTopX8
            // 
            this.txtTopX8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtTopX8.Height = 0.645F;
            this.txtTopX8.Left = 6.457F;
            this.txtTopX8.Name = "txtTopX8";
            this.txtTopX8.Style = "font-size: 8pt; font-weight: bold; text-align: center; text-justify: distribute-a" +
                "ll-lines; vertical-align: middle";
            this.txtTopX8.Text = null;
            this.txtTopX8.Top = 0.231F;
            this.txtTopX8.Width = 0.815F;
            // 
            // groupHeader
            // 
            this.groupHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtTopTitle,
            this.lbllTitle,
            this.txtTopX1,
            this.txtTopX2,
            this.txtTopX3,
            this.txtTopX4,
            this.txtTopX5,
            this.txtTopX6,
            this.txtTopX7,
            this.txtTopX8});
            this.groupHeader.Height = 0.876F;
            this.groupHeader.Name = "groupHeader";
            // 
            // groupFooter
            // 
            this.groupFooter.Height = 0F;
            this.groupFooter.Name = "groupFooter";
            // 
            // rptINSZ0002_S02
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.272F;
            this.Sections.Add(this.groupHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lbllTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInpectX8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTopX8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.Label lbllTitle;
        private DataDynamics.ActiveReports.TextBox txtTitle;
        private DataDynamics.ActiveReports.TextBox txtInpectX1;
        private DataDynamics.ActiveReports.TextBox txtInpectX2;
        private DataDynamics.ActiveReports.TextBox txtInpectX3;
        private DataDynamics.ActiveReports.TextBox txtInpectX4;
        private DataDynamics.ActiveReports.TextBox txtInpectX5;
        private DataDynamics.ActiveReports.TextBox txtInpectX6;
        private DataDynamics.ActiveReports.TextBox txtInpectX7;
        private DataDynamics.ActiveReports.TextBox txtTopTitle;
        private DataDynamics.ActiveReports.TextBox txtTopX1;
        private DataDynamics.ActiveReports.TextBox txtTopX2;
        private DataDynamics.ActiveReports.TextBox txtTopX3;
        private DataDynamics.ActiveReports.TextBox txtTopX4;
        private DataDynamics.ActiveReports.TextBox txtTopX5;
        private DataDynamics.ActiveReports.TextBox txtTopX6;
        private DataDynamics.ActiveReports.TextBox txtTopX7;
        private DataDynamics.ActiveReports.TextBox txtTopX8;
        private DataDynamics.ActiveReports.TextBox txtInpectX8;
        private DataDynamics.ActiveReports.GroupHeader groupHeader;
        private DataDynamics.ActiveReports.GroupFooter groupFooter;
    }
}
