﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질검사관리                                          */
/* 모듈(분류)명 : 공정 Low Yield List                                   */
/* 프로그램ID   : frmINSZ0013.cs                                        */
/* 프로그램명   : 공정 Low Yield List                                   */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-12                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


using System.Collections;
using System.IO;

using QRPMAS.BL;


namespace QRPINS.UI
{
    public partial class frmINSZ0013 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 			// 전역변수

        private string strRtnUserPassword = "";

        public frmINSZ0013()
        {
            InitializeComponent();
        }

        private void frmINSZ0013_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0013_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;

            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            //타이틀설정
            titleArea.mfSetLabelText("공정 Low Yield List", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Control 초기화 Method
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            initValue();
            initButton();
            InitEtc();

            SetRunMode(); //디버깅용
            SetToolAuth();
        }

        private void frmINSZ0013_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmINSZ0013_Resize(object sender, EventArgs e)
        {
            ////try
            ////{
            ////    if (this.Width > 1070)
            ////    {
            ////        uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
            ////    }
            ////    else
            ////    {
            ////        uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            ////    }

            ////}
            ////catch (System.Exception ex)
            ////{
            ////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////    frmErr.ShowDialog();
            ////}
            ////finally
            ////{
            ////}
        }

        /// <summary>
        /// 디버깅위한 매서드
        /// </summary>
        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #region 컨트롤 초기화 Method
        private void InitEtc()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextLotQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextProcessYield.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                this.uTextAriseCause.MaxLength = 200;
                this.uTextMeasureDesc.MaxLength = 200;
                this.uTextReturnComment.MaxLength = 200;
                this.uTextComfirmComment.MaxLength = 200;
                this.uTextWorkUserID.MaxLength = 20;
                this.uTextRegistUserID.MaxLength = 20;

                //Label & Textbox Invisible
                this.uTextLossAmount.Visible = false;
                this.uTextLossName.Visible = false;

                this.uDateOccurFromDate.MaskInput = "yyyy-mm-dd";
                this.uDateOccurToDate.MaskInput   = "yyyy-mm-dd";

                this.uDateOccurFromDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                this.uDateOccurToDate.Value = DateTime.Now.ToString("yyyy-MM-dd");

                // 로그인유저의 부서가 품질팀일때만 평가조건 Display
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCom);
                DataTable dtQualityDept = clsUserCom.mfReadUserCommonCode("QUA", "S0003", m_resSys.GetString("SYS_LANG"));
                if (dtQualityDept.Rows.Count > 0)
                {
                    string strDeptCheck = string.Empty;
                    var varQualityDept = from getRow in dtQualityDept.AsEnumerable()
                                         //where getRow["ComCode"].ToString() != string.Empty//m_resSys.GetString("SYS_DEPTCODE")
                                         select getRow["ComCode"].ToString();

                    if (varQualityDept.Contains(m_resSys.GetString("SYS_DEPTCODE")))
                        uGroupBoxAppraiseCondition.Visible = true;
                    else
                        uGroupBoxAppraiseCondition.Visible = false;


                }
                else
                {
                    uGroupBoxAppraiseCondition.Visible = false;
                }

                this.uCheckLotRelease.CheckedChanged += new EventHandler(uCheckLotRelease_CheckedChanged);
                this.uCheckReturn.CheckedChanged += new EventHandler(uCheckReturn_CheckedChanged);

                this.uTextAttachFile.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextAttachFile_EditorButtonClick);
                this.uTextRegistUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextRegistUserID_EditorButtonClick);
                this.uTextRegistUserID.KeyDown += new KeyEventHandler(uTextRegistUserID_KeyDown);
                this.uTextSearchProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSearchProductCode_EditorButtonClick);
                this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextWriteUserID_EditorButtonClick);
                this.uTextWriteUserID.KeyDown += new KeyEventHandler(uTextWriteUserID_KeyDown);
                this.uTextTechnRegistUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextTechnRegistUserID_EditorButtonClick);
                this.uTextTechnRegistUserID.KeyDown += new KeyEventHandler(uTextTechnRegistUserID_KeyDown);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        private void initValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextPlantCode.Clear();
                this.uTextPlantName.Clear();
                this.uTextLotNo.Clear();
                this.uTextCustomerCode.Clear();
                this.uTextCustomerName.Clear();
                this.uTextPackage.Clear();
                this.uTextCustomerProductSpec.Clear();
                this.uTextEquipCode.Clear();
                this.uTextEquipName.Clear();
                this.uTextAriseProcessCode.Clear();
                this.uTextAriseProcessName.Clear();
                this.uTextBinName.Clear();
                this.uTextWorkUserID.Clear();
                this.uTextWorkUserName.Clear();
                this.uTextAriseDate.Clear();
                this.uTextAriseTime.Clear();
                this.uTextLotQty.Clear();
                this.uTextProcessYield.Clear();

                this.uTextTechnAriseCause.Clear();
                this.uTextTechnMeasureDesc.Clear();
                this.uTextTechnRegistUserID.Clear();
                this.uTextTechnRegistUserName.Clear();

                this.uTextAriseCause.Clear();
                this.uTextMeasureDesc.Clear();
                this.uTextAttachFile.Clear();
                this.uTextRegistUserID.Clear();
                this.uTextRegistUserName.Clear();
                this.uCheckFir4MEnviroFlag.Checked = false;
                this.uCheckFir4MMachineFlag.Checked = false;
                this.uCheckFir4MManFlag.Checked = false;
                this.uCheckFir4MMaterialFlag.Checked = false;
                this.uCheckFir4MMethodFlag.Checked = false;

                this.uCheckReturn.Checked = false;
                this.uCheckConfirm.Checked = false;
                this.uCheckLotRelease.Checked = false;
                this.uCheckScrapFlag.Checked = false;
                this.uTextReturnComment.Clear();
                this.uTextComfirmComment.Clear();
                this.uTextLotReleaseReason.Clear();
                this.uTextWriteUserID.Clear();
                this.uTextWriteUserName.Clear();

                while (this.uGridProcLowD.Rows.Count > 0)
                {
                    this.uGridProcLowD.Rows[0].Delete(false);
                }

                this.uTextProductCode.Clear();
                this.uTextProductName.Clear();
                this.uTextReleaseComplete.Text = "F";
                this.uTextMESReleaseTFlag.Text = "F";
                this.uTextMESTFlag.Text = "F";

                this.uCheckLotRelease.Enabled = true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxAppraiseCondition, GroupBoxType.INFO, "평가조건", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxForGrid, GroupBoxType.INFO, "Loss", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.ultraGroupBox2, GroupBoxType.INFO, "생산 검사", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxTechn, GroupBoxType.INFO, "기술 검사", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBoxAppraiseCondition.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxAppraiseCondition.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxForGrid.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxForGrid.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxTechn.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBoxTechn.HeaderAppearance.FontData.SizeInPoints = 9;

                this.ultraGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.ultraGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
                // ContentsGroupBox 접힌 상태
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void initButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonAddData, "데이터저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Zoom);
                wButton.mfSetButton(this.uButtonDown, "Download", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);

                this.uButtonAddData.Click += new EventHandler(uButtonAddData_Click);
                this.uButtonDown.Click += new EventHandler(uButtonDown_Click);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchOccurDate, "발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSeachLotRelease, "LotRelease여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNO", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAriseProcess, "발생공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelOccurDate, "발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelBinName, "BinName", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipCode, "설 비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotQty, "Lot 수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessYield, "공정수율", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //wLabel.mfSetLabel(this.ulabelAriseCause, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelAriseCause, "发生原因", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //wLabel.mfSetLabel(this.ulabelMeasureDesc, "대책사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelMeasureDesc, "临时措施", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWorkUser, "작업자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRegistUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //wLabel.mfSetLabel(this.ulabelTechnAriseCause, "발생원인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelTechnAriseCause, "预防再发生措施", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //wLabel.mfSetLabel(this.ulabelTechnMeasureDesc, "대책사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelTechnMeasureDesc, "已生产材料措施", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTechnRegistUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelReturn, "Return", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReturnComment, "Return Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelConfirm, "Confirm", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelConfirmComment, "Confirm Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotRelease, "Lot Release 여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotReleaseReason, "Realease 사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelScrapFlag, "Scrap 여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ultraLabel2, "密码", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelImputeProcess, "归责工程", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelImputeEquip, "归责设备", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelImputeUser, "归责人员", m_resSys.GetString("SYS_FONTNAME"), true, false); 
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                //LotRelease 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clscommon = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clscommon);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                DataTable dtLotR = clscommon.mfReadCommonCode("C0056", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchLotRelease, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtLotR);

                // 고객사 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                DataTable dtCustomerCombo = new DataTable();
                if (dtCustomer.Rows.Count > 0)
                    dtCustomerCombo = dtCustomer.DefaultView.ToTable(true, "CustomerCode", "CustomerName");

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "전체", "CustomerCode", "CustomerName", dtCustomerCombo);

                //검색조건 - Package 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                                       , "Package", "ComboName", dtPackage);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process"); 
                QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process(); 
                brwChannel.mfCredentials(clsProc); 
                DataTable dtProc = clsProc.mfReadProcessDetailProcessOperationType(m_resSys.GetString("SYS_PLANTCODE"));
                wCombo.mfSetComboEditor(this.uComboImputeProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ""
                   , "DetailProcessOperationType", "ComboName", dtProc);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회결과 그리드
                wGrid.mfInitGeneralGrid(this.uGridRowYieldList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "NoticeCustomer", "是否通报客户", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "AriseProcessCode", "工程code", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "AriseProcessName", "工程名", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "AriseDate", "发生日期", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "AriseTime", "발생시간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "CustomerName", "客户社", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "EquipCode", "设备号码", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "LossName", "Loss名", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");  // 2012-12-03 LOSS명 추가 (LOSS수량이 제일 큰 LOSS명)

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "AriseCause", "发生原因", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");  // 2012-12-03 발생원인

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "MeasureDesc", "对策事项", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");  // 2012-12-03 대책사항

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "LotQty", "Lot数量", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "LossQty", "Loss数量", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "ProcessYield", "工程收益率", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnn.nnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "CurrentStatus", "状态", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "ManFlag", "Man", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "MethodFlag", "Method", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "MaterialFlag", "Material", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "MachineFlag", "Machine", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "EnvironmentFlag", "Environment", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "LotReleaseFlag", "LotRelease여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "MESTFlag", "MES전송여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "MESReleaseTFlag", "MESRelease전송여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "LotType", "LotType", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "ImputeProcess", "归责工程", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "ImputeEquipCode", "归责设备", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "ImputeUserID", "归责人员ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridRowYieldList, 0, "ImputeUserName", "归责人员", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                // 그리드 편집불가 상태로
                this.uGridRowYieldList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                #endregion

                #region 상세 그리드
                wGrid.mfInitGeneralGrid(this.uGridProcLowD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                   , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "AriseProcessCode", "발생공정", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "AriseDate", "발생일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "AriseTime", "발생시간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "LossCode", "LossCode", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "LossName", "Loss명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "LossQty", "Loss수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn,nnn,nnn", "");

                this.uGridProcLowD.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                #endregion

                // MaskInput 설정
                this.uGridRowYieldList.DisplayLayout.Bands[0].Columns["LotQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridRowYieldList.DisplayLayout.Bands[0].Columns["LossQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridProcLowD.DisplayLayout.Bands[0].Columns["LossQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                // Set FontSize
                this.uGridRowYieldList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridRowYieldList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridProcLowD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcLowD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridRowYieldList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridRowYieldList_DoubleClickRow);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPINS.BL.INSSTS.INSProcLowYield ProcLow;

                //디버깅모드인지 아닌지에 따라 생성자가 붙는다.
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                    ProcLow = new QRPINS.BL.INSSTS.INSProcLowYield();
                    brwChannel.mfCredentials(ProcLow);
                }
                else
                {
                    ProcLow = new QRPINS.BL.INSSTS.INSProcLowYield(m_strDBConn);
                }

                String strPlantCode = this.uComboSearchPlant.Value.ToString();                
                String strAriseDateFrom = Convert.ToDateTime(this.uDateOccurFromDate.Value).ToString("yyyy-MM-dd");
                String strAriseDateTo = Convert.ToDateTime(this.uDateOccurToDate.Value).ToString("yyyy-MM-dd");
                String strProductCode = this.uTextSearchProductCode.Text;
                String strLotReleaseFlag = this.uComboSearchLotRelease.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strCustomer = this.uComboSearchCustomer.Value.ToString();
                string strLotNO = this.uTextSearchLotNo.Text.Trim();

                DataTable dt = ProcLow.mfReadINSProcLowYield(strPlantCode, strAriseDateFrom, strAriseDateTo, strProductCode
                                                            , strLotReleaseFlag, strPackage, strCustomer,strLotNO, m_resSys.GetString("SYS_LANG"));
                

                this.uGridRowYieldList.DataSource = dt;
                this.uGridRowYieldList.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    for (int i = 0; i < this.uGridRowYieldList.Rows.Count; i++)
                    {
                        //Release,MESTFlag 두가지 모두 성공한 정보가 아니면 줄 색변경
                        if (!this.uGridRowYieldList.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T") 
                            && !this.uGridRowYieldList.Rows[i].Cells["MESTFlag"].Value.ToString().Equals("T"))
                        {
                            this.uGridRowYieldList.Rows[i].Appearance.BackColor = Color.Salmon;
                        }

                        //if (this.uGridRowYieldList.Rows[i].Cells["LotReleaseFlag"].Value.ToString().Equals("T"))
                        //{
                        //    this.uGridRowYieldList.Rows[i].Cells["Status"].Value = "완료";
                        //}
                        //else if (this.uGridRowYieldList.Rows[i].Cells["ReturnFlag"].Value.ToString().Equals("T"))
                        //{
                        //    this.uGridRowYieldList.Rows[i].Cells["Status"].Value = "Return";
                        //}
                        //else if (!this.uGridRowYieldList.Rows[i].Cells["AriseCause"].Value.ToString().Equals("")
                        //            || !this.uGridRowYieldList.Rows[i].Cells["MeasureDesc"].Value.ToString().Equals(""))
                        //{
                        //    this.uGridRowYieldList.Rows[i].Cells["Status"].Value = "원인대책등록";
                        //}
                        //else
                        //{
                        //    this.uGridRowYieldList.Rows[i].Cells["Status"].Value = "발생";
                        //}

                    }
                    if (this.uGroupBoxContentsArea.Expanded == true)
                    {
                        this.uGroupBoxContentsArea.Expanded = false;
                    }

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridRowYieldList, 0);
                }

                //MESTFlag
                //this.uTextMESTFlag.Clear();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                    , "M000397"
                                                    , Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (this.uTextPlantCode.Text.Equals(string.Empty) || this.uTextAriseProcessCode.Text.Equals(string.Empty)
                        || this.uTextAriseDate.Text.Equals(string.Empty) || this.uTextAriseTime.Text.Equals(string.Empty))
                {
                    // 기본키 입력확인
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                    , "M000397"
                                                    , Infragistics.Win.HAlign.Right);
                    return;
                }
                else if(this.uTextTechnAriseCause.Text.Equals(string.Empty) || this.uTextTechnMeasureDesc.Text.Equals(string.Empty)
                        || this.uTextTechnRegistUserID.Value.ToString() == string.Empty || this.uComboImputeProcess.Text.Equals(string.Empty)
                        || this.uTextImputeEquipName.Text.Equals(string.Empty) || this.uTextImputeUserName.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                 , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                 , "M001563"
                                                 , Infragistics.Win.HAlign.Right);
                    return;
                }

                else if (this.uComboImputeProcess.FindString(this.uComboImputeProcess.Text) == -1)
                {

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                 , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                 , "M001467"
                                                 , Infragistics.Win.HAlign.Right);
                    this.uComboImputeProcess.Text = string.Empty;
                    this.uComboImputeProcess.DropDown();
                    return;
                }
                
                else if (this.uTextMESTFlag.Text.Equals("T") && this.uTextReleaseComplete.Text.Equals("T") && this.uTextMESReleaseTFlag.Text.Equals("T"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000825", "M001029"
                                                    , "M000063"
                                                    , Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    if (this.uTextReleaseComplete.Text.Equals("T") && !this.uTextMESTFlag.Text.Equals("T") && !this.uTextMESReleaseTFlag.Text.Equals("T"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001053"
                                            , "M000065"
                                            , Infragistics.Win.HAlign.Right);

                        if (Result == DialogResult.Yes)
                        {
                            // MES I/F 메소드 호출
                            Send_MESInterFace();
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (!this.uTextReleaseComplete.Text.Equals("T") && this.uCheckLotRelease.Checked)
                        {
                            if (this.uTextPW.Text.Trim() == string.Empty)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                , "M001342"
                                                , Infragistics.Win.HAlign.Right);
                                this.uTextPW.Focus();
                                return;
                            }
                            else 
                            {
                                checkUserPW();     
                            }

                            if (this.uTextPW.Text.Trim() != strRtnUserPassword)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                , "M001353"
                                                , Infragistics.Win.HAlign.Right);
                                this.uTextPW.Focus();
                                return;
                            }


                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000794", "M000979"
                                                    , "M000064"
                                                    , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right);
                        }

                        if (Result == DialogResult.Yes)
                        {
                            // 저장정보 저장
                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                            QRPINS.BL.INSSTS.INSProcLowYield clsProcLow = new QRPINS.BL.INSSTS.INSProcLowYield();
                            brwChannel.mfCredentials(clsProcLow);

                            DataTable dtProcLow = clsProcLow.mfSetDataInfo();
                            DataRow drRow = dtProcLow.NewRow();
                            drRow["PlantCode"] = this.uTextPlantCode.Text;
                            drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                            drRow["AriseDate"] = this.uTextAriseDate.Text;
                            drRow["AriseTime"] = this.uTextAriseTime.Text;
                            drRow["TechnAriseCause"] = this.uTextTechnAriseCause.Text;
                            drRow["TechnMeasureDesc"] = this.uTextTechnMeasureDesc.Text;
                            drRow["TechnRegistUserID"] = this.uTextTechnRegistUserID.Text;
                            drRow["AriseCause"] = this.uTextAriseCause.Text;
                            drRow["MeasureDesc"] = this.uTextMeasureDesc.Text;
                            if (this.uTextAttachFile.Text.Contains(":\\"))
                            {
                                FileInfo fileDoc = new FileInfo(this.uTextAttachFile.Text);
                                drRow["FilePath"] = this.uTextPlantCode.Text + "-" + this.uTextAriseProcessCode.Text + "-" +
                                    this.uTextAriseDate.Text + "-" + this.uTextAriseTime.Text.Replace(":", "") + "-" + fileDoc.Name;
                            }
                            else
                            {
                                drRow["FilePath"] = this.uTextAttachFile.Text;
                            }
                            drRow["RegistUserID"] = this.uTextRegistUserID.Text;
                            drRow["ReturnFlag"] = this.uCheckReturn.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["ReturnComment"] = this.uTextReturnComment.Text;
                            drRow["ConfirmFlag"] = this.uCheckConfirm.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["ConfirmDesc"] = this.uTextComfirmComment.Text;
                            drRow["LotReleaseFlag"] = this.uCheckLotRelease.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["ReleaseDesc"] = this.uTextLotReleaseReason.Text;
                            drRow["ScrapFlag"] = this.uCheckScrapFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                            drRow["ManFlag"] = this.uCheckFir4MManFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["MethodFlag"] = this.uCheckFir4MMethodFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["MaterialFlag"] = this.uCheckFir4MMaterialFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["MachineFlag"] = this.uCheckFir4MMachineFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            drRow["EnvironmentFlag"] = this.uCheckFir4MEnviroFlag.Checked.ToString().ToUpper().Substring(0, 1);

                            drRow["ImputeProcess"] = this.uComboImputeProcess.Text;
                            drRow["ImputeEquipCode"] = this.uTextImputeEquipCode.Text;
                            drRow["ImputeUserID"] = this.uTextImputeUserID.Text;

                            dtProcLow.Rows.Add(drRow);

                            // 저장 메소드 호출
                            string strErrRtn = clsProcLow.mfSaveINSProcLowYield(dtProcLow, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            // 결과검사
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum.Equals(0))
                            {
                                // 파일 업로드 메소드 호출
                                FileUpload();
                                // MES I/F 메소드 호출
                                Send_MESInterFace();

                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000954", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                if (ErrRtn.ErrMessage.Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                                  , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000953"
                                                                  , Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                                  , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                  , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                                  , ErrRtn.ErrMessage
                                                                  , Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                }

                #region 기존소스 주석처리
                /*
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();


                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    QRPINS.BL.INSSTS.INSProcLowYield LowYield;
                    //디버깅모드인지 아닌지에 따라 생성자가 붙는다.
                    if (m_bolDebugMode == false)
                    {
                        //ProcLowYield BL 호출

                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                        LowYield = new QRPINS.BL.INSSTS.INSProcLowYield();
                        brwChannel.mfCredentials(LowYield);

                    }
                    else
                    {
                        LowYield = new QRPINS.BL.INSSTS.INSProcLowYield(m_strDBConn);
                    }

                    //파일 저장 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                    DataTable dtProc = LowYield.mfSetDataInfo();


                    DataRow dr = dtProc.NewRow();
                    dr = dtProc.NewRow();
                    dr["PlantCode"] = this.uTextPlantCode.Text;
                    dr["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                    dr["AriseDate"] = this.uTextAriseDate.Text;
                    dr["AriseTime"] = this.uTextAriseTime.Text;
                    dr["ProductCode"] = this.uTextProductCode.Text;
                    dr["LossName"] = this.uTextLossName.Text;
                    dr["LossQty"] = this.uTextLotQty.Text;
                    dr["LotQty"] = this.uTextLotQty.Text;
                    dr["ProcessYield"] = this.uTextProcessYield.Text;
                    dr["AriseCause"] = this.uTextAriseCause.Text;
                    dr["MeasureDesc"] = this.uTextMeasureDesc.Text;
                    if (this.uTextAttachFile.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextAttachFile.Text);
                        dr["FileName"] = this.uTextPlantCode.Text + "-" + this.uTextAriseProcessCode.Text + "-" +
                            this.uTextAriseDate.Text + "-" + this.uTextAriseTime.Text.Replace(":", "") + "-" + fileDoc.Name;
                    }
                    else
                    {
                        dr["FileName"] = this.uTextAttachFile.Text;
                    }
                    dr["ReturnFlag"] = this.uCheckReturn.CheckedValue.ToString();
                    if (Convert.ToBoolean(this.uCheckReturn.CheckedValue) == false)
                    {
                        dr["ReturnComment"] = "";
                    }
                    else
                    {
                        dr["ReturnComment"] = this.uTextReturnComment.Text;
                    }
                    dr["ConfirmFlag"] = this.uCheckConfirm.CheckedValue.ToString();
                    if (Convert.ToBoolean(this.uCheckConfirm.CheckedValue) == false)
                    {
                        dr["ConfirmDesc"] = "";
                    }
                    else
                    {
                        dr["ConfirmDesc"] = this.uTextComfirmComment.Text;
                    }
                    dr["LotReleaseFlag"] = this.uCheckLotRelease.CheckedValue.ToString();
                    if (Convert.ToBoolean(this.uCheckLotRelease.CheckedValue) == false)
                    {
                        dr["ReleaseDesc"] = "";
                    }
                    else
                    {
                        dr["ReleaseDesc"] = this.uTextLotReleaseReason.Text;
                    }
                    if (this.uCheckScrapFlag.Checked == true)
                    {
                        dr["ScrapFlag"] = "T";
                    }
                    else
                    {
                        dr["ScrapFlag"] = "F";
                    }
                    dr["WorkUserID"] = this.uTextWorkUserID.Text;
                    dr["LotNo"] = this.uTextLotNo.Text;
                    dr["EquipCode"] = this.uTextEquipCode.Text;
                    dr["BinName"] = this.uTextBinName.Text;
                    dr["WriteUserID"] = this.uTextWriteUserID.Text;
                    ////dr["MESTFlag"] = this.uTextMESTFlag.Text;
                    ////dr["ReleaseComplete"] = this.uTextReleaseComplete.Text;
                    dtProc.Rows.Add(dr);

                    //Item
                    DataTable dtDelProc = ProcLowD.mfSetDataInfo();
                    DataTable dtSaveProc = ProcLowD.mfSetDataInfo();

                    #region MES 전송데이터 저장

                    #region MES전송데이터 컬럼설정
                    //MES 전송데이터

                    DataTable dtMESInfo = new DataTable();
                    dtMESInfo.Columns.Add("WorkUserID", typeof(string)); //UserID
                    dtMESInfo.Columns.Add("EquipCode", typeof(string));     //EquipCode
                    dtMESInfo.Columns.Add("ScrapFlag", typeof(string));     //SCRAPFLAG
                    dtMESInfo.Columns.Add("LotNo", typeof(string));     // LotNo
                    dtMESInfo.Columns.Add("Process", typeof(string));   //공정

                    //dtLoss(불량코드SCRAPCODE, 불량수량SCRAPQTY)
                    DataTable dtLoss = new DataTable();
                    dtLoss.Columns.Add("SCRAPCODE", typeof(string));
                    dtLoss.Columns.Add("SCRAPQTY", typeof(string));

                    #endregion


                    dr = dtMESInfo.NewRow();

                    dr["WorkUserID"] = this.uTextWriteUserID.Text;
                    dr["EquipCode"] = this.uTextEquipCode.Text;
                    if (this.uCheckScrapFlag.Checked.ToString().Substring(0, 1).ToUpper().Equals("T"))
                    {
                        dr["ScrapFlag"] = "Y";
                    }
                    else
                    {
                        dr["ScrapFlag"] = "N";
                    }

                    dr["LotNo"] = this.uTextLotNo.Text;
                    dr["Process"] = this.uTextAriseProcessCode.Text;

                    dtMESInfo.Rows.Add(dr);


                    for (int i = 0; i < this.uGridProcLowD.Rows.Count; i++)
                    {
                        this.uGridProcLowD.ActiveCell = this.uGridProcLowD.Rows[0].Cells[0];
                        if (this.uGridProcLowD.Rows[i].Hidden == false)
                        {
                            //dr = dtSaveProc.NewRow();
                            //dr["PlantCode"] = this.uGrid2.ActiveRow.Cells["PlantCode"].ToString();
                            //dr["AriseProcessCode"] = this.uGrid2.ActiveRow.Cells["AriseProcessCode"].ToString();
                            //dr["AriseDate"] = this.uGrid2.ActiveRow.Cells["AriseDate"].ToString();
                            //dr["AriseTime"] = this.uGrid2.ActiveRow.Cells["AriseTime"].ToString();
                            //dr["Seq"] = this.uGrid2.ActiveRow.Cells["Seq"].ToString();
                            //dr["LossName"] = this.uGrid2.ActiveRow.Cells["LossName"].ToString();
                            //dr["LossQty"] = this.uGrid2.ActiveRow.Cells["LossQty"].ToString();

                            //dtSaveProc.Rows.Add(dr);

                            dr = dtLoss.NewRow();
                            dr["SCRAPCODE"] = this.uGridProcLowD.Rows[i].Cells["LossName"].Value.ToString();
                            dr["SCRAPQTY"] = this.uGridProcLowD.Rows[i].Cells["LossQty"].Value.ToString();
                            dtLoss.Rows.Add(dr);
                        }
                        else
                        {
                            //dr = dtDelProc.NewRow();
                            //dr["PlantCode"] = this.uGrid2.ActiveRow.Cells["PlantCode"].ToString();
                            //dr["AriseProcessCode"] = this.uGrid2.ActiveRow.Cells["AriseProcessCode"].ToString();
                            //dr["AriseDate"] = this.uGrid2.ActiveRow.Cells["ArisDate"].ToString();
                            //dr["AriseTime"] = this.uGrid2.ActiveRow.Cells["AriseTime"].ToString();
                            //dr["Seq"] = this.uGrid2.ActiveRow.Cells["Seq"].ToString();

                            //dtDelProc.Rows.Add(dr);
                        }
                    }

                    ////추가LotNo 저장
                    //dr =  dtLoss.NewRow();
                    //dr["SCRAPCODE"] = this.uTextLotNo.Text;
                    //dtLoss.Rows.Add(dr);

                    #endregion

                    if (dtProc.Rows.Count > 0)
                    {

                        if (dtProc.Rows[0]["LotReleaseFlag"].ToString().Equals("True") && dtProc.Rows[0]["MESTFlag"].ToString().Equals("T"))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "확인창", "저장확인", "Lot Release 완료,MES전송이 완료된 데이터는 수정이 불가합니다.", Infragistics.Win.HAlign.Right);

                            return;
                        }

                        DialogResult dirResult = new DialogResult();

                        if (dtProc.Rows[0]["LotReleaseFlag"].ToString().Equals("True") && dtProc.Rows[0]["MESTFlag"].ToString().Equals("F"))
                        {
                            dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "확인창", "저장확인", "Lot Release여부가 완료되어 수정이 불가합니다. MES처리 하시겠습니까?", Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "확인창", "저장확인", "입력한 정보를 저장하겠습니까?", Infragistics.Win.HAlign.Right);
                        }


                        if (dirResult == DialogResult.Yes)
                        {
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            TransErrRtn ErrRtn = new TransErrRtn();

                            String strSaveProc = LowYield.mfSaveINSProcLowYield(dtProc, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), this.Name, dtLoss, dtMESInfo);

                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strSaveProc);

                            DialogResult DResult = new DialogResult();

                            if (ErrRtn.ErrNum == 0)
                            {
                                if (this.uTextAttachFile.Text.Contains(":\\"))
                                {
                                    //File1 Upload
                                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                    ArrayList arrFile = new ArrayList();

                                    FileInfo fileDoc = new FileInfo(this.uTextAttachFile.Text);
                                    string strUploadFile = fileDoc.DirectoryName + "\\" + this.uTextPlantCode.Text + "-"
                                                            + this.uTextAriseProcessCode.Text + "-" + this.uTextAriseDate.Text + "-"
                                                            + this.uTextAriseTime.Text.Replace(":", "") + "-" + fileDoc.Name;

                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);

                                    File.Copy(this.uTextAttachFile.Text, strUploadFile);
                                    arrFile.Add(strUploadFile);

                                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                    fileAtt.ShowDialog();
                                }

                                //Lot Release 여부가 완료되었고 MES 처리정보가 F일경우 MES처리정보
                                if (dtProc.Rows[0]["LotReleaseFlag"].ToString().Equals("True") && dtProc.Rows[0]["MESTFlag"].ToString().Equals("F"))
                                {
                                    if (!ErrRtn.InterfaceResultCode.Equals("0"))
                                    {
                                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                        {
                                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "처리결과", "MES처리결과", "입력한 정보를 저장하였으나 MES 전송하지 못했습니다. MES처리를 원하시면 다시 저장해주세요.", Infragistics.Win.HAlign.Right);

                                        }
                                        else
                                        {
                                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "처리결과", "MES처리결과", ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                        }
                                    }
                                    else
                                    {
                                        DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                   , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다."
                                                   , Infragistics.Win.HAlign.Right);
                                    }
                                }
                                else
                                {

                                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다."
                                            , Infragistics.Win.HAlign.Right);
                                }
                                initValue();
                            }
                            else
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                              , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다"
                              , Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult DResult = new DialogResult();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "처리결과", "저장처리결과", "저장시 필수입력사항을 작성해주세요."
                                                    , Infragistics.Win.HAlign.Right);
                }
                 */
                #endregion
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                initValue();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridRowYieldList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();

                    wGrid.mfDownLoadGridToExcel(this.uGridRowYieldList);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 사용자명 조회 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                    strRtnUserPassword = dtUser.Rows[0]["UserPassword"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        private void FileUpload()
        {
            try
            {
                if (this.uTextAttachFile.Text.Contains(":\\"))
                {
                    //File Upload
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    FileInfo fileDoc = new FileInfo(this.uTextAttachFile.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" + this.uTextPlantCode.Text + "-"
                                            + this.uTextAriseProcessCode.Text + "-" + this.uTextAriseDate.Text + "-"
                                            + this.uTextAriseTime.Text.Replace(":", "") + "-" + fileDoc.Name;

                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);

                    File.Copy(this.uTextAttachFile.Text, strUploadFile);
                    arrFile.Add(strUploadFile);

                    if (arrFile.Count > 0)
                    {
                        //화일서버 연결정보 가져오기
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                        fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Send_MESInterFace()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                bool bolSaveCheck = true;
                TransErrRtn ErrRtn = new TransErrRtn();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uCheckLotRelease.Checked || this.uCheckScrapFlag.Checked)
                {

                    //////// 화일서버 연결정보 가져오기
                    //////QRPBrowser brwChannel = new QRPBrowser();
                    //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    //////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    //////brwChannel.mfCredentials(clsSysAccess);
                    //////QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
                    //////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, clsMESCode.MesCode);
                    ////////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S04");
                    ////////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S07");

                    //////// MES I/F
                    //////QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);
                    //////DataTable dtLotInfo = clsTibrv.LOT_INFO_REQ(this.uTextLotNo.Text.ToUpper().Trim());
                    ////////clsTibrv.Dispose();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                    QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                    brwChannel.mfCredentials(clsMES);

                    DataTable dtLotInfo = clsMES.mfRead_LOT_INFO_REQ(this.uTextPlantCode.Text, this.uTextLotNo.Text.ToUpper().Trim());

                    if (dtLotInfo.Rows.Count > 0)
                    {
                        if (dtLotInfo.Rows[0]["returncode"].ToString().Trim().Equals("0"))
                        {
                            #region 결과전송 MES I/F
                            if (!this.uTextMESTFlag.Text.Equals("T") && this.uCheckScrapFlag.Checked)
                            {
                                #region 데이터 설정
                                DataRow drRow;
                                // 데이터 테이블 컬럼설정
                                DataTable dtMESInfo = new DataTable();
                                dtMESInfo.Columns.Add("PlantCode", typeof(string));
                                dtMESInfo.Columns.Add("AriseProcessCode", typeof(string));
                                dtMESInfo.Columns.Add("AriseDate", typeof(string));
                                dtMESInfo.Columns.Add("AriseTime", typeof(string));
                                dtMESInfo.Columns.Add("FormName", typeof(string));
                                dtMESInfo.Columns.Add("WorkUserID", typeof(string)); //UserID
                                dtMESInfo.Columns.Add("EquipCode", typeof(string));     //EquipCode
                                dtMESInfo.Columns.Add("ScrapFlag", typeof(string));     //SCRAPFLAG
                                dtMESInfo.Columns.Add("LotNo", typeof(string));     // LotNo

                                drRow = dtMESInfo.NewRow();
                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                                drRow["AriseDate"] = this.uTextAriseDate.Text;
                                drRow["AriseTime"] = this.uTextAriseTime.Text;
                                drRow["FormName"] = this.Name;
                                drRow["WorkUserID"] = this.uTextWorkUserID.Text;
                                drRow["EquipCode"] = this.uTextEquipCode.Text;
                                
                                ////// Scrap 여부 체크되고, Lot상태가 Hold 일때만 Y로 보낸다.
                                ////if (this.uCheckScrapFlag.Checked && dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString().Equals("OnHold"))
                                ////{
                                ////    drRow["ScrapFlag"] = "Y";
                                ////}
                                ////else
                                ////{
                                ////    drRow["ScrapFlag"] = "N";
                                ////}
                                // ScrapFlag 2012-11-07 Scrap 처리불가
                                drRow["ScrapFlag"] = "N";
                                drRow["LotNo"] = this.uTextLotNo.Text;
                                dtMESInfo.Rows.Add(drRow);

                                // dtLoss(불량코드SCRAPCODE, 불량수량SCRAPQTY)
                                DataTable dtScrapList = new DataTable();
                                dtScrapList.Columns.Add("SCRAPCODE", typeof(string));
                                dtScrapList.Columns.Add("SCRAPQTY", typeof(string));

                                for (int i = 0; i < this.uGridProcLowD.Rows.Count; i++)
                                {
                                    this.uGridProcLowD.ActiveCell = this.uGridProcLowD.Rows[0].Cells[0];
                                    {
                                        drRow = dtScrapList.NewRow();
                                        drRow["SCRAPCODE"] = this.uGridProcLowD.Rows[i].Cells["LossCode"].Value.ToString();
                                        drRow["SCRAPQTY"] = this.uGridProcLowD.Rows[i].Cells["LossQty"].Value.ToString();
                                        dtScrapList.Rows.Add(drRow);
                                    }
                                }
                                #endregion

                                // BL 연결
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                                QRPINS.BL.INSSTS.INSProcLowYield clsProcLow = new QRPINS.BL.INSSTS.INSProcLowYield();
                                brwChannel.mfCredentials(clsProcLow);

                                string strErrRtn = clsProcLow.mfSaveINSProcLowYield_MES_Result(dtMESInfo, dtScrapList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                                {
                                    bolSaveCheck = false;

                                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000095", "M000952", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                    }
                                    return;
                                }
                                else
                                {

                                }
                            }
                            #endregion

                            #region Lot Release MES I/F
                            //if (!this.uTextMESReleaseTFlag.Text.Equals("T") && this.uCheckLotRelease.Checked && dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString().Equals("OnHold"))
                            if (!this.uTextMESReleaseTFlag.Text.Equals("T") && this.uCheckLotRelease.Checked && !this.uCheckScrapFlag.Checked)
                            {
                                #region 데이터 설정
                                DataRow drRow;
                                // 데이터 테이블 컬럼설정
                                DataTable dtMESInfo = new DataTable();
                                dtMESInfo.Columns.Add("PlantCode", typeof(string));
                                dtMESInfo.Columns.Add("AriseProcessCode", typeof(string));
                                dtMESInfo.Columns.Add("AriseDate", typeof(string));
                                dtMESInfo.Columns.Add("AriseTime", typeof(string));
                                dtMESInfo.Columns.Add("FormName", typeof(string));
                                dtMESInfo.Columns.Add("WorkUserID", typeof(string));
                                dtMESInfo.Columns.Add("Comment", typeof(string));

                                drRow = dtMESInfo.NewRow();
                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                                drRow["AriseDate"] = this.uTextAriseDate.Text;
                                drRow["AriseTime"] = this.uTextAriseTime.Text;
                                drRow["FormName"] = this.Name;
                                drRow["WorkUserID"] = this.uTextWriteUserID.Text;// this.uTextWriteUserID.Text; // 2012-11-07 작업자 -> 완료자 ID
                                drRow["Comment"] = this.uTextLotReleaseReason.Text;
                                dtMESInfo.Rows.Add(drRow);

                                // LotList
                                DataTable dtLotList = new DataTable();
                                dtLotList.Columns.Add("LOTID", typeof(string));
                                dtLotList.Columns.Add("HOLDCODE", typeof(string));

                                drRow = dtLotList.NewRow();
                                drRow["LOTID"] = this.uTextLotNo.Text;
                                //drRow["HOLDCODE"] = "HL";
                                drRow["HOLDCODE"] = "GL04";
                                dtLotList.Rows.Add(drRow);
                                #endregion

                                // BL 연결
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                                QRPINS.BL.INSSTS.INSProcLowYield clsProcLow = new QRPINS.BL.INSSTS.INSProcLowYield();
                                brwChannel.mfCredentials(clsProcLow);

                                string strErrRtn = clsProcLow.mfSaveINSProcInspectReqLot_MESRelease(dtMESInfo, dtLotList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                                {
                                    bolSaveCheck = false;

                                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000095", "M000948", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                    }
                                    return;
                                }
                                else
                                {

                                }
                            }
                            #endregion
                        }
                        else
                        {
                            if (dtLotInfo.Rows[0]["returnmessage"].ToString().Trim().Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M000066", "M000059", "M000091", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M000066", strLang), msg.GetMessge_Text("M000059", strLang)
                                    , dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            }
                            return;
                        }
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M000066", "M000059", "M000091", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    if (bolSaveCheck)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001022", "M001023", "M000930", Infragistics.Win.HAlign.Right);

                        // 리스트 갱신
                        mfSearch();
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Events...
        // ContentsArea 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                ////if (this.uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    // ContentsGroupbox 위치 설정
                ////    Point point = new Point(0, 170);
                ////    this.uGroupBoxContentsArea.Location = point;

                ////    // 그리드 높이 조절
                ////    this.uGridRowYieldList.Height = 60;
                ////}
                ////else
                ////{
                ////    // ContentsGroupbox 위치 설정
                ////    Point point = new Point(0, 825);
                ////    this.uGroupBoxContentsArea.Location = point;

                ////    // 그리드 고정상태를 풀기위한 For문
                ////    for (int i = 0; i < this.uGridRowYieldList.Rows.Count; i++)
                ////    {
                ////        this.uGridRowYieldList.Rows[i].Fixed = false;
                ////    }

                ////    this.uGridRowYieldList.Height = 720;

                ////    // Control 초기화 Method 실행
                ////    initValue();
                ////}

                if (this.uGroupBoxContentsArea.Expanded)
                {
                    this.uGridRowYieldList.Rows.FixedRows.Clear();

                    initValue();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region 텍스트박스 이벤트
        // 검색조건 제품 텍스트 박스 버튼 이벤트
        private void uTextSearchProductCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uComboSearchPlant.Value = frmPOP.PlantCode;
                this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                this.uTextSearchProductName.Text = frmPOP.ProductName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 버튼 이벤트
        private void uTextAttachFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.Equals("UP"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|All Files|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strFile = openFile.FileName;
                        uTextAttachFile.Text = strFile;
                    }
                }
                else if (e.Button.Key.Equals("DOWN"))
                {
                    if (this.uTextAttachFile.Text.Contains(":\\") || this.uTextAttachFile.Text == "")
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000357",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextAttachFile.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileDownloadNoProgView();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextAttachFile.Text);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region 작성자 텍스트박스 이벤트
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uTextPlantCode.Text;
                frmUser.ShowDialog();

                this.uTextWriteUserID.Text = frmUser.UserID;
                this.uTextWriteUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextWriteUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uTextPlantCode.Text, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextWriteUserID.Text = "";
                            this.uTextWriteUserName.Text = "";
                        }

                        else
                        {
                            this.uTextWriteUserName.Text = strRtnUserName;
                            this.uTextPW.Focus();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteUserID.TextLength <= 1 || this.uTextWriteUserID.SelectedText == this.uTextWriteUserID.Text)
                    {
                        this.uTextWriteUserName.Text = "";
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion 
        #region 등록자 텍스트박스 이벤트
        private void uTextRegistUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextRegistUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextRegistUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uTextPlantCode.Text, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextRegistUserID.Text = "";
                            this.uTextRegistUserName.Text = "";
                        }

                        else
                        {
                            this.uTextRegistUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextRegistUserID.TextLength <= 1 || this.uTextRegistUserID.SelectedText == this.uTextRegistUserID.Text)
                    {
                        this.uTextRegistUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextRegistUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uTextPlantCode.Text;
                frmUser.ShowDialog();

                this.uTextRegistUserID.Text = frmUser.UserID;
                this.uTextRegistUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 등록자 텍스트박스 이벤트
        private void uTextTechnRegistUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextTechnRegistUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextTechnRegistUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uTextPlantCode.Text, strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextTechnRegistUserID.Text = "";
                            this.uTextTechnRegistUserName.Text = "";
                        }

                        else
                        {
                            this.uTextTechnRegistUserName.Text = strRtnUserName;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextTechnRegistUserID.TextLength <= 1 || this.uTextTechnRegistUserID.SelectedText == this.uTextTechnRegistUserID.Text)
                    {
                        this.uTextTechnRegistUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextTechnRegistUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uTextPlantCode.Text;
                frmUser.ShowDialog();

                this.uTextTechnRegistUserID.Text = frmUser.UserID;
                this.uTextTechnRegistUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #endregion


        #endregion

        private void uGridRowYieldList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                e.Row.Fixed = true;
                this.uGroupBoxContentsArea.Expanded = true;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextMESTFlag.Text = e.Row.Cells["MESTFlag"].Value.ToString(); //MESTFlag
                this.uTextReleaseComplete.Text = e.Row.Cells["LotReleaseFlag"].Value.ToString(); //ReleaseFlag
                this.uTextMESReleaseTFlag.Text = e.Row.Cells["MESReleaseTFlag"].Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPINS.BL.INSSTS.INSProcLowYield ProcLow;
                QRPINS.BL.INSSTS.INSProcLowYieldD ProcLowD;
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                    ProcLow = new QRPINS.BL.INSSTS.INSProcLowYield();
                    brwChannel.mfCredentials(ProcLow);

                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYieldD), "INSProcLowYieldD");
                    ProcLowD = new QRPINS.BL.INSSTS.INSProcLowYieldD();
                    brwChannel.mfCredentials(ProcLowD);
                }
                else
                {
                    ProcLow = new QRPINS.BL.INSSTS.INSProcLowYield(m_strDBConn);
                    ProcLowD = new QRPINS.BL.INSSTS.INSProcLowYieldD(m_strDBConn);
                }

                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strAriseProcessCode = e.Row.Cells["AriseProcessCode"].Value.ToString();
                string strAriseDate = e.Row.Cells["AriseDate"].Value.ToString();
                string strAriseTime = e.Row.Cells["AriseTime"].Value.ToString();

                #region 헤더 조회
                DataTable dtHeaderDetail = ProcLow.mfReadINSProcLowYield_Detail(strPlantCode, strAriseProcessCode, strAriseDate, strAriseTime, m_resSys.GetString("SYS_LANG"));

                if (dtHeaderDetail.Rows.Count > 0)
                {
                    this.uTextPlantCode.Text = dtHeaderDetail.Rows[0]["PlantCode"].ToString();
                    this.uTextPlantName.Text = dtHeaderDetail.Rows[0]["PlantName"].ToString();
                    this.uTextLotNo.Text = dtHeaderDetail.Rows[0]["LotNo"].ToString();
                    this.uTextCustomerCode.Text = dtHeaderDetail.Rows[0]["CustomerCode"].ToString();
                    this.uTextCustomerName.Text = dtHeaderDetail.Rows[0]["CustomerName"].ToString();
                    this.uTextEquipCode.Text = dtHeaderDetail.Rows[0]["EquipCode"].ToString();
                    this.uTextEquipName.Text = dtHeaderDetail.Rows[0]["EquipName"].ToString();
                    this.uTextCustomerProductSpec.Text = dtHeaderDetail.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                    this.uTextPackage.Text = dtHeaderDetail.Rows[0]["PACKAGE"].ToString();
                    this.uTextBinName.Text = dtHeaderDetail.Rows[0]["BinName"].ToString();
                    this.uTextAriseProcessCode.Text = dtHeaderDetail.Rows[0]["AriseProcessCode"].ToString();
                    this.uTextAriseProcessName.Text = dtHeaderDetail.Rows[0]["AriseProcessName"].ToString();
                    this.uTextAriseDate.Text = Convert.ToDateTime(dtHeaderDetail.Rows[0]["AriseDate"]).ToString("yyyy-MM-dd");
                    this.uTextAriseTime.Text = Convert.ToDateTime(dtHeaderDetail.Rows[0]["AriseTime"]).ToString("HH:mm:ss");
                    this.uTextWorkUserID.Text = dtHeaderDetail.Rows[0]["WorkUserID"].ToString();
                    this.uTextWorkUserName.Text = dtHeaderDetail.Rows[0]["WorkUserName"].ToString();
                    this.uTextLotQty.Text = Math.Floor(Convert.ToDecimal(dtHeaderDetail.Rows[0]["LotQty"])).ToString();
                    this.uTextProcessYield.Text = decimal.Round(Convert.ToDecimal(dtHeaderDetail.Rows[0]["ProcessYield"]), 3).ToString();

                    this.uTextTechnAriseCause.Text = dtHeaderDetail.Rows[0]["TechnAriseCause"].ToString();
                    this.uTextTechnMeasureDesc.Text = dtHeaderDetail.Rows[0]["TechnMeasureDesc"].ToString();
                    this.uTextTechnRegistUserID.Text = dtHeaderDetail.Rows[0]["TechnUserID"].ToString();
                    this.uTextTechnRegistUserName.Text = dtHeaderDetail.Rows[0]["TechnUserName"].ToString();

                    this.uTextAriseCause.Text = dtHeaderDetail.Rows[0]["AriseCause"].ToString();
                    this.uTextMeasureDesc.Text = dtHeaderDetail.Rows[0]["MeasureDesc"].ToString();
                    this.uTextAttachFile.Text = dtHeaderDetail.Rows[0]["FilePath"].ToString();
                    this.uTextRegistUserID.Text = dtHeaderDetail.Rows[0]["RegistUserID"].ToString();
                    this.uTextRegistUserName.Text = dtHeaderDetail.Rows[0]["RegistUserName"].ToString();

                    this.uCheckReturn.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["ReturnFlag"]);
                    this.uTextReturnComment.Text = dtHeaderDetail.Rows[0]["ReturnComment"].ToString();
                    this.uCheckConfirm.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["ConfirmFlag"]);
                    this.uTextComfirmComment.Text = dtHeaderDetail.Rows[0]["ConfirmDesc"].ToString();
                    this.uCheckLotRelease.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["LotReleaseFlag"]);
                    this.uTextLotReleaseReason.Text = dtHeaderDetail.Rows[0]["ReleaseDesc"].ToString();
                    this.uCheckScrapFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["ScrapFlag"]);
                    this.uTextWriteUserID.Text = dtHeaderDetail.Rows[0]["WriteUserID"].ToString();
                    this.uTextWriteUserName.Text = dtHeaderDetail.Rows[0]["WriteUserName"].ToString();

                    this.uCheckFir4MManFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["ManFlag"]);
                    this.uCheckFir4MMethodFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["MethodFlag"]);
                    this.uCheckFir4MMaterialFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["MaterialFlag"]);
                    this.uCheckFir4MMachineFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["MachineFlag"]);
                    this.uCheckFir4MEnviroFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["EnvironmentFlag"]);

                    this.uCheckLotRelease.Enabled = !this.uCheckLotRelease.Checked;
                    this.uCheckReturn.Enabled = !this.uCheckLotRelease.Checked;
                    this.uCheckScrapFlag.Enabled = !this.uCheckLotRelease.Checked;
                    this.uCheckConfirm.Enabled = !this.uCheckLotRelease.Checked;

                    this.uComboImputeProcess.Text = dtHeaderDetail.Rows[0]["ImputeProcess"].ToString();
                    this.uTextImputeEquipCode.Text = dtHeaderDetail.Rows[0]["ImputeEquipCode"].ToString();
                    this.uTextImputeEquipName.Text = dtHeaderDetail.Rows[0]["ImputeEquipName"].ToString();
                    this.uTextImputeUserID.Text = dtHeaderDetail.Rows[0]["ImputeUserID"].ToString();
                    this.uTextImputeUserName.Text = dtHeaderDetail.Rows[0]["ImputeUserName"].ToString();

                }
                else
                {
                    DialogResult DResult = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                       , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    return;
                }
                #endregion

                #region 상세그리드 조회
                DataTable dtDGrid = ProcLowD.mfReadINSProcLowYieldD(strPlantCode, strAriseProcessCode, strAriseDate, strAriseTime, m_resSys.GetString("SYS_LANG"));

                this.uGridProcLowD.DataSource = dtDGrid;
                this.uGridProcLowD.DataBind();

                ////if (dtDGrid.Rows.Count > 0)
                ////{
                ////    WinGrid wGrid = new WinGrid();
                ////    wGrid.mfSetAutoResizeColWidth(this.uGridProcLowD, 0);
                ////}

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                #endregion

                // 로그인유저의 부서가 품질팀일때만 평가조건 Display
                // BL 연결
                QRPBrowser brwChannel1 = new QRPBrowser();
                brwChannel1.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel1.mfCredentials(clsUserCom);
                DataTable dtQualityDept = clsUserCom.mfReadUserCommonCode("QUA", "S0003", m_resSys.GetString("SYS_LANG"));
                if (dtQualityDept.Rows.Count > 0)
                {
                    string strDeptCheck = string.Empty;
                    var varQualityDept = from getRow in dtQualityDept.AsEnumerable()
                                         //where getRow["ComCode"].ToString() != string.Empty//m_resSys.GetString("SYS_DEPTCODE")
                                         select getRow["ComCode"].ToString();

                    if (varQualityDept.Contains(m_resSys.GetString("SYS_DEPTCODE")))
                    {
                        if (this.uTextWriteUserID.Text.Equals(string.Empty))
                        {
                            this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                            this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                        }
                    }
                    else
                    {
                        if (this.uTextRegistUserID.Text.Equals(string.Empty))
                        {
                            this.uTextRegistUserID.Text = m_resSys.GetString("SYS_USERID");
                            this.uTextRegistUserName.Text = m_resSys.GetString("SYS_USERNAME");
                        }
                    }


                }
                else
                {
                    if (this.uTextRegistUserID.Text.Equals(string.Empty))
                    {
                        this.uTextRegistUserID.Text = m_resSys.GetString("SYS_USERID");
                        this.uTextRegistUserName.Text = m_resSys.GetString("SYS_USERNAME");
                    }
                }

                ////if (m_resSys.GetString("SYS_DEPTCODE").Equals("S5100"))
                ////{
                ////    if (this.uTextWriteUserID.Text.Equals(string.Empty))
                ////    {
                ////        this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                ////        this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                ////    }
                ////}
                ////else
                ////{
                ////    if (this.uTextRegistUserID.Text.Equals(string.Empty))
                ////    {
                ////        this.uTextRegistUserID.Text = m_resSys.GetString("SYS_USERID");
                ////        this.uTextRegistUserName.Text = m_resSys.GetString("SYS_USERNAME");
                ////    }
                ////}

                #region 기존소스 주석처리 2011.12.15
                ////// 검색조건 변수 설정
                ////String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                ////String strAriseDate = e.Row.Cells["AriseDate"].Value.ToString();
                ////String strAriseTime = e.Row.Cells["AriseTime"].Value.ToString();
                ////String strProductCode = e.Row.Cells["ProductCode"].Value.ToString();
                ////String strAriseProcessCode = e.Row.Cells["AriseProcessCode"].Value.ToString();

                ////DataTable dtHeaderDetail = ProcLow.mfReadINSProcLowYieldGrid(strPlantCode, strAriseDate, strAriseTime,
                ////                                                strProductCode, m_resSys.GetString("SYS_LANG"));

                ////DataTable dtD = ProcLowD.mfReadINSProcLowYieldD(strPlantCode, strAriseProcessCode, strAriseDate, strAriseTime, m_resSys.GetString("SYS_LANG"));


                ////this.MdiParent.Cursor = Cursors.Default;
                ////m_ProgressPopup.mfCloseProgressPopup(this);

                ////if (dtHeaderDetail.Rows.Count > 0)
                ////{
                ////    this.uTextPlantCode.Text = dtHeaderDetail.Rows[0]["PlantCode"].ToString();
                ////    this.uTextPlantName.Text = dtHeaderDetail.Rows[0]["PlantName"].ToString();
                ////    this.uTextCustomerCode.Text = dtHeaderDetail.Rows[0]["CustomerCode"].ToString();
                ////    this.uTextCustomerName.Text = dtHeaderDetail.Rows[0]["CustomerName"].ToString();
                ////    this.uTextProductCode.Text = dtHeaderDetail.Rows[0]["ProductCode"].ToString();
                ////    this.uTextProductName.Text = dtHeaderDetail.Rows[0]["ProductName"].ToString();
                ////    this.uTextAriseProcess.Text = dtHeaderDetail.Rows[0]["AriseProcessCode"].ToString();
                ////    this.uTextAriseDate.Text = dtHeaderDetail.Rows[0]["AriseDate"].ToString();
                ////    this.uTextAriseTime.Text = dtHeaderDetail.Rows[0]["AriseTime"].ToString();
                ////    this.uTextLossName.Text = dtHeaderDetail.Rows[0]["LossName"].ToString();
                ////    this.uTextLossAmount.Text = dtHeaderDetail.Rows[0]["LossQty"].ToString();
                ////    this.uTextLotAmount.Text = Math.Floor(Convert.ToDecimal(dtHeaderDetail.Rows[0]["LotQty"])).ToString();
                ////    this.uTextProcessYield.Text = dtHeaderDetail.Rows[0]["ProcessYield"].ToString();
                ////    this.uTextOccurCause.Text = dtHeaderDetail.Rows[0]["AriseCause"].ToString();
                ////    this.uTextMeasurePoint.Text = dtHeaderDetail.Rows[0]["MeasureDesc"].ToString();
                ////    this.uTextAttachFile.Text = dtHeaderDetail.Rows[0]["FileName"].ToString();
                ////    this.uTextReturnComment.Text = dtHeaderDetail.Rows[0]["ReturnComment"].ToString();
                ////    this.uTextComfirmComment.Text = dtHeaderDetail.Rows[0]["ConfirmDesc"].ToString();
                ////    this.uTextLotReleaseReason.Text = dtHeaderDetail.Rows[0]["ReleaseDesc"].ToString();
                ////    this.uTextEquipCode.Text = dtHeaderDetail.Rows[0]["EquipCode"].ToString();
                ////    this.uTextEquipName.Text = dtHeaderDetail.Rows[0]["EquipName"].ToString();

                ////    this.uTextLotNo.Text = dtHeaderDetail.Rows[0]["LotNo"].ToString();
                ////    this.uTextBinName.Text = dtHeaderDetail.Rows[0]["BinName"].ToString();
                ////    this.uTextWorkUserID.Text = dtHeaderDetail.Rows[0]["WorkUserID"].ToString();
                ////    this.uTextWorkUserName.Text = dtHeaderDetail.Rows[0]["WorkUserName"].ToString();
                ////    this.uTextWriteUserID.Text = dtHeaderDetail.Rows[0]["WriteUserID"].ToString();
                ////    this.uTextWriteUserName.Text = dtHeaderDetail.Rows[0]["WriteUserName"].ToString();

                ////    this.uTextEquipCode.Text = dtHeaderDetail.Rows[0]["EquipCode"].ToString();
                ////    this.uTextEquipName.Text = dtHeaderDetail.Rows[0]["EquipName"].ToString();

                ////    if (dtHeaderDetail.Rows[0]["LotReleaseFlag"].ToString().Equals("T"))
                ////    {
                ////        this.uCheckLotRelease.Checked = true;
                ////    }
                ////    else
                ////    {
                ////        this.uCheckLotRelease.Checked = false;
                ////    }

                ////    if (dtHeaderDetail.Rows[0]["ConfirmFlag"].ToString().Equals("T"))
                ////    {
                ////        this.uCheckConfirm.Checked = true;
                ////    }
                ////    else
                ////    {
                ////        this.uCheckConfirm.Checked = false;
                ////    }

                ////    if (dtHeaderDetail.Rows[0]["ReturnFlag"].ToString().Equals("T"))
                ////    {
                ////        this.uCheckReturn.Checked = true;
                ////    }
                ////    else
                ////    {
                ////        this.uCheckReturn.Checked = false;
                ////    }

                ////    if (dtHeaderDetail.Rows[0]["ScrapFlag"].ToString() == "T")
                ////    {
                ////        this.uCheckScrapFlag.Checked = true;
                ////    }
                ////    else
                ////    {
                ////        this.uCheckScrapFlag.Checked = false;
                ////    }

                ////    this.uGrid2.DataSource = dtD;
                ////    this.uGrid2.DataBind();

                ////    if (dtD.Rows.Count > 0)
                ////    {
                ////        WinGrid grd = new WinGrid();
                ////        grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                ////    }

                ////    this.uGroupBoxContentsArea.Expanded = true;

                ////    if (this.uTextWriteUserID.Text == "")
                ////    {
                ////        this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                ////        this.uTextWriteUserName.Text = GetUserName(this.uGridRowYieldList.ActiveRow.Cells["PlantCode"].Value.ToString(), this.uTextWriteUserID.Text);
                ////    }
                ////}
                ////else
                ////{
                ////    DialogResult DResult = new DialogResult();
                ////    WinMessageBox msg = new WinMessageBox();
                ////    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////       , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);

                ////    this.uGroupBoxContentsArea.Expanded = false;
                ////}
                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonAddData_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                QRPINS.BL.INSSTS.INSProcLowYield ProcLow = new QRPINS.BL.INSSTS.INSProcLowYield();
                brwChannel.mfCredentials(ProcLow);

                DataTable dtProc = ProcLow.mfSetDataInfo();

                DialogResult DResult = new DialogResult();
                DataRow dr = dtProc.NewRow();
                dr["PlantCode"] = "1";
                dr["AriseProcessCode"] = "123123";
                dr["AriseDate"] = "2011-09-01";
                dr["AriseTime"] = "11:00";
                dr["ProductCode"] = "P1";
                dr["LossName"] = "AAA";
                dr["LossQty"] = "10.00000";
                dr["LotQty"] = "10.00000";
                dr["ProcessYield"] = "10.00000";
                dtProc.Rows.Add(dr);

                if (dtProc.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                         Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                         "M001264", "M001053", "M000936",
                                         Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar uProgressPopup = new QRPProgressBar();
                        Thread uth = uProgressPopup.mfStartThread();

                        uProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;


                        //처리로직
                        //저장 함수 
                        String strSaveProc = ProcLow.mfSaveINSProcLowYieldBtn(dtProc, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSaveProc);
                        //END

                        this.MdiParent.Cursor = Cursors.Default;
                        uProgressPopup.mfCloseProgressPopup(this);

                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000930"
                            , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                          , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000953"
                          , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //다운로드 버튼 클릭
        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (this.uTextAttachFile.Text.Contains(":\\") || this.uTextAttachFile.Text == "")
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M000357",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();


                        if (this.uTextAttachFile.Text.Contains(":\\") == false)
                        {
                            arrFile.Add(this.uTextAttachFile.Text);
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();
                        System.Diagnostics.Process.Start(strSaveFolder + this.uTextAttachFile.Text);

                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckLotRelease_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckLotRelease.Checked)
                {
                    this.uCheckReturn.Checked = !this.uCheckLotRelease.Checked;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckReturn_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckReturn.Checked)
                {
                    this.uCheckLotRelease.Checked = !this.uCheckReturn.Checked;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextImputeEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Enter Key만 적용
                if (!e.KeyData.Equals(Keys.Enter))
                    return;

                //설비코드가 공백이라면 리턴
                if (this.uTextImputeEquipCode.Text.Equals(string.Empty))
                    return;

                //공장정보 저장
                string strPlantCode = this.uComboSearchPlant.Value == null ? "" : this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                    return;


                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //Text에 입력한 설비정보 조회
                DataTable dtRtn = SearchEquipInfo(strPlantCode, this.uTextImputeEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                //정보가 있다면 설비명 Text에 삽입, 없다면 메세지
                if (dtRtn.Rows.Count > 0)
                    this.uTextImputeEquipName.Text = dtRtn.Rows[0]["EquipName"].ToString();
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001075", "M000902"
                                , Infragistics.Win.HAlign.Right);

                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextImputeUserID_KeyDown(object sender, KeyEventArgs e)
        {
          try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextImputeUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                            String strWriteID = this.uTextImputeUserID.Text;

                            // UserName 검색 함수 호출
                            DataTable dtUserInfo = GetUserInfo(m_resSys.GetString("SYS_PLANTCODE") , strWriteID);

                            if (dtUserInfo.Rows.Count > 0)
                            {
                                this.uTextImputeUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextImputeUserID.Clear();
                                this.uTextImputeUserName.Clear();
                            }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextImputeUserID.TextLength <= 1 || this.uTextImputeUserID.SelectedText == this.uTextImputeUserID.Text)
                    {
                        this.uTextImputeUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
            }
        }

        private DataTable SearchEquipInfo(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                dtRtn = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, strLang);
                clsEquip.Dispose();

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            { dtRtn.Dispose(); }
        }

        private void uTextImputeEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                string strPlantCode = this.uComboSearchPlant.Value == null ? "" : this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                    return;

                frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                if (frmEquip.EquipCode == null || frmEquip.EquipCode.Equals(string.Empty))
                    return;

                this.uTextImputeEquipCode.Text = frmEquip.EquipCode;
                this.uTextImputeEquipName.Text = frmEquip.EquipName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextImputeUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboSearchPlant.Value.ToString();

                frmPOP.ShowDialog();
                this.uTextImputeUserID.Text = frmPOP.UserID;
                this.uTextImputeUserName.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextImputeEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextImputeEquipName.Text.Equals(string.Empty))
            {
                this.uTextImputeEquipName.Text = string.Empty;
            }
        }

        private void uTextImputeUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextImputeUserName.Text.Equals(string.Empty)) 
            {
                this.uTextImputeUserName.Text = string.Empty;
            }
        }


        private void checkUserPW() 
        {
            if (this.uTextWriteUserID.Text != "")
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                String strUserID = this.uTextWriteUserID.Text;

                // User검색 함수 호출
                String strRtnUserName = GetUserName(this.uTextPlantCode.Text, strUserID);

                if (strRtnUserName == "")
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                               "M001264", "M000962", "M000621",
                               Infragistics.Win.HAlign.Right);

                    this.uTextWriteUserID.Text = "";
                    this.uTextWriteUserName.Text = "";
                }

                else
                {
                    this.uTextWriteUserName.Text = strRtnUserName;
                    
                }
            }
        }


    }
}
