﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

//Using Add
using System.Data;

namespace QRPINS.UI
{
    /// <summary>
    /// Summary description for rptINSZ0002_S01.
    /// </summary>
    public partial class rptINSZ0002_S01 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptINSZ0002_S01()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }
        public rptINSZ0002_S01(DataTable dtResult)
        {
        
                //
                // Required for Windows Form Designer support
                //

                this.DataSource = dtResult;
                InitializeComponent();

        }


    }
}
