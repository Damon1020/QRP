﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0011D1.cs                                      */
/* 프로그램명   : QCN LIST 장비재검 POPUP 창                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-29 : 기능구현 추가 (이종호)                   */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0011D1 : Form
    {
        // 다국어 지원을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        // 전역변수

        private string strPlantCode;
        private string strQCNNo;
        // 판정 여부 확인용 전역변수
        private string strFailCheck;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string QCNNo
        {
            get { return strQCNNo; }
            set { strQCNNo = value; }
        }

        public string FailCheck
        {
            get { return strFailCheck; }
            set { strFailCheck = value; }
        }

        public frmINSZ0011D1()
        {
            InitializeComponent();
        }

        private void frmINSZ0011D1_Load(object sender, EventArgs e)
        {
            InitGroupBox();
            InitGrid();

            QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);

            // 데이터 조회 메소드 호출F
            Search();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmINSZ0011D1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                // 1차 검사
                wGroupBox.mfSetGroupBox(this.uGroupBox1st, GroupBoxType.LIST, "1차 검사(장비 재검사)", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1st.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1st.HeaderAppearance.FontData.SizeInPoints = 9;

                // 2차 검사
                wGroupBox.mfSetGroupBox(this.uGroupBox2nd, GroupBoxType.LIST, "2차 검사(장비 재검사)", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox2nd.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2nd.HeaderAppearance.FontData.SizeInPoints = 9;

                // 3차 검사
                wGroupBox.mfSetGroupBox(this.uGroupBox3th, GroupBoxType.LIST, "3차 검사(장비 재검사)", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3th.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3th.HeaderAppearance.FontData.SizeInPoints = 9;               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 1차 검사
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1st, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1st, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectResult", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "FaultDesc", "불량내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "ResultFlag", "결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", m_resSys.GetString("SYS_USERID"));

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectUserName", "검사자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", m_resSys.GetString("SYS_USERNAME"));

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "InspectDate", "검사일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1st, 0, "OperCondition", "가동조건", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 2차 검사
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2nd, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectResult", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "FaultDesc", "불량내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "ResultFlag", "결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", m_resSys.GetString("SYS_USERID"));

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectUserName", "검사자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", m_resSys.GetString("SYS_USERNAME"));

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "InspectDate", "검사일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2nd, 0, "OperCondition", "가동조건", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 3차 검사
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid3th, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3th, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectResult", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "FaultDesc", "불량내용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "ResultFlag", "결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", m_resSys.GetString("SYS_USERID"));

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectUserName", "검사자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", m_resSys.GetString("SYS_USERNAME"));

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "InspectDate", "검사일자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3th, 0, "OperCondition", "가동조건", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // Set FontSize
                this.uGrid1st.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1st.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2nd.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2nd.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid3th.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid3th.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                //this.uGrid1st.DisplayLayout.Bands[0].Columns["ResultFlag"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
                //this.uGrid1st.DisplayLayout.Bands[0].Columns["InspectUserID"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
                //this.uGrid1st.DisplayLayout.Bands[0].Columns["InspectDate"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;

                // 빈줄추가
                wGrid.mfAddRowGrid(uGrid1st, 0);
                wGrid.mfAddRowGrid(uGrid2nd, 0);
                wGrid.mfAddRowGrid(uGrid3th, 0);

                // 결과 컬럼 DropDown 설정
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1st, 0, "ResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGrid2nd, 0, "ResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGrid3th, 0, "ResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCom);

                // 설비
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfReadEquipForPlantCombo(PlantCode, m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1st, 0, "EquipCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtEquip);
                wGrid.mfSetGridColumnValueList(this.uGrid2nd, 0, "EquipCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtEquip);
                wGrid.mfSetGridColumnValueList(this.uGrid3th, 0, "EquipCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtEquip);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 장비재검 정보 조회 Method
        /// </summary>
        private void Search()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNEquReInspect), "ProcQCNEquReInspect");
                QRPINS.BL.INSPRC.ProcQCNEquReInspect clsQCNEqu = new QRPINS.BL.INSPRC.ProcQCNEquReInspect();
                brwChannel.mfCredentials(clsQCNEqu);

                //완료 체크박스 로드
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                QRPINS.BL.INSPRC.ProcQCN clsQcn = new QRPINS.BL.INSPRC.ProcQCN();
                brwChannel.mfCredentials(clsQcn);

                DataTable dtFir = clsQCNEqu.mfReadINSProcQCNEquReInspect(PlantCode, QCNNo, 1);

                for (int i = 0; i < dtFir.Rows.Count; i++)
                {
                    if (dtFir.Rows[0]["InspectUserID"].ToString().Equals(string.Empty) && dtFir.Rows[0]["InspectUserName"].ToString().Equals(string.Empty))
                    {
                        dtFir.Rows[0]["InspectUserID"] = m_resSys.GetString("SYS_USERID");
                        dtFir.Rows[0]["InspectUserName"] = m_resSys.GetString("SYS_USERNAME");
                    }
                }

                this.uGrid1st.DataSource = dtFir;
                this.uGrid1st.DataBind();

                if (dtFir.Rows.Count > 0)
                {
                    this.uText1stComplete.Text = dtFir.Rows[0]["NextStepFlag"].ToString();

                    if (dtFir.Rows[0]["NextStepFlag"].ToString().Equals("T"))
                    {
                        //this.uGrid1st.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                        //this.uGrid1st.DisplayLayout.Bands[0].Columns["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                        
                        DataTable dtSec = clsQCNEqu.mfReadINSProcQCNEquReInspect(PlantCode, QCNNo, 2);

                        for (int i = 0; i < dtSec.Rows.Count; i++)
                        {
                            if (dtSec.Rows[0]["InspectUserID"].ToString().Equals(string.Empty) && dtSec.Rows[0]["InspectUserName"].ToString().Equals(string.Empty))
                            {
                                dtSec.Rows[0]["InspectUserID"] = m_resSys.GetString("SYS_USERID");
                                dtSec.Rows[0]["InspectUserName"] = m_resSys.GetString("SYS_USERNAME");
                            }
                        }

                        this.uGrid2nd.DataSource = dtSec;
                        this.uGrid2nd.DataBind();


                        if (dtSec.Rows.Count > 0)
                        {
                            this.uText2ndComplete.Text = dtSec.Rows[0]["NextStepFlag"].ToString();

                            if (dtSec.Rows[0]["NextStepFlag"].ToString().Equals("T"))
                            {
                                //this.uGrid2nd.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                //this.uGrid2nd.DisplayLayout.Bands[0].Columns["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;

                                DataTable dtThi = clsQCNEqu.mfReadINSProcQCNEquReInspect(PlantCode, QCNNo, 3);

                                for (int i = 0; i < dtThi.Rows.Count; i++)
                                {
                                    if (dtThi.Rows[0]["InspectUserID"].ToString().Equals(string.Empty) && dtThi.Rows[0]["InspectUserName"].ToString().Equals(string.Empty))
                                    {
                                        dtThi.Rows[0]["InspectUserID"] = m_resSys.GetString("SYS_USERID");
                                        dtThi.Rows[0]["InspectUserName"] = m_resSys.GetString("SYS_USERNAME");
                                    }
                                }

                                this.uGrid3th.DataSource = dtThi;
                                this.uGrid3th.DataBind();

                                if (dtThi.Rows.Count > 0)
                                {
                                    this.uText3thComplete.Text = dtThi.Rows[0]["NextStepFlag"].ToString();

                                    if (dtThi.Rows[0]["NextStepFlag"].ToString().Equals("T"))
                                    {
                                        //this.uGrid3th.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                        //this.uGrid3th.DisplayLayout.Bands[0].Columns["InspectUserID"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                                    }
                                }
                            }
                            else
                            {
                                this.uGrid3th.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            }
                        }
                    }
                    else
                    {
                        this.uGrid2nd.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                        this.uGrid3th.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    }
                }

                DataTable dtComplete = clsQcn.mfReadINSProcQCNDetail_List(PlantCode, QCNNo, m_resSys.GetString("SYS_LANG"));

                if (dtComplete.Rows[0]["EquReInspectCompleteFlag"].ToString().Equals("T"))
                {
                    this.uCheckComplete.Checked = true;
                    this.uCheckComplete.Enabled = false;
                }
                else
                {
                    this.uCheckComplete.Checked = false;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사자 ID 팝업창 메소드
        /// </summary>
        /// <param name="e"></param>
        private void PopUpUser(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {

                if (e.Cell.Column.Key == "InspectUserID")
                {
                    // SystemInfo ResourceSet

                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = PlantCode;
                    frmPOP.ShowDialog();

                    if (PlantCode != frmPOP.PlantCode)
                    {
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M000798", "M000275", "M001254"
                                                    , Infragistics.Win.HAlign.Right);

                        e.Cell.Row.Cells["InspectUserID"].Value = "";
                        e.Cell.Row.Cells["InspectUserName"].Value = "";
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectUserID"].Value = frmPOP.UserID;
                        e.Cell.Row.Cells["InspectUserName"].Value = frmPOP.UserName;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Dispose();
            }
        }        

        // 저장버튼 클릭이벤트

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckComplete.Enabled.Equals(true))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // BL연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCNEquReInspect), "ProcQCNEquReInspect");
                    QRPINS.BL.INSPRC.ProcQCNEquReInspect clsEqu = new QRPINS.BL.INSPRC.ProcQCNEquReInspect();
                    brwChannel.mfCredentials(clsEqu);
                    //CompleteFlag
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcQCN), "ProcQCN");
                    QRPINS.BL.INSPRC.ProcQCN clsQcn = new QRPINS.BL.INSPRC.ProcQCN();
                    brwChannel.mfCredentials(clsQcn);


                    // 데이터 테이블 컬럼설정
                    DataTable dtSave = clsEqu.mfSetDataInfo();
                    DataTable dtComplete = clsQcn.mfSetDataInfo_List();
                    DataRow drRow;

                    if(this.uGrid1st.Rows.Count > 0)
                        this.uGrid1st.ActiveCell = this.uGrid1st.Rows[0].Cells[0];
                    // 1차검사 데이터 저장
                    for (int i = 0; i < this.uGrid1st.Rows.Count; i++)
                    {
                        if (this.uGrid1st.Rows[i].Cells["LotNo"].Value.ToString() != "")
                        {
                            drRow = dtSave.NewRow();
                            drRow["PlantCode"] = PlantCode;
                            drRow["QCNNo"] = QCNNo;
                            drRow["InspectSeq"] = 1;
                            drRow["Seq"] = this.uGrid1st.Rows[i].RowSelectorNumber;
                            drRow["LotNo"] = this.uGrid1st.Rows[i].Cells["LotNo"].Value.ToString();
                            drRow["InspectResult"] = this.uGrid1st.Rows[i].Cells["InspectResult"].Value.ToString();
                            drRow["FaultDesc"] = this.uGrid1st.Rows[i].Cells["FaultDesc"].Value.ToString();
                            drRow["ResultFlag"] = this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString();
                            drRow["InspectUserID"] = this.uGrid1st.Rows[i].Cells["InspectUserID"].Value.ToString();
                            //drRow["InspectDate"] = Convert.ToDateTime(this.uGrid1st.Rows[i].Cells["InspectDate"].Value).ToString("yyyy-MM-dd");
                            drRow["EquipCode"] = this.uGrid1st.Rows[i].Cells["EquipCode"].Value.ToString();
                            drRow["OperCondition"] = this.uGrid1st.Rows[i].Cells["OperCondition"].Value.ToString();
                            dtSave.Rows.Add(drRow);
                        }
                    }
                

                    if(this.uGrid2nd.Rows.Count > 0)
                        this.uGrid2nd.ActiveCell = this.uGrid2nd.Rows[0].Cells[0];
                    // 2차 검사 데이터 저장
                    for (int i = 0; i < this.uGrid2nd.Rows.Count; i++)
                    {
                        if (this.uGrid2nd.Rows[i].Cells["LotNo"].Value.ToString() != "")
                        {
                            drRow = dtSave.NewRow();
                            drRow["PlantCode"] = PlantCode;
                            drRow["QCNNo"] = QCNNo;
                            drRow["InspectSeq"] = 2;
                            drRow["Seq"] = this.uGrid2nd.Rows[i].RowSelectorNumber;
                            drRow["LotNo"] = this.uGrid2nd.Rows[i].Cells["LotNo"].Value.ToString();
                            drRow["InspectResult"] = this.uGrid2nd.Rows[i].Cells["InspectResult"].Value.ToString();
                            drRow["FaultDesc"] = this.uGrid2nd.Rows[i].Cells["FaultDesc"].Value.ToString();
                            drRow["ResultFlag"] = this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString();
                            drRow["InspectUserID"] = this.uGrid2nd.Rows[i].Cells["InspectUserID"].Value.ToString();
                            //drRow["InspectDate"] = Convert.ToDateTime(this.uGrid2nd.Rows[i].Cells["InspectDate"].Value).ToString("yyyy-MM-dd");
                            drRow["EquipCode"] = this.uGrid2nd.Rows[i].Cells["EquipCode"].Value.ToString();
                            drRow["OperCondition"] = this.uGrid2nd.Rows[i].Cells["OperCondition"].Value.ToString();
                            dtSave.Rows.Add(drRow);
                        }
                    }
                

                    if(this.uGrid3th.Rows.Count > 0)
                        this.uGrid3th.ActiveCell = this.uGrid3th.Rows[0].Cells[0];
                    // 3차 검사 데이터 저장
                    for (int i = 0; i < this.uGrid3th.Rows.Count; i++)
                    {
                        if (this.uGrid3th.Rows[i].Cells["LotNo"].Value.ToString() != "")
                        {
                            drRow = dtSave.NewRow();
                            drRow["PlantCode"] = PlantCode;
                            drRow["QCNNo"] = QCNNo;
                            drRow["InspectSeq"] = 3;
                            drRow["Seq"] = this.uGrid3th.Rows[i].RowSelectorNumber;
                            drRow["LotNo"] = this.uGrid3th.Rows[i].Cells["LotNo"].Value.ToString();
                            drRow["InspectResult"] = this.uGrid3th.Rows[i].Cells["InspectResult"].Value.ToString();
                            drRow["FaultDesc"] = this.uGrid3th.Rows[i].Cells["FaultDesc"].Value.ToString();
                            drRow["ResultFlag"] = this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString();
                            drRow["InspectUserID"] = this.uGrid3th.Rows[i].Cells["InspectUserID"].Value.ToString();
                            //drRow["InspectDate"] = Convert.ToDateTime(this.uGrid3th.Rows[i].Cells["InspectDate"].Value).ToString("yyyy-MM-dd");
                            drRow["EquipCode"] = this.uGrid3th.Rows[i].Cells["EquipCode"].Value.ToString();
                            drRow["OperCondition"] = this.uGrid3th.Rows[i].Cells["OperCondition"].Value.ToString();
                            dtSave.Rows.Add(drRow);
                        }
                    }
                    

                    if (this.uCheckComplete.Enabled)
                    {
                        //CompleteFlag
                        drRow = dtComplete.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["QCNNo"] = QCNNo;
                        if (this.uCheckComplete.Checked == true)
                        {
                            drRow["EquReInspectCompleteFlag"] = "T";
                        }
                        else
                        {
                            drRow["EquReInspectCompleteFlag"] = "F";
                        }

                        dtComplete.Rows.Add(drRow);
                    }

                    // 프로그래스 팝업창 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.Cursor = Cursors.WaitCursor;
                    //this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 저장 메소드 실행
                    string strErrRtn = clsEqu.mfSaveINSProcQCNEquReInspect(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                    // 저장 성공시 CompleteFlag 저장
                    string strComplete = clsQcn.mfSaveINSProcQCN_ListEquComplete(dtComplete, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //불합격 체크
                    FailCheckGridValue();

                    // 팦업창 Close
                    //this.MdiParent.Cursor = Cursors.Default;
                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 결과검사

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn.mfDecodingErrMessage(strErrRtn);
                    TransErrRtn ErrRtn2 = new TransErrRtn();
                    ErrRtn2.mfDecodingErrMessage(strComplete);

                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();
                    // 처리결과에 따른 메세지 박스
                    if (ErrRtn.ErrNum == 0 && ErrRtn2.ErrNum == 0)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000930",
                                            Infragistics.Win.HAlign.Right);
                        this.Close();
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000953",
                                            Infragistics.Win.HAlign.Right);
                    }
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 1차 검사 검사자 ID 버튼클릭 이벤트
        private void uGrid1st_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                PopUpUser(e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 2차 검사 검사자 ID 버튼 클릭 이벤트
        private void uGrid2nd_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                PopUpUser(e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 3차 검사자 ID버튼 클릭 이벤트
        private void uGrid3th_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                PopUpUser(e);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 모든 차수의 그리드를 돌면서 검사결과가 불합격인 것을 체크
        private void FailCheckGridValue()
        {
            try
            {
                if (this.uText3thComplete.Text.Equals("T"))
                {
                    strFailCheck = "-1";
                }
                else if (this.uText2ndComplete.Text.Equals("T"))
                {
                    int intGrid3Fail = 0;
                    for (int i = 0; i < this.uGrid3th.Rows.Count; i++)
                    {
                        if (this.uGrid3th.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                        {
                            intGrid3Fail++;
                        }
                    }

                    strFailCheck = intGrid3Fail.ToString();
                }
                else if (this.uText1stComplete.Text.Equals("T"))
                {
                    int intGrid2Fail = 0;
                    for (int i = 0; i < this.uGrid2nd.Rows.Count; i++)
                    {
                        if (this.uGrid2nd.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                        {
                            intGrid2Fail++;
                        }
                    }

                    strFailCheck = intGrid2Fail.ToString();
                }
                else
                {
                    int intGrid1Fail = 0;
                    for (int i = 0; i < this.uGrid1st.Rows.Count; i++)
                    {
                        if (this.uGrid1st.Rows[i].Cells["ResultFlag"].Value.ToString().Equals("NG"))
                        {
                            intGrid1Fail++;
                        }
                    }

                    strFailCheck = intGrid1Fail.ToString();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 그리드 키다운 이벤트 / 관련 메소드
        private void uGrid1st_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && uGrid1st.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    SearchUserName(this.uGrid1st);
                }
                else if (e.KeyCode == Keys.Back && uGrid1st.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    string strInspectUserID = this.uGrid1st.ActiveCell.Text;
                    if (strInspectUserID.Length <= 1)
                    {
                        this.uGrid1st.ActiveRow.Cells["InspectUserName"].Value = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2nd_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && uGrid2nd.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    SearchUserName(this.uGrid2nd);
                }
                else if (e.KeyCode == Keys.Back && uGrid2nd.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    string strInspectUserID = this.uGrid2nd.ActiveCell.Text;
                    if (strInspectUserID.Length <= 1)
                    {
                        this.uGrid2nd.ActiveRow.Cells["InspectUserName"].Value = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3th_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && uGrid3th.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    SearchUserName(this.uGrid3th);
                }
                else if (e.KeyCode == Keys.Back && uGrid3th.ActiveCell.Column.Key.Equals("InspectUserID"))
                {
                    string strInspectUserID = this.uGrid3th.ActiveCell.Text;
                    if (strInspectUserID.Length <= 1)
                    {
                        this.uGrid3th.ActiveRow.Cells["InspectUserName"].Value = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사자 아이디 입력시 명 조회해서 그리드에 Display하는 메소드
        /// </summary>
        /// <param name="uGrid"></param>
        private void SearchUserName(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        {
            try
            {
                if (!uGrid.ActiveCell.Text.Equals(string.Empty))
                {
                    // SystemInfor ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    string strUserID = uGrid.ActiveCell.Text.Trim();

                    DataTable dtUser = GetUserInfo(PlantCode, strUserID);

                    if (dtUser.Rows.Count > 0)
                    {
                        uGrid.ActiveRow.Cells["InspectUserName"].Value = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000621",
                                    Infragistics.Win.HAlign.Right);

                        uGrid.ActiveCell.Row.Cells["WorkUserName"].Value = "";
                        uGrid.ActiveCell.Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
            }
        }
        #endregion
    }
}
