﻿namespace QRPINS.UI
{
    partial class frmINSZ0015
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0015));
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckSearchCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelSearchComplete = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchReqUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchReqUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchReqUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchConsumableType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ulabelSearchConsumableType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateSearchReqToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchReqFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchReqDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchStdNumber = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGridReturnList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxInput = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateReqDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextICP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelICP = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMSDS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMSDS = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApplyDevice = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApplyDevice = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqPurpose = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestPurpose = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelReqDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReqUser = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUnitCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnit = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpecNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGRDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFloorPlanNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDrawingNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShipmentQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAmount = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStorageDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStdNumber = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridFile = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridReturnList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInput)).BeginInit();
            this.uGroupBoxInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextICP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMSDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyDevice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).BeginInit();
            this.uGroupBoxInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnitCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFile)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckSearchCompleteFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchComplete);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchReqUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchReqUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchReqUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.ulabelSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchReqToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchReqFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchReqDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStdNumber);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStdNumber);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 85);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uCheckSearchCompleteFlag
            // 
            this.uCheckSearchCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckSearchCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckSearchCompleteFlag.Location = new System.Drawing.Point(432, 60);
            this.uCheckSearchCompleteFlag.Name = "uCheckSearchCompleteFlag";
            this.uCheckSearchCompleteFlag.Size = new System.Drawing.Size(20, 20);
            this.uCheckSearchCompleteFlag.TabIndex = 45;
            this.uCheckSearchCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelSearchComplete
            // 
            this.uLabelSearchComplete.Location = new System.Drawing.Point(328, 60);
            this.uLabelSearchComplete.Name = "uLabelSearchComplete";
            this.uLabelSearchComplete.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchComplete.TabIndex = 44;
            this.uLabelSearchComplete.Text = "ultraLabel2";
            // 
            // uTextSearchReqUserID
            // 
            appearance22.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance22;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchReqUserID.ButtonsRight.Add(editorButton1);
            this.uTextSearchReqUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchReqUserID.Location = new System.Drawing.Point(116, 60);
            this.uTextSearchReqUserID.Name = "uTextSearchReqUserID";
            this.uTextSearchReqUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchReqUserID.TabIndex = 43;
            this.uTextSearchReqUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchReqUserID_KeyDown);
            this.uTextSearchReqUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchReqUserID_EditorButtonClick);
            // 
            // uTextSearchReqUserName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchReqUserName.Appearance = appearance15;
            this.uTextSearchReqUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchReqUserName.Location = new System.Drawing.Point(217, 60);
            this.uTextSearchReqUserName.Name = "uTextSearchReqUserName";
            this.uTextSearchReqUserName.ReadOnly = true;
            this.uTextSearchReqUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchReqUserName.TabIndex = 42;
            // 
            // uLabelSearchReqUser
            // 
            this.uLabelSearchReqUser.Location = new System.Drawing.Point(12, 60);
            this.uLabelSearchReqUser.Name = "uLabelSearchReqUser";
            this.uLabelSearchReqUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchReqUser.TabIndex = 41;
            this.uLabelSearchReqUser.Text = "ultraLabel2";
            // 
            // uComboSearchConsumableType
            // 
            this.uComboSearchConsumableType.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchConsumableType.Name = "uComboSearchConsumableType";
            this.uComboSearchConsumableType.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchConsumableType.TabIndex = 40;
            this.uComboSearchConsumableType.Text = "ultraComboEditor1";
            // 
            // ulabelSearchConsumableType
            // 
            this.ulabelSearchConsumableType.Location = new System.Drawing.Point(12, 36);
            this.ulabelSearchConsumableType.Name = "ulabelSearchConsumableType";
            this.ulabelSearchConsumableType.Size = new System.Drawing.Size(100, 20);
            this.ulabelSearchConsumableType.TabIndex = 39;
            this.ulabelSearchConsumableType.Text = "ultraLabel1";
            // 
            // uTextSearchMaterialCode
            // 
            appearance53.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance53.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance53;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(691, 12);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialCode.TabIndex = 38;
            this.uTextSearchMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMaterialCode_KeyDown);
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick);
            // 
            // uDateSearchReqToDate
            // 
            this.uDateSearchReqToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchReqToDate.Location = new System.Drawing.Point(808, 60);
            this.uDateSearchReqToDate.Name = "uDateSearchReqToDate";
            this.uDateSearchReqToDate.Size = new System.Drawing.Size(98, 21);
            this.uDateSearchReqToDate.TabIndex = 37;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(791, 60);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 36;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchReqFromDate
            // 
            this.uDateSearchReqFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchReqFromDate.Location = new System.Drawing.Point(692, 60);
            this.uDateSearchReqFromDate.Name = "uDateSearchReqFromDate";
            this.uDateSearchReqFromDate.Size = new System.Drawing.Size(98, 21);
            this.uDateSearchReqFromDate.TabIndex = 35;
            // 
            // uLabelSearchReqDate
            // 
            this.uLabelSearchReqDate.Location = new System.Drawing.Point(586, 60);
            this.uLabelSearchReqDate.Name = "uLabelSearchReqDate";
            this.uLabelSearchReqDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchReqDate.TabIndex = 34;
            this.uLabelSearchReqDate.Text = "ultraLabel2";
            // 
            // uTextSearchVendorName
            // 
            this.uTextSearchVendorName.Location = new System.Drawing.Point(804, 36);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchVendorName.TabIndex = 33;
            // 
            // uTextSearchVendorCode
            // 
            appearance28.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance28.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance28;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton3);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(691, 36);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorCode.TabIndex = 32;
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(586, 36);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchVendor.TabIndex = 31;
            this.uLabelSearchVendor.Text = "ultraLabel2";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(432, 36);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchLotNo.TabIndex = 30;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(328, 36);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 29;
            this.uLabelSearchLotNo.Text = "ultraLabel2";
            // 
            // uTextSearchMaterialName
            // 
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(804, 12);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchMaterialName.TabIndex = 28;
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(586, 12);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterial.TabIndex = 26;
            this.uLabelSearchMaterial.Text = "ultraLabel2";
            // 
            // uTextSearchStdNumber
            // 
            this.uTextSearchStdNumber.Location = new System.Drawing.Point(431, 12);
            this.uTextSearchStdNumber.Name = "uTextSearchStdNumber";
            this.uTextSearchStdNumber.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchStdNumber.TabIndex = 5;
            // 
            // uLabelSearchStdNumber
            // 
            this.uLabelSearchStdNumber.Location = new System.Drawing.Point(328, 12);
            this.uLabelSearchStdNumber.Name = "uLabelSearchStdNumber";
            this.uLabelSearchStdNumber.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStdNumber.TabIndex = 4;
            this.uLabelSearchStdNumber.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 2;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance6;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 125);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1060, 695);
            this.uGrid1.TabIndex = 3;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid1_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 650);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 195);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 650);
            this.uGroupBoxContentsArea.TabIndex = 4;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDelete);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridFile);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridReturnList);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxInput);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxInfo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGrid2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 630);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGridReturnList
            // 
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridReturnList.DisplayLayout.Appearance = appearance16;
            this.uGridReturnList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridReturnList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReturnList.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReturnList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance29;
            this.uGridReturnList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance34.BackColor2 = System.Drawing.SystemColors.Control;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReturnList.DisplayLayout.GroupByBox.PromptAppearance = appearance34;
            this.uGridReturnList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridReturnList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridReturnList.DisplayLayout.Override.ActiveCellAppearance = appearance52;
            appearance67.BackColor = System.Drawing.SystemColors.Highlight;
            appearance67.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridReturnList.DisplayLayout.Override.ActiveRowAppearance = appearance67;
            this.uGridReturnList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridReturnList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            this.uGridReturnList.DisplayLayout.Override.CardAreaAppearance = appearance68;
            appearance69.BorderColor = System.Drawing.Color.Silver;
            appearance69.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridReturnList.DisplayLayout.Override.CellAppearance = appearance69;
            this.uGridReturnList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridReturnList.DisplayLayout.Override.CellPadding = 0;
            appearance70.BackColor = System.Drawing.SystemColors.Control;
            appearance70.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance70.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance70.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance70.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReturnList.DisplayLayout.Override.GroupByRowAppearance = appearance70;
            appearance71.TextHAlignAsString = "Left";
            this.uGridReturnList.DisplayLayout.Override.HeaderAppearance = appearance71;
            this.uGridReturnList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridReturnList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            appearance72.BorderColor = System.Drawing.Color.Silver;
            this.uGridReturnList.DisplayLayout.Override.RowAppearance = appearance72;
            this.uGridReturnList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridReturnList.DisplayLayout.Override.TemplateAddRowAppearance = appearance73;
            this.uGridReturnList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridReturnList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridReturnList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridReturnList.Location = new System.Drawing.Point(532, 432);
            this.uGridReturnList.Name = "uGridReturnList";
            this.uGridReturnList.Size = new System.Drawing.Size(520, 192);
            this.uGridReturnList.TabIndex = 85;
            // 
            // uGroupBoxInput
            // 
            this.uGroupBoxInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxInput.Controls.Add(this.uDateReqDate);
            this.uGroupBoxInput.Controls.Add(this.uTextICP);
            this.uGroupBoxInput.Controls.Add(this.uLabelICP);
            this.uGroupBoxInput.Controls.Add(this.uTextMSDS);
            this.uGroupBoxInput.Controls.Add(this.uLabelMSDS);
            this.uGroupBoxInput.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxInput.Controls.Add(this.uLabelEtc);
            this.uGroupBoxInput.Controls.Add(this.uTextApplyDevice);
            this.uGroupBoxInput.Controls.Add(this.uLabelApplyDevice);
            this.uGroupBoxInput.Controls.Add(this.uTextReqPurpose);
            this.uGroupBoxInput.Controls.Add(this.uLabelRequestPurpose);
            this.uGroupBoxInput.Controls.Add(this.uLabelReqDate);
            this.uGroupBoxInput.Controls.Add(this.uTextReqName);
            this.uGroupBoxInput.Controls.Add(this.uTextReqID);
            this.uGroupBoxInput.Controls.Add(this.uLabelReqUser);
            this.uGroupBoxInput.Location = new System.Drawing.Point(8, 112);
            this.uGroupBoxInput.Name = "uGroupBoxInput";
            this.uGroupBoxInput.Size = new System.Drawing.Size(1040, 120);
            this.uGroupBoxInput.TabIndex = 84;
            // 
            // uDateReqDate
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReqDate.Appearance = appearance39;
            this.uDateReqDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReqDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateReqDate.Location = new System.Drawing.Point(432, 28);
            this.uDateReqDate.Name = "uDateReqDate";
            this.uDateReqDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReqDate.TabIndex = 95;
            // 
            // uTextICP
            // 
            appearance30.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance30.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance30;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Key = "Up";
            appearance37.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance37.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance37;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "Down";
            this.uTextICP.ButtonsRight.Add(editorButton4);
            this.uTextICP.ButtonsRight.Add(editorButton5);
            this.uTextICP.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextICP.Location = new System.Drawing.Point(88, 76);
            this.uTextICP.Name = "uTextICP";
            this.uTextICP.ReadOnly = true;
            this.uTextICP.Size = new System.Drawing.Size(21, 21);
            this.uTextICP.TabIndex = 94;
            this.uTextICP.Visible = false;
            this.uTextICP.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextICP_EditorButtonClick);
            // 
            // uLabelICP
            // 
            this.uLabelICP.Location = new System.Drawing.Point(64, 76);
            this.uLabelICP.Name = "uLabelICP";
            this.uLabelICP.Size = new System.Drawing.Size(21, 21);
            this.uLabelICP.TabIndex = 93;
            this.uLabelICP.Text = "ultraLabel2";
            this.uLabelICP.Visible = false;
            // 
            // uTextMSDS
            // 
            appearance33.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance33.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance33;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Key = "Up";
            appearance36.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance36;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton7.Key = "Down";
            this.uTextMSDS.ButtonsRight.Add(editorButton6);
            this.uTextMSDS.ButtonsRight.Add(editorButton7);
            this.uTextMSDS.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMSDS.Location = new System.Drawing.Point(40, 76);
            this.uTextMSDS.Name = "uTextMSDS";
            this.uTextMSDS.ReadOnly = true;
            this.uTextMSDS.Size = new System.Drawing.Size(21, 21);
            this.uTextMSDS.TabIndex = 92;
            this.uTextMSDS.Visible = false;
            this.uTextMSDS.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMSDS_EditorButtonClick);
            // 
            // uLabelMSDS
            // 
            this.uLabelMSDS.Location = new System.Drawing.Point(12, 76);
            this.uLabelMSDS.Name = "uLabelMSDS";
            this.uLabelMSDS.Size = new System.Drawing.Size(21, 21);
            this.uLabelMSDS.TabIndex = 91;
            this.uLabelMSDS.Text = "ultraLabel2";
            this.uLabelMSDS.Visible = false;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(642, 52);
            this.uTextEtcDesc.Multiline = true;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(380, 65);
            this.uTextEtcDesc.TabIndex = 90;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(540, 52);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc.TabIndex = 89;
            this.uLabelEtc.Text = "ultraLabel2";
            // 
            // uTextApplyDevice
            // 
            this.uTextApplyDevice.Location = new System.Drawing.Point(642, 28);
            this.uTextApplyDevice.Name = "uTextApplyDevice";
            this.uTextApplyDevice.Size = new System.Drawing.Size(380, 21);
            this.uTextApplyDevice.TabIndex = 88;
            // 
            // uLabelApplyDevice
            // 
            this.uLabelApplyDevice.Location = new System.Drawing.Point(540, 28);
            this.uLabelApplyDevice.Name = "uLabelApplyDevice";
            this.uLabelApplyDevice.Size = new System.Drawing.Size(100, 20);
            this.uLabelApplyDevice.TabIndex = 87;
            this.uLabelApplyDevice.Text = "ultraLabel2";
            // 
            // uTextReqPurpose
            // 
            this.uTextReqPurpose.Location = new System.Drawing.Point(114, 52);
            this.uTextReqPurpose.Multiline = true;
            this.uTextReqPurpose.Name = "uTextReqPurpose";
            this.uTextReqPurpose.Size = new System.Drawing.Size(380, 65);
            this.uTextReqPurpose.TabIndex = 86;
            // 
            // uLabelRequestPurpose
            // 
            this.uLabelRequestPurpose.Location = new System.Drawing.Point(12, 52);
            this.uLabelRequestPurpose.Name = "uLabelRequestPurpose";
            this.uLabelRequestPurpose.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestPurpose.TabIndex = 85;
            this.uLabelRequestPurpose.Text = "ultraLabel2";
            // 
            // uLabelReqDate
            // 
            this.uLabelReqDate.Location = new System.Drawing.Point(328, 28);
            this.uLabelReqDate.Name = "uLabelReqDate";
            this.uLabelReqDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelReqDate.TabIndex = 84;
            this.uLabelReqDate.Text = "ultraLabel2";
            // 
            // uTextReqName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqName.Appearance = appearance19;
            this.uTextReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqName.Location = new System.Drawing.Point(218, 28);
            this.uTextReqName.Name = "uTextReqName";
            this.uTextReqName.ReadOnly = true;
            this.uTextReqName.Size = new System.Drawing.Size(100, 21);
            this.uTextReqName.TabIndex = 83;
            // 
            // uTextReqID
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReqID.Appearance = appearance20;
            this.uTextReqID.BackColor = System.Drawing.Color.PowderBlue;
            appearance3.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton8.Appearance = appearance3;
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReqID.ButtonsRight.Add(editorButton8);
            this.uTextReqID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReqID.Location = new System.Drawing.Point(114, 28);
            this.uTextReqID.Name = "uTextReqID";
            this.uTextReqID.Size = new System.Drawing.Size(100, 21);
            this.uTextReqID.TabIndex = 82;
            this.uTextReqID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReqID_KeyDown);
            this.uTextReqID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReqID_EditorButtonClick);
            // 
            // uLabelReqUser
            // 
            this.uLabelReqUser.Location = new System.Drawing.Point(12, 28);
            this.uLabelReqUser.Name = "uLabelReqUser";
            this.uLabelReqUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelReqUser.TabIndex = 81;
            this.uLabelReqUser.Text = "ultraLabel2";
            // 
            // uGroupBoxInfo
            // 
            this.uGroupBoxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxInfo.Controls.Add(this.uTextUnitCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelUnit);
            this.uGroupBoxInfo.Controls.Add(this.uTextSpecNo);
            this.uGroupBoxInfo.Controls.Add(this.uTextGRDate);
            this.uGroupBoxInfo.Controls.Add(this.uTextFloorPlanNo);
            this.uGroupBoxInfo.Controls.Add(this.uLabelDrawingNo);
            this.uGroupBoxInfo.Controls.Add(this.uTextMoldSeq);
            this.uGroupBoxInfo.Controls.Add(this.uLabelMoldSeq);
            this.uGroupBoxInfo.Controls.Add(this.uTextShipmentQty);
            this.uGroupBoxInfo.Controls.Add(this.uLabelAmount);
            this.uGroupBoxInfo.Controls.Add(this.uTextMaterialName);
            this.uGroupBoxInfo.Controls.Add(this.uTextMaterialCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelMaterial);
            this.uGroupBoxInfo.Controls.Add(this.uTextVendor);
            this.uGroupBoxInfo.Controls.Add(this.uLabelVendor);
            this.uGroupBoxInfo.Controls.Add(this.uLabelStorageDate);
            this.uGroupBoxInfo.Controls.Add(this.uTextStdNumber);
            this.uGroupBoxInfo.Controls.Add(this.uLabelStdNumber);
            this.uGroupBoxInfo.Controls.Add(this.uTextPlant);
            this.uGroupBoxInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxInfo.Location = new System.Drawing.Point(8, 8);
            this.uGroupBoxInfo.Name = "uGroupBoxInfo";
            this.uGroupBoxInfo.Size = new System.Drawing.Size(1040, 104);
            this.uGroupBoxInfo.TabIndex = 83;
            // 
            // uTextUnitCode
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnitCode.Appearance = appearance35;
            this.uTextUnitCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnitCode.Location = new System.Drawing.Point(804, 28);
            this.uTextUnitCode.Name = "uTextUnitCode";
            this.uTextUnitCode.ReadOnly = true;
            this.uTextUnitCode.Size = new System.Drawing.Size(100, 21);
            this.uTextUnitCode.TabIndex = 108;
            // 
            // uLabelUnit
            // 
            this.uLabelUnit.Location = new System.Drawing.Point(700, 28);
            this.uLabelUnit.Name = "uLabelUnit";
            this.uLabelUnit.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnit.TabIndex = 107;
            this.uLabelUnit.Text = "ultraLabel2";
            // 
            // uTextSpecNo
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Appearance = appearance26;
            this.uTextSpecNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Location = new System.Drawing.Point(906, 52);
            this.uTextSpecNo.Name = "uTextSpecNo";
            this.uTextSpecNo.ReadOnly = true;
            this.uTextSpecNo.Size = new System.Drawing.Size(100, 21);
            this.uTextSpecNo.TabIndex = 106;
            // 
            // uTextGRDate
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Appearance = appearance2;
            this.uTextGRDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Location = new System.Drawing.Point(592, 52);
            this.uTextGRDate.Name = "uTextGRDate";
            this.uTextGRDate.ReadOnly = true;
            this.uTextGRDate.Size = new System.Drawing.Size(100, 21);
            this.uTextGRDate.TabIndex = 105;
            // 
            // uTextFloorPlanNo
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Appearance = appearance32;
            this.uTextFloorPlanNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Location = new System.Drawing.Point(804, 52);
            this.uTextFloorPlanNo.Name = "uTextFloorPlanNo";
            this.uTextFloorPlanNo.ReadOnly = true;
            this.uTextFloorPlanNo.Size = new System.Drawing.Size(100, 21);
            this.uTextFloorPlanNo.TabIndex = 104;
            // 
            // uLabelDrawingNo
            // 
            this.uLabelDrawingNo.Location = new System.Drawing.Point(700, 52);
            this.uLabelDrawingNo.Name = "uLabelDrawingNo";
            this.uLabelDrawingNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelDrawingNo.TabIndex = 103;
            this.uLabelDrawingNo.Text = "ultraLabel2";
            // 
            // uTextMoldSeq
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Appearance = appearance38;
            this.uTextMoldSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Location = new System.Drawing.Point(380, 52);
            this.uTextMoldSeq.Name = "uTextMoldSeq";
            this.uTextMoldSeq.ReadOnly = true;
            this.uTextMoldSeq.Size = new System.Drawing.Size(100, 21);
            this.uTextMoldSeq.TabIndex = 102;
            // 
            // uLabelMoldSeq
            // 
            this.uLabelMoldSeq.Location = new System.Drawing.Point(276, 52);
            this.uLabelMoldSeq.Name = "uLabelMoldSeq";
            this.uLabelMoldSeq.Size = new System.Drawing.Size(100, 20);
            this.uLabelMoldSeq.TabIndex = 101;
            this.uLabelMoldSeq.Text = "ultraLabel2";
            // 
            // uTextShipmentQty
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentQty.Appearance = appearance54;
            this.uTextShipmentQty.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentQty.Location = new System.Drawing.Point(592, 28);
            this.uTextShipmentQty.Name = "uTextShipmentQty";
            this.uTextShipmentQty.ReadOnly = true;
            this.uTextShipmentQty.Size = new System.Drawing.Size(100, 21);
            this.uTextShipmentQty.TabIndex = 100;
            // 
            // uLabelAmount
            // 
            this.uLabelAmount.Location = new System.Drawing.Point(488, 28);
            this.uLabelAmount.Name = "uLabelAmount";
            this.uLabelAmount.Size = new System.Drawing.Size(100, 20);
            this.uLabelAmount.TabIndex = 99;
            this.uLabelAmount.Text = "ultraLabel2";
            // 
            // uTextMaterialName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance18;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(218, 76);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(800, 21);
            this.uTextMaterialName.TabIndex = 98;
            // 
            // uTextMaterialCode
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Appearance = appearance25;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Location = new System.Drawing.Point(116, 76);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.ReadOnly = true;
            this.uTextMaterialCode.Size = new System.Drawing.Size(100, 21);
            this.uTextMaterialCode.TabIndex = 97;
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(12, 76);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterial.TabIndex = 96;
            this.uLabelMaterial.Text = "ultraLabel2";
            // 
            // uTextVendor
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance40;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(116, 52);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(150, 21);
            this.uTextVendor.TabIndex = 95;
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(12, 52);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelVendor.TabIndex = 94;
            this.uLabelVendor.Text = "ultraLabel2";
            // 
            // uLabelStorageDate
            // 
            this.uLabelStorageDate.Location = new System.Drawing.Point(488, 52);
            this.uLabelStorageDate.Name = "uLabelStorageDate";
            this.uLabelStorageDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelStorageDate.TabIndex = 93;
            this.uLabelStorageDate.Text = "ultraLabel2";
            // 
            // uTextStdNumber
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance31;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(380, 28);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(100, 21);
            this.uTextStdNumber.TabIndex = 92;
            // 
            // uLabelStdNumber
            // 
            this.uLabelStdNumber.Location = new System.Drawing.Point(276, 28);
            this.uLabelStdNumber.Name = "uLabelStdNumber";
            this.uLabelStdNumber.Size = new System.Drawing.Size(100, 20);
            this.uLabelStdNumber.TabIndex = 91;
            this.uLabelStdNumber.Text = "ultraLabel2";
            // 
            // uTextPlant
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance23;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(116, 28);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(150, 21);
            this.uTextPlant.TabIndex = 90;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 89;
            this.uLabelPlant.Text = "ultraLabel2";
            // 
            // uGrid2
            // 
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance55;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance56;
            appearance57.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance57;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance58.BackColor2 = System.Drawing.SystemColors.Control;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance58;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance59;
            appearance60.BackColor = System.Drawing.SystemColors.Highlight;
            appearance60.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance60;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance61;
            appearance62.BorderColor = System.Drawing.Color.Silver;
            appearance62.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance62;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance63.BackColor = System.Drawing.SystemColors.Control;
            appearance63.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance63.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance63.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance63;
            appearance64.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance64;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance65;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance66.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance66;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 236);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(520, 388);
            this.uGrid2.TabIndex = 82;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            this.titleArea.Load += new System.EventHandler(this.frmINSZ0015_Activated);
            // 
            // uGridFile
            // 
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFile.DisplayLayout.Appearance = appearance24;
            this.uGridFile.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFile.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFile.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.uGridFile.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFile.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.uGridFile.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFile.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFile.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFile.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGridFile.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFile.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFile.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGridFile.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFile.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.uGridFile.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGridFile.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFile.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGridFile.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGridFile.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFile.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGridFile.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFile.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFile.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFile.Location = new System.Drawing.Point(532, 264);
            this.uGridFile.Name = "uGridFile";
            this.uGridFile.Size = new System.Drawing.Size(520, 168);
            this.uGridFile.TabIndex = 86;
            this.uGridFile.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFile_ClickCellButton);
            this.uGridFile.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridFile_DoubleClickCell);
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(532, 236);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 87;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // frmINSZ0015
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0015";
            this.Load += new System.EventHandler(this.frmINSZ0015_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0015_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0015_FormClosing);
            this.Resize += new System.EventHandler(this.frmINSZ0015_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridReturnList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInput)).EndInit();
            this.uGroupBoxInput.ResumeLayout(false);
            this.uGroupBoxInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextICP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMSDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyDevice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).EndInit();
            this.uGroupBoxInfo.ResumeLayout(false);
            this.uGroupBoxInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnitCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFile)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchReqToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchReqFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchReqDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchConsumableType;
        private Infragistics.Win.Misc.UltraLabel ulabelSearchConsumableType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchReqUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchReqUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchReqUser;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSearchCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchComplete;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnitCode;
        private Infragistics.Win.Misc.UltraLabel uLabelUnit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFloorPlanNo;
        private Infragistics.Win.Misc.UltraLabel uLabelDrawingNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShipmentQty;
        private Infragistics.Win.Misc.UltraLabel uLabelAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelStorageDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInput;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextICP;
        private Infragistics.Win.Misc.UltraLabel uLabelICP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMSDS;
        private Infragistics.Win.Misc.UltraLabel uLabelMSDS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApplyDevice;
        private Infragistics.Win.Misc.UltraLabel uLabelApplyDevice;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqPurpose;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestPurpose;
        private Infragistics.Win.Misc.UltraLabel uLabelReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqID;
        private Infragistics.Win.Misc.UltraLabel uLabelReqUser;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridReturnList;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFile;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
    }
}