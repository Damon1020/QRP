﻿namespace QRPINS.UI
{
    partial class frmINSZ0002
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem13 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem14 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton9 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton10 = new Infragistics.Win.UltraWinEditors.EditorButton("Up");
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton11 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton12 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton13 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton14 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton15 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton16 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton17 = new Infragistics.Win.UltraWinEditors.EditorButton("Down");
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton18 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem11 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem12 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem15 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem16 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem17 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem18 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem19 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem20 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton19 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton20 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0002));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonAVLMove = new Infragistics.Win.Misc.UltraButton();
            this.uCheckS1CompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextS1FileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile1 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS1EtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateS1WriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelS1WrateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudge2 = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionS1Result = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextS1UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextS1UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS1User = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uCheckS2CompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uButtonMove = new Infragistics.Win.Misc.UltraButton();
            this.uDateS2WriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelS2WrateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS2FileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS2EtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudge3 = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionS2Result = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextS2UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextS2UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS2User = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonQualMove = new Infragistics.Win.Misc.UltraButton();
            this.uTextQualMove = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelQualMove = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckS3CompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextS3EtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateS3WriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelS3WrateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS3FileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudge4 = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionS3Result = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextS3UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextS3UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS3User = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonReliabilityMove = new Infragistics.Win.Misc.UltraButton();
            this.uTextReliabilityMove = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReliabilityMove = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckS4CompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextS4EtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc4 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateS4WriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelS4WrateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS4FileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelJudge5 = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionS4Result = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextS4UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextS4UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS4User = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgressCheck1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgressCheck2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgressCheck3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProgressCheck4 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchReqUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchReqUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchReqUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchConsumableType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ulabelSearchConsumableType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProgStatus = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProgStatus = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPassFailFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPassFailFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchReqToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchReqFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchStorageNum = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGridFile = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxInput = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextReqDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSaveCheck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextICP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelICP = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMSDS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMSDS = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApplyDevice = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApplyDevice = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqPurpose = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRequestPurpose = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelReqDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReqName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReqUser = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGRNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUnitCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUnit = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpecNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGRDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFloorPlanNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDrawingNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMoldSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMoldSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uTextShipmentQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAmount = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStorageDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStdNumber = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridReturnList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uDateReturnDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelReturnDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReturnUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReturnUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReturnUser = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uOptionS4IngFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uOptionS3IngFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uOptionS2IngFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uOptionS1IngFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uDateReceiptDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelReceiptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReceiptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReceiptID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReceiptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextReturnReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReturnReason = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonReturn = new Infragistics.Win.Misc.UltraButton();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRegisterDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRegisterUser = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelJudge1 = new Infragistics.Win.Misc.UltraLabel();
            this.uOptionPassFailFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS1CompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1FileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1EtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS1WriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS1Result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserID)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS2CompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS2WriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2FileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2EtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS2Result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserID)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQualMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS3CompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3EtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS3WriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3FileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS3Result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserID)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReliabilityMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS4CompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4EtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS4WriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4FileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS4Result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProgStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPassFailFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInput)).BeginInit();
            this.uGroupBoxInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSaveCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextICP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMSDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyDevice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).BeginInit();
            this.uGroupBoxInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnitCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridReturnList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReturnDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS4IngFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS3IngFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS2IngFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS1IngFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonAVLMove);
            this.ultraTabPageControl1.Controls.Add(this.uCheckS1CompleteFlag);
            this.ultraTabPageControl1.Controls.Add(this.uTextS1FileName);
            this.ultraTabPageControl1.Controls.Add(this.uLabelAttachFile1);
            this.ultraTabPageControl1.Controls.Add(this.uTextS1EtcDesc);
            this.ultraTabPageControl1.Controls.Add(this.uLabelEtc1);
            this.ultraTabPageControl1.Controls.Add(this.uDateS1WriteDate);
            this.ultraTabPageControl1.Controls.Add(this.uLabelS1WrateDate);
            this.ultraTabPageControl1.Controls.Add(this.uLabelJudge2);
            this.ultraTabPageControl1.Controls.Add(this.uOptionS1Result);
            this.ultraTabPageControl1.Controls.Add(this.uTextS1UserName);
            this.ultraTabPageControl1.Controls.Add(this.uTextS1UserID);
            this.ultraTabPageControl1.Controls.Add(this.uLabelS1User);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(2, 24);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1026, 86);
            // 
            // uButtonAVLMove
            // 
            this.uButtonAVLMove.Location = new System.Drawing.Point(864, 20);
            this.uButtonAVLMove.Name = "uButtonAVLMove";
            this.uButtonAVLMove.Size = new System.Drawing.Size(128, 28);
            this.uButtonAVLMove.TabIndex = 134;
            this.uButtonAVLMove.Text = "ultraButton1";
            this.uButtonAVLMove.Click += new System.EventHandler(this.uButtonAVLMove_Click);
            // 
            // uCheckS1CompleteFlag
            // 
            this.uCheckS1CompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckS1CompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckS1CompleteFlag.Location = new System.Drawing.Point(584, 12);
            this.uCheckS1CompleteFlag.Name = "uCheckS1CompleteFlag";
            this.uCheckS1CompleteFlag.Size = new System.Drawing.Size(90, 20);
            this.uCheckS1CompleteFlag.TabIndex = 111;
            this.uCheckS1CompleteFlag.Text = "작성완료";
            this.uCheckS1CompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckS1CompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckS1CompleteFlag_CheckedValueChanged);
            this.uCheckS1CompleteFlag.EnabledChanged += new System.EventHandler(this.uCheckS1CompleteFlag_EnabledChanged);
            // 
            // uTextS1FileName
            // 
            appearance128.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance128.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance128;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Key = "Up";
            appearance129.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance129.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance129;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton2.Key = "Down";
            this.uTextS1FileName.ButtonsRight.Add(editorButton1);
            this.uTextS1FileName.ButtonsRight.Add(editorButton2);
            this.uTextS1FileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS1FileName.Location = new System.Drawing.Point(444, 36);
            this.uTextS1FileName.Name = "uTextS1FileName";
            this.uTextS1FileName.Size = new System.Drawing.Size(200, 21);
            this.uTextS1FileName.TabIndex = 110;
            this.uTextS1FileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS1FileName_EditorButtonClick);
            // 
            // uLabelAttachFile1
            // 
            this.uLabelAttachFile1.Location = new System.Drawing.Point(328, 36);
            this.uLabelAttachFile1.Name = "uLabelAttachFile1";
            this.uLabelAttachFile1.Size = new System.Drawing.Size(110, 20);
            this.uLabelAttachFile1.TabIndex = 109;
            this.uLabelAttachFile1.Text = "ultraLabel2";
            // 
            // uTextS1EtcDesc
            // 
            this.uTextS1EtcDesc.Location = new System.Drawing.Point(116, 60);
            this.uTextS1EtcDesc.Name = "uTextS1EtcDesc";
            this.uTextS1EtcDesc.Size = new System.Drawing.Size(880, 21);
            this.uTextS1EtcDesc.TabIndex = 108;
            // 
            // uLabelEtc1
            // 
            this.uLabelEtc1.Location = new System.Drawing.Point(12, 60);
            this.uLabelEtc1.Name = "uLabelEtc1";
            this.uLabelEtc1.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc1.TabIndex = 107;
            this.uLabelEtc1.Text = "ultraLabel2";
            // 
            // uDateS1WriteDate
            // 
            this.uDateS1WriteDate.Location = new System.Drawing.Point(116, 36);
            this.uDateS1WriteDate.Name = "uDateS1WriteDate";
            this.uDateS1WriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateS1WriteDate.TabIndex = 106;
            // 
            // uLabelS1WrateDate
            // 
            this.uLabelS1WrateDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelS1WrateDate.Name = "uLabelS1WrateDate";
            this.uLabelS1WrateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelS1WrateDate.TabIndex = 105;
            this.uLabelS1WrateDate.Text = "ultraLabel2";
            // 
            // uLabelJudge2
            // 
            this.uLabelJudge2.Location = new System.Drawing.Point(328, 12);
            this.uLabelJudge2.Name = "uLabelJudge2";
            this.uLabelJudge2.Size = new System.Drawing.Size(110, 20);
            this.uLabelJudge2.TabIndex = 104;
            this.uLabelJudge2.Text = "ultraLabel2";
            // 
            // uOptionS1Result
            // 
            appearance83.TextVAlignAsString = "Middle";
            this.uOptionS1Result.Appearance = appearance83;
            this.uOptionS1Result.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS1Result.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance84.TextVAlignAsString = "Middle";
            this.uOptionS1Result.ItemAppearance = appearance84;
            valueListItem13.DataValue = "OK";
            valueListItem13.DisplayText = "합격";
            valueListItem14.DataValue = "NG";
            valueListItem14.DisplayText = "불합격";
            this.uOptionS1Result.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem13,
            valueListItem14});
            this.uOptionS1Result.Location = new System.Drawing.Point(444, 12);
            this.uOptionS1Result.Name = "uOptionS1Result";
            this.uOptionS1Result.Size = new System.Drawing.Size(130, 20);
            this.uOptionS1Result.TabIndex = 103;
            this.uOptionS1Result.TextIndentation = 2;
            this.uOptionS1Result.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS1Result.ValueChanged += new System.EventHandler(this.uOptionS1Result_ValueChanged);
            // 
            // uTextS1UserName
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS1UserName.Appearance = appearance56;
            this.uTextS1UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS1UserName.Location = new System.Drawing.Point(218, 12);
            this.uTextS1UserName.Name = "uTextS1UserName";
            this.uTextS1UserName.ReadOnly = true;
            this.uTextS1UserName.Size = new System.Drawing.Size(100, 21);
            this.uTextS1UserName.TabIndex = 90;
            // 
            // uTextS1UserID
            // 
            appearance57.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS1UserID.Appearance = appearance57;
            this.uTextS1UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance58.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance58.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance58;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS1UserID.ButtonsRight.Add(editorButton3);
            this.uTextS1UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS1UserID.Location = new System.Drawing.Point(116, 12);
            this.uTextS1UserID.Name = "uTextS1UserID";
            this.uTextS1UserID.Size = new System.Drawing.Size(100, 21);
            this.uTextS1UserID.TabIndex = 89;
            this.uTextS1UserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextS1UserID_KeyDown);
            this.uTextS1UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS1UserID_EditorButtonClick);
            // 
            // uLabelS1User
            // 
            this.uLabelS1User.Location = new System.Drawing.Point(12, 12);
            this.uLabelS1User.Name = "uLabelS1User";
            this.uLabelS1User.Size = new System.Drawing.Size(100, 20);
            this.uLabelS1User.TabIndex = 88;
            this.uLabelS1User.Text = "ultraLabel2";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uCheckS2CompleteFlag);
            this.ultraTabPageControl2.Controls.Add(this.uButtonMove);
            this.ultraTabPageControl2.Controls.Add(this.uDateS2WriteDate);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2WrateDate);
            this.ultraTabPageControl2.Controls.Add(this.uTextS2FileName);
            this.ultraTabPageControl2.Controls.Add(this.uLabelAttachFile2);
            this.ultraTabPageControl2.Controls.Add(this.uTextS2EtcDesc);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc2);
            this.ultraTabPageControl2.Controls.Add(this.uLabelJudge3);
            this.ultraTabPageControl2.Controls.Add(this.uOptionS2Result);
            this.ultraTabPageControl2.Controls.Add(this.uTextS2UserName);
            this.ultraTabPageControl2.Controls.Add(this.uTextS2UserID);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2User);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1026, 86);
            // 
            // uCheckS2CompleteFlag
            // 
            this.uCheckS2CompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckS2CompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckS2CompleteFlag.Location = new System.Drawing.Point(584, 12);
            this.uCheckS2CompleteFlag.Name = "uCheckS2CompleteFlag";
            this.uCheckS2CompleteFlag.Size = new System.Drawing.Size(90, 20);
            this.uCheckS2CompleteFlag.TabIndex = 134;
            this.uCheckS2CompleteFlag.Text = "작성완료";
            this.uCheckS2CompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckS2CompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckS2CompleteFlag_CheckedValueChanged);
            this.uCheckS2CompleteFlag.EnabledChanged += new System.EventHandler(this.uCheckS2CompleteFlag_EnabledChanged);
            // 
            // uButtonMove
            // 
            this.uButtonMove.Location = new System.Drawing.Point(864, 20);
            this.uButtonMove.Name = "uButtonMove";
            this.uButtonMove.Size = new System.Drawing.Size(128, 28);
            this.uButtonMove.TabIndex = 133;
            this.uButtonMove.Text = "ultraButton1";
            this.uButtonMove.Click += new System.EventHandler(this.uButtonMove_Click);
            // 
            // uDateS2WriteDate
            // 
            this.uDateS2WriteDate.Location = new System.Drawing.Point(116, 36);
            this.uDateS2WriteDate.Name = "uDateS2WriteDate";
            this.uDateS2WriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateS2WriteDate.TabIndex = 132;
            // 
            // uLabelS2WrateDate
            // 
            this.uLabelS2WrateDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelS2WrateDate.Name = "uLabelS2WrateDate";
            this.uLabelS2WrateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelS2WrateDate.TabIndex = 131;
            this.uLabelS2WrateDate.Text = "ultraLabel2";
            // 
            // uTextS2FileName
            // 
            appearance46.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance46.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance46;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Key = "Up";
            appearance92.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance92.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance92;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "Down";
            this.uTextS2FileName.ButtonsRight.Add(editorButton4);
            this.uTextS2FileName.ButtonsRight.Add(editorButton5);
            this.uTextS2FileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS2FileName.Location = new System.Drawing.Point(444, 36);
            this.uTextS2FileName.Name = "uTextS2FileName";
            this.uTextS2FileName.Size = new System.Drawing.Size(200, 21);
            this.uTextS2FileName.TabIndex = 130;
            this.uTextS2FileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS2FileName_EditorButtonClick);
            // 
            // uLabelAttachFile2
            // 
            this.uLabelAttachFile2.Location = new System.Drawing.Point(328, 36);
            this.uLabelAttachFile2.Name = "uLabelAttachFile2";
            this.uLabelAttachFile2.Size = new System.Drawing.Size(112, 20);
            this.uLabelAttachFile2.TabIndex = 129;
            this.uLabelAttachFile2.Text = "ultraLabel2";
            // 
            // uTextS2EtcDesc
            // 
            this.uTextS2EtcDesc.Location = new System.Drawing.Point(116, 60);
            this.uTextS2EtcDesc.Name = "uTextS2EtcDesc";
            this.uTextS2EtcDesc.Size = new System.Drawing.Size(880, 21);
            this.uTextS2EtcDesc.TabIndex = 128;
            // 
            // uLabelEtc2
            // 
            this.uLabelEtc2.Location = new System.Drawing.Point(12, 60);
            this.uLabelEtc2.Name = "uLabelEtc2";
            this.uLabelEtc2.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc2.TabIndex = 127;
            this.uLabelEtc2.Text = "ultraLabel2";
            // 
            // uLabelJudge3
            // 
            this.uLabelJudge3.Location = new System.Drawing.Point(328, 12);
            this.uLabelJudge3.Name = "uLabelJudge3";
            this.uLabelJudge3.Size = new System.Drawing.Size(110, 20);
            this.uLabelJudge3.TabIndex = 122;
            this.uLabelJudge3.Text = "ultraLabel2";
            // 
            // uOptionS2Result
            // 
            appearance48.TextVAlignAsString = "Middle";
            this.uOptionS2Result.Appearance = appearance48;
            this.uOptionS2Result.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS2Result.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance52.TextVAlignAsString = "Middle";
            this.uOptionS2Result.ItemAppearance = appearance52;
            valueListItem7.DataValue = "OK";
            valueListItem7.DisplayText = "합격";
            valueListItem8.DataValue = "NG";
            valueListItem8.DisplayText = "불합격";
            this.uOptionS2Result.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8});
            this.uOptionS2Result.Location = new System.Drawing.Point(444, 12);
            this.uOptionS2Result.Name = "uOptionS2Result";
            this.uOptionS2Result.Size = new System.Drawing.Size(130, 20);
            this.uOptionS2Result.TabIndex = 121;
            this.uOptionS2Result.TextIndentation = 2;
            this.uOptionS2Result.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS2Result.ValueChanged += new System.EventHandler(this.uOptionS2Result_ValueChanged);
            // 
            // uTextS2UserName
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS2UserName.Appearance = appearance59;
            this.uTextS2UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS2UserName.Location = new System.Drawing.Point(218, 12);
            this.uTextS2UserName.Name = "uTextS2UserName";
            this.uTextS2UserName.ReadOnly = true;
            this.uTextS2UserName.Size = new System.Drawing.Size(100, 21);
            this.uTextS2UserName.TabIndex = 95;
            // 
            // uTextS2UserID
            // 
            appearance60.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS2UserID.Appearance = appearance60;
            this.uTextS2UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance61.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance61.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton6.Appearance = appearance61;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS2UserID.ButtonsRight.Add(editorButton6);
            this.uTextS2UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS2UserID.Location = new System.Drawing.Point(116, 12);
            this.uTextS2UserID.Name = "uTextS2UserID";
            this.uTextS2UserID.Size = new System.Drawing.Size(100, 21);
            this.uTextS2UserID.TabIndex = 94;
            this.uTextS2UserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextS2UserID_KeyDown);
            this.uTextS2UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS2UserID_EditorButtonClick);
            // 
            // uLabelS2User
            // 
            this.uLabelS2User.Location = new System.Drawing.Point(12, 12);
            this.uLabelS2User.Name = "uLabelS2User";
            this.uLabelS2User.Size = new System.Drawing.Size(100, 20);
            this.uLabelS2User.TabIndex = 93;
            this.uLabelS2User.Text = "ultraLabel2";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uButtonQualMove);
            this.ultraTabPageControl3.Controls.Add(this.uTextQualMove);
            this.ultraTabPageControl3.Controls.Add(this.uLabelQualMove);
            this.ultraTabPageControl3.Controls.Add(this.uCheckS3CompleteFlag);
            this.ultraTabPageControl3.Controls.Add(this.uTextS3EtcDesc);
            this.ultraTabPageControl3.Controls.Add(this.uLabelEtc3);
            this.ultraTabPageControl3.Controls.Add(this.uDateS3WriteDate);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3WrateDate);
            this.ultraTabPageControl3.Controls.Add(this.uTextS3FileName);
            this.ultraTabPageControl3.Controls.Add(this.uLabelAttachFile3);
            this.ultraTabPageControl3.Controls.Add(this.uLabelJudge4);
            this.ultraTabPageControl3.Controls.Add(this.uOptionS3Result);
            this.ultraTabPageControl3.Controls.Add(this.uTextS3UserName);
            this.ultraTabPageControl3.Controls.Add(this.uTextS3UserID);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3User);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1026, 86);
            // 
            // uButtonQualMove
            // 
            this.uButtonQualMove.Location = new System.Drawing.Point(892, 28);
            this.uButtonQualMove.Name = "uButtonQualMove";
            this.uButtonQualMove.Size = new System.Drawing.Size(104, 28);
            this.uButtonQualMove.TabIndex = 138;
            this.uButtonQualMove.Text = "ultraButton1";
            this.uButtonQualMove.Click += new System.EventHandler(this.uButtonQualMove_Click);
            // 
            // uTextQualMove
            // 
            this.uTextQualMove.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextQualMove.Location = new System.Drawing.Point(768, 32);
            this.uTextQualMove.Name = "uTextQualMove";
            this.uTextQualMove.Size = new System.Drawing.Size(120, 21);
            this.uTextQualMove.TabIndex = 137;
            // 
            // uLabelQualMove
            // 
            this.uLabelQualMove.Location = new System.Drawing.Point(652, 32);
            this.uLabelQualMove.Name = "uLabelQualMove";
            this.uLabelQualMove.Size = new System.Drawing.Size(110, 20);
            this.uLabelQualMove.TabIndex = 136;
            this.uLabelQualMove.Text = "ultraLabel2";
            // 
            // uCheckS3CompleteFlag
            // 
            this.uCheckS3CompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckS3CompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckS3CompleteFlag.Location = new System.Drawing.Point(584, 12);
            this.uCheckS3CompleteFlag.Name = "uCheckS3CompleteFlag";
            this.uCheckS3CompleteFlag.Size = new System.Drawing.Size(90, 20);
            this.uCheckS3CompleteFlag.TabIndex = 135;
            this.uCheckS3CompleteFlag.Text = "작성완료";
            this.uCheckS3CompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckS3CompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckS3CompleteFlag_CheckedValueChanged);
            this.uCheckS3CompleteFlag.EnabledChanged += new System.EventHandler(this.uCheckS3CompleteFlag_EnabledChanged);
            // 
            // uTextS3EtcDesc
            // 
            this.uTextS3EtcDesc.Location = new System.Drawing.Point(116, 60);
            this.uTextS3EtcDesc.Name = "uTextS3EtcDesc";
            this.uTextS3EtcDesc.Size = new System.Drawing.Size(880, 21);
            this.uTextS3EtcDesc.TabIndex = 134;
            // 
            // uLabelEtc3
            // 
            this.uLabelEtc3.Location = new System.Drawing.Point(12, 60);
            this.uLabelEtc3.Name = "uLabelEtc3";
            this.uLabelEtc3.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc3.TabIndex = 133;
            this.uLabelEtc3.Text = "ultraLabel2";
            // 
            // uDateS3WriteDate
            // 
            this.uDateS3WriteDate.Location = new System.Drawing.Point(116, 36);
            this.uDateS3WriteDate.Name = "uDateS3WriteDate";
            this.uDateS3WriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateS3WriteDate.TabIndex = 132;
            // 
            // uLabelS3WrateDate
            // 
            this.uLabelS3WrateDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelS3WrateDate.Name = "uLabelS3WrateDate";
            this.uLabelS3WrateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelS3WrateDate.TabIndex = 131;
            this.uLabelS3WrateDate.Text = "ultraLabel2";
            // 
            // uTextS3FileName
            // 
            appearance45.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance45.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance45;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton7.Key = "Up";
            appearance93.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance93.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton8.Appearance = appearance93;
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton8.Key = "Down";
            this.uTextS3FileName.ButtonsRight.Add(editorButton7);
            this.uTextS3FileName.ButtonsRight.Add(editorButton8);
            this.uTextS3FileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS3FileName.Location = new System.Drawing.Point(444, 36);
            this.uTextS3FileName.Name = "uTextS3FileName";
            this.uTextS3FileName.Size = new System.Drawing.Size(200, 21);
            this.uTextS3FileName.TabIndex = 130;
            this.uTextS3FileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS3FileName_EditorButtonClick);
            // 
            // uLabelAttachFile3
            // 
            this.uLabelAttachFile3.Location = new System.Drawing.Point(328, 36);
            this.uLabelAttachFile3.Name = "uLabelAttachFile3";
            this.uLabelAttachFile3.Size = new System.Drawing.Size(112, 20);
            this.uLabelAttachFile3.TabIndex = 129;
            this.uLabelAttachFile3.Text = "ultraLabel2";
            // 
            // uLabelJudge4
            // 
            this.uLabelJudge4.Location = new System.Drawing.Point(328, 12);
            this.uLabelJudge4.Name = "uLabelJudge4";
            this.uLabelJudge4.Size = new System.Drawing.Size(110, 20);
            this.uLabelJudge4.TabIndex = 124;
            this.uLabelJudge4.Text = "ultraLabel2";
            // 
            // uOptionS3Result
            // 
            appearance49.TextVAlignAsString = "Middle";
            this.uOptionS3Result.Appearance = appearance49;
            this.uOptionS3Result.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS3Result.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance53.TextVAlignAsString = "Middle";
            this.uOptionS3Result.ItemAppearance = appearance53;
            valueListItem9.DataValue = "OK";
            valueListItem9.DisplayText = "합격";
            valueListItem10.DataValue = "NG";
            valueListItem10.DisplayText = "불합격";
            this.uOptionS3Result.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem9,
            valueListItem10});
            this.uOptionS3Result.Location = new System.Drawing.Point(444, 12);
            this.uOptionS3Result.Name = "uOptionS3Result";
            this.uOptionS3Result.Size = new System.Drawing.Size(130, 20);
            this.uOptionS3Result.TabIndex = 123;
            this.uOptionS3Result.TextIndentation = 2;
            this.uOptionS3Result.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS3Result.ValueChanged += new System.EventHandler(this.uOptionS3Result_ValueChanged);
            // 
            // uTextS3UserName
            // 
            appearance62.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS3UserName.Appearance = appearance62;
            this.uTextS3UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS3UserName.Location = new System.Drawing.Point(218, 12);
            this.uTextS3UserName.Name = "uTextS3UserName";
            this.uTextS3UserName.ReadOnly = true;
            this.uTextS3UserName.Size = new System.Drawing.Size(100, 21);
            this.uTextS3UserName.TabIndex = 100;
            // 
            // uTextS3UserID
            // 
            appearance63.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS3UserID.Appearance = appearance63;
            this.uTextS3UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance64.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance64.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton9.Appearance = appearance64;
            editorButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS3UserID.ButtonsRight.Add(editorButton9);
            this.uTextS3UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS3UserID.Location = new System.Drawing.Point(116, 12);
            this.uTextS3UserID.Name = "uTextS3UserID";
            this.uTextS3UserID.Size = new System.Drawing.Size(100, 21);
            this.uTextS3UserID.TabIndex = 99;
            this.uTextS3UserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextS3UserID_KeyDown);
            this.uTextS3UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS3UserID_EditorButtonClick);
            // 
            // uLabelS3User
            // 
            this.uLabelS3User.Location = new System.Drawing.Point(12, 12);
            this.uLabelS3User.Name = "uLabelS3User";
            this.uLabelS3User.Size = new System.Drawing.Size(100, 20);
            this.uLabelS3User.TabIndex = 98;
            this.uLabelS3User.Text = "ultraLabel2";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uButtonReliabilityMove);
            this.ultraTabPageControl4.Controls.Add(this.uTextReliabilityMove);
            this.ultraTabPageControl4.Controls.Add(this.uLabelReliabilityMove);
            this.ultraTabPageControl4.Controls.Add(this.uCheckS4CompleteFlag);
            this.ultraTabPageControl4.Controls.Add(this.uTextS4EtcDesc);
            this.ultraTabPageControl4.Controls.Add(this.uLabelEtc4);
            this.ultraTabPageControl4.Controls.Add(this.uDateS4WriteDate);
            this.ultraTabPageControl4.Controls.Add(this.uLabelS4WrateDate);
            this.ultraTabPageControl4.Controls.Add(this.uTextS4FileName);
            this.ultraTabPageControl4.Controls.Add(this.uLabelAttachFile4);
            this.ultraTabPageControl4.Controls.Add(this.uLabelJudge5);
            this.ultraTabPageControl4.Controls.Add(this.uOptionS4Result);
            this.ultraTabPageControl4.Controls.Add(this.uTextS4UserName);
            this.ultraTabPageControl4.Controls.Add(this.uTextS4UserID);
            this.ultraTabPageControl4.Controls.Add(this.uLabelS4User);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1026, 86);
            // 
            // uButtonReliabilityMove
            // 
            this.uButtonReliabilityMove.Location = new System.Drawing.Point(892, 28);
            this.uButtonReliabilityMove.Name = "uButtonReliabilityMove";
            this.uButtonReliabilityMove.Size = new System.Drawing.Size(104, 28);
            this.uButtonReliabilityMove.TabIndex = 140;
            this.uButtonReliabilityMove.Text = "ultraButton1";
            this.uButtonReliabilityMove.Click += new System.EventHandler(this.uButtonReliabilityMove_Click);
            // 
            // uTextReliabilityMove
            // 
            this.uTextReliabilityMove.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReliabilityMove.Location = new System.Drawing.Point(768, 32);
            this.uTextReliabilityMove.Name = "uTextReliabilityMove";
            this.uTextReliabilityMove.Size = new System.Drawing.Size(120, 21);
            this.uTextReliabilityMove.TabIndex = 139;
            // 
            // uLabelReliabilityMove
            // 
            this.uLabelReliabilityMove.Location = new System.Drawing.Point(652, 32);
            this.uLabelReliabilityMove.Name = "uLabelReliabilityMove";
            this.uLabelReliabilityMove.Size = new System.Drawing.Size(110, 20);
            this.uLabelReliabilityMove.TabIndex = 138;
            this.uLabelReliabilityMove.Text = "ultraLabel2";
            // 
            // uCheckS4CompleteFlag
            // 
            this.uCheckS4CompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckS4CompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckS4CompleteFlag.Location = new System.Drawing.Point(584, 12);
            this.uCheckS4CompleteFlag.Name = "uCheckS4CompleteFlag";
            this.uCheckS4CompleteFlag.Size = new System.Drawing.Size(90, 20);
            this.uCheckS4CompleteFlag.TabIndex = 137;
            this.uCheckS4CompleteFlag.Text = "작성완료";
            this.uCheckS4CompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckS4CompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckS4CompleteFlag_CheckedValueChanged);
            this.uCheckS4CompleteFlag.EnabledChanged += new System.EventHandler(this.uCheckS4CompleteFlag_EnabledChanged);
            // 
            // uTextS4EtcDesc
            // 
            this.uTextS4EtcDesc.Location = new System.Drawing.Point(116, 60);
            this.uTextS4EtcDesc.Name = "uTextS4EtcDesc";
            this.uTextS4EtcDesc.Size = new System.Drawing.Size(880, 21);
            this.uTextS4EtcDesc.TabIndex = 136;
            // 
            // uLabelEtc4
            // 
            this.uLabelEtc4.Location = new System.Drawing.Point(12, 60);
            this.uLabelEtc4.Name = "uLabelEtc4";
            this.uLabelEtc4.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc4.TabIndex = 135;
            this.uLabelEtc4.Text = "ultraLabel2";
            // 
            // uDateS4WriteDate
            // 
            this.uDateS4WriteDate.Location = new System.Drawing.Point(116, 36);
            this.uDateS4WriteDate.Name = "uDateS4WriteDate";
            this.uDateS4WriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateS4WriteDate.TabIndex = 134;
            // 
            // uLabelS4WrateDate
            // 
            this.uLabelS4WrateDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelS4WrateDate.Name = "uLabelS4WrateDate";
            this.uLabelS4WrateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelS4WrateDate.TabIndex = 133;
            this.uLabelS4WrateDate.Text = "ultraLabel2";
            // 
            // uTextS4FileName
            // 
            appearance24.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance24.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton10.Appearance = appearance24;
            editorButton10.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton10.Key = "Up";
            appearance94.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance94.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton11.Appearance = appearance94;
            editorButton11.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton11.Key = "Down";
            this.uTextS4FileName.ButtonsRight.Add(editorButton10);
            this.uTextS4FileName.ButtonsRight.Add(editorButton11);
            this.uTextS4FileName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS4FileName.Location = new System.Drawing.Point(444, 36);
            this.uTextS4FileName.Name = "uTextS4FileName";
            this.uTextS4FileName.Size = new System.Drawing.Size(200, 21);
            this.uTextS4FileName.TabIndex = 132;
            this.uTextS4FileName.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS4FileName_EditorButtonClick);
            // 
            // uLabelAttachFile4
            // 
            this.uLabelAttachFile4.Location = new System.Drawing.Point(328, 36);
            this.uLabelAttachFile4.Name = "uLabelAttachFile4";
            this.uLabelAttachFile4.Size = new System.Drawing.Size(112, 20);
            this.uLabelAttachFile4.TabIndex = 131;
            this.uLabelAttachFile4.Text = "ultraLabel2";
            // 
            // uLabelJudge5
            // 
            this.uLabelJudge5.Location = new System.Drawing.Point(328, 12);
            this.uLabelJudge5.Name = "uLabelJudge5";
            this.uLabelJudge5.Size = new System.Drawing.Size(110, 20);
            this.uLabelJudge5.TabIndex = 126;
            this.uLabelJudge5.Text = "ultraLabel2";
            // 
            // uOptionS4Result
            // 
            appearance50.TextVAlignAsString = "Middle";
            this.uOptionS4Result.Appearance = appearance50;
            this.uOptionS4Result.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS4Result.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance117.TextVAlignAsString = "Middle";
            this.uOptionS4Result.ItemAppearance = appearance117;
            valueListItem1.DataValue = "OK";
            valueListItem1.DisplayText = "합격";
            valueListItem2.DataValue = "NG";
            valueListItem2.DisplayText = "불합격";
            this.uOptionS4Result.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionS4Result.Location = new System.Drawing.Point(444, 12);
            this.uOptionS4Result.Name = "uOptionS4Result";
            this.uOptionS4Result.Size = new System.Drawing.Size(130, 20);
            this.uOptionS4Result.TabIndex = 125;
            this.uOptionS4Result.TextIndentation = 2;
            this.uOptionS4Result.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS4Result.ValueChanged += new System.EventHandler(this.uOptionS4Result_ValueChanged);
            // 
            // uTextS4UserName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS4UserName.Appearance = appearance21;
            this.uTextS4UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS4UserName.Location = new System.Drawing.Point(218, 12);
            this.uTextS4UserName.Name = "uTextS4UserName";
            this.uTextS4UserName.ReadOnly = true;
            this.uTextS4UserName.Size = new System.Drawing.Size(100, 21);
            this.uTextS4UserName.TabIndex = 105;
            // 
            // uTextS4UserID
            // 
            appearance42.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS4UserID.Appearance = appearance42;
            this.uTextS4UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance126.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance126.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton12.Appearance = appearance126;
            editorButton12.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS4UserID.ButtonsRight.Add(editorButton12);
            this.uTextS4UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS4UserID.Location = new System.Drawing.Point(116, 12);
            this.uTextS4UserID.Name = "uTextS4UserID";
            this.uTextS4UserID.Size = new System.Drawing.Size(100, 21);
            this.uTextS4UserID.TabIndex = 104;
            this.uTextS4UserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextS4UserID_KeyDown);
            this.uTextS4UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextS4UserID_EditorButtonClick);
            // 
            // uLabelS4User
            // 
            this.uLabelS4User.Location = new System.Drawing.Point(12, 12);
            this.uLabelS4User.Name = "uLabelS4User";
            this.uLabelS4User.Size = new System.Drawing.Size(100, 20);
            this.uLabelS4User.TabIndex = 103;
            this.uLabelS4User.Text = "ultraLabel2";
            // 
            // uLabelProgressCheck1
            // 
            this.uLabelProgressCheck1.Location = new System.Drawing.Point(12, 28);
            this.uLabelProgressCheck1.Name = "uLabelProgressCheck1";
            this.uLabelProgressCheck1.Size = new System.Drawing.Size(120, 20);
            this.uLabelProgressCheck1.TabIndex = 27;
            this.uLabelProgressCheck1.Text = "ultraLabel1";
            // 
            // uLabelProgressCheck2
            // 
            this.uLabelProgressCheck2.Location = new System.Drawing.Point(284, 28);
            this.uLabelProgressCheck2.Name = "uLabelProgressCheck2";
            this.uLabelProgressCheck2.Size = new System.Drawing.Size(132, 20);
            this.uLabelProgressCheck2.TabIndex = 91;
            this.uLabelProgressCheck2.Text = "ultraLabel4";
            // 
            // uLabelProgressCheck3
            // 
            this.uLabelProgressCheck3.Location = new System.Drawing.Point(12, 52);
            this.uLabelProgressCheck3.Name = "uLabelProgressCheck3";
            this.uLabelProgressCheck3.Size = new System.Drawing.Size(120, 20);
            this.uLabelProgressCheck3.TabIndex = 96;
            this.uLabelProgressCheck3.Text = "ultraLabel4";
            // 
            // uLabelProgressCheck4
            // 
            this.uLabelProgressCheck4.Location = new System.Drawing.Point(284, 52);
            this.uLabelProgressCheck4.Name = "uLabelProgressCheck4";
            this.uLabelProgressCheck4.Size = new System.Drawing.Size(132, 20);
            this.uLabelProgressCheck4.TabIndex = 101;
            this.uLabelProgressCheck4.Text = "ultraLabel4";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraButton1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchReqUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchReqUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchReqUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.ulabelSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProgStatus);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProgStatus);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPassFailFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPassFailFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchReqToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchReqFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRequestDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStdNumber);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStorageNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 85);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextSearchReqUserID
            // 
            appearance22.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance22.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton13.Appearance = appearance22;
            editorButton13.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchReqUserID.ButtonsRight.Add(editorButton13);
            this.uTextSearchReqUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchReqUserID.Location = new System.Drawing.Point(380, 60);
            this.uTextSearchReqUserID.Name = "uTextSearchReqUserID";
            this.uTextSearchReqUserID.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchReqUserID.TabIndex = 48;
            this.uTextSearchReqUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchReqUserID_KeyDown);
            this.uTextSearchReqUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchReqUserID_EditorButtonClick);
            // 
            // uTextSearchReqUserName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchReqUserName.Appearance = appearance15;
            this.uTextSearchReqUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchReqUserName.Location = new System.Drawing.Point(493, 60);
            this.uTextSearchReqUserName.Name = "uTextSearchReqUserName";
            this.uTextSearchReqUserName.ReadOnly = true;
            this.uTextSearchReqUserName.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchReqUserName.TabIndex = 47;
            // 
            // uLabelSearchReqUser
            // 
            this.uLabelSearchReqUser.Location = new System.Drawing.Point(276, 60);
            this.uLabelSearchReqUser.Name = "uLabelSearchReqUser";
            this.uLabelSearchReqUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchReqUser.TabIndex = 46;
            this.uLabelSearchReqUser.Text = "ultraLabel2";
            // 
            // uComboSearchConsumableType
            // 
            this.uComboSearchConsumableType.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchConsumableType.Name = "uComboSearchConsumableType";
            this.uComboSearchConsumableType.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchConsumableType.TabIndex = 45;
            this.uComboSearchConsumableType.Text = "ultraComboEditor1";
            // 
            // ulabelSearchConsumableType
            // 
            this.ulabelSearchConsumableType.Location = new System.Drawing.Point(12, 36);
            this.ulabelSearchConsumableType.Name = "ulabelSearchConsumableType";
            this.ulabelSearchConsumableType.Size = new System.Drawing.Size(100, 20);
            this.ulabelSearchConsumableType.TabIndex = 44;
            this.ulabelSearchConsumableType.Text = "ultraLabel1";
            // 
            // uComboSearchProgStatus
            // 
            this.uComboSearchProgStatus.Location = new System.Drawing.Point(848, 36);
            this.uComboSearchProgStatus.Name = "uComboSearchProgStatus";
            this.uComboSearchProgStatus.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchProgStatus.TabIndex = 28;
            this.uComboSearchProgStatus.Text = "ultraComboEditor2";
            // 
            // uLabelSearchProgStatus
            // 
            this.uLabelSearchProgStatus.Location = new System.Drawing.Point(744, 36);
            this.uLabelSearchProgStatus.Name = "uLabelSearchProgStatus";
            this.uLabelSearchProgStatus.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProgStatus.TabIndex = 27;
            this.uLabelSearchProgStatus.Text = "ultraLabel2";
            // 
            // uComboSearchPassFailFlag
            // 
            this.uComboSearchPassFailFlag.Location = new System.Drawing.Point(848, 12);
            this.uComboSearchPassFailFlag.Name = "uComboSearchPassFailFlag";
            this.uComboSearchPassFailFlag.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchPassFailFlag.TabIndex = 26;
            this.uComboSearchPassFailFlag.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPassFailFlag
            // 
            this.uLabelSearchPassFailFlag.Location = new System.Drawing.Point(744, 12);
            this.uLabelSearchPassFailFlag.Name = "uLabelSearchPassFailFlag";
            this.uLabelSearchPassFailFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPassFailFlag.TabIndex = 25;
            this.uLabelSearchPassFailFlag.Text = "ultraLabel1";
            // 
            // uDateSearchReqToDate
            // 
            this.uDateSearchReqToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchReqToDate.Location = new System.Drawing.Point(964, 60);
            this.uDateSearchReqToDate.Name = "uDateSearchReqToDate";
            this.uDateSearchReqToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchReqToDate.TabIndex = 24;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(948, 60);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 23;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchReqFromDate
            // 
            this.uDateSearchReqFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchReqFromDate.Location = new System.Drawing.Point(848, 60);
            this.uDateSearchReqFromDate.Name = "uDateSearchReqFromDate";
            this.uDateSearchReqFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchReqFromDate.TabIndex = 22;
            // 
            // uLabelSearchRequestDate
            // 
            this.uLabelSearchRequestDate.Location = new System.Drawing.Point(744, 60);
            this.uLabelSearchRequestDate.Name = "uLabelSearchRequestDate";
            this.uLabelSearchRequestDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchRequestDate.TabIndex = 21;
            this.uLabelSearchRequestDate.Text = "ultraLabel2";
            // 
            // uTextSearchVendorName
            // 
            this.uTextSearchVendorName.Location = new System.Drawing.Point(493, 36);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchVendorName.TabIndex = 20;
            // 
            // uTextSearchVendorCode
            // 
            appearance41.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance41.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton14.Appearance = appearance41;
            editorButton14.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton14);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(380, 36);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorCode.TabIndex = 19;
            this.uTextSearchVendorCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchVendorCode_KeyDown);
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(276, 36);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 18;
            this.uLabelSearchCustomer.Text = "ultraLabel2";
            // 
            // uTextSearchMaterialName
            // 
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(493, 12);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchMaterialName.TabIndex = 11;
            // 
            // uTextSearchMaterialCode
            // 
            appearance119.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance119.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton15.Appearance = appearance119;
            editorButton15.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton15);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialCode.TabIndex = 10;
            this.uTextSearchMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMaterialCode_KeyDown);
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick);
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterial.TabIndex = 9;
            this.uLabelSearchMaterial.Text = "ultraLabel2";
            // 
            // uTextSearchStdNumber
            // 
            this.uTextSearchStdNumber.Location = new System.Drawing.Point(116, 60);
            this.uTextSearchStdNumber.Name = "uTextSearchStdNumber";
            this.uTextSearchStdNumber.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchStdNumber.TabIndex = 8;
            // 
            // uLabelSearchStorageNum
            // 
            this.uLabelSearchStorageNum.Location = new System.Drawing.Point(12, 60);
            this.uLabelSearchStorageNum.Name = "uLabelSearchStorageNum";
            this.uLabelSearchStorageNum.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStorageNum.TabIndex = 7;
            this.uLabelSearchStorageNum.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridHeader
            // 
            this.uGridHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHeader.DisplayLayout.Appearance = appearance16;
            this.uGridHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHeader.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHeader.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHeader.DisplayLayout.Override.CellAppearance = appearance11;
            this.uGridHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHeader.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.uGridHeader.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridHeader.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance127.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance127;
            this.uGridHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHeader.Location = new System.Drawing.Point(0, 125);
            this.uGridHeader.Name = "uGridHeader";
            this.uGridHeader.Size = new System.Drawing.Size(1060, 695);
            this.uGridHeader.TabIndex = 2;
            this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridHeader_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 655);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 190);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 655);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridFile);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxInput);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxInfo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridReturnList);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateReturnDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReturnDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReturnUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReturnUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReturnUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridDetail);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReturnReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReturnReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonReturn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 635);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGridFile
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFile.DisplayLayout.Appearance = appearance4;
            this.uGridFile.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFile.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance33.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFile.DisplayLayout.GroupByBox.BandLabelAppearance = appearance33;
            this.uGridFile.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance34.BackColor2 = System.Drawing.SystemColors.Control;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFile.DisplayLayout.GroupByBox.PromptAppearance = appearance34;
            this.uGridFile.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFile.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFile.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance37.BackColor = System.Drawing.SystemColors.Highlight;
            appearance37.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFile.DisplayLayout.Override.ActiveRowAppearance = appearance37;
            this.uGridFile.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFile.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.Override.CardAreaAppearance = appearance39;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFile.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGridFile.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFile.DisplayLayout.Override.CellPadding = 0;
            appearance95.BackColor = System.Drawing.SystemColors.Control;
            appearance95.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance95.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFile.DisplayLayout.Override.GroupByRowAppearance = appearance95;
            appearance96.TextHAlignAsString = "Left";
            this.uGridFile.DisplayLayout.Override.HeaderAppearance = appearance96;
            this.uGridFile.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFile.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.Color.Silver;
            this.uGridFile.DisplayLayout.Override.RowAppearance = appearance97;
            this.uGridFile.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance118.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFile.DisplayLayout.Override.TemplateAddRowAppearance = appearance118;
            this.uGridFile.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFile.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFile.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFile.Location = new System.Drawing.Point(527, 188);
            this.uGridFile.Name = "uGridFile";
            this.uGridFile.Size = new System.Drawing.Size(515, 90);
            this.uGridFile.TabIndex = 154;
            this.uGridFile.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridFile_DoubleClickCell);
            // 
            // uGroupBoxInput
            // 
            this.uGroupBoxInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxInput.Controls.Add(this.uTextReqDate);
            this.uGroupBoxInput.Controls.Add(this.uTextSaveCheck);
            this.uGroupBoxInput.Controls.Add(this.uTextICP);
            this.uGroupBoxInput.Controls.Add(this.uTextReqNo);
            this.uGroupBoxInput.Controls.Add(this.uTextReqSeq);
            this.uGroupBoxInput.Controls.Add(this.uLabelICP);
            this.uGroupBoxInput.Controls.Add(this.uTextMSDS);
            this.uGroupBoxInput.Controls.Add(this.uLabelMSDS);
            this.uGroupBoxInput.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxInput.Controls.Add(this.uLabelEtc);
            this.uGroupBoxInput.Controls.Add(this.uTextApplyDevice);
            this.uGroupBoxInput.Controls.Add(this.uLabelApplyDevice);
            this.uGroupBoxInput.Controls.Add(this.uTextReqPurpose);
            this.uGroupBoxInput.Controls.Add(this.uLabelRequestPurpose);
            this.uGroupBoxInput.Controls.Add(this.uLabelReqDate);
            this.uGroupBoxInput.Controls.Add(this.uTextReqName);
            this.uGroupBoxInput.Controls.Add(this.uTextReqID);
            this.uGroupBoxInput.Controls.Add(this.uLabelReqUser);
            this.uGroupBoxInput.Location = new System.Drawing.Point(7, 112);
            this.uGroupBoxInput.Name = "uGroupBoxInput";
            this.uGroupBoxInput.Size = new System.Drawing.Size(1040, 76);
            this.uGroupBoxInput.TabIndex = 153;
            // 
            // uTextReqDate
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqDate.Appearance = appearance19;
            this.uTextReqDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqDate.Location = new System.Drawing.Point(432, 28);
            this.uTextReqDate.Name = "uTextReqDate";
            this.uTextReqDate.ReadOnly = true;
            this.uTextReqDate.Size = new System.Drawing.Size(100, 21);
            this.uTextReqDate.TabIndex = 146;
            // 
            // uTextSaveCheck
            // 
            appearance120.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSaveCheck.Appearance = appearance120;
            this.uTextSaveCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSaveCheck.Location = new System.Drawing.Point(1012, 48);
            this.uTextSaveCheck.Name = "uTextSaveCheck";
            this.uTextSaveCheck.ReadOnly = true;
            this.uTextSaveCheck.Size = new System.Drawing.Size(21, 21);
            this.uTextSaveCheck.TabIndex = 145;
            this.uTextSaveCheck.Visible = false;
            // 
            // uTextICP
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextICP.Appearance = appearance29;
            this.uTextICP.BackColor = System.Drawing.Color.Gainsboro;
            appearance51.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance51.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton16.Appearance = appearance51;
            editorButton16.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton16.Key = "Down";
            this.uTextICP.ButtonsRight.Add(editorButton16);
            this.uTextICP.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextICP.Location = new System.Drawing.Point(984, 4);
            this.uTextICP.Name = "uTextICP";
            this.uTextICP.ReadOnly = true;
            this.uTextICP.Size = new System.Drawing.Size(20, 21);
            this.uTextICP.TabIndex = 99;
            this.uTextICP.Visible = false;
            this.uTextICP.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextICP_EditorButtonClick);
            // 
            // uTextReqNo
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Appearance = appearance3;
            this.uTextReqNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Location = new System.Drawing.Point(1012, 8);
            this.uTextReqNo.Name = "uTextReqNo";
            this.uTextReqNo.ReadOnly = true;
            this.uTextReqNo.Size = new System.Drawing.Size(21, 21);
            this.uTextReqNo.TabIndex = 144;
            this.uTextReqNo.Visible = false;
            // 
            // uTextReqSeq
            // 
            appearance98.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Appearance = appearance98;
            this.uTextReqSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Location = new System.Drawing.Point(1012, 28);
            this.uTextReqSeq.Name = "uTextReqSeq";
            this.uTextReqSeq.ReadOnly = true;
            this.uTextReqSeq.Size = new System.Drawing.Size(21, 21);
            this.uTextReqSeq.TabIndex = 143;
            this.uTextReqSeq.Visible = false;
            // 
            // uLabelICP
            // 
            this.uLabelICP.Location = new System.Drawing.Point(960, 4);
            this.uLabelICP.Name = "uLabelICP";
            this.uLabelICP.Size = new System.Drawing.Size(20, 20);
            this.uLabelICP.TabIndex = 98;
            this.uLabelICP.Text = "ultraLabel2";
            this.uLabelICP.Visible = false;
            // 
            // uTextMSDS
            // 
            appearance115.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMSDS.Appearance = appearance115;
            this.uTextMSDS.BackColor = System.Drawing.Color.Gainsboro;
            appearance47.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance47.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton17.Appearance = appearance47;
            editorButton17.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton17.Key = "Down";
            this.uTextMSDS.ButtonsRight.Add(editorButton17);
            this.uTextMSDS.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMSDS.Location = new System.Drawing.Point(936, 4);
            this.uTextMSDS.Name = "uTextMSDS";
            this.uTextMSDS.ReadOnly = true;
            this.uTextMSDS.Size = new System.Drawing.Size(20, 21);
            this.uTextMSDS.TabIndex = 97;
            this.uTextMSDS.Visible = false;
            this.uTextMSDS.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMSDS_EditorButtonClick);
            // 
            // uLabelMSDS
            // 
            this.uLabelMSDS.Location = new System.Drawing.Point(912, 4);
            this.uLabelMSDS.Name = "uLabelMSDS";
            this.uLabelMSDS.Size = new System.Drawing.Size(20, 20);
            this.uLabelMSDS.TabIndex = 96;
            this.uLabelMSDS.Text = "ultraLabel2";
            this.uLabelMSDS.Visible = false;
            // 
            // uTextEtcDesc
            // 
            appearance123.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtcDesc.Appearance = appearance123;
            this.uTextEtcDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtcDesc.Location = new System.Drawing.Point(644, 50);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.ReadOnly = true;
            this.uTextEtcDesc.Size = new System.Drawing.Size(350, 21);
            this.uTextEtcDesc.TabIndex = 90;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(540, 50);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 21);
            this.uLabelEtc.TabIndex = 89;
            this.uLabelEtc.Text = "ultraLabel2";
            // 
            // uTextApplyDevice
            // 
            appearance122.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApplyDevice.Appearance = appearance122;
            this.uTextApplyDevice.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApplyDevice.Location = new System.Drawing.Point(114, 50);
            this.uTextApplyDevice.Name = "uTextApplyDevice";
            this.uTextApplyDevice.ReadOnly = true;
            this.uTextApplyDevice.Size = new System.Drawing.Size(350, 21);
            this.uTextApplyDevice.TabIndex = 88;
            // 
            // uLabelApplyDevice
            // 
            this.uLabelApplyDevice.Location = new System.Drawing.Point(12, 50);
            this.uLabelApplyDevice.Name = "uLabelApplyDevice";
            this.uLabelApplyDevice.Size = new System.Drawing.Size(100, 21);
            this.uLabelApplyDevice.TabIndex = 87;
            this.uLabelApplyDevice.Text = "ultraLabel2";
            // 
            // uTextReqPurpose
            // 
            appearance124.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqPurpose.Appearance = appearance124;
            this.uTextReqPurpose.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqPurpose.Location = new System.Drawing.Point(644, 28);
            this.uTextReqPurpose.Name = "uTextReqPurpose";
            this.uTextReqPurpose.ReadOnly = true;
            this.uTextReqPurpose.Size = new System.Drawing.Size(350, 21);
            this.uTextReqPurpose.TabIndex = 86;
            // 
            // uLabelRequestPurpose
            // 
            this.uLabelRequestPurpose.Location = new System.Drawing.Point(540, 28);
            this.uLabelRequestPurpose.Name = "uLabelRequestPurpose";
            this.uLabelRequestPurpose.Size = new System.Drawing.Size(100, 20);
            this.uLabelRequestPurpose.TabIndex = 85;
            this.uLabelRequestPurpose.Text = "ultraLabel2";
            // 
            // uLabelReqDate
            // 
            this.uLabelReqDate.Location = new System.Drawing.Point(328, 28);
            this.uLabelReqDate.Name = "uLabelReqDate";
            this.uLabelReqDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelReqDate.TabIndex = 84;
            this.uLabelReqDate.Text = "ultraLabel2";
            // 
            // uTextReqName
            // 
            appearance125.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqName.Appearance = appearance125;
            this.uTextReqName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqName.Location = new System.Drawing.Point(218, 28);
            this.uTextReqName.Name = "uTextReqName";
            this.uTextReqName.ReadOnly = true;
            this.uTextReqName.Size = new System.Drawing.Size(100, 21);
            this.uTextReqName.TabIndex = 83;
            // 
            // uTextReqID
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqID.Appearance = appearance20;
            this.uTextReqID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReqID.Location = new System.Drawing.Point(114, 28);
            this.uTextReqID.Name = "uTextReqID";
            this.uTextReqID.ReadOnly = true;
            this.uTextReqID.Size = new System.Drawing.Size(100, 21);
            this.uTextReqID.TabIndex = 82;
            // 
            // uLabelReqUser
            // 
            this.uLabelReqUser.Location = new System.Drawing.Point(12, 28);
            this.uLabelReqUser.Name = "uLabelReqUser";
            this.uLabelReqUser.Size = new System.Drawing.Size(100, 21);
            this.uLabelReqUser.TabIndex = 81;
            this.uLabelReqUser.Text = "ultraLabel2";
            // 
            // uGroupBoxInfo
            // 
            this.uGroupBoxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxInfo.Controls.Add(this.uTextVendorCode);
            this.uGroupBoxInfo.Controls.Add(this.uTextGRNo);
            this.uGroupBoxInfo.Controls.Add(this.uTextUnitCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelUnit);
            this.uGroupBoxInfo.Controls.Add(this.uTextSpecNo);
            this.uGroupBoxInfo.Controls.Add(this.uTextGRDate);
            this.uGroupBoxInfo.Controls.Add(this.uTextFloorPlanNo);
            this.uGroupBoxInfo.Controls.Add(this.uLabelDrawingNo);
            this.uGroupBoxInfo.Controls.Add(this.uTextMoldSeq);
            this.uGroupBoxInfo.Controls.Add(this.uLabelMoldSeq);
            this.uGroupBoxInfo.Controls.Add(this.uTextShipmentQty);
            this.uGroupBoxInfo.Controls.Add(this.uLabelAmount);
            this.uGroupBoxInfo.Controls.Add(this.uTextMaterialName);
            this.uGroupBoxInfo.Controls.Add(this.uTextMaterialCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelMaterial);
            this.uGroupBoxInfo.Controls.Add(this.uTextVendor);
            this.uGroupBoxInfo.Controls.Add(this.uLabelVendor);
            this.uGroupBoxInfo.Controls.Add(this.uLabelStorageDate);
            this.uGroupBoxInfo.Controls.Add(this.uTextStdNumber);
            this.uGroupBoxInfo.Controls.Add(this.uLabelStdNumber);
            this.uGroupBoxInfo.Controls.Add(this.uTextPlant);
            this.uGroupBoxInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxInfo.Location = new System.Drawing.Point(8, 8);
            this.uGroupBoxInfo.Name = "uGroupBoxInfo";
            this.uGroupBoxInfo.Size = new System.Drawing.Size(1040, 104);
            this.uGroupBoxInfo.TabIndex = 152;
            // 
            // uTextVendorCode
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorCode.Appearance = appearance40;
            this.uTextVendorCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendorCode.Location = new System.Drawing.Point(236, 52);
            this.uTextVendorCode.Name = "uTextVendorCode";
            this.uTextVendorCode.ReadOnly = true;
            this.uTextVendorCode.Size = new System.Drawing.Size(30, 21);
            this.uTextVendorCode.TabIndex = 146;
            this.uTextVendorCode.Visible = false;
            // 
            // uTextGRNo
            // 
            appearance121.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRNo.Appearance = appearance121;
            this.uTextGRNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRNo.Location = new System.Drawing.Point(1012, 16);
            this.uTextGRNo.Name = "uTextGRNo";
            this.uTextGRNo.ReadOnly = true;
            this.uTextGRNo.Size = new System.Drawing.Size(21, 21);
            this.uTextGRNo.TabIndex = 145;
            this.uTextGRNo.Visible = false;
            // 
            // uTextUnitCode
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnitCode.Appearance = appearance35;
            this.uTextUnitCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUnitCode.Location = new System.Drawing.Point(804, 28);
            this.uTextUnitCode.Name = "uTextUnitCode";
            this.uTextUnitCode.ReadOnly = true;
            this.uTextUnitCode.Size = new System.Drawing.Size(100, 21);
            this.uTextUnitCode.TabIndex = 108;
            // 
            // uLabelUnit
            // 
            this.uLabelUnit.Location = new System.Drawing.Point(700, 28);
            this.uLabelUnit.Name = "uLabelUnit";
            this.uLabelUnit.Size = new System.Drawing.Size(100, 21);
            this.uLabelUnit.TabIndex = 107;
            this.uLabelUnit.Text = "ultraLabel2";
            // 
            // uTextSpecNo
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Appearance = appearance26;
            this.uTextSpecNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecNo.Location = new System.Drawing.Point(906, 52);
            this.uTextSpecNo.Name = "uTextSpecNo";
            this.uTextSpecNo.ReadOnly = true;
            this.uTextSpecNo.Size = new System.Drawing.Size(100, 21);
            this.uTextSpecNo.TabIndex = 106;
            // 
            // uTextGRDate
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Appearance = appearance2;
            this.uTextGRDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRDate.Location = new System.Drawing.Point(592, 52);
            this.uTextGRDate.Name = "uTextGRDate";
            this.uTextGRDate.ReadOnly = true;
            this.uTextGRDate.Size = new System.Drawing.Size(100, 21);
            this.uTextGRDate.TabIndex = 105;
            // 
            // uTextFloorPlanNo
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Appearance = appearance32;
            this.uTextFloorPlanNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFloorPlanNo.Location = new System.Drawing.Point(804, 52);
            this.uTextFloorPlanNo.Name = "uTextFloorPlanNo";
            this.uTextFloorPlanNo.ReadOnly = true;
            this.uTextFloorPlanNo.Size = new System.Drawing.Size(100, 21);
            this.uTextFloorPlanNo.TabIndex = 104;
            // 
            // uLabelDrawingNo
            // 
            this.uLabelDrawingNo.Location = new System.Drawing.Point(700, 52);
            this.uLabelDrawingNo.Name = "uLabelDrawingNo";
            this.uLabelDrawingNo.Size = new System.Drawing.Size(100, 21);
            this.uLabelDrawingNo.TabIndex = 103;
            this.uLabelDrawingNo.Text = "ultraLabel2";
            // 
            // uTextMoldSeq
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Appearance = appearance38;
            this.uTextMoldSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMoldSeq.Location = new System.Drawing.Point(380, 52);
            this.uTextMoldSeq.Name = "uTextMoldSeq";
            this.uTextMoldSeq.ReadOnly = true;
            this.uTextMoldSeq.Size = new System.Drawing.Size(100, 21);
            this.uTextMoldSeq.TabIndex = 102;
            // 
            // uLabelMoldSeq
            // 
            this.uLabelMoldSeq.Location = new System.Drawing.Point(276, 52);
            this.uLabelMoldSeq.Name = "uLabelMoldSeq";
            this.uLabelMoldSeq.Size = new System.Drawing.Size(100, 21);
            this.uLabelMoldSeq.TabIndex = 101;
            this.uLabelMoldSeq.Text = "ultraLabel2";
            // 
            // uTextShipmentQty
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentQty.Appearance = appearance54;
            this.uTextShipmentQty.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextShipmentQty.Location = new System.Drawing.Point(592, 28);
            this.uTextShipmentQty.Name = "uTextShipmentQty";
            this.uTextShipmentQty.ReadOnly = true;
            this.uTextShipmentQty.Size = new System.Drawing.Size(100, 21);
            this.uTextShipmentQty.TabIndex = 100;
            // 
            // uLabelAmount
            // 
            this.uLabelAmount.Location = new System.Drawing.Point(488, 28);
            this.uLabelAmount.Name = "uLabelAmount";
            this.uLabelAmount.Size = new System.Drawing.Size(100, 21);
            this.uLabelAmount.TabIndex = 99;
            this.uLabelAmount.Text = "ultraLabel2";
            // 
            // uTextMaterialName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance18;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(218, 76);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(800, 21);
            this.uTextMaterialName.TabIndex = 98;
            // 
            // uTextMaterialCode
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Appearance = appearance25;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialCode.Location = new System.Drawing.Point(116, 76);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.ReadOnly = true;
            this.uTextMaterialCode.Size = new System.Drawing.Size(100, 21);
            this.uTextMaterialCode.TabIndex = 97;
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(12, 76);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(100, 21);
            this.uLabelMaterial.TabIndex = 96;
            this.uLabelMaterial.Text = "ultraLabel2";
            // 
            // uTextVendor
            // 
            appearance91.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance91;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(116, 52);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(150, 21);
            this.uTextVendor.TabIndex = 95;
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(12, 52);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(100, 21);
            this.uLabelVendor.TabIndex = 94;
            this.uLabelVendor.Text = "ultraLabel2";
            // 
            // uLabelStorageDate
            // 
            this.uLabelStorageDate.Location = new System.Drawing.Point(488, 52);
            this.uLabelStorageDate.Name = "uLabelStorageDate";
            this.uLabelStorageDate.Size = new System.Drawing.Size(100, 21);
            this.uLabelStorageDate.TabIndex = 93;
            this.uLabelStorageDate.Text = "ultraLabel2";
            // 
            // uTextStdNumber
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance31;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(380, 28);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(100, 21);
            this.uTextStdNumber.TabIndex = 92;
            // 
            // uLabelStdNumber
            // 
            this.uLabelStdNumber.Location = new System.Drawing.Point(276, 28);
            this.uLabelStdNumber.Name = "uLabelStdNumber";
            this.uLabelStdNumber.Size = new System.Drawing.Size(100, 21);
            this.uLabelStdNumber.TabIndex = 91;
            this.uLabelStdNumber.Text = "ultraLabel2";
            // 
            // uTextPlant
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance23;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(116, 28);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(150, 21);
            this.uTextPlant.TabIndex = 90;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 21);
            this.uLabelPlant.TabIndex = 89;
            this.uLabelPlant.Text = "ultraLabel2";
            // 
            // uGridReturnList
            // 
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridReturnList.DisplayLayout.Appearance = appearance65;
            this.uGridReturnList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridReturnList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance66.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance66.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance66.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance66.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReturnList.DisplayLayout.GroupByBox.Appearance = appearance66;
            appearance67.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReturnList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance67;
            this.uGridReturnList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance68.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance68.BackColor2 = System.Drawing.SystemColors.Control;
            appearance68.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance68.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridReturnList.DisplayLayout.GroupByBox.PromptAppearance = appearance68;
            this.uGridReturnList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridReturnList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            appearance69.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridReturnList.DisplayLayout.Override.ActiveCellAppearance = appearance69;
            appearance70.BackColor = System.Drawing.SystemColors.Highlight;
            appearance70.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridReturnList.DisplayLayout.Override.ActiveRowAppearance = appearance70;
            this.uGridReturnList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridReturnList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            this.uGridReturnList.DisplayLayout.Override.CardAreaAppearance = appearance71;
            appearance72.BorderColor = System.Drawing.Color.Silver;
            appearance72.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridReturnList.DisplayLayout.Override.CellAppearance = appearance72;
            this.uGridReturnList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridReturnList.DisplayLayout.Override.CellPadding = 0;
            appearance73.BackColor = System.Drawing.SystemColors.Control;
            appearance73.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance73.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance73.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridReturnList.DisplayLayout.Override.GroupByRowAppearance = appearance73;
            appearance74.TextHAlignAsString = "Left";
            this.uGridReturnList.DisplayLayout.Override.HeaderAppearance = appearance74;
            this.uGridReturnList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridReturnList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.Color.Silver;
            this.uGridReturnList.DisplayLayout.Override.RowAppearance = appearance75;
            this.uGridReturnList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridReturnList.DisplayLayout.Override.TemplateAddRowAppearance = appearance76;
            this.uGridReturnList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridReturnList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridReturnList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridReturnList.Location = new System.Drawing.Point(527, 278);
            this.uGridReturnList.Name = "uGridReturnList";
            this.uGridReturnList.Size = new System.Drawing.Size(515, 90);
            this.uGridReturnList.TabIndex = 151;
            // 
            // uDateReturnDate
            // 
            appearance43.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReturnDate.Appearance = appearance43;
            this.uDateReturnDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReturnDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateReturnDate.Location = new System.Drawing.Point(844, 372);
            this.uDateReturnDate.Name = "uDateReturnDate";
            this.uDateReturnDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReturnDate.TabIndex = 150;
            // 
            // uLabelReturnDate
            // 
            this.uLabelReturnDate.Location = new System.Drawing.Point(740, 372);
            this.uLabelReturnDate.Name = "uLabelReturnDate";
            this.uLabelReturnDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelReturnDate.TabIndex = 149;
            this.uLabelReturnDate.Text = "ultraLabel2";
            // 
            // uTextReturnUserName
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnUserName.Appearance = appearance27;
            this.uTextReturnUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnUserName.Location = new System.Drawing.Point(630, 372);
            this.uTextReturnUserName.Name = "uTextReturnUserName";
            this.uTextReturnUserName.ReadOnly = true;
            this.uTextReturnUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextReturnUserName.TabIndex = 148;
            // 
            // uTextReturnUserID
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReturnUserID.Appearance = appearance28;
            this.uTextReturnUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance55.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance55.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton18.Appearance = appearance55;
            editorButton18.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReturnUserID.ButtonsRight.Add(editorButton18);
            this.uTextReturnUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReturnUserID.Location = new System.Drawing.Point(528, 372);
            this.uTextReturnUserID.Name = "uTextReturnUserID";
            this.uTextReturnUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextReturnUserID.TabIndex = 147;
            this.uTextReturnUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReturnUserID_KeyDown);
            this.uTextReturnUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReturnUserID_EditorButtonClick);
            // 
            // uLabelReturnUser
            // 
            this.uLabelReturnUser.Location = new System.Drawing.Point(424, 372);
            this.uLabelReturnUser.Name = "uLabelReturnUser";
            this.uLabelReturnUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelReturnUser.TabIndex = 146;
            this.uLabelReturnUser.Text = "ultraLabel2";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraGroupBox1.Controls.Add(this.uOptionS4IngFlag);
            this.ultraGroupBox1.Controls.Add(this.uOptionS3IngFlag);
            this.ultraGroupBox1.Controls.Add(this.uOptionS2IngFlag);
            this.ultraGroupBox1.Controls.Add(this.uOptionS1IngFlag);
            this.ultraGroupBox1.Controls.Add(this.uDateReceiptDate);
            this.ultraGroupBox1.Controls.Add(this.uLabelReceiptDate);
            this.ultraGroupBox1.Controls.Add(this.uTextReceiptName);
            this.ultraGroupBox1.Controls.Add(this.uTextReceiptID);
            this.ultraGroupBox1.Controls.Add(this.uLabelReceiptUser);
            this.ultraGroupBox1.Controls.Add(this.uLabelProgressCheck1);
            this.ultraGroupBox1.Controls.Add(this.uLabelProgressCheck2);
            this.ultraGroupBox1.Controls.Add(this.uLabelProgressCheck3);
            this.ultraGroupBox1.Controls.Add(this.uLabelProgressCheck4);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 396);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1030, 76);
            this.ultraGroupBox1.TabIndex = 140;
            // 
            // uOptionS4IngFlag
            // 
            appearance81.TextVAlignAsString = "Middle";
            this.uOptionS4IngFlag.Appearance = appearance81;
            this.uOptionS4IngFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS4IngFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance82.TextVAlignAsString = "Middle";
            this.uOptionS4IngFlag.ItemAppearance = appearance82;
            valueListItem11.DataValue = "T";
            valueListItem11.DisplayText = "필요";
            valueListItem12.DataValue = "F";
            valueListItem12.DisplayText = "불필요";
            this.uOptionS4IngFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem11,
            valueListItem12});
            this.uOptionS4IngFlag.Location = new System.Drawing.Point(420, 52);
            this.uOptionS4IngFlag.Name = "uOptionS4IngFlag";
            this.uOptionS4IngFlag.Size = new System.Drawing.Size(130, 20);
            this.uOptionS4IngFlag.TabIndex = 111;
            this.uOptionS4IngFlag.TextIndentation = 2;
            this.uOptionS4IngFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS4IngFlag.ValueChanged += new System.EventHandler(this.uOptionS4IngFlag_ValueChanged);
            // 
            // uOptionS3IngFlag
            // 
            appearance85.TextVAlignAsString = "Middle";
            this.uOptionS3IngFlag.Appearance = appearance85;
            this.uOptionS3IngFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS3IngFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance86.TextVAlignAsString = "Middle";
            this.uOptionS3IngFlag.ItemAppearance = appearance86;
            valueListItem15.DataValue = "T";
            valueListItem15.DisplayText = "필요";
            valueListItem16.DataValue = "F";
            valueListItem16.DisplayText = "불필요";
            this.uOptionS3IngFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem15,
            valueListItem16});
            this.uOptionS3IngFlag.Location = new System.Drawing.Point(136, 52);
            this.uOptionS3IngFlag.Name = "uOptionS3IngFlag";
            this.uOptionS3IngFlag.Size = new System.Drawing.Size(130, 20);
            this.uOptionS3IngFlag.TabIndex = 110;
            this.uOptionS3IngFlag.TextIndentation = 2;
            this.uOptionS3IngFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS3IngFlag.ValueChanged += new System.EventHandler(this.uOptionS3IngFlag_ValueChanged);
            // 
            // uOptionS2IngFlag
            // 
            appearance87.TextVAlignAsString = "Middle";
            this.uOptionS2IngFlag.Appearance = appearance87;
            this.uOptionS2IngFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS2IngFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance88.TextVAlignAsString = "Middle";
            this.uOptionS2IngFlag.ItemAppearance = appearance88;
            valueListItem17.DataValue = "T";
            valueListItem17.DisplayText = "필요";
            valueListItem18.DataValue = "F";
            valueListItem18.DisplayText = "불필요";
            this.uOptionS2IngFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem17,
            valueListItem18});
            this.uOptionS2IngFlag.Location = new System.Drawing.Point(420, 28);
            this.uOptionS2IngFlag.Name = "uOptionS2IngFlag";
            this.uOptionS2IngFlag.Size = new System.Drawing.Size(130, 20);
            this.uOptionS2IngFlag.TabIndex = 109;
            this.uOptionS2IngFlag.TextIndentation = 2;
            this.uOptionS2IngFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS2IngFlag.ValueChanged += new System.EventHandler(this.uOptionS2IngFlag_ValueChanged);
            // 
            // uOptionS1IngFlag
            // 
            appearance89.TextVAlignAsString = "Middle";
            this.uOptionS1IngFlag.Appearance = appearance89;
            this.uOptionS1IngFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionS1IngFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            appearance90.TextVAlignAsString = "Middle";
            this.uOptionS1IngFlag.ItemAppearance = appearance90;
            valueListItem19.DataValue = "T";
            valueListItem19.DisplayText = "필요";
            valueListItem20.DataValue = "F";
            valueListItem20.DisplayText = "불필요";
            this.uOptionS1IngFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem19,
            valueListItem20});
            this.uOptionS1IngFlag.Location = new System.Drawing.Point(136, 28);
            this.uOptionS1IngFlag.Name = "uOptionS1IngFlag";
            this.uOptionS1IngFlag.Size = new System.Drawing.Size(130, 20);
            this.uOptionS1IngFlag.TabIndex = 108;
            this.uOptionS1IngFlag.TextIndentation = 2;
            this.uOptionS1IngFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionS1IngFlag.ValueChanged += new System.EventHandler(this.uOptionS1IngFlag_ValueChanged);
            // 
            // uDateReceiptDate
            // 
            appearance99.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReceiptDate.Appearance = appearance99;
            this.uDateReceiptDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateReceiptDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateReceiptDate.Location = new System.Drawing.Point(704, 52);
            this.uDateReceiptDate.Name = "uDateReceiptDate";
            this.uDateReceiptDate.Size = new System.Drawing.Size(100, 21);
            this.uDateReceiptDate.TabIndex = 107;
            // 
            // uLabelReceiptDate
            // 
            this.uLabelReceiptDate.Location = new System.Drawing.Point(580, 52);
            this.uLabelReceiptDate.Name = "uLabelReceiptDate";
            this.uLabelReceiptDate.Size = new System.Drawing.Size(120, 20);
            this.uLabelReceiptDate.TabIndex = 106;
            this.uLabelReceiptDate.Text = "ultraLabel2";
            // 
            // uTextReceiptName
            // 
            appearance100.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptName.Appearance = appearance100;
            this.uTextReceiptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReceiptName.Location = new System.Drawing.Point(806, 28);
            this.uTextReceiptName.Name = "uTextReceiptName";
            this.uTextReceiptName.ReadOnly = true;
            this.uTextReceiptName.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptName.TabIndex = 105;
            // 
            // uTextReceiptID
            // 
            appearance101.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextReceiptID.Appearance = appearance101;
            this.uTextReceiptID.BackColor = System.Drawing.Color.PowderBlue;
            appearance102.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance102.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton19.Appearance = appearance102;
            editorButton19.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextReceiptID.ButtonsRight.Add(editorButton19);
            this.uTextReceiptID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextReceiptID.Location = new System.Drawing.Point(704, 28);
            this.uTextReceiptID.Name = "uTextReceiptID";
            this.uTextReceiptID.Size = new System.Drawing.Size(100, 21);
            this.uTextReceiptID.TabIndex = 104;
            this.uTextReceiptID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextReceiptID_KeyDown);
            this.uTextReceiptID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextReceiptID_EditorButtonClick);
            // 
            // uLabelReceiptUser
            // 
            this.uLabelReceiptUser.Location = new System.Drawing.Point(580, 28);
            this.uLabelReceiptUser.Name = "uLabelReceiptUser";
            this.uLabelReceiptUser.Size = new System.Drawing.Size(120, 20);
            this.uLabelReceiptUser.TabIndex = 103;
            this.uLabelReceiptUser.Text = "ultraLabel2";
            // 
            // uGridDetail
            // 
            appearance103.BackColor = System.Drawing.SystemColors.Window;
            appearance103.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetail.DisplayLayout.Appearance = appearance103;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance104.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance104.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance104.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance104.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.GroupByBox.Appearance = appearance104;
            appearance105.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance105;
            this.uGridDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance106.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance106.BackColor2 = System.Drawing.SystemColors.Control;
            appearance106.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance106.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance106;
            this.uGridDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance107.BackColor = System.Drawing.SystemColors.Window;
            appearance107.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetail.DisplayLayout.Override.ActiveCellAppearance = appearance107;
            appearance108.BackColor = System.Drawing.SystemColors.Highlight;
            appearance108.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetail.DisplayLayout.Override.ActiveRowAppearance = appearance108;
            this.uGridDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance109.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.CardAreaAppearance = appearance109;
            appearance110.BorderColor = System.Drawing.Color.Silver;
            appearance110.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetail.DisplayLayout.Override.CellAppearance = appearance110;
            this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetail.DisplayLayout.Override.CellPadding = 0;
            appearance111.BackColor = System.Drawing.SystemColors.Control;
            appearance111.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance111.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance111.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance111.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.GroupByRowAppearance = appearance111;
            appearance112.TextHAlignAsString = "Left";
            this.uGridDetail.DisplayLayout.Override.HeaderAppearance = appearance112;
            this.uGridDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance113.BackColor = System.Drawing.SystemColors.Window;
            appearance113.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetail.DisplayLayout.Override.RowAppearance = appearance113;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance114.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance114;
            this.uGridDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetail.Location = new System.Drawing.Point(12, 188);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(515, 180);
            this.uGridDetail.TabIndex = 139;
            // 
            // uTextReturnReason
            // 
            this.uTextReturnReason.Location = new System.Drawing.Point(116, 372);
            this.uTextReturnReason.Name = "uTextReturnReason";
            this.uTextReturnReason.Size = new System.Drawing.Size(300, 21);
            this.uTextReturnReason.TabIndex = 138;
            // 
            // uLabelReturnReason
            // 
            this.uLabelReturnReason.Location = new System.Drawing.Point(12, 372);
            this.uLabelReturnReason.Name = "uLabelReturnReason";
            this.uLabelReturnReason.Size = new System.Drawing.Size(100, 20);
            this.uLabelReturnReason.TabIndex = 137;
            this.uLabelReturnReason.Text = "ultraLabel2";
            // 
            // uButtonReturn
            // 
            this.uButtonReturn.Location = new System.Drawing.Point(954, 368);
            this.uButtonReturn.Name = "uButtonReturn";
            this.uButtonReturn.Size = new System.Drawing.Size(88, 28);
            this.uButtonReturn.TabIndex = 116;
            this.uButtonReturn.Text = "ultraButton1";
            this.uButtonReturn.Click += new System.EventHandler(this.uButtonReturn_Click);
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Controls.Add(this.ultraTabPageControl3);
            this.uTab.Controls.Add(this.ultraTabPageControl4);
            this.uTab.Location = new System.Drawing.Point(12, 476);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(1030, 112);
            this.uTab.TabIndex = 90;
            ultraTab1.Key = "Step1";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Step1.품질감사";
            ultraTab2.Key = "Step2";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Step2.수입검사";
            ultraTab3.Key = "Step3";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "Step3.공정평가";
            ultraTab4.Key = "Step4";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "Step4.신뢰성 평가";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1026, 86);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uDateWriteDate);
            this.uGroupBox1.Controls.Add(this.uLabelRegisterDate);
            this.uGroupBox1.Controls.Add(this.uTextWriteName);
            this.uGroupBox1.Controls.Add(this.uTextWriteID);
            this.uGroupBox1.Controls.Add(this.uLabelRegisterUser);
            this.uGroupBox1.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBox1.Controls.Add(this.uLabelJudge1);
            this.uGroupBox1.Controls.Add(this.uOptionPassFailFlag);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 588);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1030, 40);
            this.uGroupBox1.TabIndex = 85;
            // 
            // uDateWriteDate
            // 
            appearance77.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Appearance = appearance77;
            this.uDateWriteDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(776, 12);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDate.TabIndex = 89;
            // 
            // uLabelRegisterDate
            // 
            this.uLabelRegisterDate.Location = new System.Drawing.Point(672, 12);
            this.uLabelRegisterDate.Name = "uLabelRegisterDate";
            this.uLabelRegisterDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelRegisterDate.TabIndex = 88;
            this.uLabelRegisterDate.Text = "ultraLabel2";
            // 
            // uTextWriteName
            // 
            appearance78.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance78;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(562, 12);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteName.TabIndex = 87;
            // 
            // uTextWriteID
            // 
            appearance79.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteID.Appearance = appearance79;
            this.uTextWriteID.BackColor = System.Drawing.Color.PowderBlue;
            appearance80.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance80.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton20.Appearance = appearance80;
            editorButton20.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteID.ButtonsRight.Add(editorButton20);
            this.uTextWriteID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteID.Location = new System.Drawing.Point(460, 12);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteID.TabIndex = 86;
            this.uTextWriteID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteID_KeyDown);
            this.uTextWriteID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uLabelRegisterUser
            // 
            this.uLabelRegisterUser.Location = new System.Drawing.Point(356, 12);
            this.uLabelRegisterUser.Name = "uLabelRegisterUser";
            this.uLabelRegisterUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelRegisterUser.TabIndex = 85;
            this.uLabelRegisterUser.Text = "ultraLabel2";
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(256, 12);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(90, 20);
            this.uCheckCompleteFlag.TabIndex = 84;
            this.uCheckCompleteFlag.Text = "작성완료";
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCompleteFlag.CheckedChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedChanged);
            // 
            // uLabelJudge1
            // 
            this.uLabelJudge1.Location = new System.Drawing.Point(12, 12);
            this.uLabelJudge1.Name = "uLabelJudge1";
            this.uLabelJudge1.Size = new System.Drawing.Size(100, 20);
            this.uLabelJudge1.TabIndex = 83;
            this.uLabelJudge1.Text = "ultraLabel2";
            // 
            // uOptionPassFailFlag
            // 
            this.uOptionPassFailFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionPassFailFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem3.DataValue = "OK";
            valueListItem3.DisplayText = "합격";
            valueListItem4.DataValue = "NG";
            valueListItem4.DisplayText = "불합격";
            this.uOptionPassFailFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.uOptionPassFailFlag.Location = new System.Drawing.Point(116, 12);
            this.uOptionPassFailFlag.Name = "uOptionPassFailFlag";
            this.uOptionPassFailFlag.Size = new System.Drawing.Size(130, 20);
            this.uOptionPassFailFlag.TabIndex = 0;
            this.uOptionPassFailFlag.TextIndentation = 2;
            this.uOptionPassFailFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(960, 16);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(92, 36);
            this.ultraButton1.TabIndex = 49;
            this.ultraButton1.Text = "ActiveTest";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // frmINSZ0002
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0002";
            this.Load += new System.EventHandler(this.frmINSZ0002_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0002_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0002_FormClosing);
            this.Resize += new System.EventHandler(this.frmINSZ0002_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS1CompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1FileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1EtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS1WriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS1Result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserID)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS2CompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS2WriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2FileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2EtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS2Result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserID)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQualMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS3CompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3EtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS3WriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3FileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS3Result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserID)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.ultraTabPageControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReliabilityMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS4CompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4EtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS4WriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4FileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS4Result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchReqUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProgStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPassFailFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchReqFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInput)).EndInit();
            this.uGroupBoxInput.ResumeLayout(false);
            this.uGroupBoxInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSaveCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextICP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMSDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyDevice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).EndInit();
            this.uGroupBoxInfo.ResumeLayout(false);
            this.uGroupBoxInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUnitCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFloorPlanNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMoldSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextShipmentQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridReturnList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReturnDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS4IngFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS3IngFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS2IngFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionS1IngFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateReceiptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReceiptID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStorageNum;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchReqToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchReqFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProgStatus;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProgStatus;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPassFailFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPassFailFlag;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHeader;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionPassFailFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelJudge1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.Misc.UltraLabel uLabelRegisterUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRegisterDate;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraLabel uLabelProgressCheck1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS1FileName;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS1EtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS1WriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelS1WrateDate;
        private Infragistics.Win.Misc.UltraLabel uLabelJudge2;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS1Result;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS1UserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS1UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS1User;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS2UserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS2UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS2User;
        private Infragistics.Win.Misc.UltraLabel uLabelProgressCheck2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS2WriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelS2WrateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS2FileName;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS2EtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc2;
        private Infragistics.Win.Misc.UltraLabel uLabelJudge3;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS2Result;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS3EtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS3WriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelS3WrateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS3FileName;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile3;
        private Infragistics.Win.Misc.UltraLabel uLabelJudge4;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS3Result;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS3UserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS3UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS3User;
        private Infragistics.Win.Misc.UltraLabel uLabelProgressCheck3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS4EtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc4;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS4WriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelS4WrateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS4FileName;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile4;
        private Infragistics.Win.Misc.UltraLabel uLabelJudge5;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS4Result;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS4UserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS4UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS4User;
        private Infragistics.Win.Misc.UltraLabel uLabelProgressCheck4;
        private Infragistics.Win.Misc.UltraButton uButtonReturn;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnReason;
        private Infragistics.Win.Misc.UltraLabel uLabelReturnReason;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReceiptDate;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReceiptID;
        private Infragistics.Win.Misc.UltraLabel uLabelReceiptUser;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS4IngFlag;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS3IngFlag;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS2IngFlag;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionS1IngFlag;
        private Infragistics.Win.Misc.UltraButton uButtonMove;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSaveCheck;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateReturnDate;
        private Infragistics.Win.Misc.UltraLabel uLabelReturnDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelReturnUser;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridReturnList;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInput;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextICP;
        private Infragistics.Win.Misc.UltraLabel uLabelICP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMSDS;
        private Infragistics.Win.Misc.UltraLabel uLabelMSDS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApplyDevice;
        private Infragistics.Win.Misc.UltraLabel uLabelApplyDevice;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqPurpose;
        private Infragistics.Win.Misc.UltraLabel uLabelRequestPurpose;
        private Infragistics.Win.Misc.UltraLabel uLabelReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqID;
        private Infragistics.Win.Misc.UltraLabel uLabelReqUser;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUnitCode;
        private Infragistics.Win.Misc.UltraLabel uLabelUnit;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFloorPlanNo;
        private Infragistics.Win.Misc.UltraLabel uLabelDrawingNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMoldSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelMoldSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextShipmentQty;
        private Infragistics.Win.Misc.UltraLabel uLabelAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelStorageDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchReqUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchReqUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchReqUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchConsumableType;
        private Infragistics.Win.Misc.UltraLabel ulabelSearchConsumableType;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS1CompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS2CompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS3CompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS4CompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextQualMove;
        private Infragistics.Win.Misc.UltraLabel uLabelQualMove;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReliabilityMove;
        private Infragistics.Win.Misc.UltraLabel uLabelReliabilityMove;
        private Infragistics.Win.Misc.UltraButton uButtonAVLMove;
        private Infragistics.Win.Misc.UltraButton uButtonQualMove;
        private Infragistics.Win.Misc.UltraButton uButtonReliabilityMove;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorCode;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
    }
}