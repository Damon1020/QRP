﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질검사관리                                          */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINSZ0004.cs                                        */
/* 프로그램명   : AVL정보                                               */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-08 : 기능 추가 (이종호)                       */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;
using System.IO;

namespace QRPINS.UI
{
    public partial class frmINSZ0004 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        #region 폼이동 위한 전역변수

        private string m_strPlantCode;
        private string m_strVendorCode;
        private string m_strMoveFormName;

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string VendorCode
        {
            get { return m_strVendorCode; }
            set { m_strVendorCode = value; }
        }

        public string MoveFormName
        {
            get { return m_strMoveFormName; }
            set { m_strMoveFormName = value; }
        }

        #endregion

        public frmINSZ0004()
        {
            InitializeComponent();
        }

        private void frmINSZ0004_Activated(object sender, EventArgs e)
        {
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //툴바설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0004_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            SetToolAuth();
            InitComboBox();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitEtc();

            // 그리드 정보 Load
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            if (MoveFormName != null)
            {
                this.uGroupBoxContentsArea.Expanded = true;
                if (MoveFormName.Equals("frmINSZ0002"))
                {
                    titleArea.TextName = titleArea.TextName + "(신규자재 인증등록 이동)";

                    SearchData_ForMoveForm();
                }
            }
            else
            {
                this.uGroupBoxContentsArea.Expanded = false;
            }
        }

        private void frmINSZ0004_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 타이틀 설정
                titleArea.mfSetLabelText("AVL 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

                // 버튼초기화
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRow2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDownConfirm, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
                btn.mfSetButton(this.uButtonDownFile, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);

                // 텍스트 박스 최대 길이 설정
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextWriteUserID.MaxLength = 20;
                this.uTextVendorCode.MaxLength = 10;
                this.uTextEtcDesc.MaxLength = 500;

                // 기본값설정
                uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

                //첨부파일 텍스트 초기화
                this.uTextImprove.Text = "";
                this.uTextImprove.ReadOnly = true;

                this.uTextResult.Text = "";
                this.uTextResult.ReadOnly = true;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System resourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchAuditGrade, "AuditGrade", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, true);                
                lbl.mfSetLabel(this.uLabelWriteDate, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelWriteUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelAuditGrade, "AuditGrade", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelResult1, "감사결과Report", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelImprove, "개선Report", m_resSys.GetString("SYS_FONTNAME"), true,false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System resourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                //--정보
                grd.mfInitGeneralGrid(this.uGridAVLList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--첨부파일
                grd.mfInitGeneralGrid(this.uGridAVLFile, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--인증서보유현황
                grd.mfInitGeneralGrid(this.uGridAVLConfirm, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--이력정보
                grd.mfInitGeneralGrid(this.uGridAVLHistory, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--정보
                grd.mfSetGridColumn(this.uGridAVLList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLList, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLList, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLList, 0, "WriteDate", "작성일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLList, 0, "WriteUserName", "작성자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLList, 0, "AuditGrade", "AuditGrade", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--첨부파일
                grd.mfSetGridColumn(this.uGridAVLFile, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridAVLFile, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridAVLFile, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLFile, 0, "FilePassName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLFile, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--인증서보유현황
                grd.mfSetGridColumn(this.uGridAVLConfirm, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridAVLConfirm, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridAVLConfirm, 0, "ConfirmName", "인증서명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLConfirm, 0, "ConfirmDate", "인증일자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", DateTime.Now.ToString("yyyy-MM-dd"));

                grd.mfSetGridColumn(this.uGridAVLConfirm, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLConfirm, 0, "FilePassName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLConfirm, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--이력정보                
                grd.mfSetGridColumn(this.uGridAVLHistory, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridAVLHistory, 0, "WriteDate", "등록일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                grd.mfSetGridColumn(this.uGridAVLHistory, 0, "UserName", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLHistory, 0, "AuditGrade", "AuditGrade", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLHistory, 0, "ResultReport", "감사결과Report", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLHistory, 0, "ImproveReport", "개선Reporct", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAVLHistory, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridAVLList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridAVLList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridAVLFile.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridAVLFile.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridAVLConfirm.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridAVLConfirm.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridAVLHistory.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridAVLHistory.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(this.uGridAVLFile, 0);
                grd.mfAddRowGrid(this.uGridAVLConfirm, 0);                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System resourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();
                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid,
                    Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "인증서 보유현황", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "이력정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant plant = new QRPMAS.BL.MASPRC.Plant();
                brwChnnel.mfCredentials(plant);

                DataTable dtPlant = plant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                
                // 검색조건
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
                // 입력
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                brwChnnel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChnnel.mfCredentials(clsComCode);

                DataTable dtComCode = clsComCode.mfReadCommonCode("C0037", m_resSys.GetString("SYS_LANG"));
                
                //검색조건
                wCombo.mfSetComboEditor(this.uComboSearchAuditGrade, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtComCode);
                // 입력
                wCombo.mfSetComboEditor(this.uComboAuditGrade, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "1", "", ""
                    , "ComCode", "ComCodeName", dtComCode);

                // AuditGrade Combo 공백 Item 제거
                this.uComboAuditGrade.Items.Remove(0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strVendorCode = this.uTextSearchVendorCode.Text;
                string strAuditGrrade = this.uComboSearchAuditGrade.Value.ToString();
                string strFromWriteDate = Convert.ToDateTime(this.uDateSearchFromWriteDate.Value).ToString("yyyy-MM-dd");
                string strToWriteDate = Convert.ToDateTime(this.uDateSearchToWriteDate.Value).ToString("yyyy-MM-dd");

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVL), "AVL");
                QRPINS.BL.INSIMP.AVL clsAVL = new QRPINS.BL.INSIMP.AVL();
                brwChannel.mfCredentials(clsAVL);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                DataTable dtRtn = clsAVL.mfReadINSAVL(strPlantCode, strVendorCode, strAuditGrrade, strFromWriteDate, strToWriteDate, m_resSys.GetString("SYS_LANG"));

                this.uGridAVLList.DataSource = dtRtn;
                this.uGridAVLList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtn.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridAVLList, 0);
                }

                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                
                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                     Result= msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000278",
                                Infragistics.Win.HAlign.Right);

                     this.uComboPlant.DropDown();
                     return;
                }
                else if (this.uTextVendorCode.Text == "")
                {
                    Result= msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000166",
                                Infragistics.Win.HAlign.Right);

                    this.uTextVendorCode.Focus();
                    return;
                }
                else if (this.uTextWriteUserID.Text == "")
                {
                    Result= msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M001000",
                                Infragistics.Win.HAlign.Right);

                    this.uTextWriteUserID.Focus();
                    return;
                }
                else if(this.uDateWriteDate.Value == null)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000999",
                                Infragistics.Win.HAlign.Right);
                    
                    this.uDateWriteDate.DropDown();
                    return;
                }
                else if (this.uComboAuditGrade.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000033",
                                Infragistics.Win.HAlign.Right);

                    this.uComboAuditGrade.DropDown();
                    return;
                }
                else
                {
                    //콤보박스 선택값 Validation Check//////////
                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this)) return;
                    ///////////////////////////////////////////

                    // 저장여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 헤더/첨부파일/인증서 정보 DataTable로 반환하는 Method 호출
                        DataTable dtHeader = RtnHeaderDataTable();
                        DataTable dtFile = RtnFileDataTable();
                        DataTable dtConfirm = RtnConfirmDataTable();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVL), "AVL");
                        QRPINS.BL.INSIMP.AVL clsAVL = new QRPINS.BL.INSIMP.AVL();
                        brwChannel.mfCredentials(clsAVL);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장 Method 출
                        string strErrRtn = clsAVL.mfSaveINSAVL(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtFile, dtConfirm);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            // 첨부파일 Upload Method 호출
                            FileUpload_AVLConfirm();
                            FileUpload_AVLFile();
                            FileUpload_Report();

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                        , ErrRtn.ErrMessage,
                                                        Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 필수 입력사항 확인
                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000629", "M000278",
                               Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextVendorCode.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000629", "M000166",
                                Infragistics.Win.HAlign.Right);

                    this.uTextVendorCode.Focus();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 삭제 Method의 매개변수 설정
                        string strPlantCode = this.uComboPlant.Value.ToString();
                        string strVendorCode = this.uTextVendorCode.Text;

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVL), "AVL");
                        QRPINS.BL.INSIMP.AVL clsAVL = new QRPINS.BL.INSIMP.AVL();
                        brwChannel.mfCredentials(clsAVL);

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsAVL.mfDeleteINSAVL(strPlantCode, strVendorCode);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000638", strLang)
                                                            , ErrRtn.ErrMessage,
                                                            Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridAVLList.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridAVLList);
                }
              
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 데이터테이블로 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable RtnHeaderDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVL), "AVL");
                QRPINS.BL.INSIMP.AVL clsAVL = new QRPINS.BL.INSIMP.AVL();
                brwChannel.mfCredentials(clsAVL);

                // 데이터테이블 컬럼설정
                dtRtn = clsAVL.mfSetDataInfo();
                
                // 데이터 저장
                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                drRow["VendorCode"] = this.uTextVendorCode.Text;
                drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                drRow["WriteDate"] = Convert.ToDateTime(this.uDateWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["AuditGrade"] = this.uComboAuditGrade.Value.ToString();
                if (this.uTextResult.Text.Equals(string.Empty))
                {
                    drRow["ResultFile"] = string.Empty;
                }
                else if (this.uTextResult.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextResult.Text);
                    drRow["ResultFile"] = this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + "ResultReport" + "-" + fileDoc.Name;
                }
                else
                {
                    drRow["ResultFile"] = this.uTextResult.Text;
                }

                if (this.uTextImprove.Text.Equals(string.Empty))
                {
                    drRow["ImproveFile"] = string.Empty;
                }
                else if (this.uTextImprove.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextImprove.Text);
                    drRow["ImproveFile"] = this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + "ImproveReport" + "-" + fileDoc.Name;
                }
                else
                {
                    drRow["ImproveFile"] = this.uTextImprove.Text;
                }

                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 인증서 정보 DataTable로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable RtnConfirmDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVLConfirm), "AVLConfirm");
                QRPINS.BL.INSIMP.AVLConfirm clsConfirm = new QRPINS.BL.INSIMP.AVLConfirm();
                brwChannel.mfCredentials(clsConfirm);

                // 데이터테이블 컬럼설정
                dtRtn = clsConfirm.mfSetDataInfo();
                DataRow drRow;

                // 데이터 저장
                for (int i = 0; i < this.uGridAVLConfirm.Rows.Count; i++)
                {
                    this.uGridAVLConfirm.ActiveCell = this.uGridAVLConfirm.Rows[0].Cells[0];
                    if (this.uGridAVLConfirm.Rows[i].Hidden == false)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["VendorCode"] = this.uTextVendorCode.Value.ToString();
                        drRow["Seq"] = this.uGridAVLConfirm.Rows[i].RowSelectorNumber;
                        drRow["ConfirmName"] = this.uGridAVLConfirm.Rows[i].Cells["ConfirmName"].Value.ToString();
                        drRow["ConfirmDate"] = Convert.ToDateTime(this.uGridAVLConfirm.Rows[i].Cells["ConfirmDate"].Value).ToString("yyyy-MM-dd");
                        drRow["FileTitle"] = this.uGridAVLConfirm.Rows[i].Cells["FileTitle"].Value.ToString();
                        if (this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString());
                            drRow["FilePassName"] = this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + this.uGridAVLConfirm.Rows[i].RowSelectorNumber.ToString()
                                + "-C-" + fileDoc.Name;
                        }
                        else
                        {
                            drRow["FilePassName"] = this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString();
                        }
                        drRow["EtcDesc"] = this.uGridAVLConfirm.Rows[i].Cells["EtcDesc"].Value.ToString();

                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 정보 DataTable로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable RtnFileDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVLFile), "AVLFile");
                QRPINS.BL.INSIMP.AVLFile clsFile = new QRPINS.BL.INSIMP.AVLFile();
                brwChannel.mfCredentials(clsFile);

                // 데이터테이블 컬럼설정
                dtRtn = clsFile.mfSetDataInfo();
                DataRow drRow;

                // 데이터 저장
                for (int i = 0; i < this.uGridAVLFile.Rows.Count; i++)
                {
                    this.uGridAVLFile.ActiveCell = this.uGridAVLFile.Rows[0].Cells[0];
                    if (this.uGridAVLFile.Rows[i].Hidden == false)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["VendorCode"] = this.uTextVendorCode.Value.ToString();
                        drRow["Seq"] = this.uGridAVLFile.Rows[i].RowSelectorNumber;
                        drRow["FileTitle"] = this.uGridAVLFile.Rows[i].Cells["FileTitle"].Value.ToString();
                        if (this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString());
                            drRow["FilePassName"] = this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + this.uGridAVLFile.Rows[i].RowSelectorNumber.ToString()
                                + "-" + fileDoc.Name;
                        }
                        else
                        {
                            drRow["FilePassName"] = this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString();
                        }
                        drRow["EtcDesc"] = this.uGridAVLFile.Rows[i].Cells["EtcDesc"].Value.ToString();

                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        public void Clear()
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextVendorCode.Text = "";
                this.uTextVendorName.Text = "";
                this.uDateWriteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uComboAuditGrade.Value = "1";
                this.uTextEtcDesc.Text = "";

                DataTable dt = new DataTable();

                while (this.uGridAVLConfirm.Rows.Count > 0)
                {
                    this.uGridAVLConfirm.Rows[0].Delete(false);
                }

                while (this.uGridAVLFile.Rows.Count > 0)
                {
                    this.uGridAVLFile.Rows[0].Delete(false);
                }

                while (this.uGridAVLHistory.Rows.Count > 0)
                {
                    this.uGridAVLHistory.Rows[0].Delete(false);
                }

                this.uComboPlant.Enabled = true;
                this.uTextVendorCode.Enabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        private bool Search_AVLDetail(string strPlantCode, string strVendorCode)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVL), "AVL");
                QRPINS.BL.INSIMP.AVL clsAVL = new QRPINS.BL.INSIMP.AVL();
                brwChannel.mfCredentials(clsAVL);

                DataTable dtRtn = clsAVL.mfReadINSAVLDetail(strPlantCode, strVendorCode, m_resSys.GetString("SYS_LANG"));

                if (dtRtn.Rows.Count > 0)
                {
                    this.uComboPlant.Value = dtRtn.Rows[0]["PlantCode"].ToString();
                    this.uTextVendorCode.Text = dtRtn.Rows[0]["VendorCode"].ToString();
                    this.uTextVendorName.Text = dtRtn.Rows[0]["VendorName"].ToString();
                    this.uDateWriteDate.Value = dtRtn.Rows[0]["WriteDate"].ToString();
                    this.uTextWriteUserID.Text = dtRtn.Rows[0]["WriteUserID"].ToString();
                    this.uTextWriteUserName.Text = dtRtn.Rows[0]["WriteUserName"].ToString();
                    this.uComboAuditGrade.Value = dtRtn.Rows[0]["AuditGrade"].ToString();
                    this.uTextResult.Text = dtRtn.Rows[0]["ResultReport"].ToString();
                    this.uTextImprove.Text = dtRtn.Rows[0]["ImproveReport"].ToString();

                    // PK 수정불가상태로
                    this.uComboPlant.Enabled = false;
                    this.uTextVendorCode.Enabled = false;
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 인증서정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        private void Search_AVLConfirm(string strPlantCode, string strVendorCode)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVLConfirm), "AVLConfirm");
                QRPINS.BL.INSIMP.AVLConfirm clsAVLConfirm = new QRPINS.BL.INSIMP.AVLConfirm();
                brwChannel.mfCredentials(clsAVLConfirm);

                DataTable dtRtn = clsAVLConfirm.mfReadINSAVLConfirm(strPlantCode, strVendorCode);

                if (dtRtn.Rows.Count > 0)
                {
                    this.uGridAVLConfirm.DataSource = dtRtn;
                    this.uGridAVLConfirm.DataBind();

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridAVLConfirm, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 첨부파일정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <returns></returns>
        private void Search_AVLFile(string strPlantCode, string strVendorCode)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVLFile), "AVLFile");
                QRPINS.BL.INSIMP.AVLFile clsAVLFile = new QRPINS.BL.INSIMP.AVLFile();
                brwChannel.mfCredentials(clsAVLFile);

                DataTable dtRtn = clsAVLFile.mfReadINSAVLFile(strPlantCode, strVendorCode);

                if (dtRtn.Rows.Count > 0)
                {
                    this.uGridAVLFile.DataSource = dtRtn;
                    this.uGridAVLFile.DataBind();

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridAVLFile, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 이력정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <returns></returns>
        private bool Search_AVLHistory(string strPlantCode, string strVendorCode)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AVLHistory), "AVLHistory");
                QRPINS.BL.INSIMP.AVLHistory clsAVLHistory = new QRPINS.BL.INSIMP.AVLHistory();
                brwChannel.mfCredentials(clsAVLHistory);

                DataTable dtRtn = clsAVLHistory.mfReadINSAVLHistory(strPlantCode, strVendorCode, m_resSys.GetString("SYS_LANG"));

                if (dtRtn.Rows.Count > 0)
                {
                    this.uGridAVLHistory.DataSource = dtRtn;
                    this.uGridAVLHistory.DataBind();

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridAVLHistory, 0);
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 인증서 첨부파일 저장 Method
        /// </summary>
        private void FileUpload_AVLConfirm()
        {
            try
            {
                // 화일서버 연결정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                // 첨부파일 저장경로정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0010");

                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                for (int i = 0; i < this.uGridAVLConfirm.Rows.Count; i++)
                {
                    if (this.uGridAVLConfirm.Rows[i].Hidden == false)
                    {
                        if (this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\"))
                        {
                            // 파일 이름변경(공장+거래처코드+순번+C+파일명)
                            FileInfo fileDoc = new FileInfo(this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" + this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + this.uGridAVLConfirm.Rows[i].RowSelectorNumber.ToString()
                                + "-C-" + fileDoc.Name;

                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            //변경한 화일이름으로 복사하기
                            File.Copy(this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                    }
                }

                if (arrFile.Count > 0)
                {
                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUpload_NonAssync();

                    if (File.Exists(arrFile[0].ToString()))
                        File.Delete(arrFile[0].ToString());
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 첨부파일 저장 Method
        /// </summary>
        private void FileUpload_AVLFile()
        {
            try
            {
                // 화일서버 연결정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                // 첨부파일 저장경로정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0010");

                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                for (int i = 0; i < this.uGridAVLFile.Rows.Count; i++)
                {
                    if (this.uGridAVLFile.Rows[i].Hidden == false)
                    {
                        if (this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\"))
                        {
                            // 파일 이름변경(공장+거래처코드+순번+파일명)
                            FileInfo fileDoc = new FileInfo(this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" + this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + this.uGridAVLFile.Rows[i].RowSelectorNumber.ToString()
                                + "-" + fileDoc.Name;

                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            //변경한 화일이름으로 복사하기
                            File.Copy(this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                    }
                }

                if (arrFile.Count > 0)
                {
                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUpload_NonAssync();

                    if (File.Exists(arrFile[0].ToString()))
                        File.Delete(arrFile[0].ToString());
                    if (File.Exists(arrFile[1].ToString()))
                        File.Delete(arrFile[1].ToString());
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        private void FileUpload_Report()
        {
            try
            {
                // 화일서버 연결정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                // 첨부파일 저장경로정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0010");

                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                //for (int i = 0; i < this.uGridAVLFile.Rows.Count; i++)
                //{
                //    if (this.uGridAVLFile.Rows[i].Hidden == false)
                //    {
                //        if (this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\"))
                //        {
                //            // 파일 이름변경(공장+거래처코드+순번+파일명)
                //            FileInfo fileDoc = new FileInfo(this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString());
                //            string strUploadFile = fileDoc.DirectoryName + "\\" + this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + this.uGridAVLFile.Rows[i].RowSelectorNumber.ToString()
                //                + "-" + fileDoc.Name;

                //            //변경한 화일이 있으면 삭제하기
                //            if (File.Exists(strUploadFile))
                //                File.Delete(strUploadFile);
                //            //변경한 화일이름으로 복사하기
                //            File.Copy(this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString(), strUploadFile);
                //            arrFile.Add(strUploadFile);
                //        }
                //    }
                //}
                //결과 ReportFile 첨부
                if (!this.uTextResult.Text.Equals(string.Empty))
                {
                    if (this.uTextResult.Text.Contains(":\\"))
                    {
                        //파일 이름 변경
                        FileInfo fileDoc = new FileInfo(this.uTextResult.Text);
                        string strUploadFile = fileDoc.DirectoryName + "\\" + this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + "ResultReport"
                            + "-" + fileDoc.Name;

                        //변경한 파일이 있으면 삭제
                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);

                        File.Copy(this.uTextResult.Text, strUploadFile);
                        arrFile.Add(strUploadFile);
                    }
                    
                }
                //개선 ReportFile 첨부
                if (!this.uTextImprove.Text.Equals(string.Empty))
                {
                    if (this.uTextImprove.Text.Contains(":\\"))
                    {
                        //파일이름 변경
                        FileInfo fileDoc = new FileInfo(this.uTextImprove.Text);
                        string strUploadFile = fileDoc.DirectoryName + "\\" + this.uComboPlant.Value.ToString() + "-" + this.uTextVendorCode.Text + "-" + "ImproveReport"
                            + "-" + fileDoc.Name;

                        //변경한 파일이 있으면 삭제
                        if (File.Exists(strUploadFile))
                            File.Delete(strUploadFile);

                        File.Copy(this.uTextImprove.Text, strUploadFile);
                        arrFile.Add(strUploadFile);
                    }
                }

                if (arrFile.Count > 0)
                {
                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUpload_NonAssync();

                    if (File.Exists(arrFile[0].ToString()))
                        File.Delete(arrFile[0].ToString());
                    if (File.Exists(arrFile[1].ToString()))
                        File.Delete(arrFile[1].ToString());
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// DB에 이미 저장된 정보가 있는지 확인
        /// </summary>
        private void CheckData()
        {
            try
            {
                if (this.uComboPlant.Value.ToString() != "" && this.uTextVendorCode.Text != "")
                {
                    // SystemInfo ReSourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (Search_AVLDetail(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000041", "M000845",
                                            Infragistics.Win.HAlign.Right);

                        // 프로그래스바 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        Search_AVLConfirm(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text);
                        Search_AVLFile(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text);
                        Search_AVLHistory(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text);

                        // POPUP창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        //#region 이벤트
        // Contents GroupBox 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 185);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridAVLList.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridAVLList.Height = 720;

                    for (int i = 0; i < uGridAVLList.Rows.Count; i++)
                    {
                        uGridAVLList.Rows[i].Fixed = false;
                    }

                    // 컨트롤 초기화
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridAVLFile, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridAVLConfirm, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }   

        // 인증서 보유현황 CellChange 이벤트
        private void uGridAVLConfirm_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 수정시 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 순번 처리
                e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 CellChange 이벤트
        private void uGridAVLFile_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 수정시 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 순번 처리
                e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 거래처코드 팝업창
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 거래처 팝업창
        private void uTextVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextVendorCode.Text = frmPOP.CustomerCode;
                this.uTextVendorName.Text = frmPOP.CustomerName;

                if (this.uComboPlant.Value.ToString() != "")
                    CheckData();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성자 팝업창
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uComboPlant.Value.ToString() != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = uComboPlant.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlant.Value.ToString() != frmPOP.PlantCode)
                    {
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                    , msg.GetMessge_Text("M001254", strLang) + this.uComboPlant.Text + msg.GetMessge_Text("M000001", strLang)
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextWriteUserID.Text = "";
                        this.uTextWriteUserName.Text = "";
                    }
                    else
                    {
                        this.uTextWriteUserID.Text = frmPOP.UserID;
                        this.uTextWriteUserName.Text = frmPOP.UserName;
                    }
                }
                else
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성자 키다운 이벤트
        private void uTextWriteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() != "")
                        {
                            String strWriteID = this.uTextWriteUserID.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserInfo(m_resSys.GetString("SYS_PLANTCODE"), strWriteID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextWriteUserID.Text = "";
                                this.uTextWriteUserName.Text = "";
                            }
                            else
                            {
                                this.uTextWriteUserName.Text = strRtnUserName;
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);
                            
                            this.uComboPlant.DropDown();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteUserID.TextLength <= 1 || this.uTextWriteUserID.SelectedText == this.uTextWriteUserID.Text)
                    {
                        this.uTextWriteUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 행삭제 버튼
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridAVLFile.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridAVLFile.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridAVLFile.Rows[i].Hidden = true;
                    }
                    else
                    {
                        this.uGridAVLFile.Rows[i].Cells["Seq"].Value = this.uGridAVLFile.Rows[i].RowSelectorNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 인증서정보 행삭제 버튼
        private void uButtonDeleteRow2_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridAVLConfirm.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridAVLConfirm.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridAVLConfirm.Rows[i].Hidden = true;
                    }
                    else
                    {
                        this.uGridAVLConfirm.Rows[i].Cells["Seq"].Value = this.uGridAVLConfirm.Rows[i].RowSelectorNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // AVL 리스트 더블클릭시 상세정보 조회
        private void uGridAVLList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                // 현재 클릭된 행 고정
                e.Row.Fixed = true;

                // PK 수정불가상태로
                this.uComboPlant.Enabled = false;
                this.uTextVendorCode.Enabled = false;
                
                // ContentsArea 펼침상태로
                this.uGroupBoxContentsArea.Expanded = true;

                // 검색조건용 변수 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strVendorCode = e.Row.Cells["VendorCode"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                Search_AVLDetail(strPlantCode, strVendorCode);
                Search_AVLConfirm(strPlantCode, strVendorCode);
                Search_AVLFile(strPlantCode, strVendorCode);
                
                //조회 후 이력이 없어도 저장 가능하도록 변경
                if (e.Row.Cells["AuditGrade"].Value.ToString().Equals(string.Empty))
                {
                    this.uTextVendorCode.Text = e.Row.Cells["VendorCode"].Value.ToString();
                    this.uTextVendorName.Text = e.Row.Cells["VendorName"].Value.ToString();
                }

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);



                //////////// 조회 Method 호출
                //////////bool bolCheck = Search_AVLDetail(strPlantCode, strVendorCode);
                //////////if (bolCheck == true)
                //////////{
                //////////    Search_AVLConfirm(strPlantCode, strVendorCode);
                //////////    Search_AVLFile(strPlantCode, strVendorCode);
                //////////    bolCheck = Search_AVLHistory(strPlantCode, strVendorCode);

                //////////    // 팦업창 Close
                //////////    this.MdiParent.Cursor = Cursors.Default;
                //////////    m_ProgressPopup.mfCloseProgressPopup(this);

                //////////    //////if (bolCheck == false)
                //////////    //////{
                //////////    //////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //////////    //////                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //////////    //////                        "오류창", "데이터조회 오류", "데이터를 조회하는중 오류가 발생했습니다",
                //////////    //////                        Infragistics.Win.HAlign.Right);

                //////////    //////    // ContentsArea 접은상태로
                //////////    //////    this.uGroupBoxContentsArea.Expanded = false;
                //////////    //////}
                //////////}
                //////////else
                //////////{
                //////////    //DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //////////    //                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //////////    //                        "오류창", "데이터조회 오류", "데이터를 조회하는중 오류가 발생했습니다",
                //////////    //                        Infragistics.Win.HAlign.Right);
                    
                //////////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                //////////                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //////////                            "M000229", "M000378", "M000034",
                //////////                            Infragistics.Win.HAlign.Right);

                //////////    // 팦업창 Close
                //////////    this.MdiParent.Cursor = Cursors.Default;
                //////////    m_ProgressPopup.mfCloseProgressPopup(this);

                //////////    // ContentsArea 접은상태로
                //////////    this.uGroupBoxContentsArea.Expanded = false;                    
                //////////}

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일그리드 첨부파일버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridAVLFile_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "FilePassName")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        e.Cell.Value = strImageFile;

                        QRPGlobal grdImg = new QRPGlobal();
                        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 인증서 그리드 첨부파일버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridAVLConfirm_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "FilePassName")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        e.Cell.Value = strImageFile;

                        QRPGlobal grdImg = new QRPGlobal();
                        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 다운로드 버튼 이벤트
        private void uButtonDownFile_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                int intFileNum = 0;
                for (int i = 0; i < this.uGridAVLFile.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                    if (this.uGridAVLFile.Rows[i].Hidden == false && this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\") == false)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0010");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridAVLFile.Rows.Count; i++)
                    {
                        if (this.uGridAVLFile.Rows[i].Hidden == false && this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\") == false)
                            arrFile.Add(this.uGridAVLFile.Rows[i].Cells["FilePassName"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 인증서 첨부파일 다운로드 이벤트
        private void uButtonDownConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                int intFileNum = 0;
                for (int i = 0; i < this.uGridAVLFile.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                    if (this.uGridAVLConfirm.Rows[i].Hidden == false && this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\") == false)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0010");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridAVLConfirm.Rows.Count; i++)
                    {
                        if (this.uGridAVLConfirm.Rows[i].Hidden == false && this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString().Contains(":\\") == false)
                            arrFile.Add(this.uGridAVLConfirm.Rows[i].Cells["FilePassName"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장콤보박스 값이 변화될때 이벤트
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uTextVendorCode.Text != "")
                    CheckData();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINSZ0004_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //버튼클릭이벤트(감사결과Report)
        private void uTextResult_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "All files (*.*)|*.*";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    this.uTextResult.Text = strImageFile;
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        //버튼클릭이벤트(개선Report)
        private void uTextImprove_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "All files (*.*)|*.*";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    this.uTextImprove.Text = strImageFile;
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uGridAVLHistory_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                

                if (e.Cell.Column.Key.ToString().Equals("ResultReport") || e.Cell.Column.Key.ToString().Equals("ImproveReport"))
                {
                    if (e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001185", "M001402", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";
                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPBrowser brwChannel = new QRPBrowser();

                            //화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                            //설비이미지 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0010");

                            //설비이미지 Upload하기
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            arrFile.Add(e.Cell.Value.ToString());


                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uTextSearchVendorCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    string strVendorCode = this.uTextSearchVendorCode.Text;

                    DataTable dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

                    if (dtVendor.Rows.Count.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M000202", "M001095", "M000167", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        this.uTextSearchVendorCode.Text = dtVendor.Rows[0]["VendorCode"].ToString();
                        this.uTextSearchVendorName.Text = dtVendor.Rows[0]["VendorName"].ToString();
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextSearchVendorName.Text = "";
                    this.uTextSearchVendorCode.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextVendorCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    string strVendorCode = this.uTextVendorCode.Text;

                    DataTable dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

                    if (dtVendor.Rows.Count.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M000202", "M001095", "M000167", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        this.uTextVendorCode.Text = dtVendor.Rows[0]["VendorCode"].ToString();
                        this.uTextVendorName.Text = dtVendor.Rows[0]["VendorName"].ToString();
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextVendorName.Text = "";
                    this.uTextVendorCode.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void SearchData_ForMoveForm()
        {
            try
            {
                // 조회 Method 호출
                bool bolCheck = Search_AVLDetail(PlantCode, VendorCode);
                if (bolCheck == true)
                {
                    Search_AVLConfirm(PlantCode, VendorCode);
                    Search_AVLFile(PlantCode, VendorCode);
                    bolCheck = Search_AVLHistory(PlantCode, VendorCode);

                    //////if (bolCheck == false)
                    //////{
                    //////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //////                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //////                        "오류창", "데이터조회 오류", "데이터를 조회하는중 오류가 발생했습니다",
                    //////                        Infragistics.Win.HAlign.Right);

                    //////    // ContentsArea 접은상태로
                    //////    this.uGroupBoxContentsArea.Expanded = false;
                    //////}
                }
                else
                {
                    //DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                        "오류창", "데이터조회 오류", "데이터를 조회하는중 오류가 발생했습니다",
                    //                        Infragistics.Win.HAlign.Right);

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M000229", "M000378", "M000034",
                                            Infragistics.Win.HAlign.Right);

                    // ContentsArea 접은상태로
                    this.uGroupBoxContentsArea.Expanded = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
