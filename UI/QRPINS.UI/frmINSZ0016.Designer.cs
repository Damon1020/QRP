﻿namespace QRPINS.UI
{
    partial class frmINSZ0016
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton("Search");
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0016));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonS1 = new Infragistics.Win.Misc.UltraButton();
            this.uComboS1Dept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGridS1File = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uCheckS1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextS1UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS1User = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS1UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextS1Comment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS1Dept = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS1File = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS1Complete = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS1Result = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonS1DelRow = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonS2 = new Infragistics.Win.Misc.UltraButton();
            this.uTextS2Analysis = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridS2File = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboS2Dept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uCheckS2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelS2Dept = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS2Complete = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS2UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS2Date = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS2User = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS2File = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS2UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS2Analysis = new Infragistics.Win.Misc.UltraLabel();
            this.uDateS2Date = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uButtonS2DelRow = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonS3 = new Infragistics.Win.Misc.UltraButton();
            this.uTextS3Comment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridS3File = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboS3Dept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uCheckS3 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelS3Dept = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS3Complete = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS3Date = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS3User = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS3UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateS3Date = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextS3UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS3File = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS3Comment = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonS3DelRow = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonS4 = new Infragistics.Win.Misc.UltraButton();
            this.uTextS4Comment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridS4File = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uCheckS4 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelS4Complete = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS4Date = new Infragistics.Win.Misc.UltraLabel();
            this.uDateS4Date = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextS4UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS4User = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS4UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelS4File = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS4Comment = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonS4DelRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridAbormal = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateToAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchLotRelease = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAriseDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLotRelease = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGridEmail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uComboDept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonDel_Email = new Infragistics.Win.Misc.UltraButton();
            this.uLabelEmail = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboS0Dept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelS0Dept = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonAbnormal = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckFir4MMaterialFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckFir4MEnviroFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckFir4MMethodFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckFir4MMachineFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckFir4MManFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextRegistUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRegistUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uOptionAbnormal = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelAbnormal = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRegistUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAttachFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAttachFile = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMeasureDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ulabelMeasureDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAriseCause = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ulabelAriseCause = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxStep5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonReturn = new Infragistics.Win.Misc.UltraButton();
            this.uTextS5Comment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckReturn = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uOptionOkNg = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uComboReturnStep = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uCheckS5 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelS5ReturnDept = new Infragistics.Win.Misc.UltraLabel();
            this.uTextS5UserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextS5UserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateS5Date = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelS5Comment = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS5Complete = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelOkNg = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS5User = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS5Date = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextManageNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelManageNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerProductCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProcessHoldState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProcessHoldState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotProcessState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotProcessState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAcceptTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAcceptDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonAccept = new Infragistics.Win.Misc.UltraButton();
            this.uLabelAcceptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckMESReleaseTFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMESFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelPack = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAriseProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextBinName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelBinName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWorkUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAriseTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProcessYield = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProcessYield = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotQty = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAriseDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOccurDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAriseProcessCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAriseProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTabStep = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uGroupBoxLoss = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridProcLowD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxGrid = new Infragistics.Win.Misc.UltraGroupBox();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS1Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS1File)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1Comment)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2Analysis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS2File)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS2Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS2Date)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3Comment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS3File)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS3Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS3Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserID)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4Comment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS4File)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS4Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAbormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLotRelease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS0Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox6)).BeginInit();
            this.uGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MMaterialFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MEnviroFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MMethodFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MMachineFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MManFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionAbnormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAttachFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStep5)).BeginInit();
            this.uGroupBoxStep5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS5Comment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionOkNg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReturnStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS5UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS5UserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS5Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).BeginInit();
            this.uGroupBoxInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManageNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessHoldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMESReleaseTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMESFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBinName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessYield)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabStep)).BeginInit();
            this.uTabStep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLoss)).BeginInit();
            this.uGroupBoxLoss.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcLowD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGrid)).BeginInit();
            this.uGroupBoxGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonS1);
            this.ultraTabPageControl1.Controls.Add(this.uComboS1Dept);
            this.ultraTabPageControl1.Controls.Add(this.uGridS1File);
            this.ultraTabPageControl1.Controls.Add(this.uCheckS1);
            this.ultraTabPageControl1.Controls.Add(this.uTextS1UserID);
            this.ultraTabPageControl1.Controls.Add(this.uLabelS1User);
            this.ultraTabPageControl1.Controls.Add(this.uTextS1UserName);
            this.ultraTabPageControl1.Controls.Add(this.uTextS1Comment);
            this.ultraTabPageControl1.Controls.Add(this.uLabelS1Dept);
            this.ultraTabPageControl1.Controls.Add(this.uLabelS1File);
            this.ultraTabPageControl1.Controls.Add(this.uLabelS1Complete);
            this.ultraTabPageControl1.Controls.Add(this.uLabelS1Result);
            this.ultraTabPageControl1.Controls.Add(this.uButtonS1DelRow);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(904, 197);
            // 
            // uButtonS1
            // 
            this.uButtonS1.Location = new System.Drawing.Point(655, 8);
            this.uButtonS1.Name = "uButtonS1";
            this.uButtonS1.Size = new System.Drawing.Size(86, 24);
            this.uButtonS1.TabIndex = 22;
            this.uButtonS1.Text = "FileDown";
            // 
            // uComboS1Dept
            // 
            this.uComboS1Dept.Location = new System.Drawing.Point(137, 152);
            this.uComboS1Dept.MaxLength = 50;
            this.uComboS1Dept.Name = "uComboS1Dept";
            this.uComboS1Dept.Size = new System.Drawing.Size(86, 21);
            this.uComboS1Dept.TabIndex = 7;
            // 
            // uGridS1File
            // 
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridS1File.DisplayLayout.Appearance = appearance70;
            this.uGridS1File.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridS1File.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance71.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance71.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance71.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS1File.DisplayLayout.GroupByBox.Appearance = appearance71;
            appearance72.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS1File.DisplayLayout.GroupByBox.BandLabelAppearance = appearance72;
            this.uGridS1File.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance73.BackColor2 = System.Drawing.SystemColors.Control;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS1File.DisplayLayout.GroupByBox.PromptAppearance = appearance73;
            this.uGridS1File.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridS1File.DisplayLayout.MaxRowScrollRegions = 1;
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridS1File.DisplayLayout.Override.ActiveCellAppearance = appearance74;
            appearance75.BackColor = System.Drawing.SystemColors.Highlight;
            appearance75.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridS1File.DisplayLayout.Override.ActiveRowAppearance = appearance75;
            this.uGridS1File.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridS1File.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            this.uGridS1File.DisplayLayout.Override.CardAreaAppearance = appearance76;
            appearance77.BorderColor = System.Drawing.Color.Silver;
            appearance77.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridS1File.DisplayLayout.Override.CellAppearance = appearance77;
            this.uGridS1File.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridS1File.DisplayLayout.Override.CellPadding = 0;
            appearance78.BackColor = System.Drawing.SystemColors.Control;
            appearance78.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance78.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS1File.DisplayLayout.Override.GroupByRowAppearance = appearance78;
            appearance79.TextHAlignAsString = "Left";
            this.uGridS1File.DisplayLayout.Override.HeaderAppearance = appearance79;
            this.uGridS1File.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridS1File.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            this.uGridS1File.DisplayLayout.Override.RowAppearance = appearance80;
            this.uGridS1File.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridS1File.DisplayLayout.Override.TemplateAddRowAppearance = appearance81;
            this.uGridS1File.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridS1File.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridS1File.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridS1File.Location = new System.Drawing.Point(470, 32);
            this.uGridS1File.Name = "uGridS1File";
            this.uGridS1File.Size = new System.Drawing.Size(432, 162);
            this.uGridS1File.TabIndex = 6;
            // 
            // uCheckS1
            // 
            this.uCheckS1.Location = new System.Drawing.Point(137, 178);
            this.uCheckS1.Name = "uCheckS1";
            this.uCheckS1.Size = new System.Drawing.Size(17, 16);
            this.uCheckS1.TabIndex = 6;
            // 
            // uTextS1UserID
            // 
            appearance122.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS1UserID.Appearance = appearance122;
            this.uTextS1UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance114.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance114.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance114;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS1UserID.ButtonsRight.Add(editorButton1);
            this.uTextS1UserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextS1UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS1UserID.Location = new System.Drawing.Point(137, 8);
            this.uTextS1UserID.MaxLength = 20;
            this.uTextS1UserID.Name = "uTextS1UserID";
            this.uTextS1UserID.Size = new System.Drawing.Size(86, 21);
            this.uTextS1UserID.TabIndex = 5;
            // 
            // uLabelS1User
            // 
            this.uLabelS1User.Location = new System.Drawing.Point(10, 12);
            this.uLabelS1User.Name = "uLabelS1User";
            this.uLabelS1User.Size = new System.Drawing.Size(123, 20);
            this.uLabelS1User.TabIndex = 3;
            this.uLabelS1User.Text = "Analyst";
            // 
            // uTextS1UserName
            // 
            appearance109.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS1UserName.Appearance = appearance109;
            this.uTextS1UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS1UserName.Location = new System.Drawing.Point(226, 8);
            this.uTextS1UserName.Name = "uTextS1UserName";
            this.uTextS1UserName.ReadOnly = true;
            this.uTextS1UserName.Size = new System.Drawing.Size(86, 21);
            this.uTextS1UserName.TabIndex = 5;
            // 
            // uTextS1Comment
            // 
            this.uTextS1Comment.Location = new System.Drawing.Point(137, 32);
            this.uTextS1Comment.MaxLength = 2000;
            this.uTextS1Comment.Multiline = true;
            this.uTextS1Comment.Name = "uTextS1Comment";
            this.uTextS1Comment.Size = new System.Drawing.Size(326, 116);
            this.uTextS1Comment.TabIndex = 5;
            // 
            // uLabelS1Dept
            // 
            this.uLabelS1Dept.Location = new System.Drawing.Point(10, 152);
            this.uLabelS1Dept.Name = "uLabelS1Dept";
            this.uLabelS1Dept.Size = new System.Drawing.Size(123, 20);
            this.uLabelS1Dept.TabIndex = 3;
            this.uLabelS1Dept.Text = "Step2. 진행부서";
            // 
            // uLabelS1File
            // 
            this.uLabelS1File.Location = new System.Drawing.Point(470, 12);
            this.uLabelS1File.Name = "uLabelS1File";
            this.uLabelS1File.Size = new System.Drawing.Size(110, 20);
            this.uLabelS1File.TabIndex = 3;
            this.uLabelS1File.Text = "File Attachment";
            // 
            // uLabelS1Complete
            // 
            this.uLabelS1Complete.Location = new System.Drawing.Point(10, 176);
            this.uLabelS1Complete.Name = "uLabelS1Complete";
            this.uLabelS1Complete.Size = new System.Drawing.Size(123, 20);
            this.uLabelS1Complete.TabIndex = 3;
            this.uLabelS1Complete.Text = "작성완료";
            // 
            // uLabelS1Result
            // 
            this.uLabelS1Result.Location = new System.Drawing.Point(10, 36);
            this.uLabelS1Result.Name = "uLabelS1Result";
            this.uLabelS1Result.Size = new System.Drawing.Size(123, 36);
            this.uLabelS1Result.TabIndex = 3;
            this.uLabelS1Result.Text = "Initial Analysis Result";
            // 
            // uButtonS1DelRow
            // 
            this.uButtonS1DelRow.Location = new System.Drawing.Point(583, 8);
            this.uButtonS1DelRow.Name = "uButtonS1DelRow";
            this.uButtonS1DelRow.Size = new System.Drawing.Size(69, 24);
            this.uButtonS1DelRow.TabIndex = 22;
            this.uButtonS1DelRow.Text = "행 삭제";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uButtonS2);
            this.ultraTabPageControl2.Controls.Add(this.uTextS2Analysis);
            this.ultraTabPageControl2.Controls.Add(this.uGridS2File);
            this.ultraTabPageControl2.Controls.Add(this.uComboS2Dept);
            this.ultraTabPageControl2.Controls.Add(this.uCheckS2);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2Dept);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2Complete);
            this.ultraTabPageControl2.Controls.Add(this.uTextS2UserName);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2Date);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2User);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2File);
            this.ultraTabPageControl2.Controls.Add(this.uTextS2UserID);
            this.ultraTabPageControl2.Controls.Add(this.uLabelS2Analysis);
            this.ultraTabPageControl2.Controls.Add(this.uDateS2Date);
            this.ultraTabPageControl2.Controls.Add(this.uButtonS2DelRow);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(904, 197);
            // 
            // uButtonS2
            // 
            this.uButtonS2.Location = new System.Drawing.Point(655, 8);
            this.uButtonS2.Name = "uButtonS2";
            this.uButtonS2.Size = new System.Drawing.Size(86, 24);
            this.uButtonS2.TabIndex = 23;
            this.uButtonS2.Text = "FileDown";
            // 
            // uTextS2Analysis
            // 
            this.uTextS2Analysis.Location = new System.Drawing.Point(137, 60);
            this.uTextS2Analysis.MaxLength = 2000;
            this.uTextS2Analysis.Multiline = true;
            this.uTextS2Analysis.Name = "uTextS2Analysis";
            this.uTextS2Analysis.Size = new System.Drawing.Size(326, 88);
            this.uTextS2Analysis.TabIndex = 18;
            // 
            // uGridS2File
            // 
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridS2File.DisplayLayout.Appearance = appearance85;
            this.uGridS2File.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridS2File.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance86.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance86.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance86.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS2File.DisplayLayout.GroupByBox.Appearance = appearance86;
            appearance87.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS2File.DisplayLayout.GroupByBox.BandLabelAppearance = appearance87;
            this.uGridS2File.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance88.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance88.BackColor2 = System.Drawing.SystemColors.Control;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance88.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS2File.DisplayLayout.GroupByBox.PromptAppearance = appearance88;
            this.uGridS2File.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridS2File.DisplayLayout.MaxRowScrollRegions = 1;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridS2File.DisplayLayout.Override.ActiveCellAppearance = appearance89;
            appearance90.BackColor = System.Drawing.SystemColors.Highlight;
            appearance90.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridS2File.DisplayLayout.Override.ActiveRowAppearance = appearance90;
            this.uGridS2File.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridS2File.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            this.uGridS2File.DisplayLayout.Override.CardAreaAppearance = appearance91;
            appearance92.BorderColor = System.Drawing.Color.Silver;
            appearance92.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridS2File.DisplayLayout.Override.CellAppearance = appearance92;
            this.uGridS2File.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridS2File.DisplayLayout.Override.CellPadding = 0;
            appearance93.BackColor = System.Drawing.SystemColors.Control;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS2File.DisplayLayout.Override.GroupByRowAppearance = appearance93;
            appearance94.TextHAlignAsString = "Left";
            this.uGridS2File.DisplayLayout.Override.HeaderAppearance = appearance94;
            this.uGridS2File.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridS2File.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.Color.Silver;
            this.uGridS2File.DisplayLayout.Override.RowAppearance = appearance95;
            this.uGridS2File.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridS2File.DisplayLayout.Override.TemplateAddRowAppearance = appearance96;
            this.uGridS2File.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridS2File.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridS2File.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridS2File.Location = new System.Drawing.Point(470, 32);
            this.uGridS2File.Name = "uGridS2File";
            this.uGridS2File.Size = new System.Drawing.Size(432, 168);
            this.uGridS2File.TabIndex = 17;
            this.uGridS2File.Text = "ultraGrid2";
            // 
            // uComboS2Dept
            // 
            this.uComboS2Dept.Location = new System.Drawing.Point(137, 152);
            this.uComboS2Dept.MaxLength = 50;
            this.uComboS2Dept.Name = "uComboS2Dept";
            this.uComboS2Dept.Size = new System.Drawing.Size(86, 21);
            this.uComboS2Dept.TabIndex = 16;
            // 
            // uCheckS2
            // 
            this.uCheckS2.Location = new System.Drawing.Point(137, 176);
            this.uCheckS2.Name = "uCheckS2";
            this.uCheckS2.Size = new System.Drawing.Size(17, 20);
            this.uCheckS2.TabIndex = 15;
            // 
            // uLabelS2Dept
            // 
            this.uLabelS2Dept.Location = new System.Drawing.Point(10, 152);
            this.uLabelS2Dept.Name = "uLabelS2Dept";
            this.uLabelS2Dept.Size = new System.Drawing.Size(123, 20);
            this.uLabelS2Dept.TabIndex = 13;
            this.uLabelS2Dept.Text = "Step3. 진행부서";
            // 
            // uLabelS2Complete
            // 
            this.uLabelS2Complete.Location = new System.Drawing.Point(10, 176);
            this.uLabelS2Complete.Name = "uLabelS2Complete";
            this.uLabelS2Complete.Size = new System.Drawing.Size(123, 20);
            this.uLabelS2Complete.TabIndex = 14;
            this.uLabelS2Complete.Text = "작성완료";
            // 
            // uTextS2UserName
            // 
            appearance110.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS2UserName.Appearance = appearance110;
            this.uTextS2UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS2UserName.Location = new System.Drawing.Point(226, 36);
            this.uTextS2UserName.Name = "uTextS2UserName";
            this.uTextS2UserName.ReadOnly = true;
            this.uTextS2UserName.Size = new System.Drawing.Size(86, 21);
            this.uTextS2UserName.TabIndex = 5;
            // 
            // uLabelS2Date
            // 
            this.uLabelS2Date.Location = new System.Drawing.Point(10, 12);
            this.uLabelS2Date.Name = "uLabelS2Date";
            this.uLabelS2Date.Size = new System.Drawing.Size(123, 20);
            this.uLabelS2Date.TabIndex = 3;
            this.uLabelS2Date.Text = "Date";
            // 
            // uLabelS2User
            // 
            this.uLabelS2User.Location = new System.Drawing.Point(10, 36);
            this.uLabelS2User.Name = "uLabelS2User";
            this.uLabelS2User.Size = new System.Drawing.Size(123, 20);
            this.uLabelS2User.TabIndex = 3;
            this.uLabelS2User.Text = "Analysist";
            // 
            // uLabelS2File
            // 
            this.uLabelS2File.Location = new System.Drawing.Point(470, 12);
            this.uLabelS2File.Name = "uLabelS2File";
            this.uLabelS2File.Size = new System.Drawing.Size(110, 20);
            this.uLabelS2File.TabIndex = 3;
            this.uLabelS2File.Text = "File Attachment";
            // 
            // uTextS2UserID
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS2UserID.Appearance = appearance29;
            this.uTextS2UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance111.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance111.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance111;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS2UserID.ButtonsRight.Add(editorButton2);
            this.uTextS2UserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextS2UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS2UserID.Location = new System.Drawing.Point(137, 36);
            this.uTextS2UserID.MaxLength = 20;
            this.uTextS2UserID.Name = "uTextS2UserID";
            this.uTextS2UserID.Size = new System.Drawing.Size(86, 21);
            this.uTextS2UserID.TabIndex = 5;
            // 
            // uLabelS2Analysis
            // 
            this.uLabelS2Analysis.Location = new System.Drawing.Point(10, 60);
            this.uLabelS2Analysis.Name = "uLabelS2Analysis";
            this.uLabelS2Analysis.Size = new System.Drawing.Size(123, 20);
            this.uLabelS2Analysis.TabIndex = 3;
            this.uLabelS2Analysis.Text = "Analysis Result";
            // 
            // uDateS2Date
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateS2Date.Appearance = appearance39;
            this.uDateS2Date.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateS2Date.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateS2Date.Location = new System.Drawing.Point(137, 12);
            this.uDateS2Date.Name = "uDateS2Date";
            this.uDateS2Date.Size = new System.Drawing.Size(86, 21);
            this.uDateS2Date.TabIndex = 4;
            // 
            // uButtonS2DelRow
            // 
            this.uButtonS2DelRow.Location = new System.Drawing.Point(583, 8);
            this.uButtonS2DelRow.Name = "uButtonS2DelRow";
            this.uButtonS2DelRow.Size = new System.Drawing.Size(69, 24);
            this.uButtonS2DelRow.TabIndex = 24;
            this.uButtonS2DelRow.Text = "행 삭제";
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uButtonS3);
            this.ultraTabPageControl3.Controls.Add(this.uTextS3Comment);
            this.ultraTabPageControl3.Controls.Add(this.uGridS3File);
            this.ultraTabPageControl3.Controls.Add(this.uComboS3Dept);
            this.ultraTabPageControl3.Controls.Add(this.uCheckS3);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3Dept);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3Complete);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3Date);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3User);
            this.ultraTabPageControl3.Controls.Add(this.uTextS3UserName);
            this.ultraTabPageControl3.Controls.Add(this.uDateS3Date);
            this.ultraTabPageControl3.Controls.Add(this.uTextS3UserID);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3File);
            this.ultraTabPageControl3.Controls.Add(this.uLabelS3Comment);
            this.ultraTabPageControl3.Controls.Add(this.uButtonS3DelRow);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(904, 197);
            // 
            // uButtonS3
            // 
            this.uButtonS3.Location = new System.Drawing.Point(655, 8);
            this.uButtonS3.Name = "uButtonS3";
            this.uButtonS3.Size = new System.Drawing.Size(86, 24);
            this.uButtonS3.TabIndex = 24;
            this.uButtonS3.Text = "FileDown";
            // 
            // uTextS3Comment
            // 
            this.uTextS3Comment.Location = new System.Drawing.Point(137, 60);
            this.uTextS3Comment.MaxLength = 2000;
            this.uTextS3Comment.Multiline = true;
            this.uTextS3Comment.Name = "uTextS3Comment";
            this.uTextS3Comment.Size = new System.Drawing.Size(326, 88);
            this.uTextS3Comment.TabIndex = 23;
            // 
            // uGridS3File
            // 
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridS3File.DisplayLayout.Appearance = appearance97;
            this.uGridS3File.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridS3File.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance98.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance98.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance98.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance98.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS3File.DisplayLayout.GroupByBox.Appearance = appearance98;
            appearance99.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS3File.DisplayLayout.GroupByBox.BandLabelAppearance = appearance99;
            this.uGridS3File.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance100.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance100.BackColor2 = System.Drawing.SystemColors.Control;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS3File.DisplayLayout.GroupByBox.PromptAppearance = appearance100;
            this.uGridS3File.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridS3File.DisplayLayout.MaxRowScrollRegions = 1;
            appearance101.BackColor = System.Drawing.SystemColors.Window;
            appearance101.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridS3File.DisplayLayout.Override.ActiveCellAppearance = appearance101;
            appearance102.BackColor = System.Drawing.SystemColors.Highlight;
            appearance102.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridS3File.DisplayLayout.Override.ActiveRowAppearance = appearance102;
            this.uGridS3File.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridS3File.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance103.BackColor = System.Drawing.SystemColors.Window;
            this.uGridS3File.DisplayLayout.Override.CardAreaAppearance = appearance103;
            appearance104.BorderColor = System.Drawing.Color.Silver;
            appearance104.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridS3File.DisplayLayout.Override.CellAppearance = appearance104;
            this.uGridS3File.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridS3File.DisplayLayout.Override.CellPadding = 0;
            appearance105.BackColor = System.Drawing.SystemColors.Control;
            appearance105.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance105.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance105.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance105.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS3File.DisplayLayout.Override.GroupByRowAppearance = appearance105;
            appearance106.TextHAlignAsString = "Left";
            this.uGridS3File.DisplayLayout.Override.HeaderAppearance = appearance106;
            this.uGridS3File.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridS3File.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance107.BackColor = System.Drawing.SystemColors.Window;
            appearance107.BorderColor = System.Drawing.Color.Silver;
            this.uGridS3File.DisplayLayout.Override.RowAppearance = appearance107;
            this.uGridS3File.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance108.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridS3File.DisplayLayout.Override.TemplateAddRowAppearance = appearance108;
            this.uGridS3File.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridS3File.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridS3File.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridS3File.Location = new System.Drawing.Point(470, 32);
            this.uGridS3File.Name = "uGridS3File";
            this.uGridS3File.Size = new System.Drawing.Size(432, 168);
            this.uGridS3File.TabIndex = 22;
            this.uGridS3File.Text = "ultraGrid3";
            // 
            // uComboS3Dept
            // 
            this.uComboS3Dept.Location = new System.Drawing.Point(137, 152);
            this.uComboS3Dept.MaxLength = 50;
            this.uComboS3Dept.Name = "uComboS3Dept";
            this.uComboS3Dept.Size = new System.Drawing.Size(86, 21);
            this.uComboS3Dept.TabIndex = 21;
            // 
            // uCheckS3
            // 
            this.uCheckS3.Location = new System.Drawing.Point(137, 180);
            this.uCheckS3.Name = "uCheckS3";
            this.uCheckS3.Size = new System.Drawing.Size(21, 16);
            this.uCheckS3.TabIndex = 20;
            // 
            // uLabelS3Dept
            // 
            this.uLabelS3Dept.Location = new System.Drawing.Point(10, 152);
            this.uLabelS3Dept.Name = "uLabelS3Dept";
            this.uLabelS3Dept.Size = new System.Drawing.Size(123, 20);
            this.uLabelS3Dept.TabIndex = 18;
            this.uLabelS3Dept.Text = "Step4. 진행부서";
            // 
            // uLabelS3Complete
            // 
            this.uLabelS3Complete.Location = new System.Drawing.Point(10, 176);
            this.uLabelS3Complete.Name = "uLabelS3Complete";
            this.uLabelS3Complete.Size = new System.Drawing.Size(123, 20);
            this.uLabelS3Complete.TabIndex = 19;
            this.uLabelS3Complete.Text = "작성완료";
            // 
            // uLabelS3Date
            // 
            this.uLabelS3Date.Location = new System.Drawing.Point(10, 12);
            this.uLabelS3Date.Name = "uLabelS3Date";
            this.uLabelS3Date.Size = new System.Drawing.Size(123, 20);
            this.uLabelS3Date.TabIndex = 3;
            this.uLabelS3Date.Text = "Date";
            // 
            // uLabelS3User
            // 
            this.uLabelS3User.Location = new System.Drawing.Point(10, 36);
            this.uLabelS3User.Name = "uLabelS3User";
            this.uLabelS3User.Size = new System.Drawing.Size(123, 20);
            this.uLabelS3User.TabIndex = 3;
            this.uLabelS3User.Text = "Analysist";
            // 
            // uTextS3UserName
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS3UserName.Appearance = appearance67;
            this.uTextS3UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS3UserName.Location = new System.Drawing.Point(226, 36);
            this.uTextS3UserName.Name = "uTextS3UserName";
            this.uTextS3UserName.ReadOnly = true;
            this.uTextS3UserName.Size = new System.Drawing.Size(86, 21);
            this.uTextS3UserName.TabIndex = 5;
            // 
            // uDateS3Date
            // 
            appearance40.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateS3Date.Appearance = appearance40;
            this.uDateS3Date.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateS3Date.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateS3Date.Location = new System.Drawing.Point(137, 12);
            this.uDateS3Date.Name = "uDateS3Date";
            this.uDateS3Date.Size = new System.Drawing.Size(86, 21);
            this.uDateS3Date.TabIndex = 4;
            // 
            // uTextS3UserID
            // 
            appearance68.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS3UserID.Appearance = appearance68;
            this.uTextS3UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance69.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance69.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance69;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS3UserID.ButtonsRight.Add(editorButton3);
            this.uTextS3UserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextS3UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS3UserID.Location = new System.Drawing.Point(137, 36);
            this.uTextS3UserID.MaxLength = 20;
            this.uTextS3UserID.Name = "uTextS3UserID";
            this.uTextS3UserID.Size = new System.Drawing.Size(86, 21);
            this.uTextS3UserID.TabIndex = 5;
            // 
            // uLabelS3File
            // 
            this.uLabelS3File.Location = new System.Drawing.Point(470, 12);
            this.uLabelS3File.Name = "uLabelS3File";
            this.uLabelS3File.Size = new System.Drawing.Size(110, 20);
            this.uLabelS3File.TabIndex = 3;
            this.uLabelS3File.Text = "File Attachment";
            // 
            // uLabelS3Comment
            // 
            this.uLabelS3Comment.Location = new System.Drawing.Point(10, 60);
            this.uLabelS3Comment.Name = "uLabelS3Comment";
            this.uLabelS3Comment.Size = new System.Drawing.Size(123, 32);
            this.uLabelS3Comment.TabIndex = 3;
            this.uLabelS3Comment.Text = "Crrective Action Comment";
            // 
            // uButtonS3DelRow
            // 
            this.uButtonS3DelRow.Location = new System.Drawing.Point(583, 8);
            this.uButtonS3DelRow.Name = "uButtonS3DelRow";
            this.uButtonS3DelRow.Size = new System.Drawing.Size(69, 24);
            this.uButtonS3DelRow.TabIndex = 25;
            this.uButtonS3DelRow.Text = "행 삭제";
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uButtonS4);
            this.ultraTabPageControl4.Controls.Add(this.uTextS4Comment);
            this.ultraTabPageControl4.Controls.Add(this.uGridS4File);
            this.ultraTabPageControl4.Controls.Add(this.uCheckS4);
            this.ultraTabPageControl4.Controls.Add(this.uLabelS4Complete);
            this.ultraTabPageControl4.Controls.Add(this.uLabelS4Date);
            this.ultraTabPageControl4.Controls.Add(this.uDateS4Date);
            this.ultraTabPageControl4.Controls.Add(this.uTextS4UserName);
            this.ultraTabPageControl4.Controls.Add(this.uLabelS4User);
            this.ultraTabPageControl4.Controls.Add(this.uTextS4UserID);
            this.ultraTabPageControl4.Controls.Add(this.uLabelS4File);
            this.ultraTabPageControl4.Controls.Add(this.uLabelS4Comment);
            this.ultraTabPageControl4.Controls.Add(this.uButtonS4DelRow);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(904, 197);
            // 
            // uButtonS4
            // 
            this.uButtonS4.Location = new System.Drawing.Point(655, 8);
            this.uButtonS4.Name = "uButtonS4";
            this.uButtonS4.Size = new System.Drawing.Size(86, 24);
            this.uButtonS4.TabIndex = 25;
            this.uButtonS4.Text = "FileDown";
            // 
            // uTextS4Comment
            // 
            this.uTextS4Comment.Location = new System.Drawing.Point(137, 60);
            this.uTextS4Comment.MaxLength = 2000;
            this.uTextS4Comment.Multiline = true;
            this.uTextS4Comment.Name = "uTextS4Comment";
            this.uTextS4Comment.Size = new System.Drawing.Size(326, 112);
            this.uTextS4Comment.TabIndex = 24;
            // 
            // uGridS4File
            // 
            appearance112.BackColor = System.Drawing.SystemColors.Window;
            appearance112.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridS4File.DisplayLayout.Appearance = appearance112;
            this.uGridS4File.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridS4File.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS4File.DisplayLayout.GroupByBox.Appearance = appearance56;
            appearance57.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS4File.DisplayLayout.GroupByBox.BandLabelAppearance = appearance57;
            this.uGridS4File.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance58.BackColor2 = System.Drawing.SystemColors.Control;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridS4File.DisplayLayout.GroupByBox.PromptAppearance = appearance58;
            this.uGridS4File.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridS4File.DisplayLayout.MaxRowScrollRegions = 1;
            appearance113.BackColor = System.Drawing.SystemColors.Window;
            appearance113.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridS4File.DisplayLayout.Override.ActiveCellAppearance = appearance113;
            appearance60.BackColor = System.Drawing.SystemColors.Highlight;
            appearance60.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridS4File.DisplayLayout.Override.ActiveRowAppearance = appearance60;
            this.uGridS4File.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridS4File.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            this.uGridS4File.DisplayLayout.Override.CardAreaAppearance = appearance61;
            appearance62.BorderColor = System.Drawing.Color.Silver;
            appearance62.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridS4File.DisplayLayout.Override.CellAppearance = appearance62;
            this.uGridS4File.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridS4File.DisplayLayout.Override.CellPadding = 0;
            appearance63.BackColor = System.Drawing.SystemColors.Control;
            appearance63.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance63.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance63.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridS4File.DisplayLayout.Override.GroupByRowAppearance = appearance63;
            appearance64.TextHAlignAsString = "Left";
            this.uGridS4File.DisplayLayout.Override.HeaderAppearance = appearance64;
            this.uGridS4File.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridS4File.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            this.uGridS4File.DisplayLayout.Override.RowAppearance = appearance65;
            this.uGridS4File.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance66.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridS4File.DisplayLayout.Override.TemplateAddRowAppearance = appearance66;
            this.uGridS4File.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridS4File.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridS4File.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridS4File.Location = new System.Drawing.Point(470, 32);
            this.uGridS4File.Name = "uGridS4File";
            this.uGridS4File.Size = new System.Drawing.Size(432, 168);
            this.uGridS4File.TabIndex = 23;
            this.uGridS4File.Text = "ultraGrid3";
            // 
            // uCheckS4
            // 
            this.uCheckS4.Location = new System.Drawing.Point(137, 180);
            this.uCheckS4.Name = "uCheckS4";
            this.uCheckS4.Size = new System.Drawing.Size(24, 16);
            this.uCheckS4.TabIndex = 22;
            // 
            // uLabelS4Complete
            // 
            this.uLabelS4Complete.Location = new System.Drawing.Point(10, 176);
            this.uLabelS4Complete.Name = "uLabelS4Complete";
            this.uLabelS4Complete.Size = new System.Drawing.Size(123, 20);
            this.uLabelS4Complete.TabIndex = 21;
            this.uLabelS4Complete.Text = "작성완료";
            // 
            // uLabelS4Date
            // 
            this.uLabelS4Date.Location = new System.Drawing.Point(10, 12);
            this.uLabelS4Date.Name = "uLabelS4Date";
            this.uLabelS4Date.Size = new System.Drawing.Size(123, 20);
            this.uLabelS4Date.TabIndex = 8;
            this.uLabelS4Date.Text = "Date";
            // 
            // uDateS4Date
            // 
            appearance115.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateS4Date.Appearance = appearance115;
            this.uDateS4Date.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateS4Date.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateS4Date.Location = new System.Drawing.Point(137, 12);
            this.uDateS4Date.Name = "uDateS4Date";
            this.uDateS4Date.Size = new System.Drawing.Size(86, 21);
            this.uDateS4Date.TabIndex = 9;
            // 
            // uTextS4UserName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS4UserName.Appearance = appearance16;
            this.uTextS4UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS4UserName.Location = new System.Drawing.Point(226, 36);
            this.uTextS4UserName.Name = "uTextS4UserName";
            this.uTextS4UserName.ReadOnly = true;
            this.uTextS4UserName.Size = new System.Drawing.Size(86, 21);
            this.uTextS4UserName.TabIndex = 12;
            // 
            // uLabelS4User
            // 
            this.uLabelS4User.Location = new System.Drawing.Point(10, 36);
            this.uLabelS4User.Name = "uLabelS4User";
            this.uLabelS4User.Size = new System.Drawing.Size(123, 20);
            this.uLabelS4User.TabIndex = 7;
            this.uLabelS4User.Text = "Responsible person";
            // 
            // uTextS4UserID
            // 
            appearance123.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS4UserID.Appearance = appearance123;
            this.uTextS4UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance124.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance124.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance124;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS4UserID.ButtonsRight.Add(editorButton4);
            this.uTextS4UserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextS4UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS4UserID.Location = new System.Drawing.Point(137, 36);
            this.uTextS4UserID.MaxLength = 20;
            this.uTextS4UserID.Name = "uTextS4UserID";
            this.uTextS4UserID.Size = new System.Drawing.Size(86, 21);
            this.uTextS4UserID.TabIndex = 11;
            // 
            // uLabelS4File
            // 
            this.uLabelS4File.Location = new System.Drawing.Point(470, 12);
            this.uLabelS4File.Name = "uLabelS4File";
            this.uLabelS4File.Size = new System.Drawing.Size(110, 20);
            this.uLabelS4File.TabIndex = 6;
            this.uLabelS4File.Text = "File Attachment";
            // 
            // uLabelS4Comment
            // 
            this.uLabelS4Comment.Location = new System.Drawing.Point(10, 60);
            this.uLabelS4Comment.Name = "uLabelS4Comment";
            this.uLabelS4Comment.Size = new System.Drawing.Size(123, 32);
            this.uLabelS4Comment.TabIndex = 6;
            this.uLabelS4Comment.Text = "Lot Disposition Recommendataion";
            // 
            // uButtonS4DelRow
            // 
            this.uButtonS4DelRow.Location = new System.Drawing.Point(583, 8);
            this.uButtonS4DelRow.Name = "uButtonS4DelRow";
            this.uButtonS4DelRow.Size = new System.Drawing.Size(69, 24);
            this.uButtonS4DelRow.TabIndex = 26;
            this.uButtonS4DelRow.Text = "행 삭제";
            // 
            // uGridAbormal
            // 
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAbormal.DisplayLayout.Appearance = appearance14;
            this.uGridAbormal.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAbormal.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAbormal.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAbormal.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGridAbormal.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAbormal.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.uGridAbormal.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAbormal.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAbormal.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAbormal.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridAbormal.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAbormal.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAbormal.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            appearance15.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAbormal.DisplayLayout.Override.CellAppearance = appearance15;
            this.uGridAbormal.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAbormal.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAbormal.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.uGridAbormal.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridAbormal.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAbormal.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGridAbormal.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGridAbormal.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAbormal.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.uGridAbormal.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAbormal.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAbormal.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAbormal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridAbormal.Location = new System.Drawing.Point(1, 0);
            this.uGridAbormal.Name = "uGridAbormal";
            this.uGridAbormal.Size = new System.Drawing.Size(915, 39);
            this.uGridAbormal.TabIndex = 3;
            // 
            // uGroupBoxSearchArea
            // 
            appearance13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance13;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLotRelease);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLotRelease);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 60);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // uDateToAriseDate
            // 
            this.uDateToAriseDate.Location = new System.Drawing.Point(470, 12);
            this.uDateToAriseDate.Name = "uDateToAriseDate";
            this.uDateToAriseDate.Size = new System.Drawing.Size(86, 21);
            this.uDateToAriseDate.TabIndex = 2;
            // 
            // uDateFromAriseDate
            // 
            this.uDateFromAriseDate.Location = new System.Drawing.Point(374, 12);
            this.uDateFromAriseDate.Name = "uDateFromAriseDate";
            this.uDateFromAriseDate.Size = new System.Drawing.Size(86, 21);
            this.uDateFromAriseDate.TabIndex = 2;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(713, 32);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(123, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Visible = false;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(374, 36);
            this.uComboSearchPackage.MaxLength = 50;
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(123, 21);
            this.uComboSearchPackage.TabIndex = 1;
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(117, 36);
            this.uComboSearchCustomer.MaxLength = 50;
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(123, 21);
            this.uComboSearchCustomer.TabIndex = 1;
            // 
            // uComboSearchLotRelease
            // 
            this.uComboSearchLotRelease.Location = new System.Drawing.Point(117, 12);
            this.uComboSearchLotRelease.MaxLength = 50;
            this.uComboSearchLotRelease.Name = "uComboSearchLotRelease";
            this.uComboSearchLotRelease.Size = new System.Drawing.Size(123, 21);
            this.uComboSearchLotRelease.TabIndex = 1;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(717, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(10, 36);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(103, 20);
            this.uLabelSearchCustomer.TabIndex = 0;
            this.uLabelSearchCustomer.Text = "고객";
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(459, 20);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(14, 8);
            this.uLabel2.TabIndex = 0;
            this.uLabel2.Text = "~";
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(285, 36);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(86, 20);
            this.uLabelPackage.TabIndex = 0;
            this.uLabelPackage.Text = "Package";
            // 
            // uLabelAriseDate
            // 
            this.uLabelAriseDate.Location = new System.Drawing.Point(285, 12);
            this.uLabelAriseDate.Name = "uLabelAriseDate";
            this.uLabelAriseDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelAriseDate.TabIndex = 0;
            this.uLabelAriseDate.Text = "등록일";
            // 
            // uLabelLotRelease
            // 
            this.uLabelLotRelease.Location = new System.Drawing.Point(10, 12);
            this.uLabelLotRelease.Name = "uLabelLotRelease";
            this.uLabelLotRelease.Size = new System.Drawing.Size(103, 20);
            this.uLabelLotRelease.TabIndex = 0;
            this.uLabelLotRelease.Text = "LotRelease여부";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(917, 710);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 140);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(917, 710);
            this.uGroupBoxContentsArea.TabIndex = 5;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGridEmail);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonOK);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboDept);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDel_Email);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEmail);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxStep5);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxInfo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabStep);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxLoss);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(911, 690);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGridEmail
            // 
            this.uGridEmail.Anchor = System.Windows.Forms.AnchorStyles.Left;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEmail.DisplayLayout.Appearance = appearance17;
            this.uGridEmail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEmail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEmail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.uGridEmail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEmail.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGridEmail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEmail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEmail.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEmail.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.uGridEmail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEmail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEmail.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGridEmail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEmail.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEmail.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.uGridEmail.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGridEmail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEmail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGridEmail.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGridEmail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEmail.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGridEmail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEmail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEmail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEmail.Location = new System.Drawing.Point(400, 516);
            this.uGridEmail.Name = "uGridEmail";
            this.uGridEmail.Size = new System.Drawing.Size(506, 56);
            this.uGridEmail.TabIndex = 301;
            this.uGridEmail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_AfterCellUpdate);
            this.uGridEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridEmail_KeyDown);
            this.uGridEmail.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEmail_ClickCellButton);
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(281, 523);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(87, 28);
            this.uButtonOK.TabIndex = 300;
            this.uButtonOK.Text = "확인";
            // 
            // uComboDept
            // 
            this.uComboDept.Location = new System.Drawing.Point(182, 527);
            this.uComboDept.Name = "uComboDept";
            this.uComboDept.Size = new System.Drawing.Size(96, 21);
            this.uComboDept.TabIndex = 299;
            // 
            // uButtonDel_Email
            // 
            this.uButtonDel_Email.Location = new System.Drawing.Point(99, 523);
            this.uButtonDel_Email.Name = "uButtonDel_Email";
            this.uButtonDel_Email.Size = new System.Drawing.Size(75, 28);
            this.uButtonDel_Email.TabIndex = 297;
            this.uButtonDel_Email.Text = "ultraButton1";
            this.uButtonDel_Email.Click += new System.EventHandler(this.uButtonDel_Email_Click);
            // 
            // uLabelEmail
            // 
            this.uLabelEmail.Location = new System.Drawing.Point(10, 527);
            this.uLabelEmail.Name = "uLabelEmail";
            this.uLabelEmail.Size = new System.Drawing.Size(86, 20);
            this.uLabelEmail.TabIndex = 298;
            this.uLabelEmail.Text = "이메일리스트";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uComboS0Dept);
            this.uGroupBox.Controls.Add(this.uLabelS0Dept);
            this.uGroupBox.Controls.Add(this.uButtonAbnormal);
            this.uGroupBox.Controls.Add(this.uGroupBox6);
            this.uGroupBox.Controls.Add(this.uTextRegistUserID);
            this.uGroupBox.Controls.Add(this.uTextRegistUserName);
            this.uGroupBox.Controls.Add(this.uOptionAbnormal);
            this.uGroupBox.Controls.Add(this.uLabelAbnormal);
            this.uGroupBox.Controls.Add(this.uLabelRegistUser);
            this.uGroupBox.Controls.Add(this.uTextAttachFile);
            this.uGroupBox.Controls.Add(this.uLabelAttachFile);
            this.uGroupBox.Controls.Add(this.uTextMeasureDesc);
            this.uGroupBox.Controls.Add(this.ulabelMeasureDesc);
            this.uGroupBox.Controls.Add(this.uTextAriseCause);
            this.uGroupBox.Controls.Add(this.ulabelAriseCause);
            this.uGroupBox.Location = new System.Drawing.Point(0, 160);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(911, 132);
            this.uGroupBox.TabIndex = 296;
            // 
            // uComboS0Dept
            // 
            this.uComboS0Dept.Location = new System.Drawing.Point(576, 76);
            this.uComboS0Dept.MaxLength = 50;
            this.uComboS0Dept.Name = "uComboS0Dept";
            this.uComboS0Dept.Size = new System.Drawing.Size(96, 21);
            this.uComboS0Dept.TabIndex = 313;
            // 
            // uLabelS0Dept
            // 
            this.uLabelS0Dept.Location = new System.Drawing.Point(463, 76);
            this.uLabelS0Dept.Name = "uLabelS0Dept";
            this.uLabelS0Dept.Size = new System.Drawing.Size(110, 20);
            this.uLabelS0Dept.TabIndex = 312;
            this.uLabelS0Dept.Text = "Step1. 진행부서";
            // 
            // uButtonAbnormal
            // 
            this.uButtonAbnormal.Location = new System.Drawing.Point(576, 98);
            this.uButtonAbnormal.Name = "uButtonAbnormal";
            this.uButtonAbnormal.Size = new System.Drawing.Size(96, 24);
            this.uButtonAbnormal.TabIndex = 22;
            this.uButtonAbnormal.Text = "Step OK";
            // 
            // uGroupBox6
            // 
            this.uGroupBox6.Controls.Add(this.uCheckFir4MMaterialFlag);
            this.uGroupBox6.Controls.Add(this.uCheckFir4MEnviroFlag);
            this.uGroupBox6.Controls.Add(this.uCheckFir4MMethodFlag);
            this.uGroupBox6.Controls.Add(this.uCheckFir4MMachineFlag);
            this.uGroupBox6.Controls.Add(this.uCheckFir4MManFlag);
            this.uGroupBox6.Location = new System.Drawing.Point(686, 28);
            this.uGroupBox6.Name = "uGroupBox6";
            this.uGroupBox6.Size = new System.Drawing.Size(219, 64);
            this.uGroupBox6.TabIndex = 311;
            // 
            // uCheckFir4MMaterialFlag
            // 
            this.uCheckFir4MMaterialFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFir4MMaterialFlag.Location = new System.Drawing.Point(147, 12);
            this.uCheckFir4MMaterialFlag.Name = "uCheckFir4MMaterialFlag";
            this.uCheckFir4MMaterialFlag.Size = new System.Drawing.Size(65, 20);
            this.uCheckFir4MMaterialFlag.TabIndex = 71;
            this.uCheckFir4MMaterialFlag.Text = "자재";
            this.uCheckFir4MMaterialFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckFir4MEnviroFlag
            // 
            this.uCheckFir4MEnviroFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFir4MEnviroFlag.Location = new System.Drawing.Point(79, 36);
            this.uCheckFir4MEnviroFlag.Name = "uCheckFir4MEnviroFlag";
            this.uCheckFir4MEnviroFlag.Size = new System.Drawing.Size(65, 20);
            this.uCheckFir4MEnviroFlag.TabIndex = 70;
            this.uCheckFir4MEnviroFlag.Text = "환경";
            this.uCheckFir4MEnviroFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckFir4MMethodFlag
            // 
            this.uCheckFir4MMethodFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFir4MMethodFlag.Location = new System.Drawing.Point(10, 36);
            this.uCheckFir4MMethodFlag.Name = "uCheckFir4MMethodFlag";
            this.uCheckFir4MMethodFlag.Size = new System.Drawing.Size(65, 20);
            this.uCheckFir4MMethodFlag.TabIndex = 69;
            this.uCheckFir4MMethodFlag.Text = "방법";
            this.uCheckFir4MMethodFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckFir4MMachineFlag
            // 
            this.uCheckFir4MMachineFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFir4MMachineFlag.Location = new System.Drawing.Point(79, 12);
            this.uCheckFir4MMachineFlag.Name = "uCheckFir4MMachineFlag";
            this.uCheckFir4MMachineFlag.Size = new System.Drawing.Size(65, 20);
            this.uCheckFir4MMachineFlag.TabIndex = 68;
            this.uCheckFir4MMachineFlag.Text = "장비";
            this.uCheckFir4MMachineFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckFir4MManFlag
            // 
            this.uCheckFir4MManFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckFir4MManFlag.Location = new System.Drawing.Point(10, 12);
            this.uCheckFir4MManFlag.Name = "uCheckFir4MManFlag";
            this.uCheckFir4MManFlag.Size = new System.Drawing.Size(65, 20);
            this.uCheckFir4MManFlag.TabIndex = 67;
            this.uCheckFir4MManFlag.Text = "사람";
            this.uCheckFir4MManFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uTextRegistUserID
            // 
            appearance125.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRegistUserID.Appearance = appearance125;
            this.uTextRegistUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance126.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance126.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance126;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Key = "Search";
            this.uTextRegistUserID.ButtonsRight.Add(editorButton5);
            this.uTextRegistUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextRegistUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRegistUserID.Location = new System.Drawing.Point(99, 100);
            this.uTextRegistUserID.MaxLength = 20;
            this.uTextRegistUserID.Name = "uTextRegistUserID";
            this.uTextRegistUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextRegistUserID.TabIndex = 310;
            // 
            // uTextRegistUserName
            // 
            appearance127.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRegistUserName.Appearance = appearance127;
            this.uTextRegistUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRegistUserName.Location = new System.Drawing.Point(187, 100);
            this.uTextRegistUserName.Name = "uTextRegistUserName";
            this.uTextRegistUserName.ReadOnly = true;
            this.uTextRegistUserName.Size = new System.Drawing.Size(86, 21);
            this.uTextRegistUserName.TabIndex = 309;
            // 
            // uOptionAbnormal
            // 
            this.uOptionAbnormal.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionAbnormal.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem1.DataValue = "T";
            valueListItem1.DisplayText = "Yes";
            valueListItem2.DataValue = "F";
            valueListItem2.DisplayText = "No";
            this.uOptionAbnormal.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this.uOptionAbnormal.Location = new System.Drawing.Point(689, 100);
            this.uOptionAbnormal.Name = "uOptionAbnormal";
            this.uOptionAbnormal.Size = new System.Drawing.Size(89, 16);
            this.uOptionAbnormal.TabIndex = 7;
            this.uOptionAbnormal.TextIndentation = 2;
            this.uOptionAbnormal.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uOptionAbnormal.Visible = false;
            // 
            // uLabelAbnormal
            // 
            this.uLabelAbnormal.Location = new System.Drawing.Point(463, 100);
            this.uLabelAbnormal.Name = "uLabelAbnormal";
            this.uLabelAbnormal.Size = new System.Drawing.Size(110, 20);
            this.uLabelAbnormal.TabIndex = 308;
            this.uLabelAbnormal.Text = "Step진행여부";
            // 
            // uLabelRegistUser
            // 
            this.uLabelRegistUser.Location = new System.Drawing.Point(10, 100);
            this.uLabelRegistUser.Name = "uLabelRegistUser";
            this.uLabelRegistUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelRegistUser.TabIndex = 308;
            this.uLabelRegistUser.Text = "등록자";
            // 
            // uTextAttachFile
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAttachFile.Appearance = appearance59;
            this.uTextAttachFile.BackColor = System.Drawing.Color.Gainsboro;
            appearance31.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance31.TextHAlignAsString = "Center";
            editorButton6.Appearance = appearance31;
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Key = "UP";
            appearance36.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton7.Appearance = appearance36;
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton7.Key = "DOWN";
            this.uTextAttachFile.ButtonsRight.Add(editorButton6);
            this.uTextAttachFile.ButtonsRight.Add(editorButton7);
            this.uTextAttachFile.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAttachFile.Location = new System.Drawing.Point(99, 77);
            this.uTextAttachFile.Name = "uTextAttachFile";
            this.uTextAttachFile.ReadOnly = true;
            this.uTextAttachFile.Size = new System.Drawing.Size(343, 21);
            this.uTextAttachFile.TabIndex = 307;
            // 
            // uLabelAttachFile
            // 
            this.uLabelAttachFile.Location = new System.Drawing.Point(10, 76);
            this.uLabelAttachFile.Name = "uLabelAttachFile";
            this.uLabelAttachFile.Size = new System.Drawing.Size(86, 20);
            this.uLabelAttachFile.TabIndex = 306;
            this.uLabelAttachFile.Text = "첨부파일";
            // 
            // uTextMeasureDesc
            // 
            this.uTextMeasureDesc.Location = new System.Drawing.Point(99, 53);
            this.uTextMeasureDesc.MaxLength = 2000;
            this.uTextMeasureDesc.Name = "uTextMeasureDesc";
            this.uTextMeasureDesc.Size = new System.Drawing.Size(573, 21);
            this.uTextMeasureDesc.TabIndex = 305;
            // 
            // ulabelMeasureDesc
            // 
            this.ulabelMeasureDesc.Location = new System.Drawing.Point(10, 52);
            this.ulabelMeasureDesc.Name = "ulabelMeasureDesc";
            this.ulabelMeasureDesc.Size = new System.Drawing.Size(86, 20);
            this.ulabelMeasureDesc.TabIndex = 304;
            this.ulabelMeasureDesc.Text = "대책사항";
            // 
            // uTextAriseCause
            // 
            this.uTextAriseCause.Location = new System.Drawing.Point(99, 28);
            this.uTextAriseCause.MaxLength = 2000;
            this.uTextAriseCause.Name = "uTextAriseCause";
            this.uTextAriseCause.Size = new System.Drawing.Size(573, 21);
            this.uTextAriseCause.TabIndex = 303;
            // 
            // ulabelAriseCause
            // 
            this.ulabelAriseCause.Location = new System.Drawing.Point(10, 28);
            this.ulabelAriseCause.Name = "ulabelAriseCause";
            this.ulabelAriseCause.Size = new System.Drawing.Size(86, 20);
            this.ulabelAriseCause.TabIndex = 302;
            this.ulabelAriseCause.Text = "발생원인";
            // 
            // uGroupBoxStep5
            // 
            this.uGroupBoxStep5.Controls.Add(this.uButtonReturn);
            this.uGroupBoxStep5.Controls.Add(this.uTextS5Comment);
            this.uGroupBoxStep5.Controls.Add(this.uCheckReturn);
            this.uGroupBoxStep5.Controls.Add(this.uOptionOkNg);
            this.uGroupBoxStep5.Controls.Add(this.uComboReturnStep);
            this.uGroupBoxStep5.Controls.Add(this.uCheckS5);
            this.uGroupBoxStep5.Controls.Add(this.uLabelS5ReturnDept);
            this.uGroupBoxStep5.Controls.Add(this.uTextS5UserName);
            this.uGroupBoxStep5.Controls.Add(this.uTextS5UserID);
            this.uGroupBoxStep5.Controls.Add(this.uDateS5Date);
            this.uGroupBoxStep5.Controls.Add(this.uLabelS5Comment);
            this.uGroupBoxStep5.Controls.Add(this.uLabelS5Complete);
            this.uGroupBoxStep5.Controls.Add(this.uLabelOkNg);
            this.uGroupBoxStep5.Controls.Add(this.uLabelS5User);
            this.uGroupBoxStep5.Controls.Add(this.uLabelS5Date);
            this.uGroupBoxStep5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxStep5.Location = new System.Drawing.Point(0, 574);
            this.uGroupBoxStep5.Name = "uGroupBoxStep5";
            this.uGroupBoxStep5.Size = new System.Drawing.Size(911, 116);
            this.uGroupBoxStep5.TabIndex = 295;
            // 
            // uButtonReturn
            // 
            this.uButtonReturn.Location = new System.Drawing.Point(559, 76);
            this.uButtonReturn.Name = "uButtonReturn";
            this.uButtonReturn.Size = new System.Drawing.Size(69, 24);
            this.uButtonReturn.TabIndex = 22;
            this.uButtonReturn.Text = "반려";
            // 
            // uTextS5Comment
            // 
            this.uTextS5Comment.Location = new System.Drawing.Point(470, 28);
            this.uTextS5Comment.MaxLength = 2000;
            this.uTextS5Comment.Multiline = true;
            this.uTextS5Comment.Name = "uTextS5Comment";
            this.uTextS5Comment.Size = new System.Drawing.Size(432, 44);
            this.uTextS5Comment.TabIndex = 8;
            // 
            // uCheckReturn
            // 
            this.uCheckReturn.Location = new System.Drawing.Point(543, 100);
            this.uCheckReturn.Name = "uCheckReturn";
            this.uCheckReturn.Size = new System.Drawing.Size(12, 16);
            this.uCheckReturn.TabIndex = 22;
            this.uCheckReturn.Visible = false;
            // 
            // uOptionOkNg
            // 
            this.uOptionOkNg.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionOkNg.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem3.DataValue = "OK";
            valueListItem3.DisplayText = "합격";
            valueListItem4.DataValue = "NG";
            valueListItem4.DisplayText = "불합격";
            this.uOptionOkNg.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem3,
            valueListItem4});
            this.uOptionOkNg.Location = new System.Drawing.Point(137, 80);
            this.uOptionOkNg.Name = "uOptionOkNg";
            this.uOptionOkNg.Size = new System.Drawing.Size(117, 16);
            this.uOptionOkNg.TabIndex = 7;
            this.uOptionOkNg.TextIndentation = 2;
            this.uOptionOkNg.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uComboReturnStep
            // 
            this.uComboReturnStep.Location = new System.Drawing.Point(470, 77);
            this.uComboReturnStep.MaxLength = 50;
            this.uComboReturnStep.Name = "uComboReturnStep";
            this.uComboReturnStep.Size = new System.Drawing.Size(86, 21);
            this.uComboReturnStep.TabIndex = 21;
            // 
            // uCheckS5
            // 
            this.uCheckS5.Location = new System.Drawing.Point(137, 100);
            this.uCheckS5.Name = "uCheckS5";
            this.uCheckS5.Size = new System.Drawing.Size(14, 16);
            this.uCheckS5.TabIndex = 6;
            // 
            // uLabelS5ReturnDept
            // 
            this.uLabelS5ReturnDept.Location = new System.Drawing.Point(346, 77);
            this.uLabelS5ReturnDept.Name = "uLabelS5ReturnDept";
            this.uLabelS5ReturnDept.Size = new System.Drawing.Size(120, 20);
            this.uLabelS5ReturnDept.TabIndex = 18;
            this.uLabelS5ReturnDept.Text = "반려Step";
            // 
            // uTextS5UserName
            // 
            appearance82.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS5UserName.Appearance = appearance82;
            this.uTextS5UserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextS5UserName.Location = new System.Drawing.Point(226, 52);
            this.uTextS5UserName.Name = "uTextS5UserName";
            this.uTextS5UserName.ReadOnly = true;
            this.uTextS5UserName.Size = new System.Drawing.Size(86, 21);
            this.uTextS5UserName.TabIndex = 5;
            // 
            // uTextS5UserID
            // 
            appearance83.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextS5UserID.Appearance = appearance83;
            this.uTextS5UserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance84.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance84.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton8.Appearance = appearance84;
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextS5UserID.ButtonsRight.Add(editorButton8);
            this.uTextS5UserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextS5UserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextS5UserID.Location = new System.Drawing.Point(137, 52);
            this.uTextS5UserID.MaxLength = 20;
            this.uTextS5UserID.Name = "uTextS5UserID";
            this.uTextS5UserID.Size = new System.Drawing.Size(86, 21);
            this.uTextS5UserID.TabIndex = 5;
            // 
            // uDateS5Date
            // 
            this.uDateS5Date.Location = new System.Drawing.Point(137, 28);
            this.uDateS5Date.Name = "uDateS5Date";
            this.uDateS5Date.Size = new System.Drawing.Size(86, 21);
            this.uDateS5Date.TabIndex = 4;
            // 
            // uLabelS5Comment
            // 
            this.uLabelS5Comment.Location = new System.Drawing.Point(346, 28);
            this.uLabelS5Comment.Name = "uLabelS5Comment";
            this.uLabelS5Comment.Size = new System.Drawing.Size(120, 20);
            this.uLabelS5Comment.TabIndex = 3;
            this.uLabelS5Comment.Text = "Approval Comment";
            // 
            // uLabelS5Complete
            // 
            this.uLabelS5Complete.Location = new System.Drawing.Point(10, 100);
            this.uLabelS5Complete.Name = "uLabelS5Complete";
            this.uLabelS5Complete.Size = new System.Drawing.Size(123, 20);
            this.uLabelS5Complete.TabIndex = 3;
            this.uLabelS5Complete.Text = "최종완료";
            // 
            // uLabelOkNg
            // 
            this.uLabelOkNg.Location = new System.Drawing.Point(10, 76);
            this.uLabelOkNg.Name = "uLabelOkNg";
            this.uLabelOkNg.Size = new System.Drawing.Size(123, 20);
            this.uLabelOkNg.TabIndex = 3;
            this.uLabelOkNg.Text = "판정결과";
            // 
            // uLabelS5User
            // 
            this.uLabelS5User.Location = new System.Drawing.Point(10, 52);
            this.uLabelS5User.Name = "uLabelS5User";
            this.uLabelS5User.Size = new System.Drawing.Size(123, 20);
            this.uLabelS5User.TabIndex = 3;
            this.uLabelS5User.Text = "Responsible person";
            // 
            // uLabelS5Date
            // 
            this.uLabelS5Date.Location = new System.Drawing.Point(10, 28);
            this.uLabelS5Date.Name = "uLabelS5Date";
            this.uLabelS5Date.Size = new System.Drawing.Size(123, 20);
            this.uLabelS5Date.TabIndex = 3;
            this.uLabelS5Date.Text = "Date";
            // 
            // uGroupBoxInfo
            // 
            this.uGroupBoxInfo.Controls.Add(this.uTextManageNo);
            this.uGroupBoxInfo.Controls.Add(this.uLabelManageNo);
            this.uGroupBoxInfo.Controls.Add(this.uTextCustomerProductCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelCustomerProductCode);
            this.uGroupBoxInfo.Controls.Add(this.uTextEtcDesc);
            this.uGroupBoxInfo.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBoxInfo.Controls.Add(this.uTextProcessHoldState);
            this.uGroupBoxInfo.Controls.Add(this.uLabelProcessHoldState);
            this.uGroupBoxInfo.Controls.Add(this.uTextLotProcessState);
            this.uGroupBoxInfo.Controls.Add(this.uLabelLotProcessState);
            this.uGroupBoxInfo.Controls.Add(this.uTextAcceptTime);
            this.uGroupBoxInfo.Controls.Add(this.uTextAcceptDate);
            this.uGroupBoxInfo.Controls.Add(this.uButtonAccept);
            this.uGroupBoxInfo.Controls.Add(this.uLabelAcceptDate);
            this.uGroupBoxInfo.Controls.Add(this.uTextPackage);
            this.uGroupBoxInfo.Controls.Add(this.uCheckMESReleaseTFlag);
            this.uGroupBoxInfo.Controls.Add(this.uCheckMESFlag);
            this.uGroupBoxInfo.Controls.Add(this.uLabelPack);
            this.uGroupBoxInfo.Controls.Add(this.uTextAriseProcessName);
            this.uGroupBoxInfo.Controls.Add(this.uTextCustomerProductSpec);
            this.uGroupBoxInfo.Controls.Add(this.uTextPlantCode);
            this.uGroupBoxInfo.Controls.Add(this.uTextBinName);
            this.uGroupBoxInfo.Controls.Add(this.uLabelBinName);
            this.uGroupBoxInfo.Controls.Add(this.uTextEquipName);
            this.uGroupBoxInfo.Controls.Add(this.uTextEquipCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelEquipCode);
            this.uGroupBoxInfo.Controls.Add(this.uTextLotNo);
            this.uGroupBoxInfo.Controls.Add(this.uLabelLotNo);
            this.uGroupBoxInfo.Controls.Add(this.uTextWorkUserName);
            this.uGroupBoxInfo.Controls.Add(this.uTextWorkUserID);
            this.uGroupBoxInfo.Controls.Add(this.uLabelWorkUser);
            this.uGroupBoxInfo.Controls.Add(this.uTextAriseTime);
            this.uGroupBoxInfo.Controls.Add(this.uTextPlantName);
            this.uGroupBoxInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxInfo.Controls.Add(this.uTextProcessYield);
            this.uGroupBoxInfo.Controls.Add(this.uLabelProcessYield);
            this.uGroupBoxInfo.Controls.Add(this.uTextLotQty);
            this.uGroupBoxInfo.Controls.Add(this.uLabelLotQty);
            this.uGroupBoxInfo.Controls.Add(this.uTextAriseDate);
            this.uGroupBoxInfo.Controls.Add(this.uLabelOccurDate);
            this.uGroupBoxInfo.Controls.Add(this.uTextAriseProcessCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelAriseProcess);
            this.uGroupBoxInfo.Controls.Add(this.uLabelProduct);
            this.uGroupBoxInfo.Controls.Add(this.uTextCustomerName);
            this.uGroupBoxInfo.Controls.Add(this.uTextCustomerCode);
            this.uGroupBoxInfo.Controls.Add(this.uLabelCustomer);
            this.uGroupBoxInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxInfo.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxInfo.Name = "uGroupBoxInfo";
            this.uGroupBoxInfo.Size = new System.Drawing.Size(911, 156);
            this.uGroupBoxInfo.TabIndex = 294;
            // 
            // uTextManageNo
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextManageNo.Appearance = appearance42;
            this.uTextManageNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextManageNo.Location = new System.Drawing.Point(96, 8);
            this.uTextManageNo.Name = "uTextManageNo";
            this.uTextManageNo.ReadOnly = true;
            this.uTextManageNo.Size = new System.Drawing.Size(206, 21);
            this.uTextManageNo.TabIndex = 339;
            // 
            // uLabelManageNo
            // 
            this.uLabelManageNo.Location = new System.Drawing.Point(7, 8);
            this.uLabelManageNo.Name = "uLabelManageNo";
            this.uLabelManageNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelManageNo.TabIndex = 338;
            this.uLabelManageNo.Text = "관리번호";
            // 
            // uTextCustomerProductCode
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductCode.Appearance = appearance33;
            this.uTextCustomerProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductCode.Location = new System.Drawing.Point(96, 80);
            this.uTextCustomerProductCode.Name = "uTextCustomerProductCode";
            this.uTextCustomerProductCode.ReadOnly = true;
            this.uTextCustomerProductCode.Size = new System.Drawing.Size(206, 21);
            this.uTextCustomerProductCode.TabIndex = 337;
            // 
            // uLabelCustomerProductCode
            // 
            this.uLabelCustomerProductCode.Location = new System.Drawing.Point(7, 80);
            this.uLabelCustomerProductCode.Name = "uLabelCustomerProductCode";
            this.uLabelCustomerProductCode.Size = new System.Drawing.Size(86, 20);
            this.uLabelCustomerProductCode.TabIndex = 336;
            this.uLabelCustomerProductCode.Text = "고객제품코드";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(96, 128);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(473, 21);
            this.uTextEtcDesc.TabIndex = 335;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(7, 128);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(86, 20);
            this.uLabelEtcDesc.TabIndex = 334;
            this.uLabelEtcDesc.Text = "비고";
            // 
            // uTextProcessHoldState
            // 
            appearance128.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessHoldState.Appearance = appearance128;
            this.uTextProcessHoldState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessHoldState.Location = new System.Drawing.Point(675, 56);
            this.uTextProcessHoldState.Name = "uTextProcessHoldState";
            this.uTextProcessHoldState.ReadOnly = true;
            this.uTextProcessHoldState.Size = new System.Drawing.Size(130, 21);
            this.uTextProcessHoldState.TabIndex = 333;
            // 
            // uLabelProcessHoldState
            // 
            this.uLabelProcessHoldState.Location = new System.Drawing.Point(586, 56);
            this.uLabelProcessHoldState.Name = "uLabelProcessHoldState";
            this.uLabelProcessHoldState.Size = new System.Drawing.Size(86, 20);
            this.uLabelProcessHoldState.TabIndex = 332;
            this.uLabelProcessHoldState.Text = "Hold 상태";
            // 
            // uTextLotProcessState
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Appearance = appearance47;
            this.uTextLotProcessState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Location = new System.Drawing.Point(675, 32);
            this.uTextLotProcessState.Name = "uTextLotProcessState";
            this.uTextLotProcessState.ReadOnly = true;
            this.uTextLotProcessState.Size = new System.Drawing.Size(130, 21);
            this.uTextLotProcessState.TabIndex = 331;
            // 
            // uLabelLotProcessState
            // 
            this.uLabelLotProcessState.Location = new System.Drawing.Point(586, 32);
            this.uLabelLotProcessState.Name = "uLabelLotProcessState";
            this.uLabelLotProcessState.Size = new System.Drawing.Size(86, 20);
            this.uLabelLotProcessState.TabIndex = 330;
            this.uLabelLotProcessState.Text = "Lot 상태";
            // 
            // uTextAcceptTime
            // 
            appearance129.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptTime.Appearance = appearance129;
            this.uTextAcceptTime.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptTime.Location = new System.Drawing.Point(764, 104);
            this.uTextAcceptTime.Name = "uTextAcceptTime";
            this.uTextAcceptTime.ReadOnly = true;
            this.uTextAcceptTime.Size = new System.Drawing.Size(81, 21);
            this.uTextAcceptTime.TabIndex = 329;
            // 
            // uTextAcceptDate
            // 
            appearance130.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptDate.Appearance = appearance130;
            this.uTextAcceptDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptDate.Location = new System.Drawing.Point(675, 104);
            this.uTextAcceptDate.Name = "uTextAcceptDate";
            this.uTextAcceptDate.ReadOnly = true;
            this.uTextAcceptDate.Size = new System.Drawing.Size(86, 21);
            this.uTextAcceptDate.TabIndex = 328;
            // 
            // uButtonAccept
            // 
            this.uButtonAccept.Location = new System.Drawing.Point(675, 128);
            this.uButtonAccept.Name = "uButtonAccept";
            this.uButtonAccept.Size = new System.Drawing.Size(86, 24);
            this.uButtonAccept.TabIndex = 326;
            this.uButtonAccept.Text = "접수";
            this.uButtonAccept.Click += new System.EventHandler(this.uButtonAccept_Click);
            // 
            // uLabelAcceptDate
            // 
            this.uLabelAcceptDate.Location = new System.Drawing.Point(586, 104);
            this.uLabelAcceptDate.Name = "uLabelAcceptDate";
            this.uLabelAcceptDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelAcceptDate.TabIndex = 327;
            this.uLabelAcceptDate.Text = "접수일시";
            // 
            // uTextPackage
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance34;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(401, 32);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(168, 21);
            this.uTextPackage.TabIndex = 325;
            // 
            // uCheckMESReleaseTFlag
            // 
            this.uCheckMESReleaseTFlag.Location = new System.Drawing.Point(902, 76);
            this.uCheckMESReleaseTFlag.Name = "uCheckMESReleaseTFlag";
            this.uCheckMESReleaseTFlag.Size = new System.Drawing.Size(10, 16);
            this.uCheckMESReleaseTFlag.TabIndex = 22;
            this.uCheckMESReleaseTFlag.Visible = false;
            // 
            // uCheckMESFlag
            // 
            this.uCheckMESFlag.Location = new System.Drawing.Point(888, 76);
            this.uCheckMESFlag.Name = "uCheckMESFlag";
            this.uCheckMESFlag.Size = new System.Drawing.Size(10, 16);
            this.uCheckMESFlag.TabIndex = 22;
            this.uCheckMESFlag.Visible = false;
            // 
            // uLabelPack
            // 
            this.uLabelPack.Location = new System.Drawing.Point(311, 32);
            this.uLabelPack.Name = "uLabelPack";
            this.uLabelPack.Size = new System.Drawing.Size(86, 20);
            this.uLabelPack.TabIndex = 324;
            this.uLabelPack.Text = "Package";
            // 
            // uTextAriseProcessName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseProcessName.Appearance = appearance4;
            this.uTextAriseProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseProcessName.Location = new System.Drawing.Point(183, 56);
            this.uTextAriseProcessName.Name = "uTextAriseProcessName";
            this.uTextAriseProcessName.ReadOnly = true;
            this.uTextAriseProcessName.Size = new System.Drawing.Size(119, 21);
            this.uTextAriseProcessName.TabIndex = 323;
            // 
            // uTextCustomerProductSpec
            // 
            appearance120.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Appearance = appearance120;
            this.uTextCustomerProductSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Location = new System.Drawing.Point(401, 80);
            this.uTextCustomerProductSpec.Name = "uTextCustomerProductSpec";
            this.uTextCustomerProductSpec.ReadOnly = true;
            this.uTextCustomerProductSpec.Size = new System.Drawing.Size(168, 21);
            this.uTextCustomerProductSpec.TabIndex = 322;
            // 
            // uTextPlantCode
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance35;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(795, 4);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(17, 21);
            this.uTextPlantCode.TabIndex = 321;
            this.uTextPlantCode.Visible = false;
            // 
            // uTextBinName
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBinName.Appearance = appearance32;
            this.uTextBinName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextBinName.Location = new System.Drawing.Point(868, 4);
            this.uTextBinName.Name = "uTextBinName";
            this.uTextBinName.ReadOnly = true;
            this.uTextBinName.Size = new System.Drawing.Size(30, 21);
            this.uTextBinName.TabIndex = 318;
            this.uTextBinName.Visible = false;
            // 
            // uLabelBinName
            // 
            this.uLabelBinName.Location = new System.Drawing.Point(830, 4);
            this.uLabelBinName.Name = "uLabelBinName";
            this.uLabelBinName.Size = new System.Drawing.Size(34, 20);
            this.uLabelBinName.TabIndex = 317;
            this.uLabelBinName.Text = "BinName";
            this.uLabelBinName.Visible = false;
            // 
            // uTextEquipName
            // 
            appearance118.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance118;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(489, 56);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(81, 21);
            this.uTextEquipName.TabIndex = 316;
            // 
            // uTextEquipCode
            // 
            appearance131.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance131;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(401, 56);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(86, 21);
            this.uTextEquipCode.TabIndex = 315;
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(311, 56);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(86, 20);
            this.uLabelEquipCode.TabIndex = 314;
            this.uLabelEquipCode.Text = "설비";
            // 
            // uTextLotNo
            // 
            appearance55.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.Appearance = appearance55;
            this.uTextLotNo.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.Location = new System.Drawing.Point(401, 8);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(168, 21);
            this.uTextLotNo.TabIndex = 313;
            this.uTextLotNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLotNo_KeyDown);
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(311, 8);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelLotNo.TabIndex = 312;
            this.uLabelLotNo.Text = "LotNo";
            // 
            // uTextWorkUserName
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Appearance = appearance38;
            this.uTextWorkUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Location = new System.Drawing.Point(183, 104);
            this.uTextWorkUserName.Name = "uTextWorkUserName";
            this.uTextWorkUserName.ReadOnly = true;
            this.uTextWorkUserName.Size = new System.Drawing.Size(119, 21);
            this.uTextWorkUserName.TabIndex = 311;
            // 
            // uTextWorkUserID
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserID.Appearance = appearance5;
            this.uTextWorkUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserID.Location = new System.Drawing.Point(95, 104);
            this.uTextWorkUserID.Name = "uTextWorkUserID";
            this.uTextWorkUserID.ReadOnly = true;
            this.uTextWorkUserID.Size = new System.Drawing.Size(86, 21);
            this.uTextWorkUserID.TabIndex = 310;
            // 
            // uLabelWorkUser
            // 
            this.uLabelWorkUser.Location = new System.Drawing.Point(6, 104);
            this.uLabelWorkUser.Name = "uLabelWorkUser";
            this.uLabelWorkUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelWorkUser.TabIndex = 309;
            this.uLabelWorkUser.Text = "작업자";
            // 
            // uTextAriseTime
            // 
            appearance116.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseTime.Appearance = appearance116;
            this.uTextAriseTime.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseTime.Location = new System.Drawing.Point(489, 104);
            this.uTextAriseTime.Name = "uTextAriseTime";
            this.uTextAriseTime.ReadOnly = true;
            this.uTextAriseTime.Size = new System.Drawing.Size(81, 21);
            this.uTextAriseTime.TabIndex = 308;
            // 
            // uTextPlantName
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance37;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(813, 4);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(15, 21);
            this.uTextPlantName.TabIndex = 307;
            this.uTextPlantName.Visible = false;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(782, 4);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(14, 20);
            this.uLabelPlant.TabIndex = 306;
            this.uLabelPlant.Text = "공장";
            this.uLabelPlant.Visible = false;
            // 
            // uTextProcessYield
            // 
            appearance132.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessYield.Appearance = appearance132;
            this.uTextProcessYield.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessYield.Location = new System.Drawing.Point(867, 28);
            this.uTextProcessYield.Name = "uTextProcessYield";
            this.uTextProcessYield.ReadOnly = true;
            this.uTextProcessYield.Size = new System.Drawing.Size(31, 21);
            this.uTextProcessYield.TabIndex = 305;
            this.uTextProcessYield.Visible = false;
            // 
            // uLabelProcessYield
            // 
            this.uLabelProcessYield.Location = new System.Drawing.Point(830, 28);
            this.uLabelProcessYield.Name = "uLabelProcessYield";
            this.uLabelProcessYield.Size = new System.Drawing.Size(34, 20);
            this.uLabelProcessYield.TabIndex = 304;
            this.uLabelProcessYield.Text = "공정수율";
            this.uLabelProcessYield.Visible = false;
            // 
            // uTextLotQty
            // 
            appearance133.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotQty.Appearance = appearance133;
            this.uTextLotQty.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotQty.Location = new System.Drawing.Point(675, 80);
            this.uTextLotQty.Name = "uTextLotQty";
            this.uTextLotQty.ReadOnly = true;
            this.uTextLotQty.Size = new System.Drawing.Size(86, 21);
            this.uTextLotQty.TabIndex = 303;
            // 
            // uLabelLotQty
            // 
            this.uLabelLotQty.Location = new System.Drawing.Point(586, 80);
            this.uLabelLotQty.Name = "uLabelLotQty";
            this.uLabelLotQty.Size = new System.Drawing.Size(86, 20);
            this.uLabelLotQty.TabIndex = 302;
            this.uLabelLotQty.Text = "Lot 수량";
            // 
            // uTextAriseDate
            // 
            appearance117.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseDate.Appearance = appearance117;
            this.uTextAriseDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseDate.Location = new System.Drawing.Point(400, 104);
            this.uTextAriseDate.Name = "uTextAriseDate";
            this.uTextAriseDate.ReadOnly = true;
            this.uTextAriseDate.Size = new System.Drawing.Size(86, 21);
            this.uTextAriseDate.TabIndex = 301;
            // 
            // uLabelOccurDate
            // 
            this.uLabelOccurDate.Location = new System.Drawing.Point(311, 104);
            this.uLabelOccurDate.Name = "uLabelOccurDate";
            this.uLabelOccurDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelOccurDate.TabIndex = 300;
            this.uLabelOccurDate.Text = "등록일";
            // 
            // uTextAriseProcessCode
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseProcessCode.Appearance = appearance54;
            this.uTextAriseProcessCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAriseProcessCode.Location = new System.Drawing.Point(95, 56);
            this.uTextAriseProcessCode.Name = "uTextAriseProcessCode";
            this.uTextAriseProcessCode.ReadOnly = true;
            this.uTextAriseProcessCode.Size = new System.Drawing.Size(86, 21);
            this.uTextAriseProcessCode.TabIndex = 299;
            // 
            // uLabelAriseProcess
            // 
            this.uLabelAriseProcess.Location = new System.Drawing.Point(6, 56);
            this.uLabelAriseProcess.Name = "uLabelAriseProcess";
            this.uLabelAriseProcess.Size = new System.Drawing.Size(86, 20);
            this.uLabelAriseProcess.TabIndex = 297;
            this.uLabelAriseProcess.Text = "발생공정";
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(312, 80);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(86, 20);
            this.uLabelProduct.TabIndex = 294;
            this.uLabelProduct.Text = "제품코드";
            // 
            // uTextCustomerName
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance41;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(183, 32);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(119, 21);
            this.uTextCustomerName.TabIndex = 293;
            // 
            // uTextCustomerCode
            // 
            appearance121.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance121;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(95, 32);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(86, 21);
            this.uTextCustomerCode.TabIndex = 292;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(6, 32);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelCustomer.TabIndex = 291;
            this.uLabelCustomer.Text = "고객사";
            // 
            // uTabStep
            // 
            this.uTabStep.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabStep.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabStep.Controls.Add(this.ultraTabPageControl1);
            this.uTabStep.Controls.Add(this.ultraTabPageControl2);
            this.uTabStep.Controls.Add(this.ultraTabPageControl3);
            this.uTabStep.Controls.Add(this.ultraTabPageControl4);
            this.uTabStep.Location = new System.Drawing.Point(0, 292);
            this.uTabStep.Name = "uTabStep";
            this.uTabStep.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabStep.Size = new System.Drawing.Size(908, 223);
            this.uTabStep.TabIndex = 1;
            ultraTab1.Key = "S1";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Step1. ABN Lot Test Analysis";
            ultraTab2.Key = "S2";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Step2. Cause Analysis";
            ultraTab3.Key = "S3";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "Step3. Corrective Action";
            ultraTab4.Key = "S4";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "Step4. Lot Disposition";
            this.uTabStep.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(904, 197);
            // 
            // uGroupBoxLoss
            // 
            this.uGroupBoxLoss.Controls.Add(this.uGridProcLowD);
            this.uGroupBoxLoss.Location = new System.Drawing.Point(885, 160);
            this.uGroupBoxLoss.Name = "uGroupBoxLoss";
            this.uGroupBoxLoss.Size = new System.Drawing.Size(21, 20);
            this.uGroupBoxLoss.TabIndex = 0;
            // 
            // uGridProcLowD
            // 
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProcLowD.DisplayLayout.Appearance = appearance45;
            this.uGridProcLowD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProcLowD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcLowD.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcLowD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance43;
            this.uGridProcLowD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance44.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance44.BackColor2 = System.Drawing.SystemColors.Control;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProcLowD.DisplayLayout.GroupByBox.PromptAppearance = appearance44;
            this.uGridProcLowD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProcLowD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProcLowD.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance48.BackColor = System.Drawing.SystemColors.Highlight;
            appearance48.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProcLowD.DisplayLayout.Override.ActiveRowAppearance = appearance48;
            this.uGridProcLowD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProcLowD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance119.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProcLowD.DisplayLayout.Override.CardAreaAppearance = appearance119;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            appearance46.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProcLowD.DisplayLayout.Override.CellAppearance = appearance46;
            this.uGridProcLowD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProcLowD.DisplayLayout.Override.CellPadding = 0;
            appearance50.BackColor = System.Drawing.SystemColors.Control;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProcLowD.DisplayLayout.Override.GroupByRowAppearance = appearance50;
            appearance52.TextHAlignAsString = "Left";
            this.uGridProcLowD.DisplayLayout.Override.HeaderAppearance = appearance52;
            this.uGridProcLowD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProcLowD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.BorderColor = System.Drawing.Color.Silver;
            this.uGridProcLowD.DisplayLayout.Override.RowAppearance = appearance51;
            this.uGridProcLowD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance49.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProcLowD.DisplayLayout.Override.TemplateAddRowAppearance = appearance49;
            this.uGridProcLowD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProcLowD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProcLowD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProcLowD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridProcLowD.Location = new System.Drawing.Point(3, 0);
            this.uGridProcLowD.Name = "uGridProcLowD";
            this.uGridProcLowD.Size = new System.Drawing.Size(15, 17);
            this.uGridProcLowD.TabIndex = 293;
            this.uGridProcLowD.Text = "ultraGrid1";
            // 
            // uGroupBoxGrid
            // 
            this.uGroupBoxGrid.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxGrid.Controls.Add(this.uGridAbormal);
            this.uGroupBoxGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBoxGrid.Location = new System.Drawing.Point(0, 100);
            this.uGroupBoxGrid.Name = "uGroupBoxGrid";
            this.uGroupBoxGrid.Size = new System.Drawing.Size(917, 40);
            this.uGroupBoxGrid.TabIndex = 6;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 2;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "Test Abnormal Yield 정보";
            // 
            // frmINSZ0016
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxGrid);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0016";
            this.Load += new System.EventHandler(this.frmINSZ0016_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0016_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0016_FormClosing);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS1Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS1File)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS1Comment)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2Analysis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS2File)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS2Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS2UserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS2Date)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3Comment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS3File)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS3Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS3Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS3UserID)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.ultraTabPageControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4Comment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridS4File)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS4Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS4UserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAbormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLotRelease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboS0Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox6)).EndInit();
            this.uGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MMaterialFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MEnviroFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MMethodFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MMachineFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckFir4MManFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegistUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionAbnormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAttachFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMeasureDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStep5)).EndInit();
            this.uGroupBoxStep5.ResumeLayout(false);
            this.uGroupBoxStep5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS5Comment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionOkNg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboReturnStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckS5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS5UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS5UserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateS5Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInfo)).EndInit();
            this.uGroupBoxInfo.ResumeLayout(false);
            this.uGroupBoxInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextManageNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessHoldState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMESReleaseTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMESFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBinName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessYield)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAriseProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabStep)).EndInit();
            this.uTabStep.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLoss)).EndInit();
            this.uGroupBoxLoss.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridProcLowD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxGrid)).EndInit();
            this.uGroupBoxGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAbormal;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelLotRelease;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLotRelease;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabStep;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxLoss;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPack;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBinName;
        private Infragistics.Win.Misc.UltraLabel uLabelBinName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAriseTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessYield;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessYield;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotQty;
        private Infragistics.Win.Misc.UltraLabel uLabelLotQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAriseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelOccurDate;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProcLowD;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxStep5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS5UserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS5UserID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS5Date;
        private Infragistics.Win.Misc.UltraLabel uLabelS5Comment;
        private Infragistics.Win.Misc.UltraLabel uLabelS5User;
        private Infragistics.Win.Misc.UltraLabel uLabelS5Date;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionOkNg;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS5;
        private Infragistics.Win.Misc.UltraLabel uLabelS5Complete;
        private Infragistics.Win.Misc.UltraLabel uLabelOkNg;
        private Infragistics.Win.Misc.UltraLabel uLabelS4Date;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS4Date;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS4UserName;
        private Infragistics.Win.Misc.UltraLabel uLabelS4User;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS4UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS4Comment;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboS1Dept;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridS1File;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS1UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS1User;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS1UserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS1Comment;
        private Infragistics.Win.Misc.UltraLabel uLabelS1Dept;
        private Infragistics.Win.Misc.UltraLabel uLabelS1File;
        private Infragistics.Win.Misc.UltraLabel uLabelS1Complete;
        private Infragistics.Win.Misc.UltraLabel uLabelS1Result;
        private Infragistics.Win.Misc.UltraLabel uLabelS4File;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS2UserName;
        private Infragistics.Win.Misc.UltraLabel uLabelS2Date;
        private Infragistics.Win.Misc.UltraLabel uLabelS2User;
        private Infragistics.Win.Misc.UltraLabel uLabelS2File;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS2UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS2Analysis;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS2Date;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridS2File;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboS2Dept;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS2;
        private Infragistics.Win.Misc.UltraLabel uLabelS2Dept;
        private Infragistics.Win.Misc.UltraLabel uLabelS2Complete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridS3File;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboS3Dept;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS3;
        private Infragistics.Win.Misc.UltraLabel uLabelS3Dept;
        private Infragistics.Win.Misc.UltraLabel uLabelS3Complete;
        private Infragistics.Win.Misc.UltraLabel uLabelS3Date;
        private Infragistics.Win.Misc.UltraLabel uLabelS3User;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS3UserName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateS3Date;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS3UserID;
        private Infragistics.Win.Misc.UltraLabel uLabelS3File;
        private Infragistics.Win.Misc.UltraLabel uLabelS3Comment;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckS4;
        private Infragistics.Win.Misc.UltraLabel uLabelS4Complete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS2Analysis;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS3Comment;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS4Comment;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridS4File;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS5Comment;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRegistUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRegistUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelRegistUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAttachFile;
        private Infragistics.Win.Misc.UltraLabel uLabelAttachFile;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMeasureDesc;
        private Infragistics.Win.Misc.UltraLabel ulabelMeasureDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAriseCause;
        private Infragistics.Win.Misc.UltraLabel ulabelAriseCause;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox6;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFir4MMaterialFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFir4MEnviroFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFir4MMethodFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFir4MMachineFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckFir4MManFlag;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxGrid;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboReturnStep;
        private Infragistics.Win.Misc.UltraLabel uLabelS5ReturnDept;
        private Infragistics.Win.Misc.UltraButton uButtonReturn;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckReturn;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMESFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMESReleaseTFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelAbnormal;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionAbnormal;
        private Infragistics.Win.Misc.UltraButton uButtonAbnormal;
        private Infragistics.Win.Misc.UltraButton uButtonS1;
        private Infragistics.Win.Misc.UltraButton uButtonS2;
        private Infragistics.Win.Misc.UltraButton uButtonS3;
        private Infragistics.Win.Misc.UltraButton uButtonS4;
        private Infragistics.Win.Misc.UltraButton uButtonS1DelRow;
        private Infragistics.Win.Misc.UltraButton uButtonS2DelRow;
        private Infragistics.Win.Misc.UltraButton uButtonS3DelRow;
        private Infragistics.Win.Misc.UltraButton uButtonS4DelRow;
        private Infragistics.Win.Misc.UltraButton uButtonAccept;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessHoldState;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessHoldState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotProcessState;
        private Infragistics.Win.Misc.UltraLabel uLabelLotProcessState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAriseProcessName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAriseProcessCode;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProductCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboS0Dept;
        private Infragistics.Win.Misc.UltraLabel uLabelS0Dept;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextManageNo;
        private Infragistics.Win.Misc.UltraLabel uLabelManageNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEmail;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDept;
        private Infragistics.Win.Misc.UltraButton uButtonDel_Email;
        private Infragistics.Win.Misc.UltraLabel uLabelEmail;
    }
}