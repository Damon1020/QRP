﻿namespace QRPINS.UI
{
    partial class frmINSReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptViewDoc = new DataDynamics.ActiveReports.Viewer.Viewer();
            this.SuspendLayout();
            // 
            // rptViewDoc
            // 
            this.rptViewDoc.BackColor = System.Drawing.SystemColors.Control;
            this.rptViewDoc.Document = new DataDynamics.ActiveReports.Document.Document("ARNet Document");
            this.rptViewDoc.Location = new System.Drawing.Point(8, 4);
            this.rptViewDoc.Name = "rptViewDoc";
            this.rptViewDoc.ReportViewer.CurrentPage = 0;
            this.rptViewDoc.ReportViewer.MultiplePageCols = 3;
            this.rptViewDoc.ReportViewer.MultiplePageRows = 2;
            this.rptViewDoc.ReportViewer.ViewType = DataDynamics.ActiveReports.Viewer.ViewType.Normal;
            this.rptViewDoc.Size = new System.Drawing.Size(728, 564);
            this.rptViewDoc.TabIndex = 2;
            this.rptViewDoc.TableOfContents.Text = "Table Of Contents";
            this.rptViewDoc.TableOfContents.Width = 200;
            this.rptViewDoc.TabTitleLength = 35;
            this.rptViewDoc.Toolbar.Font = new System.Drawing.Font("굴림", 9F);
            // 
            // frmINSReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(744, 573);
            this.ControlBox = false;
            this.Controls.Add(this.rptViewDoc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSReport";
            this.ResumeLayout(false);

        }

        #endregion

        private DataDynamics.ActiveReports.Viewer.Viewer rptViewDoc;

    }
}