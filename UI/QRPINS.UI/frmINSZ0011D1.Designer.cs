﻿namespace QRPINS.UI
{
    partial class frmINSZ0011D1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.uGroupBox1st = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid1st = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox2nd = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2nd = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox3th = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid3th = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.uCheckComplete = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uText1stComplete = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uText2ndComplete = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uText3thComplete = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1st)).BeginInit();
            this.uGroupBox1st.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1st)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2nd)).BeginInit();
            this.uGroupBox2nd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2nd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3th)).BeginInit();
            this.uGroupBox3th.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3th)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1stComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText2ndComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText3thComplete)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBox1st
            // 
            this.uGroupBox1st.Controls.Add(this.uGrid1st);
            this.uGroupBox1st.Location = new System.Drawing.Point(5, 4);
            this.uGroupBox1st.Name = "uGroupBox1st";
            this.uGroupBox1st.Size = new System.Drawing.Size(927, 184);
            this.uGroupBox1st.TabIndex = 0;
            this.uGroupBox1st.Text = "ultraGroupBox1";
            // 
            // uGrid1st
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1st.DisplayLayout.Appearance = appearance25;
            this.uGrid1st.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1st.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1st.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1st.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGrid1st.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1st.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGrid1st.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1st.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1st.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1st.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.uGrid1st.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1st.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1st.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1st.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGrid1st.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1st.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1st.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.uGrid1st.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.uGrid1st.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1st.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1st.DisplayLayout.Override.RowAppearance = appearance35;
            this.uGrid1st.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1st.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.uGrid1st.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1st.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1st.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1st.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid1st.Location = new System.Drawing.Point(3, 16);
            this.uGrid1st.Name = "uGrid1st";
            this.uGrid1st.Size = new System.Drawing.Size(921, 165);
            this.uGrid1st.TabIndex = 0;
            this.uGrid1st.Text = "ultraGrid1";
            this.uGrid1st.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGrid1st_KeyDown);
            this.uGrid1st.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1st_ClickCellButton);
            // 
            // uGroupBox2nd
            // 
            this.uGroupBox2nd.Controls.Add(this.uGrid2nd);
            this.uGroupBox2nd.Location = new System.Drawing.Point(6, 192);
            this.uGroupBox2nd.Name = "uGroupBox2nd";
            this.uGroupBox2nd.Size = new System.Drawing.Size(930, 184);
            this.uGroupBox2nd.TabIndex = 1;
            this.uGroupBox2nd.Text = "ultraGroupBox1";
            // 
            // uGrid2nd
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2nd.DisplayLayout.Appearance = appearance13;
            this.uGrid2nd.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2nd.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2nd.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2nd.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGrid2nd.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2nd.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGrid2nd.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2nd.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2nd.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2nd.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGrid2nd.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2nd.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2nd.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2nd.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGrid2nd.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2nd.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2nd.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGrid2nd.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGrid2nd.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2nd.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2nd.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGrid2nd.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2nd.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGrid2nd.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2nd.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2nd.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2nd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid2nd.Location = new System.Drawing.Point(3, 16);
            this.uGrid2nd.Name = "uGrid2nd";
            this.uGrid2nd.Size = new System.Drawing.Size(924, 165);
            this.uGrid2nd.TabIndex = 0;
            this.uGrid2nd.Text = "ultraGrid1";
            this.uGrid2nd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGrid2nd_KeyDown);
            this.uGrid2nd.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2nd_ClickCellButton);
            // 
            // uGroupBox3th
            // 
            this.uGroupBox3th.Controls.Add(this.uGrid3th);
            this.uGroupBox3th.Location = new System.Drawing.Point(6, 380);
            this.uGroupBox3th.Name = "uGroupBox3th";
            this.uGroupBox3th.Size = new System.Drawing.Size(930, 184);
            this.uGroupBox3th.TabIndex = 2;
            this.uGroupBox3th.Text = "ultraGroupBox1";
            // 
            // uGrid3th
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3th.DisplayLayout.Appearance = appearance1;
            this.uGrid3th.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3th.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3th.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3th.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGrid3th.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3th.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGrid3th.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3th.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3th.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3th.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGrid3th.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3th.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3th.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3th.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGrid3th.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3th.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3th.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGrid3th.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGrid3th.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3th.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3th.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGrid3th.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3th.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGrid3th.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3th.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3th.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3th.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGrid3th.Location = new System.Drawing.Point(3, 16);
            this.uGrid3th.Name = "uGrid3th";
            this.uGrid3th.Size = new System.Drawing.Size(924, 165);
            this.uGrid3th.TabIndex = 0;
            this.uGrid3th.Text = "ultraGrid1";
            this.uGrid3th.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGrid3th_KeyDown);
            this.uGrid3th.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid3th_ClickCellButton);
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(828, 572);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(100, 36);
            this.ultraButton1.TabIndex = 3;
            this.ultraButton1.Text = "저장";
            this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // uCheckComplete
            // 
            this.uCheckComplete.Location = new System.Drawing.Point(720, 572);
            this.uCheckComplete.Name = "uCheckComplete";
            this.uCheckComplete.Size = new System.Drawing.Size(100, 36);
            this.uCheckComplete.TabIndex = 8;
            this.uCheckComplete.Text = "작성 완료";
            // 
            // uText1stComplete
            // 
            this.uText1stComplete.Location = new System.Drawing.Point(260, 584);
            this.uText1stComplete.Name = "uText1stComplete";
            this.uText1stComplete.Size = new System.Drawing.Size(24, 21);
            this.uText1stComplete.TabIndex = 9;
            this.uText1stComplete.Visible = false;
            // 
            // uText2ndComplete
            // 
            this.uText2ndComplete.Location = new System.Drawing.Point(286, 584);
            this.uText2ndComplete.Name = "uText2ndComplete";
            this.uText2ndComplete.Size = new System.Drawing.Size(24, 21);
            this.uText2ndComplete.TabIndex = 10;
            this.uText2ndComplete.Visible = false;
            // 
            // uText3thComplete
            // 
            this.uText3thComplete.Location = new System.Drawing.Point(314, 584);
            this.uText3thComplete.Name = "uText3thComplete";
            this.uText3thComplete.Size = new System.Drawing.Size(24, 21);
            this.uText3thComplete.TabIndex = 11;
            this.uText3thComplete.Visible = false;
            // 
            // frmINSZ0011D1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(944, 617);
            this.Controls.Add(this.uText3thComplete);
            this.Controls.Add(this.uText2ndComplete);
            this.Controls.Add(this.uText1stComplete);
            this.Controls.Add(this.uCheckComplete);
            this.Controls.Add(this.ultraButton1);
            this.Controls.Add(this.uGroupBox3th);
            this.Controls.Add(this.uGroupBox2nd);
            this.Controls.Add(this.uGroupBox1st);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0011D1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmINSZ0011D1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0011D1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1st)).EndInit();
            this.uGroupBox1st.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1st)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2nd)).EndInit();
            this.uGroupBox2nd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2nd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3th)).EndInit();
            this.uGroupBox3th.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3th)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1stComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText2ndComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText3thComplete)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1st;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1st;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2nd;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2nd;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3th;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3th;
        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText1stComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText2ndComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText3thComplete;
    }
}