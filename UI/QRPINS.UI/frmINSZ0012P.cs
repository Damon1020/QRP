﻿/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINSZ0012P.cs                                        */
/* 프로그램명   : SPCN LIST_Xn값 조회                                   */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-02-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0012P : Form
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        #region 전역변수

        private string m_strPlantCode;
        private string m_strReqNo;
        private string m_strReqSeq;
        private Int32 m_intReqLotSeq;
        private Int32 m_intReqItemSeq;

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string ReqNo
        {
            get { return m_strReqNo; }
            set { m_strReqNo = value; }
        }

        public string ReqSeq
        {
            get { return m_strReqSeq; }
            set { m_strReqSeq = value; }
        }

        public Int32 ReqLotSeq
        {
            get { return m_intReqLotSeq; }
            set { m_intReqLotSeq = value; }
        }

        public Int32 ReqItemSeq
        {
            get { return m_intReqItemSeq; }
            set { m_intReqItemSeq = value; }
        }

        #endregion

        public frmINSZ0012P()
        {
            InitializeComponent();
        }

        private void frmINSZ0012P_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("측정값 조회", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 초기화 메소드 호출
            InitButton();
            InitGrid();

            // 데이터 조회 메소드 호출
            SearchData();
        }

        /// <summary>
        /// 버튼 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonClose, "Close", m_SysRes.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);

                this.uButtonClose.Hide();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridValueList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_SysRes.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridValueList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueList, 0, "ReqNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueList, 0, "ReqSeq", "관리번호순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 4
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueList, 0, "ReqLotSeq", "Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueList, 0, "ReqItemSeq", "Item순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueList, 0, "ReqResultSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridValueList, 0, "InspectValue", "측정값", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Font 설정
                this.uGridValueList.DisplayLayout.Bands[0].Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridValueList.DisplayLayout.Bands[0].Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // 그리드 편집불가상태
                this.uGridValueList.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 측정값 조회 메소드
        private void SearchData()
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultMeasure), "ProcInspectResultMeasure");
                QRPINS.BL.INSPRC.ProcInspectResultMeasure clsResMeasure = new QRPINS.BL.INSPRC.ProcInspectResultMeasure();
                brwChannel.mfCredentials(clsResMeasure);

                DataTable dtMeasureData = clsResMeasure.mfReadINSProcInspectResultMeasure(PlantCode, ReqNo, ReqSeq, ReqLotSeq, ReqItemSeq, m_SysRes.GetString("SYS_LANG"));

                this.uGridValueList.DataSource = dtMeasureData;
                this.uGridValueList.DataBind();

                if (dtMeasureData.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfSetAutoResizeColWidth(this.uGridValueList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Close Button Click Event
        private void uButtonClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Dispose();
                this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }

    
}
