﻿namespace QRPINS.UI
{
    partial class frmINSZ0012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0012));
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement2 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect2 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCompleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchWriteToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchWriteFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchRegisterDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLCL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLCL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCpk = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCpk = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCp = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCp = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMin = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMin = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMax = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRange = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRange = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUSL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUSL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLSL = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLSL = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectItemName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectItemCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpecRange = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStdDev = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelstdDeviation = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAvg = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAvg = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateWrieteTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPACKAGE = new Infragistics.Win.Misc.UltraLabel();
            this.uTextGeneration = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStackSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteTime = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProcessCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRegister = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqItemSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqLotSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRegisterDate = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCycle = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckLowerTrend = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckUpperTrend = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckLowerRun = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckUpperRun = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckOCP = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProcessCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerProductSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSPCUnfitType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridSPCNList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelCopleteFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckComplete = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextResultUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPreventionUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCauseUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProblemUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel20 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextResultDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateResultDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel22 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextResultUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel21 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel17 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPreventionDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDatePreventionDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel19 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPreventionUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel18 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCauseDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateCauseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel16 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCauseUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel15 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProblemDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateProblemDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProblemUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridValueData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uChartX = new Infragistics.Win.UltraWinChart.UltraChart();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLCL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUSL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLSL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAvg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWrieteTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGeneration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStackSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqItemSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqLotSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckLowerTrend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUpperTrend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckLowerRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUpperRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckOCP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPCNList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPreventionUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCauseUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblemUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateResultDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPreventionDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDatePreventionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPreventionUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCauseDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCauseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCauseUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblemDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateProblemDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblemUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridValueData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChartX)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCompleteFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCompleteFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchWriteToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchWriteFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRegisterDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchCompleteFlag
            // 
            this.uComboSearchCompleteFlag.Location = new System.Drawing.Point(908, 12);
            this.uComboSearchCompleteFlag.Name = "uComboSearchCompleteFlag";
            this.uComboSearchCompleteFlag.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchCompleteFlag.TabIndex = 21;
            this.uComboSearchCompleteFlag.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCompleteFlag
            // 
            this.uLabelSearchCompleteFlag.Location = new System.Drawing.Point(792, 12);
            this.uLabelSearchCompleteFlag.Name = "uLabelSearchCompleteFlag";
            this.uLabelSearchCompleteFlag.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchCompleteFlag.TabIndex = 20;
            this.uLabelSearchCompleteFlag.Text = "ultraLabel3";
            // 
            // uDateSearchWriteToDate
            // 
            this.uDateSearchWriteToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchWriteToDate.Location = new System.Drawing.Point(686, 12);
            this.uDateSearchWriteToDate.Name = "uDateSearchWriteToDate";
            this.uDateSearchWriteToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchWriteToDate.TabIndex = 16;
            // 
            // ultraLabel2
            // 
            appearance4.TextHAlignAsString = "Center";
            appearance4.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance4;
            this.ultraLabel2.Location = new System.Drawing.Point(676, 12);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(8, 20);
            this.ultraLabel2.TabIndex = 15;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchWriteFromDate
            // 
            this.uDateSearchWriteFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchWriteFromDate.Location = new System.Drawing.Point(574, 12);
            this.uDateSearchWriteFromDate.Name = "uDateSearchWriteFromDate";
            this.uDateSearchWriteFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchWriteFromDate.TabIndex = 14;
            // 
            // uLabelSearchRegisterDate
            // 
            this.uLabelSearchRegisterDate.Location = new System.Drawing.Point(458, 12);
            this.uLabelSearchRegisterDate.Name = "uLabelSearchRegisterDate";
            this.uLabelSearchRegisterDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchRegisterDate.TabIndex = 13;
            this.uLabelSearchRegisterDate.Text = "등록일";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(352, 12);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchProcess.TabIndex = 6;
            this.uComboSearchProcess.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(236, 12);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchProcess.TabIndex = 5;
            this.uLabelSearchProcess.Text = "공정";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 680);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 680);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 660);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Controls.Add(this.uTextCL);
            this.uGroupBox4.Controls.Add(this.uLabelCL);
            this.uGroupBox4.Controls.Add(this.uTextUCL);
            this.uGroupBox4.Controls.Add(this.uLabelUCL);
            this.uGroupBox4.Controls.Add(this.uTextLCL);
            this.uGroupBox4.Controls.Add(this.uLabelLCL);
            this.uGroupBox4.Controls.Add(this.uTextCpk);
            this.uGroupBox4.Controls.Add(this.uLabelCpk);
            this.uGroupBox4.Controls.Add(this.uTextCp);
            this.uGroupBox4.Controls.Add(this.uLabelCp);
            this.uGroupBox4.Controls.Add(this.uTextMin);
            this.uGroupBox4.Controls.Add(this.uLabelMin);
            this.uGroupBox4.Controls.Add(this.uTextMax);
            this.uGroupBox4.Controls.Add(this.uLabelMax);
            this.uGroupBox4.Controls.Add(this.uTextRange);
            this.uGroupBox4.Controls.Add(this.uLabelRange);
            this.uGroupBox4.Controls.Add(this.uTextUSL);
            this.uGroupBox4.Controls.Add(this.uLabelUSL);
            this.uGroupBox4.Controls.Add(this.uTextLSL);
            this.uGroupBox4.Controls.Add(this.uLabelLSL);
            this.uGroupBox4.Controls.Add(this.uTextInspectItemName);
            this.uGroupBox4.Controls.Add(this.uTextInspectItemCode);
            this.uGroupBox4.Controls.Add(this.uTextSpecRange);
            this.uGroupBox4.Controls.Add(this.uTextStdDev);
            this.uGroupBox4.Controls.Add(this.uLabelstdDeviation);
            this.uGroupBox4.Controls.Add(this.uTextAvg);
            this.uGroupBox4.Controls.Add(this.uLabelAvg);
            this.uGroupBox4.Controls.Add(this.uLabelInspectItem);
            this.uGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBox4.Location = new System.Drawing.Point(0, 104);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(1064, 104);
            this.uGroupBox4.TabIndex = 41;
            // 
            // uTextCL
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            appearance19.TextHAlignAsString = "Right";
            this.uTextCL.Appearance = appearance19;
            this.uTextCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCL.Location = new System.Drawing.Point(656, 52);
            this.uTextCL.Name = "uTextCL";
            this.uTextCL.ReadOnly = true;
            this.uTextCL.Size = new System.Drawing.Size(92, 21);
            this.uTextCL.TabIndex = 87;
            // 
            // uLabelCL
            // 
            this.uLabelCL.Location = new System.Drawing.Point(572, 52);
            this.uLabelCL.Name = "uLabelCL";
            this.uLabelCL.Size = new System.Drawing.Size(80, 20);
            this.uLabelCL.TabIndex = 86;
            this.uLabelCL.Text = "CL";
            // 
            // uTextUCL
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            appearance18.TextHAlignAsString = "Right";
            this.uTextUCL.Appearance = appearance18;
            this.uTextUCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUCL.Location = new System.Drawing.Point(656, 76);
            this.uTextUCL.Name = "uTextUCL";
            this.uTextUCL.ReadOnly = true;
            this.uTextUCL.Size = new System.Drawing.Size(92, 21);
            this.uTextUCL.TabIndex = 85;
            // 
            // uLabelUCL
            // 
            this.uLabelUCL.Location = new System.Drawing.Point(572, 76);
            this.uLabelUCL.Name = "uLabelUCL";
            this.uLabelUCL.Size = new System.Drawing.Size(80, 20);
            this.uLabelUCL.TabIndex = 84;
            this.uLabelUCL.Text = "UCL";
            // 
            // uTextLCL
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            appearance46.TextHAlignAsString = "Right";
            this.uTextLCL.Appearance = appearance46;
            this.uTextLCL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLCL.Location = new System.Drawing.Point(656, 28);
            this.uTextLCL.Name = "uTextLCL";
            this.uTextLCL.ReadOnly = true;
            this.uTextLCL.Size = new System.Drawing.Size(92, 21);
            this.uTextLCL.TabIndex = 83;
            // 
            // uLabelLCL
            // 
            this.uLabelLCL.Location = new System.Drawing.Point(572, 28);
            this.uLabelLCL.Name = "uLabelLCL";
            this.uLabelLCL.Size = new System.Drawing.Size(80, 20);
            this.uLabelLCL.TabIndex = 82;
            this.uLabelLCL.Text = "LCL";
            // 
            // uTextCpk
            // 
            appearance68.BackColor = System.Drawing.Color.Gainsboro;
            appearance68.TextHAlignAsString = "Right";
            this.uTextCpk.Appearance = appearance68;
            this.uTextCpk.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCpk.Location = new System.Drawing.Point(840, 52);
            this.uTextCpk.Name = "uTextCpk";
            this.uTextCpk.ReadOnly = true;
            this.uTextCpk.Size = new System.Drawing.Size(92, 21);
            this.uTextCpk.TabIndex = 81;
            // 
            // uLabelCpk
            // 
            this.uLabelCpk.Location = new System.Drawing.Point(756, 52);
            this.uLabelCpk.Name = "uLabelCpk";
            this.uLabelCpk.Size = new System.Drawing.Size(80, 20);
            this.uLabelCpk.TabIndex = 80;
            this.uLabelCpk.Text = "Cpk";
            // 
            // uTextCp
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            appearance44.TextHAlignAsString = "Right";
            this.uTextCp.Appearance = appearance44;
            this.uTextCp.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCp.Location = new System.Drawing.Point(840, 28);
            this.uTextCp.Name = "uTextCp";
            this.uTextCp.ReadOnly = true;
            this.uTextCp.Size = new System.Drawing.Size(92, 21);
            this.uTextCp.TabIndex = 79;
            // 
            // uLabelCp
            // 
            this.uLabelCp.Location = new System.Drawing.Point(756, 28);
            this.uLabelCp.Name = "uLabelCp";
            this.uLabelCp.Size = new System.Drawing.Size(80, 20);
            this.uLabelCp.TabIndex = 78;
            this.uLabelCp.Text = "Cp";
            // 
            // uTextMin
            // 
            appearance62.BackColor = System.Drawing.Color.Gainsboro;
            appearance62.TextHAlignAsString = "Right";
            this.uTextMin.Appearance = appearance62;
            this.uTextMin.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMin.Location = new System.Drawing.Point(288, 76);
            this.uTextMin.Name = "uTextMin";
            this.uTextMin.ReadOnly = true;
            this.uTextMin.Size = new System.Drawing.Size(92, 21);
            this.uTextMin.TabIndex = 77;
            // 
            // uLabelMin
            // 
            this.uLabelMin.Location = new System.Drawing.Point(204, 76);
            this.uLabelMin.Name = "uLabelMin";
            this.uLabelMin.Size = new System.Drawing.Size(80, 20);
            this.uLabelMin.TabIndex = 76;
            this.uLabelMin.Text = "MIN";
            // 
            // uTextMax
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            appearance47.TextHAlignAsString = "Right";
            this.uTextMax.Appearance = appearance47;
            this.uTextMax.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMax.Location = new System.Drawing.Point(288, 52);
            this.uTextMax.Name = "uTextMax";
            this.uTextMax.ReadOnly = true;
            this.uTextMax.Size = new System.Drawing.Size(92, 21);
            this.uTextMax.TabIndex = 75;
            // 
            // uLabelMax
            // 
            this.uLabelMax.Location = new System.Drawing.Point(204, 52);
            this.uLabelMax.Name = "uLabelMax";
            this.uLabelMax.Size = new System.Drawing.Size(80, 20);
            this.uLabelMax.TabIndex = 74;
            this.uLabelMax.Text = "MAX";
            // 
            // uTextRange
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            appearance37.TextHAlignAsString = "Right";
            this.uTextRange.Appearance = appearance37;
            this.uTextRange.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRange.Location = new System.Drawing.Point(472, 76);
            this.uTextRange.Name = "uTextRange";
            this.uTextRange.ReadOnly = true;
            this.uTextRange.Size = new System.Drawing.Size(92, 21);
            this.uTextRange.TabIndex = 73;
            // 
            // uLabelRange
            // 
            this.uLabelRange.Location = new System.Drawing.Point(388, 76);
            this.uLabelRange.Name = "uLabelRange";
            this.uLabelRange.Size = new System.Drawing.Size(80, 20);
            this.uLabelRange.TabIndex = 72;
            this.uLabelRange.Text = "Range";
            // 
            // uTextUSL
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            appearance39.TextHAlignAsString = "Right";
            this.uTextUSL.Appearance = appearance39;
            this.uTextUSL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUSL.Location = new System.Drawing.Point(472, 52);
            this.uTextUSL.Name = "uTextUSL";
            this.uTextUSL.ReadOnly = true;
            this.uTextUSL.Size = new System.Drawing.Size(92, 21);
            this.uTextUSL.TabIndex = 71;
            // 
            // uLabelUSL
            // 
            this.uLabelUSL.Location = new System.Drawing.Point(388, 52);
            this.uLabelUSL.Name = "uLabelUSL";
            this.uLabelUSL.Size = new System.Drawing.Size(80, 20);
            this.uLabelUSL.TabIndex = 70;
            this.uLabelUSL.Text = "USL";
            // 
            // uTextLSL
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            appearance40.TextHAlignAsString = "Right";
            this.uTextLSL.Appearance = appearance40;
            this.uTextLSL.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLSL.Location = new System.Drawing.Point(472, 28);
            this.uTextLSL.Name = "uTextLSL";
            this.uTextLSL.ReadOnly = true;
            this.uTextLSL.Size = new System.Drawing.Size(92, 21);
            this.uTextLSL.TabIndex = 69;
            // 
            // uLabelLSL
            // 
            this.uLabelLSL.Location = new System.Drawing.Point(388, 28);
            this.uLabelLSL.Name = "uLabelLSL";
            this.uLabelLSL.Size = new System.Drawing.Size(80, 20);
            this.uLabelLSL.TabIndex = 68;
            this.uLabelLSL.Text = "LSL";
            // 
            // uTextInspectItemName
            // 
            appearance66.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemName.Appearance = appearance66;
            this.uTextInspectItemName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemName.Location = new System.Drawing.Point(96, 28);
            this.uTextInspectItemName.Name = "uTextInspectItemName";
            this.uTextInspectItemName.ReadOnly = true;
            this.uTextInspectItemName.Size = new System.Drawing.Size(284, 21);
            this.uTextInspectItemName.TabIndex = 39;
            // 
            // uTextInspectItemCode
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemCode.Appearance = appearance41;
            this.uTextInspectItemCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectItemCode.Location = new System.Drawing.Point(992, 80);
            this.uTextInspectItemCode.Name = "uTextInspectItemCode";
            this.uTextInspectItemCode.ReadOnly = true;
            this.uTextInspectItemCode.Size = new System.Drawing.Size(20, 21);
            this.uTextInspectItemCode.TabIndex = 67;
            this.uTextInspectItemCode.Visible = false;
            // 
            // uTextSpecRange
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Appearance = appearance60;
            this.uTextSpecRange.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpecRange.Location = new System.Drawing.Point(1016, 80);
            this.uTextSpecRange.Name = "uTextSpecRange";
            this.uTextSpecRange.ReadOnly = true;
            this.uTextSpecRange.Size = new System.Drawing.Size(20, 21);
            this.uTextSpecRange.TabIndex = 66;
            this.uTextSpecRange.Visible = false;
            // 
            // uTextStdDev
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            appearance38.TextHAlignAsString = "Right";
            this.uTextStdDev.Appearance = appearance38;
            this.uTextStdDev.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdDev.Location = new System.Drawing.Point(96, 52);
            this.uTextStdDev.Name = "uTextStdDev";
            this.uTextStdDev.ReadOnly = true;
            this.uTextStdDev.Size = new System.Drawing.Size(100, 21);
            this.uTextStdDev.TabIndex = 51;
            // 
            // uLabelstdDeviation
            // 
            this.uLabelstdDeviation.Location = new System.Drawing.Point(12, 52);
            this.uLabelstdDeviation.Name = "uLabelstdDeviation";
            this.uLabelstdDeviation.Size = new System.Drawing.Size(80, 20);
            this.uLabelstdDeviation.TabIndex = 50;
            this.uLabelstdDeviation.Text = "표준편차";
            // 
            // uTextAvg
            // 
            appearance45.BackColor = System.Drawing.Color.Gainsboro;
            appearance45.TextHAlignAsString = "Right";
            this.uTextAvg.Appearance = appearance45;
            this.uTextAvg.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAvg.Location = new System.Drawing.Point(96, 76);
            this.uTextAvg.Name = "uTextAvg";
            this.uTextAvg.ReadOnly = true;
            this.uTextAvg.Size = new System.Drawing.Size(100, 21);
            this.uTextAvg.TabIndex = 49;
            // 
            // uLabelAvg
            // 
            this.uLabelAvg.Location = new System.Drawing.Point(12, 76);
            this.uLabelAvg.Name = "uLabelAvg";
            this.uLabelAvg.Size = new System.Drawing.Size(80, 20);
            this.uLabelAvg.TabIndex = 48;
            this.uLabelAvg.Text = "AVG";
            // 
            // uLabelInspectItem
            // 
            this.uLabelInspectItem.Location = new System.Drawing.Point(12, 28);
            this.uLabelInspectItem.Name = "uLabelInspectItem";
            this.uLabelInspectItem.Size = new System.Drawing.Size(80, 20);
            this.uLabelInspectItem.TabIndex = 38;
            this.uLabelInspectItem.Text = "검사항목";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uDateWrieteTime);
            this.uGroupBox3.Controls.Add(this.uTextProductName);
            this.uGroupBox3.Controls.Add(this.uTextProductCode);
            this.uGroupBox3.Controls.Add(this.uLabelPACKAGE);
            this.uGroupBox3.Controls.Add(this.uTextGeneration);
            this.uGroupBox3.Controls.Add(this.uTextStackSeq);
            this.uGroupBox3.Controls.Add(this.uTextWriteTime);
            this.uGroupBox3.Controls.Add(this.uTextPackage);
            this.uGroupBox3.Controls.Add(this.uTextProcessCode);
            this.uGroupBox3.Controls.Add(this.uTextEquipCode);
            this.uGroupBox3.Controls.Add(this.uTextMESTFlag);
            this.uGroupBox3.Controls.Add(this.uTextWriteUserID);
            this.uGroupBox3.Controls.Add(this.uTextWriteUserName);
            this.uGroupBox3.Controls.Add(this.uLabelRegister);
            this.uGroupBox3.Controls.Add(this.uTextPlantCode);
            this.uGroupBox3.Controls.Add(this.uTextReqItemSeq);
            this.uGroupBox3.Controls.Add(this.uTextReqLotSeq);
            this.uGroupBox3.Controls.Add(this.uTextReqSeq);
            this.uGroupBox3.Controls.Add(this.uTextReqNo);
            this.uGroupBox3.Controls.Add(this.uTextCustomerName);
            this.uGroupBox3.Controls.Add(this.uTextCustomerProductSpec);
            this.uGroupBox3.Controls.Add(this.uTextWriteDate);
            this.uGroupBox3.Controls.Add(this.uLabelRegisterDate);
            this.uGroupBox3.Controls.Add(this.uCheckCycle);
            this.uGroupBox3.Controls.Add(this.uCheckLowerTrend);
            this.uGroupBox3.Controls.Add(this.uCheckUpperTrend);
            this.uGroupBox3.Controls.Add(this.uCheckLowerRun);
            this.uGroupBox3.Controls.Add(this.uCheckUpperRun);
            this.uGroupBox3.Controls.Add(this.uCheckOCP);
            this.uGroupBox3.Controls.Add(this.uTextLotNo);
            this.uGroupBox3.Controls.Add(this.uLabelLotNo);
            this.uGroupBox3.Controls.Add(this.uTextEquipName);
            this.uGroupBox3.Controls.Add(this.uLabelEquip);
            this.uGroupBox3.Controls.Add(this.uTextProcessName);
            this.uGroupBox3.Controls.Add(this.uLabelProcessCode);
            this.uGroupBox3.Controls.Add(this.uLabelCustomerProductSpec);
            this.uGroupBox3.Controls.Add(this.uTextCustomerCode);
            this.uGroupBox3.Controls.Add(this.uLabelCustomer);
            this.uGroupBox3.Controls.Add(this.uLabelSPCUnfitType);
            this.uGroupBox3.Controls.Add(this.uTextPlantName);
            this.uGroupBox3.Controls.Add(this.uLabelPlant);
            this.uGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1064, 104);
            this.uGroupBox3.TabIndex = 40;
            // 
            // uDateWrieteTime
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uDateWrieteTime.Appearance = appearance42;
            this.uDateWrieteTime.BackColor = System.Drawing.Color.Gainsboro;
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateWrieteTime.ButtonsRight.Add(spinEditorButton2);
            this.uDateWrieteTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateWrieteTime.Location = new System.Drawing.Point(849, 52);
            this.uDateWrieteTime.MaskInput = "{LOC}hh:mm:ss";
            this.uDateWrieteTime.Name = "uDateWrieteTime";
            this.uDateWrieteTime.Size = new System.Drawing.Size(87, 21);
            this.uDateWrieteTime.TabIndex = 83;
            this.uDateWrieteTime.Value = null;
            // 
            // uTextProductName
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance35;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(312, 52);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(21, 21);
            this.uTextProductName.TabIndex = 82;
            // 
            // uTextProductCode
            // 
            appearance74.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance74;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(288, 52);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(21, 21);
            this.uTextProductCode.TabIndex = 35;
            // 
            // uLabelPACKAGE
            // 
            this.uLabelPACKAGE.Location = new System.Drawing.Point(12, 76);
            this.uLabelPACKAGE.Name = "uLabelPACKAGE";
            this.uLabelPACKAGE.Size = new System.Drawing.Size(110, 20);
            this.uLabelPACKAGE.TabIndex = 81;
            this.uLabelPACKAGE.Text = "PACKAGE";
            // 
            // uTextGeneration
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGeneration.Appearance = appearance17;
            this.uTextGeneration.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGeneration.Location = new System.Drawing.Point(988, 0);
            this.uTextGeneration.Name = "uTextGeneration";
            this.uTextGeneration.ReadOnly = true;
            this.uTextGeneration.Size = new System.Drawing.Size(20, 21);
            this.uTextGeneration.TabIndex = 78;
            this.uTextGeneration.Visible = false;
            // 
            // uTextStackSeq
            // 
            appearance72.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStackSeq.Appearance = appearance72;
            this.uTextStackSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStackSeq.Location = new System.Drawing.Point(964, 0);
            this.uTextStackSeq.Name = "uTextStackSeq";
            this.uTextStackSeq.ReadOnly = true;
            this.uTextStackSeq.Size = new System.Drawing.Size(20, 21);
            this.uTextStackSeq.TabIndex = 77;
            this.uTextStackSeq.Visible = false;
            // 
            // uTextWriteTime
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteTime.Appearance = appearance30;
            this.uTextWriteTime.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteTime.Location = new System.Drawing.Point(940, 0);
            this.uTextWriteTime.Name = "uTextWriteTime";
            this.uTextWriteTime.ReadOnly = true;
            this.uTextWriteTime.Size = new System.Drawing.Size(20, 21);
            this.uTextWriteTime.TabIndex = 76;
            this.uTextWriteTime.Visible = false;
            // 
            // uTextPackage
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance26;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(128, 76);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(264, 21);
            this.uTextPackage.TabIndex = 75;
            // 
            // uTextProcessCode
            // 
            appearance73.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessCode.Appearance = appearance73;
            this.uTextProcessCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessCode.Location = new System.Drawing.Point(128, 28);
            this.uTextProcessCode.Name = "uTextProcessCode";
            this.uTextProcessCode.ReadOnly = true;
            this.uTextProcessCode.Size = new System.Drawing.Size(90, 21);
            this.uTextProcessCode.TabIndex = 74;
            // 
            // uTextEquipCode
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance53;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(748, 28);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 73;
            // 
            // uTextMESTFlag
            // 
            appearance61.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTFlag.Appearance = appearance61;
            this.uTextMESTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTFlag.Location = new System.Drawing.Point(1012, 0);
            this.uTextMESTFlag.Name = "uTextMESTFlag";
            this.uTextMESTFlag.ReadOnly = true;
            this.uTextMESTFlag.Size = new System.Drawing.Size(20, 21);
            this.uTextMESTFlag.TabIndex = 29;
            this.uTextMESTFlag.Visible = false;
            // 
            // uTextWriteUserID
            // 
            appearance70.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserID.Appearance = appearance70;
            this.uTextWriteUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserID.Location = new System.Drawing.Point(916, 0);
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.ReadOnly = true;
            this.uTextWriteUserID.Size = new System.Drawing.Size(20, 21);
            this.uTextWriteUserID.TabIndex = 72;
            this.uTextWriteUserID.Visible = false;
            // 
            // uTextWriteUserName
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Appearance = appearance25;
            this.uTextWriteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Location = new System.Drawing.Point(756, 0);
            this.uTextWriteUserName.Name = "uTextWriteUserName";
            this.uTextWriteUserName.ReadOnly = true;
            this.uTextWriteUserName.Size = new System.Drawing.Size(14, 21);
            this.uTextWriteUserName.TabIndex = 71;
            this.uTextWriteUserName.Visible = false;
            // 
            // uLabelRegister
            // 
            this.uLabelRegister.Location = new System.Drawing.Point(740, 0);
            this.uLabelRegister.Name = "uLabelRegister";
            this.uLabelRegister.Size = new System.Drawing.Size(16, 20);
            this.uLabelRegister.TabIndex = 70;
            this.uLabelRegister.Text = "등록자";
            this.uLabelRegister.Visible = false;
            // 
            // uTextPlantCode
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance67;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(716, 0);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(20, 21);
            this.uTextPlantCode.TabIndex = 69;
            this.uTextPlantCode.Visible = false;
            // 
            // uTextReqItemSeq
            // 
            appearance71.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqItemSeq.Appearance = appearance71;
            this.uTextReqItemSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqItemSeq.Location = new System.Drawing.Point(892, 0);
            this.uTextReqItemSeq.Name = "uTextReqItemSeq";
            this.uTextReqItemSeq.ReadOnly = true;
            this.uTextReqItemSeq.Size = new System.Drawing.Size(20, 21);
            this.uTextReqItemSeq.TabIndex = 68;
            this.uTextReqItemSeq.Visible = false;
            // 
            // uTextReqLotSeq
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqLotSeq.Appearance = appearance3;
            this.uTextReqLotSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqLotSeq.Location = new System.Drawing.Point(868, 0);
            this.uTextReqLotSeq.Name = "uTextReqLotSeq";
            this.uTextReqLotSeq.ReadOnly = true;
            this.uTextReqLotSeq.Size = new System.Drawing.Size(20, 21);
            this.uTextReqLotSeq.TabIndex = 67;
            this.uTextReqLotSeq.Visible = false;
            // 
            // uTextReqSeq
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Appearance = appearance43;
            this.uTextReqSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Location = new System.Drawing.Point(844, 0);
            this.uTextReqSeq.Name = "uTextReqSeq";
            this.uTextReqSeq.ReadOnly = true;
            this.uTextReqSeq.Size = new System.Drawing.Size(20, 21);
            this.uTextReqSeq.TabIndex = 66;
            this.uTextReqSeq.Visible = false;
            // 
            // uTextReqNo
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Appearance = appearance34;
            this.uTextReqNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Location = new System.Drawing.Point(820, 0);
            this.uTextReqNo.Name = "uTextReqNo";
            this.uTextReqNo.ReadOnly = true;
            this.uTextReqNo.Size = new System.Drawing.Size(20, 21);
            this.uTextReqNo.TabIndex = 65;
            this.uTextReqNo.Visible = false;
            // 
            // uTextCustomerName
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance20;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(600, 28);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(21, 21);
            this.uTextCustomerName.TabIndex = 64;
            // 
            // uTextCustomerProductSpec
            // 
            appearance69.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Appearance = appearance69;
            this.uTextCustomerProductSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Location = new System.Drawing.Point(128, 52);
            this.uTextCustomerProductSpec.Name = "uTextCustomerProductSpec";
            this.uTextCustomerProductSpec.ReadOnly = true;
            this.uTextCustomerProductSpec.Size = new System.Drawing.Size(264, 21);
            this.uTextCustomerProductSpec.TabIndex = 62;
            // 
            // uTextWriteDate
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteDate.Appearance = appearance27;
            this.uTextWriteDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteDate.Location = new System.Drawing.Point(748, 52);
            this.uTextWriteDate.Name = "uTextWriteDate";
            this.uTextWriteDate.ReadOnly = true;
            this.uTextWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteDate.TabIndex = 61;
            // 
            // uLabelRegisterDate
            // 
            this.uLabelRegisterDate.Location = new System.Drawing.Point(632, 52);
            this.uLabelRegisterDate.Name = "uLabelRegisterDate";
            this.uLabelRegisterDate.Size = new System.Drawing.Size(110, 20);
            this.uLabelRegisterDate.TabIndex = 60;
            this.uLabelRegisterDate.Text = "등록일";
            // 
            // uCheckCycle
            // 
            this.uCheckCycle.Location = new System.Drawing.Point(940, 52);
            this.uCheckCycle.Name = "uCheckCycle";
            this.uCheckCycle.Size = new System.Drawing.Size(76, 20);
            this.uCheckCycle.TabIndex = 49;
            this.uCheckCycle.Text = "주기";
            this.uCheckCycle.Visible = false;
            // 
            // uCheckLowerTrend
            // 
            this.uCheckLowerTrend.Location = new System.Drawing.Point(956, 76);
            this.uCheckLowerTrend.Name = "uCheckLowerTrend";
            this.uCheckLowerTrend.Size = new System.Drawing.Size(100, 20);
            this.uCheckLowerTrend.TabIndex = 48;
            this.uCheckLowerTrend.Text = "하향경향";
            // 
            // uCheckUpperTrend
            // 
            this.uCheckUpperTrend.Location = new System.Drawing.Point(852, 76);
            this.uCheckUpperTrend.Name = "uCheckUpperTrend";
            this.uCheckUpperTrend.Size = new System.Drawing.Size(100, 20);
            this.uCheckUpperTrend.TabIndex = 47;
            this.uCheckUpperTrend.Text = "상향경향";
            // 
            // uCheckLowerRun
            // 
            this.uCheckLowerRun.Location = new System.Drawing.Point(756, 76);
            this.uCheckLowerRun.Name = "uCheckLowerRun";
            this.uCheckLowerRun.Size = new System.Drawing.Size(90, 20);
            this.uCheckLowerRun.TabIndex = 46;
            this.uCheckLowerRun.Text = "하향런";
            // 
            // uCheckUpperRun
            // 
            this.uCheckUpperRun.Location = new System.Drawing.Point(660, 76);
            this.uCheckUpperRun.Name = "uCheckUpperRun";
            this.uCheckUpperRun.Size = new System.Drawing.Size(90, 20);
            this.uCheckUpperRun.TabIndex = 45;
            this.uCheckUpperRun.Text = "상향런";
            // 
            // uCheckOCP
            // 
            this.uCheckOCP.BackColor = System.Drawing.Color.White;
            this.uCheckOCP.BackColorInternal = System.Drawing.Color.White;
            this.uCheckOCP.Location = new System.Drawing.Point(524, 76);
            this.uCheckOCP.Name = "uCheckOCP";
            this.uCheckOCP.Size = new System.Drawing.Size(130, 20);
            this.uCheckOCP.TabIndex = 44;
            this.uCheckOCP.Text = "관리한계선이탈";
            // 
            // uTextLotNo
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Appearance = appearance22;
            this.uTextLotNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Location = new System.Drawing.Point(524, 52);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.ReadOnly = true;
            this.uTextLotNo.Size = new System.Drawing.Size(100, 21);
            this.uTextLotNo.TabIndex = 43;
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(400, 52);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(120, 20);
            this.uLabelLotNo.TabIndex = 42;
            this.uLabelLotNo.Text = "LotNo";
            // 
            // uTextEquipName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance23;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(844, 28);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(21, 21);
            this.uTextEquipName.TabIndex = 41;
            this.uTextEquipName.Visible = false;
            // 
            // uLabelEquip
            // 
            this.uLabelEquip.Location = new System.Drawing.Point(632, 28);
            this.uLabelEquip.Name = "uLabelEquip";
            this.uLabelEquip.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquip.TabIndex = 40;
            this.uLabelEquip.Text = "설비";
            // 
            // uTextProcessName
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessName.Appearance = appearance24;
            this.uTextProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessName.Location = new System.Drawing.Point(220, 28);
            this.uTextProcessName.Name = "uTextProcessName";
            this.uTextProcessName.ReadOnly = true;
            this.uTextProcessName.Size = new System.Drawing.Size(172, 21);
            this.uTextProcessName.TabIndex = 39;
            // 
            // uLabelProcessCode
            // 
            this.uLabelProcessCode.Location = new System.Drawing.Point(12, 28);
            this.uLabelProcessCode.Name = "uLabelProcessCode";
            this.uLabelProcessCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelProcessCode.TabIndex = 38;
            this.uLabelProcessCode.Text = "공정";
            // 
            // uLabelCustomerProductSpec
            // 
            this.uLabelCustomerProductSpec.Location = new System.Drawing.Point(12, 52);
            this.uLabelCustomerProductSpec.Name = "uLabelCustomerProductSpec";
            this.uLabelCustomerProductSpec.Size = new System.Drawing.Size(110, 20);
            this.uLabelCustomerProductSpec.TabIndex = 34;
            this.uLabelCustomerProductSpec.Text = "고객사제품코드";
            // 
            // uTextCustomerCode
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance36;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(524, 28);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(100, 21);
            this.uTextCustomerCode.TabIndex = 33;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(400, 28);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(120, 20);
            this.uLabelCustomer.TabIndex = 32;
            this.uLabelCustomer.Text = "고객사";
            // 
            // uLabelSPCUnfitType
            // 
            this.uLabelSPCUnfitType.Location = new System.Drawing.Point(400, 76);
            this.uLabelSPCUnfitType.Name = "uLabelSPCUnfitType";
            this.uLabelSPCUnfitType.Size = new System.Drawing.Size(120, 20);
            this.uLabelSPCUnfitType.TabIndex = 28;
            this.uLabelSPCUnfitType.Text = "SPC부적합유형";
            // 
            // uTextPlantName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance21;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(800, 0);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(17, 21);
            this.uTextPlantName.TabIndex = 27;
            this.uTextPlantName.Visible = false;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(772, 0);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(27, 20);
            this.uLabelPlant.TabIndex = 26;
            this.uLabelPlant.Text = "공장";
            this.uLabelPlant.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridSPCNList
            // 
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSPCNList.DisplayLayout.Appearance = appearance8;
            this.uGridSPCNList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSPCNList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPCNList.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPCNList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridSPCNList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSPCNList.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridSPCNList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSPCNList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSPCNList.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSPCNList.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridSPCNList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSPCNList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSPCNList.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSPCNList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridSPCNList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSPCNList.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSPCNList.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance15.TextHAlignAsString = "Left";
            this.uGridSPCNList.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridSPCNList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSPCNList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridSPCNList.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridSPCNList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSPCNList.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridSPCNList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSPCNList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSPCNList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSPCNList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridSPCNList.Location = new System.Drawing.Point(0, 80);
            this.uGridSPCNList.Name = "uGridSPCNList";
            this.uGridSPCNList.Size = new System.Drawing.Size(1070, 90);
            this.uGridSPCNList.TabIndex = 4;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uGroupBox1);
            this.ultraGroupBox2.Controls.Add(this.uGroupBox2);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 416);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1064, 244);
            this.ultraGroupBox2.TabIndex = 43;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uLabelCopleteFlag);
            this.uGroupBox2.Controls.Add(this.uCheckComplete);
            this.uGroupBox2.Controls.Add(this.uTextResultUserID);
            this.uGroupBox2.Controls.Add(this.uTextPreventionUserID);
            this.uGroupBox2.Controls.Add(this.uTextCauseUserID);
            this.uGroupBox2.Controls.Add(this.uTextProblemUserID);
            this.uGroupBox2.Controls.Add(this.uLabel20);
            this.uGroupBox2.Controls.Add(this.uTextResultDesc);
            this.uGroupBox2.Controls.Add(this.uDateResultDate);
            this.uGroupBox2.Controls.Add(this.uLabel22);
            this.uGroupBox2.Controls.Add(this.uTextResultUserName);
            this.uGroupBox2.Controls.Add(this.uLabel21);
            this.uGroupBox2.Controls.Add(this.uLabel17);
            this.uGroupBox2.Controls.Add(this.uTextPreventionDesc);
            this.uGroupBox2.Controls.Add(this.uDatePreventionDate);
            this.uGroupBox2.Controls.Add(this.uLabel19);
            this.uGroupBox2.Controls.Add(this.uTextPreventionUserName);
            this.uGroupBox2.Controls.Add(this.uLabel18);
            this.uGroupBox2.Controls.Add(this.uLabel14);
            this.uGroupBox2.Controls.Add(this.uTextCauseDesc);
            this.uGroupBox2.Controls.Add(this.uDateCauseDate);
            this.uGroupBox2.Controls.Add(this.uLabel16);
            this.uGroupBox2.Controls.Add(this.uTextCauseUserName);
            this.uGroupBox2.Controls.Add(this.uLabel15);
            this.uGroupBox2.Controls.Add(this.uLabel11);
            this.uGroupBox2.Controls.Add(this.uTextProblemDesc);
            this.uGroupBox2.Controls.Add(this.uDateProblemDate);
            this.uGroupBox2.Controls.Add(this.uLabel13);
            this.uGroupBox2.Controls.Add(this.uTextProblemUserName);
            this.uGroupBox2.Controls.Add(this.uLabel12);
            this.uGroupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.uGroupBox2.Location = new System.Drawing.Point(245, 0);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(816, 241);
            this.uGroupBox2.TabIndex = 41;
            // 
            // uLabelCopleteFlag
            // 
            this.uLabelCopleteFlag.Location = new System.Drawing.Point(12, 220);
            this.uLabelCopleteFlag.Name = "uLabelCopleteFlag";
            this.uLabelCopleteFlag.Size = new System.Drawing.Size(92, 20);
            this.uLabelCopleteFlag.TabIndex = 49;
            this.uLabelCopleteFlag.Text = "작성완료";
            // 
            // uCheckComplete
            // 
            this.uCheckComplete.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckComplete.Location = new System.Drawing.Point(108, 220);
            this.uCheckComplete.Name = "uCheckComplete";
            this.uCheckComplete.Size = new System.Drawing.Size(16, 20);
            this.uCheckComplete.TabIndex = 48;
            // 
            // uTextResultUserID
            // 
            appearance2.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextResultUserID.ButtonsRight.Add(editorButton1);
            this.uTextResultUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextResultUserID.Location = new System.Drawing.Point(592, 172);
            this.uTextResultUserID.Name = "uTextResultUserID";
            this.uTextResultUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextResultUserID.TabIndex = 31;
            // 
            // uTextPreventionUserID
            // 
            appearance63.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance63.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance63;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextPreventionUserID.ButtonsRight.Add(editorButton2);
            this.uTextPreventionUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextPreventionUserID.Location = new System.Drawing.Point(592, 124);
            this.uTextPreventionUserID.Name = "uTextPreventionUserID";
            this.uTextPreventionUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextPreventionUserID.TabIndex = 30;
            // 
            // uTextCauseUserID
            // 
            appearance64.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance64.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance64;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCauseUserID.ButtonsRight.Add(editorButton3);
            this.uTextCauseUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCauseUserID.Location = new System.Drawing.Point(592, 76);
            this.uTextCauseUserID.Name = "uTextCauseUserID";
            this.uTextCauseUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCauseUserID.TabIndex = 29;
            // 
            // uTextProblemUserID
            // 
            appearance65.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance65.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance65;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextProblemUserID.ButtonsRight.Add(editorButton4);
            this.uTextProblemUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextProblemUserID.Location = new System.Drawing.Point(592, 28);
            this.uTextProblemUserID.Name = "uTextProblemUserID";
            this.uTextProblemUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextProblemUserID.TabIndex = 28;
            // 
            // uLabel20
            // 
            this.uLabel20.Location = new System.Drawing.Point(12, 172);
            this.uLabel20.Name = "uLabel20";
            this.uLabel20.Size = new System.Drawing.Size(92, 44);
            this.uLabel20.TabIndex = 27;
            this.uLabel20.Text = "ultraLabel11";
            // 
            // uTextResultDesc
            // 
            this.uTextResultDesc.Location = new System.Drawing.Point(108, 172);
            this.uTextResultDesc.Multiline = true;
            this.uTextResultDesc.Name = "uTextResultDesc";
            this.uTextResultDesc.Size = new System.Drawing.Size(392, 44);
            this.uTextResultDesc.TabIndex = 26;
            // 
            // uDateResultDate
            // 
            this.uDateResultDate.Location = new System.Drawing.Point(592, 196);
            this.uDateResultDate.Name = "uDateResultDate";
            this.uDateResultDate.Size = new System.Drawing.Size(100, 21);
            this.uDateResultDate.TabIndex = 25;
            // 
            // uLabel22
            // 
            this.uLabel22.Location = new System.Drawing.Point(508, 196);
            this.uLabel22.Name = "uLabel22";
            this.uLabel22.Size = new System.Drawing.Size(80, 20);
            this.uLabel22.TabIndex = 24;
            this.uLabel22.Text = "ultraLabel12";
            // 
            // uTextResultUserName
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextResultUserName.Appearance = appearance28;
            this.uTextResultUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextResultUserName.Location = new System.Drawing.Point(696, 172);
            this.uTextResultUserName.Name = "uTextResultUserName";
            this.uTextResultUserName.ReadOnly = true;
            this.uTextResultUserName.Size = new System.Drawing.Size(108, 21);
            this.uTextResultUserName.TabIndex = 23;
            // 
            // uLabel21
            // 
            this.uLabel21.Location = new System.Drawing.Point(508, 172);
            this.uLabel21.Name = "uLabel21";
            this.uLabel21.Size = new System.Drawing.Size(80, 20);
            this.uLabel21.TabIndex = 21;
            this.uLabel21.Text = "ultraLabel13";
            // 
            // uLabel17
            // 
            this.uLabel17.Location = new System.Drawing.Point(12, 124);
            this.uLabel17.Name = "uLabel17";
            this.uLabel17.Size = new System.Drawing.Size(92, 44);
            this.uLabel17.TabIndex = 20;
            this.uLabel17.Text = "ultraLabel8";
            // 
            // uTextPreventionDesc
            // 
            this.uTextPreventionDesc.Location = new System.Drawing.Point(108, 124);
            this.uTextPreventionDesc.Multiline = true;
            this.uTextPreventionDesc.Name = "uTextPreventionDesc";
            this.uTextPreventionDesc.Size = new System.Drawing.Size(392, 44);
            this.uTextPreventionDesc.TabIndex = 19;
            // 
            // uDatePreventionDate
            // 
            this.uDatePreventionDate.Location = new System.Drawing.Point(592, 148);
            this.uDatePreventionDate.Name = "uDatePreventionDate";
            this.uDatePreventionDate.Size = new System.Drawing.Size(100, 21);
            this.uDatePreventionDate.TabIndex = 18;
            // 
            // uLabel19
            // 
            this.uLabel19.Location = new System.Drawing.Point(508, 148);
            this.uLabel19.Name = "uLabel19";
            this.uLabel19.Size = new System.Drawing.Size(80, 20);
            this.uLabel19.TabIndex = 17;
            this.uLabel19.Text = "ultraLabel9";
            // 
            // uTextPreventionUserName
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPreventionUserName.Appearance = appearance29;
            this.uTextPreventionUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPreventionUserName.Location = new System.Drawing.Point(696, 124);
            this.uTextPreventionUserName.Name = "uTextPreventionUserName";
            this.uTextPreventionUserName.ReadOnly = true;
            this.uTextPreventionUserName.Size = new System.Drawing.Size(108, 21);
            this.uTextPreventionUserName.TabIndex = 16;
            // 
            // uLabel18
            // 
            this.uLabel18.Location = new System.Drawing.Point(508, 124);
            this.uLabel18.Name = "uLabel18";
            this.uLabel18.Size = new System.Drawing.Size(80, 20);
            this.uLabel18.TabIndex = 14;
            this.uLabel18.Text = "ultraLabel10";
            // 
            // uLabel14
            // 
            this.uLabel14.Location = new System.Drawing.Point(12, 76);
            this.uLabel14.Name = "uLabel14";
            this.uLabel14.Size = new System.Drawing.Size(92, 44);
            this.uLabel14.TabIndex = 13;
            this.uLabel14.Text = "ultraLabel5";
            // 
            // uTextCauseDesc
            // 
            this.uTextCauseDesc.Location = new System.Drawing.Point(108, 76);
            this.uTextCauseDesc.Multiline = true;
            this.uTextCauseDesc.Name = "uTextCauseDesc";
            this.uTextCauseDesc.Size = new System.Drawing.Size(392, 44);
            this.uTextCauseDesc.TabIndex = 12;
            // 
            // uDateCauseDate
            // 
            this.uDateCauseDate.Location = new System.Drawing.Point(592, 100);
            this.uDateCauseDate.Name = "uDateCauseDate";
            this.uDateCauseDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCauseDate.TabIndex = 11;
            // 
            // uLabel16
            // 
            this.uLabel16.Location = new System.Drawing.Point(508, 100);
            this.uLabel16.Name = "uLabel16";
            this.uLabel16.Size = new System.Drawing.Size(80, 20);
            this.uLabel16.TabIndex = 10;
            this.uLabel16.Text = "ultraLabel6";
            // 
            // uTextCauseUserName
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCauseUserName.Appearance = appearance31;
            this.uTextCauseUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCauseUserName.Location = new System.Drawing.Point(696, 76);
            this.uTextCauseUserName.Name = "uTextCauseUserName";
            this.uTextCauseUserName.ReadOnly = true;
            this.uTextCauseUserName.Size = new System.Drawing.Size(108, 21);
            this.uTextCauseUserName.TabIndex = 9;
            // 
            // uLabel15
            // 
            this.uLabel15.Location = new System.Drawing.Point(508, 76);
            this.uLabel15.Name = "uLabel15";
            this.uLabel15.Size = new System.Drawing.Size(80, 20);
            this.uLabel15.TabIndex = 7;
            this.uLabel15.Text = "ultraLabel7";
            // 
            // uLabel11
            // 
            this.uLabel11.Location = new System.Drawing.Point(12, 28);
            this.uLabel11.Name = "uLabel11";
            this.uLabel11.Size = new System.Drawing.Size(92, 44);
            this.uLabel11.TabIndex = 6;
            this.uLabel11.Text = "uLabel4";
            // 
            // uTextProblemDesc
            // 
            this.uTextProblemDesc.Location = new System.Drawing.Point(108, 28);
            this.uTextProblemDesc.Multiline = true;
            this.uTextProblemDesc.Name = "uTextProblemDesc";
            this.uTextProblemDesc.Size = new System.Drawing.Size(392, 44);
            this.uTextProblemDesc.TabIndex = 5;
            // 
            // uDateProblemDate
            // 
            this.uDateProblemDate.Location = new System.Drawing.Point(592, 52);
            this.uDateProblemDate.Name = "uDateProblemDate";
            this.uDateProblemDate.Size = new System.Drawing.Size(100, 21);
            this.uDateProblemDate.TabIndex = 4;
            // 
            // uLabel13
            // 
            this.uLabel13.Location = new System.Drawing.Point(508, 52);
            this.uLabel13.Name = "uLabel13";
            this.uLabel13.Size = new System.Drawing.Size(80, 20);
            this.uLabel13.TabIndex = 3;
            this.uLabel13.Text = "uLabel6";
            // 
            // uTextProblemUserName
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProblemUserName.Appearance = appearance33;
            this.uTextProblemUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProblemUserName.Location = new System.Drawing.Point(696, 28);
            this.uTextProblemUserName.Name = "uTextProblemUserName";
            this.uTextProblemUserName.ReadOnly = true;
            this.uTextProblemUserName.Size = new System.Drawing.Size(108, 21);
            this.uTextProblemUserName.TabIndex = 2;
            // 
            // uLabel12
            // 
            this.uLabel12.Location = new System.Drawing.Point(508, 28);
            this.uLabel12.Name = "uLabel12";
            this.uLabel12.Size = new System.Drawing.Size(80, 20);
            this.uLabel12.TabIndex = 0;
            this.uLabel12.Text = "uLabel5";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uGridValueData);
            this.uGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBox1.Location = new System.Drawing.Point(3, 0);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(242, 241);
            this.uGroupBox1.TabIndex = 42;
            // 
            // uGridValueData
            // 
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            appearance48.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridValueData.DisplayLayout.Appearance = appearance48;
            this.uGridValueData.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridValueData.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance49.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance49.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance49.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridValueData.DisplayLayout.GroupByBox.Appearance = appearance49;
            appearance50.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridValueData.DisplayLayout.GroupByBox.BandLabelAppearance = appearance50;
            this.uGridValueData.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance51.BackColor2 = System.Drawing.SystemColors.Control;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridValueData.DisplayLayout.GroupByBox.PromptAppearance = appearance51;
            this.uGridValueData.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridValueData.DisplayLayout.MaxRowScrollRegions = 1;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridValueData.DisplayLayout.Override.ActiveCellAppearance = appearance52;
            appearance32.BackColor = System.Drawing.SystemColors.Highlight;
            appearance32.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridValueData.DisplayLayout.Override.ActiveRowAppearance = appearance32;
            this.uGridValueData.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridValueData.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            this.uGridValueData.DisplayLayout.Override.CardAreaAppearance = appearance54;
            appearance55.BorderColor = System.Drawing.Color.Silver;
            appearance55.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridValueData.DisplayLayout.Override.CellAppearance = appearance55;
            this.uGridValueData.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridValueData.DisplayLayout.Override.CellPadding = 0;
            appearance56.BackColor = System.Drawing.SystemColors.Control;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridValueData.DisplayLayout.Override.GroupByRowAppearance = appearance56;
            appearance57.TextHAlignAsString = "Left";
            this.uGridValueData.DisplayLayout.Override.HeaderAppearance = appearance57;
            this.uGridValueData.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridValueData.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.BorderColor = System.Drawing.Color.Silver;
            this.uGridValueData.DisplayLayout.Override.RowAppearance = appearance58;
            this.uGridValueData.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridValueData.DisplayLayout.Override.TemplateAddRowAppearance = appearance59;
            this.uGridValueData.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridValueData.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridValueData.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridValueData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridValueData.Location = new System.Drawing.Point(3, 0);
            this.uGridValueData.Name = "uGridValueData";
            this.uGridValueData.Size = new System.Drawing.Size(236, 238);
            this.uGridValueData.TabIndex = 0;
            this.uGridValueData.Text = "ultraGrid1";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uChartX);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 208);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1064, 208);
            this.ultraGroupBox1.TabIndex = 44;
            // 
            //			'UltraChart' properties's serialization: Since 'ChartType' changes the way axes look,
            //			'ChartType' must be persisted ahead of any Axes change made in design time.
            //		
            this.uChartX.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.Composite;
            // 
            // uChartX
            // 
            this.uChartX.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement2.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement2.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartX.Axis.PE = paintElement2;
            this.uChartX.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartX.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.X.Labels.SeriesLabels.FormatString = "";
            this.uChartX.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X.LineThickness = 1;
            this.uChartX.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X.MajorGridLines.Visible = true;
            this.uChartX.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X.MinorGridLines.Visible = false;
            this.uChartX.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.X.Visible = true;
            this.uChartX.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartX.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartX.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.X2.Labels.SeriesLabels.FormatString = "";
            this.uChartX.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartX.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X2.Labels.Visible = false;
            this.uChartX.Axis.X2.LineThickness = 1;
            this.uChartX.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X2.MajorGridLines.Visible = true;
            this.uChartX.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X2.MinorGridLines.Visible = false;
            this.uChartX.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.X2.Visible = false;
            this.uChartX.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartX.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartX.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Y.Labels.SeriesLabels.FormatString = "";
            this.uChartX.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartX.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y.LineThickness = 1;
            this.uChartX.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y.MajorGridLines.Visible = true;
            this.uChartX.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y.MinorGridLines.Visible = false;
            this.uChartX.Axis.Y.TickmarkInterval = 10;
            this.uChartX.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Y.Visible = true;
            this.uChartX.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartX.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.FormatString = "";
            this.uChartX.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y2.Labels.Visible = false;
            this.uChartX.Axis.Y2.LineThickness = 1;
            this.uChartX.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartX.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartX.Axis.Y2.TickmarkInterval = 10;
            this.uChartX.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Y2.Visible = false;
            this.uChartX.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Z.Labels.ItemFormatString = "";
            this.uChartX.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z.LineThickness = 1;
            this.uChartX.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z.MajorGridLines.Visible = true;
            this.uChartX.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z.MinorGridLines.Visible = false;
            this.uChartX.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Z.Visible = false;
            this.uChartX.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartX.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z2.Labels.Visible = false;
            this.uChartX.Axis.Z2.LineThickness = 1;
            this.uChartX.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartX.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartX.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Z2.Visible = false;
            this.uChartX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartX.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartX.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartX.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartX.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uChartX.Effects.Effects.Add(gradientEffect2);
            this.uChartX.Location = new System.Drawing.Point(3, 0);
            this.uChartX.Name = "uChartX";
            this.uChartX.Size = new System.Drawing.Size(1058, 205);
            this.uChartX.TabIndex = 0;
            this.uChartX.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartX.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // frmINSZ0012
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridSPCNList);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0012";
            this.Load += new System.EventHandler(this.frmINSZ0012_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0012_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0012_FormClosing);
            this.Resize += new System.EventHandler(this.frmINSZ0012_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchWriteFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            this.uGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLCL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCpk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUSL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLSL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAvg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWrieteTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGeneration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStackSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqItemSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqLotSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckLowerTrend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUpperTrend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckLowerRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckUpperRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckOCP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSPCNList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPreventionUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCauseUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblemUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateResultDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextResultUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPreventionDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDatePreventionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPreventionUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCauseDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCauseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCauseUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblemDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateProblemDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProblemUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridValueData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uChartX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchWriteToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchWriteFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRegisterDate;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessName;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProductSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSPCUnfitType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdDev;
        private Infragistics.Win.Misc.UltraLabel uLabelstdDeviation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAvg;
        private Infragistics.Win.Misc.UltraLabel uLabelAvg;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemName;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItem;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCycle;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckLowerTrend;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUpperTrend;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckLowerRun;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckUpperRun;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckOCP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRegisterDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqItemSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqLotSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelRegister;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecRange;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCompleteFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCpk;
        private Infragistics.Win.Misc.UltraLabel uLabelCpk;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCp;
        private Infragistics.Win.Misc.UltraLabel uLabelCp;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMin;
        private Infragistics.Win.Misc.UltraLabel uLabelMin;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMax;
        private Infragistics.Win.Misc.UltraLabel uLabelMax;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRange;
        private Infragistics.Win.Misc.UltraLabel uLabelRange;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUSL;
        private Infragistics.Win.Misc.UltraLabel uLabelUSL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLSL;
        private Infragistics.Win.Misc.UltraLabel uLabelLSL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUCL;
        private Infragistics.Win.Misc.UltraLabel uLabelUCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLCL;
        private Infragistics.Win.Misc.UltraLabel uLabelLCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCL;
        private Infragistics.Win.Misc.UltraLabel uLabelCL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStackSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGeneration;
        private Infragistics.Win.Misc.UltraLabel uLabelPACKAGE;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWrieteTime;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraLabel uLabelCopleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextResultUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPreventionUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCauseUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProblemUserID;
        private Infragistics.Win.Misc.UltraLabel uLabel20;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextResultDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateResultDate;
        private Infragistics.Win.Misc.UltraLabel uLabel22;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextResultUserName;
        private Infragistics.Win.Misc.UltraLabel uLabel21;
        private Infragistics.Win.Misc.UltraLabel uLabel17;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPreventionDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDatePreventionDate;
        private Infragistics.Win.Misc.UltraLabel uLabel19;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPreventionUserName;
        private Infragistics.Win.Misc.UltraLabel uLabel18;
        private Infragistics.Win.Misc.UltraLabel uLabel14;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCauseDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCauseDate;
        private Infragistics.Win.Misc.UltraLabel uLabel16;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCauseUserName;
        private Infragistics.Win.Misc.UltraLabel uLabel15;
        private Infragistics.Win.Misc.UltraLabel uLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProblemDesc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateProblemDate;
        private Infragistics.Win.Misc.UltraLabel uLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProblemUserName;
        private Infragistics.Win.Misc.UltraLabel uLabel12;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSPCNList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinChart.UltraChart uChartX;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridValueData;
    }
}