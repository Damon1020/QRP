﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공정검사관리                                          */
/* 프로그램ID   : frmINS0008.cs                                         */
/* 프로그램명   : 공정검사등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;

namespace QRPINS.UI
{
    public partial class frmINS0008_S : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
         
        private string m_strProcessCode;
        private string m_strEquipCode;

        private bool m_bolSaveFlag = false;

        public string ProcessCode
        {
            get { return m_strProcessCode; }
            set { m_strProcessCode = value; }
        }

        public string EquipCode
        {
            get { return m_strEquipCode; }
            set { m_strEquipCode = value; }
        }

        public frmINS0008_S()
        {
            InitializeComponent();
        }

        private void frmINS0008_Activated(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            
            // 盛辉 or 邹美娟
            if (m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_resSys.GetString("SYS_USERID").Equals("20500106") 
                || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002184")
                || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("20500176")
                || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS000829") || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS001319")
                || m_resSys.GetString("SYS_USERID").ToUpper().Equals("PS002481"))
                m_bolSaveFlag = true;

            QRPBrowser InitToolBar = new QRPBrowser();
            InitToolBar.mfActiveToolBar(this.ParentForm, true, m_bolSaveFlag, true, false, true, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINS0008_Resize(object sender, EventArgs e)
        {
            try
            {
                ////if (this.Width > 1070)
                ////{
                ////    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                ////}
                ////else
                ////{
                ////    //uGroupBoxContentsArea.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
                ////    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                ////}

                ////if (this.Width > 1070 && this.Height > 850)
                ////{
                ////    this.uGridHeader.Dock = DockStyle.Fill;
                ////}
                ////else
                ////{
                ////    this.uGridHeader.Dock = DockStyle.None;
                ////    this.uGridHeader.Height = 701;
                ////}
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINS0008_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;

            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("공정검사조회", m_SysRes.GetString("SYS_FONTNAME"), 12);
            
            // 컨트롤 초기화
            InitEvent();
            SetToolAuth();
            InitTab();
            InitButton();
            InitComboBox();
            InitGrid();
            InitLabel();
            InitEtc();
            SetChartClear();

            //this.uComboSearchPlant.Value = m_SysRes.GetString("SYS_PLANTCODE");
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            // 盛辉 or 邹美娟
            if (m_SysRes.GetString("SYS_USERID").ToUpper().Equals("PS001315") || m_SysRes.GetString("SYS_USERID").Equals("20500106") 
                || m_SysRes.GetString("SYS_USERID").ToUpper().Equals("PS002478") || m_SysRes.GetString("SYS_USERID").ToUpper().Equals("PS002184")
                || m_SysRes.GetString("SYS_USERID").ToUpper().Equals("PS001403") || m_SysRes.GetString("SYS_USERID").ToUpper().Equals("20500176")
                || m_SysRes.GetString("SYS_USERID").ToUpper().Equals("PS000829") || m_SysRes.GetString("SYS_USERID").ToUpper().Equals("PS001319")
                || m_SysRes.GetString("SYS_USERID").ToUpper().Equals("PS002481"))
                m_bolSaveFlag = true;
        }

        private void frmINS0008_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method

        private void InitEvent()
        {
            ////this.uTextSearchEquipCode.ValueChanged += new System.EventHandler(this.uTextSearchEquipCode_ValueChanged);
            ////this.uTextSearchEquipCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchEquipCode_KeyDown);
            ////this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchEquipCode_EditorButtonClick);
            ////this.uTextSearchInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ultraTextEditor2_KeyDown);
            ////this.uTextSearchInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.ultraTextEditor2_EditorButtonClick);
            ////this.uComboSearchDetailProcType.ValueChanged += new System.EventHandler(this.uComboSearchDetailProcType_ValueChanged);
            ////this.uTextSearchProductCode.Click += new System.EventHandler(this.uTextSearchProductCode_Click);
            ////this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            ////this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            ////this.uButtonTrackIn.Click += new System.EventHandler(this.uButtonTrackIn_Click);
            ////this.uTextInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextInspectUserID_KeyDown);
            ////this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectUserID_EditorButtonClick);
            ////this.uCheckCompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedValueChanged);
            ////this.uTextLotNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLotNo_KeyDown);
            ////this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridHeader_DoubleClickRow);
            ////this.uGridFault.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridFault_DoubleClickRow);
            
            
            this.uButtonDelete2.Click += new EventHandler(uButtonDelete2_Click);
            this.uButtonTrackIn.Click += new EventHandler(uButtonTrackIn_Click);
            this.uCheckCompleteFlag.CheckedValueChanged += new EventHandler(uCheckCompleteFlag_CheckedValueChanged);
            this.uComboSearchDetailProcType.ValueChanged += new EventHandler(uComboSearchDetailProcType_ValueChanged);
            this.uComboSearchPlant.ValueChanged += new EventHandler(uComboSearchPlant_ValueChanged);
            this.uDateInspectTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(uDateInspectTime_EditorSpinButtonClick);
            this.uGridFault.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridFault_AfterCellUpdate);
            this.uGridFault.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridFault_DoubleClickRow);
            this.uGridFaultData.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridFaultData_AfterCellUpdate);
            this.uGridFaultData.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(uGridFaultData_BeforeCellListDropDown);
            this.uGridFaultData.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridFaultData_ClickCellButton);
            this.uGridFaultData.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(uGridFaultData_DoubleClickCell);
            this.uGridFaultData.KeyDown += new KeyEventHandler(uGridFaultData_KeyDown);
            this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridHeader_DoubleClickRow);
            this.uGridItem.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridItem_DoubleClickRow);
            this.uGridItem.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGridItem_InitializeRow);
            this.uGridSampling.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridSampling_AfterCellUpdate);
            this.uGridSampling.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridSampling_DoubleClickRow);
            this.uGridSampling.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGridSampling_InitializeRow);
            this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            this.uTextSearchInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ultraTextEditor2_KeyDown);
            this.uTextSearchInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.ultraTextEditor2_EditorButtonClick);
            this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextInspectUserID_EditorButtonClick);
            this.uTextInspectUserID.KeyDown += new KeyEventHandler(uTextInspectUserID_KeyDown);
            this.uTextLotNo.KeyDown += new KeyEventHandler(uTextLotNo_KeyDown);
            this.uTextSearchEquipCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSearchEquipCode_EditorButtonClick);
            this.uTextSearchEquipCode.KeyDown += new KeyEventHandler(uTextSearchEquipCode_KeyDown);
            this.uTextSearchEquipCode.ValueChanged += new EventHandler(uTextSearchEquipCode_ValueChanged);
            this.uTextSearchProductCode.Click += new EventHandler(uTextSearchProductCode_Click);
        }

        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                // 최대길이 설정
                this.uTextSearchProductCode.MaxLength = 20;
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextLotNo.MaxLength = 50;
                this.uTextEtcDesc.MaxLength = 100;

                this.uOptionPassFailFlag.Enabled = false;

                //this.uDateSearchFromInspectDate.Value = DateTime.Now.AddDays(-7);
                this.uDateSearchFromInspectDate.MaskInput = "yyyy-mm-dd hh:mm:ss";
                this.uDateSearchToInspectDate.MaskInput = "yyyy-mm-dd hh:mm:ss";

                this.uDateSearchFromInspectDate.Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd 22:00:00");
                this.uDateSearchToInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd 21:59:59");

                this.uTextLotSize.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                this.uTextSearchEquipCode.ButtonsRight[0].Appearance.ImageHAlign = Infragistics.Win.HAlign.Center;
                this.uTextSearchEquipCode.ButtonsRight[0].Appearance.ImageVAlign = Infragistics.Win.VAlign.Middle;
                this.uTextSearchEquipCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;

                //this.uOptionPassFailFlag.Appearance.BackColorDisabled = Color.White;
                //this.uOptionPassFailFlag.Appearance.ForeColorDisabled = Color.Black;
                //this.uOptionPassFailFlag.Items[0].Appearance.BackColorDisabled = Color.White;
                //this.uOptionPassFailFlag.Items[1].Appearance.BackColorDisabled = Color.White;
                //this.uOptionPassFailFlag.Items[0].Appearance.ForeColorDisabled = Color.Black;
                //this.uOptionPassFailFlag.Items[1].Appearance.ForeColorDisabled = Color.Red;

                //Size size = new Size(1070, 630);
                //this.uGroupBoxContentsArea.MaximumSize.Height = size;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialCode, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInsType, "공정검사구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "검사일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDetailProcType, "공정Type", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomerProductSpec, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectUser, "검사자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProduct, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWorkProcess, "작업공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProcInspectType, "공정검사구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLotSize, "Lot수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquip, "설비번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessHoldState, "Hold상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotProcessState, "Lot상태", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelCustomerProductCode, "고객제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWokrUser, "작업자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelNowProcess, "현재공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStackSeq, "Stack", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelInspectDate, "검사일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectUser, "검사자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPassFailFlag, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelAttachFile1, "下载文件", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonDelete2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonTrackIn.Enabled = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlantCode, false, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);


                // 공정검사구분 코드 (공통코드에서 가져오기)
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode"); // 2
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode(); // 3
                brwChannel.mfCredentials(clsComCode); // 4

                DataTable dtProcInspectGubun = clsComCode.mfReadCommonCode("C0020", m_resSys.GetString("SYS_LANG")); // 5

                wCombo.mfSetComboEditor(this.uComboSearchProcInspectType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                   , "ComCode", "ComCodeName", dtProcInspectGubun);

                wCombo.mfSetComboEditor(this.uComboProcInspectType, false, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtProcInspectGubun);


                //검색조건 - 고객 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer"); // 2
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer(); // 3
                brwChannel.mfCredentials(clsCustomer); // 4
                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                   , "CustomerCode", "CustomerName", dtCustomer);

                //검색조건 - Package 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product"); // 2
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product(); // 3
                brwChannel.mfCredentials(clsPackage); // 4
                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                   , "Package", "ComboName", dtPackage);

                //검색조건 - 공정Type
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process"); // 2
                QRPMAS.BL.MASPRC.Process clsProc = new QRPMAS.BL.MASPRC.Process(); // 3
                brwChannel.mfCredentials(clsProc); // 4
                DataTable dtProc = clsProc.mfReadProcessDetailProcessOperationType(m_resSys.GetString("SYS_PLANTCODE"));
                wCombo.mfSetComboEditor(this.uComboSearchDetailProcType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                   , "DetailProcessOperationType", "ComboName", dtProc);

                this.uComboPlantCode.ReadOnly = true;
                this.uComboProcInspectType.ReadOnly = true;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetSearchComboProcess()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strDetailProcType = this.uComboSearchDetailProcType.Value.ToString();

                string strLang = m_resSys.GetString("SYS_LANG");
                string strCode = "";
                string strName = "";
                if (strLang.Equals("KOR"))
                {
                    strCode = "공정코드"; strName = "공정명";
                }
                else if (strLang.Equals("CHN"))
                {
                    strCode = "工程编号"; strName = "工程名";
                }
                else
                { strCode = "공정코드"; strName = "공정명"; }


                //검색조건 - 공정코드
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsAriseProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsAriseProcess);

                DataTable dtProcess = clsAriseProcess.mfReadProcessWithDetailProcessOperationType(strPlantCode, strDetailProcType, m_resSys.GetString("SYS_LANG"));
                WinComboGrid combo = new WinComboGrid();
                combo.mfInitGeneralComboGrid(this.uComboSearchProcessCode, false, false, true, true, "ProcessCode", "ProcessName", ""
                                            , Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                                            , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, Infragistics.Win.DefaultableBoolean.False
                                            , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                                            , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                                            , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                this.uComboSearchProcessCode.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;
                this.uComboSearchProcessCode.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.AllRowsInBand;

                combo.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessCode", strCode, false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                combo.mfSetComboGridColumn(this.uComboSearchProcessCode, 0, "ProcessName", strName, false, 100, false, Infragistics.Win.DefaultableBoolean.True
                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle
                                        , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "");

                this.uComboSearchProcessCode.DataSource = dtProcess;
                this.uComboSearchProcessCode.DataBind();

            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 List
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridHeader, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ReqNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "CUSTOMERPRODUCTSPEC", "고객제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "DETAILPROCESSOPERATIONTYPE", "공정Type", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "WorkProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "InspectDate", "검사일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "InspectUserName", "검사자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridHeader, 0, "InspectDate", "검사일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ProcInspectType", "공정검사구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "PassFailFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MESHoldTFlag", "MESHold전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MESSPCNTFlag", "MESSPCN전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ProductCode", "제품코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ProductName", "제품명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "CompleteFlag", "완료여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MESTrackInTFlag", "MESTrackIn전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MESTrackOutTFlag", "MESTrackOut전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "EtcDesc", "备注", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                // Set PontSize
                this.uGridHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 기존그리드 설정 주석처리
                /*
                // 검사결과 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridItem, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridItem, 0, "ReqItemSeq", "항목순번", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessCode", "공정코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessName", "공정", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectGroupName", "검사분류", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectTypeName", "검사유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "StackSeq", "StackSeq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Generation", "Generation", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessSampleSize", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0.0");
                

                //     wGrid.mfSetGridColumn(this.uGridItem, 0, "SampleSize", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                //         , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //         , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0");

                //     wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessSampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //         , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //         , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //     wGrid.mfSetGridColumn(this.uGrid1, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //         , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //         , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "MaxValue", "Max", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "StdDev", "StdDev.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "OCPCount", "OCPCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperRunCount", "UpperRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerRunCount", "LowerRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperTrendCount", "UpperTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerTrendCount", "LowerTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                //wGrid.mfSetGridColumn(this.uGridItem, 0, "CycleCount", "CycleCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                //// 샘플링검사 그리드

                //InitSampleGrid();
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSampling, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectIngFlag", "검사여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectTypeName", "유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SampleSize", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessSampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");
                 * */
                #endregion

                #region 검사결과 그리드
                // 검사결과 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridItem, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectIngFlag", "검사진행여부", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ReqItemSeq", "항목순번", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Seq", "순번", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 60, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessCode", "공정코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessName", "공정", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectGroupName", "검사분류", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectTypeName", "검사유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "StackSeq", "StackSeq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Generation", "Generation", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "FaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SampleSize", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "ProcessSampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //     wGrid.mfSetGridColumn(this.uGrid1, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //         , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //         , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                ////////////////////////////////////////////////////////////////////////

                wGrid.mfSetGridColumn(this.uGridItem, 0, "MaxValue", "Max", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "StdDev", "StdDev.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "OCPCount", "OCPCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperRunCount", "UpperRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerRunCount", "LowerRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UpperTrendCount", "UpperTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LowerTrendCount", "LowerTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "LCL", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "CL", "CL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "UCL", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridItem, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");
                #endregion

                #region Sampling Grid
                //// 샘플링검사 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridSampling, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectIngFlag", "검사여부", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectTypeName", "유형", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemName", "검사항목", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectResultFlag", "검사결과", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessSampleSize", "검사수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "FaultQty", "불량수", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SampleSize", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UnitCode", "단위", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerSpec", "LSL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperSpec", "USL", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                ///////////////하단 Hidden 컬럼
                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LCL", "LCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "CL", "CL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UCL", "UCL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "MaxValue", "Max", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "StdDev", "StdDev.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "OCPCount", "OCPCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperRunCount", "UpperRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerRunCount", "LowerRunCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperTrendCount", "UpperTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerTrendCount", "LowerTrendCount", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");


                // 그리드 헤더 체크박스 없애기
                this.uGridSampling.DisplayLayout.Bands[0].Columns["InspectIngFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                #endregion

                #region FaultGrid
                // 불량유형 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridFault, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                //wGrid.mfSetGridColumn(this.uGridFault, 0, "ReqLotSeq", "Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "1");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                //wGrid.mfSetGridColumn(this.uGridFault, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectTypeName", "유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 20
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGrid5, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "SpecUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "ProcessSampleSize", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFault, 0, "FaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                #endregion

                #region FatulData Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridFaultData, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ReqLotSeq", "Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "1");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ReqInspectSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "QCNFlag", "QCN대상여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "P_ITRFlag", "ITR대상여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "InspectQty", "검사수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:15.0}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.0}", "0.0");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "ExpectProcessCode", "예상공정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "CauseEquipCode", "원인설비", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "WorkUserID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "WorkUserName", "작업자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 90, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFaultData, 0, "CheckFaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                this.uGridFaultData.DisplayLayout.Bands[0].Columns["P_ITRFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                #endregion

                // Set FontSize
                this.uGridItem.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridItem.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridFault.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridFault.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridFaultData.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridFaultData.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // DropDown 설정
                // 완료여부 DropDown 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtComplete = clsCom.mfReadCommonCode("C0056", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridHeader, 0, "CompleteFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComplete);

                // 단위
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();

                wGrid.mfSetGridColumnValueList(this.uGridItem, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridFault, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                // SpecRange
                DataTable dtCom = clsCom.mfReadCommonCode("C0032", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridItem, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridFault, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                // 검사결과
                dtCom = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridHeader, 0, "PassFailFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridItem, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
                wGrid.mfSetGridColumnValueList(this.uGridFault, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

                // 그리드 수정불가 상태로
                this.uGridHeader.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridFault.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridFaultData.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridItem.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                ///////////////////////////////////////////////////////////////////////////////////////////////////
                // 2012.04.17 RowData수정하기 위해 수정불가 주석처리
                //this.uGridSampling.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                ///////////////////////////////////////////////////////////////////////////////////////////////////

                this.uGridSampling.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGridSampling.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGridSampling.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                Size size = new Size(1070, 90);
                this.uGridHeader.MinimumSize = size;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                QRPINS.BL.INSPRC.ProcInspectReqH clsReqH = new QRPINS.BL.INSPRC.ProcInspectReqH();
                brwChannel.mfCredentials(clsReqH);

                // 매개변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strProductCode = this.uTextSearchProductCode.Text;
                string strProcInspectType = this.uComboSearchProcInspectType.Value.ToString();
                string strLotNo = this.uTextSearchLotNo.Text;
                string strFromInspectDate = Convert.ToDateTime(this.uDateSearchFromInspectDate.Value).ToString("yyyy-MM-dd HH:mm:ss");
                string strToInspectDate = Convert.ToDateTime(this.uDateSearchToInspectDate.Value).ToString("yyyy-MM-dd HH:mm:ss");

                string strCustomerCode = this.uComboSearchCustomer.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strDetailProcType = this.uComboSearchDetailProcType.Value.ToString();

                string strProcessCode = "";
                if (this.uComboSearchProcessCode.SelectedRow != null)
                    strProcessCode = this.uComboSearchProcessCode.SelectedRow.Cells["ProcessCode"].Value.ToString();

                string strInspectUserID = this.uTextSearchInspectUserID.Text;
                string strEquipCode = this.uTextSearchEquipCode.Text;

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtSearch = clsReqH.mfReadINSProcInspectReqH_PSTS(strPlantCode, strProductCode, strProcInspectType, strLotNo
                                                                    , strFromInspectDate, strToInspectDate
                                                                    , strCustomerCode, strPackage, strDetailProcType, strProcessCode
                                                                    , uTextSearchCustomerProductSpec.Text, strInspectUserID,strEquipCode
                                                                    , m_resSys.GetString("SYS_LANG"));

                this.uGridHeader.DataSource = dtSearch;
                this.uGridHeader.DataBind();

                //for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                //{
                //    if ((this.uGridHeader.Rows[i].Cells["CompleteFlag"].Value.ToString().Equals("완료") && this.uGridHeader.Rows[i].Cells["MESHoldTFlag"].Value.ToString().Equals("F")) ||
                //        (this.uGridHeader.Rows[i].Cells["TrackInFlag"].Value.ToString().Equals("T") && this.uGridHeader.Rows[i].Cells["MESTrackInTFlag"].Value.ToString().Equals("F")) ||
                //        (this.uGridHeader.Rows[i].Cells["PassFailFlag"].Value.ToString().Equals("NG") && this.uGridHeader.Rows[i].Cells["MESHoldTFlag"].Value.ToString().Equals("F")) ||
                //        (Convert.ToInt32(this.uGridHeader.Rows[i].Cells["OCPCount"].Value) > 0 && this.uGridHeader.Rows[i].Cells["MESSPCNTFlag"].Value.ToString().Equals("F")))
                //    {
                //        this.uGridHeader.Rows[i].Appearance.BackColor = Color.Salmon;
                //    }
                //}

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (uGridHeader.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridHeader, 0);
                }

                // 상세정보창 접힘상태로

                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001047", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uTextReqNo.Text.Equals(string.Empty) || this.uTextReqSeq.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000396", Infragistics.Win.HAlign.Center);

                    //mfSearch();
                    return;
                }
                else
                {

                    // 저장여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        ////// 저장정보 데이터 테이블 설정
                        ////DataTable dtHeader = Rtn_HeaderDataTable();
                        ////DataTable dtLot = Rtn_LotDataTable();
                        ////DataTable dtItem = Rtn_ItemDataTable();
                        ////DataSet dsResult = Rtn_ResultDataSet();
                        ////DataTable dtFault = Rtn_FaultDataTable();
                        ////DataTable dtCycle = Rtn_CycleDataTable();

                        ////// BL 연결
                        ////QRPBrowser brwChannel = new QRPBrowser();
                        ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                        ////QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                        ////brwChannel.mfCredentials(clsHeader);

                        DataTable dtItem = Rtn_ItemDataTable();
                        DataSet dsResult = Rtn_ResultDataSet();

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                        QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                        brwChannel.mfCredentials(clsItem);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        ////// Method 호출
                        ////string strErrRtn = clsHeader.mfSaveINSProcInspectReqH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                        ////                                                    , dtLot, dtItem, dsResult, dtFault, dtCycle);

                        string strErrRtn = clsItem.mfSaveINSProcInspectItem(dtItem, dsResult, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                                                                            , this.uTextReqNo.Text, this.uTextReqSeq.Text);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            // OUTPUT 값 저장
                            string strReqNo = ErrRtn.mfGetReturnValue(0);
                            string strReqSeq = ErrRtn.mfGetReturnValue(1);

                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG"))
                                                    , ErrRtn.ErrMessage,
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }

                #region 2012.04.17 RowData 수정/저장 요청에 의한주석처리
                /*
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "입력사항 확인", "저장할 정보가 없습니다", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uCheckCompleteFlag.Enabled == false && this.uCheckCompleteFlag.Checked)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "작성완료 확인", "작성완료된 정보는 수정할 수 없습니다", Infragistics.Win.HAlign.Center);

                    //mfSearch();
                    return;
                }
                else
                {
                    // 작성완료가 활성화 되어있는경우는 저장
                    if (this.uCheckCompleteFlag.Enabled)
                    {
                        // 필수입력사항 확인
                        if (this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "입력사항 확인", "공장을 선택해 주세요", Infragistics.Win.HAlign.Center);

                            this.uComboPlantCode.DropDown();
                            return;
                        }
                        else if (this.uComboProcInspectType.Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "입력사항 확인", "공정검사구분을 선택해 주세요", Infragistics.Win.HAlign.Center);

                            this.uComboProcInspectType.DropDown();
                            return;
                        }
                        else if (this.uTextLotNo.Text.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "입력사항 확인", "LotNo을 입력해 주세요", Infragistics.Win.HAlign.Center);

                            this.uTextLotNo.Focus();
                            return;
                        }
                        else
                        {
                            if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "확인창", "작성완료 확인", "작성완료 체크후 저장시 이후 수정할 수 없습니다",
                                                Infragistics.Win.HAlign.Right);
                            }
                            // 저장여부를 묻는다
                            if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "저장확인", "입력한 정보를 저장하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                            {
                                // 저장정보 데이터 테이블 설정
                                DataTable dtHeader = Rtn_HeaderDataTable();
                                DataTable dtLot = Rtn_LotDataTable();
                                DataTable dtItem = Rtn_ItemDataTable();
                                DataSet dsResult = Rtn_ResultDataSet();
                                DataTable dtFault = Rtn_FaultDataTable();
                                DataTable dtCycle = Rtn_CycleDataTable();

                                // BL 연결
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                                QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                                brwChannel.mfCredentials(clsHeader);

                                // 프로그래스 팝업창 생성
                                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                Thread t1 = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                // Method 호출
                                string strErrRtn = clsHeader.mfSaveINSProcInspectReqH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                                                                                    , dtLot, dtItem, dsResult, dtFault, dtCycle);

                                // 팦업창 Close
                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                TransErrRtn ErrRtn = new TransErrRtn();
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                // 처리결과에 따른 메세지 박스
                                if (ErrRtn.ErrNum == 0)
                                {
                                    // OUTPUT 값 저장

                                    string strReqNo = ErrRtn.mfGetReturnValue(0);
                                    string strReqSeq = ErrRtn.mfGetReturnValue(1);

                                    // 작성완료가 체크되어있는경우
                                    if (this.uCheckCompleteFlag.Checked)
                                    {
                                        //MES I/F 진행
                                        //SaveMESInterFace(strReqNo, strReqSeq);
                                        // QCN 자동이동
                                        //MoveQCN(strReqNo, strReqSeq);
                                    }
                                    else
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                            Infragistics.Win.HAlign.Right);

                                        // 리스트 갱신
                                        mfSearch();
                                    }
                                }
                                else
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                        Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                    else    // 작성완료 비활성화인경우는 MES I/F 만 수행
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "확인창", "저장처리결과", "작성완료가 된 정보입니다. MES I/F만 진행합니다.",
                                                        Infragistics.Win.HAlign.Right);

                        string strReqNo = this.uTextReqNo.Text;
                        string strReqSeq = this.uTextReqSeq.Text;

                        //SaveMESInterFace(strReqNo, strReqSeq);
                    }
                }
                 * */
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextReqNo.Text == "" || this.uTextReqSeq.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (!this.uTextCompleteCheck.Text.ToLower().Equals("false") && this.uCheckCompleteFlag.Checked)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000980", "M000978", Infragistics.Win.HAlign.Center);

                    return;
                }
                else
                {
                    // 삭제여부를 묻는다

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                        QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                        brwChannel.mfCredentials(clsHeader);

                        // 변수 설정
                        string strPlantCode = this.uComboPlantCode.Value.ToString();
                        string strReqNo = this.uTextReqNo.Text;
                        string strReqSeq = this.uTextReqSeq.Text;

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsHeader.mfDeleteINSProcInspectReqALL(strPlantCode, strReqNo, strReqSeq);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);
                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000638", strLang)
                                                        , ErrRtn.ErrMessage,
                                                        Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                try
                {
                    if (this.uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;
                    }
                    else
                    {
                        Clear();
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridHeader.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridHeader);
                    if (this.uGridItem.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridItem);
                    }

                    if (this.uGridSampling.Rows.Count > 0)
                        wGrid.mfDownLoadGridToExcel(this.uGridSampling);

                    if (this.uGridFaultData.Rows.Count > 0)
                        wGrid.mfDownLoadGridToExcel(this.uGridFaultData);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method

        #region 컬럼설정 메소드

        /// <summary>
        /// 지정된 Size만큼 컬럼추가하는 Method
        /// </summary>
        /// <param name="uGrid"> 그리드명 </param>
        /// <param name="intBandIndex"> Band인덱스 </param>
        /// <param name="strPoint">Point 컬럼</param>
        /// <param name="strSampleSizeColKey"> Size가 입력된 컬럼Key </param>
        /// <param name="strLastIndexColKey"> 생성된 컬럼보다 뒤에 위치해야할 컬럼들의 키가 저장된 배열 </param>
        private void CreateColumn(Infragistics.Win.UltraWinGrid.UltraGrid uGrid
                                , int intBandIndex
                                , String strPoint
                                , String strSampleSizeColKey
                                , String[] strLastIndexColKey
                                )
        {
            try
            {
                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                // SampleSize가 될 최대값 찾기
                int intSampleMax = 0;
                int intPoint = 0;
                int inttotMax = 0;

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value) * Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) > inttotMax)
                    {
                        inttotMax = Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value) * Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                        intSampleMax = Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value);
                        intPoint = Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    }
                }
                if (inttotMax > 99)
                    inttotMax = 99;

                // 상세 SampleSize만큼 컬럼생성
                for (int i = 1; i <= inttotMax; i++)
                {
                    // 컬럼이 이미 존재하는경우(공정검사항목 테이블에 이미 저장되어 있는경우)
                    if (uGrid.DisplayLayout.Bands[intBandIndex].Columns.Exists(i.ToString()))
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Header.Caption = "X" + i.ToString();
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Hidden = false;
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].MaskInput = "{double:5.5}";
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Width = 70;
                    }
                    // 컬럼 신규생성(공정검사항목 테이블에 저장된 데이터가 아직없는경우)
                    else
                    {
                        wGrid.mfSetGridColumn(uGrid, intBandIndex, i.ToString(), "X" + i.ToString(), false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:5.5}", "0.0");
                    }
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].PromptChar = ' ';
                    uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                }

                // 추가된 컬럼보다 뒤에 있어야 하는 컬럼들이 있으면 뒤로 보냄
                if (strLastIndexColKey.Length > 0)
                {
                    int LastIndex = uGrid.DisplayLayout.Bands[intBandIndex].Columns.Count;
                    for (int i = 0; i < strLastIndexColKey.Length; i++)
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[strLastIndexColKey[i]].Header.VisiblePosition = LastIndex + i;
                    }
                }

                // Size 만큼의 Cell만 입력 가능하도록나머지는 편집불가처리
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    int ActivateNum = Convert.ToInt32(uGrid.Rows[i].Cells[strPoint].Value) * Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    if (ActivateNum > 99)
                        ActivateNum = 99;

                    for (int j = 1; j <= inttotMax; j++)
                    {
                        if (j <= ActivateNum)
                        {
                            uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            uGrid.Rows[i].Cells[j.ToString()].Hidden = false;
                        }
                        else
                        {
                            uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            uGrid.Rows[i].Cells[j.ToString()].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형에 맞게 Xn컬럼 설정
        /// </summary>
        private void SetSamplingGridColumn()
        {
            try
            {
                // 그리드 이벤트 헤제
                this.uGridSampling.EventManager.AllEventsEnabled = false;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 선택 DropDown 적용용 DataTable
                int intStart = this.uGridSampling.DisplayLayout.Bands[0].Columns["1"].Index;
                int intSampleSize = 0;

                // 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                MaskString.DataType = typeof(String);
                MaskString.MaxLength = 50;
                MaskString.MaskInput = "";

                Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                // 합/부 데이터 테이블
                DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                {
                    intSampleSize = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value) * Convert.ToInt32(this.uGridSampling.Rows[i].Cells["SampleSize"].Value);
                    if (intSampleSize > 99)
                        intSampleSize = 99;
                    // 계량
                    if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "1")
                    {
                        this.uGridSampling.Rows[i].Cells["FaultQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSampling.Rows[i].Cells["FaultQty"].Appearance.BackColor = Color.Gainsboro;
                        this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                            if (this.uGridSampling.Rows[i].Cells[j].Value == null)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // 계수
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // OK/NG
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "3")
                    {
                        //this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            wGrid.mfSetGridCellValueList(this.uGridSampling, i, this.uGridSampling.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "OK";
                            }
                        }
                    }
                    // 설명
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "4")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            this.uGridSampling.Rows[i].Cells[j].Editor = editorString;
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                    // 선택
                    else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "5")
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                        QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                        brwChannel.mfCredentials(clsItem);

                        // 검사분류/유형/항목/DataType 에 따른 선택항목 조회 Method 호출
                        string strInspectItemCode = this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString();

                        DataTable dtSelect = clsItem.mfReadINSProcInspectItem_DataTypeSelect(this.uComboPlantCode.Value.ToString()
                                                                                            , strInspectItemCode
                                                                                            , this.uTextProductCode.Text
                                                                                            , this.uTextWorkProcessCode.Text
                                                                                            , m_resSys.GetString("SYS_LANG"));

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
                            this.uGridSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            wGrid.mfSetGridCellValueList(this.uGridSampling, i, this.uGridSampling.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                            if (this.uGridSampling.Rows[i].Cells[j].Value == null)
                            {
                                this.uGridSampling.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                }
                this.uGridSampling.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 저장정보 반환 메소드

        /// <summary>
        /// 헤더정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_HeaderDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                brwChannel.mfCredentials(clsHeader);

                dtRtn = clsHeader.mfSetDataInfo();

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                drRow["ReqNo"] = this.uTextReqNo.Text;
                drRow["ReqSeq"] = this.uTextReqSeq.Text;
                drRow["ReqDeptCode"] = m_resSys.GetString("SYS_DEPTCODE");
                drRow["ReqUserID"] = m_resSys.GetString("SYS_USERID");
                drRow["ReqDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                drRow["InspectReqTypeCode"] = "1";
                drRow["EmergencyTypeCode"] = "1";
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_LotDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                dtRtn = clsLot.mfSetDataInfo();

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                drRow["ReqNo"] = this.uTextReqNo.Text;
                drRow["ReqSeq"] = this.uTextReqSeq.Text;
                drRow["ReqLotSeq"] = 1;
                drRow["ProductCode"] = this.uTextProductCode.Text;
                drRow["EquipCode"] = this.uTextEquipCode.Text;
                drRow["LotNo"] = this.uTextLotNo.Text;
                drRow["ProcInspectType"] = this.uComboProcInspectType.Value.ToString();
                drRow["ProcInspectPattern"] = "2";
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                drRow["LotSize"] = this.uTextLotSize.Text;
                drRow["InspectUserID"] = this.uTextInspectUserID.Text;
                drRow["InspectDate"] = Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd");
                drRow["InspectTime"] = Convert.ToDateTime(this.uDateInspectTime.Value).ToString("HH:mm:ss");
                if (this.uOptionPassFailFlag.CheckedIndex != -1)
                    drRow["PassFailFlag"] = this.uOptionPassFailFlag.Value.ToString();
                drRow["CompleteFlag"] = this.uCheckCompleteFlag.CheckedValue.ToString().Substring(0, 1);
                drRow["StackSeq"] = this.uTextStackSeq.Text;
                drRow["WorkUserID"] = this.uTextWorkUserID.Text;
                drRow["WorkProcessCode"] = this.uTextWorkProcessCode.Text;
                drRow["NowProcessCode"] = this.uTextNowProcessCode.Text;
                drRow["LotState"] = this.uTextLotProcessState.Text;
                drRow["HoldState"] = this.uTextProcessHoldState.Text;
                drRow["LossCheckFlag"] = this.uTextLossCheckFlag.Text;
                drRow["LossQty"] = Convert.ToInt32(this.uTextLossQty.Text);
                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item 정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_ItemDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                dtRtn = clsItem.mfSetDataInfo();

                if (this.uGridItem.Rows.Count > 0)
                {
                    this.uGridItem.ActiveCell = this.uGridItem.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridItem.Rows.Count; i++)
                    {
                        DataRow drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                        drRow["ReqLotSeq"] = 1;
                        drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                        drRow["Seq"] = this.uGridSampling.Rows[i].Cells["Seq"].Value.ToString();
                        drRow["ProcessCode"] = this.uGridSampling.Rows[i].Cells["ProcessCode"].Value.ToString();
                        drRow["InspectGroupCode"] = this.uGridSampling.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                        drRow["InspectTypeCode"] = this.uGridSampling.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                        drRow["InspectItemCode"] = this.uGridSampling.Rows[i].Cells["InspectItemCode"].Value.ToString();
                        if (Convert.ToBoolean(this.uGridSampling.Rows[i].Cells["InspectIngFlag"].Value))
                            drRow["InspectIngFlag"] = "T";
                        else
                            drRow["InspectIngFlag"] = "F";
                        drRow["StackSeq"] = this.uGridSampling.Rows[i].Cells["StackSeq"].Value.ToString();
                        drRow["Generation"] = this.uGridSampling.Rows[i].Cells["Generation"].Value.ToString();
                        drRow["InspectCondition"] = this.uGridSampling.Rows[i].Cells["InspectCondition"].Value.ToString();
                        drRow["Method"] = this.uGridSampling.Rows[i].Cells["Method"].Value.ToString();
                        drRow["UpperSpec"] = this.uGridSampling.Rows[i].Cells["UpperSpec"].Value.ToString();
                        drRow["LowerSpec"] = this.uGridSampling.Rows[i].Cells["LowerSpec"].Value.ToString();
                        drRow["SpecRange"] = this.uGridSampling.Rows[i].Cells["SpecRange"].Value.ToString();
                        drRow["FaultQty"] = this.uGridSampling.Rows[i].Cells["FaultQty"].Value.ToString();
                        drRow["ProcessSampleSize"] = this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value.ToString();
                        drRow["UnitCode"] = this.uGridSampling.Rows[i].Cells["UnitCode"].Value.ToString();
                        drRow["DataType"] = this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString();
                        drRow["InspectResultFlag"] = this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString();
                        drRow["Mean"] = this.uGridSampling.Rows[i].Cells["Mean"].Value.ToString();
                        drRow["StdDev"] = this.uGridSampling.Rows[i].Cells["StdDev"].Value.ToString();
                        drRow["MaxValue"] = this.uGridSampling.Rows[i].Cells["MaxValue"].Value.ToString();
                        drRow["MinValue"] = this.uGridSampling.Rows[i].Cells["MinValue"].Value.ToString();
                        drRow["DataRange"] = this.uGridSampling.Rows[i].Cells["DataRange"].Value.ToString();
                        drRow["Cp"] = this.uGridSampling.Rows[i].Cells["Cp"].Value.ToString();
                        drRow["Cpk"] = this.uGridSampling.Rows[i].Cells["Cpk"].Value.ToString();
                        drRow["OCPCount"] = this.uGridSampling.Rows[i].Cells["OCPCount"].Value.ToString();
                        drRow["UpperRunCount"] = this.uGridSampling.Rows[i].Cells["UpperRunCount"].Value.ToString();
                        drRow["LowerRunCount"] = this.uGridSampling.Rows[i].Cells["LowerRunCount"].Value.ToString();
                        drRow["UpperTrendCount"] = this.uGridSampling.Rows[i].Cells["UpperTrendCount"].Value.ToString();
                        drRow["LowerTrendCount"] = this.uGridSampling.Rows[i].Cells["LowerTrendCount"].Value.ToString();
                        drRow["LCL"] = this.uGridSampling.Rows[i].Cells["LCL"].Value.ToString();
                        drRow["CL"] = this.uGridSampling.Rows[i].Cells["CL"].Value.ToString();
                        drRow["UCL"] = this.uGridSampling.Rows[i].Cells["UCL"].Value.ToString();
                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사결과 데이터 Set으로 반환
        /// </summary>
        /// <returns></returns>
        private DataSet Rtn_ResultDataSet()
        {
            DataSet dsResult = new DataSet();
            try
            {
                // 데이터 테이블 컬럼설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultCount), "ProcInspectResultCount");
                QRPINS.BL.INSPRC.ProcInspectResultCount clsCount = new QRPINS.BL.INSPRC.ProcInspectResultCount();
                brwChannel.mfCredentials(clsCount);
                DataTable dtCount = clsCount.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultMeasure), "ProcInspectResultMeasure");
                QRPINS.BL.INSPRC.ProcInspectResultMeasure clsMeasure = new QRPINS.BL.INSPRC.ProcInspectResultMeasure();
                brwChannel.mfCredentials(clsMeasure);
                DataTable dtMeasure = clsMeasure.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultOkNg), "ProcInspectResultOkNg");
                QRPINS.BL.INSPRC.ProcInspectResultOkNg clsOkNg = new QRPINS.BL.INSPRC.ProcInspectResultOkNg();
                brwChannel.mfCredentials(clsOkNg);
                DataTable dtOkNg = clsOkNg.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultDesc), "ProcInspectResultDesc");
                QRPINS.BL.INSPRC.ProcInspectResultDesc clsDesc = new QRPINS.BL.INSPRC.ProcInspectResultDesc();
                brwChannel.mfCredentials(clsDesc);
                DataTable dtDesc = clsDesc.mfSetDataInfo();

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultSelect), "ProcInspectResultSelect");
                QRPINS.BL.INSPRC.ProcInspectResultSelect clsSelect = new QRPINS.BL.INSPRC.ProcInspectResultSelect();
                brwChannel.mfCredentials(clsSelect);
                DataTable dtSelect = clsSelect.mfSetDataInfo();

                // X1 시작지점 Index값 저장
                if (this.uGridSampling.DisplayLayout.Bands[0].Columns.Exists("1"))
                {
                    int intStart = this.uGridSampling.DisplayLayout.Bands[0].Columns["1"].Index;

                    if (this.uGridSampling.Rows.Count > 0)
                    {
                        this.uGridSampling.ActiveCell = this.uGridSampling.Rows[0].Cells[0];
                        DataRow drRow;
                        for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                        {
                            int intSampleSize = Convert.ToInt32(this.uGridSampling.Rows[i].Cells["ProcessSampleSize"].Value) * Convert.ToInt32(this.uGridSampling.Rows[i].Cells["SampleSize"].Value);
                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "1")
                                {
                                    drRow = dtMeasure.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = 0.0;
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtMeasure.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "2")
                                {
                                    drRow = dtCount.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = 0.0;
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtCount.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "3")
                                {
                                    drRow = dtOkNg.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = "";
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtOkNg.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "4")
                                {
                                    drRow = dtDesc.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = "";
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtDesc.Rows.Add(drRow);
                                }
                                else if (this.uGridSampling.Rows[i].Cells["DataType"].Value.ToString() == "5")
                                {
                                    drRow = dtSelect.NewRow();
                                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                    drRow["ReqLotSeq"] = 1;
                                    drRow["ReqItemSeq"] = this.uGridSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridSampling.Rows[i].Cells[j].Value == null || this.uGridSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = "";
                                    else
                                        drRow["InspectValue"] = this.uGridSampling.Rows[i].Cells[j].Value;
                                    dtSelect.Rows.Add(drRow);
                                }
                            }
                        }
                    }
                }
                dsResult.Tables.Add(dtCount);
                dsResult.Tables.Add(dtDesc);
                dsResult.Tables.Add(dtMeasure);
                dsResult.Tables.Add(dtOkNg);
                dsResult.Tables.Add(dtSelect);

                return dsResult;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dsResult;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형 정보 데이터 테이블로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_FaultDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                brwChannel.mfCredentials(clsFault);

                dtRtn = clsFault.mfSetDataInfo();
                DataRow drRow;

                // 불합격인 경우만

                if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                {
                    if (this.uGridFaultData.Rows.Count > 0)
                    {
                        this.uGridFaultData.ActiveCell = this.uGridFaultData.Rows[0].Cells[0];
                        for (int i = 0; i < this.uGridFaultData.Rows.Count; i++)
                        {
                            if (this.uGridFaultData.Rows[i].Hidden == false)
                            {
                                drRow = dtRtn.NewRow();
                                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                drRow["ReqNo"] = this.uTextReqNo.Text;
                                drRow["ReqSeq"] = this.uTextReqSeq.Text;
                                drRow["ReqLotSeq"] = Convert.ToInt32(this.uGridFaultData.Rows[i].Cells["ReqLotSeq"].Value);
                                drRow["ReqItemSeq"] = Convert.ToInt32(this.uGridFaultData.Rows[i].Cells["ReqItemSeq"].Value);
                                drRow["ReqInspectSeq"] = this.uGridFaultData.Rows[i].RowSelectorNumber;
                                drRow["FaultTypeCode"] = this.uGridFaultData.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                                if (Convert.ToBoolean(this.uGridFaultData.Rows[i].Cells["QCNFlag"].Value) == true)
                                    drRow["QCNFlag"] = "T";
                                else
                                    drRow["QCNFlag"] = "F";
                                drRow["InspectQty"] = Convert.ToDecimal(this.uGridFaultData.Rows[i].Cells["InspectQty"].Value);
                                drRow["FaultQty"] = Convert.ToDecimal(this.uGridFaultData.Rows[i].Cells["FaultQty"].Value);
                                drRow["UnitCode"] = this.uGridFaultData.Rows[i].Cells["UnitCode"].Value.ToString();
                                drRow["ExpectProcessCode"] = this.uGridFaultData.Rows[i].Cells["ExpectProcessCode"].Value.ToString();
                                drRow["CauseEquipCode"] = this.uGridFaultData.Rows[i].Cells["CauseEquipCode"].Value.ToString();
                                drRow["WorkUserID"] = this.uGridFaultData.Rows[i].Cells["WorkUserID"].Value.ToString();
                                drRow["EtcDesc"] = this.uGridFaultData.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtRtn.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 작성완료시 순회검사모니터링 저장용 데이터 테이블 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_CycleDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.Monitoring), "Monitoring");
                QRPINS.BL.INSPRC.Monitoring clsMot = new QRPINS.BL.INSPRC.Monitoring();
                brwChannel.mfCredentials(clsMot);

                dtRtn = clsMot.mfSetDataInfo();
                if (this.uCheckCompleteFlag.Checked)
                {
                    DataRow drRow = dtRtn.NewRow();
                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                    drRow["ReqNo"] = this.uTextReqNo.Text;
                    drRow["ReqSeq"] = this.uTextReqSeq.Text;
                    drRow["ReqLotSeq"] = 1;
                    drRow["EquipCode"] = this.uTextEquipCode.Text;
                    drRow["ProcessCode"] = ProcessCode;
                    drRow["ProductCode"] = this.uTextProductCode.Text;
                    dtRtn.Rows.Add(drRow);
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }
        #endregion

        #region 각테이블별 조회 메소드

        /// <summary>
        /// Lot정보 조회 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_LotInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                // 메소드호출
                DataTable dtLot = clsLot.mfReadINSProcInspectReqLot(strPlantCode, strReqNo, strReqSeq, strLang);

                // 컨트롤에 값 적용
                this.uComboPlantCode.Value = dtLot.Rows[0]["PlantCode"].ToString();
                this.uTextCustomerCode.Text = dtLot.Rows[0]["CustomerCode"].ToString();
                this.uTextCustomerName.Text = dtLot.Rows[0]["CustomerName"].ToString();
                this.uComboProcInspectType.Value = dtLot.Rows[0]["ProcInspectType"].ToString();
                this.uTextEquipCode.Text = dtLot.Rows[0]["EquipCode"].ToString();
                this.uTextProductCode.Text = dtLot.Rows[0]["ProductCode"].ToString();
                this.uTextProductName.Text = dtLot.Rows[0]["ProductName"].ToString();
                this.uTextLotNo.Text = dtLot.Rows[0]["LotNo"].ToString();
                this.uTextLotSize.Text = Convert.ToInt32(dtLot.Rows[0]["LotSize"]).ToString();
                this.uTextInspectUserID.Text = dtLot.Rows[0]["InspectUserID"].ToString();
                this.uTextInspectUserName.Text = dtLot.Rows[0]["InspectUserName"].ToString();
                this.uTextEtcDesc.Text = dtLot.Rows[0]["EtcDesc"].ToString();
                this.uDateInspectDate.Value = Convert.ToDateTime(dtLot.Rows[0]["InspectDate"]).ToString("yyyy-MM-dd");
                this.uDateInspectTime.Value = Convert.ToDateTime(dtLot.Rows[0]["InspectTime"]).ToString("HH:mm:ss");
                if (dtLot.Rows[0]["PassFailFlag"].ToString() != "")
                    this.uOptionPassFailFlag.Value = dtLot.Rows[0]["PassFailFlag"].ToString();
                this.uCheckCompleteFlag.Enabled = !Convert.ToBoolean(dtLot.Rows[0]["CompleteFlag"]);
                this.uCheckCompleteFlag.Checked = Convert.ToBoolean(dtLot.Rows[0]["CompleteFlag"]);
                this.uTextCompleteCheck.Text = dtLot.Rows[0]["CompleteFlag"].ToString();
                this.uTextStackSeq.Text = dtLot.Rows[0]["StackSeq"].ToString();
                this.uTextCustomerProductCode.Text = dtLot.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                this.uTextPackage.Text = dtLot.Rows[0]["PACKAGE"].ToString();
                this.uTextNowProcessCode.Text = dtLot.Rows[0]["NowProcessCode"].ToString();
                this.uTextNowProcessName.Text = dtLot.Rows[0]["NowProcessName"].ToString();

                this.uTextWorkProcessCode.Text = dtLot.Rows[0]["WorkProcessCode"].ToString();
                this.uTextWorkProcessName.Text = dtLot.Rows[0]["WorkProcessName"].ToString();
                this.uTextWorkUserID.Text = dtLot.Rows[0]["WorkUserID"].ToString();
                this.uTextWorkUserName.Text = dtLot.Rows[0]["WorkUserName"].ToString();
                this.uTextLossCheckFlag.Text = dtLot.Rows[0]["LossCheckFlag"].ToString().Substring(0, 1);
                this.uTextLossQty.Text = dtLot.Rows[0]["LossQty"].ToString();
                this.uTextLotProcessState.Text = dtLot.Rows[0]["LotState"].ToString();
                this.uTextProcessHoldState.Text = dtLot.Rows[0]["HoldState"].ToString();
                //this.uTextOUTSOURCINGVENDOR.Text = dtLot.Rows[0]["OUTSOURCINGVENDOR"].ToString();

                this.uTextMESTrackInTFlag.Text = dtLot.Rows[0]["MESTrackInTFlag"].ToString();
                this.uTextMESTrackOutTFlag.Text = dtLot.Rows[0]["MESTrackOutTFlag"].ToString();
                this.uTextMESHoldTFlag.Text = dtLot.Rows[0]["MESHoldTFlag"].ToString();
                this.uTextMESSPCNTFlag.Text = dtLot.Rows[0]["MESSPCNTFlag"].ToString();

                this.uTextFile.Text = dtLot.Rows[0]["FileName"].ToString();

                // 필수입력사항 Enable 로
                this.uComboPlantCode.Enabled = false;
                this.uComboProcInspectType.Enabled = false;
                this.uTextLotNo.Enabled = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item 정보 조회 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_ItemInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadINSProcInspectReqItem(strPlantCode, strReqNo, strReqSeq, strLang);

                DataTable dtGeneration = dtItem.DefaultView.ToTable(true, "Generation");

                if (dtGeneration.Rows.Count > 0)
                {
                    if (dtGeneration.Rows.Count > 1)
                        this.uTextGeneration.Text = string.Empty;
                    else
                        dtGeneration.Rows[0]["Generation"].ToString();
                }

                this.uGridItem.DataSource = dtItem;
                this.uGridItem.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Sampling 정보 조회 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_SamplingInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                // 샘플링그리드 컬럼설정
                //InitSampleGrid();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadINSProcInspectReqItem_Sampling(strPlantCode, strReqNo, strReqSeq, strLang);

                this.uGridSampling.DataSource = dtItem;
                this.uGridSampling.DataBind();

                this.uGridSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

                if (this.uGridSampling.Rows.Count > 0)
                {
                    // SampleSize 만큼 컬럼생성 Method 호출
                    String[] strLastColKey = { "Mean", "SpecRange" };
                    CreateColumn(this.uGridSampling, 0, "SampleSize", "ProcessSampleSize", strLastColKey);

                    // 데이터 유형에 따른 그리드 설정 메소드 호출
                    SetSamplingGridColumn();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형정보 그리드 조회 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        private void Search_FaultInfo(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtFault = clsItem.mfReadINSProcInspectReqItem_Fault(strPlantCode, strReqNo, strReqSeq, strLang);

                this.uGridFault.DataSource = dtFault;
                this.uGridFault.DataBind();

                for (int i = 0; i < this.uGridFault.Rows.Count; i++)
                {
                    if (this.uGridFault.Rows[i].Cells["InspectResultFlag"].Value.ToString().Equals("OK"))
                        //this.uGridFault.Rows[i].Appearance.BackColor = Color.Red;
                        //this.uGridFault.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                        this.uGridFault.Rows[i].Hidden = true;
                    //e.Row.Hidden = true;
                    //e.Row.Appearance.BackColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 통계값, 합부판정
        /// <summary>
        /// 계수/계량형 결과값 검사 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementMeasureCount(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Instance 객체 생성
                QRPSTA.STABAS clsSTABAS = new QRPSTA.STABAS();
                QRPSTA.STASummary structSTA = new QRPSTA.STASummary();

                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;
                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                int intSampleSize = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value));
                if (intSampleSize > 99)
                    intSampleSize = 99;
                int intLastIndex = intSampleSize + intStart;
                // Double형 배열 생성
                double[] dblXn = new double[intLastIndex - intStart];

                // Loop돌며 배열에 값 저장
                for (int i = 0; i < intLastIndex - intStart; i++)
                {
                    int intIndex = intStart + i;
                    dblXn[i] = Convert.ToDouble(e.Cell.Row.Cells[intIndex].Value);
                }

                structSTA = clsSTABAS.mfCalcSummaryStat(dblXn, Convert.ToDouble(e.Cell.Row.Cells["LowerSpec"].Value)
                                                , Convert.ToDouble(e.Cell.Row.Cells["UpperSpec"].Value), e.Cell.Row.Cells["SpecRange"].Value.ToString()
                                                , true, Convert.ToDouble(e.Cell.Row.Cells["LCL"].Value), Convert.ToDouble(e.Cell.Row.Cells["UCL"].Value));

                // 합부판정
                if (structSTA.AcceptFlagDA)
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                    e.Cell.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                    e.Cell.Row.Appearance.BackColor = Color.Salmon;
                }

                // 불량수량 입력
                e.Cell.Row.Cells["FaultQty"].Value = structSTA.SLOverCount;

                // 통계값
                e.Cell.Row.Cells["Mean"].Value = structSTA.MeanDA;
                e.Cell.Row.Cells["MaxValue"].Value = structSTA.MaxDA;
                e.Cell.Row.Cells["MinValue"].Value = structSTA.MinDA;
                e.Cell.Row.Cells["DataRange"].Value = structSTA.RangeDA;
                e.Cell.Row.Cells["StdDev"].Value = structSTA.StdDevDA;
                e.Cell.Row.Cells["Cp"].Value = structSTA.CpDA;
                e.Cell.Row.Cells["Cpk"].Value = structSTA.CpkDA;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// OK/NG 합/부 판정 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementOkNg(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;
                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                //int intLastIndex = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;
                int intSampleSize = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value));
                if (intSampleSize > 99)
                    intSampleSize = 99;
                int intLastIndex = intSampleSize + intStart;

                bool bolCheck = true;
                int intFaultCount = 0;

                // Loop 돌며 결과값 검사
                for (int i = intStart; i < intLastIndex; i++)
                {
                    if (e.Cell.Row.Cells[i].Value.ToString() == "NG")
                    {
                        bolCheck = false;
                        intFaultCount += 1;
                    }
                }

                // 불량수량 Update
                e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;

                if (bolCheck)
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                    e.Cell.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                    e.Cell.Row.Appearance.BackColor = Color.Salmon;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region MES I/F
        /// <summary>
        /// MES I/F 저장처리부분

        /// </summary>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        private void SaveMESInterFace(string strReqNo, string strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                QRPBrowser brwChannel = new QRPBrowser();
                string strErrRtn = string.Empty;
                TransErrRtn ErrRtn = new TransErrRtn();

                // TrackOut 이 진행되지 않은경우
                if (!this.uTextMESTrackOutTFlag.Text.Equals("T") && (!this.uTextMESTrackInTCheck.Text.Equals("T") || !this.uTextMESTrackInTFlag.Text.Equals("F")))
                {
                    // 설비테이블에서 AREACODE 조회
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), EquipCode, m_resSys.GetString("SYS_LANG"));

                    if (dtEquip.Rows.Count > 0)
                    {

                        // 데이터 테이블 설정
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                        QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                        brwChannel.mfCredentials(clsLot);

                        DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackOUT();
                        DataRow drRow = dtStdInfo.NewRow();
                        drRow["ReqNo"] = strReqNo;
                        drRow["ReqSeq"] = strReqSeq;
                        drRow["ReqLotSeq"] = 1;
                        drRow["FormName"] = this.Name;
                        drRow["FACTORYID"] = this.uComboPlantCode.Value.ToString();          // 공장코드
                        drRow["USERID"] = m_resSys.GetString("SYS_USERID");               // 사용자ID
                        drRow["LOTID"] = this.uTextLotNo.Text.Trim();                    // Lot 번호
                        drRow["EQPID"] = EquipCode;                                      // 설비코드
                        drRow["RECIPEID"] = "-";                                            // RecipeID 사용하지 않으면'-'
                        drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();         // AreaID
                        drRow["SPLITFLAG"] = "N";                                            // SPLIT 여부 N 고정
                        drRow["SCRAPFLAG"] = "N";                                            // SCRAP 여부
                        drRow["REPAIRFLAG"] = "N";                                            // REPAIR 여부
                        drRow["RWFLAG"] = "N";                                            // REWORK 여부
                        drRow["CONSUMEFLAG"] = "N";                                            // 자재소모여부 'N' 고정
                        drRow["COMMENT"] = "";                                             // Comment
                        drRow["QCFLAG"] = "Y";                                            // QCFlag 'Y' 고정
                        drRow["SAMPLECOUNT"] = "";                                          // Sampling 갯수
                        drRow["QUALEFLAG"] = this.uOptionPassFailFlag.Value.ToString();      // 판정결과
                        ////// DataTable 1:n
                        //////drRow["REASONQTY"] = "";                                             // 불량개수
                        //////drRow["REASONCODE"] = "";                                             // 불량코드
                        //////drRow["CAUSEEQPID"] = "";                                             // 불량원인설비
                        dtStdInfo.Rows.Add(drRow);

                        // Scrap 데이터 테이블(SCRAPCODE, SCRAPQTY)
                        DataTable dtScrapList = new DataTable();

                        // 해당공정 불량정보 가져오기

                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                        QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                        brwChannel.mfCredentials(clsFault);

                        DataTable dtFaultList = clsFault.mfReadINSProcInspectResultFault_MESTrackOut(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, 1, "II02010001");

                        // TrackOUT 처리 메소드 호출
                        strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESTrackOUT(dtStdInfo, dtScrapList, dtFaultList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 결과검사

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0) && !ErrRtn.InterfaceResultCode.Equals(string.Empty) && !ErrRtn.InterfaceResultCode.Equals(string.Empty))
                        {
                            if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M000095", "M000950", Infragistics.Win.HAlign.Right);

                                // 조회 Method 호출
                                Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                           , ErrRtn.InterfaceResultMessage
                                           , Infragistics.Win.HAlign.Right);

                                // 조회 Method 호출
                                Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            }
                        }
                        else
                        {
                            // 조회 Method 호출
                            Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                        }
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000717", "M000090",
                                                    Infragistics.Win.HAlign.Right);

                        // 조회 Method 호출
                        Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                        Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                        Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                        Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                    }
                }
                // TrackOut이 처리되고 검사결과가 불합격인데 Hold요청이 T가 아닌인 경우
                if (this.uOptionPassFailFlag.CheckedIndex == 1 && !this.uTextMESHoldTFlag.Text.Equals("T") && this.uTextMESTrackOutTFlag.Text.Equals("T"))
                {
                    DataTable dtLotList = new DataTable();
                    dtLotList.Columns.Add("LOTID", typeof(string));
                    DataRow drRow = dtLotList.NewRow();
                    drRow["LOTID"] = this.uTextLotNo.Text;
                    dtLotList.Rows.Add(drRow);

                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                    brwChannel.mfCredentials(clsLot);

                    DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                    drRow = dtStdInfo.NewRow();
                    drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                    drRow["ReqNo"] = strReqNo;
                    drRow["ReqSeq"] = strReqSeq;
                    drRow["ReqLotSeq"] = 1;
                    drRow["FormName"] = this.Name;
                    dtStdInfo.Rows.Add(drRow);

                    strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESHold(this.Name,
                                                            m_resSys.GetString("SYS_USERID"),
                                                            "",     // Comment
                                                            "HQ11",     // HoldCode
                                                            dtLotList,
                                                            m_resSys.GetString("SYS_USERIP"),
                                                            dtStdInfo);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0) && !ErrRtn.InterfaceResultCode.Equals(string.Empty) && !ErrRtn.InterfaceResultCode.Equals(string.Empty))
                    {
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M000095", "M000946", Infragistics.Win.HAlign.Right);

                            // 조회 Method 호출
                            Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                        }
                        else
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang), ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);

                            // 조회 Method 호출
                            Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                            Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                        }
                    }
                    else
                    {
                        // 조회 Method 호출
                        Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                    }
                }
                // TrackIn, TrackOut, Hold 요청이 모두 끝난후 검사결과가 관리한계선을 벗어난 경우
                if (this.uTextMESHoldTFlag.Text.Equals("T") && this.uTextMESSPCNTFlag.Text.Equals("F") &&
                    this.uTextMESTrackOutTFlag.Text.Equals("T") && this.uButtonTrackIn.Enabled.Equals(false))
                {
                    for (int i = 0; i < this.uGridItem.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(this.uGridItem.Rows[i].Cells["OCPCount"].Value) > 0)
                        {
                            // MES I/F 처리
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                            QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                            brwChannel.mfCredentials(clsLot);

                            DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                            DataRow drRow = dtStdInfo.NewRow();
                            drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                            drRow["ReqNo"] = strReqNo;
                            drRow["ReqSeq"] = strReqSeq;
                            drRow["ReqLotSeq"] = 1;
                            dtStdInfo.Rows.Add(drRow);

                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_MESSPCN(this.Name, m_resSys.GetString("SYS_USERID"), EquipCode, m_resSys.GetString("SYS_USERIP"), dtStdInfo);

                            // 결과검사

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum.Equals(0) && !ErrRtn.InterfaceResultCode.Equals(string.Empty) && !ErrRtn.InterfaceResultCode.Equals(string.Empty))
                            {
                                if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000095", "M000949", Infragistics.Win.HAlign.Right);

                                    // 조회 Method 호출
                                    Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    break;
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                        , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);

                                    // 조회 Method 호출
                                    Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    Search_ItemInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    Search_SamplingInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    Search_FaultInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                    break;
                                }
                            }
                            else
                            {
                                // 조회 Method 호출
                                Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 기타..
        /// <summary>
        /// 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlantCode.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboProcInspectType.Value = "";
                this.uTextLotNo.Clear();
                this.uTextEquipName.Clear();
                this.uTextLotSize.Clear();
                this.uTextCustomerCode.Clear();
                this.uTextCustomerName.Clear();
                this.uTextEtcDesc.Clear();
                this.uTextInspectUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextProductCode.Clear();
                this.uTextProductName.Clear();
                this.uTextReqNo.Clear();
                this.uTextReqSeq.Clear();
                this.uDateInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateInspectTime.Value = DateTime.Now.ToString("HH:mm:ss");
                this.uOptionPassFailFlag.CheckedIndex = -1;
                this.uCheckCompleteFlag.CheckedValue = false;

                this.uComboPlantCode.Enabled = true;
                this.uComboProcInspectType.Enabled = true;
                this.uTextLotNo.Enabled = true;
                this.uCheckCompleteFlag.Enabled = true;

                this.uTextStackSeq.Clear();
                this.uTextWorkProcessCode.Clear();
                this.uTextWorkProcessName.Clear();
                this.uTextWorkUserID.Clear();
                this.uTextWorkUserName.Clear();
                this.uTextPackage.Clear();
                this.uTextNowProcessName.Clear();
                this.uTextNowProcessCode.Clear();
                this.uTextMESTrackOutTFlag.Clear();
                this.uTextMESTrackInTFlag.Clear();
                this.uTextMESTrackInTCheck.Clear();
                this.uTextMESSPCNTFlag.Clear();
                this.uTextMESHoldTFlag.Clear();
                this.uTextEquipCode.Clear();
                this.uTextCustomerProductCode.Clear();

                this.uTextCompleteCheck.Clear();
                this.uTextOUTSOURCINGVENDOR.Clear();

                this.uTextGeneration.Clear();

                //this.uButtonTrackIn.Visible = false;
                this.uButtonTrackIn.Enabled = false;

                while (this.uGridItem.Rows.Count > 0)
                {
                    this.uGridItem.Rows[0].Delete(false);
                }

                while (this.uGridSampling.Rows.Count > 0)
                {
                    this.uGridSampling.Rows[0].Delete(false);
                }
                while (this.uGridFault.Rows.Count > 0)
                {
                    this.uGridFault.Rows[0].Delete(false);
                }
                while (this.uGridFaultData.Rows.Count > 0)
                {
                    this.uGridFaultData.Rows[0].Delete(false);
                }

                this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                SetChartClear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
            }
        }

        /// <summary>
        /// QCN 자동이동 메소드
        /// </summary>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        private void MoveQCN(string strReqNo, string strReqSeq)
        {
            try
            {
                string strPlantCode = this.uComboPlantCode.Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                brwChannel.mfCredentials(clsFault);

                DataTable dtCheck = clsFault.mfReadINSProcInspectResultFault_MoveQCN(strPlantCode, strReqNo, strReqSeq);

                // QCN 자동이동 여부 판단
                if (dtCheck.Rows.Count > 0)
                {
                    frmINSZ0010 frmINS = new frmINSZ0010();
                    frmINS.PlantCode = dtCheck.Rows[0]["PlantCode"].ToString();
                    frmINS.ReqNo = dtCheck.Rows[0]["ReqNo"].ToString();
                    frmINS.ReqSeq = dtCheck.Rows[0]["ReqSeq"].ToString();
                    frmINS.ReqLotSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqLotSeq"]);
                    frmINS.ReqItemSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqItemSeq"]);
                    frmINS.ReqInspectSeq = Convert.ToInt32(dtCheck.Rows[0]["ReqInspectSeq"]);
                    frmINS.FormName = this.Name;

                    CommonControl cControl = new CommonControl();
                    Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                    Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                    if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINSZ0010"))
                    {
                        uTabMenu.Tabs["QRPINS" + "," + "frmINSZ0010"].Close();
                    }
                    //탭에 추가
                    uTabMenu.Tabs.Add("QRPINS" + "," + "frmINSZ0010", "QCN 발행");
                    uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINSZ0010"];

                    //호출시 화면 속성 설정
                    frmINS.AutoScroll = true;
                    frmINS.MdiParent = this.MdiParent;
                    frmINS.ControlBox = false;
                    frmINS.Dock = DockStyle.Fill;
                    frmINS.FormBorderStyle = FormBorderStyle.None;
                    frmINS.WindowState = FormWindowState.Normal;
                    frmINS.Text = "QCN 발행";
                    frmINS.Show();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private DataTable SearchEquipInfo(string strPlantCode, string strEquipCode,string strLang)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                dtRtn = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, strLang);
                clsEquip.Dispose();
                
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            { dtRtn.Dispose(); }
        }

        ///// <summary>
        ///// 샘플링검사 그리드 초기화 메소드(Xn 컬럼이 남아 있어 그리드 초기화후 다시 바인딩 하기 위해 따로 메소드로 뺐음)
        ///// </summary>
        //private void InitSampleGrid()
        //{
        //    try
        //    {
        //        for (int i = 0; i < this.uGridSampling.DisplayLayout.Bands[0].Columns.Count; i++)
        //        {
        //            this.uGridSampling.DisplayLayout.Bands[0].Columns.Remove(i);
        //        }

        //        // SystemInfo ResourceSet
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //        WinGrid wGrid = new WinGrid();

        //        // 샘플링검사 그리드

        //        // 일반설정
        //        wGrid.mfInitGeneralGrid(this.uGridSampling, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
        //            , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
        //            , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
        //            , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

        //        // 컬럼설정
        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "ReqItemSeq", "항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
        //            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 0
        //            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
        //            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
        //            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
        //            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
        //            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
        //            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "ProcessSampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
        //            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
        //            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
        //            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 1
        //            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

        //        //wGrid.mfSetGridColumn(this.uGrid2, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
        //        //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //        //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
        //            , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
        //            , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

        //        wGrid.mfSetGridColumn(this.uGridSampling, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
        //            , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
        //            , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

        //        this.uGridSampling.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
        //        this.uGridSampling.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

        //        // DropDown 설정
        //        // 단위
        //        QRPBrowser brwChannel = new QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
        //        QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
        //        brwChannel.mfCredentials(clsUnit);
        //        DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
        //        wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

        //        // DataRange
        //        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
        //        QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
        //        brwChannel.mfCredentials(clsCom);
        //        DataTable dtCom = clsCom.mfReadCommonCode("C0032", m_resSys.GetString("SYS_LANG"));
        //        wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);

        //        // 검사결과

        //        dtCom = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));
        //        wGrid.mfSetGridColumnValueList(this.uGridSampling, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtCom);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}
        #endregion

        #endregion

        #region 이벤트

        // 행삭제버튼 이벤트
        private void uButtonDelete2_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridFaultData.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridFaultData.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridFaultData.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // TrackIn 버튼 클릭시 MES I/F Lot TrackIn 요청
        private void uButtonTrackIn_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 변수설정
                // 필수입력사항 확인
                if (this.uComboPlantCode.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Center);

                    this.uComboPlantCode.DropDown();
                    return;
                }
                else if (this.uComboProcInspectType.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000287", Infragistics.Win.HAlign.Center);

                    this.uComboProcInspectType.DropDown();
                    return;
                }
                else if (this.uTextLotNo.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000078", Infragistics.Win.HAlign.Center);

                    this.uTextLotNo.Focus();
                    return;
                }
                else
                {
                    // 저장여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000153", "M000152", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 저장정보 데이터 테이블 설정
                        DataTable dtHeader = Rtn_HeaderDataTable();
                        DataTable dtLot = Rtn_LotDataTable();
                        DataTable dtItem = Rtn_ItemDataTable();
                        DataSet dsResult = Rtn_ResultDataSet();
                        DataTable dtFault = Rtn_FaultDataTable();
                        DataTable dtCycle = Rtn_CycleDataTable();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
                        QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
                        brwChannel.mfCredentials(clsHeader);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // Method 호출
                        string strErrRtn = clsHeader.mfSaveINSProcInspectReqH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                                                                            , dtLot, dtItem, dsResult, dtFault, dtCycle);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            string strReqNo = ErrRtn.mfGetReturnValue(0);
                            string strReqSeq = ErrRtn.mfGetReturnValue(1);

                            // 저장 성공시 MESTrackIn 처리
                            // 설비테이블에서 AREACODE 조회
                            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                            QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                            brwChannel.mfCredentials(clsEquip);

                            DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(this.uComboPlantCode.Value.ToString(), EquipCode, m_resSys.GetString("SYS_LANG"));

                            if (dtEquip.Rows.Count > 0)
                            {
                                // LotList 설정
                                DataTable dtLotList = new DataTable();
                                // 컬럼설정
                                dtLotList.Columns.Add("LOTID", typeof(string));
                                dtLotList.Columns.Add("EQPID", typeof(string));
                                dtLotList.Columns.Add("RECIPEID", typeof(string));
                                dtLotList.Columns.Add("AREAID", typeof(string));
                                dtLotList.Columns.Add("IQASAMPLINGFLAG", typeof(string));
                                dtLotList.Columns.Add("COMMENT", typeof(string));

                                DataRow drRow = dtLotList.NewRow();
                                drRow["LOTID"] = this.uTextLotNo.Text;
                                drRow["EQPID"] = EquipCode;
                                drRow["RECIPEID"] = "-";
                                drRow["AREAID"] = dtEquip.Rows[0]["AreaCode"].ToString();
                                drRow["IQASAMPLINGFLAG"] = "";
                                drRow["COMMENT"] = "";
                                dtLotList.Rows.Add(drRow);

                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                                QRPINS.BL.INSPRC.ProcInspectReqLot clsLot = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                                brwChannel.mfCredentials(clsLot);

                                // 기본정보 테이블 설정
                                DataTable dtStdInfo = clsLot.mfSetDataInfo_MESTrackIn();
                                drRow = dtStdInfo.NewRow();
                                drRow["PlantCode"] = this.uComboPlantCode.Value.ToString();
                                drRow["ReqNo"] = strReqNo;
                                drRow["ReqSeq"] = strReqSeq;
                                drRow["ReqLotSeq"] = 1;
                                drRow["FormName"] = this.Name;
                                dtStdInfo.Rows.Add(drRow);

                                strErrRtn = clsLot.mfSaveINSProcINspectReqLot_MESTrackIn(dtStdInfo, dtLotList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                // 결과검사
                                if (ErrRtn.InterfaceResultCode.Equals(string.Empty) && ErrRtn.ErrNum.Equals(0))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000150",
                                                Infragistics.Win.HAlign.Right);
                                    Search_LotInfo(this.uComboPlantCode.Value.ToString(), strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                                }
                                else
                                {
                                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000095", "M000951", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                    }
                                }
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000717", "M000716",
                                    Infragistics.Win.HAlign.Right);
                                return;
                            }


                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사시간 스핀버튼 이벤트
        private void uDateInspectTime_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.EditorButtonEventArgs ed = sender as Infragistics.Win.UltraWinEditors.EditorButtonEventArgs;
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uDateInspectTime.Focus();
                    uDateInspectTime.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uDateInspectTime.Focus();
                    uDateInspectTime.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Contents 그룹박스 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 690;
                    for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                    {
                        this.uGridHeader.Rows[i].Fixed = false;
                    }

                    // Control Clear
                    Clear();
                }

                //if (this.uGroupBoxContentsArea.Expanded)
                //{
                //    this.uGridHeader.Rows.FixedRows.Clear();

                //    Clear();
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Sampling 그리드 결과값 입력시 통계값 및 합부판정 구하는 이벤트
        private void uGridSampling_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 그리드 이벤트 헤제
                this.uGridSampling.EventManager.SetEnabled(Infragistics.Win.UltraWinGrid.EventGroups.AfterEvents, false);

                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    // 입력한 셀이 Xn 컬럼일때
                    if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                    {
                        // 입력줄의 데이터유형이 계량/계수인경우
                        if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")
                        {
                            if (e.Cell.Value == DBNull.Value)
                            {
                                e.Cell.Value = e.Cell.OriginalValue;
                                return;
                            }
                            JudgementMeasureCount(e);
                        }
                        // 입력줄의 데이터 유형이 Ok/Ng 인경우
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                        {
                            JudgementOkNg(e);
                        }
                        else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "5")
                        {
                            JudgementSelect(e);
                        }
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                    }

                    // 검사결과 컬럼 Update시 헤더 자동 합부판정 
                    else if (e.Cell.Column.Key == "InspectResultFlag")
                    {
                        if (e.Cell.Value.Equals("NG"))
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            e.Cell.Row.Cells["FaultQty"].Value = 1;
                        }
                        else if (e.Cell.Value.Equals("OK"))
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                            e.Cell.Row.Cells["FaultQty"].Value = 0;
                        }
                        else
                        {
                            e.Cell.Row.Cells["InspectIngFlag"].Value = false;
                        }
                    }

                    // 합부판정 자동판정 주석처리
                    /*
                    // 상단 검사결과 업데이트
                    int intCount = 0;
                    for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGridSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount++;
                        }
                    }
                    // 하나라도 불량일경우 헤더 합부판정 불합격
                    if (intCount > 0)
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 1;
                    }
                    else
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }
                     * */
                }

                // 이벤트 등록
                this.uGridSampling.EventManager.SetEnabled(Infragistics.Win.UltraWinGrid.EventGroups.AfterEvents, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 헤더그리드 더블클릭시 상세정보 조회
        private void uGridHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // Clear
                Clear();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                e.Row.Fixed = true;

                this.uGroupBoxContentsArea.Expanded = true;

                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strFullReqNo = e.Row.Cells["ReqNo"].Value.ToString();

                string strReqNo = string.Empty;//e.Row.Cells["ReqNo"].Value.ToString().Substring(0, 8);
                string strReqSeq = string.Empty;//e.Row.Cells["ReqNo"].Value.ToString().Substring(8, 4);

                if (strFullReqNo.Length.Equals(10))
                {
                    strReqNo = strFullReqNo.Substring(0, 6);
                    strReqSeq = strFullReqNo.Substring(6, 4);
                }
                else if (strFullReqNo.Length.Equals(12))
                {
                    strReqNo = strFullReqNo.Substring(0, 8);
                    strReqSeq = strFullReqNo.Substring(8, 4);
                }

                /////////////////////////////////////////////////////////////////
                //////// 공정검사 등록 이동
                /////////////////////////////////////////////////////////////////
                ////if (e.Row.Cells["CompleteFlag"].Value.ToString().Equals("F"))
                ////{
                ////    frmINS0008C frmINS = new frmINS0008C();
                ////    frmINS.PlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                ////}
                /////////////////////////////////////////////////////////////////

                this.uTextReqNo.Text = strReqNo;
                this.uTextReqSeq.Text = strReqSeq;

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                Search_LotInfo(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                Search_ItemInfo(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                Search_SamplingInfo(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                Search_FaultInfo(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량유형그리드 더블클릭시 상세 그리드
        private void uGridFault_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //// 그리드에 내용이 있는경우
                //if (this.uGridFaultData.Rows.Count > 0)
                //{
                //    // 헤더정보가 저장된 상황이면 FaultData 만 저장

                //    if (this.uTextReqNo.Text != "" && this.uTextReqSeq.Text != "")
                //    {
                //        // SystemInfo ResourceSet
                //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //        QRPBrowser brwChannel = new QRPBrowser();
                //        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                //        QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                //        brwChannel.mfCredentials(clsFault);

                //        DataTable dtFault = Rtn_FaultDataTable();

                //        string strErrRtn = clsFault.mfSaveINSProcInspectResultFault(dtFault, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                //        // 결과검사

                //        TransErrRtn ErrRtn = new TransErrRtn();
                //        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //        if (ErrRtn.ErrNum != 0)
                //        {
                //            WinMessageBox msg = new WinMessageBox();
                //            DialogResult Result = new DialogResult();
                //            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                //                                Infragistics.Win.HAlign.Right);
                //            return;
                //        }
                //    }
                //    else
                //    {
                //        // 헤더부터 저장

                //        mfSave();
                //    }
                //}

                // 검사결과가 불량인 경우만 작성가능
                if (e.Row.Cells["InspectResultFlag"].Value.ToString().Equals("NG"))// && this.uButtonTrackIn.Enabled.Equals(false))
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectResultFault), "ProcInspectResultFault");
                    QRPINS.BL.INSPRC.ProcInspectResultFault clsFault = new QRPINS.BL.INSPRC.ProcInspectResultFault();
                    brwChannel.mfCredentials(clsFault);

                    // DropDown 설정
                    WinGrid wGrid = new WinGrid();

                    // 불량유형
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    QRPMAS.BL.MASQUA.InspectFaultType clsFType = new QRPMAS.BL.MASQUA.InspectFaultType();
                    brwChannel.mfCredentials(clsFType);
                    DataTable dtFType = clsFType.mfReadInspectFaultTypeCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFType);

                    // 예상공정
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    DataTable dtProcess = clsProcess.mfReadProcessForCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "ExpectProcessCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcess);

                    // 원인설비
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    DataTable dtEquip = clsEquip.mfReadEquipForPlantCombo(this.uComboPlantCode.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridColumnValueList(this.uGridFaultData, 0, "CauseEquipCode", Infragistics.Win.ValueListDisplayStyle.DataValue, "", "선택", dtEquip);

                    DataTable dtRtnFault = clsFault.mfReadINSProcInspectResultFault(this.uComboPlantCode.Value.ToString(), this.uTextReqNo.Text, this.uTextReqSeq.Text
                                                                        , Convert.ToInt32(e.Row.Cells["ReqLotSeq"].Value), Convert.ToInt32(e.Row.Cells["ReqItemSeq"].Value));

                    this.uGridFaultData.DataSource = dtRtnFault;
                    this.uGridFaultData.DataBind();

                    // 그리드 입력가능하게 변경
                    //this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    // PK 키값 설정
                    this.uGridFaultData.DisplayLayout.Bands[0].Columns["ReqLotSeq"].DefaultCellValue = e.Row.Cells["ReqLotSeq"].Value.ToString();
                    this.uGridFaultData.DisplayLayout.Bands[0].Columns["ReqItemSeq"].DefaultCellValue = e.Row.Cells["ReqItemSeq"].Value.ToString();
                }
                else
                {
                    while (this.uGridFaultData.Rows.Count > 0)
                    {
                        this.uGridFaultData.Rows[0].Delete(false);
                    }

                    // 그리드 입력불가능하게 변경
                    this.uGridFaultData.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량유형 상세 그리드 셀 업데이트시 이벤트들
        private void uGridFaultData_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            ////try
            ////{
            ////    // 예상공정 선택시 설비, 작업자ID, 작업자 명 가져오는 MES I/F
            ////    if (e.Cell.Column.Key.Equals("ExpectProcessCode"))
            ////    {
            ////        if (this.uButtonTrackIn.Enabled.Equals(false))
            ////        {
            ////            // 화일서버 연결정보 가져오기
            ////            QRPBrowser brwChannel = new QRPBrowser();
            ////            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
            ////            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
            ////            brwChannel.mfCredentials(clsSysAccess);
            ////            QRPINS.BL.INSPRC.MESCodeReturn clsMESCode = new QRPINS.BL.INSPRC.MESCodeReturn();
            ////            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), clsMESCode.MesCode);
            ////            //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");
            ////            //DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S07");

            ////            // MES I/F
            ////            QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);
            ////            DataTable dtEquipInfo = clsTibrv.LOT_PROCESSED_EQP_REQ(this.uTextLotNo.Text.Trim(), e.Cell.Value.ToString());


            ////            if (dtEquipInfo.Rows[0]["returncode"].ToString().Equals(string.Empty))
            ////            {
            ////                e.Cell.Row.Cells["CauseEquipCode"].Value = dtEquipInfo.Rows[0]["EQPID"].ToString();
            ////                e.Cell.Row.Cells["WorkUserID"].Value = dtEquipInfo.Rows[0]["USERID"].ToString();
            ////                e.Cell.Row.Cells["WorkUserName"].Value = dtEquipInfo.Rows[0]["USERNAME"].ToString();
            ////            }
            ////            else
            ////            {
            ////                // SystemInfo ResourceSet
            ////                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////                WinMessageBox msg = new WinMessageBox();
            ////                DialogResult Result = new DialogResult();

            ////                string strLang = m_resSys.GetString("SYS_LANG");
            ////                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            ////                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                    , msg.GetMessge_Text("M000089", strLang), msg.GetMessge_Text("M000082", strLang)
            ////                    , dtEquipInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
            ////            }
            ////        }
            ////    }
            ////    // 불량유형 선택시 기준정보에서 QCNFlag 가져오는 부분
            ////    else if (e.Cell.Column.Key.Equals("FaultTypeCode"))
            ////    {
            ////        QRPBrowser brwChannel = new QRPBrowser();
            ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
            ////        QRPMAS.BL.MASQUA.InspectFaultType clsFT = new QRPMAS.BL.MASQUA.InspectFaultType();
            ////        brwChannel.mfCredentials(clsFT);

            ////        DataTable dtQCNFlag = clsFT.mfReadInspectFaultType_QCNFlag(this.uComboPlantCode.Value.ToString(), e.Cell.Value.ToString());

            ////        e.Cell.Row.Cells["QCNFlag"].Value = Convert.ToBoolean(dtQCNFlag.Rows[0]["QCNFlag"]);
            ////    }
            ////    // 불량수량 입력시 검사수량보다 많으면 에러메세지 띄우는 부분
            ////    else if (e.Cell.Column.Key.Equals("FaultQty"))
            ////    {
            ////        if (!Convert.ToDecimal(e.Cell.Value).Equals(0))
            ////        {
            ////            if (Convert.ToDecimal(e.Cell.Value) > Convert.ToDecimal(e.Cell.Row.Cells["InspectQty"].Value))
            ////            {
            ////                // SystemInfo ResourceSet
            ////                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////                WinMessageBox msg = new WinMessageBox();

            ////                if (msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            ////                    , "M000877", "M000611", "M000608", Infragistics.Win.HAlign.Right) == DialogResult.OK)
            ////                    e.Cell.CancelUpdate();
            ////                {
            ////                    e.Cell.Value = 0;
            ////                    e.Cell.Activate();
            ////                }
            ////            }
            ////        }
            ////    }
            ////}
            ////catch (Exception ex)
            ////{
            ////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////    frmErr.ShowDialog();
            ////}
            ////finally
            ////{
            ////}
        }

        // TrackIn 이 진행되어야 하는 상황에서 설비정보 요청 방지
        private void uGridFaultData_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("ExpectProcessCode"))
                {
                    if (this.uButtonTrackIn.Enabled.Equals(true))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M000089", "M000714", "M000155", Infragistics.Win.HAlign.Right);

                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사결과 업데이트시 배경색상 변경 하기 위한 이벤트
        private void uGridFault_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("InspectResultFlag"))
                {
                    if (e.Cell.Value.Equals("OK"))
                    {
                        e.Cell.Row.Appearance.BackColor = Color.Empty;
                    }
                    else
                    {
                        e.Cell.Row.Appearance.BackColor = Color.Salmon;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridItem_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["InspectIngFlag"].Value.Equals("True"))
                {
                    e.Row.Hidden = false;
                }
                else
                {
                    e.Row.Hidden = true;
                }

                if (!e.Row.Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                {
                    e.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Row.Appearance.BackColor = Color.Salmon;
                }

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridSampling_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                    //cellError = "InspectResultFlag";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경
                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        int intSampleSize = Convert.ToInt32(e.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Row.Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) > Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) ||
                                            Convert.ToDecimal(e.Row.Cells[j.ToString()].Value) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridFaultData_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Value.ToString().Contains(":\\"))
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlantCode.Value.ToString(), "D0026");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                        arrFile.Add(e.Cell.Value.ToString());

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 불량 검사데이터 그리드 작업자 ID 셀버튼 클릭시 이벤트
        private void uGridFaultData_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (e.Cell.Column.Key == "WorkUserID")
                {
                    if (this.uComboPlantCode.Value.ToString() != "")
                    {
                        frmPOP0011 frmPOP = new frmPOP0011();
                        frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                        frmPOP.ShowDialog();

                        if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                        {
                            // SystemInfo ResourceSet
                            DialogResult Result = new DialogResult();

                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                        , msg.GetMessge_Text("M001254", strLang) + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Cells["WorkUserID"].Value = "";
                            e.Cell.Row.Cells["WorkUserName"].Value = "";
                        }
                        else
                        {
                            e.Cell.Row.Cells["WorkUserID"].Value = frmPOP.UserID;
                            e.Cell.Row.Cells["WorkUserName"].Value = frmPOP.UserName;
                        }
                    }
                    else
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M000798", "M000275", "M000266"
                                                    , Infragistics.Win.HAlign.Right);

                        this.uComboPlantCode.DropDown();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
       
        #region 그래프 관련..
        // 그리드 더블클릭시 검사유형이 계량이면 그래프 보여주는 이벤트
        private void uGridItem_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    SetChart(e);
                }
                else
                {
                    SetChartClear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 그리드 더블클릭시 검사유형이 계량이면 그래프 보여주는 이벤트
        private void uGridSampling_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    SetChart(e);
                }
                else
                {
                    SetChartClear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그래프 메소드
        private void SetChart(Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region 기존소스 주석처리
                /*
                for (int i = 0; i < this.uGridSampling.Rows.Count; i++)
                {
                    if (Convert.ToInt32(this.uGridSampling.Rows[i].Cells["Seq"].Value) == Convert.ToInt32(e.Row.Cells["Seq"].Value))
                    {
                        int intStartIndex = this.uGridSampling.Rows[i].Cells["1"].Column.Index;
                        int intSampleSize = (Convert.ToInt32(e.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Row.Cells["SampleSize"].Value));

                        double dblLSL = Convert.ToDouble(this.uGridSampling.Rows[i].Cells["LowerSpec"].Value);
                        double dblUSL = Convert.ToDouble(this.uGridSampling.Rows[i].Cells["UpperSpec"].Value);
                        string strSpecRange = this.uGridSampling.Rows[i].Cells["SpecRange"].Value.ToString();

                        // 검사값 DataTable 에 저장
                        DataTable dtValue = new DataTable();
                        dtValue.Columns.Add("DataValue", typeof(double));
                        for (int j = intStartIndex; j < intSampleSize + intStartIndex; j++)
                        {
                            DataRow drRow = dtValue.NewRow();
                            drRow["DataValue"] = Convert.ToDecimal(this.uGridSampling.Rows[i].Cells[j].Value);
                            dtValue.Rows.Add(drRow);
                        }
                        
                        QRPSTA.STASPC clsXchart = new QRPSTA.STASPC();

                        // 관리 상/하한 한계썬 가져오는 메소드 호출
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSTA.BL.STAPRC.ProcessStaticCL), "ProcessStaticCL");
                        QRPSTA.BL.STAPRC.ProcessStaticCL clsStaticCL = new QRPSTA.BL.STAPRC.ProcessStaticCL();
                        brwChannel.mfCredentials(clsStaticCL);

                        DataTable dtCL = clsStaticCL.mfReadINSProcessStaticCL_XMR(this.uComboPlantCode.Value.ToString(), this.uTextProductCode.Text, ProcessCode
                                                                        , e.Row.Cells["InspectItemCode"].Value.ToString()
                                                                        , Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd"));

                        if (dtCL.Rows.Count > 0)
                        {
                            QRPSTA.STASummary clsXSum = clsXchart.mfDrawXChart(this.uChartX, dtValue, dblLSL, dblUSL, strSpecRange
                                                                            , true, Convert.ToDouble(dtCL.Rows[0]["XLCL"]), Convert.ToDouble(dtCL.Rows[0]["XUCL"]), true, true, "", ""
                                                        , "", m_resSys.GetString("SYS_FONTNAME"), 20, 10, 0);
                        }
                        else
                        {
                            QRPSTA.STASummary clsXSum = clsXchart.mfDrawXChart(this.uChartX, dtValue, dblLSL, dblUSL, strSpecRange, false, 0, 0, true, true, "", ""
                                                        , "", m_resSys.GetString("SYS_FONTNAME"), 20, 10, 0);
                        }
  
                    }
                }
                */
                #endregion

                ////// 이전 3개월 데이터 조회(평균값)
                ////QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                ////QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                ////brwChannel.mfCredentials(clsItem);

                ////DateTime dateInspectDate = Convert.ToDateTime(this.uDateInspectDate.Value);                                                     // 검사일
                ////DateTime dateFromDate = new DateTime(dateInspectDate.AddMonths(-3).Year, dateInspectDate.AddMonths(-3).Month, 1).AddDays(-1);   // 검사일의 전달로부터 3개월전 1일날짜
                ////DateTime dateToDate = new DateTime(dateInspectDate.Year, dateInspectDate.Month, 1).AddDays(-1);                                 // 검사일의 전달 마지막 날짜

                ////DataTable dtData = clsItem.mfReadINSProcInspectReqItem_Mean_Range_FromToDate(this.uComboPlantCode.Value.ToString(), this.uTextCustomerCode.Text, this.uTextPackage.Text
                ////                                                                    , this.uTextStackSeq.Text, this.uTextGeneration.Text
                ////                                                                    , this.uTextWorkProcessCode.Text, this.uTextEquipCode.Text
                ////                                                                    , dateFromDate.ToString("yyyy-MM-dd 22:00:00")
                ////                                                                    , dateToDate.ToString("yyyy-MM-dd 21:59:59"));

                // 2012.03.09 추가
                // 그래프에 표시될 측정데이터는 최근 30개를 보여준다
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
                QRPINS.BL.INSPRC.ProcInspectReqItem clsItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtMean = clsItem.mfReadINSProcInspectReqItem_Mean_Range_Top30(this.uComboPlantCode.Value.ToString(), this.uTextCustomerCode.Text, this.uTextPackage.Text
                                                                                        , this.uTextStackSeq.Text, this.uTextGeneration.Text
                                                                                        , this.uTextWorkProcessCode.Text, this.uTextEquipCode.Text
                                                                                        , Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd")
                                                                                        , Convert.ToDateTime(this.uDateInspectTime.Value).ToString("HH:mm:ss")
                                                                                        , e.Row.Cells["InspectItemCode"].Value.ToString());

                // 내림차순으로 정렬
                DataView dvMean = dtMean.DefaultView;
                dvMean.Sort = "InspectDate ASC, InspectTime ASC";
                dtMean = dvMean.ToTable();

                ////// 현재 줄의 검사항목만 Select
                ////string strInspectItemCode = e.Row.Cells["InspectItemCode"].Value.ToString();

                ////DataRow[] drRow = dtData.Select("InspectItemCode = '" + strInspectItemCode + "'", "InspectDate ASC, InspectTime ASC");

                ////DataTable dtMean = dtData.Clone();

                ////foreach (DataRow dr in drRow)
                ////{
                ////    dtMean.ImportRow(dr);
                ////}

                // 평균, 범위 값별로 데이터 테이블로 만든다
                DataTable dtMeanValue = dtMean.DefaultView.ToTable(false, "Mean");
                DataTable dtRangeValue = dtMean.DefaultView.ToTable(false, "DataRange");

                QRPSTA.STASPC clsXchart = new QRPSTA.STASPC();

                // 변수 설정
                double dblLSL = Convert.ToDouble(e.Row.Cells["LowerSpec"].Value);
                double dblUSL = Convert.ToDouble(e.Row.Cells["UpperSpec"].Value);
                double dblUCL = Convert.ToDouble(e.Row.Cells["UCL"].Value);
                double dblLCL = Convert.ToDouble(e.Row.Cells["LCL"].Value);
                double dblCL = Convert.ToDouble(e.Row.Cells["CL"].Value);
                string strSpecRange = e.Row.Cells["SpecRange"].Value.ToString();

                QRPSTA.STASummary clsXSum = clsXchart.mfDrawXBarChartWithRChart4SubGroupMean(this.uChartX, dtMeanValue, dtRangeValue
                                                                                            , dblLSL, dblUSL, strSpecRange
                                                                                            , true, dblLCL, dblCL, dblUCL, 5
                                                                                            , true, true, "", ""
                                                                                            , "", m_resSys.GetString("SYS_FONTNAME"), 20, 10, 0);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetChartClear()
        {
            try
            {
                QRPSTA.STASPC clsXchart = new QRPSTA.STASPC();
                clsXchart.mfInitControlChart(this.uChartX);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        // 검색조건 제품코드 팝업창이벤트
        private void uTextSearchProductCode_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0002 frmPOP = new frmPOP0002();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchProductCode.Text = frmPOP.ProductCode;
                this.uTextSearchProductName.Text = frmPOP.ProductName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // LotNo 텍스트박스 엔터키입력시 MES I/F 통해 기본정보를 받아온다
        private void uTextLotNo_KeyDown(object sender, KeyEventArgs e)
        {
            //try
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        // SystemInfo ResourceSet
            //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //        if(!this.uComboPlantCode.Value.ToString().Equals(string.Empty))
            //        {
            //            // LotNo 텍스트박스가 공백이 아닐경우
            //            if (!this.uTextLotNo.Text.ToString().Equals(string.Empty))
            //            {
            //                // 화일서버 연결정보 가져오기

            //                QRPBrowser brwChannel = new QRPBrowser();
            //                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
            //                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
            //                brwChannel.mfCredentials(clsSysAccess);
            //                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlantCode.Value.ToString(), "S04");

            //                // MES I/F
            //                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAccess);
            //                DataTable dtLotInfo = clsTibrv.LOT_INFO_REQ(this.uTextLotNo.Text.Trim());

            //                // 데이터가 있는경우
            //                if (dtLotInfo.Rows[0]["returncode"].ToString().Equals(string.Empty))
            //                {
            //                    ProcessCode = dtLotInfo.Rows[0]["OPERID"].ToString();
            //                    this.uTextProcessName.Text = dtLotInfo.Rows[0]["OPERDESC"].ToString();
            //                    this.uTextProductCode.Text = dtLotInfo.Rows[0]["PRODUCTSPECNAME"].ToString();
            //                    this.uTextProductName.Text = dtLotInfo.Rows[0]["PRODUCTSPECDESC"].ToString();
            //                    EquipCode = dtLotInfo.Rows[0]["EQPID"].ToString();
            //                    this.uTextEquipName.Text = dtLotInfo.Rows[0]["EQPDESC"].ToString();
            //                    this.uTextLotSize.Text = dtLotInfo.Rows[0]["QTY"].ToString();

            //                    // 고객정보 조회
            //                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
            //                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
            //                    brwChannel.mfCredentials(clsProduct);

            //                    DataTable dtCustomer = clsProduct.mfReadMaterialCustomer(this.uComboPlantCode.Value.ToString(), this.uTextProductCode.Text, m_resSys.GetString("SYS_LANG"));

            //                    if (dtCustomer.Rows.Count > 0)
            //                    {
            //                        this.uTextCustomerCode.Text = dtCustomer.Rows[0]["CustomerCode"].ToString();
            //                        this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();
            //                    }

            //                    // 제품코드로 Package 번호, 공정코드로 TrackInFlag 가져오기

            //                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqH), "ProcInspectReqH");
            //                    QRPINS.BL.INSPRC.ProcInspectReqH clsHeader = new QRPINS.BL.INSPRC.ProcInspectReqH();
            //                    brwChannel.mfCredentials(clsHeader);

            //                    DataTable dtCheck = clsHeader.mfReadINSProcInspectReq_InitCheck(this.uComboPlantCode.Value.ToString(), this.uTextProductCode.Text, ProcessCode);

            //                    // TrackInFlag에 따라서 TrackIn 버튼 활성화 설정
            //                  if (dtCheck.Rows.Count > 0)
            //                  {
            //                       if(dtCheck.Rows[0]["TrackInFlag"].ToString().Equals("T"))
            //                      this.uButtonTrackIn.Enabled = true;
            //                   }
            //                    // 공정검사 규격서에 데이터가 있는지 조회
            //                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
            //                    QRPISO.BL.ISOPRC.ProcessInspectSpecH clsInsSpecH = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
            //                    brwChannel.mfCredentials(clsInsSpecH);

            //                    DataTable dtSpecCheck = clsInsSpecH.mfReadISOProcessInspectSpecCheck(this.uComboPlantCode.Value.ToString(), dtCheck.Rows[0]["Package"].ToString());

            //                    if (dtSpecCheck.Rows.Count > 0)
            //                    {
            //                        // 샘플링그리드 컬럼설정
            //                        //InitSampleGrid();

            //                        // 공정검사 규격서에서 Data Load
            //                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqItem), "ProcInspectReqItem");
            //                        QRPINS.BL.INSPRC.ProcInspectReqItem clsReqItem = new QRPINS.BL.INSPRC.ProcInspectReqItem();
            //                        brwChannel.mfCredentials(clsReqItem);

            //                        DataTable dtItem = clsReqItem.mfReadINSProcInspectItem_Init(this.uComboPlantCode.Value.ToString(), ProcessCode
            //                            , this.uTextProductCode.Text, m_resSys.GetString("SYS_LANG"));

            //                        this.uGridItem.DataSource = dtItem;
            //                        this.uGridItem.DataBind();

            //                        this.uGridSampling.DataSource = dtItem;
            //                        this.uGridSampling.DataBind();

            //                        // Unbount 컬럼 삭제(기존 Xn 컬럼 삭제하기 위해)
            //                        this.uGridSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

            //                        this.uGridFault.DataSource = dtItem;
            //                        this.uGridFault.DataBind();

            //                        // 컬럼생성 메소드 호출
            //                        String[] strLastColKey = { "Mean", "InspectResultFlag" };
            //                        CreateColumn(this.uGridSampling, 0, "SampleSize", "ProcessSampleSize", strLastColKey);

            //                        // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
            //                        SetSamplingGridColumn();

            //                        // OCAP에 등록된 정보가 있는지 조회
            //                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.OCAP), "OCAP");
            //                        QRPINS.BL.INSSTS.OCAP clsOCAP = new QRPINS.BL.INSSTS.OCAP();
            //                        brwChannel.mfCredentials(clsOCAP);

            //                        DataTable dtOCAP = clsOCAP.mfReadINSProcOCAP_ForINSProcReq(this.uComboPlantCode.Value.ToString(), this.uTextLotNo.Text
            //                                                                                , ProcessCode, dtCheck.Rows[0]["Package"].ToString());
            //                        if (dtOCAP.Rows.Count > 0)
            //                        {
            //                            this.uTextEtcDesc.Text = dtOCAP.Rows[0]["ActionDesc"].ToString();
            //                        }
            //                    }
            //                    else
            //                    {
            //                        WinMessageBox msg = new WinMessageBox();
            //                        DialogResult Result = new DialogResult();

            //                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //                            , "규격서확인", "공정검사 규격서 조회 결과", "등록된 공정검사 규격서가 없습니다", Infragistics.Win.HAlign.Right);
            //                    }
            //                }
            //                else
            //                {
            //                    WinMessageBox msg = new WinMessageBox();
            //                    DialogResult Result = new DialogResult();

            //                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //                        , "MES I/F", "Lot정보 요청결과", dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            WinMessageBox msg = new WinMessageBox();
            //            DialogResult Result = new DialogResult();

            //            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
            //                , "입력확인", "공장 선택 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Right);

            //            this.uComboPlantCode.DropDown();
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //}
        }

        // 검사자 버튼클릭시 팝업창 이벤트
        private void uTextInspectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlantCode.Value.ToString().Equals(string.Empty) || uComboPlantCode.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uComboPlantCode.Value.ToString() != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = uComboPlantCode.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlantCode.Value.ToString() != frmPOP.PlantCode)
                    {
                        // SystemInfo ResourceSet
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                    , msg.GetMessge_Text("M001254", strLang) + this.uComboPlantCode.Text + msg.GetMessge_Text("M000001", strLang)
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextInspectUserID.Text = "";
                        this.uTextInspectUserName.Text = "";
                    }
                    else
                    {
                        this.uTextInspectUserID.Text = frmPOP.UserID;
                        this.uTextInspectUserName.Text = frmPOP.UserName;
                    }
                }
                else
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M000798", "M000275", "M000266"
                                                , Infragistics.Win.HAlign.Right);

                    this.uComboPlantCode.DropDown();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사자 키다운 이벤트
        private void uTextInspectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlantCode.Value.ToString() != "")
                        {
                            String strWriteID = this.uTextInspectUserID.Text;

                            // UserName 검색 함수 호출
                            DataTable dtUserInfo = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                            if (dtUserInfo.Rows.Count > 0)
                            {
                                this.uTextInspectUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextInspectUserID.Clear();
                                this.uTextInspectUserName.Clear();
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlantCode.DropDown();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID.TextLength <= 1 || this.uTextInspectUserID.SelectedText == this.uTextInspectUserID.Text)
                    {
                        this.uTextInspectUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void ultraTextEditor2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchInspectUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlantCode.Value.ToString() != "")
                        {
                            String strWriteID = this.uTextSearchInspectUserID.Text;

                            // UserName 검색 함수 호출
                            DataTable dtUserInfo = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                            if (dtUserInfo.Rows.Count > 0)
                            {
                                this.uTextSearchInspectUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchInspectUserID.Clear();
                                this.uTextSearchInspectUserName.Clear();
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlantCode.DropDown();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchInspectUserID.TextLength <= 1 || this.uTextSearchInspectUserID.SelectedText == this.uTextSearchInspectUserID.Text)
                    {
                        this.uTextSearchInspectUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void ultraTextEditor2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboPlantCode.Value.ToString();

                frmPOP.ShowDialog();

                this.uComboPlantCode.Value = frmPOP.PlantCode;
                this.uTextSearchInspectUserID.Text = frmPOP.UserID;
                this.uTextSearchInspectUserName.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비코드변경시 설비명 클리어
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextSearchEquipName.Text.Equals(string.Empty))
                this.uTextSearchEquipName.Clear();
        }

        /// <summary>
        /// 설비코드 직접입력조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //Enter Key만 적용
                if (!e.KeyData.Equals(Keys.Enter))
                    return;

                //설비코드가 공백이라면 리턴
                if (this.uTextSearchEquipCode.Text.Equals(string.Empty))
                    return;

                //공장정보 저장
                string strPlantCode = this.uComboSearchPlant.Value == null ? "" : this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                    return;


                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //Text에 입력한 설비정보 조회
                DataTable dtRtn = SearchEquipInfo(strPlantCode, this.uTextSearchEquipCode.Text, m_resSys.GetString("SYS_LANG"));

                //정보가 있다면 설비명 Text에 삽입, 없다면 메세지
                if (dtRtn.Rows.Count > 0)
                    this.uTextSearchEquipName.Text = dtRtn.Rows[0]["EquipName"].ToString();
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001075", "M000902"
                                , Infragistics.Win.HAlign.Right);

                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비코드 팝업창조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                string strPlantCode = this.uComboSearchPlant.Value == null ? "" : this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                    return;

                frmPOP0005 frmEquip = new frmPOP0005();
                frmEquip.PlantCode = strPlantCode;
                frmEquip.ShowDialog();

                if (frmEquip.EquipCode == null || frmEquip.EquipCode.Equals(string.Empty))
                    return;

                this.uTextSearchEquipCode.Text = frmEquip.EquipCode;
                this.uTextSearchEquipName.Text = frmEquip.EquipName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 작업자 ID 컬럼에서 사번입력하고 엔터버튼 클릭시 이름 가져오는 이벤트
        private void uGridFaultData_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridFaultData.ActiveCell == null)
                    return;

                if (this.uGridFaultData.ActiveCell.Column.Key == "WorkUserID")
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uGridFaultData.ActiveCell.Value.ToString() != "")
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            if (this.uComboPlantCode.Value.ToString() != "")
                            {
                                String strWriteID = this.uGridFaultData.ActiveCell.Value.ToString();

                                // UserName 검색 함수 호출
                                DataTable dtUserInfo = GetUserInfo(this.uComboPlantCode.Value.ToString(), strWriteID);

                                if (dtUserInfo.Rows.Count > 0)
                                {
                                    this.uGridFaultData.ActiveCell.Row.Cells["WorkUserName"].Value = dtUserInfo.Rows[0]["UserName"].ToString();
                                }
                                else
                                {
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000621",
                                                Infragistics.Win.HAlign.Right);

                                    this.uGridFaultData.ActiveCell.Row.Cells["WorkUserName"].Value = "";
                                    this.uGridFaultData.ActiveCell.Value = "";
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000266",
                                                Infragistics.Win.HAlign.Right);

                                this.uComboPlantCode.DropDown();
                            }
                        }
                    }

                    if (e.KeyCode == Keys.Back)
                    {
                        this.uGridFaultData.ActiveCell.Value = "";
                        this.uGridFaultData.ActiveCell.Row.Cells["WorkUserName"].Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성완료 체크시 이후 수정할수 없다는 메세지 보여주는 이벤트
        private void uCheckCompleteFlag_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                // CheckEditor 가 활성화 상태일때
                if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true)
                {
                    // TrackIn 버튼이 활성화 상태일때
                    if (this.uButtonTrackIn.Enabled == true)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000149", "M000151",
                                        Infragistics.Win.HAlign.Right);

                        this.uCheckCompleteFlag.Checked = false;
                    }
                    //else
                    //{
                    //    WinMessageBox msg = new WinMessageBox();
                    //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                    "확인창", "작성완료 확인", "작성완료 체크후 저장시 이후 수정할 수 없습니다",
                    //                    Infragistics.Win.HAlign.Right);
                    //}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                SetSearchComboProcess();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        private void uComboSearchDetailProcType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                SetSearchComboProcess();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형 선택 합부판정(Bom인경우 Bom테이블에 자재코드가 존재하면 OK아니면 NG BomCheckFlag가 false이면 기존방식대로 선택
        /// </summary>
        /// <param name="e"></param>
        private void JudgementSelect(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;
                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                //int intLastIndex = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value)) + intStart;
                int intSampleSize = (Convert.ToInt32(e.Cell.Row.Cells["ProcessSampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value));
                if (intSampleSize > 99)
                    intSampleSize = 99;
                int intLastIndex = intSampleSize + intStart;

                // BomCheckFlag 확인
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType clsType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(clsType);

                string strPlantCode = this.uComboPlantCode.Value.ToString();
                string strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                string strInspectTypeCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();

                DataTable dtBomCheck = clsType.mfReadMASInspectType_BomChecFlag(strPlantCode, strInspectGroupCode, strInspectTypeCode);

                // BomCheckFlag 가 True 이면
                if (dtBomCheck.Rows[0]["BomCheckFlag"].ToString().Equals("T"))
                {
                    bool bolCheck = true;
                    int intFaultCount = 0;
                    // Loop 돌며 결과값 검사
                    for (int i = intStart; i < intLastIndex; i++)
                    {
                        if (!e.Cell.Row.Cells[i].Value.ToString().Equals(string.Empty))
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqItem), "CCSInspectReqItem");
                            QRPCCS.BL.INSCCS.CCSInspectReqItem clsItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                            brwChannel.mfCredentials(clsItem);

                            DataTable dtInspectResult = clsItem.mfReadCCSInspectReqItem_DataTypeSelectInspectResult_MASBOM(strPlantCode, this.uTextProductCode.Text
                                                                                                                        , this.uTextWorkProcessCode.Text, e.Cell.Row.Cells[i].Value.ToString());
                            if (dtInspectResult.Rows[0]["InspectResult"].ToString().Equals("NG"))
                            {
                                bolCheck = false;
                                //break;
                                intFaultCount += 1;
                            }
                        }
                    }

                    if (bolCheck)
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                        e.Cell.Row.Appearance.BackColor = Color.Empty;
                    }
                    else
                    {
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                        e.Cell.Row.Appearance.BackColor = Color.Salmon;
                    }

                    e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void uTextFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0042");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextFile.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFile.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }














    }
}
