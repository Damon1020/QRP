﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINS0017.cs                                         */
/* 프로그램명   : 원자재 이상발생 관리                                  */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-07-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-02 : 기능 추가 (이종호)                       */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

// 파일첨부를 위한 Using 추가
using System.IO;
using System.Collections;

namespace QRPINS.UI
{
    public partial class frmINS0017 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        #region 수입검사 화면에서 넘겨받을 변수 / 전역변수 속성
        private string m_strPlantCode;
        private string m_strReqNo;
        private string m_strReqSeq;
        private int m_intReqLotSeq;
        private int m_intReqItemSeq;
        private string m_strMoveFormName;

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string ReqNo
        {
            get { return m_strReqNo; }
            set { m_strReqNo = value; }
        }

        public string ReqSeq
        {
            get { return m_strReqSeq; }
            set { m_strReqSeq = value; }
        }

        public int ReqLotSeq
        {
            get { return m_intReqLotSeq; }
            set { m_intReqLotSeq = value; }
        }

        public int ReqItemSeq
        {
            get { return m_intReqItemSeq; }
            set { m_intReqItemSeq = value; }
        }

        public string MoveFormName
        {
            get { return m_strMoveFormName; }
            set { m_strMoveFormName = value; }
        }
        #endregion

        public frmINS0017()
        {
            InitializeComponent();
        }

        private void frmINS0017_Activated(object sender, EventArgs e)
        {
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, true, false, true, true, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        // Grid 변경사항 저장
        private void frmINS0017_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINS0017_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("원자재 이상발생 관리", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitTab();
            InitButton();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
            InitGroupBox();

            if (MoveFormName == "frmINS0004")
            {
                QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
                brw.mfSetFormLanguage(this);

                CommonControl cControl = new CommonControl();
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                // Tab 명 변경
                if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0017"))
                {
                    uTabMenu.Tabs["QRPINS" + "," + "frmINS0017"].Text = titleArea.TextName;
                }

                titleArea.TextName = titleArea.TextName + "(" + uTabMenu.Tabs["QRPINS,frmINS0004"].Text + ")";
                //titleArea.TextName = titleArea.TextName + "(수입검사등록 이동)";

                this.uGroupBoxContentsArea.Expanded = true;

                // 데이터 조회 Method 호출
                InitDataFromMatInspectReq();

                // 공정이상 탭 히든처리
                this.ultraTabControl1.Tabs[1].Visible = false;
            }
            else
            {
                this.uGroupBoxContentsArea.Expanded = false;
            }

            // Grid 설정값 Load
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            // E-Mail TEST
            //if (m_SysRes.GetString("SYS_USERID").Equals("TESTUSER"))
            //    this.ultraButton1.Visible = true;
            //else
            //    this.ultraButton1.Visible = false;
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method

        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.ultraTabControl1, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                wTab.mfInitGeneralTabControl(this.uTabAction, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRegistDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchFaultSeparation, "이상발생구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMgnNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialGroup, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchFinalCompleteFlag, "최종완료포함", m_resSys.GetString("SYS_FONTNAME"), true, false);

                //uGroupBoxContentsArea의 Label
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMgnNum, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterialSpec1, "자재규격#1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMoldSeq, "금형차수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAbnormalType, "이상발생구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLalbeTotalQty, "입고수량", m_resSys.GetString("SYS_FONTNAME"), true, false);               
                wLabel.mfSetLabel(this.uLabelGRDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStatus, "상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile1, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile2, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteFlag, "작성완료", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelInspectDesc, "이상발생내용", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ultraLabel3, "발생공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ultraLabel6, "발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelAttachDate1, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachDate2, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelShortUserName, "접수자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelShortActionDesc, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelShortEmail, "E-Mail", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelShortConfirm, "승인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelShortConfirmUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelShortConfirmDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelLongUserName, "접수자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLongActionDesc, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLongEmail, "E-Mail", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLongConfirm, "승인", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLongConfirmUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLongConfirmDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelFinalCompleteFlag, "최종완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterialTreate, "자재처리결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchPlant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
                
                // 이상발생구분
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtComCode = clsComCode.mfReadCommonCode("C0034", m_resSys.GetString("SYS_LANG"));

                // 검색조건
                wCombo.mfSetComboEditor(this.uComboSearchAbnomalType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodename", dtComCode);

                // 공통정보 그룹박스내 이상발생구분 콤보박스
                wCombo.mfSetComboEditor(this.uComboAbnormalType, false, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", ""
                    , "ComCode", "ComCodename", dtComCode);

                // 편집불가 상태로
                this.uComboAbnormalType.ReadOnly = true;

                // 상태
                dtComCode = clsComCode.mfReadCommonCode("C0035", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboStatus, false, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", ""
                , "ComCode", "ComCodename", dtComCode);

                // 자재종류
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsum = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsConsum);
                DataTable dtConsum = clsConsum.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchMaterialGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                , "ConsumableTypeCode", "ConsumableTypeName", dtConsum);

                // 상태 발행으로 고정
                this.uComboStatus.Value = "1";
                this.uComboStatus.ReadOnly = true;

                this.uComboAriseProcess.Items.Clear();
                this.uComboAriseProcess.Items.Add("", "선택");
                this.uComboAriseProcess.Value = "";
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        ///<summary>
        ///GroupBox 초기화
        ///</summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "수입이상발생 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "공정이상발생 정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.INFO, "공통정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonFileDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
                wButton.mfSetButton(this.uButtonDelete1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDelete2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonMoveDisplay, "수입검사등록 이동", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                QRPBrowser brwChannel = new QRPBrowser();

                #region 조회 그리드
                //일반설정
                wGrid.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridHeader, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridHeader, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "StdNumber", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "WriteDate", "등록일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "AbnormalType", "이상발생구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "StatusFlag", "상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "VendorName", "거래처명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ConsumableTypeCode", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridHeader, 0, "MaterialSpec1", "자재규격#1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridHeader, 0, "MoldSeq", "금형차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "TotalQty", "입고수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnn", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ShortAcceptDate", "단기조치 접수일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "LongAcceptDate", "장기조치 접수일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridHeader, 0, "AriseProcessCode", "발생공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MESTFlag", "MES전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "CompleteFlag", "작성완료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "FinalCompleteFlag", "최종완료", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // FontSize
                this.uGridHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region 수입이상발생 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridMatInspect, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                
                // 컬럼추가
                //wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "ReqNo", "의뢰번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "ReqSeq", "의뢰순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "ReqLotSeq", "의뢰Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "ReqItemSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "ReqFileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspect, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                // Set FontSize
                this.uGridMatInspect.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridMatInspect.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridMatInspectFault, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "ReqNo", "의뢰번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "ReqSeq", "의뢰순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "ReqLotSeq", "의뢰Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "ReqItemSeq", "Item순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "FaultSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "ReqLotSeq", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "InspectQty", "검사수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");
                
                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "FaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "DeleteFlag", "삭제여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");

                wGrid.mfSetGridColumn(this.uGridMatInspectFault, 0, "FileData", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 300, false, true, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Default, "", "", null);

                // Set FontSize
                this.uGridMatInspectFault.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridMatInspectFault.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridMatInspectFault.DisplayLayout.Override.RowSelectorNumberStyle = Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle.ListIndex;
                #endregion

                #region 공정이상 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridProcInspect, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "ReqNo", "의뢰번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "ReqSeq", "의뢰순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "ReqLotSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "ReqItemSeq", "의뢰Item순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "ProcessFaultFlag", "공정이상발생여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspect, 0, "AbnormalResultFlag", "상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridProcInspect.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcInspect.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridProcInspect.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridProcInspectFault, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                
                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "ReqNo", "의뢰번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "ReqSeq", "의뢰순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "ReqLotSeq", "의뢰Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "ReqItemSeq", "Item순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "FaultSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "InspectQty", "검사수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "FaultQty", "불량수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "FaultTypeCode", "불량유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "DeleteFlag", "삭제여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 10, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");

                wGrid.mfSetGridColumn(this.uGridProcInspectFault, 0, "FileData", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 300, false, true, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Default, "", "", null);

                // Set FontSize
                this.uGridProcInspectFault.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcInspectFault.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                // DropDown 설정
                // 검사결과
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtComCode = clsComCode.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));
                // 검사결과
                wGrid.mfSetGridColumnValueList(this.uGridMatInspect, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);
                wGrid.mfSetGridColumnValueList(this.uGridProcInspect, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);

                // 불량유형 DropDown 설정
                // 수입이상
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsFaultType);

                DataTable dtFaultType_Mat = clsFaultType.mfReadInspectFaultTypeCombo_WithInspectFaultGubun(m_resSys.GetString("SYS_PLANTCODE"), "1", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridMatInspectFault, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType_Mat);

                // 공정이상
                // 불량유형 DropDown 설정
                DataTable dtFaultType_Proc = clsFaultType.mfReadInspectFaultTypeCombo_WithInspectFaultGubun(m_resSys.GetString("SYS_PLANTCODE"), "2", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridProcInspectFault, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType_Proc);
                
                byte[] empty = { };
                this.uGridMatInspectFault.DisplayLayout.Bands[0].Columns["FileData"].DataType = typeof(byte[]);
                this.uGridMatInspectFault.DisplayLayout.Bands[0].Columns["FileData"].DefaultCellValue = empty;
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Columns["FileData"].DataType = typeof(byte[]);
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Columns["FileData"].DefaultCellValue = empty;


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Value 초기화
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 검색조건 등록일자 = 오늘날짜
                this.uDateSearchFromWriteDate.Value = DateTime.Now;
                this.uDateSearchToWriteDate.Value = DateTime.Now;
                this.uDateWriteDate.Value = DateTime.Now;

                // TextBox MaxLength 지정
                this.uTextSearchMaterialCode.MaxLength = 20;
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextSearchStdNumber.MaxLength = 20;
                this.uTextWriteUserID.MaxLength = 20;
                this.uTextLotNo.MaxLength = 50;
                this.uTextCompleteFlag.Text = "F";
                this.uTextMESTFlag.Text = "F";

                this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");  

                this.uTextTotalQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        // 조회
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건 DataTable 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsHeader);

                // 검색조건용 DataTable 설정
                DataTable dtSearch = clsHeader.mfSetSearchDataInfo();
                DataRow drRow = dtSearch.NewRow();
                drRow["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                drRow["StdNumber"] = this.uTextSearchStdNumber.Text;
                drRow["VendorCode"] = this.uTextSearchVendorCode.Text;
                drRow["MaterialCode"] = this.uTextSearchMaterialCode.Text;
                drRow["AbnormalType"] = this.uComboSearchAbnomalType.Value.ToString();
                drRow["FromWriteDate"] = Convert.ToDateTime(this.uDateSearchFromWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["ToWriteDate"] = Convert.ToDateTime(this.uDateSearchToWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["ConsumableTypeCode"] = this.uComboSearchMaterialGroup.Value.ToString();
                if (this.uCheckSearchFinalCompleteFlag.Checked == true)
                {
                    drRow["FinalCompleteFlag"] = "T";
                }
                else
                {
                    drRow["FinalCompleteFlag"] = "F";
                }
                drRow["Lang"] = m_resSys.GetString("SYS_LANG");
                dtSearch.Rows.Add(drRow);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // Method 호출
                DataTable dtRtn = clsHeader.mfReaeINSMaterialAbnormalH(dtSearch);

                this.uGridHeader.DataSource = dtRtn;
                this.uGridHeader.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtn.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                else
                {
                    for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                    {
                        //이상발행구분이 공정이상, MES전송여부가 F 작성완료가 T 인경우 
                        if (this.uGridHeader.Rows[i].Cells["AbnormalType"].Value.ToString().Equals("2") && this.uGridHeader.Rows[i].Cells["MESTFlag"].Value.ToString().Equals("F")
                            && this.uGridHeader.Rows[i].Cells["CompleteFlag"].Value.ToString().Equals("T"))
                        {
                            //색을 변경한다.
                            this.uGridHeader.Rows[i].Appearance.BackColor = Color.Salmon;
                        }
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridHeader, 0);
                }
                // 상세정보창 접힘상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                string strLang = m_resSys.GetString("SYS_LANG");

                #region 필수입력사항및작성상태확인
                // 필수사항 체크 & 작성상태 확인
                if (this.uTextPlant.Text == "" && this.uTextStdNumber.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000882", "M000397", Infragistics.Win.HAlign.Right);

                    //mfSearch();
                    return;
                }
                else if (this.uTextWriteName.Text.Trim().Equals(string.Empty))
                {
                    //등록자입력
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001032", "M000386", Infragistics.Win.HAlign.Right);

                    //mfSearch();
                    return;
                }
                //else if (this.uTextCompleteFlag.Text.Equals("T") && this.uCheckCompleteFlag.Checked == true && this.uCheckCompleteFlag.Enabled == false)
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                        , "확인창", "작성완료 확인", "작성완료된 정보는 저장할수 없습니다", Infragistics.Win.HAlign.Center);

                //    //mfSearch();
                //    return;
                //}
                //공정이상발생정보(작성완료 & MES전송 완료 상태일 경우 메세지 박스를 띄운다)
                //else if (this.uTextCompleteFlag.Text.Equals("T") && this.uTextMESTFlag.Text.Equals("T") && this.uTextWMSTFlag.Text.Equals("T"))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                        , "확인창", "작성완료 확인", "작성완료된 정보는 저장할수 없습니다", Infragistics.Win.HAlign.Center);

                //    //mfSearch();
                //    return;
                //}
                else if (!this.uComboAbnormalType.Value.ToString().Equals("1") && !this.uComboAbnormalType.Value.ToString().Equals("2"))
                {
                    //수입/공정 이상발생이 아닙니다.
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000855", "M000747", Infragistics.Win.HAlign.Right);

                    //mfSearch();
                    return;
                }
                else if (this.uComboAbnormalType.Value.ToString().Equals("1"))
                {
                    if (this.uCheckFinalCompleteFlag.Enabled == false && this.uTextWMSTFlag.Text.Equals("T"))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001369", "M001370", Infragistics.Win.HAlign.Right);

                        //mfSearch();
                        return;
                    }
                    if (!(this.uGridMatInspectFault.Rows.Count > 0))
                    {
                        //수입이상 불량정보를 입력해주세요.
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000614", "M000756", Infragistics.Win.HAlign.Right);

                        //mfSearch();
                        return;
                    }
                    else if (this.uGridMatInspectFault.Rows.Count > 0)
                    {
                        this.uGridMatInspectFault.ActiveCell = this.uGridMatInspectFault.Rows[0].Cells[0];

                        for (int i = 0; i < uGridMatInspectFault.Rows.Count; i++)
                        {
                            if (this.uGridMatInspectFault.Rows[i].Cells["ReqLotSeq"].Value.Equals(0))
                            {
                                //LotNo를 선택해 주세요.
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000614", "M000074", Infragistics.Win.HAlign.Right);

                                this.uGridMatInspectFault.Rows[i].Cells["ReqLotSeq"].Activate();
                                this.uGridMatInspectFault.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            //불량유형 선택안함
                            if (this.uGridMatInspectFault.Rows[i].Cells["FaultTypeCode"].Value.ToString().Equals(string.Empty))
                            {
                                DialogResult result;

                                result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "M001264", "M001230", "M001408",
                                                                Infragistics.Win.HAlign.Right);

                                this.uGridMatInspectFault.ActiveCell = this.uGridMatInspectFault.Rows[i].Cells["FaultTypeCode"];
                                this.uGridMatInspectFault.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;


                            }
                        }
                    }
                }
                else if (this.uComboAbnormalType.Value.ToString().Equals("2"))
                {
                    if (this.uCheckFinalCompleteFlag.Enabled == false && this.uTextMESTFlag.Text.Equals("T") && this.uTextWMSTFlag.Text.Equals("T"))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001369", "M001370", Infragistics.Win.HAlign.Right);

                        //mfSearch();
                        return;
                    }
                    if (this.uComboAriseProcess.Value.ToString().Equals(string.Empty)) //발생공정을 선택해 주세요.
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000882", "M000432", Infragistics.Win.HAlign.Right);

                        this.uComboAriseProcess.DropDown();
                        return;
                    }

                    if (!(this.uGridProcInspectFault.Rows.Count > 0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000614", "M001371", Infragistics.Win.HAlign.Right);

                        return;
                    }

                    if (this.uGridProcInspectFault.Rows.Count > 0)
                    {
                        this.uGridProcInspectFault.ActiveCell = this.uGridProcInspectFault.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridProcInspectFault.Rows.Count; i++)
                        {
                            if (!this.uGridProcInspectFault.Rows[i].Hidden)
                            {
                                //불량유형 선택안함
                                if (this.uGridProcInspectFault.Rows[i].Cells["FaultTypeCode"].Value.ToString().Equals(string.Empty))
                                {
                                    DialogResult result;

                                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001264", "M001230", "M001408",
                                                                    Infragistics.Win.HAlign.Right);

                                    this.uGridProcInspectFault.ActiveCell = this.uGridProcInspectFault.Rows[i].Cells["FaultTypeCode"];
                                    this.uGridProcInspectFault.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;


                                }
                            }
                        }
                    }
                }
                if (this.uOptLongConfirm.Value != null) //장기조치사항 승인 클릭시 승인자,확인
                {
                    if (this.uTextLongConfirmUserID.Text.Equals(string.Empty) || this.uTextLongConfirmUserName.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001234", "M000770", Infragistics.Win.HAlign.Right);

                        this.uTabAction.Tabs[1].Selected = true;
                        this.uTextLongConfirmUserID.Focus();
                        return;
                    }
                }
                else if(this.uOptShortConfirm.Value != null) //단기조치사항 승인 클릭시 승인자,확인
                {
                    if(this.uTextShortConfirmUserID.Text.Equals(string.Empty) || this.uTextShortConfirmUserName.Text.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001234", "M000770", Infragistics.Win.HAlign.Right);

                        this.uTabAction.Tabs[0].Selected = true;
                        this.uTextShortConfirmUserID.Focus();
                        return;
                    }
                }
                
                if(this.uCheckFinalCompleteFlag.Checked && this.uCheckFinalCompleteFlag.Enabled) // 이미최종완료된 정보가 아니고 최종완료Flag가 체크되어 있다면
                {                                                                                     // 단기 장기 승인확인 작성완료 WMSFlag,MESFlag 확인
                    if (!this.uCheckCompleteFlag.Checked) //작성완료
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000984", "M001372", Infragistics.Win.HAlign.Right);

                        return;

                    }
                    if (this.uComboAbnormalType.Value.ToString().Equals("1"))
                    {
                        if (!this.uTextWMSTFlag.Text.Equals("T")) //MES,WMS
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001373", "M001374", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }
                    if (this.uComboAbnormalType.Value.ToString().Equals("2"))
                    {
                        if (!this.uTextMESTFlag.Text.Equals("T") || !this.uTextWMSTFlag.Text.Equals("T")) //MES,WMS
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001373", "M001375", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }
                    if ((this.uOptLongConfirm.Value == null || this.uTextLongUserName.Text.Equals(string.Empty)) //장기
                        || (this.uOptShortConfirm.Value == null || this.uTextShortUserName.Text.Equals(string.Empty)))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001376", "M001377", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
                

                #endregion

                #region SaveMessage

                string strmsg = "";
                if (this.uComboAbnormalType.Value.ToString().Equals("1"))
                {
                    // 작성완료가 되어있고 WMSFlag가 F인경우 
                    if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled == true && this.uTextWMSTFlag.Text.Equals("F"))
                    {
                        strmsg = "작성완료 체크후 저장하시면 수입이상정보를 수정할 수 없습니다. 저장하시겠습니까?";
                    }
                    // 수입이상발생정보(WMSFlag 가 F 작성완료가 완료상태일 경우)
                    else if (this.uTextWMSTFlag.Text.Equals("F") && this.uTextCompleteFlag.Text.Equals("T"))
                    {
                        strmsg = "WMS I/F가 완료되지 않았습니다. WMS전송처리를 하시겠습니까?";
                    }
                    else if (this.uTextWMSTFlag.Text.Equals("T") && this.uTextCompleteFlag.Text.Equals("T"))
                    {
                        strmsg = "최종완료 체크후 저장하시면 차후 수정 수정할 수 없습니다. 저장하시겠습니까?";
                    }
                    else
                    {
                        strmsg = msg.GetMessge_Text("M000936",strLang);
                    }
                }
                else if (this.uComboAbnormalType.Value.ToString().Equals("2"))
                {
                    // 작성완료가 되어있고 MESFlag가 F인경우 
                    if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled == true && this.uTextWMSTFlag.Text.Equals("F"))
                    {
                        strmsg = msg.GetMessge_Text("M001381",strLang);
                    }
                    else if (this.uTextWMSTFlag.Text.Equals("F") && this.uTextCompleteFlag.Text.Equals("T") && this.uTextMESTFlag.Text.Equals("F"))
                    {
                        strmsg = msg.GetMessge_Text("M001382",strLang);
                    }
                    // 공정이상발생정보(MESFlag 가 F 작성완료가 완료상태일 경우)
                    else if (this.uTextWMSTFlag.Text.Equals("F") && this.uTextCompleteFlag.Text.Equals("T") && this.uTextMESTFlag.Text.Equals("T"))
                    {
                        strmsg = msg.GetMessge_Text("M001379",strLang);
                    }
                    else if (this.uTextWMSTFlag.Text.Equals("T") && this.uTextCompleteFlag.Text.Equals("T") && this.uTextMESTFlag.Text.Equals("F"))
                    {
                        strmsg = msg.GetMessge_Text("M001383",strLang);
                    }
                    else if (this.uTextWMSTFlag.Text.Equals("T") && this.uTextCompleteFlag.Text.Equals("T") && this.uTextMESTFlag.Text.Equals("T")
                            && this.uCheckFinalCompleteFlag.Checked && this.uCheckFinalCompleteFlag.Enabled)
                    {
                        strmsg = msg.GetMessge_Text("M001380",strLang);
                    }
                    else
                    {
                        strmsg = msg.GetMessge_Text("M000936",strLang);
                    }
                }
                
                #endregion

                // 저장여부를 묻는다
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001053",strLang), strmsg, Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // 최종 작성완료 상태가 아니면 데이터 저장후 WMS/MES 전송 작성완료상태면 데이터 저장 없이 WMS/MES 전송
                    if (this.uCheckFinalCompleteFlag.Enabled == true)
                    {
                        DataTable dtSaveHeader = new DataTable();
                        DataTable dtSaveDetail = new DataTable();
                        DataTable dtFault = new DataTable();
                        ////// 상세 정보
                        ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                        ////QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                        ////brwChannel.mfCredentials(clsDetail);

                        ////// 불량유형정보
                        ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalDFault), "MaterialAbnormalDFault");
                        ////QRPINS.BL.INSIMP.MaterialAbnormalDFault clsFault = new QRPINS.BL.INSIMP.MaterialAbnormalDFault();
                        ////brwChannel.mfCredentials(clsFault);

                        ////DataRow drRow;

                        // 수입이상발생정보인지 공정이상발생정보인지 구분
                        if (this.uComboAbnormalType.Value.ToString().Equals("1"))
                        {
                            if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled) // 2012-04-23
                                dtSaveHeader = Rtn_dtHeader_Mat();
                            else
                                dtSaveHeader = Rtn_dtHeader_Mat();
                                dtSaveDetail = Rtn_dtDetail_Mat();
                                dtFault = Rtn_dtDetailFault_Mat();
                        }
                        else if (this.uComboAbnormalType.Value.ToString().Equals("2"))
                        {
                            if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled) //2012-04-23
                                dtSaveHeader = Rtn_dtHeader_Proc();
                            else
                                dtSaveHeader = Rtn_dtHeader_Proc();
                                dtSaveDetail = Rtn_dtDetail_Proc();
                                dtFault = Rtn_dtDetailFault_Proc();
                        }

                        // 헤더
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                        QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                        brwChannel.mfCredentials(clsHeader);

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsHeader.mfSaveINSMaterialAbnormalH_ToolSave(dtSaveHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP")
                                                                                        , dtSaveDetail, dtFault);

                        // 결과확인
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 성공시
                        if (ErrRtn.ErrNum == 0)
                        {
                            // OUTPUT 관리번호 저장
                            string strStdNumber = ErrRtn.mfGetReturnValue(0);
                            
                            this.uTextStdNumber.Text = strStdNumber;

                            FileUpload_Header(strStdNumber); //파일업로드형식변경됨 (주석) 2012-03-23

                            // 메일 전송 메소드 호출
                            SendMail();

                            // 수입이상 이었을경우 첨부파일 업로드
                            if (strStdNumber.Substring(0, 4).Equals("AE02"))
                            {
                                //FileUpload(this.uGridMatInspectFault, strStdNumber); 파일업로드형식변경됨 (주석) 2012-03-23
                                Send_WMS_Mat(PlantCode, strStdNumber, ReqNo, ReqSeq);
                            }
                            else if (strStdNumber.Substring(0, 4).Equals("AE03"))
                            {
                                //FileUpload(this.uGridProcInspectFault, strStdNumber); 파일업로드형식변경됨 (주석) 2012-03-23
                                Send_WMS_MES_Proc(PlantCode, strStdNumber);
                            } 
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001037", "M000953", Infragistics.Win.HAlign.Center);
                        }
                        
                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    else if (this.uCheckCompleteFlag.Checked)
                    {
                        // 메일 전송 메소드 호출 임시주석
                        SendMail();

                        string strStdNumber = this.uTextStdNumber.Text;

                        if (strStdNumber.Substring(0, 4).Equals("AE02"))
                        {
                            Send_WMS_Mat(PlantCode, strStdNumber, ReqNo, ReqSeq);
                        }
                        else if (strStdNumber.Substring(0, 4).Equals("AE03"))
                        {
                            Send_WMS_MES_Proc(PlantCode, strStdNumber);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        // 삭제
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uCheckCompleteFlag.Checked == true)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000978", Infragistics.Win.HAlign.Center);

                    return;
                }
                
                // 삭제에 필요한 정보가 입력되었는지 확인
                if (this.uTextStdNumber.Text == "" || m_strPlantCode == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    //mfSearch();
                    return;
                }
                if(this.uGridMatInspectFault.Rows.Count <=0 && this.uGridProcInspectFault.Rows.Count <= 0)
                {
                    string strMess = "";
                    if (this.uComboAbnormalType.Value.ToString().Equals("1"))
                        strMess = "M000756";
                    else if (this.uComboAbnormalType.Value.ToString().Equals("2"))
                        strMess = "M001384";
                    else
                        return;

                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000614", strMess, Infragistics.Win.HAlign.Center);

                    //mfSearch();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                        QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                        brwChannel.mfCredentials(clsHeader);

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        DataTable dtSaveHeader = new DataTable();
                       DataTable dtSaveDetail = new DataTable();
                        DataTable    dtFault = new DataTable();
                       if (this.uComboAbnormalType.Value.ToString().Equals("1"))
                        {
                            dtSaveHeader = Rtn_dtHeader_Mat();
                            //dtSaveDetail = Rtn_dtDetail_Mat();
                            dtFault = Rtn_dtDetailFault_Mat();
                        }
                        else if (this.uComboAbnormalType.Value.ToString().Equals("2"))
                        {
                            dtSaveHeader = Rtn_dtHeader_Proc();
                           //dtSaveDetail = Rtn_dtDetail_Proc();
                            dtFault = Rtn_dtDetailFault_Proc();
                        }

                       if (dtFault.Rows.Count > 0)
                       {
                           for (int i = 0; i < dtFault.Rows.Count; i++)
                           {
                               dtFault.Rows[i]["DeleteFlag"] = "T";
                           }
                       }

                        // 삭제 Method 호출
                        String strErrRtn = clsHeader.mfSaveINSMaterialAbnormalH_Del(dtSaveHeader,m_resSys.GetString("SYS_USERID"),m_resSys.GetString("SYS_USERIP"),dtFault);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000638", strLang)
                                                            , ErrRtn.ErrMessage
                                                            ,Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;

                    this.ultraTabControl1.Tabs[1].Selected = true;
                    this.uTextLotNo.Focus();
                }
                else
                {
                    Clear();
                }
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        
        #endregion

        #region Method..
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수입검사로부터 화면이동되었을시 기본 데이터 가져오는 Method
        /// </summary>
        private void InitDataFromMatInspectReq()
        {
            try
            {
                // SystemInfo RecourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                brwChannel.mfCredentials(clsDetail);

                // 이미 등록된 자료인지 확인
                DataTable dtCheck = clsDetail.mfReadINSMaterialAbnormalD_FindStdNum(PlantCode, ReqNo, ReqSeq);

                if (dtCheck.Rows.Count > 0)
                {
                    string strStdNumber = dtCheck.Rows[0]["StdNumber"].ToString();

                    SearchHeaderDetail(strStdNumber);
                    SearchDetail_Mat(strStdNumber);

                    this.uTextLotNo.Enabled = false;
                    this.uComboAriseProcess.Enabled = false;
                    this.uDateAriseDate.Enabled = false;
                }
                else
                {
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                    QRPINS.BL.INSIMP.MatInspectReqH clsReqH = new QRPINS.BL.INSIMP.MatInspectReqH();
                    brwChannel.mfCredentials(clsReqH);

                    // ProgressBar 생성
                    //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    //Thread threadPop = m_ProgressPopup.mfStartThread();
                    //m_ProgressPopup.mfOpenProgressPopup(this, "조회중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // Method 호출
                    // 헤더정보
                    DataTable dtReqH = clsReqH.mfReadInsMatInspectReqHDetail(PlantCode, ReqNo, ReqSeq, m_resSys.GetString("SYS_LANG"));
                    // Item 정보
                    DataTable dtDetail = clsDetail.mfReadINSMaterialAbnormalD_Mat(PlantCode, "", ReqNo, ReqSeq, m_resSys.GetString("SYS_LANG"));

                    // ProgressBar Close
                    this.MdiParent.Cursor = Cursors.Default;
                    //m_ProgressPopup.mfCloseProgressPopup(this);

                    if (dtReqH.Rows.Count > 0 && dtDetail.Rows.Count > 0)
                    {
                        this.uTextPlant.Text = dtReqH.Rows[0]["PlantName"].ToString();
                        this.uTextVendorCode.Text = dtReqH.Rows[0]["VendorCode"].ToString();
                        this.uTextVendorName.Text = dtReqH.Rows[0]["VendorName"].ToString();
                        this.uTextMaterialCode.Text = dtReqH.Rows[0]["MaterialCode"].ToString();
                        this.uTextMaterialName.Text = dtReqH.Rows[0]["MaterialName"].ToString();
                        //this.uTextMaterialSpec1.Text = dtReqH.Rows[0]["MaterialSpec1"].ToString();
                        this.uTextMoldSeq.Text = dtReqH.Rows[0]["MoldSeq"].ToString();
                        this.uTextGRDate.Text = dtReqH.Rows[0]["GRDate"].ToString();
                        this.uTextTotalQty.Text = Math.Floor(Convert.ToDecimal(dtReqH.Rows[0]["TotalQty"].ToString())).ToString();
                        // 이상발생구분 수입이상으로
                        this.uComboAbnormalType.Value = "1";

                        // 수입이상 발생정보 그리드 DataBinding
                        this.uGridMatInspect.DataSource = dtDetail;
                        this.uGridMatInspect.DataBind();

                        if (dtDetail.Rows.Count > 0)
                        {
                            WinGrid grd = new WinGrid();
                            grd.mfSetAutoResizeColWidth(this.uGridMatInspect, 0);
                        }
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001099", "M001115", "M000376",
                                                            Infragistics.Win.HAlign.Right);
                        //this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조건에 따라 행 설정 Method
        /// </summary>
        /// <param name="uGrid">그리드명</param>
        /// <param name="strCheckColKey">값을 검사할 컬럼키</param>
        /// <param name="strValue"> 검사할 값 </param>
        /// <param name="strSetColorColKey"> 색을 바꿀 컬럼 </param>
        /// <param name="color"> 배경색 </param>
        private void SetFaultRow(Infragistics.Win.UltraWinGrid.UltraGrid uGrid, String strCheckColKey, String strValue, String strSetColorColKey, Color color)
        {
            try
            {
                // Loop 돌며 값 검사
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Cells[strCheckColKey].Value.ToString() == strValue)
                    {
                        // 불합격이면 배경색 변경
                        uGrid.Rows[i].Cells[strSetColorColKey].Appearance.BackColor = color;
                    }
                    //else
                    //{
                    //    // 불합격이 아닌경우 편집불가
                    //    uGrid.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //    uGrid.Rows[i].Appearance.BackColor = Color.Gainsboro;
                    //}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 발생공정 콤보박스 초기화 Method
        /// </summary>
        /// <param name="strPlantCode"></param>
        private void SetAriseProcessCombo(string strPlantCode)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
                // 콤보아아템 삭제
                this.uComboAriseProcess.Items.Clear();

                // 발생공정 콤보박스 설정
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                // 콤보박스 설정Method 호출
                wCombo.mfSetComboEditor(this.uComboAriseProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 상세조회 Method
        /// </summary>
        /// <param name="strStdNumber"> 관리번호 </param>
        private void SearchHeaderDetail(string strStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsHeader);

                // Method 호출
                DataTable dtReqH = clsHeader.mfReadINSMaterialAbnormalHDetail(PlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                this.uTextPlant.Text = dtReqH.Rows[0]["PlantName"].ToString();
                this.uTextStdNumber.Text = dtReqH.Rows[0]["StdNumber"].ToString();
                this.uTextVendorCode.Text = dtReqH.Rows[0]["VendorCode"].ToString();
                this.uTextVendorName.Text = dtReqH.Rows[0]["VendorName"].ToString();
                this.uTextMaterialCode.Text = dtReqH.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dtReqH.Rows[0]["MaterialName"].ToString();
                this.uTextMaterialSpec1.Text = dtReqH.Rows[0]["MaterialSpec1"].ToString();
                this.uTextMoldSeq.Text = dtReqH.Rows[0]["MoldSeq"].ToString();
                this.uComboAbnormalType.Value = dtReqH.Rows[0]["AbnormalType"].ToString();
                this.uTextWriteUserID.Text = dtReqH.Rows[0]["WriteUserID"].ToString();
                this.uTextWriteName.Text = dtReqH.Rows[0]["WriteName"].ToString();
                this.uDateWriteDate.Value = dtReqH.Rows[0]["WriteDate"].ToString();
                this.uTextGRDate.Text = dtReqH.Rows[0]["GRDate"].ToString();
                this.uTextTotalQty.Text = Math.Floor(Convert.ToDecimal(dtReqH.Rows[0]["TotalQty"].ToString())).ToString();
                this.uComboStatus.Value = dtReqH.Rows[0]["StatusFlag"].ToString();
                this.uCheckCompleteFlag.Checked = Convert.ToBoolean(dtReqH.Rows[0]["CompleteFlag"].ToString());
                if (strStdNumber.Substring(0, 4) == "AE03")
                {
                    this.uTextLotNo.Text = dtReqH.Rows[0]["LotNo"].ToString();
                    this.uComboAriseProcess.Value = dtReqH.Rows[0]["AriseProcessCode"].ToString();
                    this.uDateAriseDate.Value = dtReqH.Rows[0]["AriseDate"].ToString();
                }

                this.uCheckCompleteFlag.Enabled = !this.uCheckCompleteFlag.Checked;
                this.uCheckFinalCompleteFlag.Checked = Convert.ToBoolean(dtReqH.Rows[0]["FinalCompleteFlag"].ToString());
                this.uCheckFinalCompleteFlag.Enabled = !this.uCheckFinalCompleteFlag.Checked;

                this.uTextWMSTFlag.Text = dtReqH.Rows[0]["WMSTFlag"].ToString();

                this.uTextMaterialTreate.Text = dtReqH.Rows[0]["MaterialTreate"].ToString();
                this.uTextShortActionDesc.Text = dtReqH.Rows[0]["ShortActionDesc"].ToString();
                this.uTextLongActionDesc.Text = dtReqH.Rows[0]["LongActionDesc"].ToString();
                this.uDateLongAcceptDate.Value = dtReqH.Rows[0]["LongAcceptDate"].ToString();
                this.uDateShortAcceptDate.Value = dtReqH.Rows[0]["ShortAcceptDate"].ToString();
                this.uTextShortFile.Text = dtReqH.Rows[0]["ShortFile"].ToString();
                this.uTextLongFile.Text = dtReqH.Rows[0]["LongFile"].ToString();
                this.uTextShortFile.Tag = dtReqH.Rows[0]["ShortFileData"].ToString() == string.Empty ? "0x" : dtReqH.Rows[0]["ShortFileData"];
                this.uTextLongFile.Tag = dtReqH.Rows[0]["LongFileData"].ToString() == string.Empty ? "0x" : dtReqH.Rows[0]["LongFileData"];


                this.uTextShortUserName.Text = dtReqH.Rows[0]["ShortUserName"].ToString();
                this.uTextShortEmail.Text = dtReqH.Rows[0]["ShortEmail"].ToString();
                this.uOptShortConfirm.Value = dtReqH.Rows[0]["ShortConfirmFlag"];
                this.uTextShortConfirmDesc.Text = dtReqH.Rows[0]["ShortConfirmDesc"].ToString();
                this.uTextShortConfirmUserID.Text = dtReqH.Rows[0]["ShortConfirmUserID"].ToString();
                this.uTextShortConfirmUserName.Text = dtReqH.Rows[0]["ShortConfirmUserName"].ToString();
                this.uDateShortConfirmDate.Value = dtReqH.Rows[0]["ShortConfirmDate"];
                this.uTextLongUserName.Text = dtReqH.Rows[0]["LongUserName"].ToString();
                this.uTextLongEmail.Text = dtReqH.Rows[0]["LongEmail"].ToString();
                this.uOptLongConfirm.Value = dtReqH.Rows[0]["LongConfirmFlag"];
                this.uTextLongConfirmDesc.Text = dtReqH.Rows[0]["LongConfirmDesc"].ToString();
                this.uTextLongConfirmUserID.Text = dtReqH.Rows[0]["LongConfirmUserID"].ToString();
                this.uTextLongConfirmUserName.Text = dtReqH.Rows[0]["LongConfirmUserName"].ToString();
                this.uDateLongConfirmDate.Value = dtReqH.Rows[0]["LongConfirmDate"].ToString();


                //장기조치사항이 승인되었을 경우 수정불가
                if (this.uOptLongConfirm.Value != null && this.uOptLongConfirm.Value.ToString().Equals("T"))
                {
                    this.uOptShortConfirm.Enabled = true;
                    this.uTextShortConfirmDesc.Enabled = true;
                    this.uTextShortConfirmUserID.Enabled = true;
                    this.uOptLongConfirm.Enabled = false;
                    this.uTextLongConfirmDesc.Enabled = false;
                    this.uTextLongConfirmUserID.Enabled = false;

                }
                //단기조치사항이 승인되었을 경우 수정불가
                else if (this.uOptShortConfirm.Value != null && this.uOptShortConfirm.Value.ToString().Equals("T"))
                {
                    this.uOptShortConfirm.Enabled = false;
                    this.uTextShortConfirmDesc.Enabled = false;
                    this.uTextShortConfirmUserID.Enabled = false;
                    this.uOptLongConfirm.Enabled = true;
                    this.uTextLongConfirmDesc.Enabled = true;
                    this.uTextLongConfirmUserID.Enabled = true;
                }
                else
                {
                    
                    this.uOptShortConfirm.Enabled = true;
                    this.uTextShortConfirmDesc.Enabled = true;
                    this.uTextShortConfirmUserID.Enabled = true;
                    this.uOptLongConfirm.Enabled = true;
                    this.uTextLongConfirmDesc.Enabled = true;
                    this.uTextLongConfirmUserID.Enabled = true;
                    
                }
                
                

                this.uTextCompleteFlag.Text = dtReqH.Rows[0]["CompleteFlag"].ToString().ToUpper().Substring(0, 1);
                this.uTextMESTFlag.Text = dtReqH.Rows[0]["MESTFlag"].ToString(); //MES전송여부

                this.uTextLotNo.Enabled = false;
                this.uComboAriseProcess.Enabled = false;
                this.uDateAriseDate.Enabled = false;

                if (this.uCheckCompleteFlag.Enabled)
                {
                    this.uGridMatInspect.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    this.uGridMatInspectFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    this.uGridProcInspect.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    this.uDateWriteDate.Enabled = true;
                    this.uTextWriteUserID.Enabled = true;
                }
                else
                {
                    this.uGridMatInspect.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    this.uGridMatInspectFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    this.uGridProcInspect.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    this.uDateWriteDate.Enabled = false;
                    this.uTextWriteUserID.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /////// <summary>
        /////// 상세정보 조회 Method
        /////// </summary>
        /////// <param name="strStdNumber"></param>
        ////private void SearchDetail(string strStdNumber, Infragistics.Win.UltraWinGrid.UltraGrid uGRid)
        ////{
        ////    try
        ////    {
        ////        // SystemInfo ResourceSet
        ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
        ////        QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
        ////        brwChannel.mfCredentials(clsDetail);

        ////        DataTable dtRtn = new DataTable();

        ////        if (strStdNumber.Substring(0, 4) == "AE02")
        ////        {
        ////            // Method 호출
        ////            dtRtn = clsDetail.mfReadINSMaterialAbnormalD_Mat(PlantCode, strStdNumber, "", "", m_resSys.GetString("SYS_LANG"));

        ////            uGRid.DataSource = dtRtn;
        ////            uGRid.DataBind();
        ////        }
        ////        else
        ////        {
        ////            // Method 호출
        ////            dtRtn = clsDetail.mfReadINSMaterialAbnormalD_Proc(PlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

        ////            uGRid.DataSource = dtRtn;
        ////            uGRid.DataBind();

        ////            this.uTextLotNo.Enabled = false;
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        /// <summary>
        /// 상세정보(수입이상) 조회 Method
        /// </summary>
        /// <param name="strStdNumber"></param>
        private void SearchDetail_Mat(string strStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtRtn = new DataTable();

                // Method 호출
                dtRtn = clsDetail.mfReadINSMaterialAbnormalD_Mat(PlantCode, strStdNumber, "", "", m_resSys.GetString("SYS_LANG"));

                this.uGridMatInspect.DataSource = dtRtn;
                this.uGridMatInspect.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    ReqNo = dtRtn.Rows[0]["ReqNo"].ToString();
                    ReqSeq = dtRtn.Rows[0]["ReqSeq"].ToString();
                }

                this.ultraTabControl1.Tabs[1].Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보(공정이상) 조회 Method
        /// </summary>
        /// <param name="strStdNumber"></param>
        private void SearchDetail_Proc(string strStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtRtn = new DataTable();

                // Method 호출
                dtRtn = clsDetail.mfReadINSMaterialAbnormalD_Proc(PlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                this.uGridProcInspect.DataSource = dtRtn;
                this.uGridProcInspect.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    ReqNo = dtRtn.Rows[0]["ReqNo"].ToString();
                    ReqSeq = dtRtn.Rows[0]["ReqSeq"].ToString();

                    for (int i = 0; i < this.uGridProcInspect.Rows.Count; i++)
                    {
                        if (this.uGridProcInspect.Rows[i].Cells["LotNo"].Value.ToString().Equals(this.uTextLotNo.Text))
                        {
                            this.uGridProcInspect.Rows[i].Appearance.BackColor = Color.Red;
                        }
                    }
                }

                this.ultraTabControl1.Tabs[0].Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형 정보 조회 Method
        /// </summary>
        ///<param name="intRowIndex">수입이상 그리드 행 Index</param>
        private void SearchDFault_MatInspect(int intRowIndex)
        {
            try
            {
                // 검사결과가 불합격인 경우만 불량정보 입력 가능
                if (this.uGridMatInspect.Rows[intRowIndex].Cells["InspectResultFlag"].Value.ToString() == "NG")
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinGrid wGrid = new WinGrid();

                    QRPBrowser brwChannel = new QRPBrowser();
                    // 불량정보 조회 Method 호출
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalDFault), "MaterialAbnormalDFault");
                    QRPINS.BL.INSIMP.MaterialAbnormalDFault clsFault = new QRPINS.BL.INSIMP.MaterialAbnormalDFault();
                    brwChannel.mfCredentials(clsFault);

                    // 입력 가능
                    this.uGridMatInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;

                    if (this.uTextStdNumber.Text.Equals(string.Empty))
                    {
                        // 관리번호가 없는경우 저장된 데이터가 없기 때문에 공백줄만 늘려준다
                        wGrid.mfAddRowGrid(this.uGridMatInspectFault, 0);
                    }
                    else    // 관리번호가 있는경우 조회
                    {
                        // 조회용 DataTable 설정
                        DataTable dtSearch = clsFault.mfSetDataInfo();
                        DataRow drRow = dtSearch.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["ReqNo"] = this.uGridMatInspect.Rows[intRowIndex].Cells["ReqNo"].Value.ToString();
                        drRow["ReqSeq"] = this.uGridMatInspect.Rows[intRowIndex].Cells["ReqSeq"].Value.ToString();
                        //drRow["ReqLotSeq"] = this.uGridMatInspect.Rows[intRowIndex].Cells["ReqLotSeq"].Value;
                        drRow["ReqLotSeq"] = 0;
                        drRow["ReqItemSeq"] = this.uGridMatInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value;
                        dtSearch.Rows.Add(drRow);

                        // 조회 Method 호출
                        DataTable dtRtn = clsFault.mfReadINSMaterialAbnormalDFault(dtSearch);

                        this.uGridMatInspectFault.DataSource = dtRtn;
                        this.uGridMatInspectFault.DataBind();
                        //this.uGridMatInspectFault.SetDataBinding(dtFault, string.Empty);
                        WinGrid grd = new WinGrid();
                        if (dtRtn.Rows.Count > 0)
                        {
                            grd.mfSetAutoResizeColWidth(this.uGridMatInspectFault, 0);
                        }
                        //else
                        //{
                        //    this.uGridMatInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                        //    grd.mfAddRowGrid(this.uGridMatInspectFault, 0);
                        //}
                    }

                    ////// 불량유형 DropDown 설정
                    ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    ////QRPMAS.BL.MASQUA.InspectFaultType clsFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                    ////brwChannel.mfCredentials(clsFaultType);

                    ////DataTable dtFaultType = clsFaultType.mfReadInspectFaultTypeCombo(m_strPlantCode, m_resSys.GetString("SYS_LANG"));

                    ////wGrid.mfSetGridColumnValueList(this.uGridMatInspectFault, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);

                    // LotNo DropDown 설정
                    DataTable dtLotNo = clsFault.mfReadINSMaterialAbnormalDFault_Mat_LotNo_Combo(PlantCode
                                                                                                , this.uGridMatInspect.Rows[intRowIndex].Cells["ReqNo"].Value.ToString()
                                                                                                , this.uGridMatInspect.Rows[intRowIndex].Cells["ReqSeq"].Value.ToString()
                                                                                                , 0
                                                                                                , Convert.ToInt32(this.uGridMatInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value));

                    wGrid.mfSetGridColumnValueList(this.uGridMatInspectFault, 0, "ReqLotSeq", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", "선택", dtLotNo);
                }
                else
                {
                    // 입력불가 처리
                    this.uGridMatInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                    // 그리드 행 삭제
                    while (this.uGridMatInspectFault.Rows.Count > 0)
                    {
                        this.uGridMatInspectFault.Rows[0].Delete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공정이상 불량정보 그리드 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        private void SearchDFault_Process(int intRowIndex)
        {
            try
            {
                //if (this.uGridProcInspect.Rows[intRowIndex].Cells["LotNo"].Value.ToString().Equals(this.uTextLotNo.Text))
                //{
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    WinGrid wGrid = new WinGrid();

                    // 그리드 입력 가능하도록
                    this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;

                    if (this.uTextStdNumber.Text.Equals(string.Empty))
                    {
                        // 관리번호가 없는경우 저장된 데이터가 없기 때문에 공백줄만 늘려준다
                        wGrid.mfAddRowGrid(this.uGridProcInspectFault, 0);
                    }
                    else
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalDFault), "MaterialAbnormalDFault");
                        QRPINS.BL.INSIMP.MaterialAbnormalDFault clsDFault = new QRPINS.BL.INSIMP.MaterialAbnormalDFault();
                        brwChannel.mfCredentials(clsDFault);

                        // 조회용 DataTable 설정
                        DataTable dtSearch = clsDFault.mfSetDataInfo();
                        DataRow drRow = dtSearch.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["ReqNo"] = this.uGridProcInspect.Rows[intRowIndex].Cells["ReqNo"].Value;
                        drRow["ReqSeq"] = this.uGridProcInspect.Rows[intRowIndex].Cells["ReqSeq"].Value;
                        drRow["ReqLotSeq"] = this.uGridProcInspect.Rows[intRowIndex].Cells["ReqLotSeq"].Value;
                        drRow["ReqItemSeq"] = this.uGridProcInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value;
                        dtSearch.Rows.Add(drRow);

                        DataTable dtFault = clsDFault.mfReadINSMaterialAbnormalDFault(dtSearch);

                        this.uGridProcInspectFault.SetDataBinding(dtFault, string.Empty);

                        //this.uGridProcInspectFault.DataSource = dtFault;
                        //this.uGridProcInspectFault.DataBind();

                        WinGrid grd = new WinGrid();
                        if (dtFault.Rows.Count > 0)
                        {                            
                            grd.mfSetAutoResizeColWidth(this.uGridProcInspectFault, 0);
                        }
                        //else
                        //{
                        //    this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                        //    grd.mfAddRowGrid(this.uGridProcInspectFault, 0);
                        //}
                    }

                    //// 불량유형 DropDown 설정
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                    //QRPMAS.BL.MASQUA.InspectFaultType clsFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                    //brwChannel.mfCredentials(clsFaultType);

                    //DataTable dtFaultType = clsFaultType.mfReadInspectFaultTypeCombo(PlantCode, m_resSys.GetString("SYS_LANG"));

                    //wGrid.mfSetGridColumnValueList(this.uGridProcInspectFault, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                //}
                //else
                //{
                //    // 입력불가 처리
                //    this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                //    // 그리드 행 삭제
                //    while (this.uGridProcInspectFault.Rows.Count > 0)
                //    {
                //        this.uGridProcInspectFault.Rows[0].Delete(false);
                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량정보 데이터 테이블에 저장한후 반환
        /// </summary>
        /// <param name="bolCheck"> 수입(true)/공정(false) 인지 판단하기 위한 Bool변수 </param>
        /// <returns></returns>
        private DataTable RtndtFault(bool bolCheck)
        {
            DataTable dtFault = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalDFault), "MaterialAbnormalDFault");
                QRPINS.BL.INSIMP.MaterialAbnormalDFault clsFault = new QRPINS.BL.INSIMP.MaterialAbnormalDFault();
                brwChannel.mfCredentials(clsFault);

                // DataTable Columns 설정
                dtFault = clsFault.mfSetDataInfo();

                // 수입이상발생 일때
                if (bolCheck)
                {
                    if (this.uGridMatInspectFault.ActiveCell != null)
                    {
                        // ActiveCell 변경
                        this.uGridMatInspectFault.ActiveCell = this.uGridMatInspectFault.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridMatInspectFault.Rows.Count; i++)
                        {
                            // 행삭제 버튼에 의해서 삭제된 버튼은 저장에서 제외
                            if (this.uGridMatInspectFault.Rows[i].Hidden == false)
                            {
                                DataRow drRow = dtFault.NewRow();
                                drRow["PlantCode"] = PlantCode;
                                drRow["StdNumber"] = this.uTextStdNumber.Text;
                                drRow["ReqNo"] = ReqNo;
                                drRow["ReqSeq"] = ReqSeq;
                                drRow["ReqLotSeq"] = ReqLotSeq;
                                drRow["ReqItemSeq"] = ReqItemSeq;                                
                                drRow["FaultSeq"] = this.uGridMatInspectFault.Rows[i].RowSelectorNumber;
                                drRow["InspectQty"] = 0;
                                drRow["FaultQty"] = 0;
                                if (this.uGridMatInspectFault.Rows[i].Cells["FaultTypeCode"].Value == null)
                                    drRow["FaultTypeCode"] = string.Empty;
                                else
                                    drRow["FaultTypeCode"] = this.uGridMatInspectFault.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                                if (this.uGridMatInspectFault.Rows[i].Cells["EtcDesc"].Value == null)
                                    drRow["EtcDesc"] = string.Empty;
                                else
                                    drRow["EtcDesc"] = this.uGridMatInspectFault.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtFault.Rows.Add(drRow);
                            }
                        }
                    }
                }
                else    // 공정이상발생 일때
                {
                    // ActiveCell 변경
                    if (this.uGridProcInspectFault.ActiveCell != null)
                    {
                        this.uGridProcInspectFault.ActiveCell = this.uGridProcInspectFault.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridProcInspectFault.Rows.Count; i++)
                        {
                            // 행삭제 버튼에 의해서 삭제된 버튼은 저장에서 제외
                            if (this.uGridProcInspectFault.Rows[i].Hidden == false)
                            {
                                DataRow drRow = dtFault.NewRow();
                                drRow["PlantCode"] = PlantCode;
                                drRow["StdNumber"] = this.uTextStdNumber.Text;
                                drRow["ReqNo"] = ReqNo;
                                drRow["ReqSeq"] = ReqSeq;
                                drRow["ReqLotSeq"] = ReqLotSeq;
                                drRow["ReqItemSeq"] = ReqItemSeq;
                                drRow["FaultSeq"] = this.uGridProcInspectFault.Rows[i].RowSelectorNumber;
                                drRow["InspectQty"] = Convert.ToDouble(this.uGridProcInspectFault.Rows[i].Cells["InspectQty"].Value);
                                drRow["FaultQty"] = Convert.ToDouble(this.uGridProcInspectFault.Rows[i].Cells["FaultQty"].Value);
                                if (this.uGridProcInspectFault.Rows[i].Cells["FaultTypeCode"].Value == null)
                                    drRow["FaultTypeCode"] = string.Empty;
                                else
                                    drRow["FaultTypeCode"] = this.uGridProcInspectFault.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                                if (this.uGridProcInspectFault.Rows[i].Cells["EtcDesc"].Value == null)
                                    drRow["EtcDesc"] = string.Empty;
                                else
                                    drRow["EtcDesc"] = this.uGridProcInspectFault.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtFault.Rows.Add(drRow);
                            }
                        }
                    }
                }
                return dtFault;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtFault;
            }
            finally
            {
            }
        }

        ///// <summary>
        ///// 상세정보 데이터 테이블에 저장한후 반환
        ///// </summary>
        ///// <param name="uGrid"> 그리드명(수입상세그리드 or 공정상세그리드) </param>
        ///// <returns></returns>
        //private DataTable RtndtDetail(Infragistics.Win.UltraWinGrid.UltraGrid uGrid)
        //{
        //    DataTable dtRtn = new DataTable();
        //    try
        //    {
        //        bool bolCheckReqItemSeq = false;
        //        int intDummyReqItemSeq = 0;

        //        // BL 연결
        //        QRPBrowser brwChannel = new QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
        //        QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
        //        brwChannel.mfCredentials(clsDetail);

        //        // DataTable Column 설정
        //        dtRtn = clsDetail.mfSetDataInfo();

        //        // ActiveCell 이동
        //        uGrid.ActiveCell = uGrid.Rows[0].Cells[0];

        //        int intRowIndex = 0;
        //        // 검사항목순번으로 RowIndex 찾는다
        //        for (int i = 0; i < uGrid.Rows.Count; i++)
        //        {
        //            if (Convert.ToInt32(uGrid.Rows[i].Cells["ReqLotSeq"].Value) == ReqLotSeq)
        //            {
        //                if (uGrid.Rows[i].Cells["ReqItemSeq"].Value != DBNull.Value && Convert.ToInt32(uGrid.Rows[i].Cells["ReqItemSeq"].Value) == ReqItemSeq)
        //                {
        //                    intRowIndex = uGrid.Rows[i].Index;
        //                    //수입검사등록시 Item이 없는 경우가 발생하여 Item이 있는지 체크함
        //                    bolCheckReqItemSeq = true;
        //                    break;
        //                }
        //            }
        //        }
                
        //        //수입검사성적 Item이 없는 경우는 저장을 Skip한다.
        //        if (bolCheckReqItemSeq == false)
        //            intDummyReqItemSeq = 0;
        //        else
        //            intDummyReqItemSeq = ReqItemSeq;
                        
        //            //return dtRtn;


        //        // Data 저장
        //        DataRow drRow = dtRtn.NewRow();
        //        drRow["PlantCode"] = PlantCode;
        //        drRow["StdNumber"] = this.uTextStdNumber.Text;
        //        //drRow["ReqNo"] = uGrid.Rows[intRowIndex].Cells["ReqNo"].Value.ToString();
        //        //drRow["ReqSeq"] = uGrid.Rows[intRowIndex].Cells["ReqSeq"].Value.ToString();
        //        //drRow["ReqLotSeq"] = uGrid.Rows[intRowIndex].Cells["ReqLotSeq"].Value.ToString();
        //        drRow["ReqNo"] = ReqNo;
        //        drRow["ReqSeq"] = ReqSeq;
        //        drRow["ReqLotSeq"] = ReqLotSeq;
                
        //        //수입검사성적서에서 Item이 없는 Lot에 대해서 공정불량이 발생한 경우 이상발생정보를 저장하기 위해
        //        //ReqItemSeqf를 0로 지정하여 저장한다.
        //        //drRow["ReqItemSeq"] = uGrid.Rows[intRowIndex].Cells["ReqItemSeq"].Value.ToString();
        //        drRow["ReqItemSeq"] = intDummyReqItemSeq;

        //        // 첨부파일 컬럼이 있는지 없는지 판단
        //        if (uGrid.DisplayLayout.Bands[0].Columns.Exists("ReqFileName"))
        //        {
        //            if (uGrid.Rows[intRowIndex].Cells["ReqFileName"].Value != null && uGrid.Rows[intRowIndex].Cells["ReqFileName"].Value.ToString().Contains(":\\"))
        //            {
        //                FileInfo fileDoc = new FileInfo(uGrid.Rows[intRowIndex].Cells["ReqFileName"].Value.ToString());
        //                drRow["ReqFileName"] = fileDoc.Name;
        //            }
        //            else if (uGrid.Rows[intRowIndex].Cells["ReqFileName"].Value != null)
        //            {
        //                drRow["ReqFileName"] = uGrid.Rows[intRowIndex].Cells["ReqFileName"].Value.ToString();
        //            }
        //        }
        //        dtRtn.Rows.Add(drRow);
        //        return dtRtn;
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //        return dtRtn;
        //    }
        //    finally
        //    {
        //    }
        //}

        #region 수입이상발생 메소드
        /// <summary>
        /// 수입이상발생일경우 헤더정보 반환
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtHeader_Mat()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsHeader);

                // DataTable Columns 설정 Method 호출
                dtRtn = clsHeader.mfSetDataInfo();
                // DataTable 값 저장
                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = PlantCode;
                drRow["StdNumber"] = this.uTextStdNumber.Text;
                drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                drRow["WriteDate"] = Convert.ToDateTime(this.uDateWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["AbnormalType"] = this.uComboAbnormalType.Value.ToString();

                int intStatus;

                if (this.uComboStatus.Value == null || this.uComboStatus.Value.ToString().Equals(string.Empty))
                    intStatus = 1;
                else
                    intStatus = Convert.ToInt32(this.uComboStatus.Value);

                //원자재이상발생상태 우선순위대로 처리하여 저장
                if (this.uCheckFinalCompleteFlag.Checked)                                                         // 최종완료
                    drRow["StatusFlag"] = "0";
                else if (this.uOptLongConfirm.Value != null && this.uOptLongConfirm.Value.ToString().Equals("T")) // 장기승인
                    drRow["StatusFlag"] = "9";
                else if (this.uOptLongConfirm.Value != null && this.uOptLongConfirm.Value.ToString().Equals("F")) //장기반려
                    drRow["StatusFlag"] = "8";
                else if (this.uOptShortConfirm.Value != null && this.uOptShortConfirm.Value.ToString().Equals("T") && intStatus <= 8) // 단기승인(장기접수,장기재접수일경우포함)
                    drRow["StatusFlag"] = "5";
                else if (this.uOptShortConfirm.Value != null && this.uOptShortConfirm.Value.ToString().Equals("F") && intStatus <= 7) //단기반려(장기접수,장기재접수일경우포함)
                    drRow["StatusFlag"] = "4";
                //else if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)                   //발행
                //    drRow["StatusFlag"] = "1";
                //else if (!this.uCheckCompleteFlag.Checked)
                //    drRow["StatusFlag"] = "";
                else
                    drRow["StatusFlag"] = intStatus.ToString();
                    //drRow["StatusFlag"] = this.uComboStatus.Value.ToString();

                    

                drRow["VendorCode"] = this.uTextVendorCode.Text;
                drRow["VendorName"] = this.uTextVendorName.Text;
                drRow["MaterialCode"] = this.uTextMaterialCode.Text;
                drRow["MaterialName"] = this.uTextMaterialName.Text;

                drRow["CompleteFlag"] = this.uCheckCompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["FinalCompleteFlag"] = this.uCheckFinalCompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ShortActionDesc"] = this.uTextShortActionDesc.Text;
                drRow["LongActionDesc"] = this.uTextLongActionDesc.Text;
                drRow["MaterialTreate"] = this.uTextMaterialTreate.Text;
                if (this.uTextShortFile.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextShortFile.Text);
                   // drRow["ShortFile"] = fileDoc.Name;
                    drRow["ShortFile"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Short-" + fileDoc.Name;
                }
                else
                    drRow["ShortFile"] = this.uTextShortFile.Text;
                
                if (this.uTextLongFile.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextLongFile.Text);
                    //drRow["LongFile"] = fileDoc.Name;
                    drRow["LongFile"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Long-" + fileDoc.Name;
                }
                else
                    drRow["LongFile"] = this.uTextLongFile.Text;

                //단기,장기 첨부파일
                byte[] empty = { };
                if (this.uTextShortFile.Tag != null && this.uTextShortFile.Tag != DBNull.Value)
                    drRow["ShortFileData"] = (byte[])this.uTextShortFile.Tag;
                //else if (this.uTextShortFile.Text.Equals(string.Empty))
                else if (this.uTextShortFile.Text.Equals(string.Empty) || this.uTextShortFile.Tag == null || this.uTextShortFile.Tag == DBNull.Value)
                    drRow["ShortFileData"] = empty;
                if (this.uTextLongFile.Tag != null && this.uTextLongFile.Tag != DBNull.Value)
                    drRow["LongFileData"] = (byte[])this.uTextLongFile.Tag;
                //else if (this.uTextLongFile.Text.Equals(string.Empty))
                else if (this.uTextLongFile.Text.Equals(string.Empty) || this.uTextLongFile.Tag == null || this.uTextLongFile.Tag == DBNull.Value)
                    drRow["LongFileData"] = empty;

                drRow["InspectDesc"] = this.uTextInspectDesc.Text;
                //-----------------------------------------------------------------------------------------------------------------------------------------------
                //단기조치사항
                drRow["ShortUserName"] = this.uTextShortUserName.Text;
                drRow["ShortEmail"] = this.uTextShortEmail.Text;
                drRow["ShortConfirmFlag"] = this.uOptShortConfirm.Value;
                drRow["ShortConfirmDesc"] = this.uTextShortConfirmDesc.Text;
                drRow["ShortConfirmUserID"] = this.uTextShortConfirmUserID.Text;

                //승인이 되었다면 현재 날짜 입력 , 이미승인,null,반려 일경우 공백 or 기존 값
                if (this.uOptShortConfirm.Value != null && this.uOptShortConfirm.Enabled)
                    drRow["ShortConfirmDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                else
                    drRow["ShortConfirmDate"] = this.uDateShortConfirmDate.Value == null ? "" : this.uDateShortConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                //------------------------------------------------------------------------------------------------------------------------------------------------
                //장기조치사항
                drRow["LongUserName"] = this.uTextLongUserName.Text;
                drRow["LongEmail"] = this.uTextLongEmail.Text;
                drRow["LongConfirmFlag"] = this.uOptLongConfirm.Value;
                drRow["LongConfirmDesc"] = this.uTextLongConfirmDesc.Text;
                drRow["LongConfirmUserID"] = this.uTextLongConfirmUserID.Text;

                //승인이 되었다면 현재 날짜 입력 , 이미승인,null,반려 일경우 공백 or 기존 값
                if (this.uOptLongConfirm.Value != null && this.uOptLongConfirm.Enabled)
                    drRow["LongConfirmDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                else
                    drRow["LongConfirmDate"] = this.uDateLongConfirmDate.Value == null ? "" : this.uDateLongConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                
                dtRtn.Rows.Add(drRow);
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입이상 상세정보 데이터 테이블로 반환하는 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtDetail_Mat()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                brwChannel.mfCredentials(clsDetail);

                if (this.uGridMatInspect.Rows.Count > 0)
                {
                    // DataTable Columns 설정 메소드 호출
                    dtRtn = clsDetail.mfSetDataInfo();
                    DataRow drRow;

                    this.uGridMatInspect.ActiveCell = this.uGridMatInspect.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridMatInspect.Rows.Count; i++)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["ReqNo"] = this.uGridMatInspect.Rows[i].Cells["ReqNo"].Value.ToString();
                        drRow["ReqSeq"] = this.uGridMatInspect.Rows[i].Cells["ReqSeq"].Value.ToString();
                        drRow["ReqLotSeq"] = 0;                 // 현재 사용하지 않음;;;;
                        drRow["ReqItemSeq"] = this.uGridMatInspect.Rows[i].Cells["ReqItemSeq"].Value;
                        if (this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value != null && this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value.ToString());
                            drRow["ReqFileName"] = fileDoc.Name;
                        }
                        else if (this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value != null)
                        {
                            drRow["ReqFileName"] = this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value.ToString();
                        }
                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입이상발생 불량정보그리드 저장정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtDetailFault_Mat()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalDFault), "MaterialAbnormalDFault");
                QRPINS.BL.INSIMP.MaterialAbnormalDFault clsDFault = new QRPINS.BL.INSIMP.MaterialAbnormalDFault();
                brwChannel.mfCredentials(clsDFault);

                dtRtn = clsDFault.mfSetDataInfo();
                DataRow drRow;

                if (this.uGridMatInspectFault.Rows.Count > 0)
                {
                    this.uGridMatInspectFault.ActiveCell = this.uGridMatInspectFault.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridMatInspectFault.Rows.Count; i++)
                    {
                        // 행삭제 버튼에 의해서 삭제된 버튼은 저장에서 제외
                        if (this.uGridMatInspectFault.Rows[i].Hidden
                            && this.uGridMatInspectFault.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                            continue;
                        
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["ReqNo"] = this.uGridMatInspectFault.Rows[i].Cells["ReqNo"].Value.ToString();
                        drRow["ReqSeq"] = this.uGridMatInspectFault.Rows[i].Cells["ReqSeq"].Value.ToString();
                        drRow["ReqLotSeq"] = this.uGridMatInspectFault.Rows[i].Cells["ReqLotSeq"].Value.ToString();
                        drRow["ReqItemSeq"] = this.uGridMatInspectFault.Rows[i].Cells["ReqItemSeq"].Value;
                        drRow["FaultSeq"] = this.uGridMatInspectFault.Rows[i].Cells["FaultSeq"].Value; //.Cells["FaultSeq"].Value.ToString() == "0" ? i + 1 : this.uGridMatInspectFault.Rows[i].Cells["FaultSeq"].Value;
                        drRow["InspectQty"] = this.uGridMatInspectFault.Rows[i].Cells["InspectQty"].Value;
                        drRow["FaultQty"] = this.uGridMatInspectFault.Rows[i].Cells["FaultQty"].Value;

                        if (this.uGridMatInspectFault.Rows[i].Cells["FaultTypeCode"].Value == null)
                        {
                            drRow["FaultTypeCode"] = string.Empty;//불량유형코드
                            drRow["FaultTypeName"] = string.Empty;//불량유형명
                        }
                        else
                        {
                            drRow["FaultTypeCode"] = this.uGridMatInspectFault.Rows[i].Cells["FaultTypeCode"].Value.ToString(); //불량유형코드
                            drRow["FaultTypeName"] = this.uGridMatInspectFault.Rows[i].Cells["FaultTypeCode"].Text; //불량유형명
                        }

                        drRow["LotNo"] = this.uGridMatInspectFault.Rows[i].Cells["ReqLotSeq"].Text;    //LotNo

                        if (this.uGridMatInspectFault.Rows[i].Cells["EtcDesc"].Value == null)

                            drRow["EtcDesc"] = string.Empty;
                        else
                            drRow["EtcDesc"] = this.uGridMatInspectFault.Rows[i].Cells["EtcDesc"].Value.ToString();

                        if (string.IsNullOrEmpty(this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString())
                            || this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value == null)
                            drRow["FilePath"] = string.Empty;
                        else if (this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString());
                            drRow["FilePath"] = fileDoc.Name;
                        }
                        else
                            drRow["FilePath"] = this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString();

                        byte[] empty = { };
                        if (this.uGridMatInspectFault.Rows[i].Cells["FileData"].Value != null
                            && !this.uGridMatInspectFault.Rows[i].Cells["FileData"].Value.ToString().Equals(string.Empty))
                            drRow["FileVarValue"] = (byte[])this.uGridMatInspectFault.Rows[i].Cells["FileData"].Value;
                        else
                            drRow["FileVarValue"] = empty;

                        drRow["DeleteFlag"] = this.uGridMatInspectFault.Rows[i].Cells["DeleteFlag"].Value;

                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입이상 WMS 데이터 전송 메소드
        /// </summary>
        /// <returns></returns>
        private void Send_WMS_Mat(string strPlantCode, string strStdNumber, string strReqNo, string strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 작성완료상태에 WMS 전송Flag가 T가 아니면 WMS 데이터 전송
                if (this.uCheckCompleteFlag.Checked && !this.uTextWMSTFlag.Text.Equals("T"))
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalDFault), "MaterialAbnormalDFault");
                    QRPINS.BL.INSIMP.MaterialAbnormalDFault clsDFault = new QRPINS.BL.INSIMP.MaterialAbnormalDFault();
                    brwChannel.mfCredentials(clsDFault);

                    DataTable dtWMS = clsDFault.mfReadINSMaterialAbnormalDFault_Mat_WMSData(strPlantCode, strStdNumber, strReqNo, strReqSeq);

                    DataTable dtInspectReqLot = dtWMS.DefaultView.ToTable(false, "PlantCode", "ReqNo", "ReqSeq", "ReqLotSeq", "InspectResultFlag");
                    dtInspectReqLot.Columns["InspectResultFlag"].ColumnName = "AbnormalResultFlag";

                    for (int i = 0; i < dtInspectReqLot.Rows.Count; i++)
                    {
                        if (dtInspectReqLot.Rows[i]["AbnormalResultFlag"].ToString().Equals("F"))
                            dtInspectReqLot.Rows[i]["AbnormalResultFlag"] = "NG";
                        else if (dtInspectReqLot.Rows[i]["AbnormalResultFlag"].ToString().Equals("H"))
                            dtInspectReqLot.Rows[i]["AbnormalResultFlag"] = "HOLD";
                    }

                    ////for (int i = 0; i < dtWMS.Rows.Count; i++)
                    ////{
                    ////    dtWMS.Rows[i]["InspectDesc"] = this.uTextInspectDesc.Text;
                    ////}

                    // WMS 데이터 전송 메소드 호출
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                    QRPINS.BL.INSIMP.MatInspectReqH clsReqH = new QRPINS.BL.INSIMP.MatInspectReqH();
                    brwChannel.mfCredentials(clsReqH);

                    string strErrRtn = string.Empty;
                    TransErrRtn ErrRtn = new TransErrRtn();

                    strErrRtn = clsReqH.mfSaveWMSMATInspectReq(dtWMS, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrMessage.Equals("00") && ErrRtn.ErrNum.Equals(0))
                    {
                        // 전송성공이면 WMSTFlag Update 메소드 호출
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                        QRPINS.BL.INSIMP.MaterialAbnormalH clsAbnormalH = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                        brwChannel.mfCredentials(clsAbnormalH);

                        strErrRtn = clsAbnormalH.mfSaveINSMaterialAbnormal_WMS_TFlag(strPlantCode, strStdNumber);
                        // 검사결과
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        // 처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            ////Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                            ////            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            ////            "WMS 전송처리결과", "원자재이상발생결과 WMS 전송처리결과", "원자재이상발생 결과를 WMS에 전송했습니다.",
                            ////            Infragistics.Win.HAlign.Right);

                            // 수입검사Lot 테이블에 AbnormalResultFlag 업데이트 메소드 호출
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                            QRPINS.BL.INSIMP.MatInspectReqLot clsReqLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                            brwChannel.mfCredentials(clsReqLot);

                            strErrRtn = clsReqLot.mfSaveINSMatInspectReqLot_Abnormal_Result(dtInspectReqLot, m_resSys.GetString("SYS_USERID")
                                                                                        , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            // 검사결과
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            // 처리결과에 따른 메세지 박스
                            if (ErrRtn.ErrNum == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M000156", "M000156", "M001385",
                                            Infragistics.Win.HAlign.Right);
                                // List 갱신
                                mfSearch();
                            }
                        }
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M000156", "M000156", "M001386",
                                                Infragistics.Win.HAlign.Right);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001037", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                    // List 갱신
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 업로드 Method
        /// </summary>
        /// <param name="strstdNumber"> 관리번호 </param>
        private void FileUpload_Header(string strstdNumber)
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                if (this.uTextLongFile.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextLongFile.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + strstdNumber + "-Long-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextLongFile.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                if (this.uTextShortFile.Text.Contains(":\\"))
                {
                    //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                    FileInfo fileDoc = new FileInfo(this.uTextShortFile.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                           m_strPlantCode + "-" + strstdNumber + "-Short-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextShortFile.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }
                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0008");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /////// <summary>
        /////// 첨부파일 업로드 Method
        /////// </summary>
        /////// <param name="strstdNumber"> 관리번호 </param>
        ////private void FileUpload(string strstdNumber)
        ////{
        ////    try
        ////    {
        ////        // 화일서버 연결정보 가져오기
        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
        ////        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
        ////        brwChannel.mfCredentials(clsSysAccess);
        ////        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

        ////        // 첨부파일 저장경로정보 가져오기
        ////        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
        ////        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
        ////        brwChannel.mfCredentials(clsSysFilePath);
        ////        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0008");

        ////        // 첨부파일 Upload하기
        ////        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
        ////        ArrayList arrFile = new ArrayList();

        ////        // RowIndex 찾기
        ////        int intRowIndex = 0;
        ////        // 검사항목순번으로 RowIndex 찾는다
        ////        for (int i = 0; i < this.uGridMatInspect.Rows.Count; i++)
        ////        {
        ////            if (this.uGridMatInspect.Rows[i].Cells["ReqItemSeq"].Value != null && this.uGridMatInspect.Rows[i].Cells["ReqItemSeq"].Value != DBNull.Value)
        ////            {
        ////                if (Convert.ToInt32(this.uGridMatInspect.Rows[i].Cells["ReqItemSeq"].Value) == ReqItemSeq)
        ////                {
        ////                    intRowIndex = this.uGridMatInspect.Rows[i].Index;
        ////                    break;
        ////                }
        ////            }
        ////        }

        ////        if (this.uGridMatInspect.Rows[intRowIndex].Cells["ReqFileName"].Value != null && this.uGridMatInspect.Rows[intRowIndex].Cells["ReqFileName"].Value.ToString() != "")
        ////        {
        ////            if (this.uGridMatInspect.Rows[intRowIndex].Cells["ReqFileName"].Value.ToString().Contains(":\\"))
        ////            {
        ////                //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
        ////                FileInfo fileDoc = new FileInfo(this.uGridMatInspect.Rows[intRowIndex].Cells["ReqFileName"].Value.ToString());
        ////                string strUploadFile = fileDoc.DirectoryName + "\\" +
        ////                                       m_strPlantCode + "-" + strstdNumber + "-" + this.uGridMatInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value.ToString() + "-" + fileDoc.Name;
        ////                //변경한 화일이 있으면 삭제하기
        ////                if (File.Exists(strUploadFile))
        ////                    File.Delete(strUploadFile);
        ////                //변경한 화일이름으로 복사하기
        ////                File.Copy(this.uGridMatInspect.Rows[intRowIndex].Cells["ReqFileName"].Value.ToString(), strUploadFile);
        ////                arrFile.Add(strUploadFile);
        ////            }
        ////        }

        ////        if (arrFile.Count > 0)
        ////        {
        ////            //Upload정보 설정
        ////            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
        ////                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
        ////                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
        ////                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
        ////                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
        ////            fileAtt.ShowDialog();
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        ////        frmErr.ShowDialog();
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        /// <param name="strStdNumber">관리번호</param>
        private void FileUpload(Infragistics.Win.UltraWinGrid.UltraGrid uGrid, string strStdNumber)
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (uGrid.Rows[i].Cells["FilePath"].Value != null && uGrid.Rows[i].Cells["FilePath"].Value.ToString() != "")
                    {
                        if (uGrid.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            //화일이름변경(공장코드+관리번호+화일명)하여 복사하기//
                            FileInfo fileDoc = new FileInfo(uGrid.Rows[i].Cells["FilePath"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                   m_strPlantCode + "-" +
                                                   strStdNumber + "-" +
                                                   uGrid.Rows[i].Cells["ReqNo"].Value.ToString() + "-" +
                                                   uGrid.Rows[i].Cells["ReqSeq"].Value.ToString() + "-" +
                                                   uGrid.Rows[i].Cells["ReqLotSeq"].Value.ToString() + "-" +
                                                   uGrid.Rows[i].Cells["ReqItemSeq"].Value.ToString() + "-" +
                                                   uGrid.Rows[i].Cells["FaultSeq"].Value.ToString() + "-" + 
                                                   fileDoc.Name;
                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            //변경한 화일이름으로 복사하기
                            File.Copy(uGrid.Rows[i].Cells["FilePath"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                    }
                }

                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0008");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 공정이상 메소드
        /// <summary>
        /// 공정이상발생시 헤더정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtHeader_Proc()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsHeader);

                // DataTable Columns 설정 Method 호출
                dtRtn = clsHeader.mfSetDataInfo();
                // DataTable 값 저장
                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = PlantCode;
                drRow["StdNumber"] = this.uTextStdNumber.Text;
                drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                drRow["WriteDate"] = Convert.ToDateTime(this.uDateWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["AbnormalType"] = this.uComboAbnormalType.Value.ToString();

                drRow["VendorCode"] = this.uTextVendorCode.Text;
                drRow["VendorName"] = this.uTextVendorName.Text;
                drRow["MaterialCode"] = this.uTextMaterialCode.Text;
                drRow["MaterialName"] = this.uTextMaterialName.Text;

                drRow["CompleteFlag"] = this.uCheckCompleteFlag.CheckedValue;
                drRow["LotNo"] = this.uTextLotNo.Text;
                drRow["AriseProcessCode"] = this.uComboAriseProcess.Value.ToString();
                drRow["AriseDate"] = Convert.ToDateTime(this.uDateAriseDate.Value).ToString("yyyy-MM-dd");
                drRow["FinalCompleteFlag"] = this.uCheckFinalCompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                drRow["ShortActionDesc"] = this.uTextShortActionDesc.Text;
                drRow["LongActionDesc"] = this.uTextLongActionDesc.Text;
                drRow["MaterialTreate"] = this.uTextMaterialTreate.Text;

                int intStatus;

                if (this.uComboStatus.Value == null || this.uComboStatus.Value.ToString().Equals(string.Empty))
                    intStatus = 10;
                else
                    intStatus = Convert.ToInt32(this.uComboStatus.Value);

                //원자재이상발생상태 우선순위대로 처리하여 저장
                if (this.uCheckFinalCompleteFlag.Checked)                                                         // 최종완료
                    drRow["StatusFlag"] = "0";
                else if (this.uOptLongConfirm.Value != null && this.uOptLongConfirm.Value.ToString().Equals("T")) // 장기승인
                    drRow["StatusFlag"] = "9";
                else if (this.uOptLongConfirm.Value != null && this.uOptLongConfirm.Value.ToString().Equals("F")) //장기반려
                    drRow["StatusFlag"] = "8";
                else if (this.uOptShortConfirm.Value != null && this.uOptShortConfirm.Value.ToString().Equals("T") && intStatus <= 8) // 단기승인(장기접수,장기재접수일경우포함)
                    drRow["StatusFlag"] = "5";
                else if (this.uOptShortConfirm.Value != null && this.uOptShortConfirm.Value.ToString().Equals("F") && intStatus <= 7) //단기반려(장기접수,장기재접수일경우포함)
                    drRow["StatusFlag"] = "4";
                //else if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)                   //발행
                //    drRow["StatusFlag"] = "1";
                //else if (!this.uCheckCompleteFlag.Checked)
                //    drRow["StatusFlag"] = "";
                else                                                                                    //접수,재접수
                    drRow["StatusFlag"] = this.uComboStatus.Value == null ? string.Empty : this.uComboStatus.Value;


                if (this.uTextShortFile.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextShortFile.Text);
                    //drRow["ShortFile"] = fileDoc.Name;
                    drRow["ShortFile"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Short-" + fileDoc.Name;
                }
                else
                {
                    drRow["ShortFile"] = this.uTextShortFile.Text;
                }
                if (this.uTextLongFile.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextLongFile.Text);
                    //drRow["LongFile"] = fileDoc.Name;
                    drRow["LongFile"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Long-" + fileDoc.Name;
                }
                else
                {
                    drRow["LongFile"] = this.uTextLongFile.Text;
                }

                byte[] empty = { };
                if (this.uTextShortFile.Tag != null && this.uTextShortFile.Tag != DBNull.Value)
                    drRow["ShortFileData"] = (byte[])this.uTextShortFile.Tag;
                //else if (this.uTextShortFile.Text.Equals(string.Empty))
                else if (this.uTextShortFile.Text.Equals(string.Empty) || this.uTextShortFile.Tag == null || this.uTextShortFile.Tag == DBNull.Value)
                    drRow["ShortFileData"] = empty;
                if (this.uTextLongFile.Tag != null && this.uTextLongFile.Tag != DBNull.Value)
                    drRow["LongFileData"] = (byte[])this.uTextLongFile.Tag;
                //else if (this.uTextLongFile.Text.Equals(string.Empty))
                else if (this.uTextLongFile.Text.Equals(string.Empty) || this.uTextLongFile.Tag == null || this.uTextLongFile.Tag == DBNull.Value)
                    drRow["LongFileData"] = empty;

                drRow["InspectDesc"] = this.uTextInspectDesc.Text;
                //-------------------------------------------------------------------------------------------------------------------------------------------------
                //단기조치사항
                drRow["ShortUserName"] = this.uTextShortUserName.Text;
                drRow["ShortEmail"] = this.uTextShortEmail.Text;
                drRow["ShortConfirmFlag"] = this.uOptShortConfirm.Value;
                drRow["ShortConfirmDesc"] = this.uTextShortConfirmDesc.Text;
                drRow["ShortConfirmUserID"] = this.uTextShortConfirmUserID.Text;

                //승인이 되었다면 현재 날짜 입력 , 이미승인,null,반려 일경우 공백 or 기존 값
                if (this.uOptShortConfirm.Value != null && this.uOptShortConfirm.Enabled)
                    drRow["ShortConfirmDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                else
                    drRow["ShortConfirmDate"] = this.uDateShortConfirmDate.Value == null ? "" : this.uDateShortConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                //=------------------------------------------------------------------------------------------------------------------------------------------------
                //장기조치사항
                drRow["LongUserName"] = this.uTextLongUserName.Text;
                drRow["LongEmail"] = this.uTextLongEmail.Text;
                drRow["LongConfirmFlag"] = this.uOptLongConfirm.Value;
                drRow["LongConfirmDesc"] = this.uTextLongConfirmDesc.Text;
                drRow["LongConfirmUserID"] = this.uTextLongConfirmUserID.Text;

                //승인이 되었다면 현재 날짜 입력 , 이미승인,null,반려 일경우 공백 or 기존 값
                if (this.uOptLongConfirm.Value != null && this.uOptLongConfirm.Enabled)
                    drRow["LongConfirmDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                else
                    drRow["LongConfirmDate"] = this.uDateLongConfirmDate.Value == null ? "" : this.uDateLongConfirmDate.DateTime.Date.ToString("yyyy-MM-dd");
                
                dtRtn.Rows.Add(drRow);
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정이상 상세정보 데이터 테이블 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtDetail_Proc()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                if (this.uGridProcInspect.Rows.Count > 0)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                    QRPINS.BL.INSIMP.MaterialAbnormalD clsDeatil = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                    brwChannel.mfCredentials(clsDeatil);

                    dtRtn = clsDeatil.mfSetDataInfo();
                    DataRow drRow;

                    this.uGridProcInspect.ActiveCell = this.uGridProcInspect.Rows[0].Cells[0];
                    for (int i = 0; i < this.uGridProcInspect.Rows.Count; i++)
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uGridProcInspect.Rows[i].Cells["PlantCode"].Value.ToString();
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["ReqNo"] = this.uGridProcInspect.Rows[i].Cells["ReqNo"].Value.ToString();
                        drRow["ReqSeq"] = this.uGridProcInspect.Rows[i].Cells["ReqSeq"].Value.ToString();
                        drRow["ReqLotSeq"] = this.uGridProcInspect.Rows[i].Cells["ReqLotSeq"].Value;
                        drRow["ReqItemSeq"] = this.uGridProcInspect.Rows[i].Cells["ReqItemSeq"].Value;
                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정이상발생 불량정보 데이터 테이블 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtDetailFault_Proc()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                if (this.uGridProcInspectFault.Rows.Count > 0)
                {
                    this.uGridProcInspectFault.ActiveCell = this.uGridProcInspectFault.Rows[0].Cells[0];

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalDFault), "MaterialAbnormalDFault");
                    QRPINS.BL.INSIMP.MaterialAbnormalDFault clsDFault = new QRPINS.BL.INSIMP.MaterialAbnormalDFault();
                    brwChannel.mfCredentials(clsDFault);

                    dtRtn = clsDFault.mfSetDataInfo();
                    DataRow drRow;

                    for (int i = 0; i < this.uGridProcInspectFault.Rows.Count; i++)
                    {
                        //if (this.uGridProcInspectFault.Rows[i].Hidden.Equals(false))
                        //{
                        if (this.uGridProcInspectFault.Rows[i].Hidden)
                        {
                            if (this.uGridProcInspectFault.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                                continue;
                        }

                        drRow = dtRtn.NewRow();
                        //drRow["PlantCode"] = this.uGridProcInspectFault.Rows[i].Cells["PlantCode"].Value.ToString();
                        drRow["PlantCode"] = PlantCode;
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["ReqNo"] = this.uGridProcInspectFault.Rows[i].Cells["ReqNo"].Value.ToString();
                        drRow["ReqSeq"] = this.uGridProcInspectFault.Rows[i].Cells["ReqSeq"].Value.ToString();
                        drRow["ReqLotSeq"] = this.uGridProcInspectFault.Rows[i].Cells["ReqLotSeq"].Value;
                        drRow["ReqItemSeq"] = this.uGridProcInspectFault.Rows[i].Cells["ReqItemSeq"].Value;
                        drRow["FaultSeq"] = this.uGridProcInspectFault.Rows[i].Cells["FaultSeq"].Value;
                        drRow["InspectQty"] = this.uGridProcInspectFault.Rows[i].Cells["InspectQty"].Value;
                        drRow["FaultQty"] = this.uGridProcInspectFault.Rows[i].Cells["FaultQty"].Value;
                        drRow["FaultTypeCode"] = this.uGridProcInspectFault.Rows[i].Cells["FaultTypeCode"].Value.ToString();
                        drRow["FaultTypeName"] = this.uGridProcInspectFault.Rows[i].Cells["FaultTypeCode"].Text;
                        if (this.uGridProcInspectFault.Tag == null || this.uGridProcInspectFault.Tag.ToString().Equals(string.Empty)) //uGridProcInspect 더블클릭시 더블클릭한 LotNo저장함
                            drRow["LotNo"] = this.uTextLotNo.Text;
                        else
                            drRow["LotNo"] = this.uGridProcInspectFault.Tag;

                        drRow["EtcDesc"] = this.uGridProcInspectFault.Rows[i].Cells["EtcDesc"].Value.ToString();


                        if (string.IsNullOrEmpty(this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString()) || this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value == null)
                        {
                            drRow["FilePath"] = string.Empty;
                        }
                        else if (this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString());
                            drRow["FilePath"] = fileDoc.Name;
                        }
                        else
                            drRow["FilePath"] = this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString();

                        byte[] empty = { };
                        if (this.uGridProcInspectFault.Rows[i].Cells["FileData"].Value != null && this.uGridProcInspectFault.Rows[i].Cells["FileData"].Value != DBNull.Value
                            && !this.uGridProcInspectFault.Rows[i].Cells["FileData"].Value.ToString().Equals(string.Empty))
                            drRow["FileVarValue"] = (byte[])this.uGridProcInspectFault.Rows[i].Cells["FileData"].Value;
                        else
                            drRow["FileVarValue"] = empty;

                        drRow["DeleteFlag"] = this.uGridProcInspectFault.Rows[i].Cells["DeleteFlag"].Value;

                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// WMS 전송용 데이터 테이블 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtWMS_Proc()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsAH = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsAH);

                dtRtn = clsAH.mfSetWMSDataTable();
                DataRow drRow;

                if (this.uCheckCompleteFlag.Checked && !this.uTextWMSTFlag.Text.Equals("T"))
                {
                    for (int i = 0; i < this.uGridProcInspect.Rows.Count; i++)
                    {
                        if (!this.uGridProcInspect.Rows[i].GetCellValue("AbnormalResultFlag").ToString().Equals("NG"))
                        {
                            drRow = dtRtn.NewRow();
                            drRow["PlantCode"] = PlantCode;
                            drRow["GRNo"] = this.uGridProcInspect.Rows[i].Cells["GRNo"].Value.ToString();
                            drRow["LotNo"] = this.uGridProcInspect.Rows[i].Cells["LotNo"].Value.ToString();
                            drRow["InspectResultFlag"] = "F";
                            drRow["IFFlag"] = "P";
                            drRow["AdmitDesc"] = this.uTextInspectDesc.Text;
                            dtRtn.Rows.Add(drRow);
                        }
                    }
                }

                //return dtRtn = dtRtn.DefaultView.ToTable(true, "PlantCode", "GRNo", "LotNo", "InspectResultFlag", "IFFlag", "AdmitDesc");
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정이상 WMS,MES 전송 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        private void Send_WMS_MES_Proc(string strPlantCode, string strStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                bool bolCheck = true;

                if (this.uCheckCompleteFlag.Checked && !this.uTextWMSTFlag.Text.Equals("T"))
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                    QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                    brwChannel.mfCredentials(clsHeader);

                    DataTable dtWMS = Rtn_dtWMS_Proc();

                    string strErrRtn = string.Empty;
                    TransErrRtn ErrRtn = new TransErrRtn();

                    // WMS 데이터 전송 메소드 호출
                    strErrRtn = clsHeader.mfSaveWMSMATAbnormal(dtWMS, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    string strErr = "";

                    if (ErrRtn.ErrMessage.Equals("00") && ErrRtn.ErrNum.Equals(0))
                    {
                        // 저장성공시 WMS Flag 업데이트
                        strErrRtn = clsHeader.mfSaveINSMaterialAbnormal_WMS_TFlag(strPlantCode, strStdNumber);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            // 수입검사 테이블 AbnormalResultFlag Update
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                            QRPINS.BL.INSIMP.MatInspectReqLot clsReqLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                            brwChannel.mfCredentials(clsReqLot);

                            DataTable dtReqLot = clsReqLot.mfSetDataInfo_Abnormal();
                            DataRow drRow;
                            for (int i = 0; i < this.uGridProcInspect.Rows.Count; i++)
                            {
                                drRow = dtReqLot.NewRow();
                                drRow["PlantCode"] = this.uGridProcInspect.Rows[i].Cells["PlantCode"].Value.ToString();
                                drRow["ReqNo"] = this.uGridProcInspect.Rows[i].Cells["ReqNo"].Value.ToString();
                                drRow["ReqSeq"] = this.uGridProcInspect.Rows[i].Cells["ReqSeq"].Value.ToString();
                                drRow["ReqLotSeq"] = this.uGridProcInspect.Rows[i].Cells["ReqLotSeq"].Value;
                                drRow["AbnormalResultFlag"] = "HOLD";
                                dtReqLot.Rows.Add(drRow);
                            }
                            strErrRtn = clsReqLot.mfSaveINSMatInspectReqLot_Abnormal_Result(dtReqLot, m_resSys.GetString("SYS_USERID")
                                                                                        , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                bolCheck = false;
                                strErr = "M001387";

                            }
                            else
                                strErr = "M001388";
                                
                            
                             Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001389", strErr, Infragistics.Win.HAlign.Center);
                        }
                        else
                        {
                            bolCheck = false;
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001389", "M001387", Infragistics.Win.HAlign.Center);
                        }
                    }
                    else
                    {
                        bolCheck = false;
                        
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strErr = msg.GetMessge_Text("M001387", strErr);
                        else
                            strErr = ErrRtn.ErrMessage;

                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001389", strLang)
                            , strErr, Infragistics.Win.HAlign.Center);
                    }
                }

                // MES
                if (this.uCheckCompleteFlag.Checked && !this.uTextMESTFlag.Text.Equals("T"))
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                    QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                    brwChannel.mfCredentials(clsHeader);

                    string strErrRtn = string.Empty;
                    TransErrRtn ErrRtn = new TransErrRtn();

                    DataTable dtProcInspectLotNo = RtnMESProcInspectLotNo();
                    DataTable dtMESInfo = RtnMESStdInfo(strStdNumber);

                    strErrRtn = clsHeader.mfINSMaterialAbnormal_MES(dtMESInfo, dtProcInspectLotNo, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum.Equals(0) && ErrRtn.InterfaceResultCode.Equals("0"))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000089", "M001390", Infragistics.Win.HAlign.Center);
                    }
                    else
                    {
                        bolCheck = false;
                        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000089", "M001391", Infragistics.Win.HAlign.Center);
                        }
                        else
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M000089", strLang)
                                , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Center);
                        }
                    }
                }

                //if (!this.uCheckCompleteFlag.Checked)
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M001037", "M000930"
                                            , Infragistics.Win.HAlign.Right);
                }

                if (bolCheck)
                    mfSearch();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }                    
        #endregion

        /// <summary>
        /// E-Mail 검색
        /// </summary>
        private void SendMail()
        {
            try
            {
                bool bolRtn = true;

                //원자재 이상발생이 작성완료되어 저장될 경우 (최초 1회)
                if (this.uCheckCompleteFlag.Checked && this.uCheckCompleteFlag.Enabled)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 협력업체 담당자(공통기준정보의 거래처정보에 등록된 사람)
                    // 해당 거래처업체 담당자 정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    DataTable dtVendorP = clsVendor.mfReadVendorP(this.uTextVendorCode.Text);

                    // 사용자 공통코드 사용 : 2012.10.04 by.Lee
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                    QRPSYS.BL.SYSPGM.UserCommonCode clsUserCC = new QRPSYS.BL.SYSPGM.UserCommonCode();
                    brwChannel.mfCredentials(clsUserCC);

                    DataTable dtMailUser = clsUserCC.mfReadUserCommonCode("QUA", "U0016", m_resSys.GetString("SYS_LANG"));

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    for (int i = 0; i < dtMailUser.Rows.Count; i++)
                    {
                        DataTable dtUserInfo = clsUser.mfReadSYSUser(PlantCode, dtMailUser.Rows[i]["ComCodeName"].ToString(), m_resSys.GetString("SYS_LANG"));

                        if (dtUserInfo.Rows.Count > 0)
                        {
                            bolRtn = SendMail(dtUserInfo.Rows[0]["EMail"].ToString());
                        }
                    }

                    // 메일보내기
                    if (dtVendorP.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtVendorP.Rows.Count; i++)
                        {
                            bolRtn = SendMail(dtVendorP.Rows[i]["Email"].ToString());//보내려는 사람 이메일주소
                        }
                    }
                }

                //작성완료되어 단기 or 장기조치사항이 접수되었을 경우 
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled
                    && this.uCheckFinalCompleteFlag.Enabled && !this.uCheckFinalCompleteFlag.Checked)
                {

                    //단기조치사항 반려 시
                    if (!this.uTextShortUserName.Text.Equals(string.Empty)
                        && this.uOptShortConfirm.Value.ToString().Equals("F"))
                        bolRtn = SendFailMail(this.uTextShortConfirmUserID.Text, this.uTextShortConfirmDesc.Text);//보내려는 사람 ID


                    //장기조치사항 반려 시
                    if (!this.uTextLongUserName.Text.Equals(string.Empty)
                        && this.uOptLongConfirm.Value.ToString().Equals("F"))
                        bolRtn = SendFailMail(this.uTextLongConfirmUserID.Text, this.uTextLongConfirmDesc.Text);//보내려는 사람 ID

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 메일보내기
        /// </summary>
        private bool SendMail(string strEmail)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();

                #region 디렉토리 생성 및 변수선언
                //원자재 불량 파일 디렉토리 
                string strBrowserPath = Application.ExecutablePath;
                int intPos = strBrowserPath.LastIndexOf(@"\");
                string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "FaultyType\\";
                string m_strQRPBrowserActive = strBrowserPath.Substring(0, intPos + 1) + "GWpdf\\";

                //디렉토리 유무확인
                DirectoryInfo di = new DirectoryInfo(m_strQRPBrowserPath);

                //지정된경로에 폴더가 없을 경우 폴더생성
                if (di.Exists.Equals(false))
                {
                    di.Create();
                }

                string strStdNumber = this.uTextStdNumber.Text;
                string strFileName = "";    // 파일명
                ArrayList arrFile = new ArrayList();    //첨부 파일 경로 정보
                ArrayList arrFileName = new ArrayList(); //메일 첨부파일 경로에 업로드한 파일명
                QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                #endregion

                #region 불량유형파일 정보 검색
                //불량유형파일정보 검색
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsMaterialAbnormalH = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsMaterialAbnormalH);

                // DataTable dtFileInfo = clsMaterialAbnormalH.mfReadINSMaterialAbnormal_FileInfo(PlantCode, this.uTextStdNumber.Text, this.uTextLotNo.Text);

                clsMaterialAbnormalH.Dispose();
                #endregion

                #region 파일데이터 변환하여 사용자 PC에 저장 2012-12-17 임시 주석

                //for (int i = 0; i < dtFileInfo.Rows.Count; i++)
                //{
                //    if (!dtFileInfo.Rows[i]["FilePath"].ToString().Equals(string.Empty)
                //        && dtFileInfo.Rows[i]["FileData"] != DBNull.Value
                //        && dtFileInfo.Rows[i]["FileData"].ToString().Equals("System.Byte[]"))
                //    {

                //        strFileName = dtFileInfo.Rows[i]["FilePath"].ToString(); // 파일명
                //        byte[] btFileByte = (byte[])dtFileInfo.Rows[i]["FileData"]; //파일데이터

                //        //파일여부 체크 존재한다면 삭제
                //        if (File.Exists(m_strQRPBrowserPath + strFileName))
                //        {
                //            File.Delete(m_strQRPBrowserPath + strFileName);
                //        }

                //        System.IO.FileStream fs = new System.IO.FileStream(m_strQRPBrowserPath + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                //        fs.Write(btFileByte, 0, btFileByte.Length);
                //        fs.Close();


                //        string strFilePath = m_strQRPBrowserPath + strFileName;
                //        arrFile.Add(strFilePath);
                //        arrFileName.Add(strFileName);

                //    }
                //}

                #endregion

                #region 자재이상발생보고서

                DataTable dtHeader = SearchReport_Header();
                DataTable dtHistory = SearchReport_History();
                DataTable dtContents = SearchReport_ContentsState(m_resSys.GetString("SYS_LANG"));
                DataTable dtState = dtContents.Copy();

                if (dtState.Select("EtcDesc <> ''").Count() == 0 && dtState.Select("FilePath <> ''").Count() == 0)
                {
                    dtState.Clear();
                    DataRow drRow = dtState.NewRow();
                    dtState.Rows.Add(drRow);
                    drRow = dtState.NewRow();
                    dtState.Rows.Add(drRow);
                }

                //frmINS0017_Report frmReport = new frmINS0017_Report(dtHeader, dtHistory, dtContents, dtState);

                //ActiveReport 완료여부
                //if (frmReport.FileChk != null && !frmReport.FileChk.Equals(string.Empty) && File.Exists(m_strQRPBrowserActive + frmReport.FileChk))
                //{
                //    arrFile.Add(m_strQRPBrowserActive + frmReport.FileChk);

                //    arrFileName.Add(frmReport.FileChk);
                //}


                #endregion

                #region 저장된 파일 이메일 발송 첨부파일로 업로드

                if (arrFile.Count > 0)
                {
                    //첨부파일을 위한 FileServer 연결정보
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //메일 발송 첨부파일 경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFileDestPath);
                    DataTable dtFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(PlantCode, "D0022");

                    //메일에 첨부할 파일을 서버에서 검색, 메일 첨부파일 폴더로 Upload
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }

                #endregion

                #region 메일전송

                brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                brwChannel.mfCredentials(clsmail);

                if (arrFile.Count != 0)
                {
                    fileAtt.mfFileUpload_NonAssync();
                }

                string strLang = m_resSys.GetString("SYS_LANG");
                string strTitle = string.Empty;
                string strContents = string.Empty;

                if (strLang.Equals("KOR"))
                {
                    strTitle = "원자재 이상발생 등록되었습니다.";
                    strContents = "<HTML>" +
                                    "<HEAD>" +
                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                "</HEAD>" +
                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextVendorName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 입고일자 : </SPAN>" + this.uTextGRDate.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                    "</BODY>" +
                                 "</HTML>";
                }
                else if (strLang.Equals("CHN"))
                {
                    strTitle = "原材料异常发生已登录";
                    strContents = "<HTML>" +
                                    "<HEAD>" +
                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                "</HEAD>" +
                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 委托情报 *****</SPAN></STRONG></P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 供应商名称: </SPAN>" + this.uTextVendorName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 入库日期 : </SPAN>" + this.uTextGRDate.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料名&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                    "</BODY>" +
                                 "</HTML>";
                }
                else if (strLang.Equals("ENG"))
                {
                    strTitle = "원자재 이상발생 등록되었습니다.";
                    strContents = "<HTML>" +
                                    "<HEAD>" +
                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                "</HEAD>" +
                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextVendorName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 입고일자 : </SPAN>" + this.uTextGRDate.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                    "</BODY>" +
                                 "</HTML>";
                }

                bool bolRtn = clsmail.mfSendSMTPMail(PlantCode
                                                , m_resSys.GetString("SYS_EMAIL")
                                                , m_resSys.GetString("SYS_USERNAME")
                                                , strEmail          //보내려는 사람 이메일주소
                                                , strTitle
                                                , strContents
                                                , arrFileName);

                return bolRtn;

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 반려메일보내기
        /// </summary>
        /// <param name="strAdmitUserID"></param>
        /// <returns></returns>
        private bool SendFailMail(string strAdmitUserID, string strReturnMes)
        {
            try
            {
                if (!strAdmitUserID.Equals(string.Empty)) // 메일기능 됨. 단지 Live 웹수발주와의 Test 진행안됨. 진행 후 return 코드 지우기.
                    return true;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPBrowser brwChannel = new QRPBrowser();
                bool bolRtn = true;

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUserInfo = clsUser.mfReadSYSUser(PlantCode, strAdmitUserID, m_resSys.GetString("SYS_LANG"));

                if (dtUserInfo.Rows.Count == 0)
                    return false;


                // 협력업체 담당자(공통기준정보의 거래처정보에 등록된 사람)
                // 해당 거래처업체 담당자 정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                DataTable dtVendorP = new DataTable();// clsVendor.mfReadVendorP(this.uTextVendorCode.Text);

                dtVendorP.Columns.Add("Email");

                DataRow drRow = dtVendorP.NewRow();
                drRow["Email"] = "mespjt36@bokwang"; //mespjt36@bokwang,djinkim@bokwang.com
                dtVendorP.Rows.Add(drRow);

                if (dtVendorP.Rows.Count > 0)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                    QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                    brwChannel.mfCredentials(clsmail);

                    ArrayList arrFile = new ArrayList();

                    string strLang = m_resSys.GetString("SYS_LANG");
                    string strTitle = string.Empty;
                    string strContents = string.Empty;

                    if (strLang.Equals("KOR"))
                    {
                        strTitle = "[PSTS]. 이상발생 단기조치(장기조치)사항이 반려되었습니다.";
                        strContents = "<HTML>" +
                                        "<HEAD>" +
                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                    "</HEAD>" +
                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextVendorName.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 입고일자 : </SPAN>" + this.uTextGRDate.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려자&nbsp&nbsp&nbsp&nbsp: </SPAN>" + dtUserInfo.Rows[0]["UserName"].ToString() + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려사유 : </SPAN>" + strReturnMes + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                        "</BODY>" +
                                     "</HTML>";
                    }
                    else if (strLang.Equals("CHN"))
                    {
                        strTitle = "[PSTS]. 异常发生短期措施（长期措施）事项已退回.";
                        strContents = "<HTML>" +
                                        "<HEAD>" +
                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                    "</HEAD>" +
                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 委托情报 *****</SPAN></STRONG></P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 供应商名称: </SPAN>" + this.uTextVendorName.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 入库日期 : </SPAN>" + this.uTextGRDate.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料名&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 返还者&nbsp&nbsp&nbsp&nbsp: </SPAN>" + dtUserInfo.Rows[0]["UserName"].ToString() + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 返还事由 : </SPAN>" + strReturnMes + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                        "</BODY>" +
                                     "</HTML>";
                    }
                    else if (strLang.Equals("ENG"))
                    {
                        strTitle = "[PSTS]. 이상발생 단기조치(장기조치)사항이 반려되었습니다.";
                        strContents = "<HTML>" +
                                        "<HEAD>" +
                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                    "</HEAD>" +
                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextVendorName.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 입고일자 : </SPAN>" + this.uTextGRDate.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp&nbsp&nbsp&nbsp: </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려자&nbsp&nbsp&nbsp&nbsp: </SPAN>" + dtUserInfo.Rows[0]["UserName"].ToString() + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려사유 : </SPAN>" + strReturnMes + "</P>" +
                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                        "</BODY>" +
                                     "</HTML>";
                    }


                    for (int i = 0; i < dtVendorP.Rows.Count; i++)
                    {
                        bolRtn = clsmail.mfSendSMTPMail(PlantCode
                                                        , dtUserInfo.Rows[0]["Email"].ToString()
                                                        , dtUserInfo.Rows[0]["UserName"].ToString()
                                                        , dtVendorP.Rows[i]["Email"].ToString()          //보내려는 사람 이메일주소
                                                        , strTitle
                                                        , strContents
                                                        , arrFile);
                    }


                }

                return bolRtn;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }


        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uTextPlant.Text = "";
                this.uTextStdNumber.Text = "";
                this.uTextVendorCode.Text = "";
                this.uTextVendorName.Text = "";
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialName.Text = "";
                this.uTextMaterialSpec1.Text = "";
                this.uTextMoldSeq.Text = "";
                this.uComboAbnormalType.Value = "";

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strUserID = m_resSys.GetString("SYS_USERID");
                this.uTextWriteUserID.Text = strUserID;
                this.uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");  

                this.uDateWriteDate.Value = DateTime.Now;
                this.uTextGRDate.Text = "";
                this.uTextTotalQty.Text = "";
                this.uComboStatus.Value = "";

                while (this.uGridMatInspect.Rows.Count > 0)
                {
                    this.uGridMatInspect.Rows[0].Delete(false);
                }
                while (this.uGridMatInspectFault.Rows.Count > 0)
                {
                    this.uGridMatInspectFault.Rows[0].Delete(false);
                }
                while (this.uGridProcInspect.Rows.Count > 0)
                {
                    this.uGridProcInspect.Rows[0].Delete(false);
                }
                while (this.uGridProcInspectFault.Rows.Count > 0)
                {
                    this.uGridProcInspectFault.Rows[0].Delete(false);
                }

                this.uDateWriteDate.Enabled = true;
                this.uTextWriteUserID.Enabled = true;

                this.uTextLotNo.Enabled = true;
                this.uComboAriseProcess.Enabled = true;
                this.uDateAriseDate.Enabled = true;

                this.uCheckCompleteFlag.Checked = false;
                this.uCheckCompleteFlag.Enabled = true;

                this.uTextLotNo.Text = "";
                this.uDateAriseDate.Value = DateTime.Now;
                this.uComboAriseProcess.Items.Clear();
                this.uComboAriseProcess.Items.Add("", "선택");
                this.uComboAriseProcess.Value = "";

                this.uTextMESTFlag.Text = "F"; //MES전송여부
                this.uTextCompleteFlag.Text = "F"; //작성완료여부
                this.uButtonMoveDisplay.Visible = false; //수입검사등록화면이동버튼

                this.uTextShortFile.Clear();
                this.uTextLongFile.Clear();
                this.uTextShortFile.Tag = null;
                this.uTextLongFile.Tag = null;
                this.uCheckFinalCompleteFlag.Checked = false;
                this.uCheckFinalCompleteFlag.Enabled = true;
                this.uTextLongActionDesc.Clear();
                this.uTextShortActionDesc.Clear();
                this.uDateShortAcceptDate.Value = null;//DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateLongAcceptDate.Value = null;//DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateShortConfirmDate.Value = null;
                this.uDateLongConfirmDate.Value = null;
                this.uTextMaterialTreate.Clear();
                this.uTextInspectDesc.Clear();


                //장기 단기 승인
                this.uOptShortConfirm.Enabled = true;
                this.uTextShortConfirmDesc.Enabled = true;
                this.uTextShortConfirmUserID.Enabled = true;
                this.uOptLongConfirm.Enabled = true;
                this.uTextLongConfirmDesc.Enabled = true;
                this.uTextLongConfirmUserID.Enabled = true;
                this.uTextShortUserName.Clear();
                this.uTextShortEmail.Clear();
                this.uOptShortConfirm.Value = null;
                this.uTextShortConfirmDesc.Clear();
                this.uTextShortConfirmUserID.Clear();
                this.uTextShortConfirmUserName.Clear();

                this.uTextLongUserName.Clear();
                this.uTextLongEmail.Clear();
                this.uOptLongConfirm.Value = null;
                this.uTextLongConfirmDesc.Clear();
                this.uTextLongConfirmUserID.Clear();
                this.uTextLongConfirmUserName.Clear();



                this.ultraTabControl1.Tabs[0].Visible = true;
                this.ultraTabControl1.Tabs[1].Visible = true;

                this.uGridMatInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                this.uGridMatInspect.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridMatInspectFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridProcInspect.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                if (this.uGridProcInspectFault.Tag != null)
                    this.uGridProcInspectFault.Tag = string.Empty;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private DataTable RtnMESStdInfo(string strStdNumber)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsH = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsH);

                dtRtn = clsH.mfSetDataTable_MES();

                // DB에서 HoldCode를 가져온다
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                brwChannel.mfCredentials(clsReason);

                DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(PlantCode, this.Name);

                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = PlantCode;
                drRow["StdNumber"] = strStdNumber;
                drRow["FormName"] = this.Name;
                drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                drRow["Comment"] = "";
                if (dtReason.Rows.Count > 0)
                    drRow["HoldCode"] = dtReason.Rows[0]["ReasonCode"].ToString();
                else
                    drRow["HoldCode"] = "HQ04";

                dtRtn.Rows.Add(drRow);
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공정이상발생정보 그리드 LotNo정보 저장
        /// </summary>
        /// <returns>LotNo정보</returns>
        private DataTable RtnMESProcInspectLotNo()
        {
            DataTable dtProcInspectLotNo = new DataTable();
            try
            {
                //컬럼설정
                dtProcInspectLotNo.Columns.Add("CONSUMABLEID",typeof(string));

                // 정보가있을 경우 저장
                if (this.uGridProcInspect.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridProcInspect.Rows.Count; i++)
                    {                        
                        // LotNo를 저장한다.
                        DataRow drProcInspect = dtProcInspectLotNo.NewRow();
                        drProcInspect["CONSUMABLEID"] = this.uGridProcInspect.Rows[i].Cells["LotNo"].Value.ToString();
                        dtProcInspectLotNo.Rows.Add(drProcInspect);
                    }
                }

                // 중복행 제거
                dtProcInspectLotNo = dtProcInspectLotNo.DefaultView.ToTable(true, "CONSUMABLEID");
                return dtProcInspectLotNo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtProcInspectLotNo;
            }
            finally
            {
            }

        }
        
        /// <summary>
        /// 파일을 Byte[] 로 변환하여 반환하는 메소드
        /// </summary>
        /// <param name="strFileFullPath">파일경로</param>
        /// <returns></returns>
        private byte[] fun_ConvertFileToBinray(string strFileFullPath)
        {
            // FileStream 자동 소멸을 위한 Using 구문
            using (System.IO.FileStream fs = new System.IO.FileStream(strFileFullPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                int intLength = Convert.ToInt32(fs.Length);
                System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                byte[] btBuffer = br.ReadBytes(intLength);
                fs.Close();

                return btBuffer;
            }
        }

        #endregion

        #region 이벤트

        #region 공통 이벤트
        // 콘텐츠 그룹박스 상태변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 720;
                    for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                    {
                        this.uGridHeader.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 거래처 팝업창
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재코드 팝업창
        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자 팝업창
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (!this.uTextWriteUserID.Enabled)
                    return;

                if (PlantCode == null || PlantCode == "")
                {
                    PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (m_strPlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextWriteUserID.Text = "";
                    this.uTextWriteName.Text = "";
                }
                else
                {
                    this.uTextWriteUserID.Text = frmPOP.UserID;
                    this.uTextWriteName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재코드 키다운 이벤트
        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.SelectedText == this.uTextSearchMaterialCode.Text)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자 키다운 이벤트
        private void uTextWriteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextWriteUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(m_strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextWriteUserID.Text = "";
                            this.uTextWriteName.Text = "";
                        }
                        else
                        {
                            this.uTextWriteName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteUserID.TextLength <= 1 || this.uTextWriteUserID.SelectedText == this.uTextWriteUserID.Text)
                    {
                        this.uTextWriteName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //단기조치사항승인자 
        private void uTextShortConfirmUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextShortConfirmUserName.Text.Equals(string.Empty))
                this.uTextShortConfirmUserName.Clear();
        }

        //단기조치사항승인자 사용자팝업창조회
        private void uTextShortConfirmUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode == null || PlantCode == "")
                {
                    PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (m_strPlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextShortConfirmUserID.Clear();
                    this.uTextShortConfirmUserName.Clear();
                }
                else
                {
                    this.uTextShortConfirmUserID.Text = frmPOP.UserID;
                    this.uTextShortConfirmUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        //단기조치사항승인자 키다운이벤트조회
        private void uTextShortConfirmUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextShortConfirmUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strConfirmID = this.uTextShortConfirmUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(m_strPlantCode, strConfirmID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextShortConfirmUserID.Clear();
                            this.uTextShortConfirmUserName.Clear();
                        }
                        else
                        {
                            this.uTextShortConfirmUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextShortConfirmUserID.TextLength <= 1 || this.uTextShortConfirmUserID.SelectedText == this.uTextShortConfirmUserID.Text)
                    {
                        this.uTextShortConfirmUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //장기조치사항승인자 
        private void uTextLongConfirmUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextLongConfirmUserName.Text.Equals(string.Empty))
                this.uTextLongConfirmUserName.Clear();
        }

        //장기조치사항승인자 사용자팝업창조회
        private void uTextLongConfirmUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode == null || PlantCode == "")
                {
                    PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (m_strPlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextLongConfirmUserID.Clear();
                    this.uTextLongConfirmUserName.Clear();
                }
                else
                {
                    this.uTextLongConfirmUserID.Text = frmPOP.UserID;
                    this.uTextLongConfirmUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                m_resSys.Close();
            }
        }

        //장기조치사항승인자 키다운이벤트조회
        private void uTextLongConfirmUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextLongConfirmUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strConfirmID = this.uTextLongConfirmUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(m_strPlantCode, strConfirmID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextLongConfirmUserID.Clear();
                            this.uTextLongConfirmUserName.Clear();
                        }
                        else
                        {
                            this.uTextLongConfirmUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextLongConfirmUserID.TextLength <= 1 || this.uTextLongConfirmUserID.SelectedText == this.uTextLongConfirmUserID.Text)
                    {
                        this.uTextLongConfirmUserName.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextWriteName.Text.Equals(string.Empty))
                this.uTextWriteName.Clear();
        }

        #endregion

        // 수입이상정보 발생 그리드 더블클릭 이벤트
        private void uGridMatInspect_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 불량정보 그리드 Key Cell의 DefaultCellValue 설정
                this.uGridMatInspectFault.DisplayLayout.Bands[0].Columns["ReqNo"].DefaultCellValue = e.Row.Cells["ReqNo"].Value;
                this.uGridMatInspectFault.DisplayLayout.Bands[0].Columns["ReqSeq"].DefaultCellValue = e.Row.Cells["ReqSeq"].Value;
                this.uGridMatInspectFault.DisplayLayout.Bands[0].Columns["ReqItemSeq"].DefaultCellValue = e.Row.Cells["ReqItemSeq"].Value;

                int intRowIndex = e.Row.Index;

                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled) //2012-04-23
                {
                    // 불량정보 조회 Method
                    SearchDFault_MatInspect(intRowIndex);
                    this.uGridMatInspect.Rows[intRowIndex].Activate();
                    this.uGridMatInspectFault.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    return;
                }

                // 불량 그리드에 데이터가 있으면 저장 메소드 호출
                if (this.uGridMatInspectFault.Rows.Count > 0)
                {
                    string strRtnStdNumber = string.Empty;
                    string strErrRtn = string.Empty;
                    TransErrRtn ErrRtn = new TransErrRtn();

                    // 관리번호가 없으면 헤더부터 저장
                    if (this.uTextStdNumber.Text.Equals(string.Empty))
                    {
                        DataTable dtHeader = Rtn_dtHeader_Mat();
                        DataTable dtDetail = Rtn_dtDetail_Mat();
                        DataTable dtDFault = Rtn_dtDetailFault_Mat();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                        QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                        brwChannel.mfCredentials(clsHeader);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장 Method 호출
                        strErrRtn = clsHeader.mfSaveINSMaterialAbnormalH_Click(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail, dtDFault);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        //m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    else
                    {
                        // 상세부터 저장
                        DataTable dtDetail = Rtn_dtDetail_Mat();
                        DataTable dtDFault = Rtn_dtDetailFault_Mat();

                        // 상세부터 저장
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                        QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                        brwChannel.mfCredentials(clsDetail);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // Method 호출
                        strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD_Mat(dtDetail, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDFault);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        //m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum == 0)
                    {
                        // OUTPUT 관리번호 저장
                        strRtnStdNumber = ErrRtn.mfGetReturnValue(0);

                        ////// FileUpload Method 호출
                        ////FileUpload_All(strRtnStdNumber);

                        // 관리번호가 공백일때 헤더조회 Method 호출
                        if (this.uTextStdNumber.Text == "")
                            SearchHeaderDetail(strRtnStdNumber);

                        // 상세
                        SearchDetail_Mat(strRtnStdNumber);
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                    }
                }

                // 불량정보 조회 Method
                SearchDFault_MatInspect(intRowIndex);
                this.uGridMatInspect.Rows[intRowIndex].Activate();
                this.uGridMatInspectFault.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                #region 기존소스 주석처리
                /* ///////////////////////////////////
                 * 2011.12.08 수정사항에 의한 주석처리
                 * ///////////////////////////////////
                int intRowIndex = e.Row.Index;

                // 불량정보 데이터 테이블에 저장하는 Method호출
                DataTable dtFalut = RtndtFault(true);

                // 상세정보 Datatable 저장 Method
                DataTable dtDetail = RtndtDetail(this.uGridMatInspect);

                
                // 검사결과가 불량일때만 저장 / 조회
                //if (e.Row.Cells["InspectResultFlag"].Value.ToString() == "NG")
                //if (uGridMatInspect.Rows[intRowIndex].Cells["InspectResultFlag"].Value.ToString() == "NG")
                //{
                    // SystemInfor ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    TransErrRtn ErrRtn = new TransErrRtn();
                    String strErrRtn = "";
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strRtnStdNumber = "";

                    // 불량정보 리스트에 저장할 값이 있을때
                    if (this.uGridMatInspectFault.Rows.Count > 0)
                    {
                        // 전역변수에 현재 그리드 행의 의뢰번호,의뢰순번, 의뢰Lot순번, 검사항목순번 저장
                        //ReqNo = e.Row.Cells["ReqNo"].Value.ToString();
                        //ReqSeq = e.Row.Cells["ReqSeq"].Value.ToString();
                        //ReqLotSeq = Convert.ToInt32(e.Row.Cells["ReqLotSeq"].Value);
                        //ReqItemSeq = Convert.ToInt32(e.Row.Cells["ReqItemSeq"].Value);

                        // 관리번호가 없으면 헤더부터 저장
                        if (this.uTextStdNumber.Text == "")
                        {
                            // 헤더정보 Datatable 저장 Method
                            DataTable dtHeader = RtndtHeader(true);

                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                            QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                            brwChannel.mfCredentials(clsHeader);

                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // 저장 Method 호출
                            strErrRtn = clsHeader.mfSaveINSMaterialAbnormalH_Click(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail, dtFalut);

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            //m_ProgressPopup.mfCloseProgressPopup(this);
                        }
                        else
                        {
                            // 상세부터 저장
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                            QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                            brwChannel.mfCredentials(clsDetail);

                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // Method 호출
                            strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD(dtDetail, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtFalut);

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            //m_ProgressPopup.mfCloseProgressPopup(this);
                        }

                        // 결과 검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            // OUTPUT 관리번호 저장
                            strRtnStdNumber = ErrRtn.mfGetReturnValue(0);

                            // FileUpload Method 호출
                            FileUpload(strRtnStdNumber);

                            //Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                            //                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                            //                            Infragistics.Win.HAlign.Right);

                            // 관리번호가 공백일때 헤더조회 Method 호출
                            if (this.uTextStdNumber.Text == "")
                                SearchHeaderDetail(strRtnStdNumber);

                            // 상세
                            SearchDetail(strRtnStdNumber, this.uGridMatInspect);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                            Infragistics.Win.HAlign.Right);
                        }
                    }
                //}

                // 전역변수에 현재 그리드 행의 의뢰번호,의뢰순번, 의뢰Lot순번, 검사항목순번 저장
                ReqNo = this.uGridMatInspect.Rows[intRowIndex].Cells["ReqNo"].Value.ToString();
                ReqSeq = this.uGridMatInspect.Rows[intRowIndex].Cells["ReqSeq"].Value.ToString();
                ReqLotSeq = Convert.ToInt32(this.uGridMatInspect.Rows[intRowIndex].Cells["ReqLotSeq"].Value);
                if (this.uGridMatInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value != DBNull.Value)
                    ReqItemSeq = Convert.ToInt32(this.uGridMatInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value);
                else
                    ReqItemSeq = 0;

                // 불량정보 조회 Method
                SearchDFault_MatInspect(intRowIndex);
                this.uGridMatInspect.Rows[intRowIndex].Activated = true;
                */
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        // 첨부파일 버튼 이벤트
        private void uGridMatInspect_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "ReqFileName")
                {
                    // 불량일 경우에만
                    if (e.Cell.Row.Cells["InspectResultFlag"].Value.ToString() == "NG")
                    {
                        System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                        openFile.Filter = "All files (*.*)|*.*";
                        openFile.FilterIndex = 1;
                        openFile.RestoreDirectory = true;

                        if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            e.Cell.Value = openFile.FileName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 헤더 그리드 더블클릭시 상세정보 조회
        private void uGridHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                e.Row.Fixed = true;

                PlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strStdNumber = e.Row.Cells["StdNumber"].Value.ToString();

                this.uTextCompleteFlag.Text = e.Row.Cells["CompleteFlag"].Value.ToString(); //작성완료여부
                this.uTextMESTFlag.Text = e.Row.Cells["MESTFlag"].Value.ToString(); //MES전송여부

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 공정이상인지 수입이상인지 검사
                if (strStdNumber.Substring(0, 4) == "AE02")
                {
                    // 조회 Method 호출
                    SearchHeaderDetail(strStdNumber);
                    SearchDetail_Mat(strStdNumber);

                    this.uTextLotNo.Enabled = false;
                    this.uComboAriseProcess.Enabled = false;
                    this.uDateAriseDate.Enabled = false;
                    this.uButtonMoveDisplay.Visible = true; //수입검사화면이동버튼
                }
                else if (strStdNumber.Substring(0, 4) == "AE03")
                {
                    // 발생공정 콤보박스 설정 Method 호출
                    SetAriseProcessCombo(PlantCode);

                    // 조회 Method 호출
                    SearchHeaderDetail(strStdNumber);
                    SearchDetail_Proc(strStdNumber);

                    //this.uTextLotNo.Enabled = true;
                    //this.uComboAriseProcess.Enabled = true;
                    //this.uDateAriseDate.Enabled = true;
                    this.uButtonMoveDisplay.Visible = false;//수입검사화면이동버튼
                }

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 다운로드 버튼 이벤트
        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridMatInspect.Rows.Count; i++)
                {
                    // 체크가 된 행에서 경로가 없는 행이 있는지 체크
                    if (Convert.ToBoolean(this.uGridMatInspect.Rows[i].Cells["Check"].Value) == true && this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value.ToString().Contains(":\\") == false)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    // 화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0008");

                    // 첨부파일 Download
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridMatInspect.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridMatInspect.Rows[i].Cells["Check"].Value) == true && this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value.ToString().Contains(":\\") == false)
                            arrFile.Add(m_strPlantCode + "-" + this.uTextStdNumber.Text + "-" + this.uGridMatInspect.Rows[i].Cells["ReqItemSeq"].Value.ToString() + "-" +
                                this.uGridMatInspect.Rows[i].Cells["ReqFileName"].Value.ToString());
                    }

                    // Download정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // LotNo TextBox 키다운 이벤트
        private void uTextLotNo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextLotNo.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        string strLotNo = this.uTextLotNo.Text;

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                        QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                        brwChannel.mfCredentials(clsDetail);

                        DataTable dtRtn = clsDetail.mfReadINSMaterialAbnormalD_InitProcessList(m_resSys.GetString("SYS_PLANTCODE"), strLotNo, m_resSys.GetString("SYS_LANG"));

                        // 헤더정보 조회
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                        QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                        brwChannel.mfCredentials(clsHeader);

                        DataTable dtHeader = clsHeader.mfReadINSMaterialAbnormalH_InitProcessH(m_resSys.GetString("SYS_PLANTCODE"), strLotNo, m_resSys.GetString("SYS_LANG"));

                        if (dtRtn.Rows.Count > 0 && dtHeader.Rows.Count > 0)
                        {
                            // 헤더 정보 Display
                            this.uTextPlant.Text = dtHeader.Rows[0]["PlantName"].ToString();
                            this.uTextVendorCode.Text = dtHeader.Rows[0]["VendorCode"].ToString();
                            this.uTextVendorName.Text = dtHeader.Rows[0]["VendorName"].ToString();
                            this.uTextMaterialCode.Text = dtHeader.Rows[0]["MaterialCode"].ToString();
                            this.uTextMaterialName.Text = dtHeader.Rows[0]["MaterialName"].ToString();
                            this.uTextMaterialSpec1.Text = dtHeader.Rows[0]["MaterialSpec1"].ToString();
                            this.uTextMoldSeq.Text = dtHeader.Rows[0]["MoldSeq"].ToString();
                            this.uTextGRDate.Text = dtHeader.Rows[0]["GRDate"].ToString();
                            this.uTextTotalQty.Text = Math.Floor(Convert.ToDecimal(dtHeader.Rows[0]["GRQty"].ToString())).ToString();//dtHeader.Rows[0]["GRQty"].ToString();

                            // LotNo TextBox 편집 불가 상태로
                            this.uTextLotNo.Enabled = false;

                            this.uGridProcInspect.DataSource = dtRtn;
                            this.uGridProcInspect.DataBind();

                            if (dtRtn.Rows.Count > 0)
                            {
                                WinGrid grd = new WinGrid();
                                grd.mfSetAutoResizeColWidth(this.uGridProcInspect, 0);

                                //입력한 LotNo와 동일한 LotNo의 줄의 색변경
                                for (int i = 0; i < this.uGridProcInspect.Rows.Count; i++)
                                {
                                    if (this.uGridProcInspect.Rows[i].Cells["LotNo"].Value.ToString().Equals(strLotNo))
                                    {
                                        this.uGridProcInspect.Rows[i].Appearance.BackColor = Color.Red;
                                    }
                                }
                            }

                            // 불량유형 저장할때 필요한 키값 설정
                            DataRow[] drLotRow = dtRtn.Select("LotNo = '" + this.uTextLotNo.Text + "'");
                            PlantCode = drLotRow[0]["PlantCode"].ToString();
                            ReqNo = drLotRow[0]["ReqNo"].ToString();
                            ReqSeq = drLotRow[0]["ReqSeq"].ToString();
                            ReqLotSeq = Convert.ToInt32(drLotRow[0]["ReqLotSeq"].ToString());
                            ReqItemSeq = Convert.ToInt32(drLotRow[0]["ReqItemSeq"].ToString());

                            // 발생공정 콤보박스 설정 Method 호출
                            SetAriseProcessCombo(PlantCode);

                            // 이상발생구분, 상태 콤보박스 설정
                            this.uComboAbnormalType.Value = "2";
                            this.uComboStatus.Value = "1";

                            this.ultraTabControl1.Tabs[0].Visible = false;

                            ////// 관리번호가 존재하는지 확인
                            ////string strStdNumber = drLotRow[0]["StdNumber"].ToString();

                            ////// 이미 등록된 관리번호가 없는경우
                            ////if (strStdNumber == "")
                            ////{
                            ////    WinGrid wGrid = new WinGrid();
                            ////    ////// 그리드 입력 가능하도록
                            ////    ////this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                            ////    ////// 그리드 공백줄 추가
                            ////    ////wGrid.mfAddRowGrid(this.uGridProcInspectFault, 0);
                            ////    ///////////////////////////////////////////////////////////////////////////////
                            ////    //////////////////////항목별로 불량데이터 입력하도록 변경//////////////////////
                            ////    ///////////////////////////////////////////////////////////////////////////////

                            ////    // 불량유형 DropDown 설정
                            ////    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                            ////    QRPMAS.BL.MASQUA.InspectFaultType clsFaultType = new QRPMAS.BL.MASQUA.InspectFaultType();
                            ////    brwChannel.mfCredentials(clsFaultType);

                            ////    DataTable dtFaultType = clsFaultType.mfReadInspectFaultTypeCombo(PlantCode, m_resSys.GetString("SYS_LANG"));

                            ////    wGrid.mfSetGridColumnValueList(this.uGridProcInspectFault, 0, "FaultTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtFaultType);
                            ////}
                            ////// 등록된 관리번호가 있는경우
                            ////else
                            ////{
                            ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                            ////            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            ////            "확인창", "조회확인", "이미 등록된 관리번호가 존재합니다",
                            ////            Infragistics.Win.HAlign.Right);

                            ////    // 등록된 데이터 조회
                            ////    SearchHeaderDetail(strStdNumber);
                            ////    SearchDetail(strStdNumber, this.uGridProcInspect);
                            ////    //SearchDFault_Process(PlantCode, strStdNumber);                // 항목별로 조회
                            ////}

                            // 수입이상발생 상세 그리드에 데이터가 있으면 삭제
                            while (this.uGridMatInspect.Rows.Count > 0)
                            {
                                this.uGridMatInspect.Rows[0].Delete(false);
                            }
                            while (this.uGridMatInspectFault.Rows.Count > 0)
                            {
                                this.uGridMatInspectFault.Rows[0].Delete(false);
                            }
                            
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001098", "M001392",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextLotNo.Text = "";

                            // 그리드 입력 불가능
                            this.uGridProcInspectFault.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                            while (this.uGridProcInspectFault.Rows.Count > 0)
                            {
                                this.uGridProcInspectFault.Rows[0].Delete(false);
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextLotNo.TextLength <= 1 || this.uTextLotNo.SelectedText == this.uTextLotNo.Text)
                    {
                        this.uComboAriseProcess.Items.Clear();
                        this.uComboAriseProcess.Items.Add("", "선택");
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 수입이상 불량데이터 행삭제 버튼 이벤트
        private void uButtonDelete1_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {

                    for (int i = 0; i < this.uGridMatInspectFault.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridMatInspectFault.Rows[i].Cells["Check"].Value).Equals(true))
                        {
                            this.uGridMatInspectFault.Rows[i].Hidden = true;
                            this.uGridMatInspectFault.Rows[i].Cells["DeleteFlag"].Value = "T";
                        }
                    }

                    #region 주석처리
                    /*
                    //첨부파일 저장경로정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0008");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                    for (int i = 0; i < this.uGridMatInspectFault.Rows.Count; i++)
                    {
                        
                        if (Convert.ToBoolean(this.uGridMatInspectFault.Rows[i].Cells["Check"].Value).Equals(true))
                        {
                            if (this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(PlantCode) &&
                                this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uTextStdNumber.Text) &&
                                this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridMatInspectFault.Rows[i].Cells["ReqNo"].Value.ToString()) &&
                                this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridMatInspectFault.Rows[i].Cells["ReqSeq"].Value.ToString()) &&
                                this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridMatInspectFault.Rows[i].Cells["ReqLotSeq"].Value.ToString()) &&
                                this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridMatInspectFault.Rows[i].Cells["ReqItemSeq"].Value.ToString()))
                            {

                                arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value.ToString());

                                this.uGridMatInspectFault.Rows[i].Cells["FilePath"].Value = string.Empty;
                                this.uGridMatInspectFault.Rows[i].Cells["FilePath"].SetValue(string.Empty, false);
                            }
                            this.uGridMatInspectFault.Rows[i].Hidden = true;
                        }

                        this.uGridMatInspectFault.Rows[i].Hidden = true;
                    }

                    /*
                    if (arrFile.Count > 0)
                    {
                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                            , arrFile
                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileUploadNoProgView();
                    }
                    * */

                    #endregion

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공정이상 불량데이터 행삭제 버튼 이벤트
        private void uButtonDelete2_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridProcInspectFault.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridProcInspectFault.Rows[i].Cells["Check"].Value).Equals(true))
                        {
                            this.uGridProcInspectFault.Rows[i].Hidden = true;
                            this.uGridProcInspectFault.Rows[i].Cells["DeleteFlag"].Value = "T";
                        }
                    }


                    #region 주석처리
                    //////첨부파일 저장경로정보 가져오기
                    ////QRPBrowser brwChannel = new QRPBrowser();
                    ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    ////QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    ////brwChannel.mfCredentials(clsSysFilePath);
                    ////DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0008");

                    //////첨부파일 Download하기
                    ////frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ////System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                    ////for (int i = 0; i < this.uGridProcInspectFault.Rows.Count; i++)
                    ////{
                    ////    if (Convert.ToBoolean(this.uGridProcInspectFault.Rows[i].Cells["Check"].Value).Equals(true))
                    ////    {
                    ////        if (this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(PlantCode) &&
                    ////            this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uTextStdNumber.Text) &&
                    ////            this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridProcInspectFault.Rows[i].Cells["ReqNo"].Value.ToString()) &&
                    ////            this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridProcInspectFault.Rows[i].Cells["ReqSeq"].Value.ToString()) &&
                    ////            this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridProcInspectFault.Rows[i].Cells["ReqLotSeq"].Value.ToString()) &&
                    ////            this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString().Contains(this.uGridProcInspectFault.Rows[i].Cells["ReqItemSeq"].Value.ToString()))
                    ////        {
                    ////            arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value.ToString());

                    ////            this.uGridProcInspectFault.Rows[i].Cells["FilePath"].Value = string.Empty;
                    ////            this.uGridProcInspectFault.Rows[i].Cells["FilePath"].SetValue(string.Empty, false);
                    ////        }
                    ////        this.uGridProcInspectFault.Rows[i].Hidden = true;
                    ////    }
                    ////}

                    ////if (arrFile.Count > 0)
                    ////{
                    ////    //화일서버 연결정보 가져오기
                    ////    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    ////    brwChannel.mfCredentials(clsSysAccess);
                    ////    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    ////    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                    ////                                        , arrFile
                    ////                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                    ////                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    ////    fileAtt.mfFileUploadNoProgView();
                    ////}
                    #endregion

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //수입검사등록화면이동
        private void uButtonMoveDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                if (uTextStdNumber.Text.Equals(string.Empty))
                    return;

                if (ReqNo == null || ReqSeq == null)
                    return;

                if (ReqNo.Equals(string.Empty) || ReqSeq.Equals(string.Empty))
                    return;

                //호출하려는 화면을 선언
                QRPINS.UI.frmINS0004 frmInspect = new QRPINS.UI.frmINS0004();
                frmInspect.MoveFormName = this.Name;
                if (PlantCode != null)
                    frmInspect.PlantCode = PlantCode;
                else
                    frmInspect.PlantCode = this.uTextPlant.Text;

                frmInspect.ReqNo = ReqNo;
                frmInspect.ReqSeq = ReqSeq;

                // 폼에서 폼이동 시
                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0004"))
                {
                    uTabMenu.Tabs["QRPINS" + "," + "frmINS0004"].Close();
                }
                uTabMenu.Tabs.Add("QRPINS" + "," + "frmINS0004", "수입검사등록/조회");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINS0004"];

                frmInspect.AutoScroll = true;
                frmInspect.MdiParent = this.MdiParent;
                frmInspect.ControlBox = false;
                frmInspect.Dock = DockStyle.Fill;
                frmInspect.FormBorderStyle = FormBorderStyle.None;
                frmInspect.WindowState = FormWindowState.Normal;
                frmInspect.Text = "수입검사등록/조회";
                frmInspect.Show();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 공정이상 발생 상세 그리드 더블클릭시 이벤트
        private void uGridProcInspect_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (e.Row.GetCellValue("AbnormalResultFlag").ToString().Equals("NG"))
                {
                    if (!e.Row.GetCellValue("LotNo").ToString().Equals(this.uTextLotNo.Text) || this.uCheckCompleteFlag.Enabled)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000073", "M001393",
                                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                }

                // 불량정보 그리드 Key Cell의 DefaultCellValue 설정
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Columns["ReqNo"].DefaultCellValue = e.Row.Cells["ReqNo"].Value;
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Columns["ReqSeq"].DefaultCellValue = e.Row.Cells["ReqSeq"].Value;
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Columns["ReqLotSeq"].DefaultCellValue = e.Row.Cells["ReqLotSeq"].Value;
                this.uGridProcInspectFault.DisplayLayout.Bands[0].Columns["ReqItemSeq"].DefaultCellValue = e.Row.Cells["ReqItemSeq"].Value;

                int intRowIndex = e.Row.Index;

                //if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled) //2012-04-23
                //    return;

                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled) //2012-04-23
                {
                    // 불량정보 조회 Method
                    SearchDFault_Process(intRowIndex);
                    this.uGridProcInspect.Rows[intRowIndex].Activated = true;
                    //현재선택된 LotNo를 임시저장한다.
                    this.uGridProcInspectFault.Tag = e.Row.Cells["LotNo"].Value;
                    this.uGridProcInspectFault.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    return;
                }

                // 불량 그리드에 데이터가 있으면 저장 메소드 호출
                if (this.uGridProcInspectFault.Rows.Count > 0)
                {
                    string strRtnStdNumber = string.Empty;
                    string strErrRtn = string.Empty;
                    TransErrRtn ErrRtn = new TransErrRtn();

                    // 관리번호가 없으면 헤더부터 저장
                    if (this.uTextStdNumber.Text.Equals(string.Empty))
                    {
                        DataTable dtHeader = Rtn_dtHeader_Proc();
                        DataTable dtDetail = Rtn_dtDetail_Proc();
                        DataTable dtDFault = Rtn_dtDetailFault_Proc();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                        QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                        brwChannel.mfCredentials(clsHeader);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장 Method 호출
                        strErrRtn = clsHeader.mfSaveINSMaterialAbnormalH_Click(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail, dtDFault);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        //m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    else
                    {
                        // 상세부터 저장
                        DataTable dtDetail = Rtn_dtDetail_Proc();
                        DataTable dtDFault = Rtn_dtDetailFault_Proc();

                        // 상세부터 저장
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                        QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                        brwChannel.mfCredentials(clsDetail);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // Method 호출
                        strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD_Proc(dtDetail, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDFault);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        //m_ProgressPopup.mfCloseProgressPopup(this);
                    }

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum == 0)
                    {
                        // OUTPUT 관리번호 저장
                        strRtnStdNumber = ErrRtn.mfGetReturnValue(0);

                        // 관리번호가 공백일때 헤더조회 Method 호출
                        if (this.uTextStdNumber.Text == "")
                            SearchHeaderDetail(strRtnStdNumber);

                        // 상세
                        SearchDetail_Proc(strRtnStdNumber);
                    }
                    else
                    {

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                    }
                }

                // 불량정보 조회 Method
                SearchDFault_Process(intRowIndex);
                this.uGridProcInspect.Rows[intRowIndex].Activated = true;
                //현재선택된 LotNo를 임시저장한다.
                this.uGridProcInspectFault.Tag = e.Row.Cells["LotNo"].Value;
                this.uGridProcInspectFault.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                #region 기존소스 주석처리
                /*
                // 그리드의 불량데이터 저장
                DataTable dtFault = RtndtFault(false);
                // 상세정보 저장
                DataTable dtDetail = RtndtDetail(this.uGridProcInspect);

                // 전역변수에 현재 그리드 행의 의뢰번호,의뢰순번, 의뢰Lot순번, 검사항목순번 저장
                //ReqNo = e.Row.Cells["ReqNo"].Value.ToString();
                //ReqSeq = e.Row.Cells["ReqSeq"].Value.ToString();
                //ReqLotSeq = Convert.ToInt32(e.Row.Cells["ReqLotSeq"].Value);
                //ReqItemSeq = Convert.ToInt32(e.Row.Cells["ReqItemSeq"].Value);

                int intRowIndex = e.Row.Index;
                // Lot 넘버가 같을시 불량유형그리드 입력
                // 불량유형 그리드에 데이터가 있으면 저장
                if (this.uGridProcInspectFault.Rows.Count > 0)
                {
                    string strErrRtn = string.Empty;
                    TransErrRtn ErrRtn = new TransErrRtn();
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 관리번호가 없으면 헤더부터 저장
                    if (this.uTextStdNumber.Text == "")
                    {
                        // 헤더정보 Datatable 저장 Method
                        DataTable dtHeader = Rtn_dtHeader_Proc();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                        QRPINS.BL.INSIMP.MaterialAbnormalH clsHeader = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                        brwChannel.mfCredentials(clsHeader);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 저장 Method 호출
                        strErrRtn = clsHeader.mfSaveINSMaterialAbnormalH_Click(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail, dtFault);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    else
                    {
                        // 상세부터 저장
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalD), "MaterialAbnormalD");
                        QRPINS.BL.INSIMP.MaterialAbnormalD clsDetail = new QRPINS.BL.INSIMP.MaterialAbnormalD();
                        brwChannel.mfCredentials(clsDetail);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // Method 호출
                        strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD(dtDetail, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtFault);

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        //m_ProgressPopup.mfCloseProgressPopup(this);
                    }

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum == 0)
                    {
                        // OUTPUT 관리번호 저장
                        string strRtnStdNumber = ErrRtn.mfGetReturnValue(0);

                        //Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        //                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                        //                            Infragistics.Win.HAlign.Right);

                        // 관리번호가 공백일때 헤더조회 Method 호출
                        if (this.uTextStdNumber.Text == "")
                            SearchHeaderDetail(strRtnStdNumber);

                        // 상세
                        //SearchDetail(strRtnStdNumber, this.uGridProcInspect);
                    }
                    else
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                        Infragistics.Win.HAlign.Right);
                    }
                }

                // 전역변수에 현재 그리드 행의 의뢰번호,의뢰순번, 의뢰Lot순번, 검사항목순번 저장
                ReqNo = this.uGridProcInspect.Rows[intRowIndex].Cells["ReqNo"].Value.ToString();
                ReqSeq = this.uGridProcInspect.Rows[intRowIndex].Cells["ReqSeq"].Value.ToString();
                ReqLotSeq = Convert.ToInt32(this.uGridProcInspect.Rows[intRowIndex].Cells["ReqLotSeq"].Value);
                if (this.uGridProcInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value != DBNull.Value)
                    ReqItemSeq = Convert.ToInt32(this.uGridProcInspect.Rows[intRowIndex].Cells["ReqItemSeq"].Value);
                else
                    ReqItemSeq = 0;

                SearchDFault_Process(intRowIndex);

                this.uGridProcInspect.Rows[intRowIndex].Activated = true;
                 *
                 */
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공정이상 불량데이터 그리드 셀업데이트 이벤트
        private void uGridProcInspectFault_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("FaultQty") || e.Cell.Column.Key.Equals("InspectQty"))
                {
                    if (Convert.ToDouble(e.Cell.Row.Cells["FaultQty"].Value) > Convert.ToDouble(e.Cell.Row.Cells["InspectQty"].Value))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M000962", "M000374", "M000612", Infragistics.Win.HAlign.Center);

                        this.uGridProcInspectFault.ActiveCell = e.Cell.Row.Cells["FaultQty"];
                        this.uGridProcInspectFault.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                        e.Cell.Row.Cells["FaultQty"].Value = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINS0017_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextAttachFile1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (PlantCode == null || PlantCode.Equals(string.Empty))
                    return;

                if (e.Button.Key.Equals("Down"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);



                    //// 파일서버에서 불러올수 있는 파일인지 체크   (변경됨)
                    //if (!this.uTextShortFile.Text.Contains(this.uTextStdNumber.Text))
                    //{
                    //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                             "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                    //                             Infragistics.Win.HAlign.Right);
                    //    return;
                    //}

                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {

                        string strSaveFolder = "";

                        if (!this.uTextShortFile.Text.Equals(string.Empty)
                            && this.uTextShortFile.Tag != null
                            && this.uTextShortFile.Tag.ToString().Equals("System.Byte[]"))
                        {
                            strSaveFolder = saveFolder.SelectedPath + "\\";

                            string strFileName = this.uTextShortFile.Text;
                            byte[] btFileByte = (byte[])this.uTextShortFile.Tag;

                            System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                            fs.Write(btFileByte, 0, btFileByte.Length);
                            fs.Close();

                            // 저장폴더 열기
                            System.Diagnostics.Process p = new System.Diagnostics.Process();
                            p.StartInfo.FileName = strSaveFolder;
                            p.Start();
                        }

                    }
                    /*
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0008");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextShortFile.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextShortFile.Text);
                        
                }
            }
            //else if (e.Button.Key.Equals("Up"))
            //{
            //    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
            //    openFile.Filter = "All files (*.*)|*.*";
            //    openFile.FilterIndex = 1;
            //    openFile.RestoreDirectory = true;

            //    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //    {
            //        string strIFileName = openFile.FileName;
            //        this.uTextShortFile.Text = strIFileName;
            //    }
            //}
            */
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextAttachFile2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (PlantCode == null || PlantCode.Equals(string.Empty))
                    return;

                if (e.Button.Key.Equals("Down"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //// 파일서버에서 불러올수 있는 파일인지 체크
                    //if (!this.uTextLongFile.Text.Contains(this.uTextStdNumber.Text))
                    //{
                    //    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                             "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                    //                             Infragistics.Win.HAlign.Right);
                    //    return;
                    //}
                    
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {

                        string strSaveFolder = "";

                        if (!this.uTextLongFile.Text.Equals(string.Empty) 
                            && this.uTextLongFile.Tag != null
                            && this.uTextLongFile.Tag.ToString().Equals("System.Byte[]"))
                        {
                            strSaveFolder = saveFolder.SelectedPath + "\\";

                            string strFileName = this.uTextLongFile.Text;
                            byte[] btFileByte = (byte[])this.uTextLongFile.Tag;

                            System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                            fs.Write(btFileByte, 0, btFileByte.Length);
                            fs.Close();

                            // 저장폴더 열기
                            System.Diagnostics.Process p = new System.Diagnostics.Process();
                            p.StartInfo.FileName = strSaveFolder;
                            p.Start();
                        }
                          
                        
                        //else if (e.Button.Key.Equals("Up"))
                        //{
                        //    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                        //    openFile.Filter = "All files (*.*)|*.*";
                        //    openFile.FilterIndex = 1;
                        //    openFile.RestoreDirectory = true;

                        //    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        //    {
                        //        string strIFileName = openFile.FileName;
                        //        this.uTextLongFile.Text = strIFileName;
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridMatInspectFault_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.uGridMatInspectFault.DisplayLayout.Override.AllowAddNew == Infragistics.Win.UltraWinGrid.AllowAddNew.No)
                    return;
                if (e.Cell.Column.Key == "FilePath")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        //e.Cell.Value = openFile.FileName;
                        string strFilePath = openFile.FileName;

                        if (strFilePath.Contains(":\\"))
                        {
                            System.IO.FileInfo fileDoc = new System.IO.FileInfo(strFilePath);

                            e.Cell.Value = fileDoc.Name;
                            e.Cell.Row.Cells["FileData"].Value = fun_ConvertFileToBinray(strFilePath);

                            QRPGlobal grdImg = new QRPGlobal();
                            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                            this.uGridMatInspectFault.DisplayLayout.Bands[0].AddNew();
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridProcInspectFault_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.uGridProcInspectFault.DisplayLayout.Override.AllowAddNew == Infragistics.Win.UltraWinGrid.AllowAddNew.No)
                    return;
                if (e.Cell.Column.Key == "FilePath")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        //e.Cell.Value = openFile.FileName;
                        string strFilePath = openFile.FileName;

                        if (strFilePath.Contains(":\\"))
                        {
                            System.IO.FileInfo fileDoc = new System.IO.FileInfo(strFilePath);

                            e.Cell.Value = fileDoc.Name;
                            e.Cell.Row.Cells["FileData"].Value = fun_ConvertFileToBinray(strFilePath);

                            QRPGlobal grdImg = new QRPGlobal();
                            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                            this.uGridProcInspectFault.DisplayLayout.Bands[0].AddNew();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridMatInspectFault_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    if ((!e.Cell.Value.ToString().Equals(string.Empty) && e.Cell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                         || !e.Cell.Value.ToString().Contains(":\\") && !string.IsNullOrEmpty(e.Cell.Value.ToString()) && e.Cell.Value != DBNull.Value)
                    {
                        //if (e.Cell.Value.ToString().Contains(":\\") || string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value == DBNull.Value)
                        //{
                        //    ////DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        //    ////                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //    ////                         "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                        //    ////                         Infragistics.Win.HAlign.Right);
                        //    return;
                        //}

                        //if (e.Cell.Value.ToString().Equals(string.Empty) || !e.Cell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                        //    return;

                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {

                            string strSaveFolder = "";


                            if (!e.Cell.Value.ToString().Equals(string.Empty) &&
                                e.Cell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                            {
                                strSaveFolder = saveFolder.SelectedPath + "\\";

                                string strFileName = e.Cell.Value.ToString();
                                byte[] btFileByte = (byte[])e.Cell.Row.Cells["FileData"].Value;

                                System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                                fs.Write(btFileByte, 0, btFileByte.Length);
                                fs.Close();

                                // 저장폴더 열기
                                System.Diagnostics.Process p = new System.Diagnostics.Process();
                                p.StartInfo.FileName = strSaveFolder;
                                p.Start();
                            }
                            else if (!e.Cell.Value.ToString().Contains(":\\") && !string.IsNullOrEmpty(e.Cell.Value.ToString()) && e.Cell.Value != DBNull.Value)
                            {

                                strSaveFolder = saveFolder.SelectedPath + "\\";
                                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                                // 화일서버 연결정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                brwChannel.mfCredentials(clsSysAccess);
                                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                                // 첨부파일 저장경로정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                brwChannel.mfCredentials(clsSysFilePath);
                                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0008");

                                // 첨부파일 Download
                                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                ArrayList arrFile = new ArrayList();

                                arrFile.Add(e.Cell.Value.ToString());

                                // Download정보 설정
                                fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.ShowDialog();

                                // 파일 실행시키기
                                System.Diagnostics.Process.Start(strSaveFolder + "\\" + e.Cell.Value.ToString());

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridProcInspectFault_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    if ((!e.Cell.Value.ToString().Equals(string.Empty) && e.Cell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                         || !e.Cell.Value.ToString().Contains(":\\") && !string.IsNullOrEmpty(e.Cell.Value.ToString()) && e.Cell.Value != DBNull.Value)
                    {
                        //if (e.Cell.Value.ToString().Contains(":\\") || string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value == DBNull.Value)
                        //{
                        //    ////DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        //    ////                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //    ////                         "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                        //    ////                         Infragistics.Win.HAlign.Right);
                        //    return;
                        //}

                        //if (e.Cell.Value.ToString().Equals(string.Empty) || !e.Cell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                        //    return;

                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {

                            string strSaveFolder = "";


                            if (!e.Cell.Value.ToString().Equals(string.Empty) &&
                                e.Cell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                            {
                                strSaveFolder = saveFolder.SelectedPath + "\\";

                                string strFileName = e.Cell.Value.ToString();
                                byte[] btFileByte = (byte[])e.Cell.Row.Cells["FileData"].Value;

                                System.IO.FileStream fs = new System.IO.FileStream(strSaveFolder + strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                                fs.Write(btFileByte, 0, btFileByte.Length);
                                fs.Close();

                                // 저장폴더 열기
                                System.Diagnostics.Process p = new System.Diagnostics.Process();
                                p.StartInfo.FileName = strSaveFolder;
                                p.Start();
                            }
                            else if (!e.Cell.Value.ToString().Contains(":\\") && !string.IsNullOrEmpty(e.Cell.Value.ToString()) && e.Cell.Value != DBNull.Value)
                            {

                                strSaveFolder = saveFolder.SelectedPath + "\\";
                                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                                // 화일서버 연결정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                brwChannel.mfCredentials(clsSysAccess);
                                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(m_strPlantCode, "S02");

                                // 첨부파일 저장경로정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                brwChannel.mfCredentials(clsSysFilePath);
                                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(m_strPlantCode, "D0008");

                                // 첨부파일 Download
                                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                ArrayList arrFile = new ArrayList();

                                arrFile.Add(e.Cell.Value.ToString());

                                // Download정보 설정
                                fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.ShowDialog();

                                // 파일 실행시키기
                                System.Diagnostics.Process.Start(strSaveFolder + "\\" + e.Cell.Value.ToString());

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridMatInspectFault_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridMatInspectFault.ActiveCell == null)
                    return;
                if (this.uGridMatInspectFault.ActiveCell.Column.Key.Equals("FilePath"))
                {
                    if (this.uCheckCompleteFlag.Enabled)
                    {
                        if (this.uGridMatInspectFault.ActiveCell.Value.ToString().Contains(PlantCode) &&
                            this.uGridMatInspectFault.ActiveCell.Value.ToString().Contains(this.uTextStdNumber.Text) &&
                            this.uGridMatInspectFault.ActiveCell.Value.ToString().Contains(this.uGridMatInspectFault.ActiveCell.Row.Cells["ReqNo"].Value.ToString()) &&
                            this.uGridMatInspectFault.ActiveCell.Value.ToString().Contains(this.uGridMatInspectFault.ActiveCell.Row.Cells["ReqSeq"].Value.ToString()) &&
                            this.uGridMatInspectFault.ActiveCell.Value.ToString().Contains(this.uGridMatInspectFault.ActiveCell.Row.Cells["ReqLotSeq"].Value.ToString()) &&
                            this.uGridMatInspectFault.ActiveCell.Value.ToString().Contains(this.uGridMatInspectFault.ActiveCell.Row.Cells["ReqItemSeq"].Value.ToString()))
                        {
                            if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                            {
                                //FileData인경우
                                if (!this.uGridMatInspectFault.ActiveCell.Value.ToString().Equals(string.Empty) &&
                                this.uGridMatInspectFault.ActiveCell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                                {
                                    this.uGridMatInspectFault.ActiveCell.Row.Cells["FileData"].Value = string.Empty;
                                    this.uGridMatInspectFault.ActiveCell.Row.Cells["FilePath"].Value = string.Empty;
                                }
                                //일반 업로드인 경우
                                else if (!this.uGridMatInspectFault.ActiveCell.Value.ToString().Contains(":\\")
                                    && !string.IsNullOrEmpty(this.uGridMatInspectFault.ActiveCell.Value.ToString())
                                    && this.uGridMatInspectFault.ActiveCell.Value != DBNull.Value)
                                {

                                    //화일서버 연결정보 가져오기
                                    QRPBrowser brwChannel = new QRPBrowser();
                                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                    brwChannel.mfCredentials(clsSysAccess);
                                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                                    //첨부파일 저장경로정보 가져오기
                                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                    brwChannel.mfCredentials(clsSysFilePath);
                                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0008");

                                    //첨부파일 Download하기
                                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                                    //arrFile.Add(dtFilePath.Rows[0]["ServerPath"].ToString() + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridLot.ActiveCell.Value.ToString());
                                    arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridMatInspectFault.ActiveCell.Value.ToString());

                                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                        , arrFile
                                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                    fileAtt.mfFileUploadNoProgView();

                                    this.uGridMatInspectFault.ActiveCell.Value = string.Empty;
                                    this.uGridMatInspectFault.ActiveCell.SetValue(string.Empty, false);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridProcInspectFault_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridProcInspectFault.ActiveCell == null)
                    return;
                if (this.uGridProcInspectFault.ActiveCell.Column.Key.Equals("FilePath"))
                {
                    if (this.uCheckCompleteFlag.Enabled)
                    {
                        if (this.uGridProcInspectFault.ActiveCell.Value.ToString().Contains(PlantCode) &&
                            this.uGridProcInspectFault.ActiveCell.Value.ToString().Contains(this.uTextStdNumber.Text) &&
                            this.uGridProcInspectFault.ActiveCell.Value.ToString().Contains(this.uGridProcInspectFault.ActiveCell.Row.Cells["ReqNo"].Value.ToString()) &&
                            this.uGridProcInspectFault.ActiveCell.Value.ToString().Contains(this.uGridProcInspectFault.ActiveCell.Row.Cells["ReqSeq"].Value.ToString()) &&
                            this.uGridProcInspectFault.ActiveCell.Value.ToString().Contains(this.uGridProcInspectFault.ActiveCell.Row.Cells["ReqLotSeq"].Value.ToString()) &&
                            this.uGridProcInspectFault.ActiveCell.Value.ToString().Contains(this.uGridProcInspectFault.ActiveCell.Row.Cells["ReqItemSeq"].Value.ToString()))
                        {
                            if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                            {
                                //FileData인경우
                                if (!this.uGridProcInspectFault.ActiveCell.Value.ToString().Equals(string.Empty) &&
                                this.uGridProcInspectFault.ActiveCell.Row.Cells["FileData"].Value.ToString().Equals("System.Byte[]"))
                                {
                                    this.uGridProcInspectFault.ActiveCell.Row.Cells["FileData"].Value = string.Empty;
                                    this.uGridProcInspectFault.ActiveCell.Row.Cells["FilePath"].Value = string.Empty;
                                }
                                //일반 업로드인 경우
                                else if (!this.uGridProcInspectFault.ActiveCell.Value.ToString().Contains(":\\")
                                    && !string.IsNullOrEmpty(this.uGridProcInspectFault.ActiveCell.Value.ToString())
                                    && this.uGridProcInspectFault.ActiveCell.Value != DBNull.Value)
                                {

                                    //화일서버 연결정보 가져오기
                                    QRPBrowser brwChannel = new QRPBrowser();
                                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                    brwChannel.mfCredentials(clsSysAccess);
                                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                                    //첨부파일 저장경로정보 가져오기
                                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                    brwChannel.mfCredentials(clsSysFilePath);
                                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0008");

                                    //첨부파일 Download하기
                                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                                    //arrFile.Add(dtFilePath.Rows[0]["ServerPath"].ToString() + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridLot.ActiveCell.Value.ToString());
                                    arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridProcInspectFault.ActiveCell.Value.ToString());

                                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                        , arrFile
                                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                    fileAtt.mfFileUploadNoProgView();

                                    this.uGridProcInspectFault.ActiveCell.Value = string.Empty;
                                    this.uGridProcInspectFault.ActiveCell.SetValue(string.Empty, false);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        /// <summary>
        /// 거래처 팝업창
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //작성완료된 정보가 아니고 공정검사정보일 경우 사용가능
                if(this.uCheckCompleteFlag.Enabled && !this.uCheckCompleteFlag.Checked
                    && this.ultraTabControl1.SelectedTab.Index == 1
                    && !this.uTextLotNo.Text.Equals(string.Empty))
                {
                    frmPOP0004 frmVen = new frmPOP0004();
                    frmVen.ShowDialog();

                    //선택사항이 없다면 리턴
                    if (frmVen.CustomerCode == null || frmVen.CustomerCode.Equals(string.Empty))
                        return;

                    this.uTextVendorCode.Text = frmVen.CustomerCode;
                    this.uTextVendorName.Text =  frmVen.CustomerName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #region Active Report

        /// <summary>
        /// 자재이력 정보 : 전체입고 MoLot수량 AND List, Ship전체 수량 
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strReqNo"></param>
        /// <param name="strReqSeq"></param>
        /// <returns></returns>
        private DataTable SearchReport_LotInfo(string strPlantCode, string strReqNo, string strReqSeq)
        {
            DataTable dtLot = new DataTable();

            try
            {
                // LotTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                QRPINS.BL.INSIMP.MatInspectReqLot clsLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                dtLot = clsLot.mfReadINSMatInspectReqLot(strPlantCode, strReqNo, strReqSeq);

                clsLot.Dispose();

                return dtLot;
            }
            catch (Exception ex)
            {
                return dtLot;
                throw (ex);
            }
            finally
            {
                dtLot.Dispose();
            }
        }

        /// <summary>
        /// 자재정보 조회 (자재이력 정보 : 도면_NO[DRAWING_NO])
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <returns></returns>
        private DataTable SearchReport_Material(string strPlantCode, string strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetailInfo(strPlantCode, strMaterialCode);

                clsMaterial.Dispose();

                return dtMaterial;
            }
            catch (Exception ex)
            {
                return dtMaterial;
                throw (ex);
            }
            finally
            {
                dtMaterial.Dispose();
            }
        }

        /// <summary>
        /// ActiveReport 헤더 부분
        /// </summary>
        /// <returns></returns>
        private DataTable SearchReport_Header()
        {
            DataTable dtHeader = new DataTable();
            try
            {
                dtHeader.Columns.Add("StdNumber", typeof(string));
                dtHeader.Columns.Add("WriteDate", typeof(string));
                dtHeader.Columns.Add("WriteUserID", typeof(string));

                DataRow drRow = dtHeader.NewRow();

                drRow["StdNumber"] = this.uTextStdNumber.Text;
                drRow["WriteDate"] = this.uDateWriteDate.DateTime.Date.ToString("yyyy-MM-dd");
                drRow["WriteUserID"] = this.uTextWriteName.Text;

                dtHeader.Rows.Add(drRow);

                return dtHeader;
            }
            catch (Exception ex)
            {
                return dtHeader;
                throw (ex);
            }
            finally
            {
                dtHeader.Dispose();
            }
        }

        /// <summary>
        /// Active Report 자재이력 정보(화면 & DB조회)
        /// </summary>
        /// <returns></returns>
        private DataTable SearchReport_History()
        {
            DataTable dtHistory = new DataTable();
            try
            {
                #region 컬럼설정

                dtHistory.Columns.Add("StdNumber", typeof(string));
                dtHistory.Columns.Add("MaterialName", typeof(string));
                dtHistory.Columns.Add("Spec", typeof(string));
                dtHistory.Columns.Add("VendorName", typeof(string));
                dtHistory.Columns.Add("GRDate", typeof(string));
                dtHistory.Columns.Add("LotEA", typeof(string));
                dtHistory.Columns.Add("TotalQty", typeof(string));
                dtHistory.Columns.Add("MoLot", typeof(string));
                dtHistory.Columns.Add("AriseDate", typeof(string));


                #endregion

                DataTable dtLotInfo = SearchReport_LotInfo(PlantCode, ReqNo, ReqSeq);

                //MoLot수량
                int intLot = dtLotInfo.DefaultView.ToTable(true, "MoLotNo").Rows.Count;
                //Ship전체수량
                double doTotalQty = 0;
                //MoLot 정보
                string strMoLot = "";

                for (int i = 0; i < dtLotInfo.DefaultView.ToTable(true, "MoLotNo").Rows.Count; i++)
                {
                    if (strMoLot.Equals(string.Empty))
                        strMoLot = dtLotInfo.DefaultView.ToTable(true, "MoLotNo").Rows[i]["MoLotNo"].ToString();
                    else
                        strMoLot = strMoLot + ", " + dtLotInfo.DefaultView.ToTable(true, "MoLotNo").Rows[i]["MoLotNo"].ToString();

                }

                for (int i = 0; i < dtLotInfo.Rows.Count; i++)
                {
                    doTotalQty += Convert.ToDouble(dtLotInfo.Rows[i]["LotSize"]);
                }

                DataTable dtMaterial = SearchReport_Material(PlantCode, this.uTextMaterialCode.Text);
                //도면정보
                string strDRAWING_NO = "";
                if (dtMaterial.Rows.Count > 0)
                {
                    strDRAWING_NO = dtMaterial.Rows[0]["DRAWING_NO"].ToString();
                }


                DataRow drRow = dtHistory.NewRow();

                drRow["StdNumber"] = this.uTextStdNumber.Text;
                drRow["MaterialName"] = this.uTextMaterialName.Text;
                drRow["Spec"] = strDRAWING_NO;
                drRow["VendorName"] = this.uTextVendorName.Text;
                drRow["GRDate"] = this.uTextGRDate.Text;
                drRow["LotEA"] = intLot;
                drRow["TotalQty"] = doTotalQty;
                drRow["MoLot"] = strMoLot;
                drRow["AriseDate"] = this.uDateAriseDate.Value == null ? "" : this.uDateAriseDate.DateTime.Date.ToString("yyyy-MM-dd");

                dtHistory.Rows.Add(drRow);

                return dtHistory;
            }
            catch (Exception ex)
            {
                return dtHistory;
                throw (ex);
            }
            finally
            {
                dtHistory.Dispose();
            }
        }

        /// <summary>
        /// Active Report 이상내용 정보 (DB 조회)
        /// </summary>
        /// <returns></returns>
        private DataTable SearchReport_ContentsState(string strLang)
        {
            DataTable dtContents = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialAbnormalH), "MaterialAbnormalH");
                QRPINS.BL.INSIMP.MaterialAbnormalH clsMaterialAbnormalH = new QRPINS.BL.INSIMP.MaterialAbnormalH();
                brwChannel.mfCredentials(clsMaterialAbnormalH);

                //dtContents = clsMaterialAbnormalH.mfReadINSMaterialAbnormal_Report_Contents(PlantCode, this.uTextStdNumber.Text, this.uTextLotNo.Text, strLang);

                clsMaterialAbnormalH.Dispose();

                return dtContents;
            }
            catch (Exception ex)
            {
                return dtContents;
                throw (ex);
            }
            finally
            {
                dtContents.Dispose();
            }
        }


        #endregion

        /// <summary>
        /// EMail TEST
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraButton1_Click(object sender, EventArgs e)
        {
            if (this.uTextStdNumber.Text.Equals(string.Empty))
                return;
            WinMessageBox msg = new WinMessageBox();

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001449", "M001448",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                return;

            SendMail();
        }

        private void uTextShortFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextShortFile.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0008");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextShortFile.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextShortFile.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextLongFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextLongFile.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0008");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextLongFile.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextLongFile.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
