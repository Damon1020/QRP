﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINSZ0002.cs                                        */
/* 프로그램명   : 신제품 인증등록                                       */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-19 : 기능 추가 (이종호)                       */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.IO;
using System.Collections;

namespace QRPINS.UI
{
    public partial class frmINSZ0002 : Form, IToolbar
    {
        #region 전역변수
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        // 신규자재 인증의뢰에서 넘겨받을 변수
        private string m_strPlantCode;
        private string m_strStdNumber;
        private string m_strMoveFormName;

        public string MoveFormName
        {
            get { return m_strMoveFormName; }
            set { m_strMoveFormName = value; }
        }

        public string PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public string StdNumber
        {
            get { return m_strStdNumber; }
            set { m_strStdNumber = value; }
        }

        private string m_strVendorCode;
        #endregion

        public frmINSZ0002()
        {
            InitializeComponent();
        }

        private void frmINSZ0002_Activated(object sender, EventArgs e)
        {
            // 툴바 활성화 설정
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0002_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("신규자재 인증등록", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitGroupBox();
            InitTab();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitText();

            this.uOptionS3IngFlag.Enabled = false;
            this.uOptionS4IngFlag.Enabled = false;

            //이메일보내기 테스트 버튼
            //if (m_SysRes.GetString("SYS_USERID") != "TESTUSER")
            //    uButtonEMail.Visible = false;
            //else
            //    uButtonEMail.Visible = true;
            // Active Report Test
            if (m_SysRes.GetString("SYS_USERID") != "TESTUSER")
                ultraButton1.Visible = false;
            else
                ultraButton1.Visible = true;

            if (m_strMoveFormName == "frmINSZ0001")
            {
                QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
                brw.mfSetFormLanguage(this);

                CommonControl cControl = new CommonControl();
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                // Tab 명 변경
                if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINSZ0002"))
                {
                    uTabMenu.Tabs["QRPINS" + "," + "frmINSZ0002"].Text = titleArea.TextName;
                }

                titleArea.TextName = titleArea.TextName + "(" + uTabMenu.Tabs["QRPINS,frmINSZ0001"].Text + ")";

                // 조회 Method 호출
                Search_Detail(PlantCode, StdNumber);
                Search_FileList(PlantCode, StdNumber);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            else
            {
                this.uGroupBoxContentsArea.Expanded = false;
            }

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.ultraGroupBox1, GroupBoxType.INFO, "Step 진행여부", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.ultraGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.ultraGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                wGroupBox.mfSetGroupBox(this.uGroupBoxInfo, GroupBoxType.INFO, "입고정보", m_resSys.GetString("SYS_FONTNAME")
                                        , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                                        , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                                        , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxInput, GroupBoxType.INFO, "입력사항", m_resSys.GetString("SYS_FONTNAME")
                                        , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                                        , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                                        , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxInput.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxInput.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStorageNum, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRequestDate, "의뢰일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPassFailFlag, "판정결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProgStatus, "진행상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReqUser, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelSearchConsumableType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStdNumber, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStorageDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqUser, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRequestPurpose, "의뢰목적", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAmount, "수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMoldSeq, "금형차수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApplyDevice, "적용Device", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDrawingNo, "도면No", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMSDS, "MSDS", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelICP, "ICP", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnit, "단위", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelReceiptUser, "접수자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReceiptDate, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelJudge1, "합부판정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRegisterUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRegisterDate, "등록일자", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelProgressCheck1, "Step1.품질감사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS1User, "담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelJudge2, "감사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS1WrateDate, "감사일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc1, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile1, "첨부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelProgressCheck2, "Step2.수입검사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS2User, "담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelJudge3, "수입검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS2WrateDate, "평가완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc2, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile2, "첨부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelProgressCheck3, "Step3.공정평가", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS3User, "담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelJudge4, "공정검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS3WrateDate, "평가완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc3, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile3, "첨부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelProgressCheck4, "Step4.신뢰성평가", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS4User, "담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelJudge5, "신뢰성평가결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelS4WrateDate, "평가완료일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc4, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAttachFile4, "첨부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelReturnReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReturnUser, "반려자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReturnDate, "반려일", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelQualMove, "QualNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReliabilityMove, "신뢰성검사 No", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonReturn, "반려", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtonMove, "수입검사이동", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonAVLMove, "AVL 이동", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonQualMove, "이동", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonReliabilityMove, "이동", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None, m_resSys.GetString("SYS_FONTNAME"));

                this.uTab.Tabs["Step1"].Visible = false;
                this.uTab.Tabs["Step2"].Visible = false;
                this.uTab.Tabs["Step3"].Visible = false;
                this.uTab.Tabs["Step4"].Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchPlant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // 판정결과
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dt = clsComCode.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPassFailFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dt);

                // 진행상태
                dt = clsComCode.mfReadCommonCode("C0012", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProgStatus, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dt);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsum = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsConsum);

                DataTable dtConsum = clsConsum.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchConsumableType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ConsumableTypeCode", "ConsumableTypeName", dtConsum);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridHeader, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "StdNumber", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MoldSeq", "금형차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 4
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "GRDate", "입고일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ShipmentQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "FloorPlanNo", "도면번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "SpecNo", "Rev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ReqUserName", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ReqDate", "의뢰일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "ProgStatus", "진행상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region LotList

                // LotNo Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridDetail, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 300, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "GRQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDetail, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Return List

                // 반려그리드
                wGrid.mfInitGeneralGrid(this.uGridReturnList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "ReturnUserName", "반려자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "ReturnDate", "반려일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "ReturnReason", "반려사유", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridReturnList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridReturnList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Attach File List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridFile, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridFile, 0, "UniqueKey", "Key", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "CompleteDate", "만료일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", null);

                // Set FontSize
                this.uGridFile.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridFile.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드 편집 불가 상태로
                this.uGridFile.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 검색조건 의뢰일자 = 오늘날짜
                this.uDateSearchReqFromDate.Value = DateTime.Now.AddDays(-7);
                this.uDateSearchReqToDate.Value = DateTime.Now;

                // 의뢰일 기본값 = 오늘날짜
                this.uDateReceiptDate.Value = DateTime.Now;

                // MaxLength 지정
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextSearchStdNumber.MaxLength = 20;
                this.uTextSearchMaterialCode.MaxLength = 20;

                this.uTextReturnReason.MaxLength = 500;
                this.uTextReturnUserID.MaxLength = 20;
                this.uTextReceiptID.MaxLength = 20;

                this.uTextS1UserID.MaxLength = 20;
                this.uTextS1FileName.MaxLength = 1000;
                this.uTextS1EtcDesc.MaxLength = 1000;

                this.uTextS2UserID.MaxLength = 20;
                this.uTextS2FileName.MaxLength = 1000;
                this.uTextS2EtcDesc.MaxLength = 1000;

                this.uTextS3UserID.MaxLength = 20;
                this.uTextS3FileName.MaxLength = 1000;
                this.uTextS3EtcDesc.MaxLength = 1000;
                this.uTextQualMove.MaxLength = 20;

                this.uTextS4UserID.MaxLength = 20;
                this.uTextS4FileName.MaxLength = 1000;
                this.uTextS4EtcDesc.MaxLength = 1000;
                this.uTextReliabilityMove.MaxLength = 20;

                this.uTextWriteID.MaxLength = 20;

                // 의뢰자 기본값 = 로그인 사용자 ID
                String strPlantCode = m_resSys.GetString("SYS_PLANTCODE");
                String strUserID = m_resSys.GetString("SYS_USERID");

                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                //QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                //brwChannel.mfCredentials(clsUser);

                //DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                this.uTextReceiptID.Text = strUserID;
                this.uTextReceiptName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uTextShipmentQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                this.uTextSearchMaterialCode.AlwaysInEditMode = true;
                this.uTextSearchReqUserID.AlwaysInEditMode = true;
                this.uTextSearchVendorCode.AlwaysInEditMode = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                brwChannel.mfCredentials(clsHeader);

                // 조회용 DataTable 컬럼 설정 Method 호출
                DataTable dtSearch = clsHeader.mfSetSearchDataInfo();
                DataRow drRow = dtSearch.NewRow();

                drRow["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                drRow["MaterialCode"] = this.uTextSearchMaterialCode.Text;
                drRow["materialName"] = this.uTextSearchMaterialName.Text;
                drRow["PassFailFlag"] = this.uComboSearchPassFailFlag.Value.ToString();
                drRow["ProgStatus"] = this.uComboSearchProgStatus.Value.ToString();
                drRow["StdNumber"] = this.uTextSearchStdNumber.Text;
                drRow["VendorCode"] = this.uTextSearchVendorCode.Text;
                drRow["VendorName"] = this.uTextSearchVendorName.Text;
                drRow["ReqFromDate"] = Convert.ToDateTime(this.uDateSearchReqFromDate.Value).ToString("yyyy-MM-dd");
                drRow["ReqToDate"] = Convert.ToDateTime(this.uDateSearchReqToDate.Value).ToString("yyyy-MM-dd");
                drRow["ConsumableType"] = this.uComboSearchConsumableType.Value.ToString();
                drRow["ReqUserID"] = this.uTextSearchReqUserID.Text;
                dtSearch.Rows.Add(drRow);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // Method 호출
                DataTable dtRtn = clsHeader.mfReadINSMaterialConfirmHRegist(dtSearch, m_resSys.GetString("SYS_LANG"));

                this.uGridHeader.DataSource = dtRtn;
                this.uGridHeader.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtn.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridHeader, 0);
                }

                // 상세정보창 접힘상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                //SC--begin--
                if (this.uOptionS1IngFlag.CheckedIndex == 0 && this.uTextS1UserID.Text == "")
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001264", "M001053", "M001565", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                    }
                    else
                    {
                        this.uTab.Tabs["Step1"].Selected = true;
                        this.uTextS1UserID.Focus();
                        return;
                    }
                }
                if (this.uOptionS3IngFlag.CheckedIndex == 0 && this.uTextS3UserID.Text == "")
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001264", "M001053", "M001566", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                    }
                    else
                    {
                        this.uTab.Tabs["Step3"].Selected = true;
                        this.uTextS3UserID.Focus();
                        return;
                    }
                }
                if (this.uOptionS4IngFlag.CheckedIndex == 0 && this.uTextS4UserID.Text == "")
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001264", "M001053", "M001567", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                    }
                    else
                    {
                        this.uTab.Tabs["Step4"].Selected = true;
                        this.uTextS4UserID.Focus();
                        return;
                    }
                }
                //SC--end--

                if (PlantCode.Equals(string.Empty) || this.uTextStdNumber.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000397", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uCheckCompleteFlag.Enabled == false)
                {
                    if (this.uCheckCompleteFlag.Checked == true)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000984", "M000986", Infragistics.Win.HAlign.Center);

                        mfSearch();
                        return;
                    }
                }
                else
                {
                    // 필수사항 확인 및 데이터 저장
                    if (this.uTextReceiptID.Text.Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001071", Infragistics.Win.HAlign.Center);

                        this.uTextReceiptID.Focus();
                        return;
                    }

                        if (this.uOptionS1IngFlag.CheckedIndex.Equals(-1))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M000133", Infragistics.Win.HAlign.Center);

                            this.uOptionS1IngFlag.FocusedIndex = 1;
                            return;
                        }
                        else if (this.uOptionS2IngFlag.CheckedIndex.Equals(-1))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M000138", Infragistics.Win.HAlign.Center);

                            this.uOptionS2IngFlag.FocusedIndex = 1;
                            return;
                        }
                        else if (this.uOptionS3IngFlag.CheckedIndex.Equals(-1))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M000141", Infragistics.Win.HAlign.Center);

                            this.uOptionS3IngFlag.FocusedIndex = 1;
                            return;
                        }
                        else if (this.uOptionS4IngFlag.CheckedIndex.Equals(-1))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000881", "M000144", Infragistics.Win.HAlign.Center);

                            this.uOptionS4IngFlag.FocusedIndex = 1;
                            return;
                        }

                    //if (!this.uOptionS1IngFlag.CheckedIndex.Equals(-1) && !this.uOptionS2IngFlag.CheckedIndex.Equals(-1) &&
                        //    !this.uOptionS3IngFlag.CheckedIndex.Equals(-1) && !this.uOptionS4IngFlag.CheckedIndex.Equals(-1))
                        //{
                        //    if (this.uOptionS1IngFlag.CheckedIndex == 0)
                        //    {
                        //        if (this.uTextS1UserID.Text.ToString().Trim().Equals(string.Empty))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "품질감사 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uTextS1UserID.Focus();
                        //            return;
                        //        }
                        //        else if (this.uOptionS1Result.CheckedIndex.Equals(-1))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "품질감사 결과를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uOptionS1Result.FocusedIndex = 1;
                        //            return;
                        //        }
                        //    }
                        //    else if (this.uOptionS2IngFlag.CheckedIndex == 0)
                        //    {
                        //        if (this.uTextS2UserID.Text.ToString().Trim().Equals(string.Empty))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "수입검사 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uTextS2UserID.Focus();
                        //            return;
                        //        }
                        //        else if (this.uOptionS2Result.CheckedIndex.Equals(-1))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "수입검사 이동 버튼을 이용해 수입검사를 진행해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uOptionS2Result.FocusedIndex = 1;
                        //            return;
                        //        }
                        //    }
                        //    else if (this.uOptionS3IngFlag.CheckedIndex == 0)
                        //    {
                        //        if (this.uTextS3UserID.Text.ToString().Trim().Equals(string.Empty))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "공정검사 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uTextS3UserID.Focus();
                        //            return;
                        //        }
                        //        else if (this.uOptionS3Result.CheckedIndex.Equals(-1))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "공정검사 결과를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uOptionS3Result.FocusedIndex = 1;
                        //            return;
                        //        }
                        //    }
                        //    else if (this.uOptionS4IngFlag.CheckedIndex == 0)
                        //    {
                        //        if (this.uTextS4UserID.Text.ToString().Trim().Equals(string.Empty))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "신뢰성평가 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uTextS4UserID.Focus();
                        //            return;
                        //        }
                        //        else if (this.uOptionS4Result.CheckedIndex.Equals(-1))
                        //        {
                        //            Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "신뢰성평가 결과를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //            this.uOptionS4Result.FocusedIndex = 1;
                        //            return;
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    if (this.uOptionS1IngFlag.CheckedIndex.Equals(-1))
                        //    {
                        //        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "품질감사 여부를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //        this.uOptionS1IngFlag.FocusedIndex = 1;
                        //        return;
                        //    }
                        //    else if (this.uOptionS2IngFlag.CheckedIndex.Equals(-1))
                        //    {
                        //        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "수입검사 여부를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //        this.uOptionS2IngFlag.FocusedIndex = 1;
                        //        return;
                        //    }
                        //    else if (this.uOptionS3IngFlag.CheckedIndex.Equals(-1))
                        //    {
                        //        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "공정평가 여부를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //        this.uOptionS3IngFlag.FocusedIndex = 1;
                        //        return;
                        //    }
                        //    else if (this.uOptionS4IngFlag.CheckedIndex.Equals(-1))
                        //    {
                        //        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                        , "확인창", "입력사항 확인", "신뢰성평가 여부를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //        this.uOptionS4IngFlag.FocusedIndex = 1;
                        //        return;
                        //    }
                        //}
                        //else if (this.uOptionS1IngFlag.CheckedIndex == 0 && this.uTextS1UserID.Text.ToString() == "")
                        //{
                        //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                , "확인창", "입력사항 확인", "품질감사 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //    this.uTextS1UserID.Focus();
                        //    return;
                        //}
                        //else if (this.uOptionS2IngFlag.CheckedIndex == 0 && this.uTextS2UserID.Text.ToString() == "")
                        //{
                        //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                , "확인창", "입력사항 확인", "수입검사 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //    this.uTextS2UserID.Focus();
                        //    return;
                        //}
                        //else if (this.uOptionS3IngFlag.CheckedIndex == 0 && this.uTextS3UserID.Text.ToString() == "")
                        //{
                        //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                , "확인창", "입력사항 확인", "공정검사 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //    this.uTextS3UserID.Focus();
                        //    return;
                        //}
                        //else if (this.uOptionS4IngFlag.CheckedIndex == 0 && this.uTextS4UserID.Text.ToString() == "")
                        //{
                        //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                , "확인창", "입력사항 확인", "신뢰성평가 담당자를 입력해 주세요", Infragistics.Win.HAlign.Center);

                    //    this.uTextS4UserID.Focus();
                        //    return;
                        //}
                        //else if (this.uTextWriteID.Text == "") // 2012-12-19 작성완료체크시 등록자 확인으로 변경.
                        //{
                        //    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                    , "M001264", "M000881", "M000386", Infragistics.Win.HAlign.Center);

                        //    this.uTextWriteID.Focus();
                        //    return;
                        //}
                        else
                        {
                            // 헤더 BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                            QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                            brwChannel.mfCredentials(clsHeader);

                            // 화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                            // 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                            // 메일전송용 첨부파일 저장경로정보 가져오기
                            DataTable dtFilePath_Mail = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0022");

                            // 저장용 DataTable 컬럼 설정
                            DataTable dtSave = clsHeader.mfSetDataInfo();

                            // 첨부파일 Upload하기
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            // 데이터 저장
                            DataRow drRow = dtSave.NewRow();
                            drRow["PlantCode"] = PlantCode;
                            drRow["StdNumber"] = this.uTextStdNumber.Text;
                            drRow["ReturnReason"] = this.uTextReturnReason.Text;
                            drRow["ReceiptID"] = this.uTextReceiptID.Text;
                            drRow["ReceiptDate"] = this.uDateReceiptDate.Value.ToString();
                            // 진행여부가 필요일때만 저장
                            if (this.uOptionS1IngFlag.CheckedIndex == 0)
                            {
                                drRow["S1IngFlag"] = this.uOptionS1IngFlag.Value.ToString();
                                drRow["S1UserID"] = this.uTextS1UserID.Text;
                                drRow["S1WriteDate"] = this.uDateS1WriteDate.Value.ToString();
                                drRow["S1EtcDesc"] = this.uTextS1EtcDesc.Text;
                                if (this.uOptionS1Result.Value != null)
                                    drRow["S1Result"] = this.uOptionS1Result.Value.ToString();
                                if (this.uTextS1FileName.Text.Contains(":\\"))
                                {
                                    //화일이름변경(공장코드+관리번호+MSDS+화일명)하여 복사하기//
                                    FileInfo fileDoc = new FileInfo(this.uTextS1FileName.Text);
                                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                           PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step1-" + fileDoc.Name;
                                    //변경한 화일이 있으면 삭제하기
                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);
                                    //변경한 화일이름으로 복사하기
                                    File.Copy(this.uTextS1FileName.Text, strUploadFile);
                                    arrFile.Add(strUploadFile);

                                    drRow["S1FileName"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step1-" + fileDoc.Name;
                                }
                                else
                                {
                                    drRow["S1FileName"] = this.uTextS1FileName.Text;
                                }
                                drRow["S1CompleteFlag"] = this.uCheckS1CompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            }
                            else if (this.uOptionS1IngFlag.CheckedIndex == 1)
                            {
                                drRow["S1IngFlag"] = this.uOptionS1IngFlag.Value.ToString();
                            }
                            if (this.uOptionS2IngFlag.CheckedIndex == 0)
                            {
                                drRow["S2IngFlag"] = this.uOptionS2IngFlag.Value.ToString();
                                drRow["S2UserID"] = this.uTextS2UserID.Text;
                                drRow["S2WriteDate"] = this.uDateS2WriteDate.Value.ToString();
                                drRow["S2EtcDesc"] = this.uTextS2EtcDesc.Text;
                                if (this.uOptionS2Result.Value != null)
                                    drRow["S2Result"] = this.uOptionS2Result.Value.ToString();
                                if (this.uTextS2FileName.Text.Contains(":\\"))
                                {
                                    //화일이름변경(공장코드+관리번호+MSDS+화일명)하여 복사하기//
                                    FileInfo fileDoc = new FileInfo(this.uTextS2FileName.Text);
                                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                           PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step2-" + fileDoc.Name;
                                    //변경한 화일이 있으면 삭제하기
                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);
                                    //변경한 화일이름으로 복사하기
                                    File.Copy(this.uTextS2FileName.Text, strUploadFile);
                                    arrFile.Add(strUploadFile);

                                    drRow["S2FileName"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step2-" + fileDoc.Name;
                                }
                                else
                                {
                                    drRow["S2FileName"] = this.uTextS2FileName.Text;
                                }
                                drRow["S2CompleteFlag"] = this.uCheckS2CompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            }
                            else if (this.uOptionS2IngFlag.CheckedIndex == 1)
                            {
                                drRow["S2IngFlag"] = this.uOptionS2IngFlag.Value.ToString();
                            }
                            if (this.uOptionS3IngFlag.CheckedIndex == 0)
                            {
                                drRow["S3IngFlag"] = this.uOptionS3IngFlag.Value.ToString();
                                drRow["S3UserID"] = this.uTextS3UserID.Text;
                                drRow["S3WriteDate"] = this.uDateS3WriteDate.Value.ToString();
                                drRow["S3EtcDesc"] = this.uTextS3EtcDesc.Text;
                                if (this.uOptionS3Result.Value != null)
                                    drRow["S3Result"] = this.uOptionS3Result.Value.ToString();
                                if (this.uTextS3FileName.Text.Contains(":\\"))
                                {
                                    //화일이름변경(공장코드+관리번호+MSDS+화일명)하여 복사하기//
                                    FileInfo fileDoc = new FileInfo(this.uTextS3FileName.Text);
                                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                           PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step3-" + fileDoc.Name;
                                    //변경한 화일이 있으면 삭제하기
                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);
                                    //변경한 화일이름으로 복사하기
                                    File.Copy(this.uTextS3FileName.Text, strUploadFile);
                                    arrFile.Add(strUploadFile);

                                    drRow["S3FileName"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step3-" + fileDoc.Name;
                                }
                                else
                                {
                                    drRow["S3FileName"] = this.uTextS3FileName.Text;
                                }
                                drRow["S3MoveCode"] = this.uTextQualMove.Text.ToUpper().Trim();
                                drRow["S3CompleteFlag"] = this.uCheckS3CompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            }
                            else if (this.uOptionS3IngFlag.CheckedIndex == 1)
                            {
                                drRow["S3IngFlag"] = this.uOptionS3IngFlag.Value.ToString();
                            }
                            if (this.uOptionS4IngFlag.CheckedIndex == 0)
                            {
                                drRow["S4IngFlag"] = this.uOptionS4IngFlag.Value.ToString();
                                drRow["S4UserID"] = this.uTextS4UserID.Text;
                                drRow["S4WriteDate"] = this.uDateS4WriteDate.Value.ToString();
                                drRow["S4EtcDesc"] = this.uTextS4EtcDesc.Text;
                                if (this.uOptionS4Result.Value != null)
                                    drRow["S4Result"] = this.uOptionS4Result.Value.ToString();
                                if (this.uTextS4FileName.Text.Contains(":\\"))
                                {
                                    //화일이름변경(공장코드+관리번호+MSDS+화일명)하여 복사하기//
                                    FileInfo fileDoc = new FileInfo(this.uTextS4FileName.Text);
                                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                           PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step4-" + fileDoc.Name;
                                    //변경한 화일이 있으면 삭제하기
                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);
                                    //변경한 화일이름으로 복사하기
                                    File.Copy(this.uTextS4FileName.Text, strUploadFile);
                                    arrFile.Add(strUploadFile);

                                    drRow["S4FileName"] = PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step4-" + fileDoc.Name;
                                }
                                else
                                {
                                    drRow["S4FileName"] = this.uTextS4FileName.Text;
                                }
                                drRow["S4MoveCode"] = this.uTextReliabilityMove.Text.ToUpper().Trim();
                                drRow["S4CompleteFlag"] = this.uCheckS4CompleteFlag.Checked.ToString().ToUpper().Substring(0, 1);
                            }
                            else if (this.uOptionS4IngFlag.CheckedIndex == 1)
                            {
                                drRow["S4IngFlag"] = this.uOptionS4IngFlag.Value.ToString();
                            }
                            if (this.uOptionPassFailFlag.CheckedIndex != -1)
                            {
                                drRow["PassFailFlag"] = this.uOptionPassFailFlag.Value.ToString();
                            }
                            if (this.uCheckCompleteFlag.Checked == true)
                            {
                                drRow["CompleteFlag"] = "T";
                                drRow["ProgStatus"] = "4";
                            }
                            else
                            {
                                drRow["CompleteFlag"] = "F";
                                drRow["ProgStatus"] = "2";
                            }
                            drRow["WriteID"] = this.uTextWriteID.Text;
                            drRow["WriteDate"] = this.uDateWriteDate.Value.ToString();

                            dtSave.Rows.Add(drRow);

                            if (dtSave.Rows.Count > 0)
                            {
                                if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true)
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000984", "M000982",
                                                    Infragistics.Win.HAlign.Right);
                                }
                                // 저장여부를 묻는다
                                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                                {
                                    // 프로그래스 팝업창 생성
                                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                    Thread t1 = m_ProgressPopup.mfStartThread();
                                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                                    this.MdiParent.Cursor = Cursors.WaitCursor;

                                    // 저장 Method 실행
                                    String strErrRtn = clsHeader.mfSaveINSMaterialConfirmHRegist(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                    // 팦업창 Close
                                    this.MdiParent.Cursor = Cursors.Default;
                                    m_ProgressPopup.mfCloseProgressPopup(this);

                                    TransErrRtn ErrRtn = new TransErrRtn();
                                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                    // 처리결과에 따른 메세지 박스
                                    if (ErrRtn.ErrNum == 0)
                                    {
                                        if (arrFile.Count > 0)
                                        {
                                            //Upload정보 설정
                                            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                            fileAtt.ShowDialog();

                                            // 메일전송용 첨부파일 업로드
                                            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                       dtFilePath_Mail.Rows[0]["ServerPath"].ToString(),
                                                                                       dtFilePath_Mail.Rows[0]["FolderName"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                            fileAtt.ShowDialog();
                                        }

                                        //////////////////////////////////////////////// 2012-09-18 /////////////////////////////////////////////// 권종구
                                        // Mailing
                                        //최초1회 메일링 시 담당자 변경되었을 경우 발송되지 않기 때문에 이전Step완료 저장 시 송부

                                        //// 메일전송 메소드 호출
                                        ////SendMail();

                                        //수입검사완료(작업완료가 체크 & 작업완료체크박스 활성화 & 담당자가 공백이 아닌경우 & 수입검사Step 필요)
                                        if (this.uCheckS2CompleteFlag.Checked && this.uCheckS2CompleteFlag.Enabled
                                            && !this.uTextS2UserID.Text.Equals(string.Empty)
                                            && this.uTab.Tabs["Step2"].Visible)
                                        {
                                            sendS2CompleteMail();

                                            //공정평가 담당자가 공백이아니고 공정평가가 필요한경우 메일
                                            if (!this.uTextS3UserID.Text.Equals(string.Empty) && this.uTab.Tabs["Step3"].Visible)
                                                sendSetS3UserMail();
                                            //신뢰성평가 담당자가 공백이아니고 신뢰성평가가 필요한경우 메일
                                            else if (!this.uTextS4UserID.Text.Equals(string.Empty) && this.uTab.Tabs["Step4"].Visible)
                                                sendSetS4UserMail();
                                        }

                                        //공정평가완료(작업완료가 체크 & 작업완료체크박스 활성화 & 담당자가 공백이 아닌경우 & 공정평가Step 필요)
                                        else if (this.uCheckS3CompleteFlag.Checked && this.uCheckS3CompleteFlag.Enabled
                                            && !this.uTextS3UserID.Text.Equals(string.Empty)
                                            && this.uTab.Tabs["Step3"].Visible)
                                        {
                                            sendS3CompleteMail();

                                            //신뢰성평가 담당자가 공백이아니고 신뢰성평가가 필요한경우 메일
                                            if (!this.uTextS4UserID.Text.Equals(string.Empty) && this.uTab.Tabs["Step4"].Visible)
                                                sendSetS4UserMail();
                                        }
                                        //신뢰성평가완료(작업완료가 체크 & 작업완료체크박스 활성화 & 담당자가 공백이 아닌경우 & 신뢰성평가Step 필요)
                                        else if (this.uCheckS4CompleteFlag.Checked && this.uCheckS4CompleteFlag.Enabled
                                            && !this.uTextS4UserID.Text.Equals(string.Empty)
                                            && this.uTab.Tabs["Step4"].Visible)
                                            sendS4CompleteMail();

                                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////


                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M001037", "M000930",
                                                            Infragistics.Win.HAlign.Right);

                                        // 수입검사 이동 버튼 클릭시 저장이 되었는지 확인
                                        this.uTextSaveCheck.Text = "T";
                                        // 리스트 갱신
                                        mfSearch();
                                    }
                                    else
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M001037", "M000953",
                                                            Infragistics.Win.HAlign.Right);
                                    }
                                }
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    // 초기화
                    Clear();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridHeader.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridHeader);
                    if (this.uGridDetail.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridDetail);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000800",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method..
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUserInfo = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUserInfo = clsUser.mfReadSYSUser("", strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUserInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUserInfo;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 거래처 정보 조회 함수
        /// </summary>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        private DataTable GetVendorInfo(String strVendorCode)
        {
            DataTable dtVendor = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

                return dtVendor;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtVendor;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uTextPlant.Text = "";
                this.uTextStdNumber.Text = "";
                this.uTextGRDate.Text = "";
                this.uTextReqID.Text = "";
                this.uTextReqName.Text = "";
                this.uTextReqDate.Text = "";
                this.uTextReqPurpose.Text = "";
                this.uTextVendor.Text = "";
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialName.Text = "";
                this.uTextSpecNo.Text = "";
                this.uTextShipmentQty.Text = "";
                this.uTextMoldSeq.Text = "";
                this.uTextApplyDevice.Text = "";
                this.uTextFloorPlanNo.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextMSDS.Text = "";
                this.uTextICP.Text = "";

                while (this.uGridDetail.Rows.Count > 0)
                {
                    this.uGridDetail.Rows[0].Delete(false);
                }
                while (this.uGridReturnList.Rows.Count > 0)
                {
                    this.uGridReturnList.Rows[0].Delete(false);
                }

                this.uOptionS1IngFlag.CheckedIndex = 1;
                this.uOptionS2IngFlag.CheckedIndex = 0;
                this.uOptionS3IngFlag.CheckedIndex = 1;
                this.uOptionS4IngFlag.CheckedIndex = 1;

                this.uOptionS1IngFlag.Enabled = true;
                this.uOptionS2IngFlag.Enabled = false;
                this.uOptionS3IngFlag.Enabled = true;
                this.uOptionS4IngFlag.Enabled = true;
                

                this.uTextReceiptID.Text = "";
                this.uTextReceiptName.Text = "";

                this.uOptionPassFailFlag.Value = null;
                this.uCheckCompleteFlag.Checked = false;
                this.uCheckCompleteFlag.Enabled = true;
                this.uTextWriteID.Text = "";
                this.uTextWriteName.Text = "";

                //this.uButtonMove.Visible = true;
                //this.uButtonMove.Enabled = true;
                this.uTextReqNo.Clear();
                this.uTextReqSeq.Clear();
                this.uTextSaveCheck.Clear();

                this.uTextReturnReason.Clear();
                this.uTextReturnUserID.Clear();
                this.uTextReturnUserName.Clear();
                this.uDateReturnDate.Value = DateTime.Now.ToString("yyyy-MM-dd");

                this.uCheckS1CompleteFlag.Enabled = true;
                this.uCheckS1CompleteFlag.Checked = false;
                this.uCheckS2CompleteFlag.Enabled = true;
                this.uCheckS2CompleteFlag.Checked = false;
                this.uCheckS3CompleteFlag.Enabled = true;
                this.uCheckS3CompleteFlag.Checked = false;
                this.uCheckS4CompleteFlag.Enabled = true;
                this.uCheckS4CompleteFlag.Checked = false;

                this.uTextS2EtcDesc.Clear();
                this.uTextS2FileName.Clear();
                this.uTextS2UserID.Clear();
                this.uTextS2UserName.Clear();
                this.uOptionS2Result.CheckedIndex = -1;
                this.uDateS2WriteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");

                this.uTextQualMove.Clear();
                this.uTextReliabilityMove.Clear();
                this.uTextVendorCode.Clear();

                this.uTextQualMove.ReadOnly = false;
                this.uTextQualMove.Appearance.BackColor = Color.White;
                this.uTextReliabilityMove.ReadOnly = false;
                this.uTextReliabilityMove.Appearance.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ContentsArea 검색 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        private void Search_Detail(string strPlantCode, string strStdNumber)
        {
            try
            {
                Clear();
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 상세 헤더
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dt = clsHeader.mfReadINSMaterialConfirmHDetail(strPlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                PlantCode = dt.Rows[0]["PlantCode"].ToString();
                this.uTextPlant.Text = dt.Rows[0]["PlantName"].ToString();
                this.uTextStdNumber.Text = dt.Rows[0]["StdNumber"].ToString();
                this.uTextGRDate.Text = dt.Rows[0]["GRDate"].ToString();
                this.uTextReqID.Text = dt.Rows[0]["ReqID"].ToString();
                this.uTextReqName.Text = dt.Rows[0]["ReqName"].ToString();
                this.uTextReqDate.Text = dt.Rows[0]["ReqDate"].ToString();
                this.uTextReqPurpose.Text = dt.Rows[0]["ReqPurpose"].ToString();
                m_strVendorCode = dt.Rows[0]["VendorCode"].ToString();
                this.uTextVendorCode.Text = dt.Rows[0]["VendorCode"].ToString();
                this.uTextVendor.Text = dt.Rows[0]["VendorName"].ToString();
                this.uTextMaterialCode.Text = dt.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dt.Rows[0]["MaterialName"].ToString();
                this.uTextSpecNo.Text = dt.Rows[0]["SpecNo"].ToString();
                this.uTextShipmentQty.Text = Math.Floor(Convert.ToDecimal(dt.Rows[0]["ShipmentQty"])).ToString();
                this.uTextMoldSeq.Text = dt.Rows[0]["MoldSeq"].ToString();
                this.uTextApplyDevice.Text = dt.Rows[0]["ApplyDevice"].ToString();
                this.uTextFloorPlanNo.Text = dt.Rows[0]["FloorPlanNo"].ToString();
                this.uTextEtcDesc.Text = dt.Rows[0]["EtcDesc"].ToString();
                this.uTextMSDS.Text = dt.Rows[0]["MSDSFileName"].ToString();
                this.uTextICP.Text = dt.Rows[0]["ICPFileName"].ToString();
                this.uTextUnitCode.Text = dt.Rows[0]["UnitName"].ToString();
                this.uTextGRNo.Text = dt.Rows[0]["GRNo"].ToString();

                // Detail 그리드
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmD), "MaterialConfirmD");
                QRPINS.BL.INSIMP.MaterialConfirmD clsDetail = new QRPINS.BL.INSIMP.MaterialConfirmD();
                brwChannel.mfCredentials(clsDetail);

                dt = clsDetail.mfReadINSMaterialConfirmD(strPlantCode, strStdNumber);

                this.uGridDetail.DataSource = dt;
                this.uGridDetail.DataBind();

                if (dt.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDetail, 0);

                    //// 수입검사 진행되었으면 수입검사 이동버튼 히든처리
                    //if (dt.Rows[0]["InspectFlag"].ToString().Equals("Y"))
                    //{
                    //    this.uButtonMove.Visible = false;
                    //    this.uButtonMove.Enabled = false;
                    //}
                }

                // 반려그리드
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmReturn), "MaterialConfirmReturn");
                QRPINS.BL.INSIMP.MaterialConfirmReturn clsReturn = new QRPINS.BL.INSIMP.MaterialConfirmReturn();
                brwChannel.mfCredentials(clsReturn);

                DataTable dtReturn = clsReturn.mfReadMaterialConfirmReturn(strPlantCode, strStdNumber);

                this.uGridReturnList.DataSource = dtReturn;
                this.uGridReturnList.DataBind();

                if (dtReturn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridReturnList, 0);
                }

                // Stop 진행여부
                dt = clsHeader.mfReadINSMaterialConfirmHRegistDetail(strPlantCode, strStdNumber);
                if (dt.Rows[0]["S1IngFlag"].ToString() != "")
                {
                    this.uOptionS1IngFlag.Value = dt.Rows[0]["S1IngFlag"].ToString();
                }
                if (dt.Rows[0]["S2IngFlag"].ToString() != "")
                {
                    //this.uOptionS2IngFlag.Value = dt.Rows[0]["S2IngFlag"].ToString();
                    this.uOptionS2IngFlag.Value = "T";
                }
                if (dt.Rows[0]["S3IngFlag"].ToString() != "")
                {
                    this.uOptionS3IngFlag.Value = dt.Rows[0]["S3IngFlag"].ToString();
                }
                if (dt.Rows[0]["S4IngFlag"].ToString() != "")
                {
                    this.uOptionS4IngFlag.Value = dt.Rows[0]["S4IngFlag"].ToString();
                }
                this.uTextReceiptID.Text = dt.Rows[0]["ReceiptID"].ToString();
                this.uTextReceiptName.Text = dt.Rows[0]["ReceiptName"].ToString();
                if (dt.Rows[0]["ReceiptDate"].ToString() != "")
                {
                    this.uDateReceiptDate.Value = dt.Rows[0]["ReceiptDate"];
                }

                this.uTextS1UserID.Text = dt.Rows[0]["S1UserID"].ToString();
                this.uTextS1UserName.Text = dt.Rows[0]["S1UserName"].ToString();
                if (dt.Rows[0]["S1Result"].ToString() != "")
                {
                    this.uOptionS1Result.Value = dt.Rows[0]["S1Result"].ToString();
                }
                if (dt.Rows[0]["S1WriteDate"].ToString() != "")
                {
                    this.uDateS1WriteDate.Value = dt.Rows[0]["S1WriteDate"];
                }
                if (dt.Rows[0]["S1FileName"].ToString() != "")
                {
                    this.uTextS1FileName.Text = dt.Rows[0]["S1FileName"].ToString();
                }
                if (dt.Rows[0]["S1EtcDesc"].ToString() != "")
                {
                    this.uTextS1EtcDesc.Text = dt.Rows[0]["S1EtcDesc"].ToString();
                }
                this.uCheckS1CompleteFlag.Checked = Convert.ToBoolean(dt.Rows[0]["S1CompleteFlag"]);
                this.uCheckS1CompleteFlag.Enabled = !Convert.ToBoolean(dt.Rows[0]["S1CompleteFlag"]);

                this.uTextS2UserID.Text = dt.Rows[0]["S2UserID"].ToString();
                this.uTextS2UserName.Text = dt.Rows[0]["S2UserName"].ToString();
                if (dt.Rows[0]["S2Result"].ToString() != "")
                {
                    this.uOptionS2Result.Value = dt.Rows[0]["S2Result"].ToString();
                }
                if (dt.Rows[0]["S2WriteDate"].ToString() != "")
                {
                    this.uDateS2WriteDate.Value = dt.Rows[0]["S2WriteDate"];
                }
                if (dt.Rows[0]["S2FileName"].ToString() != "")
                {
                    this.uTextS2FileName.Text = dt.Rows[0]["S2FileName"].ToString();
                }
                if (dt.Rows[0]["S2EtcDesc"].ToString() != "")
                {
                    this.uTextS2EtcDesc.Text = dt.Rows[0]["S2EtcDesc"].ToString();
                }
                this.uCheckS2CompleteFlag.Checked = Convert.ToBoolean(dt.Rows[0]["S2CompleteFlag"]);
                this.uCheckS2CompleteFlag.Enabled = !Convert.ToBoolean(dt.Rows[0]["S2CompleteFlag"]);

                this.uTextS3UserID.Text = dt.Rows[0]["S3UserID"].ToString();
                this.uTextS3UserName.Text = dt.Rows[0]["S3UserName"].ToString();
                if (dt.Rows[0]["S3Result"].ToString() != "")
                {
                    this.uOptionS3Result.Value = dt.Rows[0]["S3Result"].ToString();
                }
                if (dt.Rows[0]["S3WriteDate"].ToString() != "")
                {
                    this.uDateS3WriteDate.Value = dt.Rows[0]["S3WriteDate"];
                }
                if (dt.Rows[0]["S3FileName"].ToString() != "")
                {
                    this.uTextS3FileName.Text = dt.Rows[0]["S3FileName"].ToString();
                }
                if (dt.Rows[0]["S3EtcDesc"].ToString() != "")
                {
                    this.uTextS3EtcDesc.Text = dt.Rows[0]["S3EtcDesc"].ToString();
                }
                if (!dt.Rows[0]["S3MoveCode"].ToString().Equals(string.Empty))
                    this.uTextQualMove.Text = dt.Rows[0]["S3MoveCode"].ToString();
                this.uCheckS3CompleteFlag.Checked = Convert.ToBoolean(dt.Rows[0]["S3CompleteFlag"]);
                this.uCheckS3CompleteFlag.Enabled = !Convert.ToBoolean(dt.Rows[0]["S3CompleteFlag"]);

                this.uTextS4UserID.Text = dt.Rows[0]["S4UserID"].ToString();
                this.uTextS4UserName.Text = dt.Rows[0]["S4UserName"].ToString();
                if (dt.Rows[0]["S4Result"].ToString() != "")
                {
                    this.uOptionS4Result.Value = dt.Rows[0]["S4Result"].ToString();
                }
                if (dt.Rows[0]["S4WriteDate"].ToString() != "")
                {
                    this.uDateS4WriteDate.Value = dt.Rows[0]["S4WriteDate"];
                }
                if (dt.Rows[0]["S4FileName"].ToString() != "")
                {
                    this.uTextS4FileName.Text = dt.Rows[0]["S4FileName"].ToString();
                }
                if (dt.Rows[0]["S4EtcDesc"].ToString() != "")
                {
                    this.uTextS4EtcDesc.Text = dt.Rows[0]["S4EtcDesc"].ToString();
                }
                if (!dt.Rows[0]["S4MoveCode"].ToString().Equals(string.Empty))
                    this.uTextReliabilityMove.Text = dt.Rows[0]["S4MoveCode"].ToString();
                this.uCheckS4CompleteFlag.Checked = Convert.ToBoolean(dt.Rows[0]["S4CompleteFlag"]);
                this.uCheckS4CompleteFlag.Enabled = !Convert.ToBoolean(dt.Rows[0]["S4CompleteFlag"]);

                if (dt.Rows[0]["PassFailFlag"].ToString() != "")
                {
                    this.uOptionPassFailFlag.Value = dt.Rows[0]["PassFailFlag"].ToString();
                }
                if (dt.Rows[0]["CompleteFlag"].ToString() == "T")
                {
                    this.uCheckCompleteFlag.Enabled = false;
                    this.uCheckCompleteFlag.Checked = true;

                    this.uOptionS1IngFlag.Enabled = false;
                    this.uOptionS3IngFlag.Enabled = false;
                    this.uOptionS4IngFlag.Enabled = false;

                    this.uOptionPassFailFlag.Enabled = false;

                    this.uButtonReturn.Enabled = false;
                }
                else
                {
                    this.uCheckCompleteFlag.Checked = false;
                    this.uCheckCompleteFlag.Enabled = true;

                    this.uOptionS1IngFlag.Enabled = true;
                    this.uOptionS3IngFlag.Enabled = true;
                    this.uOptionS4IngFlag.Enabled = true;

                    this.uOptionPassFailFlag.Enabled = true;

                    this.uButtonReturn.Enabled = true;
                }
                this.uTextWriteID.Text = dt.Rows[0]["WriteID"].ToString();
                this.uTextWriteName.Text = dt.Rows[0]["WriteName"].ToString();
                if (dt.Rows[0]["WriteDate"].ToString() != "")
                {
                    this.uDateWriteDate.Value = dt.Rows[0]["WriteDate"];
                }

                this.uTextReqNo.Text = dt.Rows[0]["ReqNo"].ToString();
                this.uTextReqSeq.Text = dt.Rows[0]["ReqSeq"].ToString();
                if (!this.uTextReqNo.Text.Equals(string.Empty) && !this.uTextReqNo.Text.Equals(string.Empty))
                {
                    this.uTextSaveCheck.Text = "T";
                }
                else
                {
                    this.uTextSaveCheck.Text = "F";
                }

                if (!this.uCheckS3CompleteFlag.Enabled)
                {
                    this.uTextQualMove.ReadOnly = true;
                    this.uTextQualMove.Appearance.BackColor = Color.Gainsboro;
                }
                else
                {
                    this.uTextQualMove.ReadOnly = false;
                    this.uTextQualMove.Appearance.BackColor = Color.White;
                }
                if (!this.uCheckS4CompleteFlag.Enabled)
                {
                    this.uTextReliabilityMove.ReadOnly = true;
                    this.uTextReliabilityMove.Appearance.BackColor = Color.Gainsboro;
                }
                else
                {
                    this.uTextReliabilityMove.ReadOnly = false;
                    this.uTextReliabilityMove.Appearance.BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 리스트 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        private void Search_FileList(string strPlantCode, string strStdNumber)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmFile), "MaterialConfirmFile");
                QRPINS.BL.INSIMP.MaterialConfirmFile clsFile = new QRPINS.BL.INSIMP.MaterialConfirmFile();
                brwChannel.mfCredentials(clsFile);

                DataTable dtFileList = clsFile.mfReadMaterialConfirmFile(strPlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                this.uGridFile.SetDataBinding(dtFileList, string.Empty);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Mail 전송시 메소드
        private void SendMail()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //if (this.uCheckS3CompleteFlag.Enabled)
                if (this.uCheckS3CompleteFlag.Checked && this.uCheckS3CompleteFlag.Enabled)
                {
                    ArrayList arrAttachFile = new ArrayList();

                    //메일전송Method에 보낼 Array : 경로는 제외하고 로그인사용자ID + 화일명 으로 구성 ==> 화일중복방지를 위해
                    if (this.uTextS3FileName.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextS3FileName.Text);
                        //arrAttachFile.Add(m_resSys.GetString("SYS_USERID") + PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step3-" + fileDoc.Name);
                        arrAttachFile.Add(PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step3-" + fileDoc.Name);
                    }
                    else if(!this.uTextS3FileName.Text.Equals(string.Empty))
                    {
                        arrAttachFile.Add(this.uTextS3FileName.Text);
                    }

                    // Mail 받을사람의 정보 가져오기
                    DataTable dtUserInfo = GetUserInfo(PlantCode, this.uTextS3UserID.Text);

                    if (dtUserInfo.Rows.Count > 0)
                    {
                        //메일보내기
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);
                        bool bolRtn = clsmail.mfSendSMTPMail(PlantCode
                                                        , m_resSys.GetString("SYS_EMAIL")
                                                        , m_resSys.GetString("SYS_USERNAME")
                                                        , dtUserInfo.Rows[0]["EMail"].ToString()//보내려는 사람 이메일주소
                                                        , "[QRP].자재코드[" + this.uTextMaterialCode.Text + "], 자재명[" + this.uTextMaterialName.Text +
                                                            "], 신규자재 인증등록, Setp3.공정평가정보 확인 부탁드립니다."
                                                        , "[QRP] 신규자재 인증등록 자재코드[" + this.uTextMaterialCode.Text + "], 자재명[" + this.uTextMaterialName.Text +
                                                            "] 정보의 Step3.공정평가정보 담당자이십니다. 확인부탁드립니다."
                                                            //+ dtUserInfo.Rows[0]["UserName"].ToString() + "님 좋은하루 되십시오."
                                                        , arrAttachFile);

                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();

                            string strLang = m_resSys.GetString("SYS_LANG");
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M000083", strLang), msg.GetMessge_Text("M000125", strLang)
                                    , msg.GetMessge_Text("M000124", strLang) + dtUserInfo.Rows[0]["UserName"].ToString() + msg.GetMessge_Text("M000005", strLang),
                                    Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }

                //if (this.uCheckS4CompleteFlag.Enabled)
                if (this.uCheckS4CompleteFlag.Checked && this.uCheckS4CompleteFlag.Enabled)
                {
                    ArrayList arrAttachFile = new ArrayList();

                    //메일전송Method에 보낼 Array : 경로는 제외하고 로그인사용자ID + 화일명 으로 구성 ==> 화일중복방지를 위해
                    if (this.uTextS4FileName.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextS4FileName.Text);
                        //arrAttachFile.Add(m_resSys.GetString("SYS_USERID") + PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step4-" + fileDoc.Name);
                        arrAttachFile.Add(PlantCode + "-" + this.uTextStdNumber.Text + "-" + "Step4-" + fileDoc.Name);
                    }
                    else if (!this.uTextS4FileName.Text.Equals(string.Empty))
                    {
                        arrAttachFile.Add(this.uTextS4FileName.Text);
                    }

                    // Mail 받을사람의 정보 가져오기
                    DataTable dtUserInfo = GetUserInfo(PlantCode, this.uTextS4UserID.Text);

                    if (dtUserInfo.Rows.Count > 0)
                    {
                        //메일보내기
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);
                        bool bolRtn = clsmail.mfSendSMTPMail(PlantCode
                                                        , m_resSys.GetString("SYS_EMAIL")
                                                        , m_resSys.GetString("SYS_USERNAME")
                                                        , dtUserInfo.Rows[0]["EMail"].ToString()    //보내려는 사람 이메일주소
                                                        , "[QRP].자재코드[" + this.uTextMaterialCode.Text + "], 자재명[" + this.uTextMaterialName.Text +
                                                            "], 신규자재 인증등록, Setp4.신뢰성평가정보 확인 부탁드립니다."
                                                        , "[QRP] 신규자재 인증등록 자재코드[" + this.uTextMaterialCode.Text + "], 자재명[" + this.uTextMaterialName.Text +
                                                            "] 정보의 Step4.신뢰성평가정보 담당자이십니다. 확인부탁드립니다."
                                                            //+ dtUserInfo.Rows[0]["UserName"].ToString() + "님 좋은하루 되십시오."
                                                        , arrAttachFile);

                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            string strLang = m_resSys.GetString("SYS_LANG");
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M000083", strLang), msg.GetMessge_Text("M000127", strLang)
                                    , msg.GetMessge_Text("M000126", strLang) + dtUserInfo.Rows[0]["UserName"].ToString() + msg.GetMessge_Text("M000005", strLang),
                                    Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // ContentsArea 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 190);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 695;

                    for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                    {
                        this.uGridHeader.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재 TextBox 버튼 이벤트
        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 거래처 텍스트박스 버튼 이벤트
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 의뢰자 TextBox 버튼 이벤트
        private void uTextReceiptID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                 // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (PlantCode == null || PlantCode == "")
                {
                    PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (PlantCode != frmPOP.PlantCode)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextReceiptID.Text = "";
                    this.uTextReceiptName.Text = "";
                }
                else
                {
                    this.uTextReceiptID.Text = frmPOP.UserID;
                    this.uTextReceiptName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // S1 담당자 TextBox 버튼 이벤트
        private void uTextS1UserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (PlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextS1UserID.Text = "";
                    this.uTextS1UserName.Text = "";
                }
                else
                {
                    this.uTextS1UserID.Text = frmPOP.UserID;
                    this.uTextS1UserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // S2 담당자 TextBox 버튼 이벤트
        private void uTextS2UserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (PlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextS2UserID.Text = "";
                    this.uTextS2UserName.Text = "";
                }
                else
                {
                    this.uTextS2UserID.Text = frmPOP.UserID;
                    this.uTextS2UserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // S3 담당자 TextBox 버튼 이벤트
        private void uTextS3UserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (PlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextS3UserID.Text = "";
                    this.uTextS3UserName.Text = "";
                }
                else
                {
                    this.uTextS3UserID.Text = frmPOP.UserID;
                    this.uTextS3UserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // S4 담당자 TextBox 버튼 이벤트
        private void uTextS4UserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (PlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextS4UserID.Text = "";
                    this.uTextS4UserName.Text = "";
                }
                else
                {
                    this.uTextS4UserID.Text = frmPOP.UserID;
                    this.uTextS4UserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자 TextBox 버튼 이벤트
        private void uTextWriteID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (PlantCode == null || PlantCode == "")
                {
                    PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (PlantCode != frmPOP.PlantCode)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                        , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                        , Infragistics.Win.HAlign.Right);

                    this.uTextWriteID.Text = "";
                    this.uTextWriteName.Text = "";
                }
                else
                {
                    this.uTextWriteID.Text = frmPOP.UserID;
                    this.uTextWriteName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 데이터 저장하기 위한 이벤트
        private void frmINSZ0002_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 더블클릭시 상세 데이터 조회
        private void uGridHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // 선택된 Row 고정
                e.Row.Fixed = true;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strStdNumber = e.Row.Cells["StdNumber"].Value.ToString();

                Search_Detail(strPlantCode, strStdNumber);
                Search_FileList(strPlantCode, strStdNumber);

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 반려버튼 클릭시 이벤트
        private void uButtonReturn_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (PlantCode == "" || this.uTextStdNumber.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000393", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uTextReturnReason.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000415", Infragistics.Win.HAlign.Center);

                    // Set Focus
                    this.uTextReturnReason.Focus();
                    return;
                }
                else if (this.uTextReturnUserID.Text.Equals(string.Empty) || this.uTextReturnUserName.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000417", Infragistics.Win.HAlign.Center);

                    this.uTextReturnUserID.Focus();
                    return;
                }
                else if (this.uDateReturnDate.Value == null || this.uDateReturnDate.Value == DBNull.Value || this.uDateReturnDate.Value.ToString() == string.Empty)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000416", Infragistics.Win.HAlign.Center);

                    this.uDateReturnDate.DropDown();
                    return;
                }
                else
                {
                    // 저장여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000420", "M000919", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmReturn), "MaterialConfirmReturn");
                        QRPINS.BL.INSIMP.MaterialConfirmReturn clsReturn = new QRPINS.BL.INSIMP.MaterialConfirmReturn();
                        brwChannel.mfCredentials(clsReturn);

                        DataTable dtReturn = clsReturn.mfSetDataInfo();

                        DataRow drRow = dtReturn.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["Seq"] = 0;
                        drRow["ReturnReason"] = this.uTextReturnReason.Text;
                        drRow["ReturnUserID"] = this.uTextReturnUserID.Text;
                        drRow["ReturnDate"] = Convert.ToDateTime(this.uDateReturnDate.Value).ToString("yyyy-MM-dd");
                        dtReturn.Rows.Add(drRow);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "반려중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsReturn.mfSaveMaterialConfirmReturn(dtReturn, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000418", "M000924",
                                                Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000418", "M000920",
                                                    Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000418", strLang)
                                                    , ErrRtn.ErrMessage,
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 파일다운로드 이벤트
        private void uTextMSDS_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Down")
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (!this.uTextMSDS.Text.Contains(this.uTextStdNumber.Text))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextMSDS.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileDownloadNoProgView();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextMSDS.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 파일다운로드 이벤트
        private void uTextICP_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Down")
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (!this.uTextMSDS.Text.Contains(this.uTextStdNumber.Text))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextICP.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileDownloadNoProgView();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextICP.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 파일다운로드/업로드 이벤트
        private void uTextS1FileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextS1FileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextS1FileName.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextS1FileName.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 파일다운로드/업로드 이벤트
        private void uTextS2FileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextS2FileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextS2FileName.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextS2FileName.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 파일다운로드/업로드 이벤트
        private void uTextS3FileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextS3FileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextS3FileName.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextS3FileName.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 파일다운로드/업로드 이벤트
        private void uTextS4FileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextS4FileName.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(this.uTextS4FileName.Text);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileDownloadNoProgView();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextS4FileName.Text);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 진행여부 라디오버튼 값 변화 이벤트
        private void uOptionS1IngFlag_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS1IngFlag.CheckedItem == null)
                {
                    this.uTab.Tabs["Step1"].Visible = false;
                    this.uTextS1EtcDesc.Text = "";
                    this.uTextS1FileName.Text = "";
                    this.uTextS1UserID.Text = "";
                    this.uTextS1UserName.Text = "";
                    this.uOptionS1Result.CheckedIndex = -1;
                    this.uDateS1WriteDate.Value = DateTime.Now;
                    //this.uOptionS2IngFlag.Enabled = false;
                }
                else if (this.uOptionS1IngFlag.CheckedItem.DataValue.ToString() == "F")
                {
                    this.uTab.Tabs["Step1"].Visible = false;
                    this.uTextS1EtcDesc.Text = "";
                    this.uTextS1FileName.Text = "";
                    this.uTextS1UserID.Text = "";
                    this.uTextS1UserName.Text = "";
                    this.uOptionS1Result.CheckedIndex = -1;
                    this.uDateS1WriteDate.Value = DateTime.Now;
                    //this.uOptionS2IngFlag.Enabled = true;
                }
                else
                {
                    //this.uOptionS2IngFlag.Enabled = true;
                    this.uTab.Tabs["Step1"].Visible = true;
                    this.uTab.Tabs["Step1"].Selected = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 진행여부 라디오버튼 값 변화 이벤트
        private void uOptionS2IngFlag_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS2IngFlag.CheckedItem == null)
                {
                    this.uTab.Tabs["Step2"].Visible = false;
                    this.uTextS2EtcDesc.Text = "";
                    this.uTextS2FileName.Text = "";
                    this.uTextS2UserID.Text = "";
                    this.uTextS2UserName.Text = "";
                    this.uOptionS2Result.CheckedIndex = -1;
                    this.uDateS2WriteDate.Value = DateTime.Now;
                    this.uOptionS3IngFlag.Enabled = false;
                }
                else if (this.uOptionS2IngFlag.CheckedItem.DataValue.ToString() == "F")
                {
                    this.uTab.Tabs["Step2"].Visible = false;
                    this.uTextS2EtcDesc.Text = "";
                    this.uTextS2FileName.Text = "";
                    this.uTextS2UserID.Text = "";
                    this.uTextS2UserName.Text = "";
                    this.uOptionS2Result.CheckedIndex = -1;
                    this.uDateS2WriteDate.Value = DateTime.Now;
                    this.uOptionS3IngFlag.Enabled = true;
                }
                else
                {
                    this.uOptionS3IngFlag.Enabled = true;
                    this.uTab.Tabs["Step2"].Visible = true;
                    this.uTab.Tabs["Step2"].Selected = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 진행여부 라디오버튼 값 변화 이벤트
        private void uOptionS3IngFlag_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS3IngFlag.CheckedItem == null)
                {
                    this.uTab.Tabs["Step3"].Visible = false;
                    this.uTextS3EtcDesc.Text = "";
                    this.uTextS3FileName.Text = "";
                    this.uTextS3UserID.Text = "";
                    this.uTextS3UserName.Text = "";
                    this.uOptionS3Result.CheckedIndex = -1;
                    this.uDateS3WriteDate.Value = DateTime.Now;
                    this.uOptionS4IngFlag.Enabled = false;
                }
                else if (this.uOptionS3IngFlag.CheckedItem.DataValue.ToString() == "F")
                {
                    this.uTab.Tabs["Step3"].Visible = false;
                    this.uTextS3EtcDesc.Text = "";
                    this.uTextS3FileName.Text = "";
                    this.uTextS3UserID.Text = "";
                    this.uTextS3UserName.Text = "";
                    this.uOptionS3Result.CheckedIndex = -1;
                    this.uDateS3WriteDate.Value = DateTime.Now;
                    this.uOptionS4IngFlag.Enabled = true;
                }
                else
                {
                    this.uOptionS4IngFlag.Enabled = true;
                    this.uTab.Tabs["Step3"].Visible = true;
                    this.uTab.Tabs["Step3"].Selected = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 진행여부 라디오버튼 값 변화 이벤트
        private void uOptionS4IngFlag_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS4IngFlag.CheckedItem == null || this.uOptionS4IngFlag.CheckedItem.DataValue.ToString() == "F")
                {
                    this.uTab.Tabs["Step4"].Visible = false;
                    this.uTextS4EtcDesc.Text = "";
                    this.uTextS4FileName.Text = "";
                    this.uTextS4UserID.Text = "";
                    this.uTextS4UserName.Text = "";
                    this.uOptionS4Result.CheckedIndex = -1;
                    this.uDateS4WriteDate.Value = DateTime.Now;
                }
                else
                {
                    this.uTab.Tabs["Step4"].Visible = true;
                    this.uTab.Tabs["Step4"].Selected = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재텍스트박스 키다운 이벤트
        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.SelectedText == this.uTextSearchMaterialCode.Text)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 거래처텍스트박스 키다운 이벤트
        private void uTextSearchVendorCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchVendorCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strVendorCode = this.uTextSearchVendorCode.Text;

                        // UserName 검색 함수 호출
                        DataTable dtVendor = GetVendorInfo(strVendorCode);

                        if (dtVendor.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtVendor.Rows.Count; i++)
                            {
                                this.uTextSearchVendorName.Text = dtVendor.Rows[i]["VendorName"].ToString();
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000168",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextSearchVendorCode.Text = "";
                            this.uTextSearchVendorName.Text = "";
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uTextSearchVendorCode.TextLength <= 1 || this.uTextSearchVendorCode.SelectedText == this.uTextSearchVendorCode.Text)
                    {
                        this.uTextSearchVendorName.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 접수자 텍스트박스 키다운 이벤트
        private void uTextReceiptID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReceiptID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextReceiptID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(PlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextReceiptID.Text = "";
                            this.uTextReceiptName.Text = "";
                        }
                        else
                        {
                            this.uTextReceiptName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uTextReceiptID.TextLength <= 1 || this.uTextReceiptID.SelectedText == this.uTextReceiptID.Text)
                    {
                        this.uTextReceiptName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 품질감사 담당자 텍스트박스 키다운 이벤트
        private void uTextS1UserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextS1UserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextS1UserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(PlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextS1UserID.Text = "";
                            this.uTextS1UserName.Text = "";
                        }
                        else
                        {
                            this.uTextS1UserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uTextS1UserID.TextLength <= 1 || this.uTextS1UserID.SelectedText == this.uTextS1UserID.Text)
                    {
                        this.uTextS1UserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 수입검사 담당자 텍스트박스 키다운 이벤트
        private void uTextS2UserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextS2UserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextS2UserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(PlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextS2UserID.Text = "";
                            this.uTextS2UserName.Text = "";
                        }
                        else
                        {
                            this.uTextS2UserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uTextS2UserID.TextLength <= 1 || this.uTextS2UserID.SelectedText == this.uTextS2UserID.Text)
                    {
                        this.uTextS2UserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공정평가 담당자 텍스트박스 키다운 이벤트
        private void uTextS3UserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextS3UserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextS3UserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(PlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextS3UserID.Text = "";
                            this.uTextS3UserName.Text = "";
                        }
                        else
                        {
                            this.uTextS3UserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uTextS3UserID.TextLength <= 1 || this.uTextS3UserID.SelectedText == this.uTextS3UserID.Text)
                    {
                        this.uTextS3UserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 신뢰성 평가 담당자 텍스트박스 키다운 이벤트
        private void uTextS4UserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextS4UserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextS4UserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(PlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextS4UserID.Text = "";
                            this.uTextS4UserName.Text = "";
                        }
                        else
                        {
                            this.uTextS4UserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uTextS4UserID.TextLength <= 1 || this.uTextS4UserID.SelectedText == this.uTextS4UserID.Text)
                    {
                        this.uTextS4UserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자 텍스트박스 키다운 이벤트
        private void uTextWriteID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextWriteID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(PlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextWriteID.Text = "";
                            this.uTextWriteName.Text = "";
                        }
                        else
                        {
                            this.uTextWriteName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    if (this.uTextWriteID.TextLength <= 1 || this.uTextWriteID.SelectedText == this.uTextWriteID.Text)
                    {
                        this.uTextWriteName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성완료 체크시 이후 수정불가 메세지를 띄우기 위한 이벤트
        private void uCheckCompleteFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true)
                {
                    ////// CheckEditor 가 활성화 상태일때
                    ////if(this.uOptionS1Result.CheckedIndex.Equals(1) || this.uOptionS2Result.CheckedIndex.Equals(1) ||
                    ////    this.uOptionS3Result.CheckedIndex.Equals(1) || this.uOptionS4Result.CheckedIndex.Equals(1) ||
                    ////    this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                    ////{

                    ////    WinMessageBox msg = new WinMessageBox();
                    ////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                    "확인창", "작성완료 확인", "불합격이 있어 작성완료 할 수 없습니다.",
                    ////                    Infragistics.Win.HAlign.Right);

                    ////    // 작성완료 체크해제
                    ////    this.uCheckCompleteFlag.Checked = false;
                    ////}
                    ////else
                    ////{
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (this.uOptionS1IngFlag.CheckedIndex == -1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000132", "M000133",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS2IngFlag.CheckedIndex == -1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000137", "M000138",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS3IngFlag.CheckedIndex == -1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000140", "M000141",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS4IngFlag.CheckedIndex == -1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000143", "M000144",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionPassFailFlag.CheckedIndex == -1)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001244", "M001246",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }

                    // 각 스텝별 담당자 정보 체크
                    else if (this.uOptionS1IngFlag.CheckedIndex == 0 && this.uTextS1UserID.Text.ToString() == "")
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M001213", Infragistics.Win.HAlign.Center);

                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS2IngFlag.CheckedIndex == 0 && this.uTextS2UserID.Text.ToString() == "")
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000749", Infragistics.Win.HAlign.Center);

                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS3IngFlag.CheckedIndex == 0 && this.uTextS3UserID.Text.ToString() == "")
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000286", Infragistics.Win.HAlign.Center);

                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS4IngFlag.CheckedIndex == 0 && this.uTextS4UserID.Text.ToString() == "")
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000780", Infragistics.Win.HAlign.Center);

                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }

                    // 작성완료 체크 후 저장시 각 스텝별로 필요가 체크가 되고, 작성완료가된경우만 최종적으로 작성완료를 할 수 있음.    
                    else if (this.uOptionS1IngFlag.CheckedIndex == 0 && this.uCheckS1CompleteFlag.Checked == false)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000881", "M000135",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS2IngFlag.CheckedIndex == 0 && this.uCheckS2CompleteFlag.Checked == false)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000881", "M000139",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS3IngFlag.CheckedIndex == 0 && this.uCheckS3CompleteFlag.Checked == false)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000881", "M000142",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uOptionS4IngFlag.CheckedIndex == 0 && this.uCheckS4CompleteFlag.Checked == false)
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000881", "M000145",
                                        Infragistics.Win.HAlign.Right);
                        // 작성완료 체크해제
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    else if (this.uTextWriteID.Text.Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000881", "M000386",
                                            Infragistics.Win.HAlign.Center);

                        this.uTextWriteID.Focus();
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    ////}
                    if (this.uTextReqNo.Text.Equals(string.Empty) || this.uTextReqSeq.Text.Equals(string.Empty))
                    {
                        ////WinMessageBox msg = new WinMessageBox();
                        ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000137", "M001397",
                                            Infragistics.Win.HAlign.Right);

                        this.uButtonMove.Focus();
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 수입검사로 이동
        private void uButtonMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS1IngFlag.CheckedIndex == -1)
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000134", "M000133",
                                    Infragistics.Win.HAlign.Right);
                    
                    this.uOptionS1IngFlag.Focus();
                }
                else if (this.uOptionS1IngFlag.Value.ToString() == "T" && this.uOptionS1Result.CheckedIndex == -1)
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000134", "M000136",
                                    Infragistics.Win.HAlign.Right);

                    // Focus
                    this.uTab.Tabs["Step1"].Selected = true;
                    this.uOptionS1Result.Focus();
                }
                else
                {
                    frmINS0004 frmINS = new frmINS0004();
                    frmINS.PlantCode = PlantCode;
                    frmINS.VendorCode = m_strVendorCode;
                    frmINS.MaterialCode = this.uTextMaterialCode.Text;
                    frmINS.SpecNo = this.uTextSpecNo.Text;
                    frmINS.MoldSeq = this.uTextMoldSeq.Text;
                    frmINS.GRDate = this.uTextGRDate.Text;
                    frmINS.GRNo = this.uTextGRNo.Text;
                    frmINS.MoveFormName = this.Name;
                    frmINS.ReqNo = this.uTextReqNo.Text;
                    frmINS.ReqSeq = this.uTextReqSeq.Text;
                    frmINS.ReceipTime = DateTime.Now.ToString("HH:mm:ss");

                    // 수입검사에서 저장하지 않은경우
                    if (this.uTextReqNo.Text.Equals(string.Empty) && this.uTextReqSeq.Text.Equals(string.Empty))
                    {
                        //// 저장
                        //mfSave();

                        // 이벤트 해제
                        CancelEventArgs aa = new CancelEventArgs();
                        this.uGroupBoxContentsArea.ExpandedStateChanging -= new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
                        mfSave();
                        // 이벤트 등록
                        this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);

                        if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                        {
                            Point point = new Point(0, 825);
                            this.uGroupBoxContentsArea.Location = point;
                            this.uGridHeader.Height = 695;

                            for (int i = 0; i < this.uGridHeader.Rows.Count; i++)
                            {
                                this.uGridHeader.Rows[i].Fixed = false;
                            }
                        }
                    }
                    else
                    {
                        this.uTextSaveCheck.Text = "T";
                    }

                    // 저장성공시 수입검사화면으로 이동
                    if (this.uTextSaveCheck.Text.Equals("T"))
                    {
                        CommonControl cControl = new CommonControl();
                        Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                        Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                        //해당화면의 탭이 있는경우 해당 탭 닫기
                        if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0004"))
                        {
                            uTabMenu.Tabs["QRPINS" + "," + "frmINS0004"].Close();
                        }
                        //탭에 추가
                        uTabMenu.Tabs.Add("QRPINS" + "," + "frmINS0004", "수입검사등록");
                        uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINS0004"];

                        //호출시 화면 속성 설정
                        frmINS.AutoScroll = true;
                        frmINS.MdiParent = this.MdiParent;
                        frmINS.ControlBox = false;
                        frmINS.Dock = DockStyle.Fill;
                        frmINS.FormBorderStyle = FormBorderStyle.None;
                        frmINS.WindowState = FormWindowState.Normal;
                        frmINS.Text = "수입검사등록";
                        frmINS.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINSZ0002_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 각 Step 단계별 결과값에 따른 자동 합부판정 이벤트들
        private void uOptionS1Result_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS1Result.CheckedIndex.Equals(1))
                {
                    this.uOptionPassFailFlag.CheckedIndex = 1;
                }
                else if (this.uOptionS1Result.CheckedIndex.Equals(0))
                {
                    
                    if ((this.uOptionS2IngFlag.CheckedIndex.Equals(1) || this.uOptionS2Result.CheckedIndex.Equals(0)) && 
                        (this.uOptionS3IngFlag.CheckedIndex.Equals(1) || this.uOptionS3Result.CheckedIndex.Equals(0)) &&
                        (this.uOptionS4IngFlag.CheckedIndex.Equals(1) || this.uOptionS4Result.CheckedIndex.Equals(0)))
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uOptionS2Result_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS2Result.CheckedIndex.Equals(1))
                {
                    this.uOptionPassFailFlag.CheckedIndex = 1;
                }
                else if (this.uOptionS2Result.CheckedIndex.Equals(0))
                {

                    if ((this.uOptionS1IngFlag.CheckedIndex.Equals(1) || this.uOptionS1Result.CheckedIndex.Equals(0)) &&
                        (this.uOptionS3IngFlag.CheckedIndex.Equals(1) || this.uOptionS3Result.CheckedIndex.Equals(0)) &&
                        (this.uOptionS4IngFlag.CheckedIndex.Equals(1) || this.uOptionS4Result.CheckedIndex.Equals(0)))
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uOptionS3Result_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS3Result.CheckedIndex.Equals(1))
                {
                    this.uOptionPassFailFlag.CheckedIndex = 1;
                }
                else if (this.uOptionS3Result.CheckedIndex.Equals(0))
                {

                    if ((this.uOptionS1IngFlag.CheckedIndex.Equals(1) || this.uOptionS1Result.CheckedIndex.Equals(0)) &&
                        (this.uOptionS2IngFlag.CheckedIndex.Equals(1) || this.uOptionS2Result.CheckedIndex.Equals(0)) &&
                        (this.uOptionS4IngFlag.CheckedIndex.Equals(1) || this.uOptionS4Result.CheckedIndex.Equals(0)))
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uOptionS4Result_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uOptionS4Result.CheckedIndex.Equals(1))
                {
                    this.uOptionPassFailFlag.CheckedIndex = 1;
                }
                else if (this.uOptionS4Result.CheckedIndex.Equals(0))
                {

                    if ((this.uOptionS1IngFlag.CheckedIndex.Equals(1) || this.uOptionS1Result.CheckedIndex.Equals(0)) &&
                        (this.uOptionS2IngFlag.CheckedIndex.Equals(1) || this.uOptionS2Result.CheckedIndex.Equals(0)) &&
                        (this.uOptionS3IngFlag.CheckedIndex.Equals(1) || this.uOptionS3Result.CheckedIndex.Equals(0)))
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextReturnUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                this.uTextReturnUserID.Text = frmPOP.UserID;
                this.uTextReturnUserName.Text = frmPOP.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextReturnUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReturnUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextReturnUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(PlantCode, strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextReturnUserID.Text = "";
                            this.uTextReturnUserName.Text = "";
                        }
                        else
                        {
                            this.uTextReturnUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReturnUserID.TextLength <= 1 || this.uTextReturnUserID.SelectedText == this.uTextReturnUserID.Text)
                    {
                        this.uTextReturnUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchReqUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchReqUserID.Text = frmPOP.UserID;
                this.uTextSearchReqUserName.Text = frmPOP.UserName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchReqUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchReqUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000270", "M000266",
                                        Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                            return;
                        }

                        String strWriteID = this.uTextSearchReqUserID.Text;

                        // UserName 검색 함수 호출
                        DataTable dtUserInfo = GetUserInfo(this.uComboSearchPlant.Value.ToString(), strWriteID);

                        if (!(dtUserInfo.Rows.Count > 0))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextSearchReqUserID.Text = "";
                            this.uTextSearchReqUserName.Text = "";
                        }
                        else
                        {
                            this.uTextSearchReqUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchReqUserID.TextLength <= 1 || this.uTextSearchReqUserID.SelectedText == this.uTextSearchReqUserID.Text)
                    {
                        this.uTextSearchReqUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성완료 체크시 그 탭안에 컨트롤 수정불가 상태로
        private void uCheckS1CompleteFlag_EnabledChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckS1CompleteFlag.Enabled.Equals(true))
                {
                    this.uTextS1EtcDesc.Enabled = true;
                    this.uTextS1FileName.Enabled = true;
                    this.uTextS1UserID.Enabled = true;
                    this.uDateS1WriteDate.Enabled = true;
                    this.uOptionS1Result.Enabled = true;
                }
                else
                {
                    this.uTextS1EtcDesc.Enabled = false;
                    //this.uTextS1FileName.Enabled = false;
                    this.uTextS1UserID.Enabled = false;
                    this.uDateS1WriteDate.Enabled = false;
                    this.uOptionS1Result.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckS2CompleteFlag_EnabledChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckS2CompleteFlag.Enabled.Equals(true))
                {
                    this.uTextS2EtcDesc.Enabled = true;
                    this.uTextS2FileName.Enabled = true;
                    this.uTextS2UserID.Enabled = true;
                    this.uDateS2WriteDate.Enabled = true;
                    this.uOptionS2Result.Enabled = true;
                }
                else
                {
                    this.uTextS2EtcDesc.Enabled = false;
                    //this.uTextS2FileName.Enabled = false;
                    this.uTextS2UserID.Enabled = false;
                    this.uDateS2WriteDate.Enabled = false;
                    this.uOptionS2Result.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }             
        }

        private void uCheckS3CompleteFlag_EnabledChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckS3CompleteFlag.Enabled.Equals(true))
                {
                    this.uTextS3EtcDesc.Enabled = true;
                    this.uTextS3FileName.Enabled = true;
                    this.uTextS3UserID.Enabled = true;
                    this.uDateS3WriteDate.Enabled = true;
                    this.uOptionS3Result.Enabled = true;
                }
                else
                {
                    this.uTextS3EtcDesc.Enabled = false;
                    //this.uTextS3FileName.Enabled = false;
                    this.uTextS3UserID.Enabled = false;
                    this.uDateS3WriteDate.Enabled = false;
                    this.uOptionS3Result.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckS4CompleteFlag_EnabledChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckS4CompleteFlag.Enabled.Equals(true))
                {
                    this.uTextS4EtcDesc.Enabled = true;
                    this.uTextS4FileName.Enabled = true;
                    this.uTextS4UserID.Enabled = true;
                    this.uDateS4WriteDate.Enabled = true;
                    this.uOptionS4Result.Enabled = true;
                }
                else
                {
                    this.uTextS4EtcDesc.Enabled = false;
                   // this.uTextS4FileName.Enabled = false;
                    this.uTextS4UserID.Enabled = false;
                    this.uDateS4WriteDate.Enabled = false;
                    this.uOptionS4Result.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 각스텝 작성완료 체크시 검사결과 선택했는지 체크
        private void uCheckS1CompleteFlag_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckS1CompleteFlag.Checked)
                {
                    if (this.uOptionS1Result.CheckedIndex.Equals(-1))
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000173", "M000136",
                                        Infragistics.Win.HAlign.Right);
                        
                        this.uCheckS1CompleteFlag.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckS2CompleteFlag_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckS3CompleteFlag_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckS3CompleteFlag.Checked)
                {
                    if (this.uOptionS3Result.CheckedIndex.Equals(-1))
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000173", "M001398",
                                        Infragistics.Win.HAlign.Right);

                        this.uCheckS3CompleteFlag.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckS4CompleteFlag_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckS4CompleteFlag.Checked)
                {
                    if (this.uOptionS4Result.CheckedIndex.Equals(-1))
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000173", "M001399",
                                        Infragistics.Win.HAlign.Right);

                        this.uCheckS4CompleteFlag.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonEMail_Click(object sender, EventArgs e)
        {

            if (this.uTextStdNumber.Text.Equals(string.Empty))
                return;
            WinMessageBox msg = new WinMessageBox();

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001449", "M001448",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                return;

            sendReturnMail();
            sendSetS3UserMail();
            sendSetS4UserMail();
            sendS2CompleteMail();
            sendS3CompleteMail();
            sendS4CompleteMail();

            ////try
            ////{
            ////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////    ArrayList arrAttachFile = new ArrayList();

            ////    //메일보내기
            ////    QRPBrowser brwChannel = new QRPBrowser();
            ////    brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
            ////    QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
            ////    brwChannel.mfCredentials(clsmail);
            ////    bool bolRtn = clsmail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE")
            ////                                    , "mes_pjt10@bokwang.com"
            ////                                    , "윤경희"
            ////                                    , "mes_pjt12@bokwang.com" //보내려는 사람 이메일주소
            ////                                    , "[QRP].자재코드[" + this.uTextMaterialCode.Text + "], 자재명[" + this.uTextMaterialName.Text +
            ////                                        "], 신규자재 인증등록, Setp3.공정평가정보 확인 부탁드립니다."
            ////                                    , "[QRP] 신규자재 인증등록 자재코드[" + this.uTextMaterialCode.Text + "], 자재명[" + this.uTextMaterialName.Text +
            ////                                        "] 정보의 Step3.공정평가정보 담당자이십니다. 확인부탁드립니다."
            ////        //+ dtUserInfo.Rows[0]["UserName"].ToString() + "님 좋은하루 되십시오."
            ////                                    , arrAttachFile);

            ////    if (!bolRtn)
            ////    {
            ////        WinMessageBox msg = new WinMessageBox();
            ////        DialogResult Result = new DialogResult();
            ////        string strLang = m_resSys.GetString("SYS_LANG");
            ////        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
            ////                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                msg.GetMessge_Text("M000083", strLang), msg.GetMessge_Text("M000125", strLang)
            ////                , msg.GetMessge_Text("M000124", strLang) + "테스트" + msg.GetMessge_Text("M000005", strLang),
            ////                Infragistics.Win.HAlign.Right);
            ////        return;
            ////    }

            ////}
            ////catch (Exception ex)
            ////{
            ////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////    frmErr.ShowDialog();
            ////}
            ////finally
            ////{
            ////}
        }

        private void uGridFile_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (e.Cell.Column.ToString() == "FilePath")
                {
                    if (string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value.ToString().Contains(":\\") || e.Cell.Value == DBNull.Value)
                    {
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            // 화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                            // 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0007");

                            // 첨부파일 Download
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            arrFile.Add(PlantCode+ "-" + this.uTextStdNumber.Text + "-" + e.Cell.Row.Cells["UniqueKey"].Value.ToString() + "-" + e.Cell.Value.ToString());

                            // Download정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + "\\" + PlantCode + "-" + this.uTextStdNumber.Text + "-" + e.Cell.Row.Cells["UniqueKey"].Value.ToString() + "-" + e.Cell.Value.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonAVLMove_Click(object sender, EventArgs e)
        {
            try
            {
                frmINSZ0004 frmINS = new frmINSZ0004();
                frmINS.PlantCode = PlantCode;
                frmINS.VendorCode = this.uTextVendorCode.Text;
                frmINS.MoveFormName = this.Name;
                
                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                //해당화면의 탭이 있는경우 해당 탭 닫기
                if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINSZ0004"))
                {
                    uTabMenu.Tabs["QRPINS" + "," + "frmINSZ0004"].Close();
                }
                //탭에 추가
                uTabMenu.Tabs.Add("QRPINS" + "," + "frmINSZ0004", "AVL 정보");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINSZ0004"];

                //호출시 화면 속성 설정
                frmINS.AutoScroll = true;
                frmINS.MdiParent = this.MdiParent;
                frmINS.ControlBox = false;
                frmINS.Dock = DockStyle.Fill;
                frmINS.FormBorderStyle = FormBorderStyle.None;
                frmINS.WindowState = FormWindowState.Normal;
                frmINS.Text = "AVL 정보";
                frmINS.Show();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonQualMove_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (!this.uTextQualMove.Text.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATQUL.QATNewQualH), "QATNewQualH");
                    QRPQAT.BL.QATQUL.QATNewQualH clsHeader = new QRPQAT.BL.QATQUL.QATNewQualH();
                    brwChannel.mfCredentials(clsHeader);

                    DataTable dtQualCheck = clsHeader.mfReadQATNewQualH_Detail(PlantCode, this.uTextQualMove.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtQualCheck.Rows.Count > 0)
                    {
                        QRPQAT.UI.frmQATZ0001 frmQAT = new QRPQAT.UI.frmQATZ0001();
                        frmQAT.PlantCode = PlantCode;
                        frmQAT.QualNo = this.uTextQualMove.Text;
                        frmQAT.MoveFormName = this.Name;

                        CommonControl cControl = new CommonControl();
                        Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                        Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                        //해당화면의 탭이 있는경우 해당 탭 닫기
                        if (uTabMenu.Tabs.Exists("QRPQAT" + "," + "frmQATZ0001"))
                        {
                            uTabMenu.Tabs["QRPQAT" + "," + "frmQATZ0001"].Close();
                        }
                        //탭에 추가
                        uTabMenu.Tabs.Add("QRPQAT" + "," + "frmQATZ0001", "신제품 Qual 관리");
                        uTabMenu.SelectedTab = uTabMenu.Tabs["QRPQAT,frmQATZ0001"];

                        //호출시 화면 속성 설정
                        frmQAT.AutoScroll = true;
                        frmQAT.MdiParent = this.MdiParent;
                        frmQAT.ControlBox = false;
                        frmQAT.Dock = DockStyle.Fill;
                        frmQAT.FormBorderStyle = FormBorderStyle.None;
                        frmQAT.WindowState = FormWindowState.Normal;
                        frmQAT.Text = "신제품 Qual 관리";
                        frmQAT.Show();
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001264", "M000110", "M000111", Infragistics.Win.HAlign.Right);
                        this.uTextQualMove.Focus();
                        this.uTextQualMove.SelectAll();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonReliabilityMove_Click(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (!this.uTextReliabilityMove.Text.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATREL.ReliabilityInspect), "ReliabilityInspect");
                    QRPQAT.BL.QATREL.ReliabilityInspect clsReInspect = new QRPQAT.BL.QATREL.ReliabilityInspect();
                    brwChannel.mfCredentials(clsReInspect);

                    DataTable dtReInspectCheck = clsReInspect.mfReadQATReliabilityInspectDetail(PlantCode, this.uTextReliabilityMove.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtReInspectCheck.Rows.Count > 0)
                    {
                        QRPQAT.UI.frmQATZ0007 frmQAT = new QRPQAT.UI.frmQATZ0007();
                        frmQAT.PlantCode = PlantCode;
                        frmQAT.ReliabilityNo = this.uTextReliabilityMove.Text;
                        frmQAT.MoveFormName = this.Name;

                        CommonControl cControl = new CommonControl();
                        Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                        Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                        //해당화면의 탭이 있는경우 해당 탭 닫기
                        if (uTabMenu.Tabs.Exists("QRPQAT" + "," + "frmQATZ0007"))
                        {
                            uTabMenu.Tabs["QRPQAT" + "," + "frmQATZ0007"].Close();
                        }
                        //탭에 추가
                        uTabMenu.Tabs.Add("QRPQAT" + "," + "frmQATZ0007", "신뢰성검사 의뢰/등록");
                        uTabMenu.SelectedTab = uTabMenu.Tabs["QRPQAT,frmQATZ0007"];

                        //호출시 화면 속성 설정
                        frmQAT.AutoScroll = true;
                        frmQAT.MdiParent = this.MdiParent;
                        frmQAT.ControlBox = false;
                        frmQAT.Dock = DockStyle.Fill;
                        frmQAT.FormBorderStyle = FormBorderStyle.None;
                        frmQAT.WindowState = FormWindowState.Normal;
                        frmQAT.Text = "신뢰성검사 의뢰/등록";
                        frmQAT.Show();
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001264", "M001358", "M001359", Infragistics.Win.HAlign.Right);
                        this.uTextReliabilityMove.Focus();
                        this.uTextReliabilityMove.SelectAll();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Mail Send

        /// <summary>
        /// 반려시 신규자재 의뢰자에게 메일발송 메소드
        /// 메일발송조건 : 신규자재 인증등록 화면에서 반려버튼 누르고 저장 시
        /// </summary>
        private void sendReturnMail()
        {
            try
            {
                //System Resource Info
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //발신인, 수신인 ID저장
                string strSendUserID = this.uTextReturnUserID.Text; //"TESTUSR";
                string strGetUserID = this.uTextReqID.Text; //"TESTUSR";

                //사용자정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);
                //발신인의 사용자정보,수신인의 사용자정보 가져오기
                DataTable dtSendUserInfo = clsUser.mfReadSYSUser(PlantCode, strSendUserID, m_resSys.GetString("SYS_LANG"));
                DataTable dtGetUserInfo = clsUser.mfReadSYSUser(PlantCode, strGetUserID, m_resSys.GetString("SYS_LANG"));

                

                //발신인의 정보 수신인의정보 주정보중 마우
                if (dtSendUserInfo.Rows.Count > 0 && dtGetUserInfo.Rows.Count > 0)
                {
                    //발신인 메일정보와 수신인 의 메일정보가 있는경우
                    if (!dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty) && !dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                    {
                        //첨부파일
                        ArrayList arrAttachFile = new ArrayList();

                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strTitle = string.Empty;
                        string strContents = string.Empty;

                        if (strLang.Equals("KOR"))
                        {
                            strTitle = "신규자재의뢰件 반려되었습니다. 확인 바랍니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증의뢰현황)";
                            strContents = "<HTML>" +
                                                                    "<HEAD>" +
                                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                    "</HEAD>" +
                                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp;</SPAN></P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp;</SPAN></P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 반려정보 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려일자 : </SPAN>" + Convert.ToDateTime(this.uDateReturnDate.Value).ToString("yyyy-MM-dd") + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려자&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextReturnUserName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려사유 : </SPAN>" + this.uTextReturnReason.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                    "</BODY>" +
                                                                    "</HTML>";

                        }
                        else if (strLang.Equals("CHN"))
                        {
                            strTitle = "新材料委托件已返还.请确认.(QRP ▶ 品质检查管理 ▶ 新材料认证委托现状)";
                            strContents = "<HTML>" +
                                                                    "<HEAD>" +
                                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                    "</HEAD>" +
                                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 委托情报 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 管理编号 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 委托日期 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料名&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料编号 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 供应商名称 : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp;</SPAN></P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp;</SPAN></P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 返还情报 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 返还日&nbsp;&nbsp;&nbsp;   : </SPAN>" + Convert.ToDateTime(this.uDateReturnDate.Value).ToString("yyyy-MM-dd") + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 返还者&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextReturnUserName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 返还事由 : </SPAN>" + this.uTextReturnReason.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                    "</BODY>" +
                                                                    "</HTML>";

                        }
                        else if (strLang.Equals("ENG"))
                        {
                            strTitle = "신규자재의뢰件 반려되었습니다. 확인 바랍니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증의뢰현황)";
                            strContents = "<HTML>" +
                                                                    "<HEAD>" +
                                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                    "</HEAD>" +
                                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp;</SPAN></P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">&nbsp;</SPAN></P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 반려정보 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려일자 : </SPAN>" + Convert.ToDateTime(this.uDateReturnDate.Value).ToString("yyyy-MM-dd") + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려자&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextReturnUserName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 반려사유 : </SPAN>" + this.uTextReturnReason.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                    "</BODY>" +
                                                                    "</HTML>";

                        }
                        else
                        {
                            strTitle = string.Empty;
                            strContents = string.Empty;

                        }

                        //메일보내기
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);
                        bool bolRtn = clsmail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE")
                                                        , dtSendUserInfo.Rows[0]["EMail"].ToString()
                                                        , dtSendUserInfo.Rows[0]["UserName"].ToString()
                                                        , dtGetUserInfo.Rows[0]["EMail"].ToString() //보내려는 사람 이메일주소
                                                        , strTitle
                                                        , strContents
                                                        , arrAttachFile);
                        //메일발송 실패시
                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //신규자재 의뢰자에게 반려정보 Mail 발송하는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000083", "M001428"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                    else
                    {
                        //발신인 메일정보가 공백일경우 메세지출련
                        if (dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //반려자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001429"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else if (dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //의뢰자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001431"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
                else
                {
                    //발신인 정보가 존재하지 않을경우
                    if (!(dtSendUserInfo.Rows.Count > 0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //반려자의 사용자 정보를 가져//구려니ㅣ오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000056", "M001432"
                                                    , Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (!(dtGetUserInfo.Rows.Count > 0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //의뢰자의 사용자 정보를 가져오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000056", "M001433"
                                                    , Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공정평가 지정 시 각 Step담당자로 지정된자 메일발송메소드
        /// 최초1회 메일링 시 담당자 변경되었을 경우 발송되지 않기 때문에 이전Step완료 저장 시 송부
        /// </summary>
        private void sendSetS3UserMail()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //발신자 수신자 정보 조회
                string strSendUserID = this.uTextReqID.Text; //"TESTUSR";
                string strGetUserID = this.uTextS3UserID.Text;// "TESTUSR";
                //string strGetUserID = this.uTextReqID.Text;
               

                //사용자정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                //발신자와 수신자 정보가 져오기
                DataTable dtSendUserInfo = clsUser.mfReadSYSUser(PlantCode, strSendUserID, m_resSys.GetString("SYS_LANG"));
                DataTable dtGetUserInfo = clsUser.mfReadSYSUser(PlantCode, strGetUserID, m_resSys.GetString("SYS_LANG"));

                //발신자와 수신자정보 모두있을경우 메일을 보낼 수 있다.
                if (dtSendUserInfo.Rows.Count > 0 && dtGetUserInfo.Rows.Count > 0)
                {
                    //발신자의 메일정보와 수신자의 메일정보가 공백이 아닌경우 메일을 보낼 수 있다.
                    if (!dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty) && !dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                    {
                        ArrayList arrAttachFile = new ArrayList();

                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strTitle = string.Empty;
                        string strContents = string.Empty;

                        if (strLang.Equals("KOR"))
                        {
                            strTitle = "신규자재 품질평가(공정평가) 담당자입니다. 평가정보 등록바랍니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                            strContents = "<HTML>" +
                                                                    "<HEAD>" +
                                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                    "</HEAD>" +
                                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 신규자재정보 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                    "</BODY>" +
                                                                    "</HTML>"; //Email Contents;
                        }
                        else if (strLang.Equals("CHN"))
                        {
                            strTitle = "新材料品质评价（工程评价）担当.请登录评价信息.(QRP ▶ 品质检查管理 ▶ 新材料认证登录)";
                            strContents = "<HTML>" +
                                                                    "<HEAD>" +
                                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                    "</HEAD>" +
                                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 新材料情报 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 管理编号 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 委托日期 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料名&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料编号 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 供应商名称: </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                    "</BODY>" +
                                                                    "</HTML>"; //Email Contents;
                        }
                        else if (strLang.Equals("ENG"))
                        {
                            strTitle = "신규자재 품질평가(공정평가) 담당자입니다. 평가정보 등록바랍니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                            strContents = "<HTML>" +
                                                                    "<HEAD>" +
                                                                    "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                    "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                    "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                    "</HEAD>" +
                                                                    "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 신규자재정보 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                    "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                    "</BODY>" +
                                                                    "</HTML>"; //Email Contents;

                        }


                        //메일보내기
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);
                        bool bolRtn = clsmail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE") //공장코드
                                                        , dtSendUserInfo.Rows[0]["EMail"].ToString() //발신자 이메일주소
                                                        , dtSendUserInfo.Rows[0]["UserName"].ToString() // 발신자명
                                                        , dtGetUserInfo.Rows[0]["EMail"].ToString() //보내려는 사람 이메일주소
                                                        , strTitle
                                                        , strContents
                                                        , arrAttachFile //첨부파일
                                                        );
                        //이메일 발송 실패일 경우 메세지 출력
                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //공정평가 담당자에게 Mail 발송하는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000083", "M001434"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                    else
                    {
                        //발신자 이메일 주소가 공백인경우 메세지 출력
                        if (dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //의뢰자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001431"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                        //수신자 이메일 주소가 공백인경우 메세지출력
                        else if (dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //공정평가 담당자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001436"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
                else
                {
                    //발신자 정보가 없을 경우 메세지 출력
                    if (!(dtSendUserInfo.Rows.Count > 0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //의뢰자의 사용자 정보를 가져오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000056", "M001433"
                                                        , Infragistics.Win.HAlign.Right);
                        return;
                    }
                    //수신자 정보가 없는경우 메세지출력
                    else if (!(dtGetUserInfo.Rows.Count > 0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //공정평가 담당자의 사용자 정보를 가져오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000056", "M001437"
                                , Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신뢰성평가 지정 시 각 Step담당자로 지정된자 메일발송메소드
        /// 최초1회 메일링 시 담당자 변경되었을 경우 발송되지 않기 때문에 이전Step완료 저장 시 송부
        /// </summary>
        private void sendSetS4UserMail()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //메일발신인 : 의뢰자 
                string strSendUserID = this.uTextReqID.Text; //"TESTUSR";
                //메일수신인을 담을 변수
                string strGetUserID = this.uTextS4UserID.Text; //"TESTUSR";
                //string strGetUserID = this.uTextReqID.Text;

                //사용자정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                //발신인 & 수신인 정보 가져오기
                DataTable dtSendUserInfo = clsUser.mfReadSYSUser(PlantCode, strSendUserID, m_resSys.GetString("SYS_LANG"));
                DataTable dtGetUserInfo = clsUser.mfReadSYSUser(PlantCode, strGetUserID, m_resSys.GetString("SYS_LANG"));

                //발신인과 수신인정보 두정보가 다 존재하는경우 메일을보낼 수 있다.
                if (dtSendUserInfo.Rows.Count > 0 && dtGetUserInfo.Rows.Count > 0)
                {
                    //발신인의 메일주소와 수신인의 메일주소가 공백이 아닌경우 메일을 보낼 수 있다.
                    if (!dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty) && !dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strTitle = string.Empty;
                        string strContents = string.Empty;

                        if (strLang.Equals("KOR"))
                        {
                            strTitle = "신규자재 품질평가(신뢰성평가) 담당자입니다. 평가정보 등록바랍니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                            strContents = "<HTML>" +
                                            "<HEAD>" +
                                            "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                            "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                            "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                            "</HEAD>" +
                                            "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 신규자재정보 *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                            "</BODY>" +
                                            "</HTML>";   //발신 내용 ;
                        }
                        else if (strLang.Equals("CHN"))
                        {
                            strTitle = "是新材料品质评价（信赖性评价）担当.请登录评价信息.(QRP ▶ 品质检查管理 ▶ 新材料认证登录)";
                            strContents = "<HTML>" +
                                            "<HEAD>" +
                                            "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                            "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                            "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                            "</HEAD>" +
                                            "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 新材料情报 *****</SPAN></STRONG></P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 管理编号 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 委托日期 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料名&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料编号 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 供应商名称: </SPAN>" + this.uTextVendor.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                            "</BODY>" +
                                            "</HTML>";   //발신 내용 ;
                        }
                        else if (strLang.Equals("ENG"))
                        {
                            strTitle = "신규자재 품질평가(신뢰성평가) 담당자입니다. 평가정보 등록바랍니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                            strContents = "<HTML>" +
                                            "<HEAD>" +
                                            "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                            "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                            "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                            "</HEAD>" +
                                            "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 신규자재정보 *****</SPAN></STRONG></P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                            "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                            "</BODY>" +
                                            "</HTML>";   //발신 내용 ;
                        }

                        //첨부파일을 담을 변수
                        ArrayList arrAttachFile = new ArrayList();

                        //메일보내기
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);
                        bool bolRtn = clsmail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE")    //공장코드
                                                        , dtSendUserInfo.Rows[0]["EMail"].ToString()//발신자이메일주소
                                                        , dtSendUserInfo.Rows[0]["UserName"].ToString()//발신자명
                                                        , dtGetUserInfo.Rows[0]["EMail"].ToString() //보내려는 사람 이메일주소
                                                        , strTitle
                                                        , strContents
                                                        , arrAttachFile //첨부파일 
                                                        );
                        //메일 발송실패시 메세지출력
                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //신뢰성평가 담당자에게 Mail 발송하는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000083", "M001438"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }

                    }
                    else
                    {
                        //의뢰자 이메일이 공백일경우 메세지출력
                        if (dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //의뢰자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001431"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else if (dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {//발신자 이메일이 공백일경우 메세지 출력
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //신뢰성평가 담당자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001439"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
                else
                {
                    //의뢰자의 사용자정보가 없는경우 메세지 출력
                    if (!(dtSendUserInfo.Rows.Count > 0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //의뢰자의 사용자 정보를 가져오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000056", "M001433"
                                                    , Infragistics.Win.HAlign.Right);
                        return;
                    }
                    //발신자의 사용자정보가 없는경우 메세지출력
                    else if (!(dtGetUserInfo.Rows.Count > 0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //신뢰성평가 담당자의 사용자 정보를 가져오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000056", "M001440"
                                                    , Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규자재 수입검사 완료 시
        /// 메일발송조건 : 의뢰된 자재의 수입검사가 완료됐을 때
        /// 메일수신인   : 신규자재 의뢰자
        /// 첨부파일     : 검사성적서
        /// </summary>
        private void sendS2CompleteMail()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strSendUserID = this.uTextS2UserID.Text;
                string strGetUserID = this.uTextReqID.Text;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtSendUserInfo = clsUser.mfReadSYSUser(PlantCode, strSendUserID, m_resSys.GetString("SYS_LANG"));
                DataTable dtGetUserInfo = clsUser.mfReadSYSUser(PlantCode, strGetUserID, m_resSys.GetString("SYS_LANG"));

                if (dtSendUserInfo.Rows.Count > 0 && dtGetUserInfo.Rows.Count > 0)
                {
                    if (!dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty) && !dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                    {
                        //string strFileName = WriteReport();
                        
                        ArrayList arrAttachFile = new ArrayList();

                        //if (!strFileName.Equals(string.Empty))
                        //    arrAttachFile.Add(strFileName);

                        string strLang = m_resSys.GetString("SYS_LANG");
                        string strTitle = string.Empty;
                        string strContents = string.Empty;

                        if (strLang.Equals("KOR"))
                        {
                            strTitle = "의뢰하신 신규자재 수입검사가 완료되었습니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                            strContents = "<HTML>" +
                                                "<HEAD>" +
                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                "</HEAD>" +
                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 판정결과 : </SPAN>" + this.uOptionS2Result.CheckedItem.DisplayText + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                "</BODY>" +
                                                "</HTML>";
                        }
                        else if (strLang.Equals("CHN"))
                        {
                            strTitle = "您委托的新材料收入检查已结束.(QRP ▶ 品质检查管理 ▶ 新材料认证登录)";
                            strContents = "<HTML>" +
                                                "<HEAD>" +
                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                "</HEAD>" +
                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 委托情报 *****</SPAN></STRONG></P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 管理编号 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 委托日期 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料名&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 材料编号 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 供应商名称: </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 判定结果 : </SPAN>" + this.uOptionS2Result.CheckedItem.DisplayText + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                "</BODY>" +
                                                "</HTML>";
                        }
                        else if (strLang.Equals("ENG"))
                        {
                            strTitle = "의뢰하신 신규자재 수입검사가 완료되었습니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                            strContents = "<HTML>" +
                                                "<HEAD>" +
                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                "</HEAD>" +
                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** 의뢰정보 *****</SPAN></STRONG></P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 관리번호 : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 의뢰일자 : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 자재코드 : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 업체명&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. 판정결과 : </SPAN>" + this.uOptionS2Result.CheckedItem.DisplayText + "</P>" +
                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                "</BODY>" +
                                                "</HTML>";
                        }

                        //메일보내기
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);
                        bool bolRtn = clsmail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE")
                                                        , dtSendUserInfo.Rows[0]["EMail"].ToString()
                                                        , dtSendUserInfo.Rows[0]["UserName"].ToString()
                                                        , dtGetUserInfo.Rows[0]["EMail"].ToString() //보내려는 사람 이메일주소
                                                        , strTitle
                                                        , strContents
                                                        , arrAttachFile);

                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //신규자재 의뢰자에게 반려정보 Mail 발송하는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000083", "M001428."
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                    else
                    {
                        if (dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //수입검사 담당자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001441"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else if (dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //의뢰자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001430", "M001431"
                                                        , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
                else
                {
                    if (dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //수입검사 담당자의 사용자 정보를 가져오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000056", "M001442"
                                                        , Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        //의뢰자의 사용자 정보를 가져오는대 실패하였습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M000056", "M001433"
                                                        , Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공정평가 판정 시
        /// 메일발송조건 : 공정평가 Step에서 판정되었을 경우 최초 1회
        /// 메일수신인   : 수입검사 담당자 사용자 공통코드 ("QUA","U0013")
        /// </summary>
        private void sendS3CompleteMail()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 수입검사 담당자 정보 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCom);

                DataTable dtInspectUser = clsUserCom.mfReadUserCommonCode("QUA", "U0013", m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < dtInspectUser.Rows.Count; i++)
                {
                    string strSendUserID = this.uTextS3UserID.Text; //"TESTUSR";
                    string strGetUserID = dtInspectUser.Rows[i]["ComCodeName"].ToString(); //"TESTUSR";

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtSendUserInfo = clsUser.mfReadSYSUser(PlantCode, strSendUserID, m_resSys.GetString("SYS_LANG"));
                    DataTable dtGetUserInfo = clsUser.mfReadSYSUser(PlantCode, strGetUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtSendUserInfo.Rows.Count > 0 && dtGetUserInfo.Rows.Count > 0)
                    {
                        if (!dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty) && !dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            ArrayList arrAttachFile = new ArrayList();

                            string strLang = m_resSys.GetString("SYS_LANG");
                            string strTitle = string.Empty;
                            string strNameLang = string.Empty;
                            string[] strContents = new string[10];

                            if (strLang.Equals("KOR"))
                            {
                                strTitle = "공정평가 Step이 완료되었습니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                                strNameLang = "의뢰정보,관리번호,의뢰일자,자재명,자재코드,업체명&nbsp;&nbsp;&nbsp;&nbsp;,평가Step,판정결과,판정일자";
                            }
                            else if (strLang.Equals("CHN"))
                            {
                                strTitle = "工程评价Step已结束.(QRP ▶ 品质检查管理 ▶ 新材料认证登录)";
                                strNameLang = "委托情报,管理编号,委托日期,材料名,材料编号,供应商名称,评价Step,判定结果,判定日期";
                              
                            }
                            else if (strLang.Equals("ENG"))
                            {
                                strTitle = "공정평가 Step이 완료되었습니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                                strNameLang = "의뢰정보,관리번호,의뢰일자,자재명,자재코드,업체명&nbsp;&nbsp;&nbsp;&nbsp;,평가Step,판정결과,판정일자";
                            }

                            strContents = strNameLang.Split(',');

                            //메일보내기
                            brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                            QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                            brwChannel.mfCredentials(clsmail);
                            bool bolRtn = clsmail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE")
                                                            , dtSendUserInfo.Rows[0]["EMail"].ToString()
                                                            , dtSendUserInfo.Rows[0]["UserName"].ToString()
                                                            , dtGetUserInfo.Rows[0]["EMail"].ToString() //보내려는 사람 이메일주소
                                                            , strTitle
                                                            , "<HTML>" +
                                                                "<HEAD>" +
                                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                "</HEAD>" +
                                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">***** "+strContents[0]+" *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[1] + "&nbsp; : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[2] + "&nbsp; : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[3] + "&nbsp;&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[4] + "&nbsp; : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[5] + "   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[6] + " : </SPAN>" + this.uTab.Tabs["Step3"].Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[7] + "&nbsp; : </SPAN>" + this.uOptionS3Result.CheckedItem.DisplayText + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[8] + "&nbsp; : </SPAN>" + Convert.ToDateTime(this.uDateS3WriteDate.Value).ToString("yyyy-MM-dd") + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                "</BODY>" +
                                                             "</HTML>"
                                                            , arrAttachFile);

                            if (!bolRtn)
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                //수입검사 담당자에게 Mail 발송하는대 실패하였습니다."
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M000083", "M001443"
                                                            , Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                        else
                        {
                            if (dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                //공정평가 담당자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001430", "M001436"
                                                            , Infragistics.Win.HAlign.Right);
                                return;
                            }
                            else if (dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                //수입검사 담당자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M001430", "M001441"
                                                            , Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                    else
                    {
                        if (!(dtSendUserInfo.Rows.Count > 0))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //공정평가 담당자의 사용자 정보를 가져오는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M000056", "M001437"
                                                            , Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else if (!(dtGetUserInfo.Rows.Count > 0))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //수입검사 담당자의 사용자 정보를 가져오는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M000056", "M001442"
                                                            , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신뢰성평가 판정 시
        /// 메일발송조건 : 신뢰성평가 Step에서 판정되었을 경우 최초 1회
        /// 메일수신인   : 수입검사 담당자 사용자 공통코드 ("QUA","U0013")
        /// </summary>
        private void sendS4CompleteMail()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 수입검사 담당자 정보 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserCom);

                DataTable dtInspectUser = clsUserCom.mfReadUserCommonCode("QUA", "U0013", m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < dtInspectUser.Rows.Count; i++)
                {
                    string strSendUserID = this.uTextS4UserID.Text; //"TESTUSR";
                    string strGetUserID = dtInspectUser.Rows[i]["ComCodeName"].ToString(); //"TESTUSR";

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtSendUserInfo = clsUser.mfReadSYSUser(PlantCode, strSendUserID, m_resSys.GetString("SYS_LANG"));
                    DataTable dtGetUserInfo = clsUser.mfReadSYSUser(PlantCode, strGetUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtSendUserInfo.Rows.Count > 0 && dtGetUserInfo.Rows.Count > 0)
                    {
                        if (!dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty) && !dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                        {
                            ArrayList arrAttachFile = new ArrayList();

                            string strLang = m_resSys.GetString("SYS_LANG");
                            string strTitle = string.Empty;
                            string strNameLang = string.Empty;
                            string[] strContents = new string[10];

                            if (strLang.Equals("KOR"))
                            {
                                strTitle = "신뢰성평가 Step이 완료되었습니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                                strNameLang = "의뢰정보,관리번호,의뢰일자,자재명,자재코드,업체명&nbsp;&nbsp;&nbsp;&nbsp;,평가Step,판정결과,판정일자";;
                            }
                            else if (strLang.Equals("CHN"))
                            {
                                strTitle = "信赖性评价Step已结束.(QRP ▶ 品质检查管理 ▶ 新材料认证登录)";
                                strNameLang = "委托情报,管理编号,委托日期,材料名,材料编号,供应商名称,评价Step,判定结果,判定日期";
                            }
                            else if (strLang.Equals("ENG"))
                            {
                                strTitle = "신뢰성평가 Step이 완료되었습니다.(QRP ▶ 품질검사관리 ▶ 신규자재인증등록)";
                                strNameLang = "委托情报,管理编号,委托日期,材料名,材料编号,供应商名称,评价Step,判定结果,判定日期";
                            }

                            strContents = strNameLang.Split(',');

                            //메일보내기
                            brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                            QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                            brwChannel.mfCredentials(clsmail);
                            bool bolRtn = clsmail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE")
                                                            , dtSendUserInfo.Rows[0]["EMail"].ToString()
                                                            , dtSendUserInfo.Rows[0]["UserName"].ToString()
                                                            , dtGetUserInfo.Rows[0]["EMail"].ToString() //보내려는 사람 이메일주소
                                                            , strTitle
                                                            , "<HTML>" +
                                                                "<HEAD>" +
                                                                "<META content=\"text/html; charset=ks_c_5601-1987\" http-equiv=Content-Type>" +
                                                                "<META name=GENERATOR content=\"TAGFREE Active Designer v3.0\">" +
                                                                "<LINK rel=stylesheet href=\"http://image.bokwang.com/TagFree/tagfree.css\">" +
                                                                "</HEAD>" +
                                                                "<BODY style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">*****"+ strContents[0] +" *****</SPAN></STRONG></P>" +
                                //"<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[1] + "&nbsp; : </SPAN>" + this.uTextStdNumber.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[2] + "&nbsp; : </SPAN>" + this.uTextReqDate.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[3] + "&nbsp;&nbsp;&nbsp;&nbsp;   : </SPAN>" + this.uTextMaterialName.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[4] + "&nbsp; : </SPAN>" + this.uTextMaterialCode.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[5] + "   : </SPAN>" + this.uTextVendor.Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[6] + " : </SPAN>" + this.uTab.Tabs["Step4"].Text + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[7] + "&nbsp; : </SPAN>" + this.uOptionS4Result.CheckedItem.DisplayText + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">+. " + strContents[8] + "&nbsp; : </SPAN>" + Convert.ToDateTime(this.uDateS4WriteDate.Value).ToString("yyyy-MM-dd") + "</P>" +
                                                                "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\">&nbsp;</P>" +
                                                                "</BODY>" +
                                                              "</HTML>"
                                                            , arrAttachFile);

                            if (!bolRtn)
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                //수입검사 담당자에게 Mail 발송하는대 실패하였습니다.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000083", "M001443",
                                        Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                        else
                        {
                            if (dtSendUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                //신뢰성평가 담당자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001430"
                                        , "M001439"
                                        , Infragistics.Win.HAlign.Right);
                                return;
                            }
                            else if (dtGetUserInfo.Rows[0]["EMail"].ToString().Equals(string.Empty))
                            {
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();
                                //수입검사 담당자의 E-Mail 정보가 없어 메일을 전송할 수 없습니다.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001430"
                                        , "M001441"
                                        , Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                    else
                    {
                        if (!(dtSendUserInfo.Rows.Count > 0))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //신뢰성평가 담당자의 사용자 정보를 가져오는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000056"
                                    , "M001440"
                                    , Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else if (!(dtGetUserInfo.Rows.Count > 0))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = new DialogResult();
                            //수입검사 담당자의 사용자 정보를 가져오는대 실패하였습니다.
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000056"
                                    , "M001442"
                                    , Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }

        #region 수입검사 성적서

        private string WriteReport()
        {
            try
            {

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();

                #region 디렉토리 생성 및 변수선언
                //원자재 불량 파일 디렉토리 
                string strBrowserPath = Application.ExecutablePath;
                int intPos = strBrowserPath.LastIndexOf(@"\");
                //string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "InspectReport\\";
                string m_strQRPBrowserActive = strBrowserPath.Substring(0, intPos + 1) + "GWpdf\\";
                
                ////디렉토리 유무확인
                //DirectoryInfo di = new DirectoryInfo(m_strQRPBrowserPath);

                ////지정된경로에 폴더가 없을 경우 폴더생성
                //if (di.Exists.Equals(false))
                //{
                //    di.Create();
                //}
                PlantCode = "2210";
                string strReqNo = "SMI1301";//this.uTextReqNo.Text;
                string strReqSeq = "0335";// this.uTextReqSeq.Text;
                string strFileName = string.Empty;    // 파일명
                ArrayList arrFile = new ArrayList();    //첨부 파일 경로 정보
                ArrayList arrFileName = new ArrayList(); //메일 첨부파일 경로에 업로드한 파일명
                QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                #endregion

                #region 수입검사성적서

                DataTable dtHeader = RtnReport_Header(PlantCode,strReqNo,strReqSeq,m_resSys.GetString("SYS_LANG"));
                DataTable dtResult = RtnReport_Result(PlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
                DataTable dtData = RtnReport_Data(PlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));
             

                frmINSReport frmReport = new frmINSReport(dtHeader, dtResult, dtData);

                //ActiveReport 완료여부
                if (frmReport.FileChk != null && !frmReport.FileChk.Equals(string.Empty) 
                    && File.Exists(m_strQRPBrowserActive + frmReport.FileChk))
                {
                    arrFile.Add(m_strQRPBrowserActive + frmReport.FileChk);

                    arrFileName.Add(frmReport.FileChk);
                }
                
                

                #endregion

                #region 저장된 파일 이메일 발송 첨부파일로 업로드

                if (arrFile.Count > 0)
                {
                    //첨부파일을 위한 FileServer 연결정보
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //메일 발송 첨부파일 경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFileDestPath);
                    DataTable dtFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(PlantCode, "D0022");

                    //메일에 첨부할 파일을 서버에서 검색, 메일 첨부파일 폴더로 Upload
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }

                if (arrFile.Count != 0)
                {
                    fileAtt.mfFileUpload_NonAssync();
                }

                #endregion



                return strFileName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }


        /// <summary>
        /// Report Header 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        /// <returns></returns>
        private DataTable RtnReport_Header(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                QRPINS.BL.INSIMP.MatInspectReqH clsH = new QRPINS.BL.INSIMP.MatInspectReqH();
                brwChannel.mfCredentials(clsH);

                dtRtn = clsH.mfReadInsMatInspectReq_Report_Header(strPlantCode, strReqNo, strReqSeq, strLang);

                if (dtRtn.Rows.Count > 0)
                {
                    // LotTable 조회
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                    QRPINS.BL.INSIMP.MatInspectReqLot clsLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                    brwChannel.mfCredentials(clsLot);

                    DataTable dtLot = clsLot.mfReadINSMatInspectReqLot(strPlantCode, strReqNo, strReqSeq);

                    // 입고 Lot정보
                    if (dtLot.Rows.Count > 0)
                    {
                        string strGRLotNo = string.Empty;
                        string strInspectLot = string.Empty;

                        for (int i = 0; i < dtLot.Rows.Count; i++)
                        {
                            strGRLotNo = strGRLotNo.Equals(string.Empty) ? dtLot.Rows[i]["LotNo"].ToString() : strGRLotNo + ", " + dtLot.Rows[i]["LotNo"].ToString();

                            if (dtLot.Rows[i]["SamplingLotFlag"].ToString() == "T")
                                strInspectLot = strInspectLot.Equals(string.Empty) ? dtLot.Rows[i]["LotNo"].ToString() : strInspectLot + ", " + dtLot.Rows[i]["LotNo"].ToString();
                        }

                        // Lot 수량
                        dtRtn.Rows[0]["LotQty"] = dtLot.DefaultView.ToTable(true, "MoLotNo").Rows.Count;
                        // LotNo
                        dtRtn.Rows[0]["GRLotNo"] = strGRLotNo;
                        // 검사 LotNo
                        dtRtn.Rows[0]["InspectLotNo"] = strInspectLot;



                    }

                    // 불량정보 (불량Lot : 불량내용)
                    DataTable dtAbnormal = RtnReport_Abnormal(strPlantCode, strReqNo, strReqSeq, strLang);
                    
                    if (dtAbnormal.Rows.Count > 0)
                    {
                        string strAbnormal = string.Empty;
                        for (int i = 0; i < dtAbnormal.Rows.Count; i++)
                            strAbnormal = strAbnormal.Equals(string.Empty) ? dtAbnormal.Rows[i]["InspectFault"].ToString() : strAbnormal + ", " + dtAbnormal.Rows[i]["InspectFault"].ToString();

                        dtRtn.Rows[0]["AbnormalLotNo"] = strAbnormal;
                    }
                    else
                        dtRtn.Rows[0]["AbnormalLotNo"] = "-";

                    dtLot.Dispose();
                    dtAbnormal.Dispose();
                }
               

                clsH.Dispose();

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Report 불량 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private DataTable RtnReport_Abnormal(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            DataTable dtRtn = new DataTable();
            try
            {

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                QRPINS.BL.INSIMP.MatInspectReqH clsH = new QRPINS.BL.INSIMP.MatInspectReqH();
                brwChannel.mfCredentials(clsH);

                dtRtn = clsH.mfReadInsMatInspectReq_Report_Abnormal(strPlantCode, strReqNo, strReqSeq, strLang);

                clsH.Dispose();

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }
        
        
        /// <summary>
        /// Report 결과 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private DataTable RtnReport_Result(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            DataTable dtRtn = new DataTable();
            try
            {

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                QRPINS.BL.INSIMP.MatInspectReqH clsH = new QRPINS.BL.INSIMP.MatInspectReqH();
                brwChannel.mfCredentials(clsH);

                dtRtn = clsH.mfReadInsMatInspectReq_Report_Result(strPlantCode, strReqNo, strReqSeq, strLang);

                clsH.Dispose();

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Report 검사DATA 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private DataTable RtnReport_Data(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            DataTable dtRtn = new DataTable();
            try
            {

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                QRPINS.BL.INSIMP.MatInspectReqH clsH = new QRPINS.BL.INSIMP.MatInspectReqH();
                brwChannel.mfCredentials(clsH);

                dtRtn = clsH.mfReadInsMatInspectReq_Report_Data(strPlantCode, strReqNo, strReqSeq, strLang);

                clsH.Dispose();

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }


        #endregion

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            string _str = WriteReport();
        }

        
    }
}
