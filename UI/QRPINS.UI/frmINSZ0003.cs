﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINSZ0003.cs                                        */
/* 프로그램명   : 자재입고조회(양산품)                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-11                                            */
/* 수정이력     : 2011-08-23 : 기능 추가 (이종호)                       */
/*                xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0003 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmINSZ0003()
        {
            InitializeComponent();
        }

        private void frmINSZ0003_Activated(object sender, EventArgs e)
        {
            // 툴바 활성화 지정
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0003_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("자재입고조회(양산품)", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitTextBox();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitTextBox()
        {
            try
            {
                // Date 설정
                this.uDateSearchGRFromDate.Value = DateTime.Now.AddDays(-7);
                this.uDateSearchGRToDate.Value = DateTime.Now;

                this.uTextSearchGRNo.MaxLength = 20;
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextSearchMaterialCode.MaxLength = 20;

                this.uTextSearchMaterialCode.AlwaysInEditMode = true;
                this.uTextSearchVendorCode.AlwaysInEditMode = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchGRNo, "입고번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchGRDate, "입고일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelSearchConsumableType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchGRFlag, "입고확인포함", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchPlant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // 검색조건 : 자재종류 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsum = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsConsum);

                DataTable dtConsum = clsConsum.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchConsumableType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ConsumableTypeCode", "ConsumableTypeName", dtConsum);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridINSMaterialGR, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MaterialGrade", "검사등급", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "ShipmentCount", "ShipCount", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MoLotNo", "모LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "GRQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn", "0");

                //this.uGridINSMaterialGR.DisplayLayout.Bands[0].Columns["GRQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "TOTGRQty", "총입고수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 110, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnn,nnn,nnn,nnn,nnn", "0");

                this.uGridINSMaterialGR.DisplayLayout.Bands[0].Columns["TOTGRQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "GRFlag", "입고확인", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "ReqDateTime", "요청일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MoldSeq", "금형차수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "SpecNo", "Rev", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "FloorPlanNo", "도면번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MaterialGroupName", "그룹", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 200
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MaterialType", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "MaterialSpec1", "규격1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //////wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "검사순서", "검사순서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "ManufactureDate", "제조일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                //wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "ExpirationDate", "유효기간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                //wGrid.mfSetGridColumn(this.uGridINSMaterialGR, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 500
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridINSMaterialGR.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridINSMaterialGR.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // 헤더 체크박스 없애기
                this.uGridINSMaterialGR.DisplayLayout.Bands[0].Columns["GRFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialGR), "MaterialGR");
                QRPINS.BL.INSIMP.MaterialGR clsGR = new QRPINS.BL.INSIMP.MaterialGR();
                brwChannel.mfCredentials(clsGR);

                // 검색조건용 DataTable Column 설정
                DataTable dtSearch = clsGR.mfSetSearchDataInfo();

                DataRow drRow = dtSearch.NewRow();
                drRow["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                drRow["GRNo"] = this.uTextSearchGRNo.Text;
                drRow["LotNo"] = this.uTextSearchLotNo.Text;
                drRow["VendorCode"] = this.uTextSearchVendorCode.Text;
                drRow["MaterialCode"] = this.uTextSearchMaterialCode.Text;
                drRow["GRFromDate"] = Convert.ToDateTime(this.uDateSearchGRFromDate.Value).ToString("yyyy-MM-dd");
                drRow["GRToDate"] = Convert.ToDateTime(this.uDateSearchGRToDate.Value).ToString("yyyy-MM-dd");
                drRow["ConsumableTypeCode"] = this.uComboSearchConsumableType.Value.ToString();
                if (this.uCheckSearchGRFlag.Checked == true)
                {
                    drRow["GRFlag"] = "T";
                }
                else
                {
                    drRow["GRFlag"] = "F";
                }
                dtSearch.Rows.Add(drRow);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // Method 호출
                DataTable dtRtnGR = clsGR.mfReadINSMaterialGRMassProduct(dtSearch, m_resSys.GetString("SYS_LANG"));

                // DataBinding
                this.uGridINSMaterialGR.DataSource = dtRtnGR;
                this.uGridINSMaterialGR.DataBind();

                for (int i = 0; i < this.uGridINSMaterialGR.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridINSMaterialGR.Rows[i].Cells["GRFlag"].Value))
                    {
                        this.uGridINSMaterialGR.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    }
                }

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtnGR.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    for (int i = 0; i < this.uGridINSMaterialGR.Rows.Count; i++)
                    {
                        if (this.uGridINSMaterialGR.Rows[i].Cells["MaterialGrade"].Value.ToString() == "")
                        {
                            this.uGridINSMaterialGR.Rows[i].Cells["ShipmentCount"].Value = DBNull.Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                if (this.uGridINSMaterialGR.Rows.Count > 0)
                {
                    // SystemInfo 리소스
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    QRPBrowser brwChannel = new QRPBrowser();
                    bool bolCheckAML = false;

                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialGR), "MaterialGR");
                    QRPINS.BL.INSIMP.MaterialGR clsMaterialGR = new QRPINS.BL.INSIMP.MaterialGR();
                    brwChannel.mfCredentials(clsMaterialGR);

                    DataTable dtMaterialGR = clsMaterialGR.mfSetDataInfo();
                    DataRow drRow;

                    this.uGridINSMaterialGR.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    for (int i = 0; i < this.uGridINSMaterialGR.Rows.Count; i++)
                    {
                        if (this.uGridINSMaterialGR.Rows[i].RowSelectorAppearance.Image != null && this.uGridINSMaterialGR.Rows[i].Cells["GRFlag"].Value.Equals("True"))
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");

                            if (this.uGridINSMaterialGR.Rows[i].Cells["SpecNo"].Value.ToString() == "")
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                    , (i + 1).ToString() + msg.GetMessge_Text("M000006", strLang)
                                    , Infragistics.Win.HAlign.Right);
                                // Focus
                                this.uGridINSMaterialGR.ActiveCell = this.uGridINSMaterialGR.Rows[i].Cells["SpecNo"];
                                this.uGridINSMaterialGR.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            //SC
                            if (this.uGridINSMaterialGR.Rows[i].Cells["SpecNo"].Value.ToString().Length != 3)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                    , (i + 1).ToString() + "行的SpecNo必须是三位数" + msg.GetMessge_Text("M001564", strLang)
                                    , Infragistics.Win.HAlign.Right);
                                // Focus
                                this.uGridINSMaterialGR.ActiveCell = this.uGridINSMaterialGR.Rows[i].Cells["SpecNo"];
                                this.uGridINSMaterialGR.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            if (this.uGridINSMaterialGR.Rows[i].Cells["MoldSeq"].Value.ToString() == "")
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                    , (i + 1).ToString() + msg.GetMessge_Text("M000007", strLang)
                                    , Infragistics.Win.HAlign.Right);
                                // Focus
                                this.uGridINSMaterialGR.ActiveCell = this.uGridINSMaterialGR.Rows[i].Cells["MoldSeq"];
                                this.uGridINSMaterialGR.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            drRow = dtMaterialGR.NewRow();

                            drRow["PlantCode"] = this.uGridINSMaterialGR.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["VendorCode"] = this.uGridINSMaterialGR.Rows[i].Cells["VendorCode"].Value.ToString();
                            drRow["MaterialCode"] = this.uGridINSMaterialGR.Rows[i].Cells["MaterialCode"].Value.ToString();
                            drRow["GRNo"] = this.uGridINSMaterialGR.Rows[i].Cells["GRNo"].Value.ToString();
                            drRow["MoLotNo"] = this.uGridINSMaterialGR.Rows[i].Cells["MoLotNo"].Value.ToString();
                            //drRow["LotNo"] = this.uGridINSMaterialGR.Rows[i].Cells["LotNo"].Value.ToString();
                            drRow["SpecNo"] = this.uGridINSMaterialGR.Rows[i].Cells["SpecNo"].Value.ToString();
                            drRow["MoldSeq"] = this.uGridINSMaterialGR.Rows[i].Cells["MoldSeq"].Value.ToString();
                            if (this.uGridINSMaterialGR.Rows[i].Cells["GRFlag"].Value.Equals("True"))
                                drRow["GRFlag"] = "T";
                            else
                                drRow["GRFlag"] = "F";

                            dtMaterialGR.Rows.Add(drRow);
                        }
                    }

                    DataTable dtAMLCheck = dtMaterialGR.DefaultView.ToTable(true, "PlantCode", "VendorCode", "MaterialCode", "GRNo", "MoldSeq", "SpecNo");
                    for (int i = 0; i < dtAMLCheck.Rows.Count; i++)
                    {
                        string strPlantCode = dtAMLCheck.Rows[i]["PlantCode"].ToString();
                        string strVendorCode = dtAMLCheck.Rows[i]["VendorCode"].ToString();
                        string strMaterialCode = dtAMLCheck.Rows[i]["MaterialCode"].ToString();
                        string strMoldSeq = dtAMLCheck.Rows[i]["MoldSeq"].ToString();

                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AMLHistory), "AMLHistory");
                        QRPINS.BL.INSIMP.AMLHistory clsHistory = new QRPINS.BL.INSIMP.AMLHistory();
                        brwChannel.mfCredentials(clsHistory);

                        DataTable dtRtn = clsHistory.mfReadAMLHistory(strPlantCode, strMaterialCode, strVendorCode, strMoldSeq, m_resSys.GetString("SYS_LANG"));

                        if (!(dtRtn.Rows.Count > 0))
                        {
                            bolCheckAML = true;
                            ////Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////    , "M000880", "M000880", "M001400", Infragistics.Win.HAlign.Right);

                            ////if (Result == DialogResult.No)
                            ////{
                            ////    bolCheckAML = false;
                            ////    return;
                            ////}
                        }
                        else if (dtRtn.Rows[dtRtn.Rows.Count - 1]["MaterialGradeCode"].ToString().Equals("1") && bolCheckAML.Equals(false))
                        {
                            bolCheckAML = true;
                            ////Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            ////    , "M000880", "M000880", "M001400", Infragistics.Win.HAlign.Right);

                            ////if (Result == DialogResult.No)
                            ////{
                            ////    bolCheckAML = false;
                            ////    return;
                            ////}
                        }
                    }

                    if (bolCheckAML)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M000880", "M000880", "M001400", Infragistics.Win.HAlign.Right);

                        if (Result == DialogResult.No)
                        {
                            return;
                        }
                    }

                    if (dtMaterialGR.Rows.Count > 0)
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string strErrRtn = clsMaterialGR.mfSaveINSMaterialGR_IMSI(dtMaterialGR, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            // 결과검사
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            DialogResult DResult = new DialogResult();

                            if (ErrRtn.ErrNum == 0)
                            {
                                // 신규인증자재가 있으면 메일발송 메소드 호출
                                if (ErrRtn.InterfaceResultMessage.Equals("N"))
                                {
                                    SendMail(m_resSys.GetString("SYS_PLANTCODE"));
                                    //SendMail(dtMaterialGR.Rows[0]["PlantCode"].ToString());
                                }
                                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                                mfSearch();
                            }
                            else
                            {
                                if (ErrRtn.ErrMessage.Equals(string.Empty))
                                {
                                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                        , ErrRtn.ErrMessage,
                                                        Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                    else
                    {
                        DialogResult DResult = new DialogResult();
                        DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001032", "M001047", Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                QRPBrowser brwChannel = new QRPBrowser();

                // 필수 입력사항 확인
                // 필수입력사항 확인
                if (this.uComboSearchPlant.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000629", "M000278",
                               Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (this.uTextSearchGRNo.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000629", "M001560",
                                Infragistics.Win.HAlign.Right);

                    this.uTextSearchGRNo.Focus();
                    return;
                }
                else if (this.uCheckSearchGRFlag.Checked == true)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000629", "M001561",
                               Infragistics.Win.HAlign.Right);

                    this.uTextSearchGRNo.Focus();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        // BL 연결
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialGR), "MaterialGR");
                        QRPINS.BL.INSIMP.MaterialGR clsDelMaterialGR = new QRPINS.BL.INSIMP.MaterialGR();
                        brwChannel.mfCredentials(clsDelMaterialGR);

                        // 삭제 Method의 매개변수 설정
                        //string strPlantCode = this.uComboSearchPlant.Value.ToString();
                        //string strGRNo = this.uTextSearchGRNo.Text;
                        //string strMaterialCode = this.uTextSearchMaterialCode.Text;
                        //string strConsumableType = this.uComboSearchConsumableType.Value.ToString();
                        //string strLotNo = this.uTextSearchLotNo.Text;
                        //string struVendorCode = this.uTextSearchVendorCode.Text;
                        //string struGRFromDate = this.uDateSearchGRFromDate.Value.ToString();
                        //string strGRToDate = this.uDateSearchGRToDate.Value.ToString();

                        // 검색조건용 DataTable Column 설정
                        DataTable dtSearch = clsDelMaterialGR.mfSetSearchDataInfo();

                        DataRow drRow = dtSearch.NewRow();
                        drRow["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                        drRow["GRNo"] = this.uTextSearchGRNo.Text;
                        drRow["LotNo"] = this.uTextSearchLotNo.Text;
                        drRow["VendorCode"] = this.uTextSearchVendorCode.Text;
                        drRow["MaterialCode"] = this.uTextSearchMaterialCode.Text;
                        drRow["GRFromDate"] = Convert.ToDateTime(this.uDateSearchGRFromDate.Value).ToString("yyyy-MM-dd");
                        drRow["GRToDate"] = Convert.ToDateTime(this.uDateSearchGRToDate.Value).ToString("yyyy-MM-dd");
                        drRow["ConsumableTypeCode"] = this.uComboSearchConsumableType.Value.ToString();
                        if (this.uCheckSearchGRFlag.Checked == true)
                        {
                            drRow["GRFlag"] = "T";
                        }
                        else
                        {
                            drRow["GRFlag"] = "F";
                        }
                        dtSearch.Rows.Add(drRow);

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsDelMaterialGR.mfDeleteINSMaterialGRMassProduct(dtSearch);
                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000638", strLang)
                                                            , ErrRtn.ErrMessage,
                                                            Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀 출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridINSMaterialGR.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridINSMaterialGR);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M000803", "M000809", "M000801", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method...
        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 거래처 정보 조회 함수
        /// </summary>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        private DataTable GetVendorInfo(String strVendorCode)
        {
            DataTable dtVendor = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

                return dtVendor;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtVendor;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private DataTable GetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUserInfo = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUserInfo = clsUser.mfReadSYSUser("", strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUserInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUserInfo;
            }
            finally
            {
            }
        }

        /// <summary>
        /// AML 이력정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        private void Search_AMLHistory(string strPlantCode, string strVendorCode, string strMaterialCode, string strMoldSeq)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AMLHistory), "AMLHistory");
                QRPINS.BL.INSIMP.AMLHistory clsHistory = new QRPINS.BL.INSIMP.AMLHistory();
                brwChannel.mfCredentials(clsHistory);

                DataTable dtRtn = clsHistory.mfReadAMLHistory(strPlantCode, strMaterialCode, strVendorCode, strMoldSeq, m_resSys.GetString("SYS_LANG"));

                if (dtRtn.Rows.Count == 0)
                {
                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M000880", "M000880", "M000028", Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region Mail 전송시 메소드
        private void SendMail(string strPlantCode)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                System.Collections.ArrayList arrAttachFile = new System.Collections.ArrayList();

                // 자재창고 담당자 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserComCode);

                DataTable dt = clsUserComCode.mfReadUserCommonCodeMMType("QUA", "U0012", "1", m_resSys.GetString("SYS_LANG"));

                if (dt.Rows.Count > 0)
                {
                    // Mail 받을사람의 정보 가져오기
                    DataTable dtUserInfo = GetUserInfo(strPlantCode, dt.Rows[0]["ComCodeName"].ToString());

                    if (dtUserInfo.Rows.Count > 0)
                    {
                        //메일보내기
                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsmail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsmail);
                        bool bolRtn = clsmail.mfSendSMTPMail(strPlantCode
                                                        , m_resSys.GetString("SYS_EMAIL")
                                                        , m_resSys.GetString("SYS_USERNAME")
                                                        , dtUserInfo.Rows[0]["EMail"].ToString()//보내려는 사람 이메일주소
                                                        , "[QRP].신규자재가 입고되었습니다. 확인 부탁드립니다."
                                                        , "[QRP].신규자재가 입고되었습니다. 신규자재 인증의뢰 화면에서 확인 부탁드립니다."
                            //+ dtUserInfo.Rows[0]["UserName"].ToString() + "님 좋은하루 되십시오."
                                                        , arrAttachFile);

                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult DReulst = new DialogResult();
                            string strLang = m_resSys.GetString("SYS_LANG");
                            DReulst = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M000083", strLang), msg.GetMessge_Text("M000309", strLang)
                                    , msg.GetMessge_Text("M000308", strLang) + dtUserInfo.Rows[0]["UserName"].ToString() + msg.GetMessge_Text("M000005", strLang),
                                    Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Event
        // 검색조건 거래처코드 텍스트박스 키다운 이벤트
        private void uTextSearchVendorCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchVendorCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strVendorCode = this.uTextSearchVendorCode.Text;

                        // UserName 검색 함수 호출
                        DataTable dtVendor = GetVendorInfo(strVendorCode);

                        if (dtVendor.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtVendor.Rows.Count; i++)
                            {
                                this.uTextSearchVendorName.Text = dtVendor.Rows[i]["VendorName"].ToString();
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000168",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextSearchVendorCode.Text = "";
                            this.uTextSearchVendorName.Text = "";
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchVendorCode.TextLength <= 1 || this.uTextSearchVendorCode.SelectedText == this.uTextSearchVendorCode.Text)
                    {
                        this.uTextSearchVendorName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재코드 텍스트박스 키다운 이벤트
        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.SelectedText == this.uTextSearchMaterialCode.Text)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 거래처코드 에딧버튼 클릭시 팝업창 띄워주는 이벤트
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재코드 에딧버튼 클릭시 팝업창 띄워주는 이벤트
        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 행 더블클릭시 수입검사등록 화면창 띄우기
        private void uGridINSMaterialGR_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialGR), "MaterialGR");
                QRPINS.BL.INSIMP.MaterialGR clsGR = new QRPINS.BL.INSIMP.MaterialGR();
                brwChannel.mfCredentials(clsGR);

                DataTable dtCheck = clsGR.mfReadINSmaterialGR_Check(e.Row.Cells["PlantCode"].Value.ToString()
                                                                    , e.Row.Cells["GRNo"].Value.ToString()
                                                                    , e.Row.Cells["MoLotNo"].Value.ToString()
                                                                    , m_resSys.GetString("SYS_LANG"));

                if (dtCheck.Rows.Count > 0)
                {
                    if (dtCheck.Rows[0]["GRFlag"].ToString().Equals("T"))
                    {
                        if (dtCheck.Rows[0]["InspectFlag"].ToString().Equals("F"))
                        {
                            frmINS0004 frmINS = new frmINS0004();

                            // 폼에 전달할 값 설정
                            frmINS.PlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                            frmINS.MaterialCode = e.Row.Cells["MaterialCode"].Value.ToString();
                            frmINS.VendorCode = e.Row.Cells["VendorCode"].Value.ToString();
                            frmINS.GRDate = e.Row.Cells["GRDate"].Value.ToString();
                            frmINS.GRNo = e.Row.Cells["GRNo"].Value.ToString();
                            frmINS.SpecNo = e.Row.Cells["SpecNo"].Value.ToString();
                            frmINS.MoldSeq = e.Row.Cells["MoldSeq"].Value.ToString();
                            frmINS.FloorPlan = e.Row.Cells["FloorPlanNo"].Value.ToString();
                            frmINS.ReceipTime = DateTime.Now.ToString("HH:mm:ss");
                            frmINS.MoveFormName = this.Name;

                            CommonControl cControl = new CommonControl();
                            Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                            Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                            //해당화면의 탭이 있는경우 해당 탭 닫기
                            if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0004"))
                            {
                                uTabMenu.Tabs["QRPINS" + "," + "frmINS0004"].Close();
                            }
                            //탭에 추가
                            uTabMenu.Tabs.Add("QRPINS" + "," + "frmINS0004", "수입검사등록");
                            uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINS0004"];

                            //호출시 화면 속성 설정
                            frmINS.AutoScroll = true;
                            frmINS.MdiParent = this.MdiParent;
                            frmINS.ControlBox = false;
                            frmINS.Dock = DockStyle.Fill;
                            frmINS.FormBorderStyle = FormBorderStyle.None;
                            frmINS.WindowState = FormWindowState.Normal;
                            frmINS.Text = "수입검사등록";
                            frmINS.Show();
                        }
                        else
                        {
                            DialogResult Result = new DialogResult();
                            WinMessageBox msg = new WinMessageBox();
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001135", "M001135", "M001401", Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        DialogResult Result = new DialogResult();
                        WinMessageBox msg = new WinMessageBox();
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001135", "M001135", "M000871", Infragistics.Win.HAlign.Right);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINSZ0003_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uGridINSMaterialGR_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정시 RowSelector 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uGridINSMaterialGR_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                this.uGridINSMaterialGR.EventManager.AllEventsEnabled = false;

                // 입력한 셀의 값을 해당하는 모든 Ship에 대해서 동일하게 적용한다.
                if (!e.Cell.OriginalValue.Equals(e.Cell.Value))
                {
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strGRDate = e.Cell.Row.Cells["GRDate"].Value.ToString();
                    string strMaterialCode = e.Cell.Row.Cells["MaterialCode"].Value.ToString();
                    string strVendorCode = e.Cell.Row.Cells["VendorCode"].Value.ToString();
                    string strGrNo = e.Cell.Row.Cells["GrNo"].Value.ToString();
                    string strMoldSeq = e.Cell.Row.Cells["MoldSeq"].Value.ToString();
                    string strMoLotNo = e.Cell.Row.Cells["MoLotNo"].Value.ToString();

                    // 입고확인 Flag 선택시
                    if (e.Cell.Column.Key == "GRFlag")
                    {
                        ////// AML History 정보에 입력한 정보가 있는지 확인 후 메세지 창을 보여줌.                        
                        ////Search_AMLHistory(strPlantCode, strVendorCode, strMaterialCode, strMoldSeq);

                        for (int i = 0; i < this.uGridINSMaterialGR.Rows.Count; i++)
                        {
                            if (this.uGridINSMaterialGR.Rows[i].Cells["GRDate"].Value.ToString() == strGRDate &&
                               this.uGridINSMaterialGR.Rows[i].Cells["MaterialCode"].Value.ToString() == strMaterialCode &&
                               this.uGridINSMaterialGR.Rows[i].Cells["VendorCode"].Value.ToString() == strVendorCode &&
                               this.uGridINSMaterialGR.Rows[i].Cells["GrNo"].Value.ToString() == strGrNo)// &&
                            //this.uGridINSMaterialGR.Rows[i].Cells["MoLotNo"].Value.ToString() == strMoLotNo)
                            {
                                // 셀 수정시 RowSelector 이미지 변화
                                QRPGlobal grdImg = new QRPGlobal();
                                this.uGridINSMaterialGR.Rows[i].RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                                this.uGridINSMaterialGR.Rows[i].Cells["GRFlag"].Value = e.Cell.Row.Cells["GRFlag"].Value;
                            }
                        }
                    }
                    //// 금형차수 입력시
                    else if (e.Cell.Column.Key == "MoldSeq")
                    {
                        // 그리드 이벤트 해제
                        //this.uGridINSMaterialGR.EventManager.AllEventsEnabled = false;

                        // AML History 정보에 입력한 정보가 있는지 확인 후 메세지 창을 보여줌.                        
                        //Search_AMLHistory(strPlantCode, strVendorCode, strMaterialCode, strMoldSeq);

                        for (int i = 0; i < this.uGridINSMaterialGR.Rows.Count; i++)
                        {
                            if (this.uGridINSMaterialGR.Rows[i].Cells["GRDate"].Value.ToString() == strGRDate &&
                               this.uGridINSMaterialGR.Rows[i].Cells["MaterialCode"].Value.ToString() == strMaterialCode &&
                               this.uGridINSMaterialGR.Rows[i].Cells["VendorCode"].Value.ToString() == strVendorCode &&
                               this.uGridINSMaterialGR.Rows[i].Cells["GrNo"].Value.ToString() == strGrNo)
                            {
                                // 셀 수정시 RowSelector 이미지 변화   
                                QRPGlobal grdImg = new QRPGlobal();
                                this.uGridINSMaterialGR.Rows[i].RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                                this.uGridINSMaterialGR.Rows[i].Cells["MoldSeq"].Value = e.Cell.Row.Cells["MoldSeq"].Value;
                            }
                        }
                        // 그리드 이벤트 등록
                        //this.uGridINSMaterialGR.EventManager.AllEventsEnabled = true;
                    }
                    // SpecNo(Rev) 입력시
                    else if (e.Cell.Column.Key == "SpecNo")
                    {
                        ////// AML History 정보에 입력한 정보가 있는지 확인 후 메세지 창을 보여줌.                        
                        ////Search_AMLHistory(strPlantCode, strVendorCode, strMaterialCode, strMoldSeq);
                        for (int i = 0; i < this.uGridINSMaterialGR.Rows.Count; i++)
                        {
                            if (this.uGridINSMaterialGR.Rows[i].Cells["GRDate"].Value.ToString() == strGRDate &&
                               this.uGridINSMaterialGR.Rows[i].Cells["MaterialCode"].Value.ToString() == strMaterialCode &&
                               this.uGridINSMaterialGR.Rows[i].Cells["VendorCode"].Value.ToString() == strVendorCode &&
                               this.uGridINSMaterialGR.Rows[i].Cells["GrNo"].Value.ToString() == strGrNo)// &&
                            //this.uGridINSMaterialGR.Rows[i].Cells["MoLotNo"].Value.ToString() == strMoLotNo)
                            {
                                // 셀 수정시 RowSelector 이미지 변화
                                QRPGlobal grdImg = new QRPGlobal();
                                this.uGridINSMaterialGR.Rows[i].RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                                this.uGridINSMaterialGR.Rows[i].Cells["SpecNo"].Value = e.Cell.Row.Cells["SpecNo"].Value;
                            }
                        }
                    }
                }

                this.uGridINSMaterialGR.EventManager.AllEventsEnabled = true;
            }
            catch
            {
            }
            finally
            {
            }
        }

        #endregion
    }
}
