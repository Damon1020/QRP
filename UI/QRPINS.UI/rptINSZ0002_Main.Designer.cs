﻿namespace QRPINS.UI
{
    /// <summary>
    /// Summary description for rptINSZ0002_Main.
    /// </summary>
    partial class rptINSZ0002_Main
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptINSZ0002_Main));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.subResult = new DataDynamics.ActiveReports.SubReport();
            this.subData = new DataDynamics.ActiveReports.SubReport();
            this.subData1 = new DataDynamics.ActiveReports.SubReport();
            this.subData2 = new DataDynamics.ActiveReports.SubReport();
            this.subData3 = new DataDynamics.ActiveReports.SubReport();
            this.subData4 = new DataDynamics.ActiveReports.SubReport();
            this.subData5 = new DataDynamics.ActiveReports.SubReport();
            this.subData6 = new DataDynamics.ActiveReports.SubReport();
            this.subData7 = new DataDynamics.ActiveReports.SubReport();
            this.subData8 = new DataDynamics.ActiveReports.SubReport();
            this.reportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.lblTitle = new DataDynamics.ActiveReports.Label();
            this.lblStdNumber = new DataDynamics.ActiveReports.Label();
            this.txtStdNumber = new DataDynamics.ActiveReports.TextBox();
            this.lblMaterialHistory = new DataDynamics.ActiveReports.Label();
            this.lblVen = new DataDynamics.ActiveReports.Label();
            this.label2 = new DataDynamics.ActiveReports.Label();
            this.label1 = new DataDynamics.ActiveReports.Label();
            this.label3 = new DataDynamics.ActiveReports.Label();
            this.label4 = new DataDynamics.ActiveReports.Label();
            this.label5 = new DataDynamics.ActiveReports.Label();
            this.lblGradeName = new DataDynamics.ActiveReports.Label();
            this.label8 = new DataDynamics.ActiveReports.Label();
            this.txtVendor = new DataDynamics.ActiveReports.TextBox();
            this.txtSpec = new DataDynamics.ActiveReports.TextBox();
            this.txtSpecNo = new DataDynamics.ActiveReports.TextBox();
            this.txtLotQty = new DataDynamics.ActiveReports.TextBox();
            this.label12 = new DataDynamics.ActiveReports.Label();
            this.label13 = new DataDynamics.ActiveReports.Label();
            this.label14 = new DataDynamics.ActiveReports.Label();
            this.label15 = new DataDynamics.ActiveReports.Label();
            this.label17 = new DataDynamics.ActiveReports.Label();
            this.lblEtcDesc = new DataDynamics.ActiveReports.Label();
            this.lblICPCheck = new DataDynamics.ActiveReports.Label();
            this.checkPass = new DataDynamics.ActiveReports.CheckBox();
            this.checkFail = new DataDynamics.ActiveReports.CheckBox();
            this.textBox6 = new DataDynamics.ActiveReports.TextBox();
            this.txtLotNo = new DataDynamics.ActiveReports.TextBox();
            this.txtGRQty = new DataDynamics.ActiveReports.TextBox();
            this.txtGradeName = new DataDynamics.ActiveReports.TextBox();
            this.txtEtcDesc = new DataDynamics.ActiveReports.TextBox();
            this.txtFaultContents = new DataDynamics.ActiveReports.TextBox();
            this.txtGRDate = new DataDynamics.ActiveReports.TextBox();
            this.txtReceipt = new DataDynamics.ActiveReports.TextBox();
            this.txtCompleteDate = new DataDynamics.ActiveReports.TextBox();
            this.txtLot = new DataDynamics.ActiveReports.TextBox();
            this.txtICP = new DataDynamics.ActiveReports.TextBox();
            this.txtInspectUser = new DataDynamics.ActiveReports.TextBox();
            this.txtConfirm = new DataDynamics.ActiveReports.TextBox();
            this.lblConfirm = new DataDynamics.ActiveReports.Label();
            this.lblUnit = new DataDynamics.ActiveReports.Label();
            this.txtUnit = new DataDynamics.ActiveReports.TextBox();
            this.lblLeft = new DataDynamics.ActiveReports.Label();
            this.lblRight = new DataDynamics.ActiveReports.Label();
            this.reportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.txtEtc = new DataDynamics.ActiveReports.TextBox();
            this.lblEtc = new DataDynamics.ActiveReports.Label();
            this.lbl1 = new DataDynamics.ActiveReports.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMaterialHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGradeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpecNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblICPCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGradeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaultContents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtICP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInspectUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.subResult,
            this.subData,
            this.subData1,
            this.subData2,
            this.subData3,
            this.subData4,
            this.subData5,
            this.subData6,
            this.subData7,
            this.subData8});
            this.detail.Height = 2F;
            this.detail.Name = "detail";
            // 
            // subResult
            // 
            this.subResult.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subResult.CloseBorder = false;
            this.subResult.Height = 0.219F;
            this.subResult.Left = 0.03F;
            this.subResult.Name = "subResult";
            this.subResult.Report = null;
            this.subResult.ReportName = "검사결과";
            this.subResult.Top = 0.013F;
            this.subResult.Width = 7.28F;
            // 
            // subData
            // 
            this.subData.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData.CloseBorder = false;
            this.subData.Height = 0.138F;
            this.subData.Left = 0.03F;
            this.subData.Name = "subData";
            this.subData.Report = null;
            this.subData.ReportName = "검사DATA";
            this.subData.Top = 0.327F;
            this.subData.Visible = false;
            this.subData.Width = 7.272F;
            // 
            // subData1
            // 
            this.subData1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData1.CloseBorder = false;
            this.subData1.Height = 0.138F;
            this.subData1.Left = 0.03F;
            this.subData1.Name = "subData1";
            this.subData1.Report = null;
            this.subData1.ReportName = "검사DATA1";
            this.subData1.Top = 0.5150001F;
            this.subData1.Visible = false;
            this.subData1.Width = 7.272F;
            // 
            // subData2
            // 
            this.subData2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData2.CloseBorder = false;
            this.subData2.Height = 0.138F;
            this.subData2.Left = 0.03F;
            this.subData2.Name = "subData2";
            this.subData2.Report = null;
            this.subData2.ReportName = "검사DATA2";
            this.subData2.Top = 0.7060002F;
            this.subData2.Visible = false;
            this.subData2.Width = 7.272F;
            // 
            // subData3
            // 
            this.subData3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData3.CloseBorder = false;
            this.subData3.Height = 0.138F;
            this.subData3.Left = 0.03F;
            this.subData3.Name = "subData3";
            this.subData3.Report = null;
            this.subData3.ReportName = "검사DATA3";
            this.subData3.Top = 0.8970001F;
            this.subData3.Visible = false;
            this.subData3.Width = 7.272F;
            // 
            // subData4
            // 
            this.subData4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData4.CloseBorder = false;
            this.subData4.Height = 0.138F;
            this.subData4.Left = 0.03F;
            this.subData4.Name = "subData4";
            this.subData4.Report = null;
            this.subData4.ReportName = "검사DATA4";
            this.subData4.Top = 1.075F;
            this.subData4.Visible = false;
            this.subData4.Width = 7.272F;
            // 
            // subData5
            // 
            this.subData5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData5.CloseBorder = false;
            this.subData5.Height = 0.138F;
            this.subData5.Left = 0.03F;
            this.subData5.Name = "subData5";
            this.subData5.Report = null;
            this.subData5.ReportName = "검사DATA5";
            this.subData5.Top = 1.263F;
            this.subData5.Visible = false;
            this.subData5.Width = 7.272F;
            // 
            // subData6
            // 
            this.subData6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData6.CloseBorder = false;
            this.subData6.Height = 0.138F;
            this.subData6.Left = 0.03F;
            this.subData6.Name = "subData6";
            this.subData6.Report = null;
            this.subData6.ReportName = "검사DATA6";
            this.subData6.Top = 1.431F;
            this.subData6.Visible = false;
            this.subData6.Width = 7.272F;
            // 
            // subData7
            // 
            this.subData7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData7.CloseBorder = false;
            this.subData7.Height = 0.138F;
            this.subData7.Left = 0.03F;
            this.subData7.Name = "subData7";
            this.subData7.Report = null;
            this.subData7.ReportName = "검사DATA7";
            this.subData7.Top = 1.619F;
            this.subData7.Visible = false;
            this.subData7.Width = 7.272F;
            // 
            // subData8
            // 
            this.subData8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.subData8.CloseBorder = false;
            this.subData8.Height = 0.138F;
            this.subData8.Left = 0.03F;
            this.subData8.Name = "subData8";
            this.subData8.Report = null;
            this.subData8.ReportName = "검사DATA8";
            this.subData8.Top = 1.797F;
            this.subData8.Visible = false;
            this.subData8.Width = 7.272F;
            // 
            // reportHeader
            // 
            this.reportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblTitle,
            this.lblStdNumber,
            this.txtStdNumber,
            this.lblMaterialHistory,
            this.lblVen,
            this.label2,
            this.label1,
            this.label3,
            this.label4,
            this.label5,
            this.lblGradeName,
            this.label8,
            this.txtVendor,
            this.txtSpec,
            this.txtSpecNo,
            this.txtLotQty,
            this.label12,
            this.label13,
            this.label14,
            this.label15,
            this.label17,
            this.lblEtcDesc,
            this.lblICPCheck,
            this.checkPass,
            this.checkFail,
            this.textBox6,
            this.txtLotNo,
            this.txtGRQty,
            this.txtGradeName,
            this.txtEtcDesc,
            this.txtFaultContents,
            this.txtGRDate,
            this.txtReceipt,
            this.txtCompleteDate,
            this.txtLot,
            this.txtICP,
            this.txtInspectUser,
            this.txtConfirm,
            this.lblConfirm,
            this.lblUnit,
            this.txtUnit,
            this.lblLeft,
            this.lblRight});
            this.reportHeader.Height = 3.104167F;
            this.reportHeader.Name = "reportHeader";
            this.reportHeader.Format += new System.EventHandler(this.reportHeader_Format);
            // 
            // lblTitle
            // 
            this.lblTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.lblTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.lblTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.lblTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.lblTitle.DataField = "Title";
            this.lblTitle.Height = 0.387F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 0.03F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-size: 17pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.lblTitle.Text = "수입검사 성적서";
            this.lblTitle.Top = 0.058F;
            this.lblTitle.Width = 7.28F;
            // 
            // lblStdNumber
            // 
            this.lblStdNumber.Height = 0.2F;
            this.lblStdNumber.HyperLink = null;
            this.lblStdNumber.Left = 2.771F;
            this.lblStdNumber.Name = "lblStdNumber";
            this.lblStdNumber.Style = "text-justify: auto; vertical-align: middle";
            this.lblStdNumber.Text = "관리번호 :";
            this.lblStdNumber.Top = 0.505F;
            this.lblStdNumber.Width = 0.76F;
            // 
            // txtStdNumber
            // 
            this.txtStdNumber.DataField = "ReqNumber";
            this.txtStdNumber.Height = 0.2F;
            this.txtStdNumber.Left = 3.531F;
            this.txtStdNumber.Name = "txtStdNumber";
            this.txtStdNumber.Style = "text-align: center; vertical-align: middle";
            this.txtStdNumber.Text = null;
            this.txtStdNumber.Top = 0.505F;
            this.txtStdNumber.Width = 1F;
            // 
            // lblMaterialHistory
            // 
            this.lblMaterialHistory.Height = 0.2F;
            this.lblMaterialHistory.HyperLink = null;
            this.lblMaterialHistory.Left = 0.063F;
            this.lblMaterialHistory.Name = "lblMaterialHistory";
            this.lblMaterialHistory.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.lblMaterialHistory.Text = "1. 자재이력";
            this.lblMaterialHistory.Top = 0.928F;
            this.lblMaterialHistory.Width = 1F;
            // 
            // lblVen
            // 
            this.lblVen.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblVen.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblVen.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblVen.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblVen.Height = 0.2F;
            this.lblVen.HyperLink = null;
            this.lblVen.Left = 0.04800018F;
            this.lblVen.Name = "lblVen";
            this.lblVen.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.lblVen.Text = "협력업체";
            this.lblVen.Top = 1.128F;
            this.lblVen.Width = 1F;
            // 
            // label2
            // 
            this.label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.05F;
            this.label2.Name = "label2";
            this.label2.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label2.Text = "규격";
            this.label2.Top = 1.128F;
            this.label2.Width = 1.338F;
            // 
            // label1
            // 
            this.label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 2.388F;
            this.label1.Name = "label1";
            this.label1.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label1.Text = "SPEC No";
            this.label1.Top = 1.128F;
            this.label1.Width = 1.154F;
            // 
            // label3
            // 
            this.label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 3.542F;
            this.label3.Name = "label3";
            this.label3.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label3.Text = "Lot 수";
            this.label3.Top = 1.128F;
            this.label3.Width = 0.5649998F;
            // 
            // label4
            // 
            this.label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label4.Height = 0.2F;
            this.label4.HyperLink = null;
            this.label4.Left = 4.107001F;
            this.label4.Name = "label4";
            this.label4.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label4.Text = "LOT No";
            this.label4.Top = 1.128F;
            this.label4.Width = 1.278999F;
            // 
            // label5
            // 
            this.label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label5.Height = 0.2F;
            this.label5.HyperLink = null;
            this.label5.Left = 5.39F;
            this.label5.Name = "label5";
            this.label5.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label5.Text = "입고 수량(K)";
            this.label5.Top = 1.128F;
            this.label5.Width = 0.8369999F;
            // 
            // lblGradeName
            // 
            this.lblGradeName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblGradeName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblGradeName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblGradeName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblGradeName.Height = 0.2F;
            this.lblGradeName.HyperLink = null;
            this.lblGradeName.Left = 6.227F;
            this.lblGradeName.Name = "lblGradeName";
            this.lblGradeName.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.lblGradeName.Text = "검사등급";
            this.lblGradeName.Top = 1.128F;
            this.lblGradeName.Width = 1.083F;
            // 
            // label8
            // 
            this.label8.Height = 0.2F;
            this.label8.HyperLink = null;
            this.label8.Left = 5.915F;
            this.label8.Name = "label8";
            this.label8.Style = "";
            this.label8.Text = "작성자 : ";
            this.label8.Top = 0.872F;
            this.label8.Width = 0.5639997F;
            // 
            // txtVendor
            // 
            this.txtVendor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtVendor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtVendor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtVendor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtVendor.DataField = "VendorName";
            this.txtVendor.Height = 0.4080001F;
            this.txtVendor.Left = 0.04800018F;
            this.txtVendor.Name = "txtVendor";
            this.txtVendor.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtVendor.Text = null;
            this.txtVendor.Top = 1.328F;
            this.txtVendor.Width = 1F;
            // 
            // txtSpec
            // 
            this.txtSpec.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtSpec.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtSpec.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtSpec.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtSpec.DataField = "MaterialName";
            this.txtSpec.Height = 0.4080001F;
            this.txtSpec.Left = 1.05F;
            this.txtSpec.Name = "txtSpec";
            this.txtSpec.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtSpec.Text = null;
            this.txtSpec.Top = 1.328F;
            this.txtSpec.Width = 1.338F;
            // 
            // txtSpecNo
            // 
            this.txtSpecNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtSpecNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtSpecNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtSpecNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtSpecNo.DataField = "SpecNo";
            this.txtSpecNo.Height = 0.408F;
            this.txtSpecNo.Left = 2.388F;
            this.txtSpecNo.Name = "txtSpecNo";
            this.txtSpecNo.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtSpecNo.Text = null;
            this.txtSpecNo.Top = 1.328F;
            this.txtSpecNo.Width = 1.154F;
            // 
            // txtLotQty
            // 
            this.txtLotQty.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtLotQty.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLotQty.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLotQty.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLotQty.DataField = "LotQty";
            this.txtLotQty.Height = 0.408F;
            this.txtLotQty.Left = 3.542F;
            this.txtLotQty.Name = "txtLotQty";
            this.txtLotQty.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtLotQty.Text = null;
            this.txtLotQty.Top = 1.328F;
            this.txtLotQty.Width = 0.5650003F;
            // 
            // label12
            // 
            this.label12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label12.Height = 0.1980001F;
            this.label12.HyperLink = null;
            this.label12.Left = 0.048F;
            this.label12.Name = "label12";
            this.label12.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label12.Text = "입고일";
            this.label12.Top = 1.766F;
            this.label12.Width = 1F;
            // 
            // label13
            // 
            this.label13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label13.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label13.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label13.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label13.Height = 0.1980001F;
            this.label13.HyperLink = null;
            this.label13.Left = 1.048F;
            this.label13.Name = "label13";
            this.label13.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label13.Text = "접수일";
            this.label13.Top = 1.766F;
            this.label13.Width = 0.659F;
            // 
            // label14
            // 
            this.label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label14.Height = 0.1980001F;
            this.label14.HyperLink = null;
            this.label14.Left = 1.707F;
            this.label14.Name = "label14";
            this.label14.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label14.Text = "완료일";
            this.label14.Top = 1.766F;
            this.label14.Width = 0.6810004F;
            // 
            // label15
            // 
            this.label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label15.Height = 0.1980003F;
            this.label15.HyperLink = null;
            this.label15.Left = 2.388F;
            this.label15.Name = "label15";
            this.label15.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label15.Text = "검사LOT";
            this.label15.Top = 1.766F;
            this.label15.Width = 1.154F;
            // 
            // label17
            // 
            this.label17.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label17.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.label17.Height = 0.1980002F;
            this.label17.HyperLink = null;
            this.label17.Left = 3.542F;
            this.label17.Name = "label17";
            this.label17.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.label17.Text = "불량 정보";
            this.label17.Top = 1.766F;
            this.label17.Width = 1.843999F;
            // 
            // lblEtcDesc
            // 
            this.lblEtcDesc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblEtcDesc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblEtcDesc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblEtcDesc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblEtcDesc.Height = 0.1980002F;
            this.lblEtcDesc.HyperLink = null;
            this.lblEtcDesc.Left = 6.229F;
            this.lblEtcDesc.Name = "lblEtcDesc";
            this.lblEtcDesc.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.lblEtcDesc.Text = "비고";
            this.lblEtcDesc.Top = 1.766F;
            this.lblEtcDesc.Width = 1.081F;
            // 
            // lblICPCheck
            // 
            this.lblICPCheck.Height = 0.2F;
            this.lblICPCheck.HyperLink = null;
            this.lblICPCheck.Left = 5.306001F;
            this.lblICPCheck.Name = "lblICPCheck";
            this.lblICPCheck.Style = "font-size: 9pt; font-weight: normal";
            this.lblICPCheck.Text = "ICP CHECK :";
            this.lblICPCheck.Top = 2.704F;
            this.lblICPCheck.Width = 0.8959999F;
            // 
            // checkPass
            // 
            this.checkPass.Height = 0.2F;
            this.checkPass.Left = 6.202001F;
            this.checkPass.Name = "checkPass";
            this.checkPass.Style = "font-size: 9pt";
            this.checkPass.Text = "PASS";
            this.checkPass.Top = 2.704F;
            this.checkPass.Width = 0.6349998F;
            // 
            // checkFail
            // 
            this.checkFail.Height = 0.2F;
            this.checkFail.Left = 6.837F;
            this.checkFail.Name = "checkFail";
            this.checkFail.Style = "font-size: 9pt";
            this.checkFail.Text = "FAIL";
            this.checkFail.Top = 2.704F;
            this.checkFail.Width = 0.4650012F;
            // 
            // textBox6
            // 
            this.textBox6.Height = 0.2F;
            this.textBox6.Left = 6.479F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "vertical-align: middle";
            this.textBox6.Text = null;
            this.textBox6.Top = 0.872F;
            this.textBox6.Width = 0.823F;
            // 
            // txtLotNo
            // 
            this.txtLotNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtLotNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLotNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLotNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLotNo.DataField = "GRLotNo";
            this.txtLotNo.Height = 0.408F;
            this.txtLotNo.Left = 4.107F;
            this.txtLotNo.Name = "txtLotNo";
            this.txtLotNo.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle; w" +
                "hite-space: inherit";
            this.txtLotNo.Text = null;
            this.txtLotNo.Top = 1.328F;
            this.txtLotNo.Width = 1.279F;
            // 
            // txtGRQty
            // 
            this.txtGRQty.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtGRQty.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGRQty.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGRQty.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGRQty.DataField = "TotalQty";
            this.txtGRQty.Height = 0.408F;
            this.txtGRQty.Left = 5.39F;
            this.txtGRQty.Name = "txtGRQty";
            this.txtGRQty.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtGRQty.Text = null;
            this.txtGRQty.Top = 1.328F;
            this.txtGRQty.Width = 0.8369999F;
            // 
            // txtGradeName
            // 
            this.txtGradeName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtGradeName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGradeName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtGradeName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGradeName.DataField = "MaterialGradeName";
            this.txtGradeName.Height = 0.408F;
            this.txtGradeName.Left = 6.227F;
            this.txtGradeName.MultiLine = false;
            this.txtGradeName.Name = "txtGradeName";
            this.txtGradeName.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtGradeName.Text = null;
            this.txtGradeName.Top = 1.328F;
            this.txtGradeName.Width = 1.083F;
            // 
            // txtEtcDesc
            // 
            this.txtEtcDesc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtEtcDesc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtEtcDesc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtEtcDesc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtEtcDesc.DataField = "EtcDescQA";
            this.txtEtcDesc.Height = 0.5430001F;
            this.txtEtcDesc.Left = 6.227F;
            this.txtEtcDesc.Name = "txtEtcDesc";
            this.txtEtcDesc.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle; w" +
                "hite-space: inherit";
            this.txtEtcDesc.Text = null;
            this.txtEtcDesc.Top = 1.964F;
            this.txtEtcDesc.Width = 1.083F;
            // 
            // txtFaultContents
            // 
            this.txtFaultContents.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtFaultContents.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaultContents.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaultContents.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaultContents.DataField = "AbnormalLotNo";
            this.txtFaultContents.Height = 0.5430001F;
            this.txtFaultContents.Left = 3.542F;
            this.txtFaultContents.Name = "txtFaultContents";
            this.txtFaultContents.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtFaultContents.Text = null;
            this.txtFaultContents.Top = 1.964F;
            this.txtFaultContents.Width = 1.844F;
            // 
            // txtGRDate
            // 
            this.txtGRDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtGRDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtGRDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGRDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGRDate.DataField = "GRDate";
            this.txtGRDate.Height = 0.5430001F;
            this.txtGRDate.Left = 0.0479999F;
            this.txtGRDate.MultiLine = false;
            this.txtGRDate.Name = "txtGRDate";
            this.txtGRDate.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtGRDate.Text = null;
            this.txtGRDate.Top = 1.964F;
            this.txtGRDate.Width = 0.9999999F;
            // 
            // txtReceipt
            // 
            this.txtReceipt.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtReceipt.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtReceipt.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtReceipt.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtReceipt.DataField = "ReceiptDate";
            this.txtReceipt.Height = 0.5430001F;
            this.txtReceipt.Left = 1.048F;
            this.txtReceipt.MultiLine = false;
            this.txtReceipt.Name = "txtReceipt";
            this.txtReceipt.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtReceipt.Text = null;
            this.txtReceipt.Top = 1.964F;
            this.txtReceipt.Width = 0.6590003F;
            // 
            // txtCompleteDate
            // 
            this.txtCompleteDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtCompleteDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtCompleteDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtCompleteDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtCompleteDate.DataField = "InspectDate";
            this.txtCompleteDate.Height = 0.5430001F;
            this.txtCompleteDate.Left = 1.707F;
            this.txtCompleteDate.MultiLine = false;
            this.txtCompleteDate.Name = "txtCompleteDate";
            this.txtCompleteDate.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtCompleteDate.Text = null;
            this.txtCompleteDate.Top = 1.964F;
            this.txtCompleteDate.Width = 0.6810005F;
            // 
            // txtLot
            // 
            this.txtLot.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtLot.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLot.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLot.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLot.DataField = "InspectLotNo";
            this.txtLot.Height = 0.5430001F;
            this.txtLot.Left = 2.388F;
            this.txtLot.Name = "txtLot";
            this.txtLot.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtLot.Text = null;
            this.txtLot.Top = 1.964F;
            this.txtLot.Width = 1.154F;
            // 
            // txtICP
            // 
            this.txtICP.DataField = "ICPCheckFlag";
            this.txtICP.Height = 0.2F;
            this.txtICP.Left = 6.479F;
            this.txtICP.Name = "txtICP";
            this.txtICP.Style = "vertical-align: middle";
            this.txtICP.Text = null;
            this.txtICP.Top = 0.6720001F;
            this.txtICP.Visible = false;
            this.txtICP.Width = 0.823F;
            // 
            // txtInspectUser
            // 
            this.txtInspectUser.DataField = "InspectUserName";
            this.txtInspectUser.Height = 0.2F;
            this.txtInspectUser.Left = 6.479F;
            this.txtInspectUser.Name = "txtInspectUser";
            this.txtInspectUser.Style = "font-size: 8.5pt; vertical-align: middle";
            this.txtInspectUser.Text = null;
            this.txtInspectUser.Top = 0.872F;
            this.txtInspectUser.Width = 0.823F;
            // 
            // txtConfirm
            // 
            this.txtConfirm.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtConfirm.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtConfirm.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtConfirm.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtConfirm.DataField = "PassFail";
            this.txtConfirm.Height = 0.5430003F;
            this.txtConfirm.Left = 5.386F;
            this.txtConfirm.MultiLine = false;
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.Style = "font-size: 8pt; text-align: center; text-justify: auto; vertical-align: middle";
            this.txtConfirm.Text = null;
            this.txtConfirm.Top = 1.964F;
            this.txtConfirm.Width = 0.8410001F;
            // 
            // lblConfirm
            // 
            this.lblConfirm.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblConfirm.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblConfirm.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblConfirm.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblConfirm.Height = 0.1980001F;
            this.lblConfirm.HyperLink = null;
            this.lblConfirm.Left = 5.386F;
            this.lblConfirm.Name = "lblConfirm";
            this.lblConfirm.Style = "font-size: 9pt; font-weight: normal; text-align: center; text-justify: auto; vert" +
                "ical-align: middle";
            this.lblConfirm.Text = "최종판정";
            this.lblConfirm.Top = 1.766F;
            this.lblConfirm.Width = 0.841001F;
            // 
            // lblUnit
            // 
            this.lblUnit.Height = 0.2F;
            this.lblUnit.HyperLink = null;
            this.lblUnit.Left = 5.779F;
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Style = "font-size: 9pt; vertical-align: middle";
            this.lblUnit.Text = "시료단위  :";
            this.lblUnit.Top = 2.904F;
            this.lblUnit.Width = 0.7710004F;
            // 
            // txtUnit
            // 
            this.txtUnit.DataField = "UnitCode";
            this.txtUnit.Height = 0.2F;
            this.txtUnit.Left = 6.55F;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Style = "font-size: 9pt; text-align: right; vertical-align: middle";
            this.txtUnit.Text = null;
            this.txtUnit.Top = 2.904F;
            this.txtUnit.Width = 0.7599998F;
            // 
            // lblLeft
            // 
            this.lblLeft.Height = 0.2F;
            this.lblLeft.HyperLink = null;
            this.lblLeft.Left = 2.677F;
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Style = "text-align: center; vertical-align: middle";
            this.lblLeft.Text = "(";
            this.lblLeft.Top = 0.495F;
            this.lblLeft.Width = 0.09399986F;
            // 
            // lblRight
            // 
            this.lblRight.Height = 0.2F;
            this.lblRight.HyperLink = null;
            this.lblRight.Left = 4.531F;
            this.lblRight.Name = "lblRight";
            this.lblRight.Style = "text-align: center; vertical-align: middle";
            this.lblRight.Text = ")";
            this.lblRight.Top = 0.495F;
            this.lblRight.Width = 0.09399986F;
            // 
            // reportFooter
            // 
            this.reportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtEtc,
            this.lblEtc,
            this.lbl1});
            this.reportFooter.Height = 1.1875F;
            this.reportFooter.Name = "reportFooter";
            // 
            // txtEtc
            // 
            this.txtEtc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.txtEtc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.txtEtc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.txtEtc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.txtEtc.Height = 0.47F;
            this.txtEtc.Left = 0.03F;
            this.txtEtc.Name = "txtEtc";
            this.txtEtc.Text = null;
            this.txtEtc.Top = 0.6F;
            this.txtEtc.Width = 7.272F;
            // 
            // lblEtc
            // 
            this.lblEtc.Height = 0.2F;
            this.lblEtc.HyperLink = null;
            this.lblEtc.Left = 0.048F;
            this.lblEtc.Name = "lblEtc";
            this.lblEtc.Style = "font-weight: bold; text-align: center; vertical-align: middle";
            this.lblEtc.Text = "특기 사항";
            this.lblEtc.Top = 0.39F;
            this.lblEtc.Width = 1F;
            // 
            // lbl1
            // 
            this.lbl1.Height = 0.2F;
            this.lbl1.HyperLink = null;
            this.lbl1.Left = 0.048F;
            this.lbl1.Name = "lbl1";
            this.lbl1.Style = "font-size: 8pt; vertical-align: middle";
            this.lbl1.Text = "※ 수입검사 측정 및 분석을 실시하는 항목에 대한 결과를 기록한다. [ 치수/기능/특성 등 ]";
            this.lbl1.Top = 0F;
            this.lbl1.Width = 5.989F;
            // 
            // rptINSZ0002_Main
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.357F;
            this.Sections.Add(this.reportHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.reportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMaterialHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGradeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpecNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblICPCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGradeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaultContents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGRDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtICP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInspectUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.SubReport subResult;
        private DataDynamics.ActiveReports.SubReport subData;
        private DataDynamics.ActiveReports.ReportHeader reportHeader;
        private DataDynamics.ActiveReports.ReportFooter reportFooter;
        private DataDynamics.ActiveReports.TextBox txtEtc;
        private DataDynamics.ActiveReports.Label lblEtc;
        private DataDynamics.ActiveReports.Label lblTitle;
        private DataDynamics.ActiveReports.Label lbl1;
        private DataDynamics.ActiveReports.Label lblStdNumber;
        private DataDynamics.ActiveReports.TextBox txtStdNumber;
        private DataDynamics.ActiveReports.Label lblMaterialHistory;
        private DataDynamics.ActiveReports.Label lblVen;
        private DataDynamics.ActiveReports.Label label2;
        private DataDynamics.ActiveReports.Label label1;
        private DataDynamics.ActiveReports.Label label3;
        private DataDynamics.ActiveReports.Label label4;
        private DataDynamics.ActiveReports.Label label5;
        private DataDynamics.ActiveReports.Label lblGradeName;
        private DataDynamics.ActiveReports.Label label8;
        private DataDynamics.ActiveReports.TextBox txtVendor;
        private DataDynamics.ActiveReports.TextBox txtSpec;
        private DataDynamics.ActiveReports.TextBox txtSpecNo;
        private DataDynamics.ActiveReports.TextBox txtLotQty;
        private DataDynamics.ActiveReports.Label label12;
        private DataDynamics.ActiveReports.Label label13;
        private DataDynamics.ActiveReports.Label label14;
        private DataDynamics.ActiveReports.Label label15;
        private DataDynamics.ActiveReports.Label label17;
        private DataDynamics.ActiveReports.Label lblEtcDesc;
        private DataDynamics.ActiveReports.Label lblICPCheck;
        private DataDynamics.ActiveReports.CheckBox checkPass;
        private DataDynamics.ActiveReports.CheckBox checkFail;
        private DataDynamics.ActiveReports.TextBox textBox6;
        private DataDynamics.ActiveReports.TextBox txtLotNo;
        private DataDynamics.ActiveReports.TextBox txtGRQty;
        private DataDynamics.ActiveReports.TextBox txtGradeName;
        private DataDynamics.ActiveReports.TextBox txtEtcDesc;
        private DataDynamics.ActiveReports.TextBox txtFaultContents;
        private DataDynamics.ActiveReports.TextBox txtGRDate;
        private DataDynamics.ActiveReports.TextBox txtReceipt;
        private DataDynamics.ActiveReports.TextBox txtCompleteDate;
        private DataDynamics.ActiveReports.TextBox txtLot;
        private DataDynamics.ActiveReports.SubReport subData1;
        private DataDynamics.ActiveReports.SubReport subData2;
        private DataDynamics.ActiveReports.SubReport subData3;
        private DataDynamics.ActiveReports.SubReport subData4;
        private DataDynamics.ActiveReports.TextBox txtICP;
        private DataDynamics.ActiveReports.SubReport subData5;
        private DataDynamics.ActiveReports.TextBox txtInspectUser;
        private DataDynamics.ActiveReports.TextBox txtConfirm;
        private DataDynamics.ActiveReports.Label lblConfirm;
        private DataDynamics.ActiveReports.SubReport subData6;
        private DataDynamics.ActiveReports.SubReport subData7;
        private DataDynamics.ActiveReports.SubReport subData8;
        private DataDynamics.ActiveReports.Label lblUnit;
        private DataDynamics.ActiveReports.TextBox txtUnit;
        private DataDynamics.ActiveReports.Label lblLeft;
        private DataDynamics.ActiveReports.Label lblRight;
    }
}
