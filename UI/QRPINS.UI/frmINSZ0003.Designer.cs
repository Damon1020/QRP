﻿namespace QRPINS.UI
{
    partial class frmINSZ0003
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0003));
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckSearchGRFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelSearchGRFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchConsumableType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ulabelSearchConsumableType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchGRNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchGRNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateSearchGRToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchGRFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchGRDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridINSMaterialGR = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchGRFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchGRNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridINSMaterialGR)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckSearchGRFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchGRFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.ulabelSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchGRNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchGRNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchGRToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchGRFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchGRDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchVendor);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 85);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uCheckSearchGRFlag
            // 
            this.uCheckSearchGRFlag.Location = new System.Drawing.Point(644, 60);
            this.uCheckSearchGRFlag.Name = "uCheckSearchGRFlag";
            this.uCheckSearchGRFlag.Size = new System.Drawing.Size(20, 20);
            this.uCheckSearchGRFlag.TabIndex = 49;
            // 
            // uLabelSearchGRFlag
            // 
            this.uLabelSearchGRFlag.Location = new System.Drawing.Point(540, 60);
            this.uLabelSearchGRFlag.Name = "uLabelSearchGRFlag";
            this.uLabelSearchGRFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchGRFlag.TabIndex = 48;
            this.uLabelSearchGRFlag.Text = "ultraLabel1";
            // 
            // uComboSearchConsumableType
            // 
            this.uComboSearchConsumableType.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchConsumableType.Name = "uComboSearchConsumableType";
            this.uComboSearchConsumableType.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchConsumableType.TabIndex = 47;
            this.uComboSearchConsumableType.Text = "ultraComboEditor1";
            // 
            // ulabelSearchConsumableType
            // 
            this.ulabelSearchConsumableType.Location = new System.Drawing.Point(12, 36);
            this.ulabelSearchConsumableType.Name = "ulabelSearchConsumableType";
            this.ulabelSearchConsumableType.Size = new System.Drawing.Size(100, 20);
            this.ulabelSearchConsumableType.TabIndex = 46;
            this.ulabelSearchConsumableType.Text = "ultraLabel1";
            // 
            // uTextSearchGRNo
            // 
            this.uTextSearchGRNo.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchGRNo.Name = "uTextSearchGRNo";
            this.uTextSearchGRNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchGRNo.TabIndex = 23;
            // 
            // uLabelSearchGRNo
            // 
            this.uLabelSearchGRNo.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchGRNo.Name = "uLabelSearchGRNo";
            this.uLabelSearchGRNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchGRNo.TabIndex = 22;
            this.uLabelSearchGRNo.Text = "ultraLabel1";
            // 
            // uTextSearchVendorName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Appearance = appearance16;
            this.uTextSearchVendorName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchVendorName.Location = new System.Drawing.Point(756, 36);
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.ReadOnly = true;
            this.uTextSearchVendorName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchVendorName.TabIndex = 21;
            // 
            // uTextSearchVendorCode
            // 
            appearance15.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchVendorCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchVendorCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(644, 36);
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchVendorCode.TabIndex = 20;
            this.uTextSearchVendorCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchVendorCode_KeyDown);
            this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchVendorCode_EditorButtonClick);
            // 
            // uDateSearchGRToDate
            // 
            this.uDateSearchGRToDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchGRToDate.Location = new System.Drawing.Point(232, 60);
            this.uDateSearchGRToDate.Name = "uDateSearchGRToDate";
            this.uDateSearchGRToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchGRToDate.TabIndex = 19;
            // 
            // ultraLabel3
            // 
            appearance5.TextHAlignAsString = "Center";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance5;
            this.ultraLabel3.Location = new System.Drawing.Point(216, 60);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 18;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchGRFromDate
            // 
            this.uDateSearchGRFromDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchGRFromDate.Location = new System.Drawing.Point(116, 60);
            this.uDateSearchGRFromDate.Name = "uDateSearchGRFromDate";
            this.uDateSearchGRFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchGRFromDate.TabIndex = 17;
            // 
            // uLabelSearchGRDate
            // 
            this.uLabelSearchGRDate.Location = new System.Drawing.Point(12, 60);
            this.uLabelSearchGRDate.Name = "uLabelSearchGRDate";
            this.uLabelSearchGRDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchGRDate.TabIndex = 16;
            this.uLabelSearchGRDate.Text = "ultraLabel2";
            // 
            // uLabelSearchVendor
            // 
            this.uLabelSearchVendor.Location = new System.Drawing.Point(540, 36);
            this.uLabelSearchVendor.Name = "uLabelSearchVendor";
            this.uLabelSearchVendor.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchVendor.TabIndex = 10;
            this.uLabelSearchVendor.Text = "ultraLabel1";
            // 
            // uTextSearchMaterialName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Appearance = appearance17;
            this.uTextSearchMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(756, 12);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.ReadOnly = true;
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(240, 21);
            this.uTextSearchMaterialName.TabIndex = 7;
            // 
            // uTextSearchMaterialCode
            // 
            appearance18.Image = global::QRPINS.UI.Properties.Resources.btn_Zoom;
            appearance18.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance18;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(644, 12);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(110, 21);
            this.uTextSearchMaterialCode.TabIndex = 5;
            this.uTextSearchMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMaterialCode_KeyDown);
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick);
            // 
            // uLabelSearchMaterialCode
            // 
            this.uLabelSearchMaterialCode.Location = new System.Drawing.Point(540, 12);
            this.uLabelSearchMaterialCode.Name = "uLabelSearchMaterialCode";
            this.uLabelSearchMaterialCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialCode.TabIndex = 4;
            this.uLabelSearchMaterialCode.Text = "ultraLabel1";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(380, 36);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchLotNo.TabIndex = 3;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(276, 36);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 2;
            this.uLabelSearchLotNo.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridINSMaterialGR
            // 
            this.uGridINSMaterialGR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridINSMaterialGR.DisplayLayout.Appearance = appearance6;
            this.uGridINSMaterialGR.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridINSMaterialGR.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridINSMaterialGR.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridINSMaterialGR.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridINSMaterialGR.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridINSMaterialGR.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridINSMaterialGR.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridINSMaterialGR.DisplayLayout.MaxRowScrollRegions = 1;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridINSMaterialGR.DisplayLayout.Override.ActiveCellAppearance = appearance14;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridINSMaterialGR.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.uGridINSMaterialGR.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridINSMaterialGR.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridINSMaterialGR.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance7.BorderColor = System.Drawing.Color.Silver;
            appearance7.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridINSMaterialGR.DisplayLayout.Override.CellAppearance = appearance7;
            this.uGridINSMaterialGR.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridINSMaterialGR.DisplayLayout.Override.CellPadding = 0;
            appearance11.BackColor = System.Drawing.SystemColors.Control;
            appearance11.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance11.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridINSMaterialGR.DisplayLayout.Override.GroupByRowAppearance = appearance11;
            appearance13.TextHAlignAsString = "Left";
            this.uGridINSMaterialGR.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.uGridINSMaterialGR.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridINSMaterialGR.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridINSMaterialGR.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridINSMaterialGR.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridINSMaterialGR.DisplayLayout.Override.TemplateAddRowAppearance = appearance10;
            this.uGridINSMaterialGR.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridINSMaterialGR.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridINSMaterialGR.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridINSMaterialGR.Location = new System.Drawing.Point(0, 125);
            this.uGridINSMaterialGR.Name = "uGridINSMaterialGR";
            this.uGridINSMaterialGR.Size = new System.Drawing.Size(1070, 695);
            this.uGridINSMaterialGR.TabIndex = 2;
            this.uGridINSMaterialGR.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridINSMaterialGR_AfterCellUpdate);
            this.uGridINSMaterialGR.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridINSMaterialGR_DoubleClickRow);
            this.uGridINSMaterialGR.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridINSMaterialGR_CellChange);
            // 
            // frmINSZ0003
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridINSMaterialGR);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0003";
            this.Load += new System.EventHandler(this.frmINSZ0003_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0003_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0003_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSearchGRFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchGRNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchGRFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridINSMaterialGR)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchGRToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchGRFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchGRDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridINSMaterialGR;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchGRNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchGRNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchConsumableType;
        private Infragistics.Win.Misc.UltraLabel ulabelSearchConsumableType;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSearchGRFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchGRFlag;
    }
}