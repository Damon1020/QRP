﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질검사관리                                          */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINSZ0008.cs                                        */
/* 프로그램명   : 원자재 특채정보                                       */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-18                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;
using System.Xml;

namespace QRPINS.UI
{
    public partial class frmINSZ0008 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 			// 전역변수

        public frmINSZ0008()
        {
            InitializeComponent();
        }

        private void frmINSZ0008_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0008_Load(object sender, EventArgs e)
        {
            //컨트롤초기화
            uGroupBoxContentsArea.Expanded = false;
            InitGroupBox();
            InitText();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitTab();
            InitValue();
            initButton();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

            //디버깅모드
            SetRunMode();
            SetToolAuth();
        }

        /// <summary>
        /// 디버깅위한 매서드
        /// </summary>
        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.ultraTabControl2, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그룹박스 초기화
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.INFO, "배포처 및 참석대상부서", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.INFO, "MRB 계획", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.INFO, "배포처 및 참석대상부서", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.INFO, "공정적용결과", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox5, GroupBoxType.INFO, "합의부서", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
            
                this.uGroupBox5.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox5.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox6, GroupBoxType.INFO, "배포선", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox6.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox6.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBoxShip, GroupBoxType.INFO, "Ship정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxShip.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxShip.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //타이틀설정
                titleArea.mfSetLabelText("원자재 특채정보", m_resSys.GetString("SYS_FONTNAME"), 12);

                //기본값지정
                uTextWriterID.Text = m_resSys.GetString("SYS_USERID");

                uTextDepartment.Text = m_resSys.GetString("SYS_DEPTCODE");

                uTextGRNo.Hide();


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void initButton()
        {
            try
            {
                // 버튼크기조절 <--
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonFile1Down, "Download", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
                wButton.mfSetButton(this.uButtonResultFileDown, "Download", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                //공장
                QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant plant = new QRPMAS.BL.MASPRC.Plant();
                brwChnnel.mfCredentials(plant);

                DataTable dtPlant = new DataTable();

                dtPlant = plant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //부서
                brwChnnel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChnnel.mfCredentials(clsDept);

                //공장 콤보 설정
                WinComboEditor com = new WinComboEditor();
                com.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
                com.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                   , "PlantCode", "PlantName", dtPlant);

                String strPlantCode = this.uComboPlant.Value.ToString();
                DataTable dtDept = new DataTable();

                dtDept = clsDept.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //부서 콤보 설정
                com.mfSetComboEditor(this.uComboDistPurchaseUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistProductUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistDevelopUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistEquipUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistQualityUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistEtcUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCustomer, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCreateDay, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelAdminNum, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlant1, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelWriter, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelDepartment, "의뢰부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCreatDayEx, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelMaterialCodeEx, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStandard, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelCustomerEx, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelLotNoEx, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelQuantity, "수량", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelResult, "결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelNoteEx, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRequstInfo, "의뢰목적 및 내용", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelImportInspectDecision, "수입검사결정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelValidationDate, "유효기간연장일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //1차회의
                lbl.mfSetLabel(this.uLabelMeetingDayAndTime, "회의일시", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlace, "장소", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelAttachFile, "파일첨부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelBuyTeam, "구매팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProductionTeam, "생산팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelQualityTeam, "품질팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDevelopmentTeam, "개발팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipmentTeam, "설비팀", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                lbl.mfSetLabel(this.uLabelContent, "내용", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelResponsibility, "담당", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDeadline, "납기", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelNote, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //2차회의
                lbl.mfSetLabel(this.uLabelMeetingDayAndTime2, "회의일시", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlace2, "장소", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelBuyTeam2, "구매팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProductionTeam2, "생산팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelQualityTeam2, "품질팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDevelopmentTeam2, "개발팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipmentTeam2, "설비팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //공정적용결과
                lbl.mfSetLabel(this.uLabelResultAndDecision, "검토결과 및 결정사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelAttachFile2, "파일첨부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelResponsibilityT2, "담당", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDeadlineT2, "납기", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelNoteT2, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false); 
                lbl.mfSetLabel(this.uLabelBuyTeamT2, "구매팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProductionTeamT2, "생산팀", m_resSys.GetString("SYS_FONTNAME"), true, false); 
                lbl.mfSetLabel(this.uLabelQualityTeamT2, "품질팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDevelopmentTeamT2, "개발팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipmentTeamT2, "설비팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelFutureDirection, "향후대책", m_resSys.GetString("SYS_FONTNAME"), true, false); 
                //배포선
                lbl.mfSetLabel(this.uLabelBuyTeamT3, "구매팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProductionTeamT3, "생산팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelQualityTeamT3, "품질팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelDevelopmentTeamT3, "개발팀", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipmentTeamT3, "설비팀", m_resSys.GetString("SYS_FONTNAME"), true, false);                                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                grd.mfInitGeneralGrid(this.uGridInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //컬럼설정
                grd.mfSetGridColumn(this.uGridInfo, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "StdNumber", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 15
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "Qty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn,nnn", "0");

                this.uGridInfo.DisplayLayout.Bands[0].Columns["Qty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                grd.mfSetGridColumn(this.uGridInfo, 0, "WriteDate", "작성일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 140, false, false, 15
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "InspectResult", "검사결정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "InspectResultName", "검사결정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "GWTFlag", "결재요청", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "GWResultState", "결재결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //추가 그리드

                grd.mfSetGridColumn(this.uGridInfo, 0, "CompleteFlag", "작성완료여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "MESTFlag", "MES요청여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "VendorCode", "거래처 코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInfo, 0, "GRNo", "GRNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 20
                     , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                //Ship정보 그리드
                grd.mfInitGeneralGrid(this.uGridShipInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼 설정
                grd.mfSetGridColumn(this.uGridShipInfo, 0, "LotCheckFlag", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "LotSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "GRQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn,nnn", "0");

                this.uGridShipInfo.DisplayLayout.Bands[0].Columns["GRQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "ManufactureDate", "제조일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "ExpirationDate", "유효기간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Hidden
                grd.mfSetGridColumn(this.uGridShipInfo, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridShipInfo, 0, "VendorName", "거래처명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridShipInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridShipInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //쓰기금지, 색 변경
        private void ReWrite()
        {
            try
            {
                this.uComboPlant.ReadOnly = false;
                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;

                this.uTextLotNo.ReadOnly = false;
                this.uTextLotNo.Appearance.BackColor = Color.PowderBlue;

                this.uTextGRNo.ReadOnly = false;
                this.uTextGRNo.Appearance.BackColor = Color.PowderBlue;

                this.uTextWriterID.ButtonsRight["Search"].Visible = true;
                this.uTextWriterID.ReadOnly = false;
                this.uTextWriterID.Appearance.BackColor = Color.PowderBlue;

                this.uDateWriteDate.ReadOnly = false;
                this.uDateWriteDate.Appearance.BackColor = Color.PowderBlue;

                this.uTextQty.ReadOnly = false;
                this.uTextQty.Appearance.BackColor = Color.PowderBlue;

                this.uTextEtcDesc.ReadOnly = false;
                this.uTextEtcDesc.Appearance.BackColor = Color.White;

                this.uTextReqObjectDesc.ReadOnly = false;
                this.uTextReqObjectDesc.Appearance.BackColor = Color.White;

                this.uTextResult.ReadOnly = false;
                this.uTextResult.Appearance.BackColor = Color.White;

                this.uOptionInspectResult.Enabled = true;

                this.uCheckCompleteFlag.Enabled = true;

                this.uDateMeetingDate1.ReadOnly = false;
                this.uDateMeetingDate1.Appearance.BackColor = Color.White;

                this.uTextMeetingPlace1.ReadOnly = false;
                this.uTextMeetingPlace1.Appearance.BackColor = Color.White;

                this.uTextFileName1.ButtonsRight["Search"].Visible = true;
                this.uTextFileName1.ReadOnly = false;
                this.uTextFileName1.Appearance.BackColor = Color.White;

                this.uTextDistPurchaseUserID1.ButtonsRight["Search"].Visible = true;
                this.uTextDistPurchaseUserID1.ReadOnly = false;
                this.uTextDistPurchaseUserID1.Appearance.BackColor = Color.White;

                this.uTextDistProductUserID1.ButtonsRight["Search"].Visible = true;
                this.uTextDistProductUserID1.ReadOnly = false;
                this.uTextDistProductUserID1.Appearance.BackColor = Color.White;

                this.uTextDistDevelopUserID1.ButtonsRight["Search"].Visible = true;
                this.uTextDistDevelopUserID1.ReadOnly = false;
                this.uTextDistDevelopUserID1.Appearance.BackColor = Color.White;

                this.uTextDistEquipUserID1.ButtonsRight["Search"].Visible = true;
                this.uTextDistEquipUserID1.ReadOnly = false;
                this.uTextDistEquipUserID1.Appearance.BackColor = Color.White;

                this.uTextDistQualityUserID1.ButtonsRight["Search"].Visible = true;
                this.uTextDistQualityUserID1.ReadOnly = false;
                this.uTextDistQualityUserID1.Appearance.BackColor = Color.White;

                this.uTextDistEtcUserID1.ButtonsRight["Search"].Visible = true;
                this.uTextDistEtcUserID1.ReadOnly = false;
                this.uTextDistEtcUserID1.Appearance.BackColor = Color.White;

                this.uTextMRBDesc.ReadOnly = false;
                this.uTextMRBDesc.Appearance.BackColor = Color.White;

                this.uTextMRBUserID.ButtonsRight["Search"].Visible = true;
                this.uTextMRBUserID.ReadOnly = false;
                this.uTextMRBUserID.Appearance.BackColor = Color.White;

                this.uDateMRBDeliveryDate.ReadOnly = false;
                this.uDateMRBDeliveryDate.Appearance.BackColor = Color.White;

                this.uTextMRBEtcDesc.ReadOnly = false;
                this.uTextMRBEtcDesc.Appearance.BackColor = Color.White;

                this.uDateMeetingDate2.ReadOnly = false;
                this.uDateMeetingDate2.Appearance.BackColor = Color.White;

                this.uTextMeetingPlace2.ReadOnly = false;
                this.uTextMeetingPlace2.Appearance.BackColor = Color.White;

                this.uTextDistPurchaseUserID2.ButtonsRight["Search"].Visible = true;
                this.uTextDistPurchaseUserID2.ReadOnly = false;
                this.uTextDistPurchaseUserID2.Appearance.BackColor = Color.White;

                this.uTextDistProductUserID2.ButtonsRight["Search"].Visible = true;
                this.uTextDistProductUserID2.ReadOnly = false;
                this.uTextDistProductUserID2.Appearance.BackColor = Color.White;

                this.uTextDistDevelopUserID2.ButtonsRight["Search"].Visible = true;
                this.uTextDistDevelopUserID2.ReadOnly = false;
                this.uTextDistDevelopUserID2.Appearance.BackColor = Color.White;

                this.uTextDistEquipUserID2.ButtonsRight["Search"].Visible = true;
                this.uTextDistEquipUserID2.ReadOnly = false;
                this.uTextDistEquipUserID2.Appearance.BackColor = Color.White;

                this.uTextDistQualityUserID2.ButtonsRight["Search"].Visible = true;
                this.uTextDistQualityUserID2.ReadOnly = false;
                this.uTextDistQualityUserID2.Appearance.BackColor = Color.White;

                this.uTextResultDesc.ReadOnly = false;
                this.uTextResultDesc.Appearance.BackColor = Color.White;

                this.uTextResultFileName.ButtonsRight["Search"].Visible = true;
                this.uTextResultFileName.ReadOnly = false;
                this.uTextResultFileName.Appearance.BackColor = Color.White;

                this.uTextResultUserID.ButtonsRight["Search"].Visible = true;
                this.uTextResultUserID.ReadOnly = false;
                this.uTextResultUserID.Appearance.BackColor = Color.White;

                this.uDateResultDeliveryDate.ReadOnly = false;
                this.uDateResultDeliveryDate.Appearance.BackColor = Color.White;

                this.uTextResultEtcDesc.ReadOnly = false;
                this.uTextResultEtcDesc.Appearance.BackColor = Color.White;

                this.uTextAgreePurchaseUserID.ButtonsRight["Search"].Visible = true;
                this.uTextAgreePurchaseUserID.ReadOnly = false;
                this.uTextAgreePurchaseUserID.Appearance.BackColor = Color.White;

                this.uTextAgreeProductUserID.ButtonsRight["Search"].Visible = true;
                this.uTextAgreeProductUserID.ReadOnly = false;
                this.uTextAgreeProductUserID.Appearance.BackColor = Color.White;

                this.uTextAgreeDevelopUserID.ButtonsRight["Search"].Visible = true;
                this.uTextAgreeDevelopUserID.ReadOnly = false;
                this.uTextAgreeDevelopUserID.Appearance.BackColor = Color.White;

                this.uTextAgreeEquipUserID.ButtonsRight["Search"].Visible = true;
                this.uTextAgreeEquipUserID.ReadOnly = false;
                this.uTextAgreeEquipUserID.Appearance.BackColor = Color.White;

                this.uTextAgreeQualityUserID.ButtonsRight["Search"].Visible = true;
                this.uTextAgreeQualityUserID.ReadOnly = false;
                this.uTextAgreeQualityUserID.Appearance.BackColor = Color.White;

                this.uTextAgreeDesc.ReadOnly = false;
                this.uTextAgreeDesc.Appearance.BackColor = Color.White;

                this.uTextDistPurchaseUserID3.ButtonsRight["Search"].Visible = true;
                this.uTextDistPurchaseUserID3.ReadOnly = false;
                this.uTextDistPurchaseUserID3.Appearance.BackColor = Color.White;

                this.uTextDistProductUserID3.ButtonsRight["Search"].Visible = true;
                this.uTextDistProductUserID3.ReadOnly = false;
                this.uTextDistProductUserID3.Appearance.BackColor = Color.White;

                this.uTextDistDevelopUserID3.ButtonsRight["Search"].Visible = true;
                this.uTextDistDevelopUserID3.ReadOnly = false;
                this.uTextDistDevelopUserID3.Appearance.BackColor = Color.White;

                this.uTextDistEquipUserID3.ButtonsRight["Search"].Visible = true;
                this.uTextDistEquipUserID3.ReadOnly = false;
                this.uTextDistEquipUserID3.Appearance.BackColor = Color.White;


                this.uTextDistQualityUserID3.ButtonsRight["Search"].Visible = true;
                this.uTextDistQualityUserID3.ReadOnly = false;
                this.uTextDistQualityUserID3.Appearance.BackColor = Color.White;

                this.uCheckValidationDateFlag.Enabled = true;
                this.uDateValidationDate.ReadOnly = false;
                this.uDateValidationDate.Appearance.BackColor = Color.Empty;

                this.uComboDistPurchaseUserID3.ReadOnly = false;
                this.uComboDistProductUserID3.ReadOnly = false;
                this.uComboDistDevelopUserID3.ReadOnly = false;
                this.uComboDistEquipUserID3.ReadOnly = false;
                this.uComboDistQualityUserID3.ReadOnly = false;
                this.uComboDistEtcUserID3.ReadOnly = false;

                this.uGridShipInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        /// <summary>
        /// 텍스트박스 초기화
        /// </summary>
        private void InitValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //this.uDateWriteDateFrom.Value = DateTime.Now.AddDays(-7);
                //this.uDateWriteDateTo.Value = DateTime.Now;
                //this.uComboSearchPlant.Value = "";
                //this.uTextSearchVendorCode.Text = "";
                //this.uTextSearchVendorCode.MaxLength = 20;
                //this.uTextSearchVendorName.Text = "";
                //this.uTextSearchMaterialCode.Text = "";
                //this.uTextSearchMaterialCode.MaxLength = 20;
                //this.uTextSearchMaterialName.Text = "";
                //this.uTexSearchLotNo.Text = "";
                //this.uTexSearchLotNo.MaxLength = 50;
                this.uTextStdNumber.Text = "";
                this.uTextStdNumber.MaxLength = 20;
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextLotNo.Text = "";
                this.uTextLotNo.MaxLength = 50;
                this.uDateWriteDate.Value = DateTime.Now;
                this.uTextWriterID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriterID.MaxLength = 20;
                this.uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialCode.MaxLength = 20;
                this.uTextMaterialName.Text = "";
                this.uTextMaterialSpec.Text = "";
                this.uTextVendorCode.Text = "";
                this.uTextVendorCode.MaxLength = 20;
                this.uTextVendorName.Text = "";
                this.uTextQty.Text = "";
                this.uTextQty.MaxLength = 25;
                this.uTextEtcDesc.Text = "";
                this.uTextEtcDesc.MaxLength = 200;
                this.uTextReqObjectDesc.Text = "";
                this.uTextReqObjectDesc.MaxLength = 500;
                this.uTextResult.Text = "";
                this.uTextResult.MaxLength = 200;
                this.uTextDepartment.Text = "";
                this.uTextMESFlag.Text = "F";    //MESFlag
                this.uTextCompleteState.Text = "F"; //테이블 작성완료상태

                this.uDateMeetingDate1.Value = DateTime.Now;
                this.uTextMeetingPlace1.Text = "";
                this.uTextMeetingPlace1.MaxLength = 100;
                this.uTextFileName1.Text = "";
                this.uTextDistPurchaseUserID1.Text = "";
                this.uTextDistPurchaseUserID1.MaxLength = 20;
                this.uTextDistPurchaseUserName1.Text = "";
                this.uTextDistPurchaseUserDept1.Text = "";
                this.uTextDistProductUserID1.Text = "";
                this.uTextDistProductUserID1.MaxLength = 20;
                this.uTextDistProductUserName1.Text = "";
                this.uTextDistProductUserDept1.Text = "";
                this.uTextDistDevelopUserID1.Text = "";
                this.uTextDistDevelopUserID1.MaxLength = 20;
                this.uTextDistDevelopUserName1.Text = "";
                this.uTextDistDevelopUserDept1.Text = "";
                this.uTextDistEquipUserID1.Text = "";
                this.uTextDistEquipUserID1.MaxLength = 20;
                this.uTextDistEquipUserName1.Text = "";
                this.uTextDistEquipUserDept1.Text = "";
                this.uTextDistQualityUserID1.Text = "";
                this.uTextDistQualityUserID1.MaxLength = 20;
                this.uTextDistQualityUserName1.Text = "";
                this.uTextDistQualityUserDept1.Text = "";
                this.uTextDistEtcUserID1.Text = "";
                this.uTextDistEtcUserID1.MaxLength = 20;
                this.uTextDistEtcUserName1.Text = "";
                this.uTextDistEtcUserDept1.Text = "";
                this.uTextMRBDesc.Text = "";
                this.uTextMRBDesc.MaxLength = 500;
                this.uTextMRBUserID.Text = "";
                this.uTextMRBUserName.Text = "";
                this.uDateMRBDeliveryDate.Value = DateTime.Now;
                this.uTextMRBEtcDesc.Text = "";
                this.uTextMRBEtcDesc.MaxLength = 200;

                this.uDateMeetingDate2.Value = DateTime.Now;
                this.uTextMeetingPlace2.Text = "";
                this.uTextMeetingPlace2.MaxLength = 100;
                this.uTextDistPurchaseUserID2.Text = "";
                this.uTextDistPurchaseUserID2.MaxLength = 20;
                this.uTextDistPurchaseUserName2.Text = "";
                this.uTextDistPurchaseUserDept2.Text = "";
                this.uTextDistProductUserID2.Text = "";
                this.uTextDistProductUserID2.MaxLength = 20;
                this.uTextDistProductUserName2.Text = "";
                this.uTextDistProductUserDept2.Text = "";
                this.uTextDistQualityUserID2.Text = "";
                this.uTextDistQualityUserID2.MaxLength = 20;
                this.uTextDistQualityUserName2.Text = "";
                this.uTextDistQualityUserDept2.Text = "";
                this.uTextDistProductUserID2.Text = "";
                this.uTextDistProductUserID2.MaxLength = 20;
                this.uTextDistProductUserName2.Text = "";
                this.uTextDistProductUserDept2.Text = "";
                this.uTextDistEquipUserID2.Text = "";
                this.uTextDistEquipUserID2.MaxLength = 20;
                this.uTextDistEquipUserName2.Text = "";
                this.uTextDistEquipUserDept2.Text = "";
                this.uTextDistDevelopUserID2.Text = "";
                this.uTextDistDevelopUserID2.MaxLength = 20;
                this.uTextDistDevelopUserName2.Text = "";
                this.uTextDistDevelopUserDept2.Text = "";
                this.uTextResultDesc.Text = "";
                this.uTextResultDesc.MaxLength = 500;
                this.uTextResultFileName.Text = "";
                this.uTextResultUserID.Text = "";
                this.uTextResultUserID.MaxLength = 20;
                this.uTextResultUserName.Text = "";
                this.uDateResultDeliveryDate.Value = DateTime.Now;
                this.uTextResultEtcDesc.Text = "";
                this.uTextResultEtcDesc.MaxLength = 200;

                this.uTextAgreePurchaseUserID.Text = "";
                this.uTextAgreePurchaseUserID.MaxLength = 20;
                this.uTextAgreePurchaseUserName.Text = "";
                this.uTextAgreeProductUserID.Text = "";
                this.uTextAgreeProductUserID.MaxLength = 20;
                this.uTextAgreeProductUserName.Text = "";
                this.uTextAgreeDevelopUserID.Text = "";
                this.uTextAgreeDevelopUserID.MaxLength = 20;
                this.uTextAgreeDevelopUserName.Text = "";
                this.uTextAgreeEquipUserID.Text = "";
                this.uTextAgreeEquipUserID.MaxLength = 20;
                this.uTextAgreeEquipUserName.Text = "";
                this.uTextAgreeQualityUserID.Text = "";
                this.uTextAgreeQualityUserID.MaxLength = 20;
                this.uTextAgreeQualityUserName.Text = "";
                this.uTextAgreeDesc.Text = "";
                this.uTextAgreeDesc.MaxLength = 500;


                this.uTextDistPurchaseUserID3.Text = "";
                this.uTextDistPurchaseUserID3.MaxLength = 20;
                this.uTextDistPurchaseUserName3.Text = "";
                this.uTextDistProductUserID3.Text = "";
                this.uTextDistProductUserID3.MaxLength = 20;
                this.uTextDistProductUserName3.Text = "";
                this.uTextDistDevelopUserID3.Text = "";
                this.uTextDistDevelopUserID3.MaxLength = 20;
                this.uTextDistDevelopUserName3.Text = "";
                this.uTextDistEquipUserID3.Text = "";
                this.uTextDistEquipUserID3.MaxLength = 20;
                this.uTextDistEquipUserName3.Text = "";
                this.uTextDistQualityUserID3.Text = "";
                this.uTextDistQualityUserID3.MaxLength = 20;
                this.uTextDistQualityUserName3.Text = "";

                this.uComboDistPurchaseUserID3.Value = "";
                this.uComboDistProductUserID3.Value = "";
                this.uComboDistDevelopUserID3.Value = "";
                this.uComboDistEquipUserID3.Value = "";
                this.uComboDistQualityUserID3.Value = "";
                this.uComboDistEtcUserID3.Value = "";
                this.uCheckReturn.Checked = false;
                this.uCheckReturn.Enabled = false;

                //스크롤바
                //this.uTextFrom.Scrollbars = ScrollBars.Vertical;
                //this.uTextFrom.SelectionStart = uTextFrom.Text.Length;
                //this.uTextFrom.ScrollToCaret();
                this.uTextReqObjectDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextReqObjectDesc.SelectionStart = uTextReqObjectDesc.Text.Length;
                this.uTextReqObjectDesc.ScrollToCaret();

                this.uTextMRBDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextMRBDesc.SelectionStart = uTextMRBDesc.Text.Length;
                this.uTextMRBDesc.ScrollToCaret();

                this.uTextResultDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextResultDesc.SelectionStart = uTextResultDesc.Text.Length;
                this.uTextResultDesc.ScrollToCaret();

                this.uTextAgreeDesc.Scrollbars = ScrollBars.Vertical;
                this.uTextAgreeDesc.SelectionStart = uTextAgreeDesc.Text.Length;
                this.uTextAgreeDesc.ScrollToCaret();

                this.uCheckValidationDateFlag.Checked = false;
                this.uOptionInspectResult.Value = "";

                this.uCheckCompleteFlag.CheckedValue = false;

                while (this.uGridShipInfo.Rows.Count > 0)
                {
                    this.uGridShipInfo.Rows[0].Delete(false);
                }

                uCheckGWTFlag.Checked = false;
                uCheckGWResultState.Checked = false;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                QRPINS.BL.INSSTS.INSMaterialSpecial MaterialSpecial;
                //디버깅모드인지 아닌지에 따라 생성자가 붙는다.
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecial), "INSMaterialSpecial");
                    MaterialSpecial = new QRPINS.BL.INSSTS.INSMaterialSpecial();
                    brwChannel.mfCredentials(MaterialSpecial);
                }
                else
                {
                    MaterialSpecial = new QRPINS.BL.INSSTS.INSMaterialSpecial(m_strDBConn);
                }

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strVendorCode = this.uTextSearchVendorCode.Text;
                String strMaterialCode = this.uTextSearchMaterialCode.Text;
                String strLotNo = this.uTexSearchLotNo.Text;
                String strWriteDateFrom = Convert.ToDateTime(this.uDateWriteDateFrom.Value).ToString("yyyy-MM-dd");
                String strWriteDateTo = Convert.ToDateTime(this.uDateWriteDateTo.Value).ToString("yyyy-MM-dd");

                DataTable dt = MaterialSpecial.mfReadMaterialSpecial(strPlantCode, strVendorCode, strMaterialCode, strLotNo, strWriteDateFrom
                                                                        , strWriteDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGridInfo.DataSource = dt;
                this.uGridInfo.DataBind();

                // 정보가 있을 경우
                if (dt.Rows.Count > 0)
                {
                    // 수입검사결과 가 공백이아니고 작성완료여부가 T 이고 MESTFlag 가 F 인 줄을 찾아서 색을 바꾼다.
                    for (int i = 0; i < this.uGridInfo.Rows.Count; i++)
                    {
                        //if (this.uGridInfo.Rows[i].Cells["InspectResult"].Value.ToString() != "" && this.uGridInfo.Rows[i].Cells["CompleteFlag"].Value.ToString() == "T" && this.uGridInfo.Rows[i].Cells["MESTFlag"].Value.ToString() == "F")
                        if (this.uGridInfo.Rows[i].Cells["CompleteFlag"].Value.ToString() != "T")
                        {
                            // 색이 변하지 않음 흠...
                            this.uGridInfo.Rows[i].Appearance.BackColor = Color.Salmon;
                            //this.uGridInfo.DisplayLayout.RowConnectorColor = Color.Salmon;
                        }
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridInfo, 0);
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                
                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }

                this.uGroupBoxContentsArea.Expanded = false;
                //this.uTextMESFlag.Text = "";    //MESTFlag
                //this.uTextCompleteState.Clear(); //CompleteState

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                //// 결재선 팝업창
                //QRPCOM.UI.frmCOM0012 frm = new QRPCOM.UI.frmCOM0012();
                QRPCOM.UI.frmCOM0013 frm = new QRPCOM.UI.frmCOM0013();
                
                DialogResult _dr = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001027", "M001028", Infragistics.Win.HAlign.Right);
                }
                else
                {

                    #region 필수확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        // Set Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uTextLotNo.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000078", Infragistics.Win.HAlign.Center);

                        // Set Focus
                        this.uTextLotNo.Focus();
                        return;
                    }
                    else if (this.uTextQty.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000729", Infragistics.Win.HAlign.Center);

                        // Set Focus
                        this.uTextQty.Focus();
                        return;
                    }
                    else if (this.uTextWriterID.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001000", Infragistics.Win.HAlign.Center);

                        // Set Focus
                        this.uTextWriterID.Focus();
                        return;
                    }
                    else if (this.uCheckValidationDateFlag.Checked && this.uDateValidationDate.Value.Equals(null))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000834", Infragistics.Win.HAlign.Center);

                        // Set Focus
                        this.uDateValidationDate.DropDown();
                        return;
                    }
                    else if (this.uOptionInspectResult.Value == null)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000752", Infragistics.Win.HAlign.Center);

                        // Set Focus
                        this.uDateValidationDate.DropDown();
                        return;
                    }
                    //else if (uTextCompleteState.Text.Equals("T") && uCheckGWTFlag.Checked && uCheckGWResultState.Checked)
                    //{
                    //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                    , "확인창", "입력사항 확인", "MES/WMS 전송할 내용이 없습니다.", Infragistics.Win.HAlign.Center);

                    //    // Set Focus
                    //    return;
                    //}
                    #endregion

                    else
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        #region BL 호출
                        //BL호출
                        QRPBrowser brwChannel = new QRPBrowser();

                        QRPINS.BL.INSSTS.INSMaterialSpecial INSms;
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess;
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath;

                        //파일 서버 연결
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);


                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);

                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecialShip), "INSMaterialSpecialShip");
                        QRPINS.BL.INSSTS.INSMaterialSpecialShip clsShip = new QRPINS.BL.INSSTS.INSMaterialSpecialShip();
                        brwChannel.mfCredentials(clsShip);

                        if (m_bolDebugMode == false)   //실행용
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecial), "INSMaterialSpecial");
                            INSms = new QRPINS.BL.INSSTS.INSMaterialSpecial();
                            brwChannel.mfCredentials(INSms);
                        }
                        else //디버깅용
                        {
                            INSms = new QRPINS.BL.INSSTS.INSMaterialSpecial(m_strDBConn);
                        }
                        #endregion

                        #region 데이터
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0009");

                        //수정시 파일을 첨부할 경우 파일 이름을 일괄적으로 변경하여 파일명 변경은 SP에서 처리
                        String strAttFileName = "";
                        String strAttResultFileName = "";

                        if (this.uTextFileName1.Text.Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uTextFileName1.Text);
                            strAttFileName = fileDoc.Name;
                        }
                        else if (this.uTextFileName1.Text == "")
                        {
                            strAttFileName = "";
                        }
                        else
                        {
                            String fName = this.uTextFileName1.Text;
                            String[] splitName = fName.Split(new char[] { '-' });

                            if (splitName.Length > 4)
                            {
                                for (int j = 3; j < splitName.Length; j++)
                                {
                                    if (!j.Equals(splitName.Length - 1))
                                        strAttFileName = strAttFileName + splitName[j] + "-";
                                    else
                                        strAttFileName = strAttFileName + splitName[j];
                                }
                            }
                            else
                            {
                                strAttFileName = splitName[3];
                            }
                        }

                        if (this.uTextResultFileName.Text.Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uTextResultFileName.Text);
                            strAttResultFileName = fileDoc.Name;
                        }
                        else if (this.uTextResultFileName.Text == "")
                        {
                            strAttResultFileName = "";
                        }
                        else
                        {
                            String fName = this.uTextResultFileName.Text;
                            String[] splitName = fName.Split(new char[] { '-' });
                            if (splitName.Length > 4)
                            {
                                for (int j = 3; j < splitName.Length; j++)
                                {
                                    if (!j.Equals(splitName.Length - 1))
                                    {
                                        strAttResultFileName = strAttResultFileName + splitName[j] + "-";
                                    }
                                    else
                                        strAttResultFileName = strAttResultFileName + splitName[j];
                                }
                            }
                            else
                            {
                                strAttResultFileName = splitName[3];
                            }
                        }

                        DataTable dt = INSms.mfSetDataInfo();
                        DataTable dtShip = clsShip.mfSetDataInfo();
                        DataTable dtShipCheck4GW = clsShip.mfSetDataInfo(); //선택한 Lot만 전자결재로 보내도록 함
                        DataRow dr = dt.NewRow();
                        
                        dr["PlantCode"] = this.uComboPlant.Value.ToString();
                        dr["StdNumber"] = this.uTextStdNumber.Text;
                        dr["GRNo"] = this.uTextGRNo.Text;
                        dr["MaterialCode"] = this.uTextMaterialCode.Text;
                        dr["MaterialSpec"] = uTextMaterialSpec.Text;
                        dr["VendorCode"] = this.uTextVendorCode.Text;
                        dr["VendorName"] = uTextVendorName.Text;
                        dr["LotNo"] = this.uTextLotNo.Text;
                        dr["WriteUserID"] = this.uTextWriterID.Text;
                        dr["WriteDate"] = DateTime.Parse(this.uDateWriteDate.Value.ToString()).ToString("yyyy-MM-dd");
                        dr["Qty"] = Convert.ToDecimal(this.uTextQty.Text);
                        dr["EtcDesc"] = this.uTextEtcDesc.Text;
                        dr["ReqObjectDesc"] = this.uTextReqObjectDesc.Text;
                        dr["Result"] = this.uTextResult.Text;
                        dr["GWTFlag"] = uCheckGWTFlag.Checked == true ? "T" : "F";
                        if (this.uOptionInspectResult.Value != null)
                        {
                            dr["InspectResult"] = this.uOptionInspectResult.Value.ToString();
                        }
                        else
                        {
                            dr["InspectResult"] = "";
                        }
                        dr["MeetingDate1"] = Convert.ToDateTime(this.uDateMeetingDate1.Value).ToString("yyyy-MM-dd");
                        dr["MeetingPlace1"] = this.uTextMeetingPlace1.Text;
                        dr["FileName1"] = strAttFileName;

                        dr["DistPurchaseUserID1"] = this.uTextDistPurchaseUserID1.Text;
                        dr["DistPurchaseUserName1"] = this.uTextDistPurchaseUserName1.Text;
                        dr["DistPurchaseDeptName1"] = this.uTextDistPurchaseUserDept1.Text;

                        dr["DistProductUserID1"] = this.uTextDistProductUserID1.Text;
                        dr["DistProductUserName1"] = this.uTextDistProductUserName1.Text;
                        dr["DistProductDeptName1"] = this.uTextDistProductUserDept1.Text;

                        dr["DistDevelopUserID1"] = this.uTextDistDevelopUserID1.Text;
                        dr["DistDevelopUserName1"] = this.uTextDistDevelopUserName1.Text;
                        dr["DistDevelopDeptName1"] = this.uTextDistDevelopUserDept1.Text;

                        dr["DistEquipUserID1"] = this.uTextDistEquipUserID1.Text;
                        dr["DistEquipUserName1"] = this.uTextDistEquipUserName1.Text;
                        dr["DistEquipDeptName1"] = this.uTextDistEquipUserDept1.Text;

                        dr["DistQualityUserID1"] = this.uTextDistQualityUserID1.Text;
                        dr["DistQualityUserName1"] = this.uTextDistQualityUserName1.Text;
                        dr["DistQualityDeptName1"] = this.uTextDistQualityUserDept1.Text;

                        dr["DistEtcUserID1"] = this.uTextDistEtcUserID1.Text;
                        dr["DistEtcUserName1"] = this.uTextDistEtcUserName1.Text;
                        dr["DistEtcDeptName1"] = this.uTextDistEtcUserDept1.Text;

                        dr["MRBDesc"] = this.uTextMRBDesc.Text;
                        dr["MRBUserID"] = this.uTextMRBUserID.Text;
                        dr["MRBUserName"] = this.uTextMRBUserName.Text;
                        dr["MRBDeliveryDate"] = Convert.ToDateTime(this.uDateMRBDeliveryDate.Value).ToString("yyyy-MM-dd");
                        dr["MRBEtcDesc"] = this.uTextMRBEtcDesc.Text;
                        dr["MeetingDate2"] = Convert.ToDateTime(this.uDateMeetingDate2.Value).ToString("yyyy-MM-dd");
                        dr["MeetingPlace2"] = this.uTextMeetingPlace2.Text;
                        dr["DistPurchaseUserID2"] = this.uTextDistPurchaseUserID2.Text;
                        dr["DistPurchaseUserName2"] = this.uTextDistPurchaseUserName2.Text;
                        dr["DistPurchaseDeptName2"] = this.uTextDistPurchaseUserDept2.Text;

                        dr["DistProductUserID2"] = this.uTextDistProductUserID2.Text;
                        dr["DistProductUserName2"] = this.uTextDistProductUserName2.Text;
                        dr["DistProductDeptName2"] = this.uTextDistProductUserDept2.Text;

                        dr["DistDevelopUserID2"] = this.uTextDistDevelopUserID2.Text;
                        dr["DistDevelopUserName2"] = this.uTextDistDevelopUserName2.Text;
                        dr["DistDevelopDeptName2"] = this.uTextDistDevelopUserDept2.Text;

                        dr["DistEquipUserID2"] = this.uTextDistEquipUserID2.Text;
                        dr["DistEquipUserName2"] = this.uTextDistEquipUserName2.Text;
                        dr["DistEquipDeptName2"] = this.uTextDistEquipUserDept2.Text;

                        dr["DistQualityUserID2"] = this.uTextDistQualityUserID2.Text;
                        dr["DistQualityUserName2"] = this.uTextDistQualityUserName2.Text;
                        dr["DistQualityDeptName2"] = this.uTextDistQualityUserDept2.Text;

                        dr["DIstEtcUserID2"] = this.uTextDistEtcUserID2.Text;
                        dr["DistEtcUserName2"] = this.uTextDistEtcUserName2.Text;
                        dr["DistEtcDeptName2"] = this.uTextDistEtcUserDept2.Text;

                        dr["ResultDesc"] = this.uTextResultDesc.Text;
                        dr["ResultFileName"] = strAttResultFileName;
                        dr["ResultUserID"] = this.uTextResultUserID.Text;
                        dr["ResultUserName"] = uTextResultUserName.Text;
                        dr["ResultDeliveryDate"] = Convert.ToDateTime(this.uDateResultDeliveryDate.Value).ToString("yyyy-MM-dd");

                        dr["ResultEtcDesc"] = this.uTextResultEtcDesc.Text;
                        dr["AgreePurchaseUserID"] = this.uTextAgreePurchaseUserID.Text;
                        dr["AgreePurchaseUserName"] = this.uTextAgreePurchaseUserName.Text;
                        dr["AgreeProductUserID"] = this.uTextAgreeProductUserID.Text;
                        dr["AgreeProductUserName"] = this.uTextAgreeProductUserName.Text;
                        dr["AgreeDevelopUserID"] = this.uTextAgreeDevelopUserID.Text;
                        dr["AgreeDevelopUserName"] = this.uTextAgreeDevelopUserName.Text;
                        dr["AgreeEquipUserID"] = this.uTextAgreeEquipUserID.Text;
                        dr["AgreeEquipUserName"] = this.uTextAgreeEquipUserName.Text;
                        dr["AgreeQualityUserID"] = this.uTextAgreeQualityUserID.Text;
                        dr["AgreeQualityUserName"] = this.uTextAgreeQualityUserName.Text;

                        dr["AgreeDesc"] = this.uTextAgreeDesc.Text;
                        dr["DistPurchaseUserID3"] = this.uComboDistPurchaseUserID3.Value.ToString();
                        if (uComboDistPurchaseUserID3.Value.ToString().Equals(string.Empty))
                            dr["DistPurchaseDeptName3"] = string.Empty;
                        else
                            dr["DistPurchaseDeptName3"] = this.uComboDistPurchaseUserID3.Text;

                        dr["DistProductUserID3"] = this.uComboDistProductUserID3.Value.ToString();
                        if(uComboDistProductUserID3.Value.ToString().Equals(string.Empty))
                            dr["DistProductDeptName3"] = string.Empty;
                        else
                            dr["DistProductDeptName3"] = this.uComboDistProductUserID3.Text;

                        dr["DistDevelopUserID3"] = this.uComboDistDevelopUserID3.Value.ToString();
                        if (uComboDistDevelopUserID3.Value.ToString().Equals(string.Empty))
                            dr["DistDevelopDeptName3"] = string.Empty;
                        else
                            dr["DistDevelopDeptName3"] = this.uComboDistDevelopUserID3.Text;

                        dr["DistEquipUserID3"] = this.uComboDistEquipUserID3.Value.ToString();
                        if (uComboDistEquipUserID3.Value.ToString().Equals(string.Empty))
                            dr["DistEquipDeptName3"] = string.Empty;
                        else
                            dr["DistEquipDeptName3"] = this.uComboDistEquipUserID3.Text;

                        dr["DistQualityUserID3"] = this.uComboDistQualityUserID3.Value.ToString();
                        if (uComboDistQualityUserID3.Value.ToString().Equals(string.Empty))
                            dr["DistQualityDeptName3"] = string.Empty;
                        else
                            dr["DistQualityDeptName3"] = this.uComboDistQualityUserID3.Text;

                        dr["DistEtcUserID3"] = this.uComboDistEtcUserID3.Value.ToString();
                        if (uComboDistEtcUserID3.Value.ToString().Equals(string.Empty))
                            dr["DistEtcDeptName3"] = string.Empty;
                        else
                            dr["DistEtcDeptName3"] = this.uComboDistEtcUserID3.Text;

                        dr["CompleteFlag"] = this.uCheckCompleteFlag.CheckedValue;
                        dr["MESTFlag"] = this.uTextMESFlag.Text; //MESFlag
                        dr["CompleteSate"] = this.uTextCompleteState.Text; //테이블안 작성완료상태

                        dt.Rows.Add(dr);

                        //Ship정보 저장
                        for (int i = 0; i < this.uGridShipInfo.Rows.Count; i++)
                        {
                            dr = dtShip.NewRow();
                            
                            dr["PlantCode"] = this.uComboPlant.Value.ToString();
                            dr["LotSeq"] = this.uGridShipInfo.Rows[i].Cells["LotSeq"].Value;

                            if (Convert.ToBoolean(this.uGridShipInfo.Rows[i].Cells["LotCheckFlag"].Value) == true)
                            {
                                dr["LotCheckFlag"] = "T";
                            }
                            else
                            {
                                dr["LotCheckFlag"] = "F";
                            }
                            dr["LotNo"] = this.uGridShipInfo.Rows[i].Cells["LotNo"].Value.ToString();
                            dr["GRNo"] = this.uGridShipInfo.Rows[i].Cells["GRNo"].Value.ToString();
                            dr["GRQty"] = this.uGridShipInfo.Rows[i].Cells["GRQty"].Value.ToString();
                            dr["GRDate"] = this.uGridShipInfo.Rows[i].Cells["GRDate"].Value.ToString();
                            dr["EtcDesc"] = this.uGridShipInfo.Rows[i].Cells["EtcDesc"].Value.ToString();
                            dr["MaterialCode"] = this.uGridShipInfo.Rows[i].Cells["MaterialCode"].Value.ToString();
                            dr["MaterialName"] = this.uGridShipInfo.Rows[i].Cells["MaterialName"].Value.ToString();
                            dr["VendorCode"] = this.uGridShipInfo.Rows[i].Cells["VendorCode"].Value.ToString();
                            dr["VendorName"] = this.uGridShipInfo.Rows[i].Cells["VendorName"].Value.ToString();

                            dtShip.Rows.Add(dr);

                            //선택한 Lot만 전자결재로 보내도록 함
                            if (Convert.ToBoolean(this.uGridShipInfo.Rows[i].Cells["LotCheckFlag"].Value) == true)
                            {
                                DataRow drShip = dtShipCheck4GW.NewRow();

                                drShip["PlantCode"] = this.uComboPlant.Value.ToString();
                                drShip["LotSeq"] = this.uGridShipInfo.Rows[i].Cells["LotSeq"].Value;

                                if (Convert.ToBoolean(this.uGridShipInfo.Rows[i].Cells["LotCheckFlag"].Value) == true)
                                {
                                    drShip["LotCheckFlag"] = "T";
                                }
                                else
                                {
                                    drShip["LotCheckFlag"] = "F";
                                }
                                drShip["LotNo"] = this.uGridShipInfo.Rows[i].Cells["LotNo"].Value.ToString();
                                drShip["GRNo"] = this.uGridShipInfo.Rows[i].Cells["GRNo"].Value.ToString();
                                drShip["GRQty"] = this.uGridShipInfo.Rows[i].Cells["GRQty"].Value.ToString();
                                drShip["GRDate"] = this.uGridShipInfo.Rows[i].Cells["GRDate"].Value.ToString();
                                drShip["EtcDesc"] = this.uGridShipInfo.Rows[i].Cells["EtcDesc"].Value.ToString();
                                drShip["MaterialCode"] = this.uGridShipInfo.Rows[i].Cells["MaterialCode"].Value.ToString();
                                drShip["MaterialName"] = this.uGridShipInfo.Rows[i].Cells["MaterialName"].Value.ToString();
                                drShip["VendorCode"] = this.uGridShipInfo.Rows[i].Cells["VendorCode"].Value.ToString();
                                drShip["VendorName"] = this.uGridShipInfo.Rows[i].Cells["VendorName"].Value.ToString();

                                dtShipCheck4GW.Rows.Add(drShip);
                            }
                        }
                        DataTable dtWMS = INSms.mfSetWMSDataTable();

                        if (this.uCheckCompleteFlag.Checked && !this.uTextWMSTFlag.Text.Equals("T"))
                        {
                            // Ship단위로 WMS에 정보를 전송한다.
                            for (int i = 0; i < this.uGridShipInfo.Rows.Count; i++)
                            {
                                if (Convert.ToBoolean(this.uGridShipInfo.Rows[i].Cells["LotCheckFlag"].Value).Equals(true))
                                {
                                    dr = dtWMS.NewRow();
                                    dr["PlantCode"] = this.uComboPlant.Value.ToString();
                                    dr["GRNo"] = this.uGridShipInfo.Rows[i].Cells["GRNo"].Value.ToString();
                                    dr["LotNo"] = this.uGridShipInfo.Rows[i].Cells["LotNo"].Value.ToString();
                                    if (uOptionInspectResult.CheckedIndex.Equals(0) || uOptionInspectResult.CheckedIndex.Equals(2))
                                    {
                                        dr["InspectResultFlag"] = "T";
                                    }
                                    else if (uOptionInspectResult.CheckedIndex.Equals(1))
                                    {
                                        dr["InspectResultFlag"] = "F";
                                    }
                                    if (this.uCheckValidationDateFlag.Checked)
                                    {
                                        dr["ValidationDateFlag"] = "T";
                                        if (this.uDateValidationDate.Value != null)
                                            dr["ValidationDate"] = Convert.ToDateTime(this.uDateValidationDate.Value).ToString("yyyy-MM-dd");
                                    }
                                    else
                                        dr["ValidationDateFlag"] = "F";
                                    //dr["IFFlag"] = "S";
                                    dr["IFFlag"] = "MRB";
                                    dr["AdmitDesc"] = this.uTextResult.Text.ToString();
                                    dtWMS.Rows.Add(dr);
                                }
                            }
                        }
                        #endregion

                        DialogResult r = new DialogResult();

                        if (dt.Rows.Count > 0)
                        {
                            DialogResult dirResult = new DialogResult();

                            if (!uCheckGWTFlag.Checked && !uCheckGWResultState.Checked && !dt.Rows[0]["MESTFlag"].ToString().Equals("T") && !this.uTextWMSTFlag.Text.Equals("T"))
                            {
                                r = msg.mfSetMessageBox(MessageBoxType.ReqAgreeSave, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000234", Infragistics.Win.HAlign.Right);

                                if (r == DialogResult.Yes)
                                {
                                    #region 그룹웨어 결재요청
                                    dirResult = DialogResult.Yes;

                                    //결재선 팝업창 호출
                                    frm.WriteID = uTextWriterID.Text;
                                    frm.WriteName = uTextWriteName.Text;
                                    _dr = frm.ShowDialog();

                                    if (_dr == DialogResult.No || _dr == DialogResult.Cancel)
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001264", "M001228", "M001030", Infragistics.Win.HAlign.Center);

                                        return;
                                    }
                                    #endregion
                                }
                                else if (r == DialogResult.No)
                                {
                                    dirResult = DialogResult.Yes;
                                }
                                else if (r == DialogResult.Cancel)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001228", "M001030", Infragistics.Win.HAlign.Center);
                                    return;
                                }
                            }
                            else
                            {
                                #region 저장 종류 확인
                                //작성완료와 MES 전송이 완료된 정보면 메세지 박스를 띄운다.
                                if (dt.Rows[0]["CompleteSate"].ToString().Equals("T") && dt.Rows[0]["MESTFlag"].ToString().Equals("T") && this.uTextWMSTFlag.Text.Equals("T") && dt.Rows[0]["GWTFlag"].ToString().Equals("T"))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001075", "M000985", Infragistics.Win.HAlign.Right);

                                    return;
                                }
                                else
                                {
                                    //작성완료 체크박스가
                                    if (dt.Rows[0]["CompleteFlag"].ToString().Equals("True") &&
                                        dt.Rows[0]["InspectResult"].ToString() != "" &&
                                        dt.Rows[0]["MESTFlag"].ToString().Equals("F") &&
                                        dt.Rows[0]["CompleteSate"].ToString().Equals("F") &&
                                        !this.uTextWMSTFlag.Text.Equals("T"))
                                    {
                                        dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001075", "M000995", Infragistics.Win.HAlign.Right);
                                    }

                                    else if (dt.Rows[0]["CompleteSate"].ToString().Equals("T") &&
                                        dt.Rows[0]["MESTFlag"].ToString().Equals("F") &&
                                        this.uTextWMSTFlag.Text.Equals("T"))
                                    {
                                        dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001053", "M000988", Infragistics.Win.HAlign.Right);
                                    }

                                    else if (dt.Rows[0]["CompleteSate"].ToString().Equals("T") &&
                                        dt.Rows[0]["MESTFlag"].ToString().Equals("T") &&
                                        !this.uTextWMSTFlag.Text.Equals("T"))
                                    {
                                        dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001053", "M000990", Infragistics.Win.HAlign.Right);
                                    }

                                    else if (dt.Rows[0]["CompleteSate"].ToString().Equals("T") &&
                                        dt.Rows[0]["MESTFlag"].ToString().Equals("F") &&
                                        !this.uTextWMSTFlag.Text.Equals("T"))
                                    {
                                        dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001053", "M000989", Infragistics.Win.HAlign.Right);
                                    }
                                    else if (uCheckGWTFlag.Checked && !uCheckGWResultState.Checked)
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001075", "M000232", Infragistics.Win.HAlign.Right);

                                        return;
                                    }
                                    else
                                    {
                                        dirResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right);
                                    }
                                }
                                #endregion
                            }

                            if (dirResult == DialogResult.Yes)
                            {
                                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                Thread t1 = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                String strErrRtn = INSms.mfSaveMaterialSpecial(dt, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), this.Name, dtWMS, dtShip);

                                TransErrRtn ErrRtn = new TransErrRtn();
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                String strRtnStdNumber = string.Empty;//ErrRtn.mfGetReturnValue(0);

                                if (!dt.Rows[0]["StdNumber"].ToString().Equals(string.Empty))
                                {
                                    strRtnStdNumber = dt.Rows[0]["StdNumber"].ToString();
                                }
                                else
                                {
                                    strRtnStdNumber = ErrRtn.mfGetReturnValue(0);
                                }

                                //처리결과에 따른 메세지 박스
                                if (ErrRtn.ErrNum == 0)
                                {
                                    #region 파일업로드
                                    //--업데이트시 에러위험 업데이트시 텍스트 파일 정보는 uComboPlant.Value + "-" + strRtnStdNumber + "-Sec-" + fileDoc.Name 상태 제어필요
                                    //File1 Upload
                                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                    ArrayList arrFile = new ArrayList();
                                    FileInfo fileDoc;
                                    String strUploadFile;
                                    String strPlantCode = this.uComboPlant.Value.ToString();

                                    if (this.uTextFileName1.Text.Contains("\\"))
                                    {
                                        //파일 이름 변경(PlantCode-StdNumber-Fir-FileName)
                                        fileDoc = new FileInfo(this.uTextFileName1.Text);

                                        strUploadFile = fileDoc.DirectoryName + "\\" + strPlantCode + "-" + strRtnStdNumber + "-Fir-" + fileDoc.Name;

                                        //파일이 있으면 삭제
                                        if (File.Exists(strUploadFile))
                                            File.Delete(strUploadFile);

                                        //변경한 파일 이름으로 복사
                                        File.Copy(this.uTextFileName1.Text, strUploadFile);
                                        arrFile.Add(strUploadFile);
                                    }



                                    if (this.uTextResultFileName.Text.Contains("\\"))
                                    {
                                        //ResultFile 이름 변경
                                        fileDoc = new FileInfo(this.uTextResultFileName.Text);
                                        strUploadFile = fileDoc.DirectoryName + "\\" + strPlantCode + "-" + strRtnStdNumber + "-Sec-" + fileDoc.Name;

                                        //변경한 파일이 있으면 삭제
                                        if (File.Exists(strUploadFile))
                                            File.Delete(strUploadFile);
                                        //변경한 파일 이름으로 복사
                                        File.Copy(this.uTextResultFileName.Text, strUploadFile);
                                        arrFile.Add(strUploadFile);
                                    }
                                    //Upload정보 설정
                                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                    if (arrFile.Count != 0)
                                    {
                                        //fileAtt.ShowDialog();
                                        fileAtt.mfFileUpload_NonAssync();
                                    }
                                    #endregion

                                    if (r == DialogResult.Yes && !uCheckGWTFlag.Checked && !uCheckGWResultState.Checked && ErrRtn.ErrNum.Equals(0) && !strRtnStdNumber.Equals(string.Empty))
                                    {
                                        #region 그룹웨어 전송
                                        dt.Rows[0]["StdNumber"] = strRtnStdNumber;

                                        ////////테스트코드

                                        //////for (int i = 0; i < frm.dtSendLine.Rows.Count; i++)
                                        //////{
                                        //////    frm.dtSendLine.Rows[i]["CD_USERKEY"] = "tica100";
                                        //////    frm.dtSendLine.Rows[i]["UserName"] = "이종민";
                                        //////    frm.dtSendLine.Rows[i]["DeptName"] = "ERP추진팀";
                                        //////}

                                        //선택한 Lot만 전자결재 보내도록 수정//
                                        DataTable dtRtn = INSms.mfSaveMaterialSpecialGRWApproval(dt, dtShipCheck4GW, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                                        //DataTable dtRtn = INSms.mfSaveMaterialSpecialGRWApproval(dt, dtShip, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                                        //DataTable dtRtn = mfSaveMaterialSpecialGRWApproval(dt, dtShip, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                                        
                                        if (dtRtn == null)
                                        {

                                            this.MdiParent.Cursor = Cursors.Default;
                                            m_ProgressPopup.mfCloseProgressPopup(this);

                                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                               "M001135", "M001037", "M000328",
                                                               Infragistics.Win.HAlign.Right);
                                            return;
                                        }
                                        else if (dtRtn.Rows.Count.Equals(0))
                                        {
                                            this.MdiParent.Cursor = Cursors.Default;
                                            m_ProgressPopup.mfCloseProgressPopup(this);

                                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                               "M001135", "M001037", "M000328",
                                                               Infragistics.Win.HAlign.Right);
                                            return;
                                        }
                                        else
                                        {
                                            if (!dtRtn.Rows[0]["CD_CODE"].ToString().Equals("00") || !Convert.ToBoolean(dtRtn.Rows[0]["CD_STATUS"].ToString()))
                                            {
                                                this.MdiParent.Cursor = Cursors.Default;
                                                m_ProgressPopup.mfCloseProgressPopup(this);
                                                string strLang = m_resSys.GetString("SYS_LANG");
                                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                   msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                                   , dtRtn.Rows[0]["MSG"].ToString(),
                                                                   Infragistics.Win.HAlign.Right);
                                                return;
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (!ErrRtn.ErrNum.Equals(0))
                                    {
                                        this.MdiParent.Cursor = Cursors.Default;
                                        m_ProgressPopup.mfCloseProgressPopup(this);

                                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M001037", "M000953",
                                                           Infragistics.Win.HAlign.Right);
                                        return;
                                    }
                                    else if (strRtnStdNumber.Equals(string.Empty))
                                    {
                                        this.MdiParent.Cursor = Cursors.Default;
                                        m_ProgressPopup.mfCloseProgressPopup(this);

                                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M001037", "M000131",
                                                           Infragistics.Win.HAlign.Right);
                                        return;
                                    }

                                    //this.MdiParent.Cursor = Cursors.Default;
                                    //m_ProgressPopup.mfCloseProgressPopup(this);


                                    //MES 처리결과에 따라서 메세지 박스를 보여준다.
                                    if (dt.Rows[0]["CompleteFlag"].ToString() == "T" && dt.Rows[0]["InspectResult"].ToString() != "")
                                    {
                                        //MES 처리정보가 실패면
                                        if (ErrRtn.InterfaceResultCode != "0")
                                        {
                                            //MES 에러메세지가 없는 경우 임의메세지를 띄우고 있는 경우 MES메세지를 띄운다.
                                            if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                            {
                                                this.MdiParent.Cursor = Cursors.Default;
                                                m_ProgressPopup.mfCloseProgressPopup(this);

                                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000095", "M000951", Infragistics.Win.HAlign.Right);
                                            }
                                            else
                                            {
                                                this.MdiParent.Cursor = Cursors.Default;
                                                m_ProgressPopup.mfCloseProgressPopup(this);
                                                string strLang = m_resSys.GetString("SYS_LANG");
                                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    ,msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                                    , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                            }
                                        }
                                        //MES 전송이 성공시
                                        else
                                        {
                                            this.MdiParent.Cursor = Cursors.Default;
                                            m_ProgressPopup.mfCloseProgressPopup(this);

                                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                          "M001135", "M001037", "M000930", Infragistics.Win.HAlign.Right);
                                        }
                                    }
                                    else
                                    {
                                        this.MdiParent.Cursor = Cursors.Default;
                                        m_ProgressPopup.mfCloseProgressPopup(this);

                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "M001135", "M001037", "M000930",
                                                                Infragistics.Win.HAlign.Right);
                                    }
                                    //리스트 갱신
                                    mfSearch();
                                }
                                else
                                {
                                    this.MdiParent.Cursor = Cursors.Default;
                                    m_ProgressPopup.mfCloseProgressPopup(this);

                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000953",
                                                       Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }


                }



            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 테스트용 사용금지
        //////////#region const
        //////////private const string m_strFmpf_CreateISO = "WF_STS_JEGEJUNG";
        //////////private const string m_strFmpf_CancelISO = "WF_STS_PEJI";
        //////////private const string m_strFmpf_Normal = "WF_STS_QRPGIAN";
        //////////private const string m_strFmpf_EquipDiscard = "WF_STS_EQPDEL";
        //////////private const string m_strFmpf_Material = "WF_STS_MATJEGUM";

        //////////private const string strSystemKey = "AP47ST36-PR1F-6X9Q-C5X9-3PLUJX3D5XTM8D";

        //////////private const string m_xmlSendApvLine = "xmlSendApvLine";
        //////////private const string m_attSendApvLine_APV = "APV";
        //////////private const string m_attSendApvLine_CD_KIND = "CD_KIND";
        //////////private const string m_attSendApvLine_CD_COMPANY = "CD_COMPANY";
        //////////private const string m_attSendApvLine_CD_USERKEY = "CD_USERKEY";

        //////////private const string m_xmlCcLine = "xmlCcLine";
        //////////private const string m_attCcLine_APV = "APV";
        //////////private const string m_attCcLine_CD_KIND = "CD_KIND";
        //////////private const string m_attCcLine_CD_COMPANY = "CD_COMPANY";
        //////////private const string m_attCcLine_CD_USERKEY = "CD_USERKEY";

        //////////private const string m_xmlReceiveApvLine = "";

        //////////private const string m_xmlFormInfo = "<FormInfo>\n" +
        //////////                                "\t<UserType></UserType>\n" +
        //////////                                "\t<SaveTerm></SaveTerm>\n" +
        //////////                                "\t<DocLevel></DocLevel>\n" +
        //////////                                "\t<DocClassId></DocClassId>\n" +
        //////////                                "\t<DocClassName></DocClassName>\n" +
        //////////                                "\t<DocNo></DocNo>\n" +
        //////////                                "\t<EntName></EntName>\n" +
        //////////                            "</FormInfo>";

        //////////private const string m_strFormInfo_UserType = "UserType";
        //////////private const string m_strFormInfo_SaveTerm = "SaveTerm";
        //////////private const string m_strFormInfo_DocLevel = "DocLevel";
        //////////private const string m_strFormInfo_DocClassId = "DocClassId";
        //////////private const string m_strFormInfo_DocClassName = "DocClassName";
        //////////private const string m_strFormInfo_DocNo = "DocNo";
        //////////private const string m_strFormInfo_EntName = "EntName";

        //////////private const string m_strResult_CD_CODE = "CD_CODE";
        //////////private const string m_strResult_CD_STATUS = "CD_STATUS";
        //////////private const string m_strResult_CD_LEGACYKEY = "CD_LEGACYKEY";
        //////////private const string m_strResult_NO_EMPLOYEE = "NO_EMPLOYEE";
        //////////private const string m_strResult_DS_POSITION = "DS_POSITION";
        //////////private const string m_strResult_MSG = "MSG";

        //////////private const string m_strISODOCSubject = "표준 제 /개정 신청서";
        //////////private const string m_strISODOCCancelSubject = "표준 폐지 신청서";
        //////////private const string m_strMaterialSubject = "자재 재검토 의뢰서 : 특채정보";
        //////////private const string m_strEquipSubject = "설비인증의뢰";
        //////////private const string m_strEquipDiscardSubject = "설비폐기";

        //////////private const string m_strBrainsFileSvrPath = "";

        //////////private enum SendApvLine_CD_KIND
        //////////{
        //////////    SI,     // : Send Initiator
        //////////    SA,     // : Send Approver
        //////////    SS,     // : Send Serial
        //////////    SP      // : Send Parallel
        //////////}
        //////////private enum CcLine_CD_KIND
        //////////{
        //////////    CP, // : Cc Person
        //////////    CD  // : Cc Department
        //////////}

        //////////private const string m_strFile = "<FileInfo></FileInfo>";
        //////////private const string m_strFile_Legacy = "LegacyInfo";
        //////////private const string m_strFile_Legacy_AttCompany = "CD_COMPANY";
        //////////private const string m_strFile_Legacy_AttACTION = "CD_ACTION";
        //////////private const string m_strFile_Legacy_AttLINK = "IS_LINK";
        //////////private const string m_strFile_Files = "Files";
        //////////private const string m_strFile_Files_File = "File";
        //////////private const string m_strFile_Files_File_AttNM = "NM_FILE";
        //////////private const string m_strFile_Files_File_AttLOCATION = "NM_LOCATION";
        //////////private const string m_strFile_SendUri_Test = @"\\gwdev.mybokwang.com";
        //////////private const string m_strFile_SendUri = @"\\gwfile1.mybokwang.com";
        //////////private const string m_strFile_ID = @"FileAccess_StsQrp@mybokwang.com";
        //////////private const string m_strFile_Pwd = @"fileaccessqrp!@#";
        //////////private const string m_strFile_SendUriFolder = @"\WebSvcAttachTemp250";
        //////////private const string m_strFile_SendUriFolder_Test = @"\WebSvcAttachTemp250";
        //////////#endregion
        //////////public DataTable mfSaveMaterialSpecialGRWApproval(DataTable dt, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        //////////{
        //////////    QRPBrowser brwChannel = new QRPBrowser();

        //////////    //파일 서버 연결
        //////////    brwChannel.mfRegisterChannel(typeof(QRPGRW.IF.QRPGRW), "QRPGRW");
        //////////    QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
        //////////    brwChannel.mfCredentials(grw);


        //////////    XmlDocument _xml = new XmlDocument();
        //////////    XmlAttribute _att;

        //////////    string strContents = ChangeContentString_Material(dt);



        //////////    DataTable dtRtn = new DataTable();

        //////////    try
        //////////    {
        //////////        string strPlantCode = dt.Rows[0]["PlantCode"].ToString();
        //////////        string strStdNumber = dt.Rows[0]["StdNumber"].ToString();
        //////////        string strLegacyKey = strPlantCode + "||" + strStdNumber;

        //////////        string strFilePath = "\\INSMaterialSpecialFile\\";

        //////////        DataTable dtFileInfo = grw.mfSetFileInfoDataTable();

        //////////        #region SendApvLine
        //////////        string strSendLine = string.Empty;
        //////////        foreach (DataRow dr in dtSendLine.Rows)
        //////////        {
        //////////            XmlNode _child = _xml.CreateElement(m_attSendApvLine_APV);

        //////////            if (dr.Table.Columns.Contains(m_attSendApvLine_CD_KIND))
        //////////            {
        //////////                _att = _xml.CreateAttribute(m_attSendApvLine_CD_KIND);
        //////////                _att.Value = dr[m_attSendApvLine_CD_KIND].ToString();
        //////////                _child.Attributes.Append(_att);
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_attSendApvLine_CD_COMPANY))
        //////////            {
        //////////                _att = _xml.CreateAttribute(m_attSendApvLine_CD_COMPANY);
        //////////                _att.Value = dr[m_attSendApvLine_CD_COMPANY].ToString();
        //////////                _child.Attributes.Append(_att);
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_attSendApvLine_CD_USERKEY))
        //////////            {
        //////////                _att = _xml.CreateAttribute(m_attSendApvLine_CD_USERKEY);
        //////////                _att.Value = dr[m_attSendApvLine_CD_USERKEY].ToString();
        //////////                _child.Attributes.Append(_att);
        //////////            }
        //////////            strSendLine = strSendLine + _child.OuterXml.ToString();
        //////////            //_xml.AppendChild(_child);
        //////////        }

        //////////        //string strSendLine = _xml.OuterXml.ToString();
        //////////        #endregion

        //////////        #region CcLine

        //////////        _xml = new XmlDocument();
        //////////        //_param = _xml.CreateElement("param");
        //////////        //_att = _xml.CreateAttribute("name");
        //////////        //_att.Value = m_xmlCcLine;
        //////////        //_param.Attributes.Append(_att);
        //////////        string strCcLine = string.Empty;
        //////////        foreach (DataRow dr in dtCcLine.Rows)
        //////////        {
        //////////            XmlNode _child = _xml.CreateElement(m_attCcLine_APV);

        //////////            if (dr.Table.Columns.Contains(m_attCcLine_CD_KIND))
        //////////            {
        //////////                _att = _xml.CreateAttribute(m_attCcLine_CD_KIND);
        //////////                _att.Value = dr[m_attCcLine_CD_KIND].ToString();
        //////////                _child.Attributes.Append(_att);
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_attCcLine_CD_COMPANY))
        //////////            {
        //////////                _att = _xml.CreateAttribute(m_attCcLine_CD_COMPANY);
        //////////                _att.Value = dr[m_attCcLine_CD_COMPANY].ToString();
        //////////                _child.Attributes.Append(_att);
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_attCcLine_CD_USERKEY))
        //////////            {
        //////////                _att = _xml.CreateAttribute(m_attCcLine_CD_USERKEY);
        //////////                _att.Value = dr[m_attCcLine_CD_USERKEY].ToString();
        //////////                _child.Attributes.Append(_att);
        //////////            }
        //////////            strCcLine = strCcLine + _child.OuterXml.ToString();
        //////////            //_xml.AppendChild(_child);
        //////////        }

        //////////        //string strCcLine = _xml.OuterXml.ToString();
        //////////        #endregion

        //////////        #region FormInfo
        //////////        _xml.LoadXml(m_xmlFormInfo);

        //////////        foreach (DataRow dr in dtFormInfo.Rows)
        //////////        {
        //////////            string str = string.Empty;
        //////////            if (dr.Table.Columns.Contains(m_strFormInfo_UserType))
        //////////            {
        //////////                str = "FormInfo/" + m_strFormInfo_UserType;// +"/";
        //////////                _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_UserType].ToString();
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_strFormInfo_SaveTerm))
        //////////            {
        //////////                str = "FormInfo/" + m_strFormInfo_SaveTerm;// +"/";
        //////////                _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_SaveTerm].ToString();
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_strFormInfo_DocLevel))
        //////////            {
        //////////                str = "FormInfo/" + m_strFormInfo_DocLevel;// +"/";
        //////////                _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_DocLevel].ToString();
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_strFormInfo_DocClassId))
        //////////            {
        //////////                str = "FormInfo/" + m_strFormInfo_DocClassId;// +"/";
        //////////                _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_DocClassId].ToString();
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_strFormInfo_DocClassName))
        //////////            {
        //////////                str = "FormInfo/" + m_strFormInfo_DocClassName;// +"/";
        //////////                _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_DocClassName].ToString();
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_strFormInfo_DocNo))
        //////////            {
        //////////                str = "FormInfo/" + m_strFormInfo_DocNo;// +"/";
        //////////                _xml.SelectSingleNode(str).InnerText = strLegacyKey;
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_strFormInfo_EntName))
        //////////            {
        //////////                str = "FormInfo/" + m_strFormInfo_EntName;// +"/";
        //////////                _xml.SelectSingleNode(str).InnerText = dr[m_strFormInfo_EntName].ToString();
        //////////            }
        //////////        }

        //////////        string strFormInfo = _xml.OuterXml.ToString();
        //////////        #endregion

        //////////        #region FileInfo
        //////////        _xml.LoadXml(m_strFile);
        //////////        XmlNode _file = _xml.CreateElement(m_strFile_Legacy);

        //////////        XmlAttribute _fileatt = _xml.CreateAttribute(m_strFile_Legacy_AttCompany);
        //////////        _fileatt.Value = dtSendLine.Rows[0][m_strFile_Legacy_AttCompany].ToString();
        //////////        _file.Attributes.Append(_fileatt);

        //////////        _fileatt = _xml.CreateAttribute(m_strFile_Legacy_AttACTION);
        //////////        _fileatt.Value = "MOVE";
        //////////        _file.Attributes.Append(_fileatt);

        //////////        _fileatt = _xml.CreateAttribute(m_strFile_Legacy_AttLINK);
        //////////        _fileatt.Value = "FALSE";
        //////////        _file.Attributes.Append(_fileatt);

        //////////        _xml.SelectSingleNode("FileInfo").AppendChild(_file);

        //////////        _file = _xml.CreateElement(m_strFile_Files);
        //////////        _xml.SelectSingleNode("FileInfo").AppendChild(_file);

        //////////        if (!dt.Rows[0]["FileName1"].ToString().Equals(string.Empty))
        //////////        {
        //////////            DataRow dr = dtFileInfo.NewRow();
        //////////            dr["NM_FILE"] = strPlantCode + "-" + strStdNumber + "-Fir-" + dt.Rows[0]["FileName1"].ToString();
        //////////            dr["PlantCode"] = dt.Rows[0]["PlantCode"].ToString();
        //////////            dr["NM_LOCATION"] = strFilePath;

        //////////            dtFileInfo.Rows.Add(dr);
        //////////        }
        //////////        if (!dt.Rows[0]["ResultFileName"].ToString().Equals(string.Empty))
        //////////        {
        //////////            DataRow dr = dtFileInfo.NewRow();
        //////////            dr["NM_FILE"] = strPlantCode + "-" + strStdNumber + "-Sec-" + dt.Rows[0]["ResultFileName"].ToString();
        //////////            dr["PlantCode"] = dt.Rows[0]["PlantCode"].ToString();
        //////////            dr["NM_LOCATION"] = strFilePath;

        //////////            dtFileInfo.Rows.Add(dr);
        //////////        }

        //////////        foreach (DataRow dr in dtFileInfo.Rows)
        //////////        {
        //////////            XmlNode _child = _xml.CreateElement(m_strFile_Files_File);

        //////////            if (dr.Table.Columns.Contains(m_strFile_Files_File_AttNM))
        //////////            {
        //////////                _fileatt = _xml.CreateAttribute(m_strFile_Files_File_AttNM);
        //////////                _fileatt.Value = dr[m_strFile_Files_File_AttNM].ToString();
        //////////                _child.Attributes.Append(_fileatt);
        //////////            }
        //////////            if (dr.Table.Columns.Contains(m_strFile_Files_File_AttLOCATION))
        //////////            {
        //////////                _fileatt = _xml.CreateAttribute(m_strFile_Files_File_AttLOCATION);
        //////////                _fileatt.Value = string.Empty;//dr[m_strFile_Files_File_AttLOCATION].ToString();
        //////////                _child.Attributes.Append(_fileatt);
        //////////            }
        //////////            _xml.SelectSingleNode("FileInfo/Files").AppendChild(_child);
        //////////        }

        //////////        string strfileinfo = _xml.OuterXml.ToString();
        //////////        #endregion

        //////////        dtRtn = grw.Material(strLegacyKey, dtSendLine, dtCcLine, dt, dtFormInfo, dtFileInfo, strUserIP, strUserID);

        //////////    }
        //////////    catch (Exception ex)
        //////////    {
        //////////        return dtRtn;
        //////////        throw (ex);
        //////////    }

        //////////    return dtRtn;
        //////////}

        //////////private string ChangeContentString_Material(DataTable dtContent)
        //////////{
        //////////    string strReturnString = string.Empty;

        //////////    XmlDocument xml = new XmlDocument();
        //////////    xml.Load(Application.StartupPath + "\\QRPGRW.IF.dll.config");

        //////////    XmlNode node = xml.SelectSingleNode("configuration/LegacyHtml/Material");

        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>1", @"<P align=center>" + (dtContent.Rows[0]["MaterialCode"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MaterialCode"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>2", @"<P align=center>" + (dtContent.Rows[0]["MaterialSpec"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MaterialSpec"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>3", @"<P align=center>" + (dtContent.Rows[0]["GRDate"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["GRDate"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>4", @"<P align=center>" + (dtContent.Rows[0]["Qty"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["Qty"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>5", @"<P align=center>" + (dtContent.Rows[0]["VendorName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["VendorName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>6", @"<P align=center>" + (dtContent.Rows[0]["LotNo"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["LotNo"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>7", @"<P align=center>" + (dtContent.Rows[0]["Result"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["Result"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>8", @"<P align=center>" + (dtContent.Rows[0]["EtcDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["EtcDesc"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: left"" align=center>9", @"<P style=""TEXT-ALIGN: left"" align=center>" + (dtContent.Rows[0]["ReqObjectDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ReqObjectDesc"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>10", @"<P align=center>" + (dtContent.Rows[0]["MeetingDate1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingDate1"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>11", @"<P align=center>" + (dtContent.Rows[0]["MeetingPlace1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingPlace1"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>12", @"<P align=center>" + (dtContent.Rows[0]["DistPurchaseUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseUserName1"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>13", @"<P align=center>" + (dtContent.Rows[0]["DistProductUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductUserName1"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>14", @"<P align=center>" + (dtContent.Rows[0]["DistQualityUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityUserName1"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>15", @"<P align=center>" + (dtContent.Rows[0]["DistDevelopUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopUserName1"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>16", @"<P align=center>" + (dtContent.Rows[0]["DistEquipUserName1"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipUserName1"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: left"" align=left>17", @"<P style=""TEXT-ALIGN: left"" align=left>" + (dtContent.Rows[0]["MRBDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBDesc"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>18", @"<P align=center>" + (dtContent.Rows[0]["MRBUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBUserName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>19", @"<P align=center>" + (dtContent.Rows[0]["MRBDeliveryDate"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBDeliveryDate"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>20", @"<P align=center>" + (dtContent.Rows[0]["MRBEtcDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MRBEtcDesc"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>21", @"<P align=center>" + (dtContent.Rows[0]["MeetingDate2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingDate2"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>22", @"<P align=center>" + (dtContent.Rows[0]["MeetingPlace2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["MeetingPlace2"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>23", @"<P align=center>" + (dtContent.Rows[0]["DistPurchaseUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseUserName2"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>24", @"<P align=center>" + (dtContent.Rows[0]["DistProductUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductUserName2"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>25", @"<P align=center>" + (dtContent.Rows[0]["DistQualityUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityUserName2"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>26", @"<P align=center>" + (dtContent.Rows[0]["DistDevelopUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopUserName2"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>27", @"<P align=center>" + (dtContent.Rows[0]["DistEquipUserName2"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipUserName2"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: left"" align=left>28", @"<P style=""TEXT-ALIGN: left"" align=left>" + (dtContent.Rows[0]["ResultDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultDesc"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>29", @"<P align=center>" + (dtContent.Rows[0]["ResultUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultUserName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>30", @"<P align=center>" + (dtContent.Rows[0]["ResultDeliveryDate"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultDeliveryDate"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>31", @"<P align=center>" + (dtContent.Rows[0]["ResultEtcDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["ResultEtcDesc"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>32", @"<P align=center>" + (dtContent.Rows[0]["AgreePurchaseUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["AgreePurchaseUserName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>33", @"<P align=center>" + (dtContent.Rows[0]["AgreeProductUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["AgreeProductUserName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>34", @"<P align=center>" + (dtContent.Rows[0]["AgreeQualityUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["AgreeQualityUserName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>35", @"<P align=center>" + (dtContent.Rows[0]["AgreeDevelopUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["AgreeDevelopUserName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P align=center>36", @"<P align=center>" + (dtContent.Rows[0]["AgreeEquipUserName"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["AgreeEquipUserName"].ToString()));
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: left"" align=left>37", @"<P style=""TEXT-ALIGN: left"" align=left>" + (dtContent.Rows[0]["AgreeDesc"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["AgreeDesc"].ToString()));

        //////////    if (dtContent.Rows[0]["InspectResult"].ToString().Equals("T") || dtContent.Rows[0]["InspectResult"].ToString().Equals("S"))
        //////////    {
        //////////        node.InnerText = node.InnerText.Replace(@"<P align=center>38", @"<P align=center>" + @"O");
        //////////        node.InnerText = node.InnerText.Replace(@"<P align=center>39", @"<P align=center>" + @"&nbsp;");
        //////////    }
        //////////    else
        //////////    {
        //////////        node.InnerText = node.InnerText.Replace(@"<P align=center>38", @"<P align=center>" + @"&nbsp;");
        //////////        node.InnerText = node.InnerText.Replace(@"<P align=center>39", @"<P align=center>" + @"O");
        //////////    }

        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: center"" align=center><STRONG>40</STRONG>", @"<P style=""TEXT-ALIGN: center"" align=center><STRONG>" + (dtContent.Rows[0]["DistPurchaseUserName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistPurchaseUserName3"].ToString()) + "</STRONG>");
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: center"" align=center><STRONG>41</STRONG>", @"<P style=""TEXT-ALIGN: center"" align=center><STRONG>" + (dtContent.Rows[0]["DistProductUserName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistProductUserName3"].ToString()) + "</STRONG>");
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: center"" align=center><STRONG>42</STRONG>", @"<P style=""TEXT-ALIGN: center"" align=center><STRONG>" + (dtContent.Rows[0]["DistQualityUserName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistQualityUserName3"].ToString()) + "</STRONG>");
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: center"" align=center><STRONG>43</STRONG>", @"<P style=""TEXT-ALIGN: center"" align=center><STRONG>" + (dtContent.Rows[0]["DistDevelopUserName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistDevelopUserName3"].ToString()) + "</STRONG>");
        //////////    node.InnerText = node.InnerText.Replace(@"<P style=""TEXT-ALIGN: center"" align=center><STRONG>44</STRONG>", @"<P style=""TEXT-ALIGN: center"" align=center><STRONG>" + (dtContent.Rows[0]["DistEquipUserName3"].ToString() == string.Empty ? @"&nbsp;" : dtContent.Rows[0]["DistEquipUserName3"].ToString()) + "</STRONG>");
        //////////    strReturnString = node.InnerText;
        //////////    return strReturnString;
        //////////}
        #endregion

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //정보가없는데 삭제메세지가 뜸.
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001135", "M000638", "M000640",
                        Infragistics.Win.HAlign.Right);

                    return;
                }
                else if (this.uCheckCompleteFlag.Checked == true)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001135", "M000638", "M000978",
                        Infragistics.Win.HAlign.Right);

                    return;
                }

                else
                {
                    //BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecial), "INSMaterialSpecial");
                    QRPINS.BL.INSSTS.INSMaterialSpecial Material = new QRPINS.BL.INSSTS.INSMaterialSpecial();
                    brwChannel.mfCredentials(Material);

                    DataTable dtMaterial = Material.mfSetDataInfo();

                    DataRow drMaterial = dtMaterial.NewRow();
                    drMaterial["PlantCode"] = this.uComboPlant.Value.ToString();
                    drMaterial["StdNumber"] = this.uTextStdNumber.Text;
                    dtMaterial.Rows.Add(drMaterial);

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread thread = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        String rtMSG = Material.mfDeleteMatrialSpecial(dtMaterial);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);


                        //삭제 성공 여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001135", "M000638", "M000926",
                            Infragistics.Win.HAlign.Right);

                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001135", "M000638", "M000923",
                            Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitTab();
                InitValue();
                ReWrite();
                uGroupBoxContentsArea.Expanded = true;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridInfo.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridInfo);
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        private String GetUserName(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자정보 검색 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private DataTable dtGetUserinfo(String strPlantCode, String strUserID)
        {
            DataTable dtUserInfo = new DataTable();
            try
            {
                //SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUserInfo = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                return dtUserInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUserInfo;
            }
            finally
            { }
        }



        #region 이벤트
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridInfo_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridInfo, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInfo.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInfo.Height = 720;

                    for (int i = 0; i < uGridInfo.Rows.Count; i++)
                    {
                        uGridInfo.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }
        #endregion

        #region EditorButtonClick 이벤트

        private void uTextWriterID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = uComboPlant.Value.ToString();
                frmUser.ShowDialog();

                this.uTextWriterID.Text = frmUser.UserID;
                this.uTextWriteName.Text = frmUser.UserName;
                this.uTextDepartment.Text = frmUser.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0001 frmMaterial = new frmPOP0001();
                frmMaterial.PlantCode = uComboSearchPlant.Value.ToString();
                frmMaterial.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmMaterial.MaterialCode;
                this.uTextSearchMaterialName.Text = frmMaterial.MaterialName;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmVendor = new frmPOP0004();
                frmVendor.ShowDialog();

                this.uTextSearchVendorCode.Text = frmVendor.CustomerCode;
                this.uTextSearchVendorName.Text = frmVendor.CustomerName;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        private void uTextMRBUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmMBRUser = new frmPOP0011();
                frmMBRUser.PlantCode = uComboPlant.Value.ToString();
                frmMBRUser.ShowDialog();

                this.uTextMRBUserID.Text = frmMBRUser.UserID;
                this.uTextMRBUserName.Text = frmMBRUser.UserName;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
           
        }

        private void uTextFileName1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.ToString().Equals("Search"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All File (*.*)|*.*"; //excel files (*.xlsx)|*.xlsx";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strFile = openFile.FileName;
                        uTextFileName1.Text = strFile;
                    }
                }
                else if (e.Button.Key.ToString().Equals("Down"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
           
                    if (this.uTextFileName1.Text.Contains(":\\") || this.uTextFileName1.Text == "")
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000357",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0009");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextFileName1.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextFileName1.Text);

                    }
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextResultFileName_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.ToString().Equals("Search"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*"; //excel files (*.xlsx)|*.xlsx";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strFile = openFile.FileName;
                        uTextResultFileName.Text = strFile;
                    }
                }
                else if(e.Button.Key.ToString().Equals("Down"))
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    if (this.uTextResultFileName.Text.Contains(":\\") || this.uTextResultFileName.Text == "")
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000357",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0009");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextResultFileName.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextResultFileName.Text);

                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uTextResultUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmResultUser = new frmPOP0011();
                frmResultUser.PlantCode = uComboPlant.Value.ToString();
                frmResultUser.ShowDialog();

                this.uTextResultUserID.Text = frmResultUser.UserID;
                this.uTextResultUserName.Text = frmResultUser.UserName;

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            

        }

        
        #endregion


        private void uTextLotNo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextLotNo.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                              , "M001135", "M001115", "M000078", Infragistics.Win.HAlign.Right);

                        this.uTextLotNo.Focus();

                        return;
                    }
                    else
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecial), "INSMaterialSpecial");
                        QRPINS.BL.INSSTS.INSMaterialSpecial MaterialSpecial = new QRPINS.BL.INSSTS.INSMaterialSpecial();
                        brwChannel.mfCredentials(MaterialSpecial);

                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecialShip), "INSMaterialSpecialShip");
                        QRPINS.BL.INSSTS.INSMaterialSpecialShip clsShip = new QRPINS.BL.INSSTS.INSMaterialSpecialShip();
                        brwChannel.mfCredentials(clsShip);

                        String strPlantCode = this.uComboPlant.Value.ToString();
                        String strLotNo = this.uTextLotNo.Text;

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001027", "M000266", Infragistics.Win.HAlign.Right);

                            //Focus
                            uComboPlant.DropDown();
                            mfCreate();
                            return;
                        }

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        DataTable dt = MaterialSpecial.mfReadMaterialGRForMS(strPlantCode, strLotNo, m_resSys.GetString("SYS_LANG"));


                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        if (dt.Rows.Count == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                               , "M001135", "M001115", "M000750", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        else
                        {
                            //if (dt.Rows[0]["PassFailFlag"].ToString() == "OK")
                            //{

                            //    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //       , "M001135", "M001115", "M000755", Infragistics.Win.HAlign.Right);

                            //    return;
                            //}
                            //else if (dt.Rows[0]["PassFailFlag"].ToString() == "NG")
                            //if (dt.Rows[0]["PassFailFlag"].ToString() == "NG")
                            //{
                                this.uTextMaterialCode.Text = dt.Rows[0]["MaterialCode"].ToString();
                                this.uTextMaterialName.Text = dt.Rows[0]["MaterialName"].ToString();
                                this.uTextMaterialSpec.Text = dt.Rows[0]["MaterialSpec1"].ToString();
                                this.uTextVendorCode.Text = dt.Rows[0]["VendorCode"].ToString();
                                this.uTextVendorName.Text = dt.Rows[0]["VendorName"].ToString();
                                this.uTextGRNo.Text = dt.Rows[0]["GRNo"].ToString();
                                this.uTextQty.Text = dt.Rows[0]["Qty"].ToString();

                                String strMaterialCode = this.uTextMaterialCode.Text;

                                DataTable dtShip = clsShip.mfReadShipInfo(strPlantCode, strLotNo, strMaterialCode, m_resSys.GetString("SYS_LANG"));
                                dtShip.Columns.Add("LotCheckFlag",typeof(String));
                                dtShip.Columns.Add("EtcDesc", typeof(String));
                                if (dtShip.Rows.Count > 0)
                                {
                                    this.uGridShipInfo.DataSource = dtShip;
                                    this.uGridShipInfo.DataBind();
                                    for (int i = 0; i < this.uGridShipInfo.Rows.Count; i++)
                                    {
                                        this.uGridShipInfo.Rows[i].Cells["LotCheckFlag"].Value = false;
                                    }
                                }
                            //}
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uGridInfo_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                e.Row.Fixed = true;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                QRPINS.BL.INSSTS.INSMaterialSpecial MaterialSpecial;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecial), "INSMaterialSpecial");
                    MaterialSpecial = new QRPINS.BL.INSSTS.INSMaterialSpecial();
                    brwChannel.mfCredentials(MaterialSpecial);
                }
                else
                {
                    MaterialSpecial = new QRPINS.BL.INSSTS.INSMaterialSpecial(m_strDBConn);
                }

                this.uTextMESFlag.Text = e.Row.Cells["MESTFlag"].Value.ToString();   //MESFlag
                this.uTextCompleteState.Text = e.Row.Cells["CompleteFlag"].Value.ToString(); //테이블안 작성완료 상태

                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strStdNumber = e.Row.Cells["StdNumber"].Value.ToString();
                String strUserName = "";

                DataTable dt = MaterialSpecial.mfReadMaterialSpecialGrid(strPlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                if (dt.Rows.Count > 0)
                {
                    this.uTextStdNumber.Text = dt.Rows[0]["StdNumber"].ToString();
                    this.uComboPlant.Value = dt.Rows[0]["PlantCode"].ToString();
                    this.uTextLotNo.Text = dt.Rows[0]["LotNo"].ToString();
                    
                    //데이터가 없으면 오늘날짜로 찍어준다.
                    if (dt.Rows[0]["WriteDate"].ToString() == "" || dt.Rows[0]["WriteDate"] == DBNull.Value)
                        this.uDateWriteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    else
                        this.uDateWriteDate.Value = dt.Rows[0]["WriteDate"].ToString();

                    this.uTextWriterID.Text = dt.Rows[0]["WriteUserID"].ToString();
                    this.uTextDepartment.Text = dt.Rows[0]["WriteUserDept"].ToString();
                    this.uTextMaterialCode.Text = dt.Rows[0]["MaterialCode"].ToString();
                    this.uTextMaterialSpec.Text = dt.Rows[0]["MaterialSpec1"].ToString();
                    this.uTextVendorCode.Text = dt.Rows[0]["VendorCode"].ToString();
                    this.uTextQty.Text = dt.Rows[0]["Qty"].ToString();
                    this.uTextEtcDesc.Text = dt.Rows[0]["EtcDesc"].ToString();
                    this.uTextReqObjectDesc.Text = dt.Rows[0]["ReqObjectDesc"].ToString();
                    this.uTextResult.Text = dt.Rows[0]["Result"].ToString();
                    if (dt.Rows[0]["InspectResult"].ToString() == "")
                    {
                        this.uOptionInspectResult.Value = "";
                    }
                    else if (dt.Rows[0]["InspectResult"].ToString() == "T")
                    {
                        this.uOptionInspectResult.Value = "T";
                    }
                    else if (dt.Rows[0]["InspectResult"].ToString() == "F")
                    {
                        this.uOptionInspectResult.Value = "F";
                    }
                    else if (dt.Rows[0]["InspectResult"].ToString() == "S")
                    {
                        this.uOptionInspectResult.Value = "S";
                    }
                    this.uOptionInspectResult.Value = dt.Rows[0]["InspectResult"].ToString();

                    if (dt.Rows[0]["MeetingDate1"].ToString() == "" || dt.Rows[0]["MeetingDate1"] == DBNull.Value)
                    {
                        this.uDateMeetingDate1.Value = "";
                    }
                    else
                    {
                        this.uDateMeetingDate1.Value = dt.Rows[0]["MeetingDate1"].ToString();
                    }
                    DataTable dtUser = new DataTable();
                    this.uTextMeetingPlace1.Text = dt.Rows[0]["MeetingPlace1"].ToString();
                    this.uTextFileName1.Text = dt.Rows[0]["FileName1"].ToString();
                    //각 사용자 마다 ID를 받아서 ID로 이름, 부서명을 받는다.
                    this.uTextDistPurchaseUserID1.Text = dt.Rows[0]["DistPurchaseUserID1"].ToString();
                    dtUser = dtGetUserinfo(strPlantCode, this.uTextDistPurchaseUserID1.Text);
                    if (this.uTextDistPurchaseUserID1.Text != "")
                    {
                        this.uTextDistPurchaseUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistPurchaseUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistProductUserID1.Text = dt.Rows[0]["DIstProductUserID1"].ToString();
                    if (this.uTextDistProductUserID1.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistProductUserID1.Text);
                        this.uTextDistProductUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistProductUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistDevelopUserID1.Text = dt.Rows[0]["DIstDevelopUserID1"].ToString();
                    if (this.uTextDistDevelopUserID1.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistDevelopUserID1.Text);
                        this.uTextDistDevelopUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistDevelopUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistEquipUserID1.Text = dt.Rows[0]["DistEquipUserID1"].ToString();
                    if (this.uTextDistEquipUserID1.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistEquipUserID1.Text);
                        this.uTextDistEquipUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistEquipUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistQualityUserID1.Text = dt.Rows[0]["DistQualityUserID1"].ToString();
                    if (this.uTextDistQualityUserID1.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistQualityUserID1.Text);
                        this.uTextDistQualityUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistQualityUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistEtcUserID1.Text = dt.Rows[0]["DistEtcUserID1"].ToString();
                    if (this.uTextDistEtcUserID1.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistEtcUserID1.Text);
                        this.uTextDistEtcUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistEtcUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextMRBDesc.Text = dt.Rows[0]["MRBDesc"].ToString();
                    
                    this.uTextMRBUserID.Text = dt.Rows[0]["MRBUserID"].ToString();
                    strUserName = GetUserName(strPlantCode, this.uTextMRBUserID.Text);
                    this.uTextMRBUserName.Text = strUserName;

                    this.uDateMRBDeliveryDate.Value = dt.Rows[0]["MRBDeliveryDate"].ToString();
                    this.uTextMRBEtcDesc.Text = dt.Rows[0]["MRBEtcDesc"].ToString();
                    this.uDateMeetingDate2.Value = dt.Rows[0]["MeetingDate2"].ToString();
                    this.uTextMeetingPlace2.Text = dt.Rows[0]["MeetingPlace2"].ToString();

                    this.uTextDistPurchaseUserID2.Text = dt.Rows[0]["DistPurchaseUserID2"].ToString();
                    if (this.uTextDistPurchaseUserID2.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistPurchaseUserID2.Text);
                        this.uTextDistPurchaseUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistPurchaseUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistProductUserID2.Text = dt.Rows[0]["DistProductUserID2"].ToString();
                    if (this.uTextDistProductUserID2.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistProductUserID2.Text);
                        this.uTextDistProductUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistProductUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistDevelopUserID2.Text = dt.Rows[0]["DistDevelopUserID2"].ToString();
                    if (this.uTextDistDevelopUserID2.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistDevelopUserID2.Text);
                        this.uTextDistDevelopUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistDevelopUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistEquipUserID2.Text = dt.Rows[0]["DistEquipUserID2"].ToString();
                    if (this.uTextDistEquipUserID2.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistEquipUserID2.Text);
                        this.uTextDistEquipUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistEquipUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistQualityUserID2.Text = dt.Rows[0]["DistQualityUserID2"].ToString();
                    if (this.uTextDistQualityUserID2.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistQualityUserID2.Text);
                        this.uTextDistQualityUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistQualityUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextDistEtcUserID2.Text = dt.Rows[0]["DistEtcUserID2"].ToString();
                    if (this.uTextDistEtcUserID2.Text != "")
                    {
                        dtUser = dtGetUserinfo(strPlantCode, this.uTextDistEtcUserID2.Text);
                        this.uTextDistEtcUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                        this.uTextDistEtcUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }

                    this.uTextResultDesc.Text = dt.Rows[0]["ResultDesc"].ToString();
                    this.uTextResultFileName.Text = dt.Rows[0]["ResultFileName"].ToString();
                    this.uTextResultUserID.Text = dt.Rows[0]["ResultUserID"].ToString();
                    this.uTextResultEtcDesc.Text = dt.Rows[0]["ResultEtcDesc"].ToString();
                    this.uTextAgreePurchaseUserID.Text = dt.Rows[0]["AgreePurchaseUserID"].ToString();
                    this.uTextAgreeProductUserID.Text = dt.Rows[0]["AgreeProductUserID"].ToString();
                    this.uTextAgreeDevelopUserID.Text = dt.Rows[0]["AgreeDevelopUserID"].ToString();
                    this.uTextAgreeEquipUserID.Text = dt.Rows[0]["AgreeEquipUserID"].ToString();
                    this.uTextAgreeQualityUserID.Text = dt.Rows[0]["AgreeQualityUserID"].ToString();
                    this.uTextAgreeDesc.Text = dt.Rows[0]["AgreeDesc"].ToString();
                    //this.uTextDistPurchaseUserID3.Text = dt.Rows[0]["DistPurchaseUserID3"].ToString();
                    this.uComboDistPurchaseUserID3.Value = dt.Rows[0]["DistPurchaseUserID3"].ToString();
                    //this.uTextDistProductUserID3.Text = dt.Rows[0]["DistProductUserID3"].ToString();
                    this.uComboDistProductUserID3.Value = dt.Rows[0]["DistProductUserID3"].ToString();
                    //this.uTextDistDevelopUserID3.Text = dt.Rows[0]["DistDevelopUserID3"].ToString();
                    this.uComboDistDevelopUserID3.Value = dt.Rows[0]["DistDevelopUserID3"].ToString();
                    //this.uTextDistEquipUserID3.Text = dt.Rows[0]["DistEquipUserID3"].ToString();
                    this.uComboDistEquipUserID3.Value = dt.Rows[0]["DistEquipUserID3"].ToString();
                    //this.uTextDistQualityUserID3.Text = dt.Rows[0]["DistQualityUserID3"].ToString();
                    this.uComboDistQualityUserID3.Value = dt.Rows[0]["DistQualityUserID3"].ToString();
                    this.uComboDistEtcUserID3.Value = dt.Rows[0]["DistEtcUserID3"].ToString();
                    this.uTextGRNo.Text = dt.Rows[0]["GRNo"].ToString();
                    this.uTextWriteName.Text = dt.Rows[0]["UserName"].ToString();
                    this.uTextMaterialName.Text = dt.Rows[0]["MaterialName"].ToString();
                    this.uTextVendorName.Text = dt.Rows[0]["VendorName"].ToString();

                    this.uCheckGWTFlag.Checked = dt.Rows[0]["GWTFlag"].ToString() == "T" ? true : false;
                    this.uCheckGWResultState.Checked = dt.Rows[0]["GWResultState"].ToString() == "T" ? true : false;
                    if (dt.Rows[0]["AdmitReturnFlag"].ToString() == "R")
                    {
                        this.uCheckReturn.Checked = true;
                    }
                    else
                    {
                        this.uCheckReturn.Checked = false;
                    }

                    this.uTextAgreePurchaseUserName.Text = dt.Rows[0]["AgreePurchaseUserName"].ToString();
                    this.uTextAgreeProductUserName.Text = dt.Rows[0]["AgreeProductUserName"].ToString();
                    this.uTextAgreeDevelopUserName.Text = dt.Rows[0]["AgreeDevelopUserName"].ToString();
                    this.uTextAgreeEquipUserName.Text = dt.Rows[0]["AgreeEquipUserName"].ToString();
                    this.uTextAgreeQualityUserName.Text = dt.Rows[0]["AgreeQualityUserName"].ToString();

                    this.uTextResultUserName.Text = dt.Rows[0]["ResultUserName"].ToString();
                    this.uTextMRBUserName.Text = dt.Rows[0]["MRBUserName"].ToString();
                    if (dt.Rows[0]["CompleteFlag"].ToString() == "T")
                    {
                        this.uCheckCompleteFlag.Checked = true;
                    }
                    else
                    {
                        this.uCheckCompleteFlag.Checked = false;
                    }
                    this.uTextWMSTFlag.Text = dt.Rows[0]["WMSTFlag"].ToString();

                    if(dt.Rows[0]["ValidationDateFlag"].ToString().Equals("T"))
                        this.uCheckValidationDateFlag.Checked = true;
                    else
                        this.uCheckValidationDateFlag.Checked = false;
                    this.uDateValidationDate.Value = dt.Rows[0]["ValidationDate"].ToString();

                    this.uGroupBoxContentsArea.Expanded = true;
                }

                if (this.uCheckCompleteFlag.Checked == true && this.uCheckReturn.Checked == false)
                {
                    SetDisWriteable();
                }
                else
                {
                    ReWrite();
                }
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSMaterialSpecialShip), "INSMaterialSpecialShip");
                QRPINS.BL.INSSTS.INSMaterialSpecialShip clsShip = new QRPINS.BL.INSSTS.INSMaterialSpecialShip();
                brwChannel.mfCredentials(clsShip);
                DataTable dtShip = clsShip.mfReadShipInfo_Grid(strPlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                if (dtShip.Rows.Count > 0)
                {
                    //dtShip.Columns["LotCheckFlag"].ColumnName = "Check";
                    this.uGridShipInfo.DataSource = dtShip;
                    this.uGridShipInfo.DataBind();
                }
                //for (int i = 0; i < dtShip.Rows.Count; i++)
                //{
                //    if (dtShip.Rows[i]["LotCheckFlag"].ToString().Equals("T"))
                //    {
                //        this.uGridShipInfo.Rows[i].Cells["Check"].Value = "true";
                //    }
                //}

                    this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (this.uComboPlant.Value.ToString() == "")
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                       , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }

            
                
        }

        private void uButtonFile1Down_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (this.uTextFileName1.Text.Contains(":\\") || this.uTextFileName1.Text == "")
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M000357",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0009");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();


                        if (this.uTextFileName1.Text.Contains(":\\") == false)
                        {
                            arrFile.Add(this.uTextFileName1.Text);
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        System.Diagnostics.Process.Start(strSaveFolder + this.uTextFileName1.Text);

                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonResultFileDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (this.uTextResultFileName.Text.Contains(":\\") || this.uTextResultFileName.Text == "")
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M000357",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0009");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();


                        if (this.uTextResultFileName.Text.Contains(":\\") == false)
                        {
                            arrFile.Add(this.uTextResultFileName.Text);
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                        System.Diagnostics.Process.Start(strSaveFolder + this.uTextResultFileName.Text);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 수량 텍스트 박스에 숫자만 입력 가능
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        #region ID검색 메소드(EditButton click)
        private void uTextDistPurchaseUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistPurchaseUserID1.Text = frmUsr.UserID;
                this.uTextDistPurchaseUserName1.Text = frmUsr.UserName;
                this.uTextDistPurchaseUserDept1.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextDistProductUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistProductUserID1.Text = frmUsr.UserID;
                this.uTextDistProductUserName1.Text = frmUsr.UserName;
                this.uTextDistProductUserDept1.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistDevelopUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistDevelopUserID1.Text = frmUsr.UserID;
                this.uTextDistDevelopUserName1.Text = frmUsr.UserName;
                this.uTextDistDevelopUserDept1.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistEquipUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistEquipUserID1.Text = frmUsr.UserID;
                this.uTextDistEquipUserName1.Text = frmUsr.UserName;
                this.uTextDistEquipUserDept1.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistQualityUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistQualityUserID1.Text = frmUsr.UserID;
                this.uTextDistQualityUserName1.Text = frmUsr.UserName;
                this.uTextDistQualityUserDept1.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistPurchaseUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistPurchaseUserID2.Text = frmUsr.UserID;
                this.uTextDistPurchaseUserName2.Text = frmUsr.UserName;
                this.uTextDistPurchaseUserDept2.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistProductUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistProductUserID2.Text = frmUsr.UserID;
                this.uTextDistProductUserName2.Text = frmUsr.UserName;
                this.uTextDistProductUserDept2.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistDevelopUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistDevelopUserID2.Text = frmUsr.UserID;
                this.uTextDistDevelopUserName2.Text = frmUsr.UserName;
                this.uTextDistDevelopUserDept2.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistEquipUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistEquipUserID2.Text = frmUsr.UserID;
                this.uTextDistEquipUserName2.Text = frmUsr.UserName;
                this.uTextDistEquipUserDept2.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistQualityUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistQualityUserID2.Text = frmUsr.UserID;
                this.uTextDistQualityUserName2.Text = frmUsr.UserName;
                this.uTextDistQualityUserDept2.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextAgreePurchaseUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextAgreePurchaseUserID.Text = frmUsr.UserID;
                this.uTextAgreePurchaseUserName.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextAgreeProductUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextAgreeProductUserID.Text = frmUsr.UserID;
                this.uTextAgreeProductUserName.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextAgreeDevelopUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextAgreeDevelopUserID.Text = frmUsr.UserID;
                this.uTextAgreeDevelopUserName.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }



        private void uTextAgreeQualityUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextAgreeQualityUserID.Text = frmUsr.UserID;
                this.uTextAgreeQualityUserName.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextAgreeEquipUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextAgreeEquipUserID.Text = frmUsr.UserID;
                this.uTextAgreeEquipUserName.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistPurchaseUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistPurchaseUserID3.Text = frmUsr.UserID;
                this.uTextDistPurchaseUserName3.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistProductUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistProductUserID3.Text = frmUsr.UserID;
                this.uTextDistProductUserName3.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistDevelopUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistDevelopUserID3.Text = frmUsr.UserID;
                this.uTextDistDevelopUserName3.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistEquipUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistEquipUserID3.Text = frmUsr.UserID;
                this.uTextDistEquipUserName3.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistQualityUserID3_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistQualityUserID3.Text = frmUsr.UserID;
                this.uTextDistQualityUserName3.Text = frmUsr.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion


        #region ID검색 메소드(Enter)
        private void uTextDistPurchaseUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistPurchaseUserID2.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistPurchaseUserID2.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistPurchaseUserID2.Text = "";
                            this.uTextDistPurchaseUserName2.Text = "";
                            this.uTextDistPurchaseUserDept2.Text = "";
                        }

                        else
                        {
                            this.uTextDistPurchaseUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistPurchaseUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDistPurchaseUserID2.Text = "";
                    this.uTextDistPurchaseUserName2.Text = "";
                    this.uTextDistPurchaseUserDept2.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistProductUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistProductUserID2.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistProductUserID2.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistProductUserID2.Text = "";
                            this.uTextDistProductUserName2.Text = "";
                            this.uTextDistProductUserDept2.Text = "";
                        }

                        else
                        {
                            this.uTextDistProductUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistProductUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextDistProductUserID2.Text = "";
                    this.uTextDistProductUserName2.Text = "";
                    this.uTextDistProductUserDept2.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistDevelopUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistDevelopUserID2.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistDevelopUserID2.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistDevelopUserID2.Text = "";
                            this.uTextDistDevelopUserName2.Text = "";
                            this.uTextDistDevelopUserDept2.Text = "";
                        }

                        else
                        {
                            this.uTextDistDevelopUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistDevelopUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDistDevelopUserID2.Text = "";
                    this.uTextDistDevelopUserName2.Text = "";
                    this.uTextDistDevelopUserDept2.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistEquipUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistEquipUserID2.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistEquipUserID2.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistEquipUserID2.Text = "";
                            this.uTextDistEquipUserName2.Text = "";
                            this.uTextDistEquipUserDept2.Text =  "";
                        }

                        else
                        {
                            this.uTextDistEquipUserName2.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistEquipUserDept2.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextDistEquipUserID2.Text = "";
                    this.uTextDistEquipUserName2.Text = "";
                    this.uTextDistEquipUserDept2.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistQualityUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistQualityUserID2.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistQualityUserID2.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUSer = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUSer.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistQualityUserID2.Text = "";
                            this.uTextDistQualityUserName2.Text = "";
                            this.uTextDistQualityUserDept2.Text = "";
                        }

                        else
                        {
                            this.uTextDistQualityUserName2.Text = dtUSer.Rows[0]["UserName"].ToString();
                            this.uTextDistQualityUserDept2.Text = dtUSer.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDistQualityUserID2.Text = "";
                    this.uTextDistQualityUserName2.Text = "";
                    this.uTextDistQualityUserDept2.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextAgreePurchaseUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAgreePurchaseUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextAgreePurchaseUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextAgreePurchaseUserID.Text = "";
                            this.uTextAgreePurchaseUserName.Text = "";
                        }

                        else
                        {
                            this.uTextAgreePurchaseUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextAgreeProductUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAgreeProductUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextAgreeProductUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextAgreeProductUserID.Text = "";
                            this.uTextAgreeProductUserName.Text = "";
                        }

                        else
                        {
                            this.uTextAgreeProductUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextAgreeDevelopUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAgreeDevelopUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextAgreeDevelopUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextAgreeDevelopUserID.Text = "";
                            this.uTextAgreeDevelopUserName.Text = "";
                        }

                        else
                        {
                            this.uTextAgreeDevelopUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextAgreeEquipUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAgreeEquipUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextAgreeEquipUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextAgreeEquipUserID.Text = "";
                            this.uTextAgreeEquipUserName.Text = "";
                        }

                        else
                        {
                            this.uTextAgreeEquipUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextAgreeQualityUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAgreeQualityUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextAgreeQualityUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextAgreeQualityUserID.Text = "";
                            this.uTextAgreeQualityUserName.Text = "";
                        }

                        else
                        {
                            this.uTextAgreeQualityUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        private void uTextDistPurchaseUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistPurchaseUserID3.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistPurchaseUserID3.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistPurchaseUserID3.Text = "";
                            this.uTextDistPurchaseUserName3.Text = "";
                        }

                        else
                        {
                            this.uTextDistPurchaseUserName3.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistProductUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistProductUserID3.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistProductUserID3.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistProductUserID3.Text = "";
                            this.uTextDistProductUserName3.Text = "";
                        }

                        else
                        {
                            this.uTextDistProductUserName3.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistDevelopUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistDevelopUserID3.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistDevelopUserID3.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistDevelopUserID3.Text = "";
                            this.uTextDistDevelopUserName3.Text = "";
                        }

                        else
                        {
                            this.uTextDistDevelopUserName3.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistEquipUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistEquipUserID3.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistEquipUserID3.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistEquipUserID3.Text = "";
                            this.uTextDistEquipUserName3.Text = "";
                        }

                        else
                        {
                            this.uTextDistEquipUserName3.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistQualityUserID3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistQualityUserID3.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistQualityUserID3.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistQualityUserID3.Text = "";
                            this.uTextDistQualityUserName3.Text = "";
                        }

                        else
                        {
                            this.uTextDistQualityUserName3.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        

        private void uTextDistPurchaseUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistPurchaseUserID1.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistPurchaseUserID1.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistPurchaseUserID1.Text = "";
                            this.uTextDistPurchaseUserName1.Text = "";
                            this.uTextDistPurchaseUserDept1.Text = "";
                        }

                        else
                        {
                            this.uTextDistPurchaseUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistPurchaseUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDistPurchaseUserID1.Text = "";
                    this.uTextDistPurchaseUserName1.Text = "";
                    this.uTextDistPurchaseUserDept1.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistProductUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistProductUserID1.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistProductUserID1.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), this.uTextDistProductUserID1.Text);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistProductUserID1.Text = "";
                            this.uTextDistProductUserName1.Text = "";
                        }

                        else
                        {
                            this.uTextDistProductUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistProductUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextDistProductUserID1.Text = "";
                    this.uTextDistProductUserName1.Text = "";
                    this.uTextDistProductUserDept1.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistDevelopUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistDevelopUserID1.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistDevelopUserID1.Text;
                        
                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistDevelopUserID1.Text = "";
                            this.uTextDistDevelopUserName1.Text = "";
                            this.uTextDistDevelopUserDept1.Text = "";
                        }

                        else
                        {
                            this.uTextDistDevelopUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistDevelopUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextDistDevelopUserID1.Text = "";
                    this.uTextDistDevelopUserName1.Text = "";
                    this.uTextDistDevelopUserDept1.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistEquipUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistEquipUserID1.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistEquipUserID1.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistEquipUserID1.Text = "";
                            this.uTextDistEquipUserName1.Text = "";
                            this.uTextDistEquipUserDept1.Text = "";
                        }

                        else
                        {
                            this.uTextDistEquipUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistEquipUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDistEquipUserID1.Text = "";
                    this.uTextDistEquipUserName1.Text = "";
                    this.uTextDistEquipUserDept1.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistQualityUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistQualityUserID1.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistQualityUserID1.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUser = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUser.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistQualityUserID1.Text = "";
                            this.uTextDistQualityUserName1.Text = "";
                            this.uTextDistQualityUserDept1.Text = "";
                        }

                        else
                        {
                            this.uTextDistQualityUserName1.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDistQualityUserDept1.Text = dtUser.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDistQualityUserID1.Text = "";
                    this.uTextDistQualityUserName1.Text = "";
                    this.uTextDistQualityUserDept1.Text = "";
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextDistEtcUserID1_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uComboPlant.Value.ToString();
                frmUser.ShowDialog();

                this.uTextDistEtcUserID1.Text = frmUser.UserID;
                this.uTextDistEtcUserName1.Text = frmUser.UserName;
                this.uTextDistEtcUserDept1.Text = frmUser.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistEtcUserID1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistEtcUserID1.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistEtcUserID1.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUsr = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUsr.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistEtcUserID1.Text = "";
                            this.uTextDistEtcUserName1.Text = "";
                            this.uTextDistEtcUserDept1.Text = "";
                        }

                        else
                        {
                            this.uTextDistEtcUserName1.Text = dtUsr.Rows[0]["UserName"].ToString();
                            this.uTextDistEtcUserDept1.Text = dtUsr.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextDistEtcUserID1.Text = "";
                    this.uTextDistEtcUserName1.Text = "";
                    this.uTextDistEtcUserDept1.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistEtcUserID2_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmUsr = new frmPOP0011();
                frmUsr.PlantCode = this.uComboPlant.Value.ToString();
                frmUsr.ShowDialog();

                this.uTextDistEtcUserID2.Text = frmUsr.UserID;
                this.uTextDistEtcUserName2.Text = frmUsr.UserName;
                this.uTextDistEtcUserDept2.Text = frmUsr.DeptName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextDistEtcUserID2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistEtcUserID2.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextDistEtcUserID2.Text;

                        // User검색 함수 호출
                        //String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);
                        DataTable dtUsr = dtGetUserinfo(this.uComboPlant.Value.ToString(), strUserID);

                        if (dtUsr.Rows.Count == 0)
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextDistEtcUserID2.Text = "";
                            this.uTextDistEtcUserName2.Text = "";
                            this.uTextDistEtcUserDept2.Text = "";
                        }

                        else
                        {
                            this.uTextDistEtcUserName2.Text = dtUsr.Rows[0]["UserName"].ToString();
                            this.uTextDistEtcUserDept2.Text = dtUsr.Rows[0]["DeptName"].ToString();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextDistEtcUserID2.Text = "";
                    this.uTextDistEtcUserName2.Text = "";
                    this.uTextDistEtcUserDept2.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextMRBUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextMRBUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextMRBUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextMRBUserID.Text = "";
                            this.uTextMRBUserName.Text = "";
                        }

                        else
                        {
                            this.uTextMRBUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextResultUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextResultUserID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextResultUserID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextResultUserID.Text = "";
                            this.uTextResultUserName.Text = "";
                        }

                        else
                        {
                            this.uTextResultUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextWriterID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriterID.Text != "")
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strUserID = this.uTextWriterID.Text;

                        // User검색 함수 호출
                        String strRtnUserName = GetUserName(this.uComboPlant.Value.ToString(), strUserID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                       "M001264", "M000962", "M000621",
                                       Infragistics.Win.HAlign.Right);

                            this.uTextWriterID.Text = "";
                            this.uTextWriteName.Text = "";
                        }

                        else
                        {
                            this.uTextWriteName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }
        #endregion

        private void frmINSZ0008_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        // 유효기간 연장일 체크박스 상태변화 이벤트
        private void uCheckValidationDateFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckValidationDateFlag.Checked)
                {
                    this.uDateValidationDate.Visible = true;
                    this.uLabelValidationDate.Visible = true;
                }
                else
                {
                    this.uLabelValidationDate.Visible = false;
                    this.uDateValidationDate.Visible = false;
                    this.uDateValidationDate.Value = null;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINSZ0008_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckCompleteFlag_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Enabled == true && this.uCheckCompleteFlag.Checked == true && this.uCheckGWResultState.Checked == false)
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000984", "M000231",
                                    Infragistics.Win.HAlign.Right);

                    this.uCheckCompleteFlag.Checked = false;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 공장 콤보 변경시 부서콤보박스 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboDistPurchaseUserID3.Items.Clear();
                this.uComboDistProductUserID3.Items.Clear();
                this.uComboDistDevelopUserID3.Items.Clear();
                this.uComboDistEquipUserID3.Items.Clear();
                this.uComboDistQualityUserID3.Items.Clear();
                this.uComboDistEtcUserID3.Items.Clear();


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                String strPlantCode = this.uComboPlant.Value.ToString();

                DataTable dtDept = clsDept.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                //부서 콤보 설정
                WinComboEditor com = new WinComboEditor();
                com.mfSetComboEditor(this.uComboDistPurchaseUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistProductUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistDevelopUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistEquipUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistQualityUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

                com.mfSetComboEditor(this.uComboDistEtcUserID3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "DeptCode", "DeptName", dtDept);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #region 작성불가
        private void SetDisWriteable()
        {
            try
            {
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;

                this.uTextLotNo.ReadOnly = true;
                this.uTextLotNo.Appearance.BackColor = Color.Gainsboro;

                this.uTextGRNo.ReadOnly = true;
                this.uTextGRNo.Appearance.BackColor = Color.Gainsboro;

                this.uTextWriterID.ButtonsRight["Search"].Visible = false;
                this.uTextWriterID.ReadOnly = true;
                this.uTextWriterID.Appearance.BackColor = Color.Gainsboro;

                this.uDateWriteDate.ReadOnly = true;
                this.uDateWriteDate.Appearance.BackColor = Color.Gainsboro;

                this.uTextWriteName.ReadOnly = true;
                this.uTextWriteName.Appearance.BackColor = Color.Gainsboro;

                this.uTextMaterialCode.ReadOnly = true;
                this.uTextMaterialCode.Appearance.BackColor = Color.Gainsboro;

                this.uTextMaterialName.ReadOnly = true;
                this.uTextMaterialName.Appearance.BackColor = Color.Gainsboro;

                this.uTextMaterialSpec.ReadOnly = true;
                this.uTextMaterialSpec.Appearance.BackColor = Color.Gainsboro;

                this.uTextVendorCode.ReadOnly = true;
                this.uTextVendorCode.Appearance.BackColor = Color.Gainsboro;

                this.uTextVendorName.ReadOnly = true;
                this.uTextVendorName.Appearance.BackColor = Color.Gainsboro;

                this.uTextDepartment.ReadOnly = true;
                this.uTextDepartment.Appearance.BackColor = Color.Gainsboro;

                this.uTextQty.ReadOnly = true;
                this.uTextQty.Appearance.BackColor = Color.Gainsboro;

                this.uTextEtcDesc.ReadOnly = true;
                this.uTextEtcDesc.Appearance.BackColor = Color.Gainsboro;

                this.uTextReqObjectDesc.ReadOnly = true;
                this.uTextReqObjectDesc.Appearance.BackColor = Color.Gainsboro;

                this.uTextResult.ReadOnly = true;
                this.uTextResult.Appearance.BackColor = Color.Gainsboro;

                this.uOptionInspectResult.Enabled = false;

                this.uCheckCompleteFlag.Enabled = false;

                this.uDateMeetingDate1.ReadOnly = true;
                this.uDateMeetingDate1.Appearance.BackColor = Color.Gainsboro;

                this.uTextMeetingPlace1.ReadOnly = true;
                this.uTextMeetingPlace1.Appearance.BackColor = Color.Gainsboro;

                this.uTextFileName1.ButtonsRight["Search"].Visible = false;
                this.uTextFileName1.ReadOnly = true;
                this.uTextFileName1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistPurchaseUserID1.ButtonsRight["Search"].Visible = false;
                this.uTextDistPurchaseUserID1.ReadOnly = true;
                this.uTextDistPurchaseUserID1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistPurchaseUserName1.ReadOnly = true;
                this.uTextDistPurchaseUserName1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistProductUserID1.ButtonsRight["Search"].Visible = false;
                this.uTextDistProductUserID1.ReadOnly = true;
                this.uTextDistProductUserID1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistProductUserName1.ReadOnly = true;
                this.uTextDistProductUserName1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistDevelopUserID1.ButtonsRight["Search"].Visible = false;
                this.uTextDistDevelopUserID1.ReadOnly = true;
                this.uTextDistDevelopUserID1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistDevelopUserName1.ReadOnly = true;
                this.uTextDistDevelopUserName1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEquipUserID1.ButtonsRight["Search"].Visible = false;
                this.uTextDistEquipUserID1.ReadOnly = true;
                this.uTextDistEquipUserID1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEquipUserName1.ReadOnly = true;
                this.uTextDistEquipUserName1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistQualityUserID1.ButtonsRight["Search"].Visible = false;
                this.uTextDistQualityUserID1.ReadOnly = true;
                this.uTextDistQualityUserID1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistQualityUserName1.ReadOnly = true;
                this.uTextDistQualityUserName1.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEtcUserID1.ButtonsRight["Search"].Visible = false;
                this.uTextDistEtcUserID1.ReadOnly = true;
                this.uTextDistEtcUserID1.Appearance.BackColor = Color.Gainsboro;

                this.uTextMRBDesc.ReadOnly = true;
                this.uTextMRBDesc.Appearance.BackColor = Color.Gainsboro;

                this.uTextMRBUserID.ButtonsRight["Search"].Visible = false;
                this.uTextMRBUserID.ReadOnly = true;
                this.uTextMRBUserID.Appearance.BackColor = Color.Gainsboro;

                this.uTextMRBUserName.ReadOnly = true;
                this.uTextMRBUserName.Appearance.BackColor = Color.Gainsboro;

                this.uDateMRBDeliveryDate.ReadOnly = true;
                this.uDateMRBDeliveryDate.Appearance.BackColor = Color.Gainsboro;

                this.uTextMRBEtcDesc.ReadOnly = true;
                this.uTextMRBEtcDesc.Appearance.BackColor = Color.Gainsboro;

                this.uDateMeetingDate2.ReadOnly = true;
                this.uDateMeetingDate2.Appearance.BackColor = Color.Gainsboro;

                this.uTextMeetingPlace2.ReadOnly = true;
                this.uTextMeetingPlace2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistPurchaseUserID2.ButtonsRight["Search"].Visible = false;
                this.uTextDistPurchaseUserID2.ReadOnly = true;
                this.uTextDistPurchaseUserID2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistPurchaseUserName2.ReadOnly = true;
                this.uTextDistPurchaseUserName2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistProductUserID2.ButtonsRight["Search"].Visible = false;
                this.uTextDistProductUserID2.ReadOnly = true;
                this.uTextDistProductUserID2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistProductUserName2.ReadOnly = true;
                this.uTextDistProductUserName2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistDevelopUserID2.ButtonsRight["Search"].Visible = false;
                this.uTextDistDevelopUserID2.ReadOnly = true;
                this.uTextDistDevelopUserID2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistDevelopUserName2.ReadOnly = true;
                this.uTextDistDevelopUserName2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEquipUserID2.ButtonsRight["Search"].Visible = false;
                this.uTextDistEquipUserID2.ReadOnly = true;
                this.uTextDistEquipUserID2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEquipUserName2.ReadOnly = true;
                this.uTextDistEquipUserName2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistQualityUserID2.ButtonsRight["Search"].Visible = false;
                this.uTextDistQualityUserID2.ReadOnly = true;
                this.uTextDistQualityUserID2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistQualityUserName2.ReadOnly = true;
                this.uTextDistQualityUserName2.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEtcUserID2.ButtonsRight["Search"].Visible = false;
                this.uTextDistEtcUserID2.ReadOnly = true;
                this.uTextDistEtcUserID2.Appearance.BackColor = Color.Gainsboro;

                this.uTextResultDesc.ReadOnly = true;
                this.uTextResultDesc.Appearance.BackColor = Color.Gainsboro;

                this.uTextResultFileName.ButtonsRight["Search"].Visible = false;
                this.uTextResultFileName.ReadOnly = true;
                this.uTextResultFileName.Appearance.BackColor = Color.Gainsboro;

                this.uTextResultUserID.ButtonsRight["Search"].Visible = false;
                this.uTextResultUserID.ReadOnly = true;
                this.uTextResultUserID.Appearance.BackColor = Color.Gainsboro;

                this.uTextResultUserName.ReadOnly = true;
                this.uTextResultUserName.Appearance.BackColor = Color.Gainsboro;

                this.uDateResultDeliveryDate.ReadOnly = true;
                this.uDateResultDeliveryDate.Appearance.BackColor = Color.Gainsboro;

                this.uTextResultEtcDesc.ReadOnly = true;
                this.uTextResultEtcDesc.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreePurchaseUserID.ButtonsRight["Search"].Visible = false;
                this.uTextAgreePurchaseUserID.ReadOnly = true;
                this.uTextAgreePurchaseUserID.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreePurchaseUserName.ReadOnly = true;
                this.uTextAgreePurchaseUserName.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeProductUserID.ButtonsRight["Search"].Visible = false;
                this.uTextAgreeProductUserID.ReadOnly = true;
                this.uTextAgreeProductUserID.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeProductUserName.ReadOnly = true;
                this.uTextAgreeProductUserName.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeDevelopUserID.ButtonsRight["Search"].Visible = false;
                this.uTextAgreeDevelopUserID.ReadOnly = true;
                this.uTextAgreeDevelopUserID.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeDevelopUserName.ReadOnly = true;
                this.uTextAgreeDevelopUserName.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeEquipUserID.ButtonsRight["Search"].Visible = false;
                this.uTextAgreeEquipUserID.ReadOnly = true;
                this.uTextAgreeEquipUserID.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeEquipUserName.ReadOnly = true;
                this.uTextAgreeEquipUserName.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeQualityUserID.ButtonsRight["Search"].Visible = false;
                this.uTextAgreeQualityUserID.ReadOnly = true;
                this.uTextAgreeQualityUserID.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeQualityUserName.ReadOnly = true;
                this.uTextAgreeQualityUserName.Appearance.BackColor = Color.Gainsboro;

                this.uTextAgreeDesc.ReadOnly = true;
                this.uTextAgreeDesc.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistPurchaseUserID3.ButtonsRight["Search"].Visible = false;
                this.uTextDistPurchaseUserID3.ReadOnly = true;
                this.uTextDistPurchaseUserID3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistPurchaseUserName3.ReadOnly = true;
                this.uTextDistPurchaseUserName3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistProductUserID3.ButtonsRight["Search"].Visible = false;
                this.uTextDistProductUserID3.ReadOnly = true;
                this.uTextDistProductUserID3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistProductUserName3.ReadOnly = true;
                this.uTextDistProductUserName3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistDevelopUserID3.ButtonsRight["Search"].Visible = false;
                this.uTextDistDevelopUserID3.ReadOnly = true;
                this.uTextDistDevelopUserID3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEquipUserID3.ButtonsRight["Search"].Visible = false;
                this.uTextDistEquipUserID3.ReadOnly = true;
                this.uTextDistEquipUserID3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistEquipUserName3.ReadOnly = true;
                this.uTextDistEquipUserName3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistQualityUserID3.ButtonsRight["Search"].Visible = false;
                this.uTextDistQualityUserID3.ReadOnly = true;
                this.uTextDistQualityUserID3.Appearance.BackColor = Color.Gainsboro;

                this.uTextDistQualityUserName3.ReadOnly = true;
                this.uTextDistQualityUserName3.Appearance.BackColor = Color.Gainsboro;

                this.uCheckValidationDateFlag.Enabled = false;
                this.uDateValidationDate.ReadOnly = true;
                this.uDateValidationDate.Appearance.BackColor = Color.Gainsboro;

                this.uComboDistPurchaseUserID3.ReadOnly = true;
                this.uComboDistProductUserID3.ReadOnly = true;
                this.uComboDistDevelopUserID3.ReadOnly = true;
                this.uComboDistEquipUserID3.ReadOnly = true;
                this.uComboDistQualityUserID3.ReadOnly = true;
                this.uComboDistEtcUserID3.ReadOnly = true;

                this.uGridShipInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo();
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion
    }
}
