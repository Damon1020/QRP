﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질검사관리                                          */
/* 모듈(분류)명 : 공정 Low Yield List                                   */
/* 프로그램ID   : frmINSZ0016.cs                                        */
/* 프로그램명   : Test Abnormal Yield 정보                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-12-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
//using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.IO;
using System.Collections;
using Microsoft.VisualBasic;
using System.Net.Mail;

namespace QRPINS.UI
{
    // IToolbar Interface 상속 추가
    public partial class frmINSZ0016 : Form, IToolbar
    {

        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private int m_intStep = 0;

        public frmINSZ0016()
        {
            InitializeComponent();
        }

        private void frmINSZ0016_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
            m_resSys.Close();
        }

        private void frmINSZ0016_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmINSZ0016_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;


            SetToolAuth();
            InitTitle();
            InitLabel();
            InitGroupBox();
            InitButton();
            initTab();
            InitComboBox();
            InitGrid();


            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region Init

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

                dtAuth.Dispose();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Title
        /// </summary>
        private void InitTitle()
        {

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            titleArea.mfSetLabelText(titleArea.TextName, m_resSys.GetString("SYS_FONTNAME"), 12);
            m_resSys.Close();

            this.uTextPlantCode.ValueChanged += new System.EventHandler(this.uTextPlantCode_ValueChanged);
            this.uTextAttachFile.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAttachFile_EditorButtonClick);
            this.uButtonAbnormal.Click += new System.EventHandler(this.uButtonAbnormal_Click);
            this.uOptionAbnormal.ValueChanged += new System.EventHandler(this.uOptionAbnormal_ValueChanged);
            this.uCheckS5.CheckedChanged += new EventHandler(this.uCheckS5_CheckedChanged);

            this.uTextRegistUserID.KeyDown += new KeyEventHandler(uTextUserID_KeyDown);
            this.uTextRegistUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextUserID_EditorButtonClick);
            this.uTextRegistUserID.ValueChanged += new EventHandler(this.uTextUserID_ValueChanged);

            this.uTextS1UserID.KeyDown += new KeyEventHandler(uTextUserID_KeyDown);
            this.uTextS1UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextUserID_EditorButtonClick);
            this.uTextS1UserID.ValueChanged += new EventHandler(this.uTextUserID_ValueChanged);
            this.uGridS1File.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFile_ClickCellButton);
            this.uGridS1File.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridFile_DoubleClickCell);
            this.uGridS1File.KeyDown += new KeyEventHandler(this.uGridFile_KeyDown);
            this.uButtonS1.Click += new System.EventHandler(this.uButtonFileDown_Click);
            this.uButtonS1DelRow.Click += new System.EventHandler(this.uButtonDelRow_Click);

            this.uTextS2UserID.KeyDown += new KeyEventHandler(uTextUserID_KeyDown);
            this.uTextS2UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextUserID_EditorButtonClick);
            this.uTextS2UserID.ValueChanged += new EventHandler(this.uTextUserID_ValueChanged);
            this.uGridS2File.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFile_ClickCellButton);
            this.uGridS2File.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridFile_DoubleClickCell);
            this.uGridS2File.KeyDown += new KeyEventHandler(this.uGridFile_KeyDown);
            this.uButtonS2.Click += new System.EventHandler(this.uButtonFileDown_Click);
            this.uButtonS2DelRow.Click += new System.EventHandler(this.uButtonDelRow_Click);

            this.uTextS3UserID.KeyDown += new KeyEventHandler(uTextUserID_KeyDown);
            this.uTextS3UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextUserID_EditorButtonClick);
            this.uTextS3UserID.ValueChanged += new EventHandler(this.uTextUserID_ValueChanged);
            this.uGridS3File.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFile_ClickCellButton);
            this.uGridS3File.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridFile_DoubleClickCell);
            this.uGridS3File.KeyDown += new KeyEventHandler(this.uGridFile_KeyDown);
            this.uButtonS3.Click += new System.EventHandler(this.uButtonFileDown_Click);
            this.uButtonS3DelRow.Click += new System.EventHandler(this.uButtonDelRow_Click);

            this.uTextS4UserID.KeyDown += new KeyEventHandler(uTextUserID_KeyDown);
            this.uTextS4UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextUserID_EditorButtonClick);
            this.uTextS4UserID.ValueChanged += new EventHandler(this.uTextUserID_ValueChanged);
            this.uGridS4File.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFile_ClickCellButton);
            this.uGridS4File.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridFile_DoubleClickCell);
            this.uGridS4File.KeyDown += new KeyEventHandler(this.uGridFile_KeyDown);
            this.uButtonS4.Click += new System.EventHandler(this.uButtonFileDown_Click);
            this.uButtonS4DelRow.Click += new System.EventHandler(this.uButtonDelRow_Click);

            this.uTextS5UserID.KeyDown += new KeyEventHandler(uTextUserID_KeyDown);
            this.uTextS5UserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextUserID_EditorButtonClick);
            this.uTextS5UserID.ValueChanged += new EventHandler(this.uTextUserID_ValueChanged);
            this.uButtonReturn.Click += new System.EventHandler(this.uButtonReturn_Click);
        }

        /// <summary>
        /// Button
        /// </summary>
        private void InitButton()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinButton wbtn = new WinButton();

            wbtn.mfSetButton(this.uButtonAccept, this.uButtonAccept.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
            wbtn.mfSetButton(this.uButtonAbnormal, this.uButtonAbnormal.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
            wbtn.mfSetButton(this.uButtonReturn, this.uButtonReturn.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);

            wbtn.mfSetButton(this.uButtonS1, this.uButtonS1.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            wbtn.mfSetButton(this.uButtonS2, this.uButtonS2.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            wbtn.mfSetButton(this.uButtonS3, this.uButtonS3.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            wbtn.mfSetButton(this.uButtonS4, this.uButtonS4.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);

            wbtn.mfSetButton(this.uButtonS1DelRow, this.uButtonS1DelRow.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            wbtn.mfSetButton(this.uButtonS2DelRow, this.uButtonS2DelRow.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            wbtn.mfSetButton(this.uButtonS3DelRow, this.uButtonS3DelRow.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            wbtn.mfSetButton(this.uButtonS4DelRow, this.uButtonS4DelRow.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

            wbtn.mfSetButton(this.uButtonDel_Email, this.uButtonDel_Email.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            wbtn.mfSetButton(this.uButtonOK, this.uButtonOK.Text, m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

            this.uButtonOK.Click += new EventHandler(uButtonOK_Click);
            m_resSys.Close();
        }

        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void initTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTabStep, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Office2007Ribbon
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label
        /// </summary>
        private void InitLabel()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinLabel wlbl = new WinLabel();

            Infragistics.Win.Misc.UltraLabel[] uLabel = { this.ulabelAriseCause, this.uLabelAriseDate, this.uLabelAriseProcess, 
                                                          this.uLabelAttachFile, this.uLabelBinName,this.uLabelCustomer,this.uLabelEquipCode,
                                                          this.uLabelLotNo,this.uLabelLotQty,this.uLabelLotRelease,this.ulabelMeasureDesc,this.uLabelOccurDate, 
                                                          this.uLabelPackage, this.uLabelPlant,this.uLabelProcessYield,this.uLabelProduct,this.uLabelRegistUser, this.uLabelAbnormal,
                                                          this.uLabelS1User, this.uLabelS1Complete,this.uLabelS1Dept,this.uLabelS1File,this.uLabelS1Result,
                                                          this.uLabelS2Analysis,this.uLabelS2Complete,this.uLabelS2Date,this.uLabelS2Dept,this.uLabelS2File,this.uLabelS2User,
                                                          this.uLabelS3Comment,this.uLabelS3Complete,this.uLabelS3Date,this.uLabelS3Dept,this.uLabelS3File,this.uLabelS3User,
                                                          this.uLabelS4Comment,this.uLabelS4Complete,this.uLabelS4Date,this.uLabelS4File,this.uLabelS4User,
                                                          this.uLabelS5Comment,this.uLabelS5Complete,this.uLabelS5Date,this.uLabelS5User,
                                                          this.uLabelSearchCustomer,this.uLabelSearchPlant,this.uLabelWorkUser,this.uLabelOkNg,this.uLabelPack,this.uLabelS5ReturnDept,
                                                          this.uLabelCustomerProductCode, this.uLabelLotProcessState, this.uLabelProcessHoldState, this.uLabelAcceptDate,
                                                          this.uLabelEtcDesc, this.uLabelS0Dept, this.uLabelManageNo, this.uLabelEmail
                                                        };
            foreach (Infragistics.Win.Misc.UltraLabel Label in uLabel)
            {
                if (Label.Name.Equals("uLabelRegistUser") || Label.Name.Equals("uLabelS1Dept") || Label.Name.Equals("uLabelS1User")
                    || Label.Name.Equals("uLabelS2Date") || Label.Name.Equals("uLabelS2Dept") || Label.Name.Equals("uLabelS2User")
                    || Label.Name.Equals("uLabelS3Date") || Label.Name.Equals("uLabelS3Dept") || Label.Name.Equals("uLabelS3User")
                    || Label.Name.Equals("uLabelS4Date") || Label.Name.Equals("uLabelS4User")
                    || Label.Name.Equals("uLabelS5Date") || Label.Name.Equals("uLabelS5User") || Label.Name.Equals("uLabelOkNg"))
                    wlbl.mfSetLabel(Label, Label.Text, m_resSys.GetString("SYS_FONTNAME"), true, true);
                else
                    wlbl.mfSetLabel(Label, Label.Text, m_resSys.GetString("SYS_FONTNAME"), true, false);
            }

            m_resSys.Close();
        }

        /// <summary>
        /// GroupBox
        /// </summary>
        private void InitGroupBox()
        {
            ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);
            WinGroupBox wGroup = new WinGroupBox();

            wGroup.mfSetGroupBox(this.uGroupBox, GroupBoxType.INFO, "Eng'r Check", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

            wGroup.mfSetGroupBox(this.uGroupBoxLoss, GroupBoxType.LIST, "Loss", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

            wGroup.mfSetGroupBox(this.uGroupBoxStep5, GroupBoxType.INFO, "Step5. QA Approval", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


            // Set Font
            this.uGroupBox.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            this.uGroupBox.HeaderAppearance.FontData.SizeInPoints = 9;

            this.uGroupBoxLoss.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            this.uGroupBoxLoss.HeaderAppearance.FontData.SizeInPoints = 9;

            this.uGroupBoxStep5.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            this.uGroupBoxStep5.HeaderAppearance.FontData.SizeInPoints = 9;

            this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);

            // ContentsGroupBox 접힌 상태
            this.uGroupBoxContentsArea.Expanded = false;

        }

        /// <summary>
        /// Combo
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtDept = new DataTable();
                dtDept.Columns.Add("DeptCode", typeof(string));
                dtDept.Columns.Add("DeptName", typeof(string));

                // 공장콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                //LotRelease 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clscommon = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clscommon);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                DataTable dtLotR = clscommon.mfReadCommonCode("C0056", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchLotRelease, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtLotR);

                // 고객사 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomerPopup(m_resSys.GetString("SYS_LANG"));
                DataTable dtCustomerCombo = new DataTable();
                if (dtCustomer.Rows.Count > 0)
                    dtCustomerCombo = dtCustomer.DefaultView.ToTable(true, "CustomerCode", "CustomerName");

                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                        , "", "", "전체", "CustomerCode", "CustomerName", dtCustomerCombo);

                //검색조건 - Package 콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                                       , "Package", "ComboName", dtPackage);

                dtLotR = clscommon.mfReadCommonCode("C0080", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboReturnStep, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                                       , "ComCode", "ComCodeName", dtLotR);

                //获取部门信息
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                dtDept = clsDept.mfReadSYSDeptForCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                // 부서정보 콤보 
                wCombo.mfSetComboEditor(this.uComboS0Dept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);

                wCombo.mfSetComboEditor(this.uComboS1Dept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);

                wCombo.mfSetComboEditor(this.uComboS2Dept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);

                wCombo.mfSetComboEditor(this.uComboS3Dept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);

                wCombo.mfSetComboEditor(this.uComboDept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, true, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);

                clscommon.Dispose();
                clsCustomer.Dispose();
                clsPackage.Dispose();
                clsPlant.Dispose();
                dtLotR.Dispose();
                dtCustomer.Dispose();
                dtCustomerCombo.Dispose();
                dtPackage.Dispose();
                dtPlant.Dispose();
                clsDept.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            


        }

        /// <summary>
        /// Grid
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회결과 그리드
                wGrid.mfInitGeneralGrid(this.uGridAbormal, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "ManageNo", "ManageNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "AriseProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "AriseProcessName", "공정명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "AriseDate", "발생일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "AriseTime", "발생시간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "DeptName", "귀책부서", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "PACKAGE", "PACKAGE", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "EquipCode", "설비번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "LossName", "LOSS명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");  // 2012-12-03 LOSS명 추가 (LOSS수량이 제일 큰 LOSS명)

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "AriseCause", "발생원인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");  // 2012-12-03 발생원인

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "MeasureDesc", "대책사항", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");  // 2012-12-03 대책사항

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "LotQty", "Lot수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "LossQty", "Loss수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "ProcessYield", "공정수율", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnn.nnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "CurrentStatus", "상태", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "ManFlag", "Man", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "MethodFlag", "Method", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "MaterialFlag", "Material", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "MachineFlag", "Machine", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "EnvironmentFlag", "Environment", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "LotReleaseFlag", "LotRelease여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "MESTFlag", "MES전송여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridAbormal, 0, "MESReleaseTFlag", "MESRelease전송여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 그리드 편집불가 상태로
                //this.uGridAbormal.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                #endregion

                #region 상세 그리드
                wGrid.mfInitGeneralGrid(this.uGridProcLowD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                   , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "LossCode", "LossCode", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "LossName", "Loss명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridProcLowD, 0, "LossQty", "Loss수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                   , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn,nnn,nnn", "");

                this.uGridProcLowD.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                #endregion

                #region 첨부파일

                wGrid.mfInitGeneralGrid(this.uGridS1File, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridS1File, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridS1File, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS1File, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridS1File, 0, "FileName", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS1File, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfInitGeneralGrid(this.uGridS2File, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                  , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                  , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                  , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                  , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridS2File, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridS2File, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS2File, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridS2File, 0, "FileName", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS2File, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfInitGeneralGrid(this.uGridS3File, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                  , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                  , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                  , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                  , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridS3File, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridS3File, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS3File, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridS3File, 0, "FileName", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS3File, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfInitGeneralGrid(this.uGridS4File, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                  , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                  , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                  , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                  , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridS4File, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridS4File, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS4File, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridS4File, 0, "FileName", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridS4File, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfAddRowGrid(this.uGridS1File, 0);
                wGrid.mfAddRowGrid(this.uGridS2File, 0);
                wGrid.mfAddRowGrid(this.uGridS3File, 0);
                wGrid.mfAddRowGrid(this.uGridS4File, 0);

                #endregion

                #region Email

                wGrid.mfInitGeneralGrid(this.uGridEmail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                   , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.None
                   , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                   , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEmail, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 1
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "UserID", "사용자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "UserName", "사용자명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "DeptCode", "부서코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "DeptName", "부서명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "EMail", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEmail, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGridEmail, 0);

                #endregion

                // MaskInput 설정
                this.uGridAbormal.DisplayLayout.Bands[0].Columns["LotQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridAbormal.DisplayLayout.Bands[0].Columns["LossQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridProcLowD.DisplayLayout.Bands[0].Columns["LossQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                // Set FontSize
                this.uGridAbormal.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridAbormal.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridProcLowD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridProcLowD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridEmail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEmail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridAbormal.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridAbormal_DoubleClickRow);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// Clear
        /// </summary>
        private void Init()
        {
            try
            {
                bool bolfalse = false;

                this.uTextManageNo.Clear();
                this.uTextCustomerProductCode.Clear();
                this.uTextPlantCode.Text = "2210";
                this.uTextPlantName.Clear();
                this.uTextLotNo.Clear();
                this.uTextCustomerCode.Clear();
                this.uTextCustomerName.Clear();
                this.uTextEquipCode.Clear();
                this.uTextEquipName.Clear();
                this.uTextCustomerProductSpec.Clear();
                this.uTextPackage.Clear();
                this.uTextBinName.Clear();
                this.uTextAriseProcessCode.Clear();
                this.uTextAriseProcessName.Clear();
                this.uTextAriseDate.Clear();
                this.uTextAriseTime.Clear();
                this.uTextWorkUserID.Clear();
                this.uTextWorkUserName.Clear();
                this.uTextLotProcessState.Clear();
                this.uTextProcessHoldState.Clear();
                this.uTextLotQty.Clear();
                this.uTextProcessYield.Clear();

                this.uTextAcceptDate.Clear();
                this.uTextAcceptTime.Clear();
                this.uTextAriseCause.Clear();
                this.uTextMeasureDesc.Clear();
                this.uTextRegistUserID.Clear();
                this.uTextRegistUserName.Clear();
                this.uTextAttachFile.Clear();
                this.uComboS0Dept.Value = string.Empty;

                this.uCheckFir4MManFlag.Checked = bolfalse;
                this.uCheckFir4MMethodFlag.Checked = bolfalse;
                this.uCheckFir4MMaterialFlag.Checked = bolfalse;
                this.uCheckFir4MMachineFlag.Checked = bolfalse;
                this.uCheckFir4MEnviroFlag.Checked = bolfalse;

                this.uButtonAbnormal.Enabled = bolfalse;
                this.uTabStep.Enabled = bolfalse;
                this.uGroupBox.Enabled = bolfalse;

                this.uOptionAbnormal.Value = null;

                // Step1
                this.uTextS1UserID.Clear();
                this.uTextS1UserName.Clear();
                this.uTextS1Comment.Clear();
                this.uComboS1Dept.Value = string.Empty;
                this.uCheckS1.Checked = bolfalse;
                // Step2
                this.uTextS2UserID.Clear();
                this.uTextS2UserName.Clear();
                this.uTextS2Analysis.Clear();
                this.uDateS2Date.Value = DateTime.Now;
                this.uComboS2Dept.Value = string.Empty;
                this.uCheckS2.Checked = bolfalse;
                // Step3
                this.uTextS3UserID.Clear();
                this.uTextS3UserName.Clear();
                this.uTextS3Comment.Clear();
                this.uDateS3Date.Value = DateTime.Now;
                this.uComboS3Dept.Value = string.Empty;
                this.uCheckS3.Checked = bolfalse;
                // Step4
                this.uTextS4UserID.Clear();
                this.uTextS4UserName.Clear();
                this.uTextS4Comment.Clear();
                this.uDateS4Date.Value = DateTime.Now;
                this.uCheckS4.Checked = bolfalse;
                
                // Step5
                this.uTextS5UserID.Clear();
                this.uTextS5UserName.Clear();
                this.uTextS5Comment.Clear();
                this.uDateS5Date.Value = DateTime.Now;
                this.uCheckS5.Checked = bolfalse;

                //Email
                this.uComboDept.Clear();
                this.uComboDept.Value = string.Empty;
                
                this.uOptionOkNg.Value = null;
                this.uCheckReturn.Checked = bolfalse;
                this.uComboReturnStep.Value = string.Empty;

                this.uCheckMESFlag.Checked = bolfalse;
                this.uCheckMESReleaseTFlag.Checked = bolfalse;
                EngrCheck(bolfalse);
                if (this.uGridProcLowD.Rows.Count > 0)
                {
                    this.uGridProcLowD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridProcLowD.Rows.All);
                    this.uGridProcLowD.DeleteSelectedRows(false);
                }
                if (this.uGridS1File.Rows.Count > 0)
                {
                    this.uGridS1File.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridS1File.Rows.All);
                    this.uGridS1File.DeleteSelectedRows(false);

                }
                if (this.uGridS2File.Rows.Count > 0)
                {
                    this.uGridS2File.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridS2File.Rows.All);
                    this.uGridS2File.DeleteSelectedRows(false);
                }
                if (this.uGridS3File.Rows.Count > 0)
                {
                    this.uGridS3File.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridS3File.Rows.All);
                    this.uGridS3File.DeleteSelectedRows(false);
                }
                if (this.uGridS4File.Rows.Count > 0)
                {
                    this.uGridS4File.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridS4File.Rows.All);
                    this.uGridS4File.DeleteSelectedRows(false);
                }
                if (this.uGridEmail.Rows.Count > 0)
                {
                    this.uGridEmail.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEmail.Rows.All);
                    this.uGridEmail.DeleteSelectedRows(false);
                }

                m_intStep = 0;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region IToolbar 멤버

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;
                string strLang = m_resSys.GetString("SYS_LANG");

                if (this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = false;

                // 검색시작일이 검색 종료일 보다 큰경우 메세지 처리.
                if (this.uDateFromAriseDate.DateTime.Date > this.uDateToAriseDate.DateTime.Date)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M000200"
                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                // 검색조건 저장
                string strPlantCode = this.uComboSearchPlant.Value == null ? string.Empty : this.uComboSearchPlant.Value.ToString();
                string strLotRealase = this.uComboSearchLotRelease.Value == null ? string.Empty : this.uComboSearchLotRelease.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value == null ? string.Empty : this.uComboSearchPackage.Value.ToString();
                string strCustomer = this.uComboSearchCustomer.Value == null ? string.Empty : this.uComboSearchCustomer.Value.ToString();
                string strFromAriseDate = this.uDateFromAriseDate.DateTime.Date.ToString("yyyy-MM-dd");
                string strToAriseDate = this.uDateToAriseDate.DateTime.Date.ToString("yyyy-MM-dd");


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                QRPINS.BL.INSSTS.AbnormalYield clsAbo = new QRPINS.BL.INSSTS.AbnormalYield();
                brwChannel.mfCredentials(clsAbo);

                DataTable dtRtn = clsAbo.mfReadINSProcAbnormalYield(strPlantCode, strFromAriseDate, strToAriseDate, string.Empty, strLotRealase, strPackage, strCustomer, strLang);

                /////////////

                this.uGridAbormal.DataSource = dtRtn;
                this.uGridAbormal.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */



                if (dtRtn.Rows.Count == 0)
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);
                else
                    for (int i = 0; i < this.uGridAbormal.Rows.Count; i++)
                        //Release,MESTFlag 두가지 모두 성공한 정보가 아니면 줄 색변경
                        if (!this.uGridAbormal.Rows[i].Cells["MESReleaseTFlag"].Value.ToString().Equals("T")
                            && !this.uGridAbormal.Rows[i].Cells["MESTFlag"].Value.ToString().Equals("T"))
                        {
                            this.uGridAbormal.Rows[i].Appearance.BackColor = Color.Salmon;
                        }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfCreate()
        {
            this.uGroupBoxContentsArea.Expanded = true;
            
        }

        public void mfDelete()
        {
            
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridAbormal.Rows.Count == 0)
                    return;

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridAbormal);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
            
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");

                if (CheckInput())
                {
                    // 최종완료되었지만 MES전송이 안된경우
                    if (m_intStep == 5
                        && !this.uCheckMESFlag.Checked && !this.uCheckMESReleaseTFlag.Checked)
                    {
                        // 최종완료되어 수정이 불가합니다. MES처리 하시겠습니까?
                        Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001053"
                                            , "M001541"
                                            , Infragistics.Win.HAlign.Right);

                        if (Result == DialogResult.Yes)
                        {
                            // MES I/F 메소드 호출
                            Send_MESInterFace();
                            return;
                        }
                        else
                            return;
                    }
                    else
                    {
                        if (!this.uCheckMESReleaseTFlag.Checked && this.uCheckS5.Checked)
                        {
                            // 작성완료 체크후 저장하시면 차후 수정할수 없습니다. 저장하시겠습니까?
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000794", "M000979"
                                                    , "M000983"
                                                    , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right);
                        }

                        if (Result == DialogResult.Yes)
                        {

                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                            QRPINS.BL.INSSTS.AbnormalYield clsAb = new QRPINS.BL.INSSTS.AbnormalYield();
                            brwChannel.mfCredentials(clsAb);

                            DataTable dtHeader = RtnHeader();
                            DataTable dtFile = RtnFile();

                            // 저장 메소드 호출
                            string strErrRtn = clsAb.mfSaveINSProcAbnormalYield(dtHeader, dtFile, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            clsAb.Dispose();
                            dtHeader.Dispose();
                            dtFile.Dispose();
                            // 결과검사
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum == 0)
                            {
                                // 파일 업로드 메소드 호출
                                FileUpload();

                                if (this.uGridEmail.Rows.Count > 0)
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001449"
                                        , "M001448"
                                        , Infragistics.Win.HAlign.Right);
                                    if (Result == DialogResult.Yes)
                                    {
                                        SendMail_Receipt(this.uComboSearchPlant.Value.ToString());
                                    }
                                }

                                string strReleaseFlag = string.Empty;
                                if (!this.uCheckMESReleaseTFlag.Checked
                                    && (this.uCheckS5.Checked && this.uCheckS5.Enabled))
                                {
                                    strReleaseFlag = "Y";

                                    // MES I/F 메소드 호출
                                    Send_MESInterFace(strReleaseFlag);
                                }
                                else
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001022", "M001023", "M000930", Infragistics.Win.HAlign.Right);

                                    mfSearch();
                                }

                            }
                            else
                            {
                                if (ErrRtn.ErrMessage.Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                                  , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000953"
                                                                  , Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                                  , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                  , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                                  , ErrRtn.ErrMessage
                                                                  , Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }



        #endregion

        #region Method

        /// <summary>
        /// Header
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAriseProcessCode">발생공정</param>
        /// <param name="strAriseDate">발생일</param>
        /// <param name="strAriseTime">발생시간</param>
        /// <param name="strLang">사용언어</param>
        private void SearchHeaderDetail(string strPlantCode, string strAriseProcessCode, string strAriseDate, string strAriseTime, string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                QRPINS.BL.INSSTS.AbnormalYield ProcLow = new QRPINS.BL.INSSTS.AbnormalYield();
                brwChannel.mfCredentials(ProcLow);

                DataTable dtHeaderDetail = ProcLow.mfReadINSProcAbnormalYield_Detail(strPlantCode, strAriseProcessCode, strAriseDate, strAriseTime, strLang);

                ProcLow.Dispose();

                if (dtHeaderDetail.Rows.Count == 0)
                    return;

                this.uButtonAccept.Enabled = false;

                this.uTextPlantCode.Text = dtHeaderDetail.Rows[0]["PlantCode"].ToString();
                this.uTextManageNo.Text = dtHeaderDetail.Rows[0]["ManageNo"].ToString();
                this.uTextPlantName.Text = dtHeaderDetail.Rows[0]["PlantName"].ToString();
                this.uTextLotNo.Text = dtHeaderDetail.Rows[0]["LotNo"].ToString();
                this.uTextCustomerCode.Text = dtHeaderDetail.Rows[0]["CustomerCode"].ToString();
                this.uTextCustomerName.Text = dtHeaderDetail.Rows[0]["CustomerName"].ToString();
                this.uTextEquipCode.Text = dtHeaderDetail.Rows[0]["EquipCode"].ToString();
                this.uTextEquipName.Text = dtHeaderDetail.Rows[0]["EquipName"].ToString();
                this.uTextCustomerProductCode.Text = dtHeaderDetail.Rows[0]["ProductCode"].ToString();
                this.uTextCustomerProductSpec.Text = dtHeaderDetail.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                this.uTextPackage.Text = dtHeaderDetail.Rows[0]["PACKAGE"].ToString();
                //this.uTextBinName.Text = dtHeaderDetail.Rows[0]["BinName"].ToString();
                this.uTextAriseProcessCode.Text = dtHeaderDetail.Rows[0]["AriseProcessCode"].ToString();
                this.uTextAriseProcessName.Text = dtHeaderDetail.Rows[0]["AriseProcessName"].ToString();
                this.uTextAriseDate.Text = Convert.ToDateTime(dtHeaderDetail.Rows[0]["AriseDate"]).ToString("yyyy-MM-dd");
                this.uTextAriseTime.Text = Convert.ToDateTime(dtHeaderDetail.Rows[0]["AriseTime"]).ToString("HH:mm:ss");
                this.uTextLotProcessState.Text = dtHeaderDetail.Rows[0]["LotState"].ToString();
                this.uTextProcessHoldState.Text = dtHeaderDetail.Rows[0]["HoldState"].ToString();
                this.uTextWorkUserID.Text = dtHeaderDetail.Rows[0]["WorkUserID"].ToString();
                this.uTextWorkUserName.Text = dtHeaderDetail.Rows[0]["WorkUserName"].ToString();
                this.uTextEtcDesc.Text = dtHeaderDetail.Rows[0]["EtcDesc"].ToString();
                this.uTextLotQty.Text = Math.Floor(Convert.ToDecimal(dtHeaderDetail.Rows[0]["LotQty"])).ToString();
                //this.uTextProcessYield.Text = decimal.Round(Convert.ToDecimal(dtHeaderDetail.Rows[0]["ProcessYield"]), 3).ToString();
                this.uTextAcceptDate.Text = Convert.ToDateTime(dtHeaderDetail.Rows[0]["AcceptDate"]).ToString("yyyy-MM-dd");
                this.uTextAcceptTime.Text = Convert.ToDateTime(dtHeaderDetail.Rows[0]["AcceptTime"]).ToString("yyyy-MM-dd");

                this.uTextAriseCause.Text = dtHeaderDetail.Rows[0]["AriseCause"].ToString();
                this.uTextMeasureDesc.Text = dtHeaderDetail.Rows[0]["MeasureDesc"].ToString();
                this.uTextRegistUserID.Text = dtHeaderDetail.Rows[0]["RegistUserID"].ToString();
                this.uTextRegistUserName.Text = dtHeaderDetail.Rows[0]["RegistUserName"].ToString();
                this.uComboS0Dept.Value = dtHeaderDetail.Rows[0]["S0DeptCode"];

                this.uCheckFir4MManFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["ManFlag"]);
                this.uCheckFir4MMethodFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["MethodFlag"]);
                this.uCheckFir4MMaterialFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["MaterialFlag"]);
                this.uCheckFir4MMachineFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["MachineFlag"]);
                this.uCheckFir4MEnviroFlag.Checked = Convert.ToBoolean(dtHeaderDetail.Rows[0]["EnvironmentFlag"]);

                this.uOptionAbnormal.ValueChanged -= new System.EventHandler(this.uOptionAbnormal_ValueChanged);
                this.uOptionAbnormal.Value = dtHeaderDetail.Rows[0]["AbnormalFlag"];
                this.uOptionAbnormal.ValueChanged += new System.EventHandler(this.uOptionAbnormal_ValueChanged);

                // Step1
                this.uTextS1UserID.Text = dtHeaderDetail.Rows[0]["S1UserID"].ToString();
                this.uTextS1UserName.Text = dtHeaderDetail.Rows[0]["S1UserName"].ToString();
                this.uTextS1Comment.Text = dtHeaderDetail.Rows[0]["S1Comment"].ToString();
                this.uComboS1Dept.Value = dtHeaderDetail.Rows[0]["S1DeptCode"];
                this.uCheckS1.Checked = dtHeaderDetail.Rows[0]["S1CompleteFlag"].ToString() == "T" ? true : false;
                this.uCheckS1.Enabled = this.uCheckS1.Checked == true ? false : true;
                // Step2
                this.uTextS2UserID.Text = dtHeaderDetail.Rows[0]["S2UserID"].ToString();
                this.uTextS2UserName.Text = dtHeaderDetail.Rows[0]["S2UserName"].ToString();
                this.uTextS2Analysis.Text = dtHeaderDetail.Rows[0]["S2Comment"].ToString();
                this.uDateS2Date.Value = dtHeaderDetail.Rows[0]["S2Date"];
                this.uComboS2Dept.Value = dtHeaderDetail.Rows[0]["S2DeptCode"];
                this.uCheckS2.Checked = dtHeaderDetail.Rows[0]["S2CompleteFlag"].ToString() == "T" ? true : false;
                this.uCheckS2.Enabled = this.uCheckS2.Checked == true ? false : true;
                // Step3
                this.uTextS3UserID.Text = dtHeaderDetail.Rows[0]["S3UserID"].ToString();
                this.uTextS3UserName.Text = dtHeaderDetail.Rows[0]["S3UserName"].ToString();
                this.uTextS3Comment.Text = dtHeaderDetail.Rows[0]["S3Comment"].ToString();
                this.uDateS3Date.Value = dtHeaderDetail.Rows[0]["S3Date"];
                this.uComboS3Dept.Value = dtHeaderDetail.Rows[0]["S3DeptCode"];
                this.uCheckS3.Checked = dtHeaderDetail.Rows[0]["S3CompleteFlag"].ToString() == "T" ? true : false;
                this.uCheckS3.Enabled = this.uCheckS3.Checked == true ? false : true;
                // Step4
                this.uTextS4UserID.Text = dtHeaderDetail.Rows[0]["S4UserID"].ToString();
                this.uTextS4UserName.Text = dtHeaderDetail.Rows[0]["S4UserName"].ToString();
                this.uTextS4Comment.Text = dtHeaderDetail.Rows[0]["S4Comment"].ToString();
                this.uDateS4Date.Value = dtHeaderDetail.Rows[0]["S4Date"];
                this.uCheckS4.Checked = dtHeaderDetail.Rows[0]["S4CompleteFlag"].ToString() == "T" ? true : false;
                this.uCheckS4.Enabled = this.uCheckS4.Checked == true ? false : true;
                // Step5
                this.uTextS5UserID.Text = dtHeaderDetail.Rows[0]["S5UserID"].ToString();
                this.uTextS5UserName.Text = dtHeaderDetail.Rows[0]["S5UserName"].ToString();
                this.uTextS5Comment.Text = dtHeaderDetail.Rows[0]["S5Comment"].ToString();
                this.uDateS5Date.Value = dtHeaderDetail.Rows[0]["S5Date"];
                this.uCheckS5.CheckedChanged -= new EventHandler(this.uCheckS5_CheckedChanged);
                this.uCheckS5.Checked = dtHeaderDetail.Rows[0]["S5CompleteFlag"].ToString() == "T" ? true : false;
                this.uCheckS5.CheckedChanged += new EventHandler(this.uCheckS5_CheckedChanged);
                this.uCheckS5.Enabled = this.uCheckS5.Checked == true ? false : true;

                this.uOptionOkNg.Value = dtHeaderDetail.Rows[0]["ResultFlag"].ToString() == string.Empty ? null : dtHeaderDetail.Rows[0]["ResultFlag"];
                this.uCheckReturn.Checked = dtHeaderDetail.Rows[0]["ReturnFlag"].ToString() == "T" ? true : false;
                this.uComboReturnStep.Value = dtHeaderDetail.Rows[0]["ReturnStep"];

                this.uCheckMESFlag.Checked = dtHeaderDetail.Rows[0]["MESTFlag"].ToString() == "T" ? true : false;
                this.uCheckMESReleaseTFlag.Checked = dtHeaderDetail.Rows[0]["MESReleaseTFlag"].ToString() == "T" ? true : false;

                if(dtHeaderDetail.Rows[0]["AcceptFlag"].ToString().Equals("T"))
                    this.uButtonAbnormal.Enabled = true;
                else
                    this.uButtonAbnormal.Enabled = false;

                dtHeaderDetail.Dispose();

                // Eng'r 선택에따라 비활성화, 활성화로 변경
                if (this.uOptionAbnormal.Value == null
                    || this.uOptionAbnormal.Value.ToString().Equals("F"))
                    EngrCheck(false);
                else
                    EngrCheck(true);

                if (this.uCheckS5.Enabled == false)
                {
                    this.uGroupBox.Enabled = false;
                }


                if (this.uCheckS5.Checked && !this.uCheckS5.Enabled)
                    m_intStep = 5;
                else if (this.uCheckS4.Checked && !this.uCheckS4.Enabled)
                    m_intStep = 4;
                else if (this.uCheckS3.Checked && !this.uCheckS3.Enabled)
                    m_intStep = 3;
                else if (this.uCheckS2.Checked && !this.uCheckS2.Enabled)
                    m_intStep = 2;
                else if (this.uCheckS1.Checked && !this.uCheckS1.Enabled)
                    m_intStep = 1;
                else
                    m_intStep = 0;

                StepEnabled(m_intStep + 1, true);

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                StepState(m_resSys.GetString("SYS_DEPTCODE"));


                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        /// <summary>
        /// Detail
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAriseProcessCode">발생공정</param>
        /// <param name="strAriseDate">발생일</param>
        /// <param name="strAriseTime">발생시간</param>
        /// <param name="strLang">사용언어</param>
        private void SearchDetail(string strPlantCode, string strAriseProcessCode, string strAriseDate, string strAriseTime, string strLang)
        {
            try
            {

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYieldD), "AbnormalYieldD");
                QRPINS.BL.INSSTS.AbnormalYieldD ProcLowD = new QRPINS.BL.INSSTS.AbnormalYieldD();
                brwChannel.mfCredentials(ProcLowD);
                
                DataTable dtDGrid = ProcLowD.mfReadINSAbnormalYieldD(strPlantCode, strAriseProcessCode, strAriseDate, strAriseTime, strLang);

                this.uGridProcLowD.DataSource = dtDGrid;
                this.uGridProcLowD.DataBind();

                ProcLowD.Dispose();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// FileInfo
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAriseProcessCode">발생공정</param>
        /// <param name="strAriseDate">발생일</param>
        /// <param name="strAriseTime">발생시간</param>
        private void SearchFile(string strPlantCode, string strManageNo, string strAriseProcessCode, string strAriseDate, string strAriseTime, string strLang)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYieldFile), "AbnormalYieldFile");
                QRPINS.BL.INSSTS.AbnormalYieldFile clsAboFile = new QRPINS.BL.INSSTS.AbnormalYieldFile();
                brwChannel.mfCredentials(clsAboFile);

                DataTable dtFileInfo = clsAboFile.mfReadINSAbnormalYieldFile(strPlantCode, strManageNo, strAriseProcessCode, strAriseDate, strAriseTime, strLang);

                clsAboFile.Dispose();

                DataTable dtCopy = new DataTable();
                if (dtFileInfo.Select("Step = 'S0'").Count() > 0)
                {
                    dtCopy = dtFileInfo.Select("Step = 'S0'").CopyToDataTable();

                    this.uTextAttachFile.Text = dtCopy.Rows[0]["FilePath"].ToString();
                }
                if (dtFileInfo.Select("Step = 'S1'").Count() > 0)
                {
                    dtCopy = dtFileInfo.Select("Step = 'S1'").CopyToDataTable();
                    this.uGridS1File.DataSource = dtCopy;

                }
                if (dtFileInfo.Select("Step = 'S2'").Count() > 0)
                {
                    dtCopy = dtFileInfo.Select("Step = 'S2'").CopyToDataTable();
                    this.uGridS2File.DataSource = dtCopy;
                }
                if (dtFileInfo.Select("Step = 'S3'").Count() > 0)
                {
                    dtCopy = dtFileInfo.Select("Step = 'S3'").CopyToDataTable();
                    this.uGridS3File.DataSource = dtCopy;
                }
                if (dtFileInfo.Select("Step = 'S4'").Count() > 0)
                {
                    dtCopy = dtFileInfo.Select("Step = 'S4'").CopyToDataTable();
                    this.uGridS4File.DataSource = dtCopy;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        /// <summary>
        /// 사용자명 조회 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strUserID, string strLang)
        {
            String strRtnUserName = "";
            try
            {
                // 사용자 정보BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                // 사용자정보 조회 매서드 실행
                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, strLang);

                // 정보가 있는경우 저장
                if (dtUser.Rows.Count > 0)
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();

                
                // Reosurce Clear
                clsUser.Dispose();
                dtUser.Dispose();
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 정보 저장
        /// </summary>
        /// <returns></returns>
        private DataTable RtnHeader()
        {
            DataTable dtRtn = new DataTable();

            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                QRPINS.BL.INSSTS.AbnormalYield clsAb = new QRPINS.BL.INSSTS.AbnormalYield();
                brwChannel.mfCredentials(clsAb);

                dtRtn = clsAb.mfDataSet();

                DataRow drRow = dtRtn.NewRow();

                drRow["PlantCode"] = "2210";   //公司代码
                drRow["ManageNo"] = this.uTextManageNo.Text;  //管理编号
                drRow["LotNo"] = this.uTextLotNo.Text; //LotNo
                drRow["LotQty"] = this.uTextLotQty.Text; //LOT QTY
                drRow["LotProcessState"] = this.uTextLotProcessState.Text;  //LOT State
                drRow["ProcessHoldState"] = this.uTextProcessHoldState.Text; //HOLD State
                drRow["Customer"] = this.uTextCustomerCode.Text; //公司名称
                drRow["ProductCode"] = this.uTextCustomerProductCode.Text; //产品CODE
                drRow["ProductName"] = this.uTextCustomerProductSpec.Text; //产品名称
                drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;  //工序CODE
                drRow["AriseProcessName"] = this.uTextAriseProcessName.Text;  //工序名称
                drRow["WorkUserID"] = this.uTextWorkUserID.Text; //接受人员工号
                drRow["EquipCode"] = this.uTextEquipCode.Text;  //设备名称
                drRow["Package"] = this.uTextPackage.Text;   //Package
                drRow["AriseDate"] = this.uTextAriseDate.Text;  //登陆日期年月日
                drRow["AriseTime"] = this.uTextAriseTime.Text;  //登陆日期时分秒
                drRow["AcceptDate"] = this.uTextAcceptDate.Text;  //接受日期年月日
                drRow["AcceptTime"] = this.uTextAcceptTime.Text;  //接受日期时分秒
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                drRow["AriseCause"] = this.uTextAriseCause.Text;   //发生原因
                drRow["MeasureDesc"] = this.uTextMeasureDesc.Text;  //对策事项
                drRow["RegistUserID"] = this.uTextRegistUserID.Text;  //登录者
                drRow["S0DeptCode"] = this.uComboS0Dept.Value == null ? string.Empty : this.uComboS0Dept.Value;
                drRow["ManFlag"] = this.uCheckFir4MManFlag.Checked == true ? "T" : "F";
                drRow["MethodFlag"] = this.uCheckFir4MMethodFlag.Checked == true ? "T" : "F";
                drRow["MaterialFlag"] = this.uCheckFir4MMaterialFlag.Checked == true ? "T" : "F";
                drRow["MachineFlag"] = this.uCheckFir4MMachineFlag.Checked == true ? "T":"F";
                drRow["EnvironmentFlag"] = this.uCheckFir4MEnviroFlag.Checked == true ? "T" : "F";

                drRow["S1UserID"] = this.uTextS1UserID.Text;
                drRow["S1Comment"] = this.uTextS1Comment.Text;
                drRow["S1DeptCode"] = this.uComboS1Dept.Value == null ? string.Empty : this.uComboS1Dept.Value;
                drRow["S1CompleteFlag"] = this.uCheckS1.Checked == true ? "T" : "F";

                drRow["S2Date"] = this.uDateS2Date.Value == null ? string.Empty : this.uDateS2Date.DateTime.Date.ToString("yyyy-MM-dd");
                drRow["S2UserID"] = this.uTextS2UserID.Text;
                drRow["S2Comment"] = this.uTextS2Analysis.Text;
                drRow["S2DeptCode"] = this.uComboS2Dept.Value == null ? string.Empty : this.uComboS2Dept.Value;
                drRow["S2CompleteFlag"] = this.uCheckS2.Checked == true ? "T" : "F";

                drRow["S3Date"] = this.uDateS3Date.Value == null ? string.Empty : this.uDateS3Date.DateTime.Date.ToString("yyyy-MM-dd");
                drRow["S3UserID"] = this.uTextS3UserID.Text;
                drRow["S3Comment"] = this.uTextS3Comment.Text;
                drRow["S3DeptCode"] = this.uComboS3Dept.Value == null ? string.Empty : this.uComboS3Dept.Value;
                drRow["S3CompleteFlag"] = this.uCheckS3.Checked == true ? "T" : "F";

                drRow["S4Date"] = this.uDateS4Date.Value == null ? string.Empty : this.uDateS4Date.DateTime.Date.ToString("yyyy-MM-dd");
                drRow["S4UserID"] = this.uTextS4UserID.Text;
                drRow["S4Comment"] = this.uTextS4Comment.Text;
                drRow["S4CompleteFlag"] = this.uCheckS4.Checked == true ? "T" : "F";

                drRow["S5Date"] = this.uDateS5Date.Value == null ? string.Empty : this.uDateS5Date.DateTime.Date.ToString("yyyy-MM-dd");
                drRow["S5UserID"] = this.uTextS5UserID.Text;
                drRow["S5Comment"] = this.uTextS5Comment.Text;
                drRow["S5CompleteFlag"] = this.uCheckS5.Checked == true ? "T" : "F";

                drRow["ResultFlag"] = this.uOptionOkNg.Value == null ? string.Empty : this.uOptionOkNg.Value;
                // 반려된 Step 정보를 입력하여 저장한경우.
                string strReturnStep = this.uComboReturnStep.Value == null ? string.Empty : this.uComboReturnStep.Value.ToString();
                if (this.uCheckS4.Checked && this.uCheckS4.Enabled)
                {
                    drRow["ReturnFlag"] = "F";
                    drRow["ReturnStep"] = string.Empty;
                }
                else
                {
                    drRow["ReturnFlag"] = this.uCheckReturn.Checked == true ? "T" : "F";
                    drRow["ReturnStep"] = strReturnStep;
                }
                
                drRow["AbnormalFlag"] = this.uOptionAbnormal.Value == null ? "F" : this.uOptionAbnormal.Value;


                dtRtn.Rows.Add(drRow);


                clsAb.Dispose();

                return dtRtn;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 파일정보 저장
        /// </summary>
        /// <returns></returns>
        private DataTable RtnFile()
        {
            DataTable dtRtn = new DataTable();

            try
            {
                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYieldFile), "AbnormalYieldFile");
                QRPINS.BL.INSSTS.AbnormalYieldFile clsFile = new QRPINS.BL.INSSTS.AbnormalYieldFile();
                brwChannel.mfCredentials(clsFile);

                // Columns Set
                dtRtn = clsFile.mfDataSet();

                // clsFile Resource Clear
                clsFile.Dispose();

                DataRow drRow;

                // 파일정보가 없는경우 PASS
                if (!this.uTextAttachFile.Text.Equals(string.Empty))
                {
                    drRow = dtRtn.NewRow();

                    drRow["PlantCode"] = this.uTextPlantCode.Text;                  // 공장
                    drRow["ManageNo"] = this.uTextManageNo.Text;                    // 발생공정
                    drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                    drRow["AriseDate"] = this.uTextAriseDate.Text;                  // 발생일
                    drRow["AriseTime"] = this.uTextAriseTime.Text;                  // 발생시간
                    drRow["Step"] = "S0";                                           // Step
                    drRow["Seq"] = 1;                                               // 순번
                    drRow["FileName"] = "";                                         // 파일명

                    // 파일경로
                    if (this.uTextAttachFile.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextAttachFile.Text);
                        drRow["FilePath"] = this.uTextPlantCode.Text + "-" + this.uTextManageNo.Text + "-" +
                            this.uTextAriseDate.Text + "-" + this.uTextAriseTime.Text.Replace(":", "") + "-" + fileDoc.Name;
                    }
                    else
                        drRow["FilePath"] = this.uTextAttachFile.Text;

                    dtRtn.Rows.Add(drRow);
                }

                // 각 Step별 파일정보를 담은 Grid 저장
                Infragistics.Win.UltraWinGrid.UltraGrid[] uGrid = { this.uGridS1File, this.uGridS2File, this.uGridS3File, this.uGridS4File };

                // Grid Loop
                foreach (Infragistics.Win.UltraWinGrid.UltraGrid G in uGrid)
                {
                    // 정보가 없는경우 PASS
                    if (G.Rows.Count == 0)
                        continue;

                    // Step 저장
                    string strStep = string.Empty;

                    if (G.Name.Contains("1"))
                        strStep = "S1";
                    else if (G.Name.Contains("2"))
                        strStep = "S2";
                    else if (G.Name.Contains("3"))
                        strStep = "S3";
                    else if (G.Name.Contains("4"))
                        strStep = "S4";
                    else
                        continue;

                    // Grid Rows Loop
                    for (int i = 0; i < G.Rows.Count; i++)
                    {
                        drRow = dtRtn.NewRow();

                        drRow["PlantCode"] = this.uTextPlantCode.Text;                  // 공장
                        drRow["ManageNo"] = this.uTextManageNo.Text;                    // 발생공정
                        drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                        drRow["AriseDate"] = this.uTextAriseDate.Text;                  // 발생일
                        drRow["AriseTime"] = this.uTextAriseTime.Text;                  // 발생시간
                        drRow["Step"] = strStep;                                        // Step
                        drRow["Seq"] = G.Rows[i].RowSelectorNumber;                     // 순번
                        drRow["FileName"] = G.Rows[i].GetCellValue("FileName");         // 파일명

                        // 파일경로
                        if (G.Rows[i].GetCellValue("FilePath").ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(G.Rows[i].GetCellValue("FilePath").ToString());
                            drRow["FilePath"] = this.uTextPlantCode.Text + "-" + this.uTextManageNo.Text + "-" +
                                this.uTextAriseDate.Text + "-" + this.uTextAriseTime.Text.Replace(":", "") + "-" + strStep+G.Rows[i].RowSelectorNumber + "-" + fileDoc.Name;
                        }
                        else
                            drRow["FilePath"] = G.Rows[i].GetCellValue("FilePath").ToString();

                        dtRtn.Rows.Add(drRow);
                    }

                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            { dtRtn.Dispose(); }
        }


        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        private void FileUpload()
        {
            try
            {
                
                ArrayList arrFile = new ArrayList();

                if (this.uTextAttachFile.Text.Contains(":\\"))
                {


                    FileInfo fileDoc = new FileInfo(this.uTextAttachFile.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" + this.uTextPlantCode.Text + "-"
                                            + this.uTextManageNo.Text + "-" + this.uTextAriseDate.Text + "-"
                                            + this.uTextAriseTime.Text.Replace(":", "") + "-" + fileDoc.Name;

                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);

                    File.Copy(this.uTextAttachFile.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                Infragistics.Win.UltraWinGrid.UltraGrid[] uGrd = { this.uGridS1File, this.uGridS2File, this.uGridS3File, this.uGridS4File };

                foreach (Infragistics.Win.UltraWinGrid.UltraGrid Ugrid in uGrd)
                {
                    if (Ugrid.Rows.Count == 0)
                        continue;
                    string[] strStep = Ugrid.Name.Split('d');

                    for (int i = 0; i < Ugrid.Rows.Count; i++)
                    {
                        if (Ugrid.Rows[i].Hidden)
                            continue;
                            
                        if (Ugrid.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            // 파일이름변경(공장코드+발생공정+발생일+발생시간+Step+순번)하여 복사하기//
                            FileInfo fileDoc = new FileInfo(Ugrid.Rows[i].Cells["FilePath"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" + this.uTextPlantCode.Text + "-"
                                                    + this.uTextManageNo.Text + "-" + this.uTextAriseDate.Text + "-"
                                                    + this.uTextAriseTime.Text.Replace(":", "") + "-" 
                                                    + strStep[1].Substring(0,2) + Ugrid.Rows[i].RowSelectorNumber+ "-" + fileDoc.Name;

                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            //변경한 화일이름으로 복사하기
                            File.Copy(Ugrid.Rows[i].Cells["FilePath"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                        
                    }
                }

                if (arrFile.Count == 0)
                    return;


                //File Upload
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();

                //화일서버 연결정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                //설비이미지 저장경로정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                fileAtt.ShowDialog();
                
                
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// MES I/F
        /// </summary>
        private void Send_MESInterFace()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                bool bolSaveCheck = true;
                TransErrRtn ErrRtn = new TransErrRtn();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                
                // 정보상 Lot상태가 Hlod 상태이거나 최종완료 처리 인경우
                if (!this.uCheckMESReleaseTFlag.Checked 
                    ||(this.uCheckS5.Checked && this.uCheckS5.Enabled))
                {

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                    QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                    brwChannel.mfCredentials(clsMES);

                    DataTable dtLotInfo = clsMES.mfRead_LOT_INFO_REQ(this.uTextPlantCode.Text, this.uTextLotNo.Text.ToUpper().Trim());

                    if (dtLotInfo.Rows.Count > 0)
                    {
                        if (dtLotInfo.Rows[0]["returncode"].ToString().Trim().Equals("0"))
                        {
                            #region 결과전송 MES I/F

                            //if (!this.uCheckMESFlag.Checked && this.uCheckScrapFlag.Checked)
                            //{
                            //    #region 데이터 설정
                            //    DataRow drRow;
                            //    // 데이터 테이블 컬럼설정
                            //    DataTable dtMESInfo = new DataTable();
                            //    dtMESInfo.Columns.Add("PlantCode", typeof(string));
                            //    dtMESInfo.Columns.Add("AriseProcessCode", typeof(string));
                            //    dtMESInfo.Columns.Add("AriseDate", typeof(string));
                            //    dtMESInfo.Columns.Add("AriseTime", typeof(string));
                            //    dtMESInfo.Columns.Add("FormName", typeof(string));
                            //    dtMESInfo.Columns.Add("WorkUserID", typeof(string)); //UserID
                            //    dtMESInfo.Columns.Add("EquipCode", typeof(string));     //EquipCode
                            //    dtMESInfo.Columns.Add("ScrapFlag", typeof(string));     //SCRAPFLAG
                            //    dtMESInfo.Columns.Add("LotNo", typeof(string));     // LotNo

                            //    drRow = dtMESInfo.NewRow();
                            //    drRow["PlantCode"] = this.uTextPlantCode.Text;
                            //    drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                            //    drRow["AriseDate"] = this.uTextAriseDate.Text;
                            //    drRow["AriseTime"] = this.uTextAriseTime.Text;
                            //    drRow["FormName"] = this.Name;
                            //    drRow["WorkUserID"] = this.uTextWorkUserID.Text;
                            //    drRow["EquipCode"] = this.uTextEquipCode.Text;

                            //    // ScrapFlag 2012-11-07 Scrap 처리불가
                            //    drRow["ScrapFlag"] = "N";
                            //    drRow["LotNo"] = this.uTextLotNo.Text;

                            //    dtMESInfo.Rows.Add(drRow);

                            //    // dtLoss(불량코드SCRAPCODE, 불량수량SCRAPQTY)
                            //    DataTable dtScrapList = new DataTable();
                            //    dtScrapList.Columns.Add("SCRAPCODE", typeof(string));
                            //    dtScrapList.Columns.Add("SCRAPQTY", typeof(string));

                            //    for (int i = 0; i < this.uGridProcLowD.Rows.Count; i++)
                            //    {
                            //        this.uGridProcLowD.ActiveCell = this.uGridProcLowD.Rows[0].Cells[0];
                            //        {
                            //            drRow = dtScrapList.NewRow();
                            //            drRow["SCRAPCODE"] = this.uGridProcLowD.Rows[i].Cells["LossCode"].Value.ToString();
                            //            drRow["SCRAPQTY"] = this.uGridProcLowD.Rows[i].Cells["LossQty"].Value.ToString();
                            //            dtScrapList.Rows.Add(drRow);
                            //        }
                            //    }
                            //    #endregion

                            //    // BL 연결
                            //    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                            //    QRPINS.BL.INSSTS.AbnormalYield clsProcLow = new QRPINS.BL.INSSTS.AbnormalYield();
                            //    brwChannel.mfCredentials(clsProcLow);

                            //    string strErrRtn = clsProcLow.mfSaveINSProcAbnormalYield_MES_Result(dtMESInfo, dtScrapList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            //    // 결과검사
                            //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            //    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                            //    {
                            //        bolSaveCheck = false;

                            //        if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                            //        {
                            //            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                        "M001135", "M000095", "M000952", Infragistics.Win.HAlign.Right);
                            //        }
                            //        else
                            //        {
                            //            string strLang = m_resSys.GetString("SYS_LANG");
                            //            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            //                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //                , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                            //                , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                            //        }
                            //        return;
                            //    }
                            //    else
                            //    {

                            //    }
                            //}

                            #endregion

                            #region Lot Release MES I/F
                            
                            if (!this.uCheckMESReleaseTFlag.Checked && this.uCheckS5.Checked)// && !this.uCheckScrapFlag.Checked)
                            {
                                #region 데이터 설정
                                DataRow drRow;
                                // 데이터 테이블 컬럼설정
                                DataTable dtMESInfo = new DataTable();
                                dtMESInfo.Columns.Add("PlantCode", typeof(string));
                                dtMESInfo.Columns.Add("AriseProcessCode", typeof(string));
                                dtMESInfo.Columns.Add("AriseDate", typeof(string));
                                dtMESInfo.Columns.Add("AriseTime", typeof(string));
                                dtMESInfo.Columns.Add("FormName", typeof(string));
                                dtMESInfo.Columns.Add("WorkUserID", typeof(string));
                                dtMESInfo.Columns.Add("Comment", typeof(string));

                                drRow = dtMESInfo.NewRow();
                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                                drRow["AriseDate"] = this.uTextAriseDate.Text;
                                drRow["AriseTime"] = this.uTextAriseTime.Text;
                                drRow["FormName"] = this.Name;
                                drRow["WorkUserID"] = this.uTextS5UserID.Text; // 완료자 ID
                                drRow["Comment"] = this.uTextS5Comment.Text;
                                dtMESInfo.Rows.Add(drRow);

                                // LotList
                                DataTable dtLotList = new DataTable();
                                dtLotList.Columns.Add("LOTID", typeof(string));
                                dtLotList.Columns.Add("HOLDCODE", typeof(string));

                                drRow = dtLotList.NewRow();
                                drRow["LOTID"] = this.uTextLotNo.Text;
                                //drRow["HOLDCODE"] = "HL";
                                drRow["HOLDCODE"] = "GL04";
                                dtLotList.Rows.Add(drRow);
                                #endregion

                                // BL 연결
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                                QRPINS.BL.INSSTS.AbnormalYield clsProcLow = new QRPINS.BL.INSSTS.AbnormalYield();
                                brwChannel.mfCredentials(clsProcLow);

                                string strErrRtn = clsProcLow.mfSaveINSProcAbnormalYield_MES_MESRelease(dtMESInfo, dtLotList, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                                {
                                    bolSaveCheck = false;

                                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000095", "M000948", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                    }
                                    return;
                                }
                                else
                                {

                                }
                            }
                            #endregion

                        }
                        else
                        {
                            if (dtLotInfo.Rows[0]["returnmessage"].ToString().Trim().Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M000066", "M000059", "M000091", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M000066", strLang), msg.GetMessge_Text("M000059", strLang)
                                    , dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            }
                            return;
                        }
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M000066", "M000059", "M000091", Infragistics.Win.HAlign.Right);
                        return;
                    }

                }

                if (bolSaveCheck)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "M001022", "M001023", "M000930", Infragistics.Win.HAlign.Right);

                    // 리스트 갱신
                    mfSearch();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// MES I/F
        /// </summary>
        private bool Send_MESInterFace(string strReleaseFlag)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                bool bolSaveCheck = true;
                TransErrRtn ErrRtn = new TransErrRtn();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 정보상 Lot상태가 Hlod 상태이거나 최종완료 처리 인경우
                if (!strReleaseFlag.Equals(String.Empty))
                {

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                    QRPINS.BL.INSPRC.ProcMESInterface clsMES = new QRPINS.BL.INSPRC.ProcMESInterface();
                    brwChannel.mfCredentials(clsMES);

                    DataTable dtLotInfo = clsMES.mfRead_LOT_INFO_REQ(this.uTextPlantCode.Text, this.uTextLotNo.Text.ToUpper().Trim());

                    if (dtLotInfo.Rows.Count > 0)
                    {
                        if (dtLotInfo.Rows[0]["returncode"].ToString().Trim().Equals("0"))
                        {
                            #region Lot Release MES I/F

                            if (dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString().Equals("OnHold"))
                            {

                                #region 데이터 설정


                                // BL 연결
                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                                QRPINS.BL.INSSTS.AbnormalYield clsProcLow = new QRPINS.BL.INSSTS.AbnormalYield();
                                brwChannel.mfCredentials(clsProcLow);


                                DataRow drRow;
                                // 데이터 테이블 컬럼설정
                                DataTable dtMESInfo = clsProcLow.RtnMESDataColumns();


                                drRow = dtMESInfo.NewRow();
                                drRow["PlantCode"] = this.uTextPlantCode.Text;
                                drRow["ManageNo"] = this.uTextManageNo.Text;
                                drRow["FORMNAME"] = this.Name;
                                drRow["LOTID"] = this.uTextLotNo.Text;
                                drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                                drRow["RELEASEFLAG"] = strReleaseFlag; // 완료자 ID
                                if (strReleaseFlag.Equals("Y"))
                                {
                                    drRow["USERID"] = this.uTextS5UserID.Text;
                                    drRow["COMMENT"] = this.uTextS5Comment.Text;
                                }
                                else
                                {
                                    drRow["USERID"] = m_resSys.GetString("SYS_USERID");
                                    drRow["COMMENT"] = this.uTextEtcDesc.Text;
                                }

                                drRow["USERIP"] = m_resSys.GetString("SYS_USERIP");

                                dtMESInfo.Rows.Add(drRow);

                                #endregion

                                string strErrRtn = clsProcLow.mfSaveINSProcAbnormalYield_LOT_TESTHOLD4QC_REQ(dtMESInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.InterfaceResultCode.Equals("0"))
                                {
                                    bolSaveCheck = false;

                                    if (ErrRtn.InterfaceResultMessage.Equals(string.Empty))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000095", "M000948", Infragistics.Win.HAlign.Right);
                                    }
                                    else
                                    {
                                        string strLang = m_resSys.GetString("SYS_LANG");
                                        Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000095", strLang)
                                            , ErrRtn.InterfaceResultMessage, Infragistics.Win.HAlign.Right);
                                    }
                                    return false;
                                }
                                else
                                {

                                }

                            }
                            #endregion

                        }
                        else
                        {
                            if (dtLotInfo.Rows[0]["returnmessage"].ToString().Trim().Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M000066", "M000059", "M000091", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M000066", strLang), msg.GetMessge_Text("M000059", strLang)
                                    , dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                            }
                            return false;
                        }
                    }
                    else
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M000066", "M000059", "M000091", Infragistics.Win.HAlign.Right);
                        return false;
                    }

                }

                if (bolSaveCheck)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "M001022", "M001023", "M000930", Infragistics.Win.HAlign.Right);

                    // 리스트 갱신
                    if(strReleaseFlag.Equals("Y"))
                        mfSearch();
                }

                return bolSaveCheck;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 필수입력사항체크
        /// </summary>
        /// <returns></returns>
        private bool CheckInput()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                string strStepFlag = this.uOptionAbnormal.Value == null ? string.Empty : this.uOptionAbnormal.Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                    , "M000397"
                                                    , Infragistics.Win.HAlign.Right);
                    return false;
                }
                else if (this.uTextPlantCode.Text.Equals(string.Empty) || this.uTextAriseProcessCode.Text.Equals(string.Empty)
                        || this.uTextAriseDate.Text.Equals(string.Empty) || this.uTextAriseTime.Text.Equals(string.Empty))
                {
                    // 기본키 입력확인
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000882", "M001230"
                                                    , "M000397"
                                                    , Infragistics.Win.HAlign.Right);
                    return false;
                }
                else if (this.uCheckMESReleaseTFlag.Checked 
                            && m_intStep == 5) //(this.uCheckMESFlag.Checked && )
                {
                    // 최종완료가 체크되고 MES I/F 성공한 정보는 수정할 수 없습니다.
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000825", "M001029"
                                                    , "M001540"
                                                    , Infragistics.Win.HAlign.Right);
                    return false;
                }
                else if (m_intStep == 4
                        && (strStepFlag.Equals("T")))
                {
                    if (!this.uDateS5Date.Enabled)
                    {
                        // 진행부서에 등록 된 부서가 아닌경우 저장하실 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001547", strLang)
                                                       , "( Step5 )" + msg.GetMessge_Text("M001554", strLang)
                                                       , Infragistics.Win.HAlign.Right);

                        return false;
                    }

                    // Step5
                    if (this.uTextS5UserID.Text.Equals(string.Empty)
                                      || this.uTextS5UserName.Text.Equals(string.Empty))
                    {
                        // 진행자 ID를 입력해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001549"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uTextS5UserID.Focus();
                        return false;
                    }
                    if (this.uDateS5Date.Value == null || this.uDateS5Date.Value.ToString().Equals(string.Empty))
                    {
                        // 진행일을 선택해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001550"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uDateS5Date.DropDown();
                        return false;
                    }
                    if (this.uOptionOkNg.Value == null || this.uOptionOkNg.Value.ToString().Equals(string.Empty))
                    {
                        // 합부판정을 선택해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001246"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uOptionOkNg.Focus();
                        return false;
                    }

                    if (this.uCheckS5.Checked && this.uCheckS5.Enabled)
                    {
                        // 작성완료 체크후 저장시 이후 수정할 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M000984", "M000982"
                                                       , Infragistics.Win.HAlign.Right);
                    }
                    
                }
                else if (m_intStep == 3
                        && (strStepFlag.Equals("T")))
                {
                    if (!this.uTabStep.Tabs["S4"].Enabled)
                    {
                        // 진행부서에 등록 된 부서가 아닌경우 저장하실 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001547", strLang)
                                                       , "( Step4 )" + msg.GetMessge_Text("M001553", strLang)
                                                       , Infragistics.Win.HAlign.Right);

                        return false;
                    }

                    if (!this.uTabStep.Tabs["S4"].Selected)
                        this.uTabStep.Tabs["S4"].Selected = true;

                    // Step4
                    if (this.uTextS4UserID.Text.Equals(string.Empty)
                        || this.uTextS4UserName.Text.Equals(string.Empty))
                    {
                        // 진행자 ID를 입력해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001549"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uTextS4UserID.Focus();
                        return false;
                    }
                    if (this.uDateS4Date.Value == null || this.uDateS4Date.Value.ToString().Equals(string.Empty))
                    {
                        // 진행일을 선택해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001550"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uDateS4Date.DropDown();
                        return false;
                    }
                    if (this.uGridS4File.Rows.Count > 0)
                    {
                        this.uGridS4File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstCellInGrid);
                        for (int i = 0; i < this.uGridS4File.Rows.Count; i++ )
                        {
                            if (this.uGridS4File.Rows[i].Hidden)
                                continue;

                            if (this.uGridS4File.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty))
                            {

                                // 첨부파일 제목을 입력해주세요.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                               , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                               , "M001264", "M001231", "M001140"
                                                               , Infragistics.Win.HAlign.Right);
                                this.uGridS4File.ActiveCell = this.uGridS4File.Rows[i].Cells["FileName"];
                                this.uGridS4File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return false;
                            }

                        }
                    }
                }
                else if (m_intStep == 2
                        && (strStepFlag.Equals("T")))
                {
                    if (!this.uTabStep.Tabs["S3"].Enabled)
                    {
                        // 진행부서에 등록 된 부서가 아닌경우 저장하실 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001547", strLang)
                                                       , "( Step3 )" + msg.GetMessge_Text("M001553", strLang)
                                                       , Infragistics.Win.HAlign.Right);

                        return false;
                    }

                    if (!this.uTabStep.Tabs["S3"].Selected)
                        this.uTabStep.Tabs["S3"].Selected = true;

                    // Step3
                    if (this.uTextS3UserID.Text.Equals(string.Empty)
                    || this.uTextS3UserName.Text.Equals(string.Empty))
                    {
                        // 진행자 ID를 입력해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001549"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uTextS3UserID.Focus();
                        return false;
                    }
                    if (this.uDateS3Date.Value == null || this.uDateS3Date.Value.ToString().Equals(string.Empty))
                    {
                        // 진행일을 선택해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001550"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uDateS3Date.DropDown();
                        return false;
                    }
                    if (this.uComboS3Dept.Value == null || this.uComboS3Dept.Value.ToString().Equals(string.Empty))
                    {
                        // 진행부서를 선택해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001551"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uComboS3Dept.DropDown();
                        return false;
                    }
                    if (this.uGridS3File.Rows.Count > 0)
                    {
                        this.uGridS3File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstCellInGrid);
                        for (int i = 0; i < this.uGridS3File.Rows.Count; i++)
                        {
                            if (this.uGridS3File.Rows[i].Hidden)
                                continue;

                            if (this.uGridS3File.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty))
                            {

                                // 첨부파일 제목을 입력해주세요.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                               , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                               , "M001264", "M001231", "M001140"
                                                               , Infragistics.Win.HAlign.Right);
                                this.uGridS3File.ActiveCell = this.uGridS3File.Rows[i].Cells["FileName"];
                                this.uGridS3File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return false;
                            }

                        }
                    }
                }
                else if (m_intStep == 1
                        && (strStepFlag.Equals("T")))
                {
                    if (!this.uTabStep.Tabs["S2"].Enabled)
                    {
                        // 진행부서에 등록 된 부서가 아닌경우 저장하실 수 없습니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001547",strLang)
                                                       , "( Step2 )"+msg.GetMessge_Text("M001553",strLang)
                                                       , Infragistics.Win.HAlign.Right);

                        return false;
                    }
                    if (!this.uTabStep.Tabs["S2"].Selected)
                        this.uTabStep.Tabs["S2"].Selected = true;

                    // Step2
                    if (this.uTextS2UserID.Text.Equals(string.Empty)
                    || this.uTextS2UserName.Text.Equals(string.Empty))
                    {
                        // 진행자 ID를 입력해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001549"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uTextS2UserID.Focus();
                        return false;
                    }
                    if (this.uDateS2Date.Value == null || this.uDateS2Date.Value.ToString().Equals(string.Empty))
                    {
                        // 진행일을 선택해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001550"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uDateS2Date.DropDown();
                        return false;
                    }
                    if (this.uComboS2Dept.Value == null || this.uComboS2Dept.Value.ToString().Equals(string.Empty))
                    {
                        // 진행부서를 선택해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001551"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uComboS2Dept.DropDown();
                        return false;
                    }
                    if (this.uGridS2File.Rows.Count > 0)
                    {
                        this.uGridS2File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstCellInGrid);
                        for (int i = 0; i < this.uGridS2File.Rows.Count; i++)
                        {
                            if (this.uGridS2File.Rows[i].Hidden)
                                continue;

                            if (this.uGridS2File.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty))
                            {

                                // 첨부파일 제목을 입력해주세요.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                               , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                               , "M001264", "M001231", "M001140"
                                                               , Infragistics.Win.HAlign.Right);
                                this.uGridS2File.ActiveCell = this.uGridS2File.Rows[i].Cells["FileName"];
                                this.uGridS2File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return false;
                            }

                        }
                    }
                }
                else if (m_intStep == 0
                    && (strStepFlag.Equals("T")))
                {
                    if (!this.uTabStep.Tabs["S1"].Selected)
                        this.uTabStep.Tabs["S1"].Selected = true;

                    // Step1
                    if (this.uTextS1UserID.Text.Equals(string.Empty)
                    || this.uTextS1UserName.Text.Equals(string.Empty))
                    {
                        // 진행자 ID를 입력해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001549"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uTextS1UserID.Focus();
                        return false;
                    }
                    if (this.uComboS1Dept.Value == null || this.uComboS1Dept.Value.ToString().Equals(string.Empty))
                    {
                        // 진행부서를 선택해주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M001551"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uComboS1Dept.DropDown();
                        return false;
                    }
                    if (this.uGridS1File.Rows.Count > 0)
                    {
                        this.uGridS1File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.FirstCellInGrid);
                        for (int i = 0; i < this.uGridS1File.Rows.Count; i++)
                        {
                            if (this.uGridS1File.Rows[i].Hidden)
                                continue;

                            if (this.uGridS1File.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty))
                            {

                                // 첨부파일 제목을 입력해주세요.
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                               , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                               , "M001264", "M001231", "M001140"
                                                               , Infragistics.Win.HAlign.Right);
                                this.uGridS1File.ActiveCell = this.uGridS1File.Rows[i].Cells["FileName"];
                                this.uGridS1File.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return false;
                            }

                        }
                    }
                }
                else
                {
                    if (this.uTextRegistUserID.Text.Equals(string.Empty)
                                       || this.uTextRegistUserName.Text.Equals(string.Empty))
                    {
                        // 등록자를 입력해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                       , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , "M001264", "M001231", "M000386"
                                                       , Infragistics.Win.HAlign.Right);
                        this.uTextRegistUserID.Focus();
                        return false;
                    }
                }

                return true;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Eng'rCheck Abnormal 사용여부에 따라 활성화, 비활성화
        /// </summary>
        /// <param name="bolEnabled"></param>
        private void EngrCheck(bool bolEnabled)
        {
            if (bolEnabled)
            {
                this.uButtonAbnormal.Text = "Step Cancel";
                this.uButtonAbnormal.Appearance.Image = Properties.Resources.btn_Cancel;
                this.uGroupBox.Enabled = !bolEnabled;
            }
            else
            {
                this.uButtonAbnormal.Text = "Step OK";
                this.uButtonAbnormal.Appearance.Image = Properties.Resources.btn_OK;
                this.uGroupBox.Enabled = !bolEnabled;
            }

            this.uTabStep.Enabled = bolEnabled;
            this.uTextAriseCause.Enabled = bolEnabled == true ? false : true;
            this.uTextMeasureDesc.Enabled = bolEnabled == true ? false : true;
            this.uTextRegistUserID.ReadOnly = bolEnabled;
            

        }

        /// <summary>
        /// Step별 ReadOnly처리
        /// </summary>
        /// <param name="strStep">Step</param>
        /// <param name="bolReadOnly">ReadOnly</param>
        private void StepReadOnly(int intStep, bool bolReadOnly)
        {

            if (intStep == 5)
            {
                // Step5
                this.uTextS5UserID.ReadOnly = bolReadOnly;
                this.uTextS5Comment.ReadOnly = bolReadOnly;
                this.uDateS5Date.ReadOnly = bolReadOnly;

                this.uOptionOkNg.Enabled = bolReadOnly == true ? false : true;
                //this.uCheckS5.Enabled = bolReadOnly == true ? false : true;
                this.uComboReturnStep.ReadOnly = bolReadOnly;
                
            }
            else if (intStep == 4)
            {
                // Step4
                this.uTextS4UserID.ReadOnly = bolReadOnly;
                this.uTextS4Comment.ReadOnly = bolReadOnly;
                this.uDateS4Date.ReadOnly = bolReadOnly;
                if (bolReadOnly)
                {
                    this.uGridS4File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    for (int i = 0; i < this.uGridS4File.Rows.Count; i++)
                        this.uGridS4File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;

                }
                else
                {
                    this.uGridS4File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    for (int i = 0; i < this.uGridS4File.Rows.Count; i++)
                        this.uGridS4File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            else if (intStep == 3)
            {
                // Step3
                this.uTextS3UserID.ReadOnly = bolReadOnly;
                this.uTextS3Comment.ReadOnly = bolReadOnly;
                this.uDateS3Date.ReadOnly = bolReadOnly;
                this.uComboS3Dept.ReadOnly = bolReadOnly;

                if (bolReadOnly)
                {
                    this.uGridS3File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    for (int i = 0; i < this.uGridS3File.Rows.Count; i++)
                        this.uGridS3File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;

                }
                else
                {
                    this.uGridS3File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    for (int i = 0; i < this.uGridS3File.Rows.Count; i++)
                        this.uGridS3File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            else if (intStep == 2)
            {
                // Step2
                this.uTextS2UserID.ReadOnly = bolReadOnly;
                this.uTextS2Analysis.ReadOnly = bolReadOnly;
                this.uDateS2Date.ReadOnly = bolReadOnly;
                this.uComboS2Dept.ReadOnly = bolReadOnly;

                if (bolReadOnly)
                {
                    this.uGridS2File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    for (int i = 0; i < this.uGridS2File.Rows.Count; i++)
                        this.uGridS2File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;

                }
                else
                {
                    this.uGridS2File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    for (int i = 0; i < this.uGridS2File.Rows.Count; i++)
                        this.uGridS2File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            else if (intStep == 1)
            {
                // Step1
                this.uTextS1UserID.ReadOnly = bolReadOnly;
                this.uTextS1Comment.ReadOnly = bolReadOnly;
                this.uComboS1Dept.ReadOnly = bolReadOnly;

                if (bolReadOnly)
                {
                    this.uGridS1File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    for (int i = 0; i < this.uGridS1File.Rows.Count; i++)
                        this.uGridS1File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;

                }
                else
                {
                    this.uGridS1File.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    for (int i = 0; i < this.uGridS1File.Rows.Count; i++)
                        this.uGridS1File.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }

        }

        /// <summary>
        /// Step의 해당 Tab Enabled 처리 (TabIndex 활성여부)
        /// </summary>
        /// <param name="intStep"></param>
        /// <param name="bolEnabled"></param>
        private void StepEnabled(int intStep, bool bolEnabled)
        {
            if(intStep < 5)
                this.uTabStep.Tabs["S" + intStep.ToString()].Enabled = bolEnabled;
        }

        /// <summary>
        /// 반려처리
        /// </summary>
        private void Return()
        {
            try
            {
                // 반려 Step상태가 비활성화인경우 Return
                if (!this.uTabStep.Enabled)
                    return;

                // Step5 입력불가상태거나 최종완료된 정보인경우 Return
                if (!this.uTextS5UserID.Enabled || !this.uCheckS5.Enabled)
                    return;

                // System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                // 반려 Step저장
                string strReturnStep = this.uComboReturnStep.Value == null ? string.Empty : this.uComboReturnStep.Value.ToString();

                // 반려 Step 콤보가 공백인경우 Return
                if (strReturnStep.Equals(string.Empty))
                {

                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001548"
                                                    , Infragistics.Win.HAlign.Right);
                    this.uComboReturnStep.DropDown();

                    return;
                }
                if (this.uOptionAbnormal.Value != null
                    && this.uOptionAbnormal.Value.ToString().Equals("T"))
                {
                    if (!this.uTabStep.Tabs["S4"].Enabled
                        || !this.uCheckS4.Checked
                        || this.uCheckS4.Enabled)
                    {

                        // Step4 진행이 필요합니다.
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M001547", "M001546",
                                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
                // Step5 날짜가 선택되지 않았을경우 Return
                if (this.uDateS5Date.Value == null || this.uDateS5Date.Value.ToString().Equals(string.Empty))
                {

                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001550"
                                                    , Infragistics.Win.HAlign.Right);
                    this.uDateS5Date.DropDown();
                    return;
                }

                // S5 아이디가 공백인경우 Return
                if (this.uTextS5UserID.Text.Equals(string.Empty) || this.uTextS5UserName.Text.Equals(string.Empty))
                {

                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001549"
                                                    , Infragistics.Win.HAlign.Right);
                    this.uTextS5UserID.Focus();
                    return;
                }

                string strLang = m_resSys.GetString("SYS_LANG");

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M000420", "M000419",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                    return;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                QRPINS.BL.INSSTS.AbnormalYield clsAbo = new QRPINS.BL.INSSTS.AbnormalYield();
                brwChannel.mfCredentials(clsAbo);

                string strPlantCode = this.uTextPlantCode.Text;
                string strAriseProcessCode = this.uTextAriseProcessCode.Text;
                string strAriseDate = this.uTextAriseDate.Text;
                string strAriseTime = this.uTextAriseTime.Text;

                string strS5Date = this.uDateS5Date.Value.ToString();
                string strS5UserID = this.uTextS5UserID.Text;
                string strS5Comment = this.uTextS5Comment.Text;
                string strResultFlag = "NG";

                TransErrRtn ErrRtn = new TransErrRtn();
                string strErrRtn = clsAbo.mfSaveINSProcAbnormalYield_Return(strPlantCode, strAriseProcessCode, strAriseDate, strAriseTime,
                                                                            "T", strReturnStep, 
                                                                            strResultFlag,strS5Date,strS5UserID,strS5Comment,
                                                                            m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001037", "M000921",
                                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
                else
                {
                    string strMsg = "";

                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = msg.GetMessge_Text("M000920", strLang);
                    else
                        strMsg = ErrRtn.ErrMessage;
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                  , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                  , msg.GetMessge_Text("M001135", strLang)
                                                  , msg.GetMessge_Text("M001037", strLang), strMsg
                                                  , Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
       
        /// <summary>
        /// Step 상태
        /// </summary>
        private void StepState(string strUserDeptCode)
        {
            int intStep = 0;
            string strNextDept = string.Empty;
            if (this.uCheckS5.Checked && !this.uCheckS5.Enabled)
                intStep = 5;
            else if (this.uCheckS4.Checked && !this.uCheckS4.Enabled)
                intStep = 4;
            else if (this.uCheckS3.Checked && !this.uCheckS3.Enabled)
            {
                intStep = 3;
                strNextDept = this.uComboS3Dept.Value == null ? string.Empty : this.uComboS3Dept.Value.ToString();
            }
            else if (this.uCheckS2.Checked && !this.uCheckS2.Enabled)
            {
                intStep = 2;
                strNextDept = this.uComboS2Dept.Value == null ? string.Empty : this.uComboS2Dept.Value.ToString();
            }
            else if (this.uCheckS1.Checked && !this.uCheckS1.Enabled)
            {
                intStep = 1;
                strNextDept = this.uComboS1Dept.Value == null ? string.Empty : this.uComboS1Dept.Value.ToString();
            }
            ///////////////////  수정 불가 처리 ///////////////////////
            for (int i = 0; i <= intStep; i++)
            {
                if(i == 0 && intStep != 5)
                    StepReadOnly(5, true);
                if (i == 0)
                    continue;

                StepReadOnly(i, true);
            }

            ////////////////////////////////////////////////////////////////////
            /////// 수정 가능 처리 (Step -> 수정가능 불가 처리)/////////////////
            if (intStep == 0)
            {
                StepReadOnly(1, false);
                for (int i = 1; i <= 4; i++)
                    if (i == 1)
                        StepEnabled(i, true);
                    else
                        StepEnabled(i, false);
            }
            else
            {
                // 다음 Step의 지정된 부서가 아닌경우 수정불가.
                bool bolChk = false;

                if (!strNextDept.Equals(string.Empty)
                    && strNextDept.Equals(strUserDeptCode))
                {
                    bolChk = true;
                    SetStepUserID(intStep + 1);
                }
                else if (strNextDept.Equals(string.Empty))
                    bolChk = true;

                StepReadOnly(intStep + 1, bolChk == true ? false : true);

                if (m_intStep == 4)
                    for (int i = 1; i <= 4; i++)
                        StepEnabled(i, true); // 
                else
                    for (int i = 1; i <= 4; i++)
                        if (intStep + 1 < i)
                            StepEnabled(i, false);
                        else if(intStep + 1 == i)
                            StepEnabled(i, bolChk);
                        else
                            StepEnabled(i, true);
            }
            ///////////////////////////////////////////////////////
            
        }

        /// <summary>
        /// 인자로 들어 문자에 특수 문자가 존재 하는지 여부를 검사 한다.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public bool CheckingSpecialText(string txt)
        {
            bool temp = false;
            try
            {
                //C:\Documents and Settings\All Users\Documents\My Pictures\그림 샘플\겨울.jpg
                string str = @"[#+]";
                System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
                temp = rex.IsMatch(txt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
            return temp;
        }

        /// <summary>
        /// 각 Step 사용자 설정 (진행부서가 동일시)
        /// </summary>
        /// <param name="intStep"></param>
        private void SetStepUserID(int intStep)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // Step5
            if (intStep == 5 && this.uTextS5UserID.Text.Equals(string.Empty))
            {
                this.uTextS5UserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextS5UserName.Text = m_resSys.GetString("SYS_USERNAME");
            }
            else if (intStep == 4 && this.uTextS4UserID.Text.Equals(string.Empty))
            {
                // Step4
                this.uTextS4UserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextS4UserName.Text = m_resSys.GetString("SYS_USERNAME");
                
            }
            else if (intStep == 3 && this.uTextS3UserID.Text.Equals(string.Empty))
            {
                // Step3
                this.uTextS3UserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextS3UserName.Text = m_resSys.GetString("SYS_USERNAME");
                
            }
            else if (intStep == 2 && this.uTextS2UserID.Text.Equals(string.Empty))
            {
                // Step2
                this.uTextS2UserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextS2UserName.Text = m_resSys.GetString("SYS_USERNAME");
                
            }
            else if (intStep == 1 && this.uTextS1UserID.Text.Equals(string.Empty))
            {
                // Step1
                this.uTextS1UserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextS1UserName.Text = m_resSys.GetString("SYS_USERNAME");
            }

            m_resSys.Close();
        }

        /// <summary>
        /// 获取LOT相关信息
        /// </summary>
        private void Rtn_MES_LotInfo()
        {
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 设置各内容为空，以便于接受MES数据                                                       
                this.uTextManageNo.Clear();
                this.uTextCustomerCode.Clear();
                this.uTextCustomerName.Clear();
                this.uTextAriseProcessCode.Clear();
                this.uTextAriseProcessName.Clear();
                this.uTextCustomerProductCode.Clear();
                this.uTextEtcDesc.Value = string.Empty;
                this.uTextWorkUserID.Clear();
                this.uTextWorkUserName.Clear();
                this.uTextPackage.Clear();
                this.uTextEquipCode.Clear();
                this.uTextEquipName.Clear();
                this.uTextCustomerProductSpec.Clear();
                this.uTextAriseDate.Clear();
                this.uTextAriseTime.Clear();
                this.uTextLotProcessState.Clear();
                this.uTextProcessHoldState.Clear();
                this.uTextLotQty.Clear();
                this.uTextAcceptDate.Clear();
                this.uTextAcceptTime.Clear();

                // Connect BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcMESInterface), "ProcMESInterface");
                QRPINS.BL.INSPRC.ProcMESInterface clsMes = new QRPINS.BL.INSPRC.ProcMESInterface();
                brwChannel.mfCredentials(clsMes);

                string strLotNo = this.uTextLotNo.Text.ToUpper().Trim();
                DataTable dtLotInfo = clsMes.mfRead_LOT_INFO_REQ(this.uTextPlantCode.Text, strLotNo);

                // 데이터가 있는경우
                if (dtLotInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {

                    this.uTextLotNo.ReadOnly = true;
                    this.uTextLotNo.Appearance.BackColor = Color.Gainsboro;

                    this.uTextAriseProcessCode.Text = dtLotInfo.Rows[0]["OPERID"].ToString();
                    this.uTextAriseProcessName.Text = dtLotInfo.Rows[0]["OPERDESC"].ToString();
                    this.uTextCustomerProductCode.Text = dtLotInfo.Rows[0]["PRODUCTSPECNAME"].ToString();
                    this.uTextWorkUserID.Text = dtLotInfo.Rows[0]["LASTEVENTUSER"].ToString();
                    this.uTextPackage.Text = dtLotInfo.Rows[0]["PACKAGE"].ToString();
                    this.uTextEquipCode.Text = dtLotInfo.Rows[0]["EQPID"].ToString();
                    this.uTextEquipName.Text = dtLotInfo.Rows[0]["EQPDESC"].ToString();
                    this.uTextCustomerProductSpec.Text = dtLotInfo.Rows[0]["CUSTOMPRODUCTSPECNAME"].ToString();
                    this.uTextAriseDate.Text = string.Format("{0:d}", DateTime.Now);
                    this.uTextAriseTime.Text = DateTime.Now.ToString("HH:mm:ss"); // string.Format("{0:T}", DateTime.Now);
                    this.uTextLotProcessState.Text = dtLotInfo.Rows[0]["LOTPROCESSSTATE"].ToString();
                    this.uTextProcessHoldState.Text = dtLotInfo.Rows[0]["LOTHOLDSTATE"].ToString();
                    this.uTextLotQty.Text = Convert.ToInt32(dtLotInfo.Rows[0]["QTY"].ToString()).ToString();
                    this.uTextAcceptDate.Text = string.Format("{0:d}", DateTime.Now);
                    this.uTextAcceptTime.Text = DateTime.Now.ToString("HH:mm:ss"); // string.Format("{0:T}", DateTime.Now);

                    // 通过工号获取人员名称
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);
                    DataTable dtUser = clsUser.mfReadSYSUser(this.uTextPlantCode.Text, this.uTextWorkUserID.Text, m_resSys.GetString("SYS_LANG"));
                    if (dtUser.Rows.Count > 0 && dtUser.Columns.Contains("UserName"))
                        this.uTextWorkUserName.Text = dtUser.Rows[0]["UserName"].ToString();

                    // 通过ProductCode获取公司名称和代码
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);
                    DataTable dtProductInfo = clsProduct.mfReadProductInfo_frmINS0008C_PSTS(this.uTextPlantCode.Text, this.uTextCustomerProductCode.Text, m_resSys.GetString("SYS_LANG"));
                    if (dtProductInfo.Rows.Count > 0 &&
                        dtProductInfo.Columns.Contains("CustomerCode") &&
                        dtProductInfo.Columns.Contains("CustomerName"))
                    {
                        this.uTextCustomerCode.Text = dtProductInfo.Rows[0]["CustomerCode"].ToString();
                        this.uTextCustomerName.Text = dtProductInfo.Rows[0]["CustomerName"].ToString();
                    }

                    // 获取新的管理编号
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                    QRPINS.BL.INSSTS.AbnormalYield clsManageNo = new QRPINS.BL.INSSTS.AbnormalYield();
                    brwChannel.mfCredentials(clsManageNo);

                    DataTable dtManageNoInfo = clsManageNo.mfManageNo();
                    string dtime = DateTime.Now.ToString("yyyyMMdd");   //20130201
                    if (dtManageNoInfo.Rows.Count > 0 &&
                        dtManageNoInfo.Columns.Contains("ManageNo"))
                    {
                        string mno = dtManageNoInfo.Rows[0]["ManageNo"].ToString();
                        string headmno = mno.Substring(3, 6);
                        int intmno = Convert.ToInt32(mno.Substring(9, 4));
                        if (string.Equals(headmno, dtime.Substring(2,6)))
                        {
                            intmno = intmno + 1;
                            string strtxt = Convert.ToString(intmno);
                            this.uTextManageNo.Text = "STH" + headmno + strtxt.PadLeft(4,'0');
                        }
                        else
                        {

                            this.uTextManageNo.Text = "STH" + dtime.Substring(2, 6) + "0001";
                        }

                    }
                    else
                    {

                        this.uTextManageNo.Text = "STH" + dtime.Substring(2, 6) + "0001";
                    }

                    // LotNo获取是量产品还是开发品
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSPRC.ProcInspectReqLot), "ProcInspectReqLot");
                    QRPINS.BL.INSPRC.ProcInspectReqLot clsLotProductionType = new QRPINS.BL.INSPRC.ProcInspectReqLot();
                    brwChannel.mfCredentials(clsLotProductionType);
                    DataTable dtLotProductionType = clsLotProductionType.mfReadINSProcInspectReq_Check_MP(this.uTextPlantCode.Text, this.uTextLotNo.Text);
                    string strProductionType = "";
                    if (dtLotProductionType.Rows.Count > 0)
                    {
                        strProductionType = dtLotProductionType.Rows[0]["ProductionType"].ToString();
                    }

                    if (!this.uTextProcessHoldState.Text.Equals("OnHold"))
                    {
                        this.uButtonAccept.Enabled = false;
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M000089", strLang), msg.GetMessge_Text("M000082", strLang)
                        , dtLotInfo.Rows[0]["returnmessage"].ToString(), Infragistics.Win.HAlign.Right);
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region Event

        /// <summary>
        /// ContentsArea 펼침상태 변화 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {

                if (this.uGroupBoxContentsArea.Expanded)
                {
                    this.uGridAbormal.Rows.FixedRows.Clear();

                    this.uTextLotNo.ReadOnly = false;
                    this.uTextLotNo.Appearance.BackColor = Color.PowderBlue;
                    this.uButtonAccept.Enabled = true;

                    Init();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 파일경로등록
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridFile_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Row.Activation == Infragistics.Win.UltraWinGrid.Activation.ActivateOnly)
                    return;

                WinMessageBox msg = new WinMessageBox();
                //첨부파일이 등록되어 있는경우 메세지 출력
                if (!e.Cell.Value.ToString().Equals(string.Empty)
                    && !e.Cell.Value.ToString().Contains(":\\"))
                {
                    //파일을 먼저 삭제해주세요.(Delete or BackSpace)
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001150", "M001465", Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "All files (*.*)|*.*"; //"Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    frmCOMFileAttach clsFile = new frmCOMFileAttach();
                    //파일경로중 특수문자가 있는경우 메세지출력
                    if (CheckingSpecialText(strImageFile))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001053", "M001452", "M001451", Infragistics.Win.HAlign.Right);
                        return;
                    }

                    e.Cell.Value = strImageFile;

                }
                
            
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridFile_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                // 삭제를 하는경우
                if (e.KeyCode != Keys.Back && e.KeyCode != Keys.Delete)
                    return;

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
                Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheck = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
                string strStep = string.Empty;
                if (this.uTabStep.Tabs["S1"].Selected)
                {
                    uGrid = this.uGridS1File;
                    uCheck = this.uCheckS1;
                    strStep = "S1";
                }
                else if (this.uTabStep.Tabs["S2"].Selected)
                {
                    uGrid = this.uGridS2File;
                    uCheck = this.uCheckS2;
                    strStep = "S2";
                }
                else if (this.uTabStep.Tabs["S3"].Selected)
                {
                    uGrid = this.uGridS3File;
                    uCheck = this.uCheckS3;
                    strStep = "S3";
                }
                else if (this.uTabStep.Tabs["S4"].Selected)
                {
                    uGrid = this.uGridS4File;
                    uCheck = this.uCheckS4;
                    strStep = "S4";
                }


                //선택된 Cell이 없는경우 Return
                if (uGrid.ActiveCell == null)
                    return;

                // 작성완료한 상태면 Return
                if (!uCheck.Enabled
                    && uCheck.Checked)
                    return;

                //첨부파일 경로가 아니면 Pass
                if (!uGrid.ActiveCell.Column.Key.Equals("FilePath"))
                    return;

                // 아직 첨부파일이 올라간 상태가 아니면 셀 Text만 삭제
                if (!uGrid.ActiveCell.Value.ToString().Equals(string.Empty) &&
                        !uGrid.ActiveCell.Value.ToString().Contains(":\\"))
                {
                    //공장코드, 발생일, 발생시간, Step, 순번
                    string strPlantCode = this.uTextPlantCode.Text;
                    string strManageNo = this.uTextManageNo.Text;
                    string strAriseProcessCode = this.uTextAriseProcessCode.Text;
                    string strAriseDate = this.uTextAriseDate.Text;
                    string strAriseTime = this.uTextAriseTime.Text;
                    string strSeq = uGrid.ActiveCell.Row.RowSelectorNumber.ToString();

                    // 첨부파일이 서버에 올라간경우 첨부파일 삭제
                    //화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //첨부파일 경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0011");

                    //첨부파일 삭제하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                    arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + uGrid.ActiveCell.Value.ToString());

                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrFile
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUploadNoProgView();


                    //Prograss Popup Open
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    //커서변경
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // Step별 파일정보삭제
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYieldFile), "AbnormalYieldFile");
                    QRPINS.BL.INSSTS.AbnormalYieldFile clsFile = new QRPINS.BL.INSSTS.AbnormalYieldFile();
                    brwChannel.mfCredentials(clsFile);

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //첨부파일명 공백처리
                    string strErrRtn = clsFile.mfDeleteINSAbnormalYieldFile(strPlantCode, strManageNo,strAriseProcessCode, strAriseDate, strAriseTime, strStep, strSeq);

                    //커서기본값처리
                    this.MdiParent.Cursor = Cursors.Default;
                    //Prograss Popup close
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    clsSysAccess.Dispose();
                    clsSysFilePath.Dispose();
                    clsFile.Dispose();
                }

                uGrid.ActiveCell.SetValue(string.Empty, false);



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 셀 더블클릭시 파일 다운로드 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridFile_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (e.Cell.Column.ToString() == "FilePath")
                {
                    if (string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value.ToString().Contains(":\\") || e.Cell.Value == DBNull.Value)
                    {
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            // 화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                            // 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                            // 첨부파일 Download
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            arrFile.Add(e.Cell.Value.ToString());

                            // Download정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + "\\" + e.Cell.Value.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        /// <summary>
        /// 헤더 리스트 선택시 상세정보를 볼 수 있음.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridAbormal_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                e.Row.Fixed = true;
                if(!this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = true;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                Init();

                string strPlantCode = e.Row.GetCellValue("PlantCode").ToString();   // 공장
                string strAriseProcessCode = e.Row.GetCellValue("AriseProcessCode").ToString(); // 발생공정
                string strManageNo = e.Row.GetCellValue("ManageNo").ToString();
                string strAriseDate = e.Row.GetCellValue("AriseDate").ToString();   // 발생일
                string strAriseTime = e.Row.GetCellValue("AriseTime").ToString();   // 발생시간
                
                SearchHeaderDetail(strPlantCode,strAriseProcessCode,strAriseDate,strAriseTime,m_resSys.GetString("SYS_LANG"));

                //SearchDetail(strPlantCode, strAriseProcessCode, strAriseDate, strAriseTime, m_resSys.GetString("SYS_LANG")); 查询LOSS数量

                SearchFile(strPlantCode, strManageNo, strAriseProcessCode, strAriseDate, strAriseTime, m_resSys.GetString("SYS_LANG"));



                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 로그인유저의 부서가 품질팀일때만 평가조건 Display
                // BL 연결
                QRPBrowser brwChannel1 = new QRPBrowser();
                brwChannel1.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserCom = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel1.mfCredentials(clsUserCom);
                DataTable dtQualityDept = clsUserCom.mfReadUserCommonCode("QUA", "S0003", m_resSys.GetString("SYS_LANG"));
                if (dtQualityDept.Rows.Count > 0)
                {
                    string strDeptCheck = string.Empty;
                    var varQualityDept = from getRow in dtQualityDept.AsEnumerable()
                                         //where getRow["ComCode"].ToString() != string.Empty//m_resSys.GetString("SYS_DEPTCODE")
                                         select getRow["ComCode"].ToString();

                    if (varQualityDept.Contains(m_resSys.GetString("SYS_DEPTCODE")))
                    {
                        if (this.uTextS5UserID.Text.Equals(string.Empty))
                            SetStepUserID(5);
                        
                        StepReadOnly(5, false);
                    }

                }

                if (this.uCheckReturn.Checked)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();
                    // 반려되었습니다. 다시 확인해주세요.
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000420"
                                        , "M000413"
                                        , Infragistics.Win.HAlign.Right);


                    string strReturnStep = this.uComboReturnStep.Value == null ? string.Empty : this.uComboReturnStep.Value.ToString();
                    if (this.uTabStep.Enabled && !strReturnStep.Equals(string.Empty))
                        this.uTabStep.Tabs[strReturnStep].Selected = true;
                }

                
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 공장코드 변경시 부서콤보 등등 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextPlantCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                string strPlantCode = this.uTextPlantCode.Text.Trim();

                if (strPlantCode.Equals(string.Empty))
                    return;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                brwChannel.mfCredentials(clsDept);

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                DataTable dtDept = clsDept.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCombo = new WinComboEditor();

                this.uComboS1Dept.Items.Clear();
                this.uComboS2Dept.Items.Clear();
                this.uComboS3Dept.Items.Clear();

                // 부서정보 콤보
                wCombo.mfSetComboEditor(this.uComboS1Dept, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);

                wCombo.mfSetComboEditor(this.uComboS2Dept, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);

                wCombo.mfSetComboEditor(this.uComboS3Dept, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "DeptCode", "DeptName", dtDept);


                clsDept.Dispose();
                dtDept.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 사용자 ID KeyDown 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode != Keys.Enter)
                    return;

                Infragistics.Win.UltraWinEditors.UltraTextEditor uText = (Infragistics.Win.UltraWinEditors.UltraTextEditor)sender;

                if (uText.Text.Equals(string.Empty))
                    return;

                // 비활성화 상태면 Return
                if (!uText.Enabled || uText.ReadOnly)
                    return;

                // System Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                String strUserID = uText.Text;

                // User검색 함수 호출
                String strRtnUserName = GetUserName(this.uTextPlantCode.Text, strUserID,m_resSys.GetString("SYS_LANG"));

                if (strRtnUserName.Equals(string.Empty))
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                               "M001264", "M000962", "M000621",
                               Infragistics.Win.HAlign.Right);

                }

                else
                {
                    
                    if (uText.Name.Equals("uTextRegistUserID"))
                        this.uTextRegistUserName.Text = strRtnUserName;
                    else if (uText.Name.Equals("uTextS1UserID"))
                        this.uTextS1UserName.Text = strRtnUserName;
                    else if (uText.Name.Equals("uTextS2UserID"))
                        this.uTextS2UserName.Text = strRtnUserName;
                    else if (uText.Name.Equals("uTextS3UserID"))
                        this.uTextS3UserName.Text = strRtnUserName;
                    else if (uText.Name.Equals("uTextS4UserID"))
                        this.uTextS4UserName.Text = strRtnUserName;
                    else if (uText.Name.Equals("uTextS5UserID"))
                        this.uTextS5UserName.Text = strRtnUserName;
                    else
                        return;
                }
                
                
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자ID EditorButton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraTextEditor uText = (Infragistics.Win.UltraWinEditors.UltraTextEditor)sender;

                if (!uText.Enabled || uText.ReadOnly)
                    return;

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = this.uTextPlantCode.Text;
                frmUser.ShowDialog();

                uText.Text = frmUser.UserID;

                if (uText.Name.Equals("uTextRegistUserID"))
                    this.uTextRegistUserName.Text = frmUser.UserName;
                else if (uText.Name.Equals("uTextS1UserID"))
                    this.uTextS1UserName.Text = frmUser.UserName;
                else if (uText.Name.Equals("uTextS2UserID"))
                    this.uTextS2UserName.Text = frmUser.UserName;
                else if (uText.Name.Equals("uTextS3UserID"))
                    this.uTextS3UserName.Text = frmUser.UserName;
                else if (uText.Name.Equals("uTextS4UserID"))
                    this.uTextS4UserName.Text = frmUser.UserName;
                else if (uText.Name.Equals("uTextS5UserID"))
                    this.uTextS5UserName.Text = frmUser.UserName;
                else
                    return;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자ID 변경시 사용자명 Clear
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextUserID_ValueChanged(object sender, EventArgs e)
        {
            Infragistics.Win.UltraWinEditors.UltraTextEditor uText = (Infragistics.Win.UltraWinEditors.UltraTextEditor)sender;

            if (uText.Name.Equals("uTextRegistUserID"))
            {
                if (!this.uTextRegistUserName.Text.Equals(string.Empty))
                    this.uTextRegistUserName.Clear();
            }
            else if (uText.Name.Equals("uTextS1UserID"))
            {
                if (!this.uTextS1UserName.Text.Equals(string.Empty))
                    this.uTextS1UserName.Clear();
            }
            else if (uText.Name.Equals("uTextS2UserID"))
            {
                if (!this.uTextS2UserName.Text.Equals(string.Empty))
                    this.uTextS2UserName.Clear();
            }
            else if (uText.Name.Equals("uTextS3UserID"))
            {
                if (!this.uTextS3UserName.Text.Equals(string.Empty))
                    this.uTextS3UserName.Clear();
            }
            else if (uText.Name.Equals("uTextS4UserID"))
            {
                if (!this.uTextS4UserName.Text.Equals(string.Empty))
                    this.uTextS4UserName.Clear();
            }
            else if (uText.Name.Equals("uTextS5UserID"))
            {
                if (!this.uTextS5UserName.Text.Equals(string.Empty))
                    this.uTextS5UserName.Clear();
            }
            else
                return;

        }

        /// <summary>
        /// 첨부파일 버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextAttachFile_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key.Equals("UP"))
                {
                    if (!this.uTextAriseCause.Enabled)
                        return;

                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|All Files|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strFile = openFile.FileName;
                        uTextAttachFile.Text = strFile;
                    }
                    
                }
                else if (e.Button.Key.Equals("DOWN"))
                {
                    if (this.uTextAttachFile.Text.Contains(":\\") || this.uTextAttachFile.Text == "")
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000357",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextAttachFile.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileDownloadNoProgView();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextAttachFile.Text);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드에 선택된 파일을 다운로드함.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
                string strStep = string.Empty;
                if (this.uTabStep.Tabs["S1"].Selected)
                {
                    uGrid = this.uGridS1File;
                    strStep = "S1";
                }
                else if (this.uTabStep.Tabs["S2"].Selected)
                {
                    uGrid = this.uGridS2File;
                    strStep = "S2";
                }
                else if (this.uTabStep.Tabs["S3"].Selected)
                {
                    uGrid = this.uGridS3File;
                    strStep = "S3";
                }
                else if (this.uTabStep.Tabs["S4"].Selected)
                {
                    uGrid = this.uGridS4File;
                    strStep = "S4";
                }

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (uGrid.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\")
                            || uGrid.Rows[i].Cells["FilePath"].Value.ToString().Equals(string.Empty))
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        // 화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                        // 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        for (int i = 0; i < uGrid.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(uGrid.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (!uGrid.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\") &&
                                    !string.IsNullOrEmpty(uGrid.Rows[i].Cells["FilePath"].Value.ToString()))
                                {
                                    arrFile.Add(uGrid.Rows[i].Cells["FilePath"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        if (arrFile.Count > 0)
                        {
                            fileAtt.ShowDialog();
                        }

                        // 폴더 열기
                        System.Diagnostics.Process.Start(strSaveFolder);

                        clsSysAccess.Dispose();
                        clsSysFilePath.Dispose();
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 선택된 행에 등록 된 파일정보가 있는경우 파일 및 정보 삭제
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDelRow_Click(object sender, EventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                Infragistics.Win.UltraWinGrid.UltraGrid uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
                string strStep = string.Empty;

                if (this.uTabStep.Tabs["S1"].Selected && this.uCheckS1.Enabled && this.uGridS1File.Rows.Count > 0)
                {
                    uGrid = this.uGridS1File;
                    strStep = "S1";
                }
                else if (this.uTabStep.Tabs["S2"].Selected && this.uCheckS2.Enabled && this.uGridS2File.Rows.Count > 0)
                {
                    uGrid = this.uGridS2File;
                    strStep = "S2";
                }
                else if (this.uTabStep.Tabs["S3"].Selected && this.uCheckS3.Enabled && this.uGridS3File.Rows.Count > 0)
                {
                    uGrid = this.uGridS3File;
                    strStep = "S3";
                }
                else if (this.uTabStep.Tabs["S4"].Selected && this.uCheckS4.Enabled && this.uGridS4File.Rows.Count > 0)
                {
                    uGrid = this.uGridS4File;
                    strStep = "S4";
                }
                else
                {
                    uGrid.Dispose();
                    m_resSys.Close();
                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001323", "M001555",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                    return;
                

                // 첨부파일이 서버에 올라간경우 첨부파일 삭제
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYieldFile), "AbnormalYieldFile");
                QRPINS.BL.INSSTS.AbnormalYieldFile clsFile = new QRPINS.BL.INSSTS.AbnormalYieldFile();
                brwChannel.mfCredentials(clsFile);

                //화일서버 연결정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uTextPlantCode.Text, "S02");

                //첨부파일 경로정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uTextPlantCode.Text, "D0011");

                DataTable dtFile = clsFile.mfDataSet();
                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (!Convert.ToBoolean(uGrid.Rows[i].Cells["Check"].Value))
                        continue;

                    if (uGrid.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\")
                        || uGrid.Rows[i].Cells["FilePath"].Value.ToString().Equals(string.Empty))
                    {
                        uGrid.Rows[i].Hidden = true;
                        continue;
                    }
                    else
                    {
                        DataRow drRow = dtFile.NewRow();

                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["AriseProcessCode"] = this.uTextAriseProcessCode.Text;
                        drRow["AriseDate"] = this.uTextAriseDate.Text;
                        drRow["AriseTime"] = this.uTextAriseTime.Text;
                        drRow["Step"] = strStep;
                        drRow["Seq"] = uGrid.Rows[i].GetCellValue("Seq");
                        

                        dtFile.Rows.Add(drRow);
                        arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + uGrid.Rows[i].GetCellValue("FilePath").ToString());
                        uGrid.Rows[i].Hidden = true;
                        continue;
                    }
                    
                }

                //Prograss Popup Open
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //첨부파일 삭제하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();

                fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                    , arrFile
                                    , dtSysAccess.Rows[0]["AccessID"].ToString()
                                    , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                fileAtt.mfFileUploadNoProgView();

                //첨부파일명 공백처리
                string strErrRtn = clsFile.mfDeleteINSAbnormalYieldFile(dtFile);

                //커서기본값처리
                this.MdiParent.Cursor = Cursors.Default;
                //Prograss Popup close
                m_ProgressPopup.mfCloseProgressPopup(this);

                m_resSys.Close();
                clsFile.Dispose();
                clsSysAccess.Dispose();
                clsSysFilePath.Dispose();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Abnormal UseFlag (Eng'r Check)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonAbnormal_Click(object sender, EventArgs e)
        {
            try
            {
                // 공장정보 없는경우 Return
                if(this.uTextPlantCode.Text.Equals(string.Empty))
                    return;

                // 발생공정이 없는경우 Return
                if(this.uTextAriseProcessCode.Text.Equals(String.Empty))
                    return;

                if (this.uTextLotNo.Text.Equals(string.Empty))
                    return;

                if (this.uTextManageNo.Text.Equals(string.Empty))
                    return;

                if (this.uCheckS5.Checked && !this.uCheckS5.Enabled)
                    return;

                if (this.uComboS0Dept.Value.Equals(string.Empty))
                    return;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");    // 사용자 언어

                if (this.uTextRegistUserID.Text.Equals(string.Empty) 
                    || this.uTextRegistUserName.Text.Equals(string.Empty))
                {
                    // 등록자를 입력해 주세요.
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                   , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "M001264", "M001231", "M000386"
                                                   , Infragistics.Win.HAlign.Right);
                    this.uTextRegistUserID.Focus();
                    return;
                }

                if (this.uComboS0Dept.Value == null || this.uComboS0Dept.Value.ToString().Equals(string.Empty))
                {

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                  , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                  , "M001264", "M001547", "M001551"
                                                  , Infragistics.Win.HAlign.Right);
                    this.uComboS0Dept.DropDown();
                    return;
                }

                this.uOptionAbnormal.Select();

                string strAbnormalFlag = this.uOptionAbnormal.Value == null ? "T" : this.uOptionAbnormal.Value.ToString() == "F" ? "T" : "F";

                // 진행, 취소 여부와 결과 메세지 설정
                string strMsg = strAbnormalFlag == "T" ? "M001545" : "M001544";
                string strMsg1 = strAbnormalFlag == "T" ? "M000930" : "M001552";
                
                //선택하신 정보를 Step별로 진행하시겠습니까?
                //선택하신 정보를 진행취소하시겠습니까?
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001053", strMsg,
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
                    return;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 공장, 발생공정, 발생일, 발생시간 저장
                //string strPlantCode = this.uTextPlantCode.Text;
                //string strAriseProcessCode = this.uTextAriseProcessCode.Text;
                //string strAriseCause = this.uTextAriseCause.Text;
                //string strMeasureDesc = this.uTextMeasureDesc.Text;
                //string strAriseDate = this.uTextAriseDate.Text;
                //string strAriseTime = this.uTextAriseTime.Text;
                //string strAttachFile = this.uTextAttachFile.Text;
                //string strLotNo = this.uTextLotNo.Text;
                //string strManageNo = this.uTextManageNo.Text;
                //string struComboS0Dept = this.uComboS0Dept.Value.ToString();
                //string strRegistUserID = this.uTextRegistUserID.Text;

                // 저장할 정보를 데이터 테이블로 받는다
                DataTable dtHeader = RtnHeader();

                if (dtHeader.Rows.Count > 0)
                    dtHeader.Rows[0]["AbnormalFlag"] = strAbnormalFlag;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                QRPINS.BL.INSSTS.AbnormalYield clsAb = new QRPINS.BL.INSSTS.AbnormalYield();
                brwChannel.mfCredentials(clsAb);

                TransErrRtn ErrRtn = new TransErrRtn();

                // AbnormalFalg 설정 매서드 실행
                string strErrRtn = clsAb.mfSaveINSProcAbnormalYield_Abnormal(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                // 처리결과 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                clsAb.Dispose();    // Resouce Clear

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (ErrRtn.ErrNum == 0)
                {
                    // 입력한 정보를 성공적으로 저장했습니다
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", strMsg1,
                                                    Infragistics.Win.HAlign.Right);

                    this.uOptionAbnormal.Value = strAbnormalFlag;
                    this.uTabStep.Enabled = true;
                }
                else
                {
                    if (ErrRtn.ErrMessage.Contains("AbnormalFlagT")
                        || ErrRtn.ErrMessage.Contains("AbnormalFlagF"))
                    {
                        if (ErrRtn.ErrMessage.Contains("AbnormalFlagT"))
                            strMsg = msg.GetMessge_Text("M001542", strLang);    // 선택하신 정보는 이미 진행되고 있습니다. <br/> 리스트에서 다시 선택해주세요.
                        else
                            strMsg = msg.GetMessge_Text("M001543", strLang);    // 선택하신 정보는 이미 취소처리되었습니다. <br/> 리스트에서 다시 선택해주세요.
                    }
                    else if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMsg = msg.GetMessge_Text("M000953", strLang);    // 입력한 정보를 저장하지 못했습니다.
                    else
                        strMsg = ErrRtn.ErrMessage;

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                      , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , msg.GetMessge_Text("M001135", strLang)
                                                      , msg.GetMessge_Text("M001037", strLang), strMsg
                                                      , Infragistics.Win.HAlign.Right);


                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }


        /// <summary>
        /// Return button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonReturn_Click(object sender, EventArgs e)
        {
            try
            {
                Return();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonAccept_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxInfo.Enabled == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M001041", Infragistics.Win.HAlign.Center);
                    return;
                }
                else
                {
                    if(this.uTextLotNo.Text.Equals(string.Empty))
                    {

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000079", "M000075", Infragistics.Win.HAlign.Right);
                        
                        this.uTextLotNo.Focus();
                        return;
                    }

                    Result = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right);


                    if (Result == DialogResult.No)
                        return;


                    // 저장할 정보를 데이터 테이블로 받는다
                    DataTable dtHeader = RtnHeader();

                    if (dtHeader.Rows.Count > 0)
                        dtHeader.Rows[0]["AcceptFlag"] = "T";

                     QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                     Thread threadPop = m_ProgressPopup.mfStartThread();
                     m_ProgressPopup.mfOpenProgressPopup(this, "처리중...");
                     this.MdiParent.Cursor = Cursors.WaitCursor;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.AbnormalYield), "AbnormalYield");
                    QRPINS.BL.INSSTS.AbnormalYield clsAb = new QRPINS.BL.INSSTS.AbnormalYield();
                    brwChannel.mfCredentials(clsAb);

                    // 저장 메소드 호출
                    string strErrRtn = clsAb.mfSaveINSProcAbnormalYieldHead(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    clsAb.Dispose();
                    dtHeader.Dispose();

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 결과검사
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum == 0)
                    {
                        // MES I/F 메소드 호출
                        if (Send_MESInterFace("N"))
                        {
                            //Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            //                    "M001022", "M001023", "M000930", Infragistics.Win.HAlign.Right);

                            this.uButtonAbnormal.Enabled = true;
                            this.uButtonAccept.Enabled = false;
                            this.uGroupBox.Enabled = true;
                        }

                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                          , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                          , "M001135", "M001037", "M000953"
                                                          , Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                          , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                          , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang)
                                                          , ErrRtn.ErrMessage
                                                          , Infragistics.Win.HAlign.Right);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 输入LOT信息获取MES相关信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextLotNo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextLotNo.ReadOnly) { return; }

                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (!this.uTextLotNo.Text.ToString().Equals(string.Empty))
                    {
                        Rtn_MES_LotInfo();
                    }
                }
            }
            catch
            {

            }
            finally
            { }

        }


        /// <summary>
        /// Eng'r 선택에따라 비활성화, 활성화로 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uOptionAbnormal_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // Eng'r 선택에따라 비활성화, 활성화로 변경
                if (this.uOptionAbnormal.Value == null
                    || this.uOptionAbnormal.Value.ToString().Equals("F"))
                    EngrCheck(false);
                else
                    EngrCheck(true);

                this.uCheckS1.Checked = false;
                this.uCheckS1.Enabled = true;
                this.uCheckS2.Checked = false;
                this.uCheckS2.Enabled = true;
                this.uCheckS3.Checked = false;
                this.uCheckS3.Enabled = true;
                this.uCheckS4.Checked = false;
                this.uCheckS4.Enabled = true;
                for (int i = 2; i <= 4; i++)
                    if (i <= 4)
                        StepEnabled(i, false); // 

                this.uTabStep.Tabs["S1"].Selected = true;

                
               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 최종확인 체크시 (Step진행여부가 "T" && Step4 진행이 안되었다면 메세지 출력 후 체크해제)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uCheckS5_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uCheckS5.Checked)
                    return;
                
                if (this.uOptionAbnormal.Value != null 
                    && this.uOptionAbnormal.Value.ToString().Equals("T"))
                {
                    if (!this.uTabStep.Tabs["S4"].Enabled
                        || !this.uCheckS4.Checked
                        || this.uCheckS4.Enabled)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        System.Windows.Forms.DialogResult result;
                        // Step4 진행이 필요합니다.
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M001547", "M001546",
                                                    Infragistics.Win.HAlign.Right);
                        this.uCheckS5.Checked = false;
                    }
                }
                
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Email

        private void uButtonDel_Email_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEmail.Rows[i].Cells["Check"].Value))
                        this.uGridEmail.Rows[i].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                // 부서정보 콤보정보가 0인경우 Return
                if (this.uComboDept.Items.Count == 0)
                    return;

                // 부서코드 저장
                //string strDept = this.uComboDept.Value == null ? string.Empty : this.uComboDept.Value.ToString();
                string strDept = string.Empty; // "'1010','1008'";//,'D','E','H'";


                //Combo MultiSelect 선택된 정보 저장방법
                for (int i = 0; i < this.uComboDept.Items.Count; i++)
                {
                    if (this.uComboDept.Items[i].CheckState == CheckState.Checked)
                    {
                        if (strDept.Equals(string.Empty))
                            strDept = "'" + this.uComboDept.Items[i].DataValue.ToString() + "'";
                        else
                            strDept = strDept + ", '" + this.uComboDept.Items[i].DataValue.ToString() + "'";

                    }
                }

                // 공백이면 Return
                if (strDept.Equals(string.Empty))
                    return;

                // 사용자 정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자 정보 조회 매서드 실행
                DataTable dtUser = clsUser.mfReadSYSUser_InDept(this.uComboSearchPlant.Value.ToString(), strDept, m_resSys.GetString("SYS_LANG"));

                // Resource 해제
                m_resSys.Close();
                clsUser.Dispose();

                // 유저정보중 Email정보가 있는 정보확인
                if (dtUser.Select("Email <> ''").Count() != 0)
                    dtUser = dtUser.Select("Email <> ''").CopyToDataTable();
                else
                    dtUser.Clear();

                // Email정보가 있는 정보만 Grid에 Bind
                this.uGridEmail.DataSource = dtUser;
                this.uGridEmail.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                    e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridEmail, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!this.uCheckS5.Enabled)
                {
                    if (e.Cell.Column.Key.Equals("UserID"))
                    {
                        //System ResourceInfo
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty) || this.uComboSearchPlant.SelectedIndex.Equals(0))
                        {
                            WinMessageBox msg = new WinMessageBox();
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);
                            return;
                        }

                        frmPOP0011 frmPOP = new frmPOP0011();
                        frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                        frmPOP.ShowDialog();

                        if (this.uComboSearchPlant.Value.ToString() != frmPOP.PlantCode)
                        {
                            DialogResult Result = new DialogResult();
                            WinMessageBox msg = new WinMessageBox();
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M000798", "M000275", "M001254" + this.uComboSearchPlant.Text + "M000009"
                                                        , Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Cells["UserID"].Value = string.Empty;
                            e.Cell.Row.Cells["UserName"].Value = string.Empty;
                            e.Cell.Row.Cells["DeptCode"].Value = string.Empty;
                            e.Cell.Row.Cells["DeptName"].Value = string.Empty;
                            e.Cell.Row.Cells["EMail"].Value = string.Empty;
                        }
                        else
                        {
                            e.Cell.Row.Cells["UserID"].Value = frmPOP.UserID;
                            e.Cell.Row.Cells["UserName"].Value = frmPOP.UserName;
                            e.Cell.Row.Cells["DeptName"].Value = frmPOP.DeptName;
                            e.Cell.Row.Cells["DeptCode"].Value = frmPOP.DeptCode;
                            e.Cell.Row.Cells["EMail"].Value = frmPOP.EMail;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEmail_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridEmail.ActiveCell == null)
                    return;

                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridEmail.ActiveCell;
                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (!uCell.Text.Equals(string.Empty))
                            {
                                //System ResourceInfo
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                                // BL 연결
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                                brwChannel.mfCredentials(clsUser);

                                DataTable dtUserInfo = clsUser.mfReadSYSUser(uCell.Value.ToString(), uCell.Text, m_resSys.GetString("SYS_LANG"));

                                if (dtUserInfo.Rows.Count > 0)
                                {
                                    uCell.Row.Cells["UserName"].Value = dtUserInfo.Rows[0]["UserName"].ToString();
                                    uCell.Row.Cells["DeptCode"].Value = dtUserInfo.Rows[0]["DeptCode"].ToString();
                                    uCell.Row.Cells["DeptName"].Value = dtUserInfo.Rows[0]["DeptName"].ToString();
                                    uCell.Row.Cells["EMail"].Value = dtUserInfo.Rows[0]["Email"].ToString();
                                }
                                else
                                {
                                    DialogResult Result = new DialogResult();
                                    WinMessageBox msg = new WinMessageBox();
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001095", "M000366", "M000368"
                                                                , Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                    else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell uCell = this.uGridEmail.ActiveCell;
                        if (uCell.Text.Equals(string.Empty))
                            return;

                        if (uCell.Column.Key.Equals("UserID"))
                        {
                            if (uCell.Text.Length <= 1 || uCell.SelText == uCell.Text)
                            {
                                uCell.Row.Delete(false);
                            }
                        }
                    }
                }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 접수완료 이메일
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strClaimNo"></param>
        public void SendMail_Receipt(string strPlantCode)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPQAT.BL.QATCLM.CustomerClaimH), "CustomerClaimH");
                //QRPQAT.BL.QATCLM.CustomerClaimH clsClaim = new QRPQAT.BL.QATCLM.CustomerClaimH();
                //brwChannel.mfCredentials(clsClaim);

                //DataTable dtSendLine = clsClaim.mfReadQATCustomerSendMailLine(strPlantCode);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUserInfo = clsUser.mfReadSYSUser(strPlantCode, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                //첨부파일을 위한 FileServer 연결정보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboSearchPlant.Value.ToString(), "S02");

                //메일 발송 첨부파일 경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFileDestPath);
                DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");  //把需要发送的文件附件COPY到该文件目录下。

                //고객불만관리 첨부파일 저장경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysTargetFilePath);
                DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0011");

                QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                ArrayList arrFile = new ArrayList();    //복사할 파일 서버경로, 파일명
                ArrayList arrAttachFile = new ArrayList();  //파일을 복사할 경로
                ArrayList arrFileNonPath = new ArrayList(); //첨부할 파일명

                //AttachentFileName이 있을경우

                string strFileName = "";
                if (!this.uTextAttachFile.Text.Equals(string.Empty))
                {
                    if (this.uTextAttachFile.Text.Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(this.uTextAttachFile.Text);
                        strFileName = this.uTextPlantCode.Text + "-" + this.uTextManageNo.Text + "-" +
                           this.uTextAriseDate.Text + "-" + this.uTextAriseTime.Text.Replace(":", "") + "-" + fileDoc.Name; ;
                    }
                    else
                    {
                        strFileName = this.uTextAttachFile.Text;
                    }
                    string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + strFileName;
                    string TargetFile = "INSProcLowYieldFile/" + strFileName;
                    string NonPathFile = strFileName;
                    arrAttachFile.Add(TargetFile);
                    arrFile.Add(DestFile);
                    arrFileNonPath.Add(NonPathFile);

                    //메일에 첨부할 파일을 서버에서 검색, 메일 첨부파일 폴더로 복사
                    fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                        , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrAttachFile
                                                        , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                        , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                }

                brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                QRPCOM.BL.Mail clsMail = new QRPCOM.BL.Mail();
                brwChannel.mfCredentials(clsMail);

                if (arrFile.Count != 0)
                {
                    fileAtt.mfFileUpload_NonAssync();
                }

                // 2012-11-16 추가
                string strLang = m_resSys.GetString("SYS_LANG");

                string strMail = string.Empty;

                string strManageNo = string.Empty;
                string strCustomer = string.Empty;
                string strPackage = string.Empty;
                string strFaultTypeName = string.Empty;
                string strQty = string.Empty;
                string strFont = string.Empty;
                string strTilte = string.Empty;
                string strContents = string.Empty;
                string strContents1 = string.Empty;

                #region Body
                if (strLang.Equals("KOR"))
                {
                    strMail = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Test Abnormal 인텔리 전스  접수 통보 메일</SPAN></P>";

                    strManageNo = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">관리번호</SPAN>&nbsp&nbsp&nbsp : " + this.uTextManageNo.Value.ToString() + "</P>";
                    strCustomer = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객사</SPAN>&nbsp&nbsp&nbsp : " + this.uTextCustomerCode.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uTextPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">제품명</SPAN>&nbsp&nbsp&nbsp : " + this.uTextCustomerProductSpec.Text + "</P></BR>";
                    strQty = "수량";
                    strFont = "굴림";
                    strTilte = "[QRP]. 있는지 확인 합니다 Test Abnormal 인텔리 전스.";
                    strContents = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">Test Abnormal 인텔리 전스  접수 통보 메일</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";
                }
                else if (strLang.Equals("CHN"))
                {
                    strMail = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Test Abnormal 情报 接收通知邮件</SPAN></P>";

                    strManageNo = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">管理编号</SPAN>&nbsp&nbsp&nbsp : " + this.uTextManageNo.Value.ToString() + "</P>";
                    strCustomer = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">客户社</SPAN>&nbsp&nbsp&nbsp : " + this.uTextCustomerCode.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uTextPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">产品名</SPAN>&nbsp&nbsp&nbsp : " + this.uTextCustomerProductSpec.Text + "</P></BR>";
                    strQty = "数量";
                    strFont = "SimSun";
                    strTilte = "[QRP]. 请确认Test Abnormal 情报";
                    strContents = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">Test Abnormal 情报 接收通知邮件</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: SimSun; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";

                }
                else if (strLang.Equals("ENG"))
                {
                    strMail = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Test Abnormal 인텔리 전스  접수 통보 메일</SPAN></P>";

                    strManageNo = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">관리번호</SPAN>&nbsp&nbsp&nbsp : " + this.uTextManageNo.Value.ToString() + "</P>";
                    strCustomer = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">고객사</SPAN>&nbsp&nbsp&nbsp : " + this.uTextCustomerCode.Value.ToString() + "</P>";
                    strPackage = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">Package</SPAN> : " + this.uTextPackage.Value.ToString() + "</P>";
                    strFaultTypeName = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">제품명</SPAN>&nbsp&nbsp&nbsp : " + this.uTextCustomerProductSpec.Text + "</P></BR>";
                    strQty = "수량";
                    strFont = "굴림";
                    strTilte = "[QRP]. 있는지 확인 합니다 Test Abnormal 인텔리 전스.";
                    strContents = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\">고객불만 접수 통보메일</SPAN></STRONG></P>";
                    strContents1 = "<P style=\"FONT-FAMILY: 굴림; FONT-SIZE: 10pt\"><STRONG><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #0000ff\"></SPAN></STRONG>&nbsp;</P>";
                }
                #endregion

                DataTable uGridDetail = RtnHeader();
                string strLotNo = "";
                for (int k = 0; k < uGridDetail.Rows.Count; k++)
                {                    
                    double dbInQty = Convert.ToDouble(uGridDetail.Rows[k]["LotQty"].ToString());
                    string strInQty = Math.Round(dbInQty).ToString();

                    if (!uGridDetail.Rows[k]["LotNo"].ToString().Equals(string.Empty))
                    {
                        if (strLotNo.Equals(string.Empty))
                        {
                            strLotNo = "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + uGridDetail.Rows[k]["LotNo"].ToString() + "&nbsp; &nbsp; "
                                    + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">" + strQty + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "&nbsp; &nbsp; ";                        }
                        else
                        {
                            strLotNo = strLotNo + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">LotNo</SPAN>&nbsp&nbsp&nbsp : " + uGridDetail.Rows[k]["LotNo"].ToString() + "&nbsp; &nbsp; "
                                + "<P style=\"FONT-FAMILY: " + strFont + "; FONT-SIZE: 10pt\"><SPAN style=\"BACKGROUND-COLOR: #ffffff; COLOR: #000000\">" + strQty + "</SPAN>&nbsp&nbsp&nbsp&nbsp&nbsp : " + strInQty + "&nbsp; &nbsp; ";
                        }
                    }
                }

                for (int i = 0; i < this.uGridEmail.Rows.Count; i++)
                {
                    if (this.uGridEmail.Rows[i].Hidden)
                        continue;

                    if (this.uGridEmail.Rows[i].Cells["EMail"].Value.ToString().Equals(string.Empty))
                        continue;

                    ArrayList eMailList = new ArrayList();
                    eMailList.Add(this.uGridEmail.Rows[i].GetCellValue("EMail").ToString());

                    bool bolRtn = clsMail.mfSendSMTPMail(strPlantCode
                                            , dtUserInfo.Rows[0]["UserName"].ToString()
                                            , this.uGridEmail.Rows[i].GetCellValue("EMail").ToString()
                                            , strTilte
                                            , strContents
                                            + strContents1 + strManageNo
                                            + strCustomer + strPackage + strFaultTypeName + strLotNo + "</Body>" + "</HTML>"
                                            , arrFileNonPath);

                    if (!bolRtn)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M000083", "M001444"
                                , "M001445" + this.uGridEmail.Rows[i].Cells["UserName"].Value.ToString() + "M000005",
                                Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        DialogResult Result = new DialogResult();
                        WinMessageBox msg = new WinMessageBox();

                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M001562",
                                            Infragistics.Win.HAlign.Right);
                        return;
 
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

        }
        #region 测试的邮件发送方法，只是附件发送需要把该方法放在BL里面执行。才能够发送附件。
        public bool mfSendSMTPMail(string strPlantCode, string strFromUserName, string strToEMail,
                                 string strSubject, string strBody, System.Collections.ArrayList arrAttchFileList)
        {
            string strFolderPath = "";
            string strFilePath = "";
            try
            {
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S06");

                if (dtSysAccess.Rows.Count <= 0)
                    return false;

                string strSMTPServer = dtSysAccess.Rows[0]["SystemAddressPath"].ToString();
                int intPort = Convert.ToInt32(dtSysAccess.Rows[0]["EtcAccessInfo1"].ToString());
                string strFrom = dtSysAccess.Rows[0]["AccessID"].ToString();
                string strPwd = dtSysAccess.Rows[0]["AccessPassword"].ToString();

                SmtpClient client = new SmtpClient(strSMTPServer,intPort);

                #region 邮件服务器验证用户名和密码
                    MailAddress net_From = new MailAddress(strFrom, strFromUserName, System.Text.Encoding.UTF8);
                    try
                    {
                        System.Net.NetworkCredential network = new System.Net.NetworkCredential(strFrom, strPwd);
                        client.Credentials = new System.Net.NetworkCredential(net_From.Address, strPwd);
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        return false;
                    }
                #endregion

                MailMessage mail = new MailMessage();
                mail.From = net_From;
                mail.To.Clear();
                mail.To.Add(strToEMail);
                mail.Subject = strSubject;
                mail.Body = strBody;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                // Body에 HTML태그 사용
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.Normal;

                //清空历史附件，防止附件多加载发送
                mail.Attachments.Clear();

                if (arrAttchFileList.Count > 0)
                {
                    //메일 첨부화일 경로 가지고 오기
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                    if (dtFilePath.Rows.Count > 0)
                    {
                        string strServerPath = @dtFilePath.Rows[0]["ServerPath"].ToString();  //@"d:\QRP_STS_FileSvr\UploadFile\";
                        string strFolderName = dtFilePath.Rows[0]["FolderName"].ToString();  //"MailAttachment";

                        strFolderPath = strServerPath + strFolderName;

                        System.Net.Mail.Attachment attach;
                        for (int i = 0; i < arrAttchFileList.Count; i++)
                        {
                            //첨부화일 경로 및 첨부화일명
                            strFilePath = strFolderPath + "\\" + arrAttchFileList[i].ToString();
                            //첨부화일이 있는지 체크함.
                            if (System.IO.File.Exists(strFilePath))
                            {
                                attach = new System.Net.Mail.Attachment(strFilePath);
                                mail.Attachments.Add(attach);
                            }
                        }
                    }
                }

                client.Send(mail);
                mail.Dispose();

                return true;
            }
            catch (System.Exception ex)
            {
                throw (ex);
                return false;
            }
            finally
            {
                //string strFilePath = "";
                //메일을 모두 보낸 후에 첨부화일을 삭제한다.
                if (arrAttchFileList.Count > 0 && strFolderPath != "")
                {
                    for (int i = 0; i < arrAttchFileList.Count; i++)
                    {
                        strFilePath = strFolderPath + "\\" + arrAttchFileList[i].ToString();
                        //첨부화일이 있는지 체크함.
                        if (System.IO.File.Exists(strFilePath))
                            System.IO.File.Delete(strFilePath);
                    }
                }
            }
        }
        #endregion

        #endregion

        #endregion

    }
}
