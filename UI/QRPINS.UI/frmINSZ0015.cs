﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINSZ0015.cs                                        */
/* 프로그램명   : 신제품인증의뢰현황                                    */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-07-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-18 : 기능 추가 (이종호)                       */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.IO;
using System.Collections;

namespace QRPINS.UI
{
    public partial class frmINSZ0015 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        String strAllPlantCode = "";

        public frmINSZ0015()
        {
            InitializeComponent();
        }

        private void frmINSZ0015_Activated(object sender, EventArgs e)
        {
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0015_Load(object sender, EventArgs e)
        {
            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("신규자재인증의뢰현황", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitBuuton();
            InitComboBox();
            InitGrid();
            InitValue();
            InitText();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                this.uTextReqPurpose.MaxLength = 100;
                this.uTextReqID.MaxLength = 20;
                this.uTextEtcDesc.MaxLength = 500;
                this.uTextMSDS.MaxLength = 1000;
                this.uTextICP.MaxLength = 1000;
                this.uTextApplyDevice.MaxLength = 100;

                this.uTextSearchStdNumber.MaxLength = 20;
                this.uTextSearchMaterialCode.MaxLength = 20;
                this.uTextSearchMaterialName.MaxLength = 50;
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextSearchVendorName.MaxLength = 50;
                this.uTextSearchReqUserID.MaxLength = 20;

                // Focus Out 시 Text 안잘리도록
                this.uTextSearchMaterialCode.AlwaysInEditMode = true;
                this.uTextSearchVendorCode.AlwaysInEditMode = true;
                this.uTextSearchReqUserID.AlwaysInEditMode = true;
                this.uTextReqID.AlwaysInEditMode = true;

                // 텍스트박스 스크롤바 생성
                this.uTextReqPurpose.Scrollbars = ScrollBars.Vertical;
                this.uTextEtcDesc.Scrollbars = ScrollBars.Vertical;

                this.uTextShipmentQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxInfo, GroupBoxType.INFO, "입고정보", m_resSys.GetString("SYS_FONTNAME")
                                        , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                                        , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                                        , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBoxInput, GroupBoxType.INFO, "입력사항", m_resSys.GetString("SYS_FONTNAME")
                                        , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                                        , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                                        , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxInput.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxInput.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼 초기화
        /// </summary>
        private void InitBuuton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton button = new WinButton();

                button.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStdNumber, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReqDate, "의뢰일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchComplete, "완료", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelSearchConsumableType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReqUser, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStdNumber, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStorageDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqUser, "의뢰자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelReqDate, "의뢰일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRequestPurpose, "의뢰목적", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAmount, "수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMoldSeq, "금형차수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApplyDevice, "적용Device", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDrawingNo, "도면No", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMSDS, "MSDS", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelICP, "ICP", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUnit, "단위", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// 
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchPlant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsum = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsConsum);

                DataTable dtConsum = clsConsum.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchConsumableType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ConsumableTypeCode", "ConsumableTypeName", dtConsum);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// 
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdNumber", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid1, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MoldSeq", "금형차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid1, 0, "MaterialSpec1", "규격1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "GRDate", "입고일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ShipmentQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnnnnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FloorPlanNo", "도면번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Rev", "Rev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ReqUserName", "의뢰자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ReqDate", "의뢰일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid1, 0, "ReturnReason", "반려사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 500
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid1, 0, "ApplyDevice", "적용Device", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ProgStatus", "진행상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region LotInfo List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGrid2, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "GRQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Return List

                // 반려그리드
                wGrid.mfInitGeneralGrid(this.uGridReturnList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "ReturnUserName", "반려자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "ReturnDate", "반려일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReturnList, 0, "ReturnReason", "반려사유", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                this.uGridReturnList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridReturnList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Attach File List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridFile, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridFile, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "UniqueKey", "Key", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, true, false, 500
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridFile, 0, "CompleteDate", "만료일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", null);

                // Set FontSize
                this.uGridFile.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridFile.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                wGrid.mfAddRowGrid(this.uGridFile, 0);

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 검색조건 의뢰일자 = 오늘날짜
                this.uDateSearchReqFromDate.Value = DateTime.Now.AddDays(-7);
                this.uDateSearchReqToDate.Value = DateTime.Now;

                // 의뢰자 기본값 = 로그인 사용자 ID
                this.uTextReqID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextReqName.Text = m_resSys.GetString("SYS_USERNAME");

                // 의뢰일 기본값 = 오늘날짜
                this.uDateReqDate.Value = DateTime.Now;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                brwChannel.mfCredentials(clsHeader);

                // 조회용 DataTable 컬럼 설정 Method 호출
                DataTable dtSearch = clsHeader.mfSetSearchDataInfo();

                // DataTable 값 저장
                DataRow drRow = dtSearch.NewRow();
                drRow["PlantCode"] = this.uComboSearchPlant.Value.ToString();
                drRow["StdNumber"] = this.uTextSearchStdNumber.Text;
                drRow["MaterialCode"] = this.uTextSearchMaterialCode.Text;
                drRow["MaterialName"] = this.uTextSearchMaterialName.Text;
                drRow["LotNo"] = this.uTextSearchLotNo.Text;
                drRow["VendorCode"] = this.uTextSearchVendorCode.Text;
                drRow["VendorName"] = this.uTextSearchVendorName.Text;
                drRow["ReqFromDate"] = Convert.ToDateTime(this.uDateSearchReqFromDate.Value).ToString("yyyy-MM-dd");
                drRow["ReqToDate"] = Convert.ToDateTime(this.uDateSearchReqToDate.Value).ToString("yyyy-MM-dd");
                drRow["ConsumableType"] = this.uComboSearchConsumableType.Value.ToString();
                drRow["ReqUserID"] = this.uTextSearchReqUserID.Text;
                drRow["CompleteFlag"] = this.uCheckSearchCompleteFlag.Checked.ToString().ToUpper();
                dtSearch.Rows.Add(drRow);

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // Method 호출
                DataTable dtRtnHeader = clsHeader.mfReadINSMaterialConfirmH(dtSearch, m_resSys.GetString("SYS_LANG"));

                this.uGrid1.DataSource = dtRtnHeader;
                this.uGrid1.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtnHeader.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }

                // 상세정보창 접힘상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (strAllPlantCode == "" || this.uTextStdNumber.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000397", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uTextReqID.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000841", Infragistics.Win.HAlign.Center);

                    this.uTextReqID.Focus();
                    return;
                }
                else if (this.uDateReqDate.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000840", Infragistics.Win.HAlign.Center);

                    this.uDateReqDate.DropDown();
                    return;
                }
                else
                {
                    this.uGridFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                    // 첨부파일 입력확인
                    for (int i = 0; i < this.uGridFile.Rows.Count; i++)
                    {
                        if (!this.uGridFile.Rows[i].Hidden)
                        {
                            if (this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\") &&
                                this.uGridFile.Rows[i].Cells["FileTitle"].Value.ToString().Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001085", Infragistics.Win.HAlign.Center);

                                // Set Focus
                                this.uGridFile.Rows[i].Cells["FileTitle"].Activate();
                                this.uGridFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                        }
                    }

                    DataTable dtHeader = RtnHeaderData();
                    DataTable dtFile = RtnFileData();

                    #region 기존소스 주석처리
                    /*
                    ////QRPBrowser brwChannel = new QRPBrowser();
                    ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                    ////QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                    ////brwChannel.mfCredentials(clsHeader);

                    //////화일서버 연결정보 가져오기
                    ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    ////brwChannel.mfCredentials(clsSysAccess);
                    ////DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strAllPlantCode, "S02");

                    //////설비이미지 저장경로정보 가져오기
                    ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    ////QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    ////brwChannel.mfCredentials(clsSysFilePath);
                    ////DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strAllPlantCode, "D0007");

                    ////// 헤더정보 저장할 DataTable Column 설정
                    ////DataTable dtHeader = clsHeader.mfSetDataInfo();
                    ////DataRow drRow = dtHeader.NewRow();

                    //////설비이미지 Upload하기
                    ////frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ////ArrayList arrFile = new ArrayList();

                    ////drRow["PlantCode"] = strAllPlantCode;
                    ////drRow["StdNumber"] = this.uTextStdNumber.Text;
                    ////drRow["ReqID"] = this.uTextReqID.Text;
                    ////drRow["ReqDate"] = this.uDateReqDate.Value.ToString();
                    ////drRow["ReqPurpose"] = this.uTextReqPurpose.Text;
                    ////drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                    ////if (this.uTextMSDS.Text.Contains(":\\"))
                    ////{
                    ////    //화일이름변경(공장코드+관리번호+MSDS+화일명)하여 복사하기//
                    ////    FileInfo fileDoc = new FileInfo(this.uTextMSDS.Text);
                    ////    string strUploadFile = fileDoc.DirectoryName + "\\" +
                    ////                           strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + "MSDS-" + fileDoc.Name;
                    ////    //변경한 화일이 있으면 삭제하기
                    ////    if (File.Exists(strUploadFile))
                    ////        File.Delete(strUploadFile);
                    ////    //변경한 화일이름으로 복사하기
                    ////    File.Copy(this.uTextMSDS.Text, strUploadFile);
                    ////    arrFile.Add(strUploadFile);

                    ////    drRow["MSDSFileName"] = strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + "MSDS-" + fileDoc.Name;
                    ////}
                    ////else
                    ////{
                    ////    drRow["MSDSFileName"] = this.uTextMSDS.Text;
                    ////}
                    ////if (this.uTextICP.Text.Contains(":\\"))
                    ////{
                    ////    //화일이름변경(공장코드+관리번호+ICP+화일명)하여 복사하기//
                    ////    FileInfo fileDoc = new FileInfo(this.uTextICP.Text);
                    ////    string strUploadFile = fileDoc.DirectoryName + "\\" +
                    ////                           strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + "ICP-" + fileDoc.Name;
                    ////    //변경한 화일이 있으면 삭제하기
                    ////    if (File.Exists(strUploadFile))
                    ////        File.Delete(strUploadFile);
                    ////    //변경한 화일이름으로 복사하기
                    ////    File.Copy(this.uTextICP.Text, strUploadFile);
                    ////    arrFile.Add(strUploadFile);

                    ////    drRow["ICPFileName"] = strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + "ICP-" + fileDoc.Name;
                    ////}
                    ////else
                    ////{
                    ////    drRow["ICPFileName"] = this.uTextICP.Text;
                    ////}
                    ////drRow["ShipmentQty"] = this.uTextShipmentQty.Text;
                    ////drRow["ApplyDevice"] = this.uTextApplyDevice.Text;

                    ////dtHeader.Rows.Add(drRow);
                     * */
                    #endregion

                    if (dtHeader.Rows.Count > 0)
                    {
                        // 저장여부를 묻는다
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            // 프로그래스 팝업창 생성
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                            QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                            brwChannel.mfCredentials(clsHeader);

                            // 저장 Method 실행
                            String strErrRtn = clsHeader.mfSaveINSMaterialConfirmH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtFile);

                            // 팦업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // 처리결과에 따른 메세지 박스
                            if (ErrRtn.ErrNum == 0)
                            {
                                ////if (arrFile.Count > 0)
                                ////{
                                ////    //Upload정보 설정
                                ////    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                ////                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                ////                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                ////                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                ////                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                ////    fileAtt.ShowDialog();
                                ////}

                                //FileUpload_Old(this.uTextStdNumber.Text);
                                FileUpload(this.uTextStdNumber.Text);

                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid1.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid1);
                    if (this.uGrid2.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGrid2);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000778",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method...
        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        private void Clear()
        {
            try
            {
                strAllPlantCode = "";
                this.uTextPlant.Text = "";
                this.uTextStdNumber.Text = "";
                this.uTextGRDate.Text = "";
                this.uTextReqID.Text = "";
                this.uTextReqName.Text = "";
                this.uDateReqDate.Value = DateTime.Now;
                this.uTextReqPurpose.Text = "";
                this.uTextVendor.Text = "";
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialName.Text = "";
                this.uTextSpecNo.Text = "";
                this.uTextShipmentQty.Text = "";
                this.uTextMoldSeq.Text = "";
                this.uTextApplyDevice.Text = "";
                this.uTextFloorPlanNo.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextMSDS.Text = "";
                this.uTextICP.Text = "";

                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        private void Search_HeaderData(string strPlantCode, string strStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 상세 헤더
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dt = clsHeader.mfReadINSMaterialConfirmHDetail(strPlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                strAllPlantCode = dt.Rows[0]["PlantCode"].ToString();
                this.uTextPlant.Text = dt.Rows[0]["PlantName"].ToString();
                this.uTextStdNumber.Text = dt.Rows[0]["StdNumber"].ToString();
                this.uTextGRDate.Text = dt.Rows[0]["GRDate"].ToString();
                this.uTextReqID.Text = dt.Rows[0]["ReqID"].ToString();
                this.uTextReqName.Text = dt.Rows[0]["ReqName"].ToString();
                this.uDateReqDate.Value = dt.Rows[0]["ReqDate"];
                this.uTextReqPurpose.Text = dt.Rows[0]["ReqPurpose"].ToString();
                this.uTextVendor.Text = dt.Rows[0]["VendorName"].ToString();
                this.uTextMaterialCode.Text = dt.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dt.Rows[0]["MaterialName"].ToString();
                this.uTextSpecNo.Text = dt.Rows[0]["SpecNo"].ToString();
                this.uTextShipmentQty.Text = Math.Floor(Convert.ToDecimal(dt.Rows[0]["ShipmentQty"])).ToString();
                this.uTextUnitCode.Text = dt.Rows[0]["UnitName"].ToString();
                this.uTextMoldSeq.Text = dt.Rows[0]["MoldSeq"].ToString();
                this.uTextApplyDevice.Text = dt.Rows[0]["ApplyDevice"].ToString();
                this.uTextFloorPlanNo.Text = dt.Rows[0]["FloorPlanNo"].ToString();
                this.uTextEtcDesc.Text = dt.Rows[0]["EtcDesc"].ToString();
                this.uTextMSDS.Text = dt.Rows[0]["MSDSFileName"].ToString();
                this.uTextICP.Text = dt.Rows[0]["ICPFileName"].ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장정보</param>
        /// <param name="strStdNumber">관리번호</param>
        private void Search_DetailData(string strPlantCode, string strStdNumber)
        {
            try
            {
                // Detail
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmD), "MaterialConfirmD");
                QRPINS.BL.INSIMP.MaterialConfirmD clsDetail = new QRPINS.BL.INSIMP.MaterialConfirmD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dt = clsDetail.mfReadINSMaterialConfirmD(strPlantCode, strStdNumber);

                this.uGrid2.DataSource = dt;
                this.uGrid2.DataBind();

                if (dt.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 반려정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        private void Search_ReturnData(string strPlantCode, string strStdNumber)
        {
            try
            {
                // 반려사유
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmReturn), "MaterialConfirmReturn");
                QRPINS.BL.INSIMP.MaterialConfirmReturn clsReturn = new QRPINS.BL.INSIMP.MaterialConfirmReturn();
                brwChannel.mfCredentials(clsReturn);

                DataTable dtReturn = clsReturn.mfReadMaterialConfirmReturn(strPlantCode, strStdNumber);

                this.uGridReturnList.DataSource = dtReturn;
                this.uGridReturnList.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Search_FileData(string strPlantCode, string strStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmFile), "MaterialConfirmFile");
                QRPINS.BL.INSIMP.MaterialConfirmFile clsFile = new QRPINS.BL.INSIMP.MaterialConfirmFile();
                brwChannel.mfCredentials(clsFile);

                DataTable dtFile = clsFile.mfReadMaterialConfirmFile(strPlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                this.uGridFile.SetDataBinding(dtFile, string.Empty);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Events...
        // Contents GroupBox 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 195);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 695;

                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        this.uGrid1.Rows[i].Fixed = false;
                    }

                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 검색조건 자재코드 EditButton 클릭 이벤트
        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 검색조건 거래처코드 EditButton 클릭 이벤트
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 의뢰자ID EditButton 클릭 이벤트
        private void uTextReqID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                //if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                    "확인창", "입력확인", "공장을 선택해주세요.",
                //                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uTextPlant.Text;
                frmPOP.ShowDialog();

                if (strAllPlantCode == frmPOP.PlantCode)
                {
                    this.uTextReqID.Text = frmPOP.UserID;
                    this.uTextReqName.Text = frmPOP.UserName;
                }
                else
                {
                    string strLang = m_resSys.GetString("SYS_LANG");
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                             , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                             , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                             , msg.GetMessge_Text("M001254", strLang) + this.uTextPlant.Text + msg.GetMessge_Text("M000001", strLang)
                                                             , Infragistics.Win.HAlign.Right);
                    this.uTextReqID.Text = "";
                    this.uTextReqName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.SelectedText == this.uTextSearchMaterialCode.Text)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 의뢰자ID 텍스트박스 키다운 이벤트
        private void uTextReqID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReqID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextReqID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(strAllPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextReqID.Text = "";
                            this.uTextReqName.Text = "";
                        }
                        else
                        {
                            this.uTextReqName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReqID.TextLength <= 1 || this.uTextReqID.SelectedText == this.uTextReqID.Text)
                    {
                        this.uTextReqName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 더블클릭시 이벤트(상세정보 조회)
        private void uGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strStdNumber = e.Row.Cells["StdNumber"].Value.ToString();

                Search_HeaderData(strPlantCode, strStdNumber);
                Search_DetailData(strPlantCode, strStdNumber);
                Search_ReturnData(strPlantCode, strStdNumber);
                Search_FileData(strPlantCode, strStdNumber);

                #region 기존 조회 소스 주석처리
                ////// 상세 헤더
                ////QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                ////QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                ////brwChannel.mfCredentials(clsHeader);

                ////DataTable dt = clsHeader.mfReadINSMaterialConfirmHDetail(strPlantCode, strStdNumber, m_resSys.GetString("SYS_LANG"));

                ////strAllPlantCode = dt.Rows[0]["PlantCode"].ToString();
                ////this.uTextPlant.Text = dt.Rows[0]["PlantName"].ToString();
                ////this.uTextStdNumber.Text = dt.Rows[0]["StdNumber"].ToString();
                ////this.uTextGRDate.Text = dt.Rows[0]["GRDate"].ToString();
                ////this.uTextReqID.Text = dt.Rows[0]["ReqID"].ToString();
                ////this.uTextReqName.Text = dt.Rows[0]["ReqName"].ToString();
                ////this.uDateReqDate.Value = dt.Rows[0]["ReqDate"];
                ////this.uTextReqPurpose.Text = dt.Rows[0]["ReqPurpose"].ToString();
                ////this.uTextVendor.Text = dt.Rows[0]["VendorName"].ToString();
                ////this.uTextMaterialCode.Text = dt.Rows[0]["MaterialCode"].ToString();
                ////this.uTextMaterialName.Text = dt.Rows[0]["MaterialName"].ToString();
                ////this.uTextSpecNo.Text = dt.Rows[0]["SpecNo"].ToString();
                ////this.uTextShipmentQty.Text = Math.Floor(Convert.ToDecimal(dt.Rows[0]["ShipmentQty"])).ToString();
                ////this.uTextUnitCode.Text = dt.Rows[0]["UnitName"].ToString();
                ////this.uTextMoldSeq.Text = dt.Rows[0]["MoldSeq"].ToString();
                ////this.uTextApplyDevice.Text = dt.Rows[0]["ApplyDevice"].ToString();
                ////this.uTextFloorPlanNo.Text = dt.Rows[0]["FloorPlanNo"].ToString();
                ////this.uTextEtcDesc.Text = dt.Rows[0]["EtcDesc"].ToString();
                ////this.uTextMSDS.Text = dt.Rows[0]["MSDSFileName"].ToString();
                ////this.uTextICP.Text = dt.Rows[0]["ICPFileName"].ToString();

                ////// Detail
                ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmD), "MaterialConfirmD");
                ////QRPINS.BL.INSIMP.MaterialConfirmD clsDetail = new QRPINS.BL.INSIMP.MaterialConfirmD();
                ////brwChannel.mfCredentials(clsDetail);

                ////dt = clsDetail.mfReadINSMaterialConfirmD(strPlantCode, strStdNumber);

                ////this.uGrid2.DataSource = dt;
                ////this.uGrid2.DataBind();

                ////if (dt.Rows.Count > 0)
                ////{
                ////    WinGrid grd = new WinGrid();
                ////    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                ////}

                ////// 반려사유
                ////brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmReturn), "MaterialConfirmReturn");
                ////QRPINS.BL.INSIMP.MaterialConfirmReturn clsReturn = new QRPINS.BL.INSIMP.MaterialConfirmReturn();
                ////brwChannel.mfCredentials(clsReturn);

                ////DataTable dtReturn = clsReturn.mfReadMaterialConfirmReturn(strPlantCode, strStdNumber);

                ////this.uGridReturnList.DataSource = dtReturn;
                ////this.uGridReturnList.DataBind();
                #endregion

                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 파일 업로드/다운로드 버튼 이벤트
        private void uTextMSDS_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextMSDS.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (!this.uTextMSDS.Text.Contains(this.uTextStdNumber.Text))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strAllPlantCode, "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strAllPlantCode, "D0007");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextMSDS.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileDownloadNoProgView();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextMSDS.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 파일 업로드/다운로드 버튼 이벤트
        private void uTextICP_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (e.Button.Key == "Up")
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        this.uTextICP.Text = strImageFile;
                    }
                }
                else if (e.Button.Key == "Down")
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    // 파일서버에서 불러올수 있는 파일인지 체크
                    if (!this.uTextMSDS.Text.Contains(this.uTextStdNumber.Text))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001135", "M000359",
                                                 Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strAllPlantCode, "S02");

                        //첨부파일 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strAllPlantCode, "D0007");

                        //첨부파일 Download하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(this.uTextICP.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.mfFileDownloadNoProgView();

                        // 파일 실행시키기
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uTextICP.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void frmINSZ0015_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmINSZ0015_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchReqUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = this.uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchReqUserID.Text = frmPOP.UserID;
                this.uTextSearchReqUserName.Text = frmPOP.UserName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchReqUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchReqUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000270", "M000266",
                                        Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                            return;
                        }

                        String strWriteID = this.uTextSearchReqUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboSearchPlant.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextSearchReqUserID.Text = "";
                            this.uTextSearchReqUserName.Text = "";
                        }
                        else
                        {
                            this.uTextSearchReqUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchReqUserID.TextLength <= 1 || this.uTextSearchReqUserID.SelectedText == this.uTextSearchReqUserID.Text)
                    {
                        this.uTextSearchReqUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// 첨부파일 행삭제 버튼 이벤트
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.uGridFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // 첨부파일 저장경로정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strAllPlantCode, "D0007");

                // DB데이터 삭제
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmFile), "MaterialConfirmFile");
                QRPINS.BL.INSIMP.MaterialConfirmFile clsFile = new QRPINS.BL.INSIMP.MaterialConfirmFile();
                brwChannel.mfCredentials(clsFile);

                DataTable dtDelFile = clsFile.mfSetDataInfo();
                DataRow drRow;

                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                for (int i = 0; i < this.uGridFile.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridFile.Rows[i].Cells["Check"].Value))
                    {
                        if (!this.uGridFile.Rows[i].Cells["UniqueKey"].Value.ToString().Equals(string.Empty) &&
                            !this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            // DB 삭제용 데이터 설정
                            drRow = dtDelFile.NewRow();
                            drRow["PlantCode"] = strAllPlantCode;
                            drRow["StdNumber"] = this.uTextStdNumber.Text;
                            drRow["UniqueKey"] = this.uGridFile.Rows[i].Cells["UniqueKey"].Value.ToString();
                            dtDelFile.Rows.Add(drRow);

                            // 첨부파일 폴더 파일삭제용 ArrayList 설정
                            arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + strAllPlantCode+"-"+this.uTextStdNumber.Text+"-"+
                                this.uGridFile.Rows[i].Cells["UniqueKey"].Value.ToString()+"-"+this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString());

                            this.uGridFile.Rows[i].Cells["FilePath"].Value = string.Empty;
                            this.uGridFile.Rows[i].Cells["FilePath"].SetValue(string.Empty, false);
                        }
                        this.uGridFile.Rows[i].Hidden = true;
                    }
                }

                if (arrFile.Count > 0)
                {
                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strAllPlantCode, "S02");

                    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                        , arrFile
                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUploadNoProgView();
                }

                if (dtDelFile.Rows.Count > 0)
                {
                    string strErrRtn = clsFile.mfDeleteMaterialConfirmFile(dtDelFile);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001323", "M000689",
                                            Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridFile_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|Text Files|*.txt|Portable Document Format Files|*.pdf|All Files|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        e.Cell.Value = strImageFile;
                        // Key설정
                        e.Cell.Row.Cells["UniqueKey"].Value = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");

                        this.uGridFile.DisplayLayout.Bands[0].AddNew();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridFile_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (e.Cell.Column.ToString() == "FilePath")
                {
                    if (string.IsNullOrEmpty(e.Cell.Value.ToString()) || e.Cell.Value.ToString().Contains(":\\") || e.Cell.Value == DBNull.Value)
                    {
                        return;
                    }
                    else
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            // 화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strAllPlantCode, "S02");

                            // 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strAllPlantCode, "D0007");

                            // 첨부파일 Download
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            arrFile.Add(strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + e.Cell.Row.Cells["UniqueKey"].Value.ToString() + "-" + e.Cell.Value.ToString());

                            // Download정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + "\\" + strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + e.Cell.Row.Cells["UniqueKey"].Value.ToString() + "-" + e.Cell.Value.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private DataTable RtnHeaderData()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmH), "MaterialConfirmH");
                QRPINS.BL.INSIMP.MaterialConfirmH clsHeader = new QRPINS.BL.INSIMP.MaterialConfirmH();
                brwChannel.mfCredentials(clsHeader);

                // 헤더정보 저장할 DataTable Column 설정
                dtRtn = clsHeader.mfSetDataInfo();
                DataRow drRow = dtRtn.NewRow();

                //설비이미지 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                drRow["PlantCode"] = strAllPlantCode;
                drRow["StdNumber"] = this.uTextStdNumber.Text;
                drRow["ReqID"] = this.uTextReqID.Text;
                drRow["ReqDate"] = this.uDateReqDate.Value.ToString();
                drRow["ReqPurpose"] = this.uTextReqPurpose.Text;
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                if (this.uTextMSDS.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextMSDS.Text);
                    drRow["MSDSFileName"] = strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + "MSDS-" + fileDoc.Name;
                }
                else
                {
                    drRow["MSDSFileName"] = this.uTextMSDS.Text;
                }
                if (this.uTextICP.Text.Contains(":\\"))
                {
                    FileInfo fileDoc = new FileInfo(this.uTextICP.Text);
                    drRow["ICPFileName"] = strAllPlantCode + "-" + this.uTextStdNumber.Text + "-" + "ICP-" + fileDoc.Name;
                }
                else
                {
                    drRow["ICPFileName"] = this.uTextICP.Text;
                }
                drRow["ShipmentQty"] = this.uTextShipmentQty.Text;
                drRow["ApplyDevice"] = this.uTextApplyDevice.Text;

                dtRtn.Rows.Add(drRow);

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable RtnFileData()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialConfirmFile), "MaterialConfirmFile");
                QRPINS.BL.INSIMP.MaterialConfirmFile clsConfirmFile = new QRPINS.BL.INSIMP.MaterialConfirmFile();
                brwChannel.mfCredentials(clsConfirmFile);

                this.uGridFile.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // SET Column
                dtRtn = clsConfirmFile.mfSetDataInfo();
                DataRow drRow;
                // FIle정보 저장
                for (int i = 0; i < this.uGridFile.Rows.Count; i++)
                {
                    if (this.uGridFile.Rows[i].Hidden.Equals(false))
                    {
                        drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = strAllPlantCode;
                        drRow["StdNumber"] = this.uTextStdNumber.Text;
                        drRow["UniqueKey"] = this.uGridFile.Rows[i].Cells["UniqueKey"].Value.ToString();
                        drRow["FileTitle"] = this.uGridFile.Rows[i].Cells["FileTitle"].Value.ToString();
                        if (this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            FileInfo fileDoc = new FileInfo(this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString());
                            drRow["FilePath"] = fileDoc.Name;
                        }
                        else
                        {
                            drRow["FilePath"] = this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString();
                        }
                        drRow["EtcDesc"] = this.uGridFile.Rows[i].Cells["EtcDesc"].Value.ToString();
                        if (this.uGridFile.Rows[i].Cells["CompleteDate"].Value == null || this.uGridFile.Rows[i].Cells["CompleteDate"].Value == DBNull.Value ||
                            this.uGridFile.Rows[i].Cells["CompleteDate"].Value.ToString() == string.Empty)
                            drRow["CompleteDate"] = string.Empty;
                        else
                            drRow["CompleteDate"] = this.uGridFile.Rows[i].Cells["CompleteDate"].Value.ToString();

                        dtRtn.Rows.Add(drRow);
                    }
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        private void FileUpload_Old(string strStdNumber)
        {
            try
            {
                //화일서버 연결정보 가져오기
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strAllPlantCode, "S02");

                //첨부파일 저장경로정보 가져오기
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strAllPlantCode, "D0007");

                //설비이미지 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();
                FileInfo fileDoc;
                //화일이름변경(공장코드+관리번호+MSDS+화일명)하여 복사하기//
                if (this.uTextMSDS.Text.Length > 0)
                {
                    fileDoc = new FileInfo(this.uTextMSDS.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                           strAllPlantCode + "-" + strStdNumber + "-" + "MSDS-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextMSDS.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }

                if (this.uTextICP.Text.Length > 0)
                {
                    //화일이름변경(공장코드+설비점검그룹코드+화일명)하여 복사하기//
                    fileDoc = new FileInfo(this.uTextICP.Text);
                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                           strAllPlantCode + "-" + strStdNumber + "-" + "ICP-" + fileDoc.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadFile))
                        File.Delete(strUploadFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(this.uTextICP.Text, strUploadFile);
                    arrFile.Add(strUploadFile);
                }
                if (arrFile.Count > 0)
                {
                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 Upload 메소드
        /// </summary>
        /// <param name="strClaimNo">발행번호</param>
        private void FileUpload(string strStdNumber)
        {
            try
            {
                // 첨부파일 Upload하기
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                ArrayList arrFile = new ArrayList();

                for (int i = 0; i < this.uGridFile.Rows.Count; i++)
                {
                    if (!this.uGridFile.Rows[i].Hidden)
                    {
                        if (this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString().Contains(":\\"))
                        {
                            // 파일이름변경(공장코드+관리번호+순번+화일명)하여 복사하기//
                            FileInfo fileDoc = new FileInfo(this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString());
                            string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                   strAllPlantCode + "-" + strStdNumber + "-" +
                                                   this.uGridFile.Rows[i].Cells["UniqueKey"].Value.ToString() + "-" + fileDoc.Name;
                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(strUploadFile))
                                File.Delete(strUploadFile);
                            //변경한 화일이름으로 복사하기
                            File.Copy(this.uGridFile.Rows[i].Cells["FilePath"].Value.ToString(), strUploadFile);
                            arrFile.Add(strUploadFile);
                        }
                    }
                }

                // 업로드할 파일이 있는경우 파일 업로드
                if (arrFile.Count > 0)
                {
                    // 화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strAllPlantCode, "S02");

                    // 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strAllPlantCode, "D0007");

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();

                    // 업로드 완료후 복사된 파일 삭제
                    for (int i = 0; i < arrFile.Count; i++)
                    {
                        File.Delete(arrFile[i].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
