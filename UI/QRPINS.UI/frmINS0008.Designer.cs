﻿namespace QRPINS.UI
{
    partial class frmINS0008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINS0008));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
            Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridItem = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridSampling = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridFaultData = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete2 = new Infragistics.Win.Misc.UltraButton();
            this.uGridFault = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInsType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uTextSearchCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomerProductSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDetailProcType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDetailProcType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchFromInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uChartX = new Infragistics.Win.UltraWinChart.UltraChart();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextStackSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStackSeq = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMESTrackInTCheck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCompleteCheck = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateInspectTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uButtonTrackIn = new Infragistics.Win.Misc.UltraButton();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uOptionPassFailFlag = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uLabelPassFailFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uDateInspectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelInspectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextLossCheckFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLossQty = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextGeneration = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProcessHoldState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProcessHoldState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelNowProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWokrUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerProductCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextNowProcessCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextNowProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWorkProcessCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkProcessName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotProcessState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotProcessState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMESSPCNTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESHoldTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTrackOutTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTrackInTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboProcInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLotSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotSize = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextOUTSOURCINGVENDOR = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelOUTSOURCINGVENDOR = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridItem)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSampling)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFaultData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uChartX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStackSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLossCheckFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLossQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGeneration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessHoldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESSPCNTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESHoldTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackOutTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOUTSOURCINGVENDOR)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridItem);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1040, 266);
            // 
            // uGridItem
            // 
            this.uGridItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridItem.DisplayLayout.Appearance = appearance11;
            this.uGridItem.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridItem.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance8.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance8.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.GroupByBox.Appearance = appearance8;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItem.DisplayLayout.GroupByBox.BandLabelAppearance = appearance9;
            this.uGridItem.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance10.BackColor2 = System.Drawing.SystemColors.Control;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridItem.DisplayLayout.GroupByBox.PromptAppearance = appearance10;
            this.uGridItem.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridItem.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridItem.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridItem.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.uGridItem.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridItem.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.Override.CardAreaAppearance = appearance13;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridItem.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridItem.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridItem.DisplayLayout.Override.CellPadding = 0;
            appearance16.BackColor = System.Drawing.SystemColors.Control;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridItem.DisplayLayout.Override.GroupByRowAppearance = appearance16;
            appearance52.TextHAlignAsString = "Left";
            this.uGridItem.DisplayLayout.Override.HeaderAppearance = appearance52;
            this.uGridItem.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridItem.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGridItem.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGridItem.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridItem.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.uGridItem.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridItem.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridItem.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridItem.Location = new System.Drawing.Point(12, 12);
            this.uGridItem.Name = "uGridItem";
            this.uGridItem.Size = new System.Drawing.Size(1020, 252);
            this.uGridItem.TabIndex = 0;
            this.uGridItem.Text = "ultraGrid1";
            this.uGridItem.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridItem_DoubleClickRow);
            this.uGridItem.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridItem_InitializeRow);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridSampling);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1040, 266);
            // 
            // uGridSampling
            // 
            this.uGridSampling.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSampling.DisplayLayout.Appearance = appearance20;
            this.uGridSampling.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSampling.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSampling.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSampling.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGridSampling.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSampling.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.uGridSampling.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSampling.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSampling.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSampling.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.uGridSampling.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSampling.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSampling.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSampling.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridSampling.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSampling.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSampling.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.uGridSampling.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.uGridSampling.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSampling.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.uGridSampling.DisplayLayout.Override.RowAppearance = appearance32;
            this.uGridSampling.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSampling.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridSampling.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSampling.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSampling.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSampling.Location = new System.Drawing.Point(12, 12);
            this.uGridSampling.Name = "uGridSampling";
            this.uGridSampling.Size = new System.Drawing.Size(1020, 252);
            this.uGridSampling.TabIndex = 0;
            this.uGridSampling.Text = "ultraGrid1";
            this.uGridSampling.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridSampling_AfterCellUpdate);
            this.uGridSampling.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridSampling_DoubleClickRow);
            this.uGridSampling.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridSampling_InitializeRow);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uGridFaultData);
            this.ultraTabPageControl4.Controls.Add(this.uButtonDelete2);
            this.ultraTabPageControl4.Controls.Add(this.uGridFault);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1040, 266);
            // 
            // uGridFaultData
            // 
            this.uGridFaultData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFaultData.DisplayLayout.Appearance = appearance74;
            this.uGridFaultData.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFaultData.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance75.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance75.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance75.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFaultData.DisplayLayout.GroupByBox.Appearance = appearance75;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFaultData.DisplayLayout.GroupByBox.BandLabelAppearance = appearance76;
            this.uGridFaultData.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance77.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance77.BackColor2 = System.Drawing.SystemColors.Control;
            appearance77.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance77.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFaultData.DisplayLayout.GroupByBox.PromptAppearance = appearance77;
            this.uGridFaultData.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFaultData.DisplayLayout.MaxRowScrollRegions = 1;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFaultData.DisplayLayout.Override.ActiveCellAppearance = appearance78;
            appearance79.BackColor = System.Drawing.SystemColors.Highlight;
            appearance79.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFaultData.DisplayLayout.Override.ActiveRowAppearance = appearance79;
            this.uGridFaultData.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFaultData.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFaultData.DisplayLayout.Override.CardAreaAppearance = appearance80;
            appearance81.BorderColor = System.Drawing.Color.Silver;
            appearance81.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFaultData.DisplayLayout.Override.CellAppearance = appearance81;
            this.uGridFaultData.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFaultData.DisplayLayout.Override.CellPadding = 0;
            appearance82.BackColor = System.Drawing.SystemColors.Control;
            appearance82.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance82.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance82.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFaultData.DisplayLayout.Override.GroupByRowAppearance = appearance82;
            appearance83.TextHAlignAsString = "Left";
            this.uGridFaultData.DisplayLayout.Override.HeaderAppearance = appearance83;
            this.uGridFaultData.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFaultData.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance84.BackColor = System.Drawing.SystemColors.Window;
            appearance84.BorderColor = System.Drawing.Color.Silver;
            this.uGridFaultData.DisplayLayout.Override.RowAppearance = appearance84;
            this.uGridFaultData.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance85.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFaultData.DisplayLayout.Override.TemplateAddRowAppearance = appearance85;
            this.uGridFaultData.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFaultData.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFaultData.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFaultData.Location = new System.Drawing.Point(304, 12);
            this.uGridFaultData.Name = "uGridFaultData";
            this.uGridFaultData.Size = new System.Drawing.Size(728, 252);
            this.uGridFaultData.TabIndex = 3;
            this.uGridFaultData.Text = "ultraGrid1";
            this.uGridFaultData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridFaultData_KeyDown);
            this.uGridFaultData.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFaultData_ClickCellButton);
            this.uGridFaultData.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridFaultData_BeforeCellListDropDown);
            this.uGridFaultData.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridFaultData_DoubleClickCell);
            // 
            // uButtonDelete2
            // 
            this.uButtonDelete2.Location = new System.Drawing.Point(656, 12);
            this.uButtonDelete2.Name = "uButtonDelete2";
            this.uButtonDelete2.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete2.TabIndex = 2;
            this.uButtonDelete2.Text = "ultraButton1";
            this.uButtonDelete2.Visible = false;
            this.uButtonDelete2.Click += new System.EventHandler(this.uButtonDelete2_Click);
            // 
            // uGridFault
            // 
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFault.DisplayLayout.Appearance = appearance65;
            this.uGridFault.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFault.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFault.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFault.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.uGridFault.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFault.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.uGridFault.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFault.DisplayLayout.MaxRowScrollRegions = 1;
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFault.DisplayLayout.Override.ActiveCellAppearance = appearance73;
            appearance68.BackColor = System.Drawing.SystemColors.Highlight;
            appearance68.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFault.DisplayLayout.Override.ActiveRowAppearance = appearance68;
            this.uGridFault.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFault.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFault.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance66.BorderColor = System.Drawing.Color.Silver;
            appearance66.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFault.DisplayLayout.Override.CellAppearance = appearance66;
            this.uGridFault.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFault.DisplayLayout.Override.CellPadding = 0;
            appearance70.BackColor = System.Drawing.SystemColors.Control;
            appearance70.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance70.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance70.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance70.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFault.DisplayLayout.Override.GroupByRowAppearance = appearance70;
            appearance72.TextHAlignAsString = "Left";
            this.uGridFault.DisplayLayout.Override.HeaderAppearance = appearance72;
            this.uGridFault.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFault.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.uGridFault.DisplayLayout.Override.RowAppearance = appearance71;
            this.uGridFault.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFault.DisplayLayout.Override.TemplateAddRowAppearance = appearance69;
            this.uGridFault.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFault.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFault.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFault.Location = new System.Drawing.Point(12, 12);
            this.uGridFault.Name = "uGridFault";
            this.uGridFault.Size = new System.Drawing.Size(288, 252);
            this.uGridFault.TabIndex = 0;
            this.uGridFault.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFault_AfterCellUpdate);
            this.uGridFault.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridFault_DoubleClickRow);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchInspectUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchInspectUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcInspectType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInsType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcessCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerProductSpec);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomerProductSpec);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDetailProcType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDetailProcType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel3);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 90);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uTextSearchInspectUserName
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchInspectUserName.Appearance = appearance7;
            this.uTextSearchInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchInspectUserName.Location = new System.Drawing.Point(852, 60);
            this.uTextSearchInspectUserName.Name = "uTextSearchInspectUserName";
            this.uTextSearchInspectUserName.ReadOnly = true;
            this.uTextSearchInspectUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchInspectUserName.TabIndex = 62;
            // 
            // uTextSearchInspectUserID
            // 
            appearance49.Image = ((object)(resources.GetObject("appearance49.Image")));
            appearance49.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance49;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchInspectUserID.ButtonsRight.Add(editorButton1);
            this.uTextSearchInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchInspectUserID.Location = new System.Drawing.Point(748, 60);
            this.uTextSearchInspectUserID.Name = "uTextSearchInspectUserID";
            this.uTextSearchInspectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchInspectUserID.TabIndex = 61;
            this.uTextSearchInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ultraTextEditor2_KeyDown);
            this.uTextSearchInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.ultraTextEditor2_EditorButtonClick);
            // 
            // uLabelSearchInspectUser
            // 
            this.uLabelSearchInspectUser.Location = new System.Drawing.Point(644, 60);
            this.uLabelSearchInspectUser.Name = "uLabelSearchInspectUser";
            this.uLabelSearchInspectUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectUser.TabIndex = 60;
            this.uLabelSearchInspectUser.Text = "ultraLabel1";
            // 
            // uComboSearchProcInspectType
            // 
            this.uComboSearchProcInspectType.Location = new System.Drawing.Point(748, 12);
            this.uComboSearchProcInspectType.Name = "uComboSearchProcInspectType";
            this.uComboSearchProcInspectType.Size = new System.Drawing.Size(108, 21);
            this.uComboSearchProcInspectType.TabIndex = 59;
            this.uComboSearchProcInspectType.Text = "uSrchProcInspectGubun";
            // 
            // uLabelSearchInsType
            // 
            this.uLabelSearchInsType.Location = new System.Drawing.Point(644, 12);
            this.uLabelSearchInsType.Name = "uLabelSearchInsType";
            this.uLabelSearchInsType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInsType.TabIndex = 58;
            this.uLabelSearchInsType.Text = "공정검사구분";
            // 
            // uComboSearchProcessCode
            // 
            this.uComboSearchProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance92.BackColor = System.Drawing.SystemColors.Window;
            appearance92.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboSearchProcessCode.DisplayLayout.Appearance = appearance92;
            this.uComboSearchProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance93.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance93.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance93.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.Appearance = appearance93;
            appearance94.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance94;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance95.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance95.BackColor2 = System.Drawing.SystemColors.Control;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboSearchProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance95;
            this.uComboSearchProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboSearchProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance96.BackColor = System.Drawing.SystemColors.Window;
            appearance96.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance96;
            appearance97.BackColor = System.Drawing.SystemColors.Highlight;
            appearance97.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboSearchProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance97;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboSearchProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance98.BackColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance98;
            appearance99.BorderColor = System.Drawing.Color.Silver;
            appearance99.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellAppearance = appearance99;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboSearchProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance100.BackColor = System.Drawing.SystemColors.Control;
            appearance100.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance100.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance100.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboSearchProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance100;
            appearance101.TextHAlignAsString = "Left";
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderAppearance = appearance101;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboSearchProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance102.BackColor = System.Drawing.SystemColors.Window;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowAppearance = appearance102;
            this.uComboSearchProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance103.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboSearchProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance103;
            this.uComboSearchProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboSearchProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboSearchProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboSearchProcessCode.Location = new System.Drawing.Point(928, 36);
            this.uComboSearchProcessCode.Name = "uComboSearchProcessCode";
            this.uComboSearchProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboSearchProcessCode.Size = new System.Drawing.Size(108, 22);
            this.uComboSearchProcessCode.TabIndex = 57;
            this.uComboSearchProcessCode.Text = "ultraCombo1";
            // 
            // uTextSearchCustomerProductSpec
            // 
            this.uTextSearchCustomerProductSpec.Location = new System.Drawing.Point(436, 36);
            this.uTextSearchCustomerProductSpec.Name = "uTextSearchCustomerProductSpec";
            this.uTextSearchCustomerProductSpec.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchCustomerProductSpec.TabIndex = 37;
            // 
            // uLabelSearchCustomerProductSpec
            // 
            this.uLabelSearchCustomerProductSpec.Location = new System.Drawing.Point(332, 36);
            this.uLabelSearchCustomerProductSpec.Name = "uLabelSearchCustomerProductSpec";
            this.uLabelSearchCustomerProductSpec.Size = new System.Drawing.Size(100, 21);
            this.uLabelSearchCustomerProductSpec.TabIndex = 36;
            this.uLabelSearchCustomerProductSpec.Text = "고객제품코드";
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(864, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(60, 20);
            this.uLabelSearchProcess.TabIndex = 34;
            this.uLabelSearchProcess.Text = "공정";
            // 
            // uComboSearchDetailProcType
            // 
            this.uComboSearchDetailProcType.Location = new System.Drawing.Point(748, 36);
            this.uComboSearchDetailProcType.Name = "uComboSearchDetailProcType";
            this.uComboSearchDetailProcType.Size = new System.Drawing.Size(108, 21);
            this.uComboSearchDetailProcType.TabIndex = 33;
            this.uComboSearchDetailProcType.Text = "uSrchProcInspectGubun";
            this.uComboSearchDetailProcType.ValueChanged += new System.EventHandler(this.uComboSearchDetailProcType_ValueChanged);
            // 
            // uLabelSearchDetailProcType
            // 
            this.uLabelSearchDetailProcType.Location = new System.Drawing.Point(644, 36);
            this.uLabelSearchDetailProcType.Name = "uLabelSearchDetailProcType";
            this.uLabelSearchDetailProcType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDetailProcType.TabIndex = 32;
            this.uLabelSearchDetailProcType.Text = "공정Type";
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(436, 12);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(200, 21);
            this.uComboSearchPackage.TabIndex = 31;
            this.uComboSearchPackage.Text = "uSrchProcInspectGubun";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(332, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 30;
            this.uLabelSearchPackage.Text = "Package";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(928, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(108, 21);
            this.uComboSearchCustomer.TabIndex = 29;
            this.uComboSearchCustomer.Text = "uSrchProcInspectGubun";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(864, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(60, 20);
            this.uLabelSearchCustomer.TabIndex = 28;
            this.uLabelSearchCustomer.Text = "고객";
            // 
            // uDateSearchToInspectDate
            // 
            this.uDateSearchToInspectDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToInspectDate.Location = new System.Drawing.Point(284, 60);
            this.uDateSearchToInspectDate.Name = "uDateSearchToInspectDate";
            this.uDateSearchToInspectDate.Size = new System.Drawing.Size(150, 21);
            this.uDateSearchToInspectDate.TabIndex = 27;
            // 
            // ultraLabel3
            // 
            appearance61.TextHAlignAsString = "Center";
            appearance61.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance61;
            this.ultraLabel3.Location = new System.Drawing.Point(268, 60);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 26;
            this.ultraLabel3.Text = "~";
            // 
            // uDateSearchFromInspectDate
            // 
            this.uDateSearchFromInspectDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromInspectDate.Location = new System.Drawing.Point(116, 60);
            this.uDateSearchFromInspectDate.Name = "uDateSearchFromInspectDate";
            this.uDateSearchFromInspectDate.Size = new System.Drawing.Size(150, 21);
            this.uDateSearchFromInspectDate.TabIndex = 25;
            // 
            // uLabelSearchInspectDate
            // 
            this.uLabelSearchInspectDate.Location = new System.Drawing.Point(12, 60);
            this.uLabelSearchInspectDate.Name = "uLabelSearchInspectDate";
            this.uLabelSearchInspectDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectDate.TabIndex = 24;
            this.uLabelSearchInspectDate.Text = "검사일자";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(208, 21);
            this.uTextSearchLotNo.TabIndex = 23;
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 10;
            this.uLabelSearchLotNo.Text = "LotNo";
            // 
            // uTextSearchProductName
            // 
            appearance111.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Appearance = appearance111;
            this.uTextSearchProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Location = new System.Drawing.Point(1040, 64);
            this.uTextSearchProductName.Name = "uTextSearchProductName";
            this.uTextSearchProductName.ReadOnly = true;
            this.uTextSearchProductName.Size = new System.Drawing.Size(21, 21);
            this.uTextSearchProductName.TabIndex = 7;
            this.uTextSearchProductName.Visible = false;
            // 
            // uTextSearchProductCode
            // 
            appearance112.Image = ((object)(resources.GetObject("appearance112.Image")));
            appearance112.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance112;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchProductCode.ButtonsRight.Add(editorButton2);
            this.uTextSearchProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchProductCode.Location = new System.Drawing.Point(1012, 64);
            this.uTextSearchProductCode.Name = "uTextSearchProductCode";
            this.uTextSearchProductCode.Size = new System.Drawing.Size(21, 21);
            this.uTextSearchProductCode.TabIndex = 5;
            this.uTextSearchProductCode.Visible = false;
            this.uTextSearchProductCode.Click += new System.EventHandler(this.uTextSearchProductCode_Click);
            // 
            // uLabelSearchMaterialCode
            // 
            this.uLabelSearchMaterialCode.Location = new System.Drawing.Point(984, 64);
            this.uLabelSearchMaterialCode.Name = "uLabelSearchMaterialCode";
            this.uLabelSearchMaterialCode.Size = new System.Drawing.Size(21, 21);
            this.uLabelSearchMaterialCode.TabIndex = 4;
            this.uLabelSearchMaterialCode.Text = "ultraLabel1";
            this.uLabelSearchMaterialCode.Visible = false;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(208, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 645);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 200);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 645);
            this.uGroupBoxContentsArea.TabIndex = 6;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uChartX);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 625);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uChartX
            // 
            this.uChartX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uChartX.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
            paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
            this.uChartX.Axis.PE = paintElement1;
            this.uChartX.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartX.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X.LineThickness = 1;
            this.uChartX.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X.MajorGridLines.Visible = true;
            this.uChartX.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X.MinorGridLines.Visible = false;
            this.uChartX.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.X.Visible = true;
            this.uChartX.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartX.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
            this.uChartX.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.X2.Labels.Visible = false;
            this.uChartX.Axis.X2.LineThickness = 1;
            this.uChartX.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X2.MajorGridLines.Visible = true;
            this.uChartX.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.X2.MinorGridLines.Visible = false;
            this.uChartX.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.X2.Visible = false;
            this.uChartX.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
            this.uChartX.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartX.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y.LineThickness = 1;
            this.uChartX.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y.MajorGridLines.Visible = true;
            this.uChartX.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y.MinorGridLines.Visible = false;
            this.uChartX.Axis.Y.TickmarkInterval = 200;
            this.uChartX.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Y.Visible = true;
            this.uChartX.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
            this.uChartX.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
            this.uChartX.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Y2.Labels.Visible = false;
            this.uChartX.Axis.Y2.LineThickness = 1;
            this.uChartX.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y2.MajorGridLines.Visible = true;
            this.uChartX.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Y2.MinorGridLines.Visible = false;
            this.uChartX.Axis.Y2.TickmarkInterval = 200;
            this.uChartX.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Y2.Visible = false;
            this.uChartX.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Z.Labels.ItemFormatString = "";
            this.uChartX.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
            this.uChartX.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z.LineThickness = 1;
            this.uChartX.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z.MajorGridLines.Visible = true;
            this.uChartX.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z.MinorGridLines.Visible = false;
            this.uChartX.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Z.Visible = false;
            this.uChartX.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
            this.uChartX.Axis.Z2.Labels.ItemFormatString = "";
            this.uChartX.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
            this.uChartX.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
            this.uChartX.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
            this.uChartX.Axis.Z2.Labels.Visible = false;
            this.uChartX.Axis.Z2.LineThickness = 1;
            this.uChartX.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
            this.uChartX.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z2.MajorGridLines.Visible = true;
            this.uChartX.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
            this.uChartX.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
            this.uChartX.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
            this.uChartX.Axis.Z2.MinorGridLines.Visible = false;
            this.uChartX.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
            this.uChartX.Axis.Z2.Visible = false;
            this.uChartX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.uChartX.ColorModel.AlphaLevel = ((byte)(150));
            this.uChartX.ColorModel.ColorBegin = System.Drawing.Color.Pink;
            this.uChartX.ColorModel.ColorEnd = System.Drawing.Color.DarkRed;
            this.uChartX.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
            this.uChartX.Effects.Effects.Add(gradientEffect1);
            this.uChartX.Location = new System.Drawing.Point(4, 470);
            this.uChartX.Name = "uChartX";
            this.uChartX.Size = new System.Drawing.Size(1040, 148);
            this.uChartX.TabIndex = 8;
            this.uChartX.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
            this.uChartX.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Controls.Add(this.ultraTabPageControl4);
            this.uTab.Location = new System.Drawing.Point(4, 180);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(1044, 292);
            this.uTab.TabIndex = 7;
            ultraTab1.Key = "0";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "검사결과";
            ultraTab2.Key = "1";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "샘플링검사";
            ultraTab4.Key = "2";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "불량유형";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1040, 266);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uTextStackSeq);
            this.uGroupBox2.Controls.Add(this.uLabelStackSeq);
            this.uGroupBox2.Controls.Add(this.uTextMESTrackInTCheck);
            this.uGroupBox2.Controls.Add(this.uTextCompleteCheck);
            this.uGroupBox2.Controls.Add(this.uDateInspectTime);
            this.uGroupBox2.Controls.Add(this.uButtonTrackIn);
            this.uGroupBox2.Controls.Add(this.uTextInspectUserName);
            this.uGroupBox2.Controls.Add(this.uTextInspectUserID);
            this.uGroupBox2.Controls.Add(this.uLabelInspectUser);
            this.uGroupBox2.Controls.Add(this.uCheckCompleteFlag);
            this.uGroupBox2.Controls.Add(this.uOptionPassFailFlag);
            this.uGroupBox2.Controls.Add(this.uLabelPassFailFlag);
            this.uGroupBox2.Controls.Add(this.uDateInspectDate);
            this.uGroupBox2.Controls.Add(this.uLabelInspectDate);
            this.uGroupBox2.Location = new System.Drawing.Point(4, 136);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1044, 44);
            this.uGroupBox2.TabIndex = 6;
            // 
            // uTextStackSeq
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStackSeq.Appearance = appearance60;
            this.uTextStackSeq.BackColor = System.Drawing.Color.Gainsboro;
            appearance86.Image = ((object)(resources.GetObject("appearance86.Image")));
            appearance86.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance86;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Visible = false;
            this.uTextStackSeq.ButtonsRight.Add(editorButton3);
            this.uTextStackSeq.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextStackSeq.Location = new System.Drawing.Point(518, 12);
            this.uTextStackSeq.Name = "uTextStackSeq";
            this.uTextStackSeq.ReadOnly = true;
            this.uTextStackSeq.Size = new System.Drawing.Size(100, 21);
            this.uTextStackSeq.TabIndex = 96;
            // 
            // uLabelStackSeq
            // 
            this.uLabelStackSeq.Location = new System.Drawing.Point(456, 12);
            this.uLabelStackSeq.Name = "uLabelStackSeq";
            this.uLabelStackSeq.Size = new System.Drawing.Size(60, 20);
            this.uLabelStackSeq.TabIndex = 95;
            this.uLabelStackSeq.Text = "ultraLabel1";
            // 
            // uTextMESTrackInTCheck
            // 
            appearance114.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTCheck.Appearance = appearance114;
            this.uTextMESTrackInTCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTCheck.Location = new System.Drawing.Point(992, 8);
            this.uTextMESTrackInTCheck.Name = "uTextMESTrackInTCheck";
            this.uTextMESTrackInTCheck.ReadOnly = true;
            this.uTextMESTrackInTCheck.Size = new System.Drawing.Size(20, 21);
            this.uTextMESTrackInTCheck.TabIndex = 94;
            this.uTextMESTrackInTCheck.Visible = false;
            // 
            // uTextCompleteCheck
            // 
            appearance115.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteCheck.Appearance = appearance115;
            this.uTextCompleteCheck.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteCheck.Location = new System.Drawing.Point(1016, 8);
            this.uTextCompleteCheck.Name = "uTextCompleteCheck";
            this.uTextCompleteCheck.ReadOnly = true;
            this.uTextCompleteCheck.Size = new System.Drawing.Size(20, 21);
            this.uTextCompleteCheck.TabIndex = 93;
            this.uTextCompleteCheck.Visible = false;
            // 
            // uDateInspectTime
            // 
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateInspectTime.ButtonsRight.Add(spinEditorButton1);
            this.uDateInspectTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateInspectTime.Location = new System.Drawing.Point(52, 4);
            this.uDateInspectTime.MaskInput = "{LOC}hh:mm:ss";
            this.uDateInspectTime.Name = "uDateInspectTime";
            this.uDateInspectTime.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectTime.TabIndex = 92;
            this.uDateInspectTime.Value = null;
            this.uDateInspectTime.Visible = false;
            // 
            // uButtonTrackIn
            // 
            this.uButtonTrackIn.Location = new System.Drawing.Point(904, 12);
            this.uButtonTrackIn.Name = "uButtonTrackIn";
            this.uButtonTrackIn.Size = new System.Drawing.Size(75, 23);
            this.uButtonTrackIn.TabIndex = 91;
            this.uButtonTrackIn.Text = "TrackIn";
            this.uButtonTrackIn.Visible = false;
            this.uButtonTrackIn.Click += new System.EventHandler(this.uButtonTrackIn_Click);
            // 
            // uTextInspectUserName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance4;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(348, 12);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserName.TabIndex = 89;
            // 
            // uTextInspectUserID
            // 
            appearance104.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserID.Appearance = appearance104;
            this.uTextInspectUserID.BackColor = System.Drawing.Color.Gainsboro;
            appearance105.Image = ((object)(resources.GetObject("appearance105.Image")));
            appearance105.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance105;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Visible = false;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton4);
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(246, 12);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.ReadOnly = true;
            this.uTextInspectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserID.TabIndex = 88;
            this.uTextInspectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextInspectUserID_KeyDown);
            this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextInspectUserID_EditorButtonClick);
            // 
            // uLabelInspectUser
            // 
            this.uLabelInspectUser.Location = new System.Drawing.Point(184, 12);
            this.uLabelInspectUser.Name = "uLabelInspectUser";
            this.uLabelInspectUser.Size = new System.Drawing.Size(60, 20);
            this.uLabelInspectUser.TabIndex = 87;
            this.uLabelInspectUser.Text = "ultraLabel1";
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckCompleteFlag.Enabled = false;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(820, 12);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(72, 20);
            this.uCheckCompleteFlag.TabIndex = 86;
            this.uCheckCompleteFlag.Text = "작성완료";
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCompleteFlag.CheckedValueChanged += new System.EventHandler(this.uCheckCompleteFlag_CheckedValueChanged);
            // 
            // uOptionPassFailFlag
            // 
            this.uOptionPassFailFlag.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.uOptionPassFailFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007RadioButtonGlyphInfo;
            valueListItem6.DataValue = "OK";
            valueListItem6.DisplayText = "합격";
            valueListItem7.DataValue = "NG";
            valueListItem7.DisplayText = "불합격";
            this.uOptionPassFailFlag.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem6,
            valueListItem7});
            this.uOptionPassFailFlag.Location = new System.Drawing.Point(704, 12);
            this.uOptionPassFailFlag.Name = "uOptionPassFailFlag";
            this.uOptionPassFailFlag.Size = new System.Drawing.Size(108, 20);
            this.uOptionPassFailFlag.TabIndex = 41;
            this.uOptionPassFailFlag.TextIndentation = 2;
            this.uOptionPassFailFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelPassFailFlag
            // 
            this.uLabelPassFailFlag.Location = new System.Drawing.Point(628, 12);
            this.uLabelPassFailFlag.Name = "uLabelPassFailFlag";
            this.uLabelPassFailFlag.Size = new System.Drawing.Size(70, 20);
            this.uLabelPassFailFlag.TabIndex = 40;
            this.uLabelPassFailFlag.Text = "ultraLabel1";
            // 
            // uDateInspectDate
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.uDateInspectDate.Appearance = appearance59;
            this.uDateInspectDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uDateInspectDate.Location = new System.Drawing.Point(76, 12);
            this.uDateInspectDate.Name = "uDateInspectDate";
            this.uDateInspectDate.ReadOnly = true;
            this.uDateInspectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateInspectDate.TabIndex = 17;
            // 
            // uLabelInspectDate
            // 
            this.uLabelInspectDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelInspectDate.Name = "uLabelInspectDate";
            this.uLabelInspectDate.Size = new System.Drawing.Size(60, 20);
            this.uLabelInspectDate.TabIndex = 16;
            this.uLabelInspectDate.Text = "ultraLabel2";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextOUTSOURCINGVENDOR);
            this.uGroupBox1.Controls.Add(this.uLabelOUTSOURCINGVENDOR);
            this.uGroupBox1.Controls.Add(this.uTextLossCheckFlag);
            this.uGroupBox1.Controls.Add(this.uTextLossQty);
            this.uGroupBox1.Controls.Add(this.uTextGeneration);
            this.uGroupBox1.Controls.Add(this.uTextProcessHoldState);
            this.uGroupBox1.Controls.Add(this.uLabelProcessHoldState);
            this.uGroupBox1.Controls.Add(this.uTextWorkUserID);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uTextCustomerName);
            this.uGroupBox1.Controls.Add(this.uTextProductName);
            this.uGroupBox1.Controls.Add(this.uLabelNowProcess);
            this.uGroupBox1.Controls.Add(this.uTextWorkUserName);
            this.uGroupBox1.Controls.Add(this.uLabelWokrUser);
            this.uGroupBox1.Controls.Add(this.uLabelCustomerProductCode);
            this.uGroupBox1.Controls.Add(this.uTextCustomerProductCode);
            this.uGroupBox1.Controls.Add(this.uTextNowProcessCode);
            this.uGroupBox1.Controls.Add(this.uTextNowProcessName);
            this.uGroupBox1.Controls.Add(this.uLabelPackage);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uTextWorkProcessCode);
            this.uGroupBox1.Controls.Add(this.uTextPackage);
            this.uGroupBox1.Controls.Add(this.uTextCustomerCode);
            this.uGroupBox1.Controls.Add(this.uLabelCustomer);
            this.uGroupBox1.Controls.Add(this.uTextWorkProcessName);
            this.uGroupBox1.Controls.Add(this.uLabelWorkProcess);
            this.uGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabelEquip);
            this.uGroupBox1.Controls.Add(this.uTextProductCode);
            this.uGroupBox1.Controls.Add(this.uLabelProduct);
            this.uGroupBox1.Controls.Add(this.uTextLotProcessState);
            this.uGroupBox1.Controls.Add(this.uLabelLotProcessState);
            this.uGroupBox1.Controls.Add(this.uTextMESSPCNTFlag);
            this.uGroupBox1.Controls.Add(this.uTextMESHoldTFlag);
            this.uGroupBox1.Controls.Add(this.uTextMESTrackOutTFlag);
            this.uGroupBox1.Controls.Add(this.uTextMESTrackInTFlag);
            this.uGroupBox1.Controls.Add(this.uTextReqSeq);
            this.uGroupBox1.Controls.Add(this.uTextReqNo);
            this.uGroupBox1.Controls.Add(this.uComboProcInspectType);
            this.uGroupBox1.Controls.Add(this.uLabelProcInspectType);
            this.uGroupBox1.Controls.Add(this.uTextLotNo);
            this.uGroupBox1.Controls.Add(this.uTextLotSize);
            this.uGroupBox1.Controls.Add(this.uLabelLotSize);
            this.uGroupBox1.Controls.Add(this.uLabelLotNo);
            this.uGroupBox1.Controls.Add(this.uComboPlantCode);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Location = new System.Drawing.Point(4, 4);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1044, 132);
            this.uGroupBox1.TabIndex = 5;
            // 
            // uTextLossCheckFlag
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLossCheckFlag.Appearance = appearance18;
            this.uTextLossCheckFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLossCheckFlag.Location = new System.Drawing.Point(992, 104);
            this.uTextLossCheckFlag.Name = "uTextLossCheckFlag";
            this.uTextLossCheckFlag.ReadOnly = true;
            this.uTextLossCheckFlag.Size = new System.Drawing.Size(20, 21);
            this.uTextLossCheckFlag.TabIndex = 163;
            this.uTextLossCheckFlag.Visible = false;
            // 
            // uTextLossQty
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLossQty.Appearance = appearance57;
            this.uTextLossQty.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLossQty.Location = new System.Drawing.Point(1016, 104);
            this.uTextLossQty.Name = "uTextLossQty";
            this.uTextLossQty.ReadOnly = true;
            this.uTextLossQty.Size = new System.Drawing.Size(20, 21);
            this.uTextLossQty.TabIndex = 162;
            this.uTextLossQty.Visible = false;
            // 
            // uTextGeneration
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGeneration.Appearance = appearance2;
            this.uTextGeneration.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGeneration.Location = new System.Drawing.Point(1016, 80);
            this.uTextGeneration.Name = "uTextGeneration";
            this.uTextGeneration.ReadOnly = true;
            this.uTextGeneration.Size = new System.Drawing.Size(21, 21);
            this.uTextGeneration.TabIndex = 161;
            this.uTextGeneration.Visible = false;
            // 
            // uTextProcessHoldState
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessHoldState.Appearance = appearance21;
            this.uTextProcessHoldState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcessHoldState.Location = new System.Drawing.Point(380, 108);
            this.uTextProcessHoldState.Name = "uTextProcessHoldState";
            this.uTextProcessHoldState.ReadOnly = true;
            this.uTextProcessHoldState.Size = new System.Drawing.Size(230, 21);
            this.uTextProcessHoldState.TabIndex = 160;
            // 
            // uLabelProcessHoldState
            // 
            this.uLabelProcessHoldState.Location = new System.Drawing.Point(276, 108);
            this.uLabelProcessHoldState.Name = "uLabelProcessHoldState";
            this.uLabelProcessHoldState.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessHoldState.TabIndex = 159;
            this.uLabelProcessHoldState.Text = "ultraLabel2";
            // 
            // uTextWorkUserID
            // 
            appearance39.BackColor = System.Drawing.Color.White;
            this.uTextWorkUserID.Appearance = appearance39;
            this.uTextWorkUserID.BackColor = System.Drawing.Color.White;
            appearance106.Image = ((object)(resources.GetObject("appearance106.Image")));
            appearance106.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton5.Appearance = appearance106;
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Visible = false;
            this.uTextWorkUserID.ButtonsRight.Add(editorButton5);
            this.uTextWorkUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkUserID.Location = new System.Drawing.Point(960, 84);
            this.uTextWorkUserID.Name = "uTextWorkUserID";
            this.uTextWorkUserID.Size = new System.Drawing.Size(21, 21);
            this.uTextWorkUserID.TabIndex = 156;
            this.uTextWorkUserID.Visible = false;
            // 
            // uTextEquipName
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance38;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(950, 60);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(21, 21);
            this.uTextEquipName.TabIndex = 139;
            this.uTextEquipName.Visible = false;
            // 
            // uTextCustomerName
            // 
            appearance50.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance50;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(584, 12);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(21, 21);
            this.uTextCustomerName.TabIndex = 146;
            this.uTextCustomerName.Visible = false;
            // 
            // uTextProductName
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance22;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(584, 36);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(21, 21);
            this.uTextProductName.TabIndex = 137;
            this.uTextProductName.Visible = false;
            // 
            // uLabelNowProcess
            // 
            this.uLabelNowProcess.Location = new System.Drawing.Point(644, 12);
            this.uLabelNowProcess.Name = "uLabelNowProcess";
            this.uLabelNowProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelNowProcess.TabIndex = 158;
            this.uLabelNowProcess.Text = "ultraLabel2";
            // 
            // uTextWorkUserName
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Appearance = appearance5;
            this.uTextWorkUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Location = new System.Drawing.Point(748, 84);
            this.uTextWorkUserName.Name = "uTextWorkUserName";
            this.uTextWorkUserName.ReadOnly = true;
            this.uTextWorkUserName.Size = new System.Drawing.Size(233, 21);
            this.uTextWorkUserName.TabIndex = 157;
            // 
            // uLabelWokrUser
            // 
            this.uLabelWokrUser.Location = new System.Drawing.Point(644, 84);
            this.uLabelWokrUser.Name = "uLabelWokrUser";
            this.uLabelWokrUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelWokrUser.TabIndex = 155;
            this.uLabelWokrUser.Text = "ultraLabel1";
            // 
            // uLabelCustomerProductCode
            // 
            this.uLabelCustomerProductCode.Location = new System.Drawing.Point(276, 60);
            this.uLabelCustomerProductCode.Name = "uLabelCustomerProductCode";
            this.uLabelCustomerProductCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomerProductCode.TabIndex = 154;
            this.uLabelCustomerProductCode.Text = "ultraLabel2";
            // 
            // uTextCustomerProductCode
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductCode.Appearance = appearance55;
            this.uTextCustomerProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductCode.Location = new System.Drawing.Point(380, 60);
            this.uTextCustomerProductCode.Name = "uTextCustomerProductCode";
            this.uTextCustomerProductCode.ReadOnly = true;
            this.uTextCustomerProductCode.Size = new System.Drawing.Size(230, 21);
            this.uTextCustomerProductCode.TabIndex = 153;
            // 
            // uTextNowProcessCode
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessCode.Appearance = appearance6;
            this.uTextNowProcessCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessCode.Location = new System.Drawing.Point(748, 12);
            this.uTextNowProcessCode.Name = "uTextNowProcessCode";
            this.uTextNowProcessCode.ReadOnly = true;
            this.uTextNowProcessCode.Size = new System.Drawing.Size(100, 21);
            this.uTextNowProcessCode.TabIndex = 151;
            // 
            // uTextNowProcessName
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessName.Appearance = appearance37;
            this.uTextNowProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextNowProcessName.Location = new System.Drawing.Point(852, 12);
            this.uTextNowProcessName.Name = "uTextNowProcessName";
            this.uTextNowProcessName.ReadOnly = true;
            this.uTextNowProcessName.Size = new System.Drawing.Size(130, 21);
            this.uTextNowProcessName.TabIndex = 150;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(276, 84);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackage.TabIndex = 152;
            this.uLabelPackage.Text = "ultraLabel2";
            // 
            // uTextEquipCode
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance3;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(748, 60);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(233, 21);
            this.uTextEquipCode.TabIndex = 149;
            // 
            // uTextWorkProcessCode
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessCode.Appearance = appearance46;
            this.uTextWorkProcessCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessCode.Location = new System.Drawing.Point(748, 36);
            this.uTextWorkProcessCode.Name = "uTextWorkProcessCode";
            this.uTextWorkProcessCode.ReadOnly = true;
            this.uTextWorkProcessCode.Size = new System.Drawing.Size(100, 21);
            this.uTextWorkProcessCode.TabIndex = 148;
            // 
            // uTextPackage
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance43;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(380, 84);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(230, 21);
            this.uTextPackage.TabIndex = 147;
            // 
            // uTextCustomerCode
            // 
            appearance51.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance51;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(380, 12);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(230, 21);
            this.uTextCustomerCode.TabIndex = 145;
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(276, 12);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomer.TabIndex = 144;
            this.uLabelCustomer.Text = "ultraLabel2";
            // 
            // uTextWorkProcessName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessName.Appearance = appearance40;
            this.uTextWorkProcessName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkProcessName.Location = new System.Drawing.Point(852, 36);
            this.uTextWorkProcessName.Name = "uTextWorkProcessName";
            this.uTextWorkProcessName.ReadOnly = true;
            this.uTextWorkProcessName.Size = new System.Drawing.Size(130, 21);
            this.uTextWorkProcessName.TabIndex = 143;
            // 
            // uLabelWorkProcess
            // 
            this.uLabelWorkProcess.Location = new System.Drawing.Point(644, 36);
            this.uLabelWorkProcess.Name = "uLabelWorkProcess";
            this.uLabelWorkProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkProcess.TabIndex = 142;
            this.uLabelWorkProcess.Text = "ultraLabel2";
            // 
            // uTextEtcDesc
            // 
            appearance107.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtcDesc.Appearance = appearance107;
            this.uTextEtcDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtcDesc.Location = new System.Drawing.Point(748, 108);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.ReadOnly = true;
            this.uTextEtcDesc.Size = new System.Drawing.Size(233, 21);
            this.uTextEtcDesc.TabIndex = 141;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(644, 108);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtcDesc.TabIndex = 140;
            this.uLabelEtcDesc.Text = "ultraLabel2";
            // 
            // uLabelEquip
            // 
            this.uLabelEquip.Location = new System.Drawing.Point(644, 60);
            this.uLabelEquip.Name = "uLabelEquip";
            this.uLabelEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquip.TabIndex = 138;
            this.uLabelEquip.Text = "ultraLabel1";
            // 
            // uTextProductCode
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance36;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(380, 36);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(230, 21);
            this.uTextProductCode.TabIndex = 136;
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(276, 36);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelProduct.TabIndex = 135;
            this.uLabelProduct.Text = "ultraLabel2";
            // 
            // uTextLotProcessState
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Appearance = appearance47;
            this.uTextLotProcessState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Location = new System.Drawing.Point(116, 108);
            this.uTextLotProcessState.Name = "uTextLotProcessState";
            this.uTextLotProcessState.ReadOnly = true;
            this.uTextLotProcessState.Size = new System.Drawing.Size(150, 21);
            this.uTextLotProcessState.TabIndex = 134;
            // 
            // uLabelLotProcessState
            // 
            this.uLabelLotProcessState.Location = new System.Drawing.Point(12, 108);
            this.uLabelLotProcessState.Name = "uLabelLotProcessState";
            this.uLabelLotProcessState.Size = new System.Drawing.Size(100, 20);
            this.uLabelLotProcessState.TabIndex = 133;
            this.uLabelLotProcessState.Text = "ultraLabel2";
            // 
            // uTextMESSPCNTFlag
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESSPCNTFlag.Appearance = appearance35;
            this.uTextMESSPCNTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESSPCNTFlag.Location = new System.Drawing.Point(1016, 32);
            this.uTextMESSPCNTFlag.Name = "uTextMESSPCNTFlag";
            this.uTextMESSPCNTFlag.ReadOnly = true;
            this.uTextMESSPCNTFlag.Size = new System.Drawing.Size(20, 21);
            this.uTextMESSPCNTFlag.TabIndex = 31;
            this.uTextMESSPCNTFlag.Visible = false;
            // 
            // uTextMESHoldTFlag
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESHoldTFlag.Appearance = appearance56;
            this.uTextMESHoldTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESHoldTFlag.Location = new System.Drawing.Point(992, 32);
            this.uTextMESHoldTFlag.Name = "uTextMESHoldTFlag";
            this.uTextMESHoldTFlag.ReadOnly = true;
            this.uTextMESHoldTFlag.Size = new System.Drawing.Size(20, 21);
            this.uTextMESHoldTFlag.TabIndex = 30;
            this.uTextMESHoldTFlag.Visible = false;
            // 
            // uTextMESTrackOutTFlag
            // 
            appearance54.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackOutTFlag.Appearance = appearance54;
            this.uTextMESTrackOutTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackOutTFlag.Location = new System.Drawing.Point(1016, 8);
            this.uTextMESTrackOutTFlag.Name = "uTextMESTrackOutTFlag";
            this.uTextMESTrackOutTFlag.ReadOnly = true;
            this.uTextMESTrackOutTFlag.Size = new System.Drawing.Size(20, 21);
            this.uTextMESTrackOutTFlag.TabIndex = 29;
            this.uTextMESTrackOutTFlag.Visible = false;
            // 
            // uTextMESTrackInTFlag
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTFlag.Appearance = appearance53;
            this.uTextMESTrackInTFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESTrackInTFlag.Location = new System.Drawing.Point(992, 8);
            this.uTextMESTrackInTFlag.Name = "uTextMESTrackInTFlag";
            this.uTextMESTrackInTFlag.ReadOnly = true;
            this.uTextMESTrackInTFlag.Size = new System.Drawing.Size(20, 21);
            this.uTextMESTrackInTFlag.TabIndex = 28;
            this.uTextMESTrackInTFlag.Visible = false;
            // 
            // uTextReqSeq
            // 
            appearance87.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Appearance = appearance87;
            this.uTextReqSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Location = new System.Drawing.Point(992, 56);
            this.uTextReqSeq.Name = "uTextReqSeq";
            this.uTextReqSeq.ReadOnly = true;
            this.uTextReqSeq.Size = new System.Drawing.Size(21, 21);
            this.uTextReqSeq.TabIndex = 27;
            this.uTextReqSeq.Visible = false;
            // 
            // uTextReqNo
            // 
            appearance113.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Appearance = appearance113;
            this.uTextReqNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Location = new System.Drawing.Point(1016, 56);
            this.uTextReqNo.Name = "uTextReqNo";
            this.uTextReqNo.ReadOnly = true;
            this.uTextReqNo.Size = new System.Drawing.Size(21, 21);
            this.uTextReqNo.TabIndex = 26;
            this.uTextReqNo.Visible = false;
            // 
            // uComboProcInspectType
            // 
            this.uComboProcInspectType.Location = new System.Drawing.Point(116, 36);
            this.uComboProcInspectType.Name = "uComboProcInspectType";
            this.uComboProcInspectType.Size = new System.Drawing.Size(150, 21);
            this.uComboProcInspectType.TabIndex = 20;
            // 
            // uLabelProcInspectType
            // 
            this.uLabelProcInspectType.Location = new System.Drawing.Point(12, 36);
            this.uLabelProcInspectType.Name = "uLabelProcInspectType";
            this.uLabelProcInspectType.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcInspectType.TabIndex = 19;
            this.uLabelProcInspectType.Text = "공정검사구분";
            // 
            // uTextLotNo
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.Appearance = appearance34;
            this.uTextLotNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLotNo.Location = new System.Drawing.Point(116, 60);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.ReadOnly = true;
            this.uTextLotNo.Size = new System.Drawing.Size(150, 21);
            this.uTextLotNo.TabIndex = 13;
            this.uTextLotNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLotNo_KeyDown);
            // 
            // uTextLotSize
            // 
            appearance58.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotSize.Appearance = appearance58;
            this.uTextLotSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotSize.Location = new System.Drawing.Point(116, 84);
            this.uTextLotSize.Name = "uTextLotSize";
            this.uTextLotSize.ReadOnly = true;
            this.uTextLotSize.Size = new System.Drawing.Size(150, 21);
            this.uTextLotSize.TabIndex = 10;
            // 
            // uLabelLotSize
            // 
            this.uLabelLotSize.Location = new System.Drawing.Point(12, 84);
            this.uLabelLotSize.Name = "uLabelLotSize";
            this.uLabelLotSize.Size = new System.Drawing.Size(100, 20);
            this.uLabelLotSize.TabIndex = 9;
            this.uLabelLotSize.Text = "ultraLabel2";
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(12, 60);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelLotNo.TabIndex = 7;
            this.uLabelLotNo.Text = "ultraLabel2";
            // 
            // uComboPlantCode
            // 
            this.uComboPlantCode.Location = new System.Drawing.Point(116, 12);
            this.uComboPlantCode.Name = "uComboPlantCode";
            this.uComboPlantCode.Size = new System.Drawing.Size(150, 21);
            this.uComboPlantCode.TabIndex = 1;
            this.uComboPlantCode.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGridHeader
            // 
            this.uGridHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance88.BackColor = System.Drawing.SystemColors.Window;
            appearance88.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHeader.DisplayLayout.Appearance = appearance88;
            this.uGridHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance89.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance89.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance89.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance89.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.GroupByBox.Appearance = appearance89;
            appearance108.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance108;
            this.uGridHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance90.BackColor2 = System.Drawing.SystemColors.Control;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance90;
            this.uGridHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHeader.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHeader.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.uGridHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.CardAreaAppearance = appearance91;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHeader.DisplayLayout.Override.CellAppearance = appearance44;
            this.uGridHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHeader.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance109.TextHAlignAsString = "Left";
            this.uGridHeader.DisplayLayout.Override.HeaderAppearance = appearance109;
            this.uGridHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance110.BackColor = System.Drawing.SystemColors.Window;
            appearance110.BorderColor = System.Drawing.Color.Silver;
            this.uGridHeader.DisplayLayout.Override.RowAppearance = appearance110;
            this.uGridHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.uGridHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHeader.Location = new System.Drawing.Point(0, 130);
            this.uGridHeader.Name = "uGridHeader";
            this.uGridHeader.Size = new System.Drawing.Size(1060, 690);
            this.uGridHeader.TabIndex = 7;
            this.uGridHeader.Text = "ultraGrid1";
            this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridHeader_DoubleClickRow);
            // 
            // uTextOUTSOURCINGVENDOR
            // 
            this.uTextOUTSOURCINGVENDOR.Location = new System.Drawing.Point(1016, 84);
            this.uTextOUTSOURCINGVENDOR.Name = "uTextOUTSOURCINGVENDOR";
            this.uTextOUTSOURCINGVENDOR.Size = new System.Drawing.Size(20, 21);
            this.uTextOUTSOURCINGVENDOR.TabIndex = 167;
            this.uTextOUTSOURCINGVENDOR.Visible = false;
            // 
            // uLabelOUTSOURCINGVENDOR
            // 
            this.uLabelOUTSOURCINGVENDOR.Location = new System.Drawing.Point(992, 84);
            this.uLabelOUTSOURCINGVENDOR.Name = "uLabelOUTSOURCINGVENDOR";
            this.uLabelOUTSOURCINGVENDOR.Size = new System.Drawing.Size(20, 20);
            this.uLabelOUTSOURCINGVENDOR.TabIndex = 166;
            this.uLabelOUTSOURCINGVENDOR.Text = "ultraLabel2";
            this.uLabelOUTSOURCINGVENDOR.Visible = false;
            // 
            // frmINS0008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGridHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINS0008";
            this.Load += new System.EventHandler(this.frmINS0008_Load);
            this.Activated += new System.EventHandler(this.frmINS0008_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINS0008_FormClosing);
            this.Resize += new System.EventHandler(this.frmINS0008_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridItem)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSampling)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFaultData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDetailProcType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uChartX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStackSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionPassFailFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLossCheckFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLossQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGeneration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcessHoldState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNowProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkProcessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESSPCNTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESHoldTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackOutTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTrackInTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextOUTSOURCINGVENDOR)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinChart.UltraChart uChartX;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridItem;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSampling;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFaultData;
        private Infragistics.Win.Misc.UltraButton uButtonDelete2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFault;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionPassFailFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelPassFailFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelProcInspectType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotSize;
        private Infragistics.Win.Misc.UltraLabel uLabelLotSize;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHeader;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqNo;
        private Infragistics.Win.Misc.UltraButton uButtonTrackIn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESSPCNTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESHoldTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTrackOutTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTrackInTFlag;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateInspectTime;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCompleteCheck;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTrackInTCheck;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToInspectDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromInspectDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStackSeq;
        private Infragistics.Win.Misc.UltraLabel uLabelStackSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotProcessState;
        private Infragistics.Win.Misc.UltraLabel uLabelLotProcessState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcessHoldState;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessHoldState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.Misc.UltraLabel uLabelNowProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelWokrUser;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProductCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextNowProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextNowProcessName;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkProcessName;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDetailProcType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDetailProcType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerProductSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomerProductSpec;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboSearchProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInsType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGeneration;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLossCheckFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLossQty;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextOUTSOURCINGVENDOR;
        private Infragistics.Win.Misc.UltraLabel uLabelOUTSOURCINGVENDOR;
    }
}