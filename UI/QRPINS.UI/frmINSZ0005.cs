﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질검사관리                                          */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINSZ0005.cs                                        */
/* 프로그램명   : AML정보                                               */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-13                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-09 : 기능구현 추가 (이종호)                   */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0005 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmINSZ0005()
        {
            InitializeComponent();
        }

        private void frmINSZ0005_Activated(object sender, EventArgs e)
        {
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //툴바설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmINSZ0005_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;

            //컨트롤초기화
            SetToolAuth();
            InitText();
            InitLabel();
            InitGrid();
            InitGroupBox();
            InitComboBox();
            InitBuuton();
            uGroupBoxContentsArea.Expanded = false;

            // 그리드 정보 Load
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 기본값 설정
        /// </summary>
        private void InitValue()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //기본값설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uCheckHistoryInfo.Checked = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //타이틀 설정
                titleArea.mfSetLabelText("AML 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

                // MAXLength 설정
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextVendorCode.MaxLength = 10;
                this.uTextMaterialCode.MaxLength = 20;
                this.uTextWriteUserID.MaxLength = 20;
                this.uTextEtcDesc.MaxLength = 100;
                this.uTextCustomerCode.MaxLength = 10;
                this.uTextMoldSeq.MaxLength = 4;
                this.uTextSpec.MaxLength = 200;
                this.uTextSpecNo.MaxLength = 200;

                this.uDateCustomerAdmitDate.MaskInput = "yyyy-MM-dd";

                this.uTextCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextCustomerCode_EditorButtonClick);
                this.uTextMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextMaterialCode_EditorButtonClick);
                this.uTextMaterialCode.KeyDown += new KeyEventHandler(uTextMaterialCode_KeyDown);
                this.uTextMoldSeq.AfterExitEditMode += new EventHandler(uTextMoldSeq_AfterExitEditMode);
                this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSearchVendorCode_EditorButtonClick);
                this.uTextSearchVendorCode.KeyDown += new KeyEventHandler(uTextSearchVendorCode_KeyDown);
                this.uTextVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextVendorCode_EditorButtonClick);
                this.uTextVendorCode.KeyDown += new KeyEventHandler(uTextVendorCode_KeyDown);
                this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextWriteUserID_EditorButtonClick);
                this.uTextWriteUserID.KeyDown += new KeyEventHandler(uTextWriteUserID_KeyDown);

                this.uNumShipCount.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uNumShipCount_EditorButtonClick);
                this.uNumShipCount.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(uNumShipCount_EditorSpinButtonClick);

                this.uTextVendorCode.ValueChanged += new System.EventHandler(this.uTextVendorCode_ValueChanged);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System resourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelSearchMaterialGrade, "자재등급", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelVendor, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMoldSeq, "금형차수", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelDrawing_No, "도면No", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpecNo, "Rev", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerAdmitDate, "고객 승인일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteDate, "최종등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelWriteUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterialGrade, "자재등급", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectLevel, "검사수준", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectRate, "검사비율", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAQLFlag, "AQL 적용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelHistoryInfo, "이력정보추가", m_resSys.GetString("SYS_FONTNAME"), true,false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        private void InitBuuton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton button = new WinButton();

                button.mfSetButton(uButtonDel, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                this.uButtonDel.Hide();

                this.uButtonDel.Click += new EventHandler(uButtonDel_Click);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System resourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본설정
                //--정보
                grd.mfInitGeneralGrid(this.uGridAMLList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--정보
                grd.mfSetGridColumn(this.uGridAMLList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "VendorCode", "거래처Code", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "ConsumableTypeCode", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "MoldSeq", "금형차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 4
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "MaterialGrade", "자재등급", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "SpecNo", "Rev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "CustomerAdmitDate", "고객승인일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                     , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "WriteDate", "동록일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "WriteUserName", "등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--AML 이력정보
                grd.mfInitGeneralGrid(this.uGridAMLHistoryList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--AML 이력정보
                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "HistorySeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "SpecNo", "Rev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "MaterialGrade", "자재등급", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", DateTime.Now.ToString("yyyy-MM-dd"));

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "InspectDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "InspectResult", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //History삭제를 위한 파라미터
                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "ShipmentCount", "ShipmentCount", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnnnnn", "0");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "MoldSeq", "MoldSeq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "CreateDate", "최초등록일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "CreateUserName", "최초등록자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "ModifyDate", "최종수정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "ModifyUserName", "최종수정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridAMLHistoryList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridAMLList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridAMLList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridAMLHistoryList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridAMLHistoryList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 상세그리드 헤데 정렬 방지
                this.uGridAMLHistoryList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;

                this.uGridAMLList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridAMLList_DoubleClickRow);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System resourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.ultraGroupBox1, GroupBoxType.LIST, "AML이력정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                ultraGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                ultraGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.ultraGroupBox3.Hide();

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant plant = new QRPMAS.BL.MASPRC.Plant();
                brwChnnel.mfCredentials(plant);

                DataTable dtPlant = plant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCombo = new WinComboEditor();

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // 자재등급
                brwChnnel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChnnel.mfCredentials(clsComCode);

                DataTable dtCom = clsComCode.mfReadCommonCode("C0013", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchMaterialGrade, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ComCode", "ComCodeName", dtCom);

                // 신제품 항목 삭제
                DataRow[] _dr = dtCom.Select("ComCode <> '1'");
                DataTable dtMaterialGrade = dtCom.Clone();
                foreach (DataRow dr in _dr)
                    dtMaterialGrade.ImportRow(dr);

                wCombo.mfSetComboEditor(this.uComboMaterialGrade, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtMaterialGrade);

                // 검사수준
                dtCom = clsComCode.mfReadCommonCode("C0033", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboInspectLevel, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCom);

                // 검사비율
                dtCom = clsComCode.mfReadCommonCode("C0038", m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboInspectRate, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCom);

                // 검색조건 : 자재종류
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsum = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChnnel.mfCredentials(clsConsum);
                DataTable dtConsum = clsConsum.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchMaterialType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ConsumableTypeCode", "ConsumableTypeName", dtConsum);

                this.uComboPlant.ValueChanged += new EventHandler(uComboPlant_ValueChanged);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건용 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strVendorCode = this.uTextSearchVendorCode.Text;
                string strMaterialGrade = this.uComboSearchMaterialGrade.Value.ToString();
                string strFromWriteDate = Convert.ToDateTime(this.uDateSearchFromWriteDate.Value).ToString("yyyy-MM-dd");
                string strToWriteDate = Convert.ToDateTime(this.uDateSearchToWriteDate.Value).ToString("yyyy-MM-dd");
                string strMaterialCode = this.uTextSearchMaterialCode.Text;
                string strMaterialTypeCode = this.uComboSearchMaterialType.Value.ToString();

                // BL연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AML), "AML");
                QRPINS.BL.INSIMP.AML clsAML = new QRPINS.BL.INSIMP.AML();
                brwChannel.mfCredentials(clsAML);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                DataTable dtRtn = clsAML.mfReadINSAML(strPlantCode, strVendorCode, strMaterialGrade, strFromWriteDate, strToWriteDate, strMaterialCode, strMaterialTypeCode, m_resSys.GetString("SYS_LANG"));

                // Binding
                this.uGridAMLList.DataSource = dtRtn;
                this.uGridAMLList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtRtn.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001099", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridAMLList, 0);
                }

                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                //if (this.uComboPlant.Value.ToString() == "" || this.uTextMaterialCode.Text == "" || this.uTextVendorCode.Text == "")
                if(this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001011", "M000395",
                                Infragistics.Win.HAlign.Right);
                    //mfSearch();
                }
                else if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000266",
                                Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextMaterialCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000965",
                                Infragistics.Win.HAlign.Right);
                    
                    this.uTextMaterialCode.Focus();
                    return;
                }
                else if (this.uTextVendorCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000166",
                                Infragistics.Win.HAlign.Right);
                    this.uTextVendorCode.Focus();
                    return;
                }
                else if (this.uTextMoldSeq.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000338",
                                Infragistics.Win.HAlign.Right);
                    this.uTextMoldSeq.Focus();
                    return;
                }
                else if (this.uTextSpecNo.Text.Equals(string.Empty)) //RevNo 공백인경우 메세지 출력
                {
                    //Rev을 입력해 주세요
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M000114",
                                Infragistics.Win.HAlign.Right);
                    this.uTextSpecNo.Focus();
                    return;
                }
                else if (this.uCheckHistoryInfo.Checked
                        && (this.uNumShipCount.Value == null 
                            || this.uNumShipCount.Value.ToString().Equals("0") 
                            || this.uNumShipCount.Value.ToString().Equals(string.Empty))
                        ) //이력정보추가 체크시 ShipCount 여부확인 
                {
                    //ShipCount를 입력해주세요.
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M001515",
                                Infragistics.Win.HAlign.Right);
                    this.uNumShipCount.Focus();
                    return;
                }
                else if (!this.uCheckHistoryInfo.Checked
                        && this.uNumShipCount.Value != null
                        && !this.uNumShipCount.Value.ToString().Equals("0")
                        && !this.uNumShipCount.Value.ToString().Equals(string.Empty)
                        ) //ShipCount 입력되었을 경우 이력정보추가 체크여부확인 
                {
                    //이력정보추가를 체크해주세요.
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001015", "M001516",
                                Infragistics.Win.HAlign.Right);
                    this.uCheckHistoryInfo.Focus();
                    return;
                }
                else
                {
                    //콤보박스 선택값 Validation Check//////////
                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this)) return;
                    ///////////////////////////////////////////

                    // 저장여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 헤더정보 데이터 테이블에 저장하는 Method 호출
                        DataTable dtSaveHeader = Rtn_dtHeader();
                        DataTable dtDel = dtDelHistory();

                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AML), "AML");
                        QRPINS.BL.INSIMP.AML clsAML = new QRPINS.BL.INSIMP.AML();
                        brwChannel.mfCredentials(clsAML);

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsAML.mfSaveINSAML(dtSaveHeader, dtDel, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {
                // Systems ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                //if (this.uComboPlant.Value.ToString() == "" || this.uTextMaterialCode.Text == "" || this.uTextVendorCode.Text == "")
                if (this.uGroupBoxContentsArea.Expanded.Equals(false))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000625", "M000396",
                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
                else if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000629", "M000266",
                                Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextMaterialCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000629", "M000965",
                                Infragistics.Win.HAlign.Right);

                    this.uTextMaterialCode.Focus();
                    return;
                }
                else if (this.uTextVendorCode.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000629", "M000166",
                                Infragistics.Win.HAlign.Right);
                    this.uTextVendorCode.Focus();
                    return;
                }
                else if (this.uTextMoldSeq.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M000629", "M000338",
                                Infragistics.Win.HAlign.Right);
                    this.uTextMoldSeq.Focus();
                    return;
                }
                else
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AML), "AML");
                    QRPINS.BL.INSIMP.AML clsAML = new QRPINS.BL.INSIMP.AML();
                    brwChannel.mfCredentials(clsAML);

                    DataTable dtDelete = clsAML.mfSetDataInfo();

                    DataRow drRow = dtDelete.NewRow();
                    drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                    drRow["VendorCode"] = this.uTextVendorCode.Text;
                    drRow["MaterialCode"] = this.uTextMaterialCode.Text;
                    drRow["MoldSeq"] = this.uTextMoldSeq.Text;
                    dtDelete.Rows.Add(drRow);

                    dtDelete = dtDelete.DefaultView.ToTable(true, "PlantCode", "VendorCode", "MaterialCode", "MoldSeq");

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000650", "M000675",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 삭제 메소드 호출
                        string strErrRtn = clsAML.mfDeleteINSAML(dtDelete);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000677",
                                                Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            this.uCheckHistoryInfo.Checked = false;
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000676",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {
                
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridAMLList.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridAMLList);
                }
                else
                {
                    // Systems ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M000799", "M000146", "M000802",
                                                Infragistics.Win.HAlign.Right);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// 헤더 상세 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <returns></returns>
        private bool Search_HeaderDetail(string strPlantCode, string strVendorCode, string strMaterialCode, string strMoldSeq)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AML), "AML");
                QRPINS.BL.INSIMP.AML clsAML = new QRPINS.BL.INSIMP.AML();
                brwChannel.mfCredentials(clsAML);

                DataTable dtRtn = clsAML.mfReadINSAML_Detail(strPlantCode, strVendorCode, strMaterialCode, strMoldSeq, m_resSys.GetString("SYS_LANG"));

                if (dtRtn.Rows.Count > 0)
                {
                    this.uComboPlant.Value = dtRtn.Rows[0]["PlantCode"].ToString();
                    this.uTextVendorCode.Text = dtRtn.Rows[0]["VendorCode"].ToString();
                    this.uTextVendorName.Text = dtRtn.Rows[0]["VendorName"].ToString();
                    this.uComboMaterialGrade.Value = dtRtn.Rows[0]["MaterialGrade"].ToString();
                    this.uTextMaterialCode.Text = dtRtn.Rows[0]["MaterialCode"].ToString();
                    this.uTextMaterialName.Text = dtRtn.Rows[0]["MaterialName"].ToString();
                    //this.uTextMaterialName.Text = dtRtn.Rows[0]["CUSTOMERPRODUCTSPEC"].ToString();
                    this.uTextMoldSeq.Text = dtRtn.Rows[0]["MoldSeq"].ToString();
                    this.uTextSpec.Text = dtRtn.Rows[0]["Spec"].ToString();
                    this.uTextSpecNo.Text = dtRtn.Rows[0]["SpecNo"].ToString();
                    this.uTextDrawing_No.Text = dtRtn.Rows[0]["DRAWING_NO"].ToString();
                    //this.uTextWriteUserID.Text = dtRtn.Rows[0]["WriteUserID"].ToString();
                    //this.uTextWriteUserName.Text = dtRtn.Rows[0]["WriteUserName"].ToString();
                    this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                    this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");                    
                    //if(!dtRtn.Rows[0]["WriteDate"].Equals(null) && !dtRtn.Rows[0]["WriteDate"].Equals(DBNull.Value) && !dtRtn.Rows[0]["WriteDate"].ToString().Equals(string.Empty))
                    //    this.uDateWriteDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["WriteDate"]).ToString("yyyy-MM-dd");
                    this.uDateWriteDate.Value = DateTime.Now;
                    //if (!dtRtn.Rows[0]["CustomerAdmitDate"].Equals(null) && !dtRtn.Rows[0]["CustomerAdmitDate"].Equals(DBNull.Value) && !dtRtn.Rows[0]["CustomerAdmitDate"].ToString().Equals(string.Empty))
                    //    this.uDateCustomerAdmitDate.Value = Convert.ToDateTime(dtRtn.Rows[0]["CustomerAdmitDate"]).ToString("yyyy-MM-dd");
                    this.uDateCustomerAdmitDate.Value = DateTime.Now;
                    this.uTextCustomerCode.Text = dtRtn.Rows[0]["CustomerCode"].ToString();
                    this.uTextCustomerName.Text = dtRtn.Rows[0]["CustomerName"].ToString();
                    this.uComboInspectLevel.Value = dtRtn.Rows[0]["InspectLevel"].ToString();
                    this.uComboInspectRate.Value = dtRtn.Rows[0]["InspectRate"].ToString();
                    this.uTextEtcDesc.Text = dtRtn.Rows[0]["EtcDesc"].ToString();
                    this.uCheckAQLFlag.Checked = Convert.ToBoolean(dtRtn.Rows[0]["AQLFlag"]);
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// AML 이력정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        private void Search_AMLHistory(string strPlantCode, string strVendorCode, string strMaterialCode, string strMoldSeq)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AMLHistory), "AMLHistory");
                QRPINS.BL.INSIMP.AMLHistory clsHistory = new QRPINS.BL.INSIMP.AMLHistory();
                brwChannel.mfCredentials(clsHistory);

                DataTable dtRtn = clsHistory.mfReadAMLHistory(strPlantCode, strMaterialCode, strVendorCode, strMoldSeq, m_resSys.GetString("SYS_LANG"));

                this.uGridAMLHistoryList.DataSource = dtRtn;
                this.uGridAMLHistoryList.DataBind();

                if (dtRtn.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridAMLHistoryList, 0);

                    for (int i = 0; i < this.uGridAMLHistoryList.Rows.Count; i++)
                    {
                        this.uGridAMLHistoryList.Rows[i].Cells["HistorySeq"].Value = this.uGridAMLHistoryList.Rows[i].RowSelectorNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 데이터테이블에 저장하여 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_dtHeader()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AML), "AML");
                QRPINS.BL.INSIMP.AML clsAML = new QRPINS.BL.INSIMP.AML();
                brwChannel.mfCredentials(clsAML);

                dtRtn = clsAML.mfSetDataInfo();
                DataRow drRow = dtRtn.NewRow();
                drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                drRow["VendorCode"] = this.uTextVendorCode.Text;
                drRow["MaterialCode"] = this.uTextMaterialCode.Text;
                drRow["MoldSeq"] = this.uTextMoldSeq.Text;
                drRow["Spec"] = this.uTextSpec.Text;
                drRow["SpecNo"] = this.uTextSpecNo.Text;
                drRow["CustomerCode"] = this.uTextCustomerCode.Text;
                drRow["CustomerAdmitDate"] = Convert.ToDateTime(this.uDateCustomerAdmitDate.Value).ToString("yyyy-MM-dd");
                drRow["WriteUserID"] = this.uTextWriteUserID.Text;
                drRow["WriteDate"] = Convert.ToDateTime(this.uDateWriteDate.Value).ToString("yyyy-MM-dd");
                drRow["MaterialGrade"] = this.uComboMaterialGrade.Value.ToString();
                drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                drRow["InspectLevel"] = this.uComboInspectLevel.Value.ToString();
                drRow["InspectRate"] = this.uComboInspectRate.Value.ToString();
                if (this.uCheckAQLFlag.Checked)
                    drRow["AQLFlag"] = "T";
                else
                    drRow["AQLFLag"] = "F";
                if (this.uCheckHistoryInfo.Checked)
                    drRow["HistoryInfo"] = "T";
                else
                    drRow["HistoryInfo"] = "F";
                drRow["ShipCount"] = this.uNumShipCount.Value;
                dtRtn.Rows.Add(drRow);
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }
        //AMLHistory삭제 테이블
        private DataTable dtDelHistory()
        {
            DataTable dtRtnDel = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AMLHistory), "AMLHistory");
                QRPINS.BL.INSIMP.AMLHistory clsHistory = new QRPINS.BL.INSIMP.AMLHistory();
                brwChannel.mfCredentials(clsHistory);

                dtRtnDel = clsHistory.mfSetDataInfo();
                
                for (int i = 0; i < this.uGridAMLHistoryList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridAMLHistoryList.Rows[i].Cells["Check"].Value) == true)
                    {
                        DataRow dr = dtRtnDel.NewRow();
                        dr["PlantCode"] = this.uGridAMLHistoryList.Rows[i].Cells["PlantCode"].Value.ToString();
                        dr["VendorCode"] = this.uGridAMLHistoryList.Rows[i].Cells["VendorCode"].Value.ToString();
                        dr["MaterialCode"] = this.uGridAMLHistoryList.Rows[i].Cells["MaterialCode"].Value.ToString();
                        dr["MoldSeq"] = this.uGridAMLHistoryList.Rows[i].Cells["MoldSeq"].Value.ToString();
                        dr["Seq"] = Convert.ToInt32(this.uGridAMLHistoryList.Rows[i].Cells["Seq"].Value.ToString());

                        dtRtnDel.Rows.Add(dr);
                    }
                }
                return dtRtnDel;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtnDel;
            }
            finally
            { }
        }

        /// <summary>
        /// DB 에 이미 저장된 데이터가 있는지 조회
        /// </summary>
        private void CheckData()
        {
            try
            {
                if (this.uComboPlant.Value.ToString() != "" && this.uTextVendorCode.Text != "" && this.uTextMaterialCode.Text != "" && this.uTextMoldSeq.Text != "")
                {
                    // SystemInfo ReSourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (Search_HeaderDetail(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text, this.uTextMaterialCode.Text, this.uTextMoldSeq.Text))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000041", "M000845",
                                            Infragistics.Win.HAlign.Right);

                        // 프로그래스바 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // AML 이력정보 조회 Method 호출
                        // 헤더정보 Display
                        Search_HeaderDetail(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text, this.uTextMaterialCode.Text, this.uTextMoldSeq.Text);

                        // History 정보 Display
                        Search_AMLHistory(this.uComboPlant.Value.ToString(), this.uTextVendorCode.Text, this.uTextMaterialCode.Text, this.uTextMoldSeq.Text);
                         
                        // POPUP창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextVendorCode.Text = "";
                this.uTextVendorName.Text = "";
                this.uTextMoldSeq.Text = "0";
                this.uComboMaterialGrade.Value = "";
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialName.Text = "";
                this.uTextSpec.Text = "";
                this.uTextSpecNo.Text = "000";
                this.uTextDrawing_No.Clear();
                this.uDateWriteDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextCustomerCode.Text = "";
                this.uTextCustomerName.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uComboInspectLevel.Value = "";
                this.uComboInspectRate.Value = "";
                this.uDateCustomerAdmitDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uCheckAQLFlag.Checked = false;
                this.uCheckHistoryInfo.Checked = false;
                this.uNumShipCount.Value = 0;
                while (this.uGridAMLHistoryList.Rows.Count > 0)
                {
                    this.uGridAMLHistoryList.Rows[0].Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 거래처 정보 조회 함수
        /// </summary>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        private DataTable GetVendorInfo(String strVendorCode)
        {
            DataTable dtVendor = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

                return dtVendor;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtVendor;
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트

        #region 기타 컨트롤 이벤트
        // ContentsArea 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                ////if (uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    Point point = new Point(0, 145);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridAMLList.Height = 60;
                ////}
                ////else
                ////{
                ////    Point point = new Point(0, 825);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridAMLList.Height = 740;

                ////    for (int i = 0; i < uGridAMLList.Rows.Count; i++)
                ////    {
                ////        uGridAMLList.Rows[i].Fixed = false;
                ////    }

                ////    // 컨트롤 Clear
                ////    Clear();
                ////}
                if (this.uGroupBoxContentsArea.Expanded)
                {
                    this.uGridAMLList.Rows.FixedRows.Clear();
                    Clear();
                }
            }
            catch(Exception ex) 
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            {
            }
        }

        // 공장코드값이 변경되었을때
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // DB에 저장된 데이터가 있는지 조회
                CheckData();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // AMLList 더블클릭시 상세데이터 조회
        private void uGridAMLList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ReSourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                // 현재 클릭된 행 고정
                e.Row.Fixed = true;

                // ContentsArea 펼침상태로
                this.uGroupBoxContentsArea.Expanded = true;

                // 검색조건용 변수 설정
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strVendorCode = e.Row.Cells["VendorCode"].Value.ToString();
                string strMaterialCode = e.Row.Cells["MaterialCode"].Value.ToString();
                string strMoldSeq = e.Row.Cells["MoldSeq"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                if (Search_HeaderDetail(strPlantCode, strVendorCode, strMaterialCode, strMoldSeq))
                {
                    // 이력정보 조회
                    Search_AMLHistory(strPlantCode, strVendorCode, strMaterialCode, strMoldSeq);
                }
                else
                {
                    //DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                                            "오류창", "데이터조회 오류", "데이터를 조회하는중 오류가 발생했습니다",
                    //                                            Infragistics.Win.HAlign.Right);

                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                "M000229", "M000378", "M000029",
                                                                Infragistics.Win.HAlign.Right);

                    // ContentsArea 접은상태로
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINSZ0005_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 팝업창
        // 검색조건 거래처 팝업창
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 입력 거래처 팝업창
        private void uTextVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0020 frmPOP = new frmPOP0020();
                frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                if (frmPOP.VendorCode != null && frmPOP.VendorName != null)
                {
                    this.uTextVendorCode.Text = frmPOP.VendorCode;
                    this.uTextVendorName.Text = frmPOP.VendorName;

                    CheckData();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자재 팝업창
        private void uTextMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextMaterialName.Text = frmPOP.MaterialName;
                this.uTextSpec.Text = frmPOP.Spec;
                this.uComboPlant.Value = frmPOP.PlantCode;

                // DB에 저장된 데이터가 있는지 조회
                CheckData();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 등록자 팝업창
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uComboPlant.Value.ToString() != "")
                {
                    frmPOP0011 frmPOP = new frmPOP0011();
                    frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                    frmPOP.ShowDialog();

                    if (this.uComboPlant.Value.ToString() != frmPOP.PlantCode)
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();
                        string strLang = m_resSys.GetString("SYS_LANG");
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , msg.GetMessge_Text("M000798", strLang), msg.GetMessge_Text("M000275", strLang)
                                                    , msg.GetMessge_Text("M001254", strLang) + this.uComboPlant.Text + msg.GetMessge_Text("M000001", strLang)
                                                    , Infragistics.Win.HAlign.Right);

                        this.uTextWriteUserID.Text = "";
                        this.uTextWriteUserName.Text = "";
                    }
                    else
                    {
                        this.uTextWriteUserID.Text = frmPOP.UserID;
                        this.uTextWriteUserName.Text = frmPOP.UserName;
                    }
                }
                else
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        // 고객 팝업창
        private void uTextCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();
                frmPOP.ShowDialog();

                this.uTextCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextCustomerName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 키다운
        // 자재코드 키다운 이벤트
        private void uTextMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    // SystemInfo ReSourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (this.uComboPlant.Value.ToString().Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001264", "M000263", "M000266"
                                                                , Infragistics.Win.HAlign.Center);

                        this.uComboPlant.DropDown();
                        return;
                    }
                    else
                    {
                        if (!this.uTextMaterialCode.Text.Equals(string.Empty))
                        {
                            string strPlantCode = this.uComboPlant.Value.ToString();
                            string strMaterialCode = this.uTextMaterialCode.Text;

                            DataTable dtMat = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMat.Rows.Count > 0)
                            {
                                this.uTextMaterialName.Text = dtMat.Rows[0]["MaterialName"].ToString();
                                //this.uTextMaterialName.Text = dtMat.Rows[0]["CustomerProductSpec"].ToString();
                                this.uTextSpec.Text = dtMat.Rows[0]["Spec"].ToString();
                                
                                CheckData();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001099", "M000970", "M000971"
                                                                , Infragistics.Win.HAlign.Center);

                                this.uTextMaterialCode.Clear();
                                this.uTextMaterialName.Clear();
                                this.uTextSpec.Clear();
                                return;
                            }
                        }
                    }
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextMaterialCode.TextLength <= 1 || this.uTextMaterialCode.SelectedText == this.uTextMaterialCode.Text)
                    {
                        this.uTextMaterialName.Clear();
                        this.uTextSpec.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 작성자 키다운 이벤트
        private void uTextWriteUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextWriteUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(this.uComboPlant.Value.ToString(), strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextWriteUserID.Text = "";
                            this.uTextWriteUserName.Text = "";
                        }
                        else
                        {
                            this.uTextWriteUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteUserID.TextLength <= 1 || this.uTextWriteUserID.SelectedText == this.uTextWriteUserID.Text)
                    {
                        this.uTextWriteUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uTextMoldSeq_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                CheckData();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // Numeric에디터 0버튼 클릭시 이벤트
        private void uNumShipCount_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Numeric 에디터 스핀버튼 이벤트
        private void uNumShipCount_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    this.uNumShipCount.Focus();
                    this.uNumShipCount.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    this.uNumShipCount.Focus();
                    this.uNumShipCount.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// 행삭제 버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridAMLHistoryList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridAMLHistoryList.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridAMLHistoryList.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextVendorCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    string strVendorCode = this.uTextVendorCode.Text;
                    DataTable dtVendor = GetVendorInfo(strVendorCode);
                    if (dtVendor.Rows.Count > 0)
                    {
                        this.uTextVendorName.Text = dtVendor.Rows[0]["VendorName"].ToString();
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult result = new DialogResult();
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001113", "M000168", Infragistics.Win.HAlign.Right);
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    this.uTextVendorCode.Clear();
                    this.uTextVendorName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchVendorCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    string strVendorCode = this.uTextSearchVendorCode.Text;
                    DataTable dtVendor = GetVendorInfo(strVendorCode);
                    if (dtVendor.Rows.Count > 0)
                    {
                        this.uTextSearchVendorName.Text = dtVendor.Rows[0]["VendorName"].ToString();
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult result = new DialogResult();
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001113", "M000168", Infragistics.Win.HAlign.Right);
                    }
                }
                else if (e.KeyCode == Keys.Back)
                {
                    this.uTextSearchVendorCode.Clear();
                    this.uTextSearchVendorName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextVendorCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextVendorName.Text.Equals(string.Empty))
                this.uTextVendorName.Clear();
        }
    }
}
