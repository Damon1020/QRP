﻿namespace QRPINS.UI
{
    partial class frmINSZ0010_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmINSZ0010_S));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton6 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton3 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton7 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton4 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton8 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton5 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton9 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton6 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton10 = new Infragistics.Win.UltraWinEditors.EditorButton("UP");
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton11 = new Infragistics.Win.UltraWinEditors.EditorButton("DOWN");
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton12 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton7 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton13 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton8 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton14 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton9 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSerachAriseDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchAriseDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchAriseDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchQCNNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchQCNNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcessCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridAffectLotList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInspectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInspectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWorkUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkUser = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckActionFlag9 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag8 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag7 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag6 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag5 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag4 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag3 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag2 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckActionFlag1 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGroupQCNUser = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridQCNUser = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uComboDept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelDept = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxFaulty = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridFaultType = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRowFaulty = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBoxStdInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSYSTEM = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSYSTEM = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProgram = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProgram = new Infragistics.Win.Misc.UltraLabel();
            this.uComboHandler = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelHandler = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uNumRetestFaultQty3 = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumRetestInspectQty3 = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelRetest3 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uNumRetestFaultQty2 = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumRetestInspectQty2 = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelRetest2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uNumRetestFaultQty1 = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumRetestInspectQty1 = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelRetest1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboP_QcnItr = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextFaultImg = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFaultImg = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridExpectEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqSeq = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextReqNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLotHoldState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLotProcessState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboAriseEquipCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uComboExpectProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uComboAriseProcessCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPublishComplete = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMESTFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerProductSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckPublishFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelPublishFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMESWorkUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uDateAriseTime = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uNumFaultQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumInspectQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uNumQty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uCheckWorkStepEquipFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckWorkStepMaterialFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelWorkStep = new Infragistics.Win.Misc.UltraLabel();
            this.uComboInspectFaultTypeCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInspectFaultType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectQty = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelQty = new Infragistics.Win.Misc.UltraLabel();
            this.uDateAriseDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAriseDate = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelExpectProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAriseEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAriseProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomerName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uTextQCNNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelQCNNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlantCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uGridQCNList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSerachAriseDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchAriseDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchQCNNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).BeginInit();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridAffectLotList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupQCNUser)).BeginInit();
            this.uGroupQCNUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFaulty)).BeginInit();
            this.uGroupBoxFaulty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFaultType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).BeginInit();
            this.uGroupBoxStdInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSYSTEM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHandler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestFaultQty3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestInspectQty3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestFaultQty2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestInspectQty2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestFaultQty1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestInspectQty1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboP_QcnItr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExpectEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotHoldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboExpectProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcessCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPublishComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckPublishFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESWorkUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumFaultQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInspectQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepEquipFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepMaterialFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFaultTypeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQCNNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNList)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSerachAriseDateTo);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchAriseDateFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchAriseDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchQCNNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchQCNNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchProductCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProduct);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcessCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlantCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 4;
            this.uGroupBoxSearchArea.Visible = false;
            // 
            // uDateSerachAriseDateTo
            // 
            this.uDateSerachAriseDateTo.DateTime = new System.DateTime(2012, 10, 8, 0, 0, 0, 0);
            this.uDateSerachAriseDateTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSerachAriseDateTo.Location = new System.Drawing.Point(808, 36);
            this.uDateSerachAriseDateTo.Name = "uDateSerachAriseDateTo";
            this.uDateSerachAriseDateTo.Size = new System.Drawing.Size(100, 21);
            this.uDateSerachAriseDateTo.TabIndex = 55;
            this.uDateSerachAriseDateTo.Value = new System.DateTime(2012, 10, 8, 0, 0, 0, 0);
            // 
            // ultraLabel2
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(792, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel2.TabIndex = 54;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchAriseDateFrom
            // 
            this.uDateSearchAriseDateFrom.DateTime = new System.DateTime(2012, 10, 8, 0, 0, 0, 0);
            this.uDateSearchAriseDateFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchAriseDateFrom.Location = new System.Drawing.Point(692, 36);
            this.uDateSearchAriseDateFrom.Name = "uDateSearchAriseDateFrom";
            this.uDateSearchAriseDateFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchAriseDateFrom.TabIndex = 53;
            this.uDateSearchAriseDateFrom.Value = new System.DateTime(2012, 10, 8, 0, 0, 0, 0);
            // 
            // uLabelSearchAriseDate
            // 
            this.uLabelSearchAriseDate.Location = new System.Drawing.Point(588, 36);
            this.uLabelSearchAriseDate.Name = "uLabelSearchAriseDate";
            this.uLabelSearchAriseDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchAriseDate.TabIndex = 52;
            this.uLabelSearchAriseDate.Text = "ultraLabel1";
            // 
            // uTextSearchLotNo
            // 
            this.uTextSearchLotNo.Location = new System.Drawing.Point(380, 36);
            this.uTextSearchLotNo.Name = "uTextSearchLotNo";
            this.uTextSearchLotNo.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchLotNo.TabIndex = 51;
            // 
            // uTextSearchQCNNo
            // 
            this.uTextSearchQCNNo.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchQCNNo.Name = "uTextSearchQCNNo";
            this.uTextSearchQCNNo.Size = new System.Drawing.Size(200, 21);
            this.uTextSearchQCNNo.TabIndex = 50;
            // 
            // uLabelSearchQCNNo
            // 
            this.uLabelSearchQCNNo.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchQCNNo.Name = "uLabelSearchQCNNo";
            this.uLabelSearchQCNNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchQCNNo.TabIndex = 49;
            this.uLabelSearchQCNNo.Text = "ultraLabel1";
            // 
            // uLabelSearchLotNo
            // 
            this.uLabelSearchLotNo.Location = new System.Drawing.Point(276, 36);
            this.uLabelSearchLotNo.Name = "uLabelSearchLotNo";
            this.uLabelSearchLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLotNo.TabIndex = 48;
            this.uLabelSearchLotNo.Text = "ultraLabel1";
            // 
            // uTextSearchProductName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Appearance = appearance3;
            this.uTextSearchProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchProductName.Location = new System.Drawing.Point(794, 12);
            this.uTextSearchProductName.Name = "uTextSearchProductName";
            this.uTextSearchProductName.ReadOnly = true;
            this.uTextSearchProductName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchProductName.TabIndex = 47;
            // 
            // uTextSearchProductCode
            // 
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchProductCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchProductCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchProductCode.Location = new System.Drawing.Point(692, 12);
            this.uTextSearchProductCode.Name = "uTextSearchProductCode";
            this.uTextSearchProductCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchProductCode.TabIndex = 46;
            this.uTextSearchProductCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchProductCode_EditorButtonClick);
            // 
            // uLabelSearchProduct
            // 
            this.uLabelSearchProduct.Location = new System.Drawing.Point(588, 12);
            this.uLabelSearchProduct.Name = "uLabelSearchProduct";
            this.uLabelSearchProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProduct.TabIndex = 5;
            this.uLabelSearchProduct.Text = "ultraLabel1";
            // 
            // uComboSearchProcessCode
            // 
            this.uComboSearchProcessCode.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchProcessCode.Name = "uComboSearchProcessCode";
            this.uComboSearchProcessCode.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchProcessCode.TabIndex = 4;
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcess.TabIndex = 3;
            this.uLabelSearchProcess.Text = "ultraLabel1";
            // 
            // uComboSearchPlantCode
            // 
            this.uComboSearchPlantCode.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlantCode.Name = "uComboSearchPlantCode";
            this.uComboSearchPlantCode.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlantCode.TabIndex = 2;
            this.uComboSearchPlantCode.ValueChanged += new System.EventHandler(this.uComboSearchPlantCode_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 1;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxStdInfo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 700);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.uGroupBox2);
            this.ultraGroupBox3.Controls.Add(this.uGroupBox3);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 256);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1064, 236);
            this.ultraGroupBox3.TabIndex = 9;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uGridAffectLotList);
            this.uGroupBox2.Controls.Add(this.ultraGroupBox4);
            this.uGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBox2.Location = new System.Drawing.Point(3, 0);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(538, 233);
            this.uGroupBox2.TabIndex = 13;
            // 
            // uGridAffectLotList
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridAffectLotList.DisplayLayout.Appearance = appearance5;
            this.uGridAffectLotList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridAffectLotList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridAffectLotList.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridAffectLotList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridAffectLotList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridAffectLotList.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridAffectLotList.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridAffectLotList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridAffectLotList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridAffectLotList.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridAffectLotList.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridAffectLotList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridAffectLotList.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridAffectLotList.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridAffectLotList.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridAffectLotList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridAffectLotList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridAffectLotList.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridAffectLotList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridAffectLotList.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridAffectLotList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridAffectLotList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridAffectLotList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridAffectLotList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridAffectLotList.Location = new System.Drawing.Point(3, 40);
            this.uGridAffectLotList.Name = "uGridAffectLotList";
            this.uGridAffectLotList.Size = new System.Drawing.Size(532, 190);
            this.uGridAffectLotList.TabIndex = 36;
            this.uGridAffectLotList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridAffectLotList_AfterCellUpdate);
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.uButtonDelete);
            this.ultraGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox4.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(532, 40);
            this.ultraGroupBox4.TabIndex = 35;
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(8, 8);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 34;
            this.uButtonDelete.Text = "ultraButton1";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox3.Controls.Add(this.uLabelEtc);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserName);
            this.uGroupBox3.Controls.Add(this.uTextInspectUserID);
            this.uGroupBox3.Controls.Add(this.uLabelInspectUser);
            this.uGroupBox3.Controls.Add(this.uTextWorkUserName);
            this.uGroupBox3.Controls.Add(this.uTextWorkUserID);
            this.uGroupBox3.Controls.Add(this.uLabelWorkUser);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag9);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag8);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag7);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag6);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag5);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag4);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag3);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag2);
            this.uGroupBox3.Controls.Add(this.uCheckActionFlag1);
            this.uGroupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.uGroupBox3.Location = new System.Drawing.Point(541, 0);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(520, 233);
            this.uGroupBox3.TabIndex = 12;
            // 
            // uTextEtcDesc
            // 
            appearance20.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEtcDesc.Appearance = appearance20;
            this.uTextEtcDesc.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEtcDesc.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 204);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(388, 21);
            this.uTextEtcDesc.TabIndex = 50;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 204);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc.TabIndex = 49;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uTextInspectUserName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Appearance = appearance17;
            this.uTextInspectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInspectUserName.Location = new System.Drawing.Point(218, 180);
            this.uTextInspectUserName.Name = "uTextInspectUserName";
            this.uTextInspectUserName.ReadOnly = true;
            this.uTextInspectUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserName.TabIndex = 48;
            // 
            // uTextInspectUserID
            // 
            appearance18.Image = ((object)(resources.GetObject("appearance18.Image")));
            appearance18.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance18;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextInspectUserID.ButtonsRight.Add(editorButton2);
            this.uTextInspectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectUserID.Location = new System.Drawing.Point(116, 180);
            this.uTextInspectUserID.Name = "uTextInspectUserID";
            this.uTextInspectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextInspectUserID.TabIndex = 47;
            // 
            // uLabelInspectUser
            // 
            this.uLabelInspectUser.Location = new System.Drawing.Point(12, 180);
            this.uLabelInspectUser.Name = "uLabelInspectUser";
            this.uLabelInspectUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectUser.TabIndex = 46;
            this.uLabelInspectUser.Text = "ultraLabel1";
            // 
            // uTextWorkUserName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Appearance = appearance19;
            this.uTextWorkUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWorkUserName.Location = new System.Drawing.Point(218, 156);
            this.uTextWorkUserName.Name = "uTextWorkUserName";
            this.uTextWorkUserName.ReadOnly = true;
            this.uTextWorkUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextWorkUserName.TabIndex = 45;
            // 
            // uTextWorkUserID
            // 
            appearance21.Image = ((object)(resources.GetObject("appearance21.Image")));
            appearance21.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance21;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWorkUserID.ButtonsRight.Add(editorButton3);
            this.uTextWorkUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkUserID.Location = new System.Drawing.Point(116, 156);
            this.uTextWorkUserID.Name = "uTextWorkUserID";
            this.uTextWorkUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextWorkUserID.TabIndex = 44;
            // 
            // uLabelWorkUser
            // 
            this.uLabelWorkUser.Location = new System.Drawing.Point(12, 156);
            this.uLabelWorkUser.Name = "uLabelWorkUser";
            this.uLabelWorkUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkUser.TabIndex = 43;
            this.uLabelWorkUser.Text = "ultraLabel1";
            // 
            // uCheckActionFlag9
            // 
            this.uCheckActionFlag9.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag9.Location = new System.Drawing.Point(12, 124);
            this.uCheckActionFlag9.Name = "uCheckActionFlag9";
            this.uCheckActionFlag9.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag9.TabIndex = 42;
            this.uCheckActionFlag9.Text = "전수 선별 QC 의뢰";
            this.uCheckActionFlag9.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag8
            // 
            this.uCheckActionFlag8.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag8.Location = new System.Drawing.Point(268, 100);
            this.uCheckActionFlag8.Name = "uCheckActionFlag8";
            this.uCheckActionFlag8.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag8.TabIndex = 41;
            this.uCheckActionFlag8.Text = "작업 중지";
            this.uCheckActionFlag8.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag7
            // 
            this.uCheckActionFlag7.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag7.Location = new System.Drawing.Point(12, 100);
            this.uCheckActionFlag7.Name = "uCheckActionFlag7";
            this.uCheckActionFlag7.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag7.TabIndex = 40;
            this.uCheckActionFlag7.Text = "기술부서 검토 의뢰";
            this.uCheckActionFlag7.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag6
            // 
            this.uCheckActionFlag6.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag6.Location = new System.Drawing.Point(268, 76);
            this.uCheckActionFlag6.Name = "uCheckActionFlag6";
            this.uCheckActionFlag6.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag6.TabIndex = 39;
            this.uCheckActionFlag6.Text = "표준 발행 유무 점검";
            this.uCheckActionFlag6.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag5
            // 
            this.uCheckActionFlag5.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag5.Location = new System.Drawing.Point(12, 76);
            this.uCheckActionFlag5.Name = "uCheckActionFlag5";
            this.uCheckActionFlag5.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag5.TabIndex = 38;
            this.uCheckActionFlag5.Text = "장비 상태 및 가동 조건 점검";
            this.uCheckActionFlag5.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag4
            // 
            this.uCheckActionFlag4.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag4.Location = new System.Drawing.Point(268, 52);
            this.uCheckActionFlag4.Name = "uCheckActionFlag4";
            this.uCheckActionFlag4.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag4.TabIndex = 37;
            this.uCheckActionFlag4.Text = "교육 실시";
            this.uCheckActionFlag4.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag3
            // 
            this.uCheckActionFlag3.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag3.Location = new System.Drawing.Point(12, 52);
            this.uCheckActionFlag3.Name = "uCheckActionFlag3";
            this.uCheckActionFlag3.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag3.TabIndex = 36;
            this.uCheckActionFlag3.Text = "원/부자재 상태 점검";
            this.uCheckActionFlag3.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag2
            // 
            this.uCheckActionFlag2.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag2.Location = new System.Drawing.Point(268, 28);
            this.uCheckActionFlag2.Name = "uCheckActionFlag2";
            this.uCheckActionFlag2.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag2.TabIndex = 35;
            this.uCheckActionFlag2.Text = "부적합 처리 절차에 의해 처리";
            this.uCheckActionFlag2.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckActionFlag1
            // 
            this.uCheckActionFlag1.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckActionFlag1.Location = new System.Drawing.Point(12, 28);
            this.uCheckActionFlag1.Name = "uCheckActionFlag1";
            this.uCheckActionFlag1.Size = new System.Drawing.Size(250, 20);
            this.uCheckActionFlag1.TabIndex = 34;
            this.uCheckActionFlag1.Text = "작업 방법 점검";
            this.uCheckActionFlag1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uGroupQCNUser);
            this.ultraGroupBox1.Controls.Add(this.uGroupBoxFaulty);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 492);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1064, 208);
            this.ultraGroupBox1.TabIndex = 8;
            // 
            // uGroupQCNUser
            // 
            this.uGroupQCNUser.Controls.Add(this.uGridQCNUser);
            this.uGroupQCNUser.Controls.Add(this.ultraGroupBox5);
            this.uGroupQCNUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupQCNUser.Location = new System.Drawing.Point(515, 0);
            this.uGroupQCNUser.Name = "uGroupQCNUser";
            this.uGroupQCNUser.Size = new System.Drawing.Size(546, 205);
            this.uGroupQCNUser.TabIndex = 7;
            // 
            // uGridQCNUser
            // 
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQCNUser.DisplayLayout.Appearance = appearance22;
            this.uGridQCNUser.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQCNUser.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNUser.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNUser.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.uGridQCNUser.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNUser.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.uGridQCNUser.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQCNUser.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQCNUser.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQCNUser.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.uGridQCNUser.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQCNUser.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQCNUser.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQCNUser.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridQCNUser.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQCNUser.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNUser.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.uGridQCNUser.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.uGridQCNUser.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQCNUser.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.uGridQCNUser.DisplayLayout.Override.RowAppearance = appearance32;
            this.uGridQCNUser.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQCNUser.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.uGridQCNUser.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQCNUser.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQCNUser.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQCNUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridQCNUser.Location = new System.Drawing.Point(3, 40);
            this.uGridQCNUser.Name = "uGridQCNUser";
            this.uGridQCNUser.Size = new System.Drawing.Size(540, 162);
            this.uGridQCNUser.TabIndex = 36;
            this.uGridQCNUser.Text = "ultraGrid1";
            this.uGridQCNUser.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridQCNUser_AfterCellUpdate);
            this.uGridQCNUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridQCNUser_KeyDown);
            this.uGridQCNUser.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridQCNUser_ClickCellButton);
            this.uGridQCNUser.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridQCNUser_CellChange);
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.uButtonOK);
            this.ultraGroupBox5.Controls.Add(this.uComboDept);
            this.ultraGroupBox5.Controls.Add(this.uLabelDept);
            this.ultraGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox5.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(540, 40);
            this.ultraGroupBox5.TabIndex = 37;
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(264, 8);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 2;
            this.uButtonOK.Text = "확인";
            // 
            // uComboDept
            // 
            this.uComboDept.Location = new System.Drawing.Point(116, 12);
            this.uComboDept.Name = "uComboDept";
            this.uComboDept.Size = new System.Drawing.Size(144, 21);
            this.uComboDept.TabIndex = 1;
            // 
            // uLabelDept
            // 
            this.uLabelDept.Location = new System.Drawing.Point(12, 12);
            this.uLabelDept.Name = "uLabelDept";
            this.uLabelDept.Size = new System.Drawing.Size(100, 20);
            this.uLabelDept.TabIndex = 0;
            this.uLabelDept.Text = "부서";
            // 
            // uGroupBoxFaulty
            // 
            this.uGroupBoxFaulty.Controls.Add(this.uGridFaultType);
            this.uGroupBoxFaulty.Controls.Add(this.ultraGroupBox2);
            this.uGroupBoxFaulty.Dock = System.Windows.Forms.DockStyle.Left;
            this.uGroupBoxFaulty.Location = new System.Drawing.Point(3, 0);
            this.uGroupBoxFaulty.Name = "uGroupBoxFaulty";
            this.uGroupBoxFaulty.Size = new System.Drawing.Size(512, 205);
            this.uGroupBoxFaulty.TabIndex = 5;
            // 
            // uGridFaultType
            // 
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFaultType.DisplayLayout.Appearance = appearance34;
            this.uGridFaultType.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFaultType.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFaultType.DisplayLayout.GroupByBox.Appearance = appearance35;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFaultType.DisplayLayout.GroupByBox.BandLabelAppearance = appearance36;
            this.uGridFaultType.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance37.BackColor2 = System.Drawing.SystemColors.Control;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFaultType.DisplayLayout.GroupByBox.PromptAppearance = appearance37;
            this.uGridFaultType.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFaultType.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFaultType.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFaultType.DisplayLayout.Override.ActiveRowAppearance = appearance39;
            this.uGridFaultType.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFaultType.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFaultType.DisplayLayout.Override.CardAreaAppearance = appearance40;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            appearance41.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFaultType.DisplayLayout.Override.CellAppearance = appearance41;
            this.uGridFaultType.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFaultType.DisplayLayout.Override.CellPadding = 0;
            appearance42.BackColor = System.Drawing.SystemColors.Control;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFaultType.DisplayLayout.Override.GroupByRowAppearance = appearance42;
            appearance43.TextHAlignAsString = "Left";
            this.uGridFaultType.DisplayLayout.Override.HeaderAppearance = appearance43;
            this.uGridFaultType.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFaultType.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            this.uGridFaultType.DisplayLayout.Override.RowAppearance = appearance44;
            this.uGridFaultType.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFaultType.DisplayLayout.Override.TemplateAddRowAppearance = appearance45;
            this.uGridFaultType.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFaultType.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFaultType.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFaultType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridFaultType.Location = new System.Drawing.Point(3, 40);
            this.uGridFaultType.Name = "uGridFaultType";
            this.uGridFaultType.Size = new System.Drawing.Size(506, 162);
            this.uGridFaultType.TabIndex = 36;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uButtonDeleteRowFaulty);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(506, 40);
            this.ultraGroupBox2.TabIndex = 35;
            // 
            // uButtonDeleteRowFaulty
            // 
            this.uButtonDeleteRowFaulty.Location = new System.Drawing.Point(8, 8);
            this.uButtonDeleteRowFaulty.Name = "uButtonDeleteRowFaulty";
            this.uButtonDeleteRowFaulty.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRowFaulty.TabIndex = 34;
            this.uButtonDeleteRowFaulty.Text = "삭제";
            // 
            // uGroupBoxStdInfo
            // 
            this.uGroupBoxStdInfo.Controls.Add(this.uComboSYSTEM);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelSYSTEM);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextProgram);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelProgram);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboHandler);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelHandler);
            this.uGroupBoxStdInfo.Controls.Add(this.ultraLabel7);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumRetestFaultQty3);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumRetestInspectQty3);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelRetest3);
            this.uGroupBoxStdInfo.Controls.Add(this.ultraLabel5);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumRetestFaultQty2);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumRetestInspectQty2);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelRetest2);
            this.uGroupBoxStdInfo.Controls.Add(this.ultraLabel3);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumRetestFaultQty1);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumRetestInspectQty1);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelRetest1);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboP_QcnItr);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextFaultImg);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelFaultImg);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uGridExpectEquipList);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextProductName);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextReqSeq);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextReqNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextLotHoldState);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextLotProcessState);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboAriseEquipCode);
            this.uGroupBoxStdInfo.Controls.Add(this.ultraLabel1);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboExpectProcessCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboAriseProcessCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextPackage);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPackage);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextPublishComplete);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextMESTFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerProductSpec);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelCustomerProductSpec);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckPublishFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPublishFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextMESWorkUserID);
            this.uGroupBoxStdInfo.Controls.Add(this.uDateAriseTime);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumFaultQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumInspectQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uNumQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckWorkStepEquipFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uCheckWorkStepMaterialFlag);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelWorkStep);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboInspectFaultTypeCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelInspectFaultType);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelInspectQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelQty);
            this.uGroupBoxStdInfo.Controls.Add(this.uDateAriseDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelAriseDate);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelExpectProcess);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelAriseEquip);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelAriseProcess);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextProductCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelProduct);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextCustomerName);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelCustomerName);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextLotNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelLotNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelGubun);
            this.uGroupBoxStdInfo.Controls.Add(this.uTextQCNNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelQCNNo);
            this.uGroupBoxStdInfo.Controls.Add(this.uComboPlantCode);
            this.uGroupBoxStdInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxStdInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxStdInfo.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxStdInfo.Name = "uGroupBoxStdInfo";
            this.uGroupBoxStdInfo.Size = new System.Drawing.Size(1064, 256);
            this.uGroupBoxStdInfo.TabIndex = 4;
            // 
            // uComboSYSTEM
            // 
            this.uComboSYSTEM.Location = new System.Drawing.Point(904, 36);
            this.uComboSYSTEM.Name = "uComboSYSTEM";
            this.uComboSYSTEM.Size = new System.Drawing.Size(120, 21);
            this.uComboSYSTEM.TabIndex = 128;
            this.uComboSYSTEM.Text = "ultraComboEditor1";
            // 
            // uLabelSYSTEM
            // 
            this.uLabelSYSTEM.Location = new System.Drawing.Point(800, 36);
            this.uLabelSYSTEM.Name = "uLabelSYSTEM";
            this.uLabelSYSTEM.Size = new System.Drawing.Size(100, 20);
            this.uLabelSYSTEM.TabIndex = 127;
            this.uLabelSYSTEM.Text = "ultraLabel1";
            // 
            // uTextProgram
            // 
            this.uTextProgram.Location = new System.Drawing.Point(644, 84);
            this.uTextProgram.Name = "uTextProgram";
            this.uTextProgram.Size = new System.Drawing.Size(380, 21);
            this.uTextProgram.TabIndex = 126;
            // 
            // uLabelProgram
            // 
            this.uLabelProgram.Location = new System.Drawing.Point(540, 84);
            this.uLabelProgram.Name = "uLabelProgram";
            this.uLabelProgram.Size = new System.Drawing.Size(100, 20);
            this.uLabelProgram.TabIndex = 125;
            this.uLabelProgram.Text = "ultraLabel2";
            // 
            // uComboHandler
            // 
            this.uComboHandler.Location = new System.Drawing.Point(904, 60);
            this.uComboHandler.Name = "uComboHandler";
            this.uComboHandler.Size = new System.Drawing.Size(120, 21);
            this.uComboHandler.TabIndex = 124;
            this.uComboHandler.Text = "ultraComboEditor1";
            // 
            // uLabelHandler
            // 
            this.uLabelHandler.Location = new System.Drawing.Point(800, 60);
            this.uLabelHandler.Name = "uLabelHandler";
            this.uLabelHandler.Size = new System.Drawing.Size(100, 20);
            this.uLabelHandler.TabIndex = 123;
            this.uLabelHandler.Text = "ultraLabel1";
            // 
            // ultraLabel7
            // 
            appearance46.TextHAlignAsString = "Center";
            appearance46.TextVAlignAsString = "Middle";
            this.ultraLabel7.Appearance = appearance46;
            this.ultraLabel7.Location = new System.Drawing.Point(228, 204);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel7.TabIndex = 82;
            this.ultraLabel7.Text = "/";
            // 
            // uNumRetestFaultQty3
            // 
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton4.Text = "0";
            this.uNumRetestFaultQty3.ButtonsLeft.Add(editorButton4);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumRetestFaultQty3.ButtonsRight.Add(spinEditorButton1);
            this.uNumRetestFaultQty3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumRetestFaultQty3.Location = new System.Drawing.Point(116, 204);
            this.uNumRetestFaultQty3.MaskInput = "nnnnn";
            this.uNumRetestFaultQty3.MinValue = 0;
            this.uNumRetestFaultQty3.Name = "uNumRetestFaultQty3";
            this.uNumRetestFaultQty3.Size = new System.Drawing.Size(110, 21);
            this.uNumRetestFaultQty3.TabIndex = 81;
            // 
            // uNumRetestInspectQty3
            // 
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Text = "0";
            this.uNumRetestInspectQty3.ButtonsLeft.Add(editorButton5);
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumRetestInspectQty3.ButtonsRight.Add(spinEditorButton2);
            this.uNumRetestInspectQty3.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumRetestInspectQty3.Location = new System.Drawing.Point(244, 204);
            this.uNumRetestInspectQty3.MaskInput = "nnnnn";
            this.uNumRetestInspectQty3.MinValue = 0;
            this.uNumRetestInspectQty3.Name = "uNumRetestInspectQty3";
            this.uNumRetestInspectQty3.Size = new System.Drawing.Size(110, 21);
            this.uNumRetestInspectQty3.TabIndex = 80;
            // 
            // uLabelRetest3
            // 
            this.uLabelRetest3.Location = new System.Drawing.Point(12, 204);
            this.uLabelRetest3.Name = "uLabelRetest3";
            this.uLabelRetest3.Size = new System.Drawing.Size(100, 20);
            this.uLabelRetest3.TabIndex = 79;
            this.uLabelRetest3.Text = "ultraLabel1";
            // 
            // ultraLabel5
            // 
            appearance47.TextHAlignAsString = "Center";
            appearance47.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance47;
            this.ultraLabel5.Location = new System.Drawing.Point(228, 180);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel5.TabIndex = 78;
            this.ultraLabel5.Text = "/";
            // 
            // uNumRetestFaultQty2
            // 
            editorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton6.Text = "0";
            this.uNumRetestFaultQty2.ButtonsLeft.Add(editorButton6);
            spinEditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumRetestFaultQty2.ButtonsRight.Add(spinEditorButton3);
            this.uNumRetestFaultQty2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumRetestFaultQty2.Location = new System.Drawing.Point(116, 180);
            this.uNumRetestFaultQty2.MaskInput = "nnnnn";
            this.uNumRetestFaultQty2.MinValue = 0;
            this.uNumRetestFaultQty2.Name = "uNumRetestFaultQty2";
            this.uNumRetestFaultQty2.Size = new System.Drawing.Size(110, 21);
            this.uNumRetestFaultQty2.TabIndex = 77;
            // 
            // uNumRetestInspectQty2
            // 
            editorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton7.Text = "0";
            this.uNumRetestInspectQty2.ButtonsLeft.Add(editorButton7);
            spinEditorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumRetestInspectQty2.ButtonsRight.Add(spinEditorButton4);
            this.uNumRetestInspectQty2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumRetestInspectQty2.Location = new System.Drawing.Point(244, 180);
            this.uNumRetestInspectQty2.MaskInput = "nnnnn";
            this.uNumRetestInspectQty2.MinValue = 0;
            this.uNumRetestInspectQty2.Name = "uNumRetestInspectQty2";
            this.uNumRetestInspectQty2.Size = new System.Drawing.Size(110, 21);
            this.uNumRetestInspectQty2.TabIndex = 76;
            // 
            // uLabelRetest2
            // 
            this.uLabelRetest2.Location = new System.Drawing.Point(12, 180);
            this.uLabelRetest2.Name = "uLabelRetest2";
            this.uLabelRetest2.Size = new System.Drawing.Size(100, 20);
            this.uLabelRetest2.TabIndex = 75;
            this.uLabelRetest2.Text = "ultraLabel1";
            // 
            // ultraLabel3
            // 
            appearance48.TextHAlignAsString = "Center";
            appearance48.TextVAlignAsString = "Middle";
            this.ultraLabel3.Appearance = appearance48;
            this.ultraLabel3.Location = new System.Drawing.Point(228, 156);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel3.TabIndex = 74;
            this.ultraLabel3.Text = "/";
            // 
            // uNumRetestFaultQty1
            // 
            editorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton8.Text = "0";
            this.uNumRetestFaultQty1.ButtonsLeft.Add(editorButton8);
            spinEditorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumRetestFaultQty1.ButtonsRight.Add(spinEditorButton5);
            this.uNumRetestFaultQty1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumRetestFaultQty1.Location = new System.Drawing.Point(116, 156);
            this.uNumRetestFaultQty1.MaskInput = "nnnnn";
            this.uNumRetestFaultQty1.MinValue = 0;
            this.uNumRetestFaultQty1.Name = "uNumRetestFaultQty1";
            this.uNumRetestFaultQty1.Size = new System.Drawing.Size(110, 21);
            this.uNumRetestFaultQty1.TabIndex = 73;
            // 
            // uNumRetestInspectQty1
            // 
            editorButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton9.Text = "0";
            this.uNumRetestInspectQty1.ButtonsLeft.Add(editorButton9);
            spinEditorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumRetestInspectQty1.ButtonsRight.Add(spinEditorButton6);
            this.uNumRetestInspectQty1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumRetestInspectQty1.Location = new System.Drawing.Point(244, 156);
            this.uNumRetestInspectQty1.MaskInput = "nnnnn";
            this.uNumRetestInspectQty1.MinValue = 0;
            this.uNumRetestInspectQty1.Name = "uNumRetestInspectQty1";
            this.uNumRetestInspectQty1.Size = new System.Drawing.Size(110, 21);
            this.uNumRetestInspectQty1.TabIndex = 72;
            // 
            // uLabelRetest1
            // 
            this.uLabelRetest1.Location = new System.Drawing.Point(12, 156);
            this.uLabelRetest1.Name = "uLabelRetest1";
            this.uLabelRetest1.Size = new System.Drawing.Size(100, 20);
            this.uLabelRetest1.TabIndex = 71;
            this.uLabelRetest1.Text = "ultraLabel1";
            // 
            // uComboP_QcnItr
            // 
            this.uComboP_QcnItr.Location = new System.Drawing.Point(380, 12);
            this.uComboP_QcnItr.Name = "uComboP_QcnItr";
            this.uComboP_QcnItr.Size = new System.Drawing.Size(144, 21);
            this.uComboP_QcnItr.TabIndex = 70;
            // 
            // uTextFaultImg
            // 
            appearance49.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFaultImg.Appearance = appearance49;
            this.uTextFaultImg.BackColor = System.Drawing.Color.Gainsboro;
            appearance50.Image = global::QRPINS.UI.Properties.Resources.btn_Fileupload;
            appearance50.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton10.Appearance = appearance50;
            editorButton10.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton10.Key = "UP";
            appearance51.Image = global::QRPINS.UI.Properties.Resources.btn_Filedownload;
            appearance51.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton11.Appearance = appearance51;
            editorButton11.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton11.Key = "DOWN";
            this.uTextFaultImg.ButtonsRight.Add(editorButton10);
            this.uTextFaultImg.ButtonsRight.Add(editorButton11);
            this.uTextFaultImg.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextFaultImg.Location = new System.Drawing.Point(116, 84);
            this.uTextFaultImg.Name = "uTextFaultImg";
            this.uTextFaultImg.ReadOnly = true;
            this.uTextFaultImg.Size = new System.Drawing.Size(150, 21);
            this.uTextFaultImg.TabIndex = 68;
            this.uTextFaultImg.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextFaultImg_KeyDown);
            this.uTextFaultImg.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextFaultImg_EditorButtonClick);
            // 
            // uLabelFaultImg
            // 
            this.uLabelFaultImg.Location = new System.Drawing.Point(12, 84);
            this.uLabelFaultImg.Name = "uLabelFaultImg";
            this.uLabelFaultImg.Size = new System.Drawing.Size(100, 20);
            this.uLabelFaultImg.TabIndex = 69;
            this.uLabelFaultImg.Text = "ultraLabel1";
            // 
            // uTextCustomerCode
            // 
            appearance52.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance52;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Location = new System.Drawing.Point(244, 36);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(21, 21);
            this.uTextCustomerCode.TabIndex = 67;
            this.uTextCustomerCode.Visible = false;
            // 
            // uGridExpectEquipList
            // 
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridExpectEquipList.DisplayLayout.Appearance = appearance53;
            this.uGridExpectEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridExpectEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance54.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance54.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance54.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.Appearance = appearance54;
            appearance55.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance55;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance56.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance56.BackColor2 = System.Drawing.SystemColors.Control;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance56.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridExpectEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance56;
            this.uGridExpectEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridExpectEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            appearance57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridExpectEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance57;
            appearance58.BackColor = System.Drawing.SystemColors.Highlight;
            appearance58.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridExpectEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance58;
            this.uGridExpectEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridExpectEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            this.uGridExpectEquipList.DisplayLayout.Override.CardAreaAppearance = appearance59;
            appearance60.BorderColor = System.Drawing.Color.Silver;
            appearance60.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridExpectEquipList.DisplayLayout.Override.CellAppearance = appearance60;
            this.uGridExpectEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridExpectEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance61.BackColor = System.Drawing.SystemColors.Control;
            appearance61.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance61.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance61.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridExpectEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance61;
            appearance62.TextHAlignAsString = "Left";
            this.uGridExpectEquipList.DisplayLayout.Override.HeaderAppearance = appearance62;
            this.uGridExpectEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridExpectEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.BorderColor = System.Drawing.Color.Silver;
            this.uGridExpectEquipList.DisplayLayout.Override.RowAppearance = appearance63;
            this.uGridExpectEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridExpectEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance64;
            this.uGridExpectEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridExpectEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridExpectEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridExpectEquipList.Location = new System.Drawing.Point(380, 108);
            this.uGridExpectEquipList.Name = "uGridExpectEquipList";
            this.uGridExpectEquipList.Size = new System.Drawing.Size(480, 92);
            this.uGridExpectEquipList.TabIndex = 66;
            this.uGridExpectEquipList.Text = "ultraGrid1";
            this.uGridExpectEquipList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridExpectEquipList_DoubleClickRow);
            // 
            // uTextProductName
            // 
            appearance65.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance65;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(768, 12);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(26, 21);
            this.uTextProductName.TabIndex = 10;
            this.uTextProductName.Visible = false;
            // 
            // uTextReqSeq
            // 
            appearance66.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Appearance = appearance66;
            this.uTextReqSeq.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqSeq.Location = new System.Drawing.Point(956, 228);
            this.uTextReqSeq.Name = "uTextReqSeq";
            this.uTextReqSeq.ReadOnly = true;
            this.uTextReqSeq.Size = new System.Drawing.Size(21, 21);
            this.uTextReqSeq.TabIndex = 65;
            this.uTextReqSeq.Visible = false;
            // 
            // uTextReqNo
            // 
            appearance67.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Appearance = appearance67;
            this.uTextReqNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReqNo.Location = new System.Drawing.Point(932, 228);
            this.uTextReqNo.Name = "uTextReqNo";
            this.uTextReqNo.ReadOnly = true;
            this.uTextReqNo.Size = new System.Drawing.Size(21, 21);
            this.uTextReqNo.TabIndex = 64;
            this.uTextReqNo.Visible = false;
            // 
            // uTextLotHoldState
            // 
            appearance68.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotHoldState.Appearance = appearance68;
            this.uTextLotHoldState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotHoldState.Location = new System.Drawing.Point(1004, 228);
            this.uTextLotHoldState.Name = "uTextLotHoldState";
            this.uTextLotHoldState.ReadOnly = true;
            this.uTextLotHoldState.Size = new System.Drawing.Size(21, 21);
            this.uTextLotHoldState.TabIndex = 63;
            this.uTextLotHoldState.Visible = false;
            // 
            // uTextLotProcessState
            // 
            appearance69.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Appearance = appearance69;
            this.uTextLotProcessState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLotProcessState.Location = new System.Drawing.Point(980, 228);
            this.uTextLotProcessState.Name = "uTextLotProcessState";
            this.uTextLotProcessState.ReadOnly = true;
            this.uTextLotProcessState.Size = new System.Drawing.Size(21, 21);
            this.uTextLotProcessState.TabIndex = 62;
            this.uTextLotProcessState.Visible = false;
            // 
            // uComboAriseEquipCode
            // 
            this.uComboAriseEquipCode.CheckedListSettings.CheckStateMember = "";
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboAriseEquipCode.DisplayLayout.Appearance = appearance70;
            this.uComboAriseEquipCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboAriseEquipCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance71.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance71.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance71.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.Appearance = appearance71;
            appearance72.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance72;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance73.BackColor2 = System.Drawing.SystemColors.Control;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseEquipCode.DisplayLayout.GroupByBox.PromptAppearance = appearance73;
            this.uComboAriseEquipCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboAriseEquipCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboAriseEquipCode.DisplayLayout.Override.ActiveCellAppearance = appearance74;
            appearance75.BackColor = System.Drawing.SystemColors.Highlight;
            appearance75.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboAriseEquipCode.DisplayLayout.Override.ActiveRowAppearance = appearance75;
            this.uComboAriseEquipCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboAriseEquipCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            this.uComboAriseEquipCode.DisplayLayout.Override.CardAreaAppearance = appearance76;
            appearance77.BorderColor = System.Drawing.Color.Silver;
            appearance77.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboAriseEquipCode.DisplayLayout.Override.CellAppearance = appearance77;
            this.uComboAriseEquipCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboAriseEquipCode.DisplayLayout.Override.CellPadding = 0;
            appearance78.BackColor = System.Drawing.SystemColors.Control;
            appearance78.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance78.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseEquipCode.DisplayLayout.Override.GroupByRowAppearance = appearance78;
            appearance79.TextHAlignAsString = "Left";
            this.uComboAriseEquipCode.DisplayLayout.Override.HeaderAppearance = appearance79;
            this.uComboAriseEquipCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboAriseEquipCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            this.uComboAriseEquipCode.DisplayLayout.Override.RowAppearance = appearance80;
            this.uComboAriseEquipCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboAriseEquipCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance81;
            this.uComboAriseEquipCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboAriseEquipCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboAriseEquipCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboAriseEquipCode.Location = new System.Drawing.Point(644, 60);
            this.uComboAriseEquipCode.Name = "uComboAriseEquipCode";
            this.uComboAriseEquipCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboAriseEquipCode.Size = new System.Drawing.Size(150, 22);
            this.uComboAriseEquipCode.TabIndex = 59;
            this.uComboAriseEquipCode.Text = "ultraCombo1";
            this.uComboAriseEquipCode.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboAriseEquipCode_RowSelected);
            this.uComboAriseEquipCode.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboAriseEquipCode_BeforeDropDown);
            // 
            // ultraLabel1
            // 
            appearance82.TextHAlignAsString = "Center";
            appearance82.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance82;
            this.ultraLabel1.Location = new System.Drawing.Point(228, 132);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 20);
            this.ultraLabel1.TabIndex = 58;
            this.ultraLabel1.Text = "/";
            // 
            // uComboExpectProcessCode
            // 
            this.uComboExpectProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboExpectProcessCode.DisplayLayout.Appearance = appearance83;
            this.uComboExpectProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboExpectProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance84.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance84.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance84.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance84.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.Appearance = appearance84;
            appearance85.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance85;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance86.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance86.BackColor2 = System.Drawing.SystemColors.Control;
            appearance86.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance86.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboExpectProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance86;
            this.uComboExpectProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboExpectProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboExpectProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance87;
            appearance88.BackColor = System.Drawing.SystemColors.Highlight;
            appearance88.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboExpectProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance88;
            this.uComboExpectProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboExpectProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            this.uComboExpectProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance89;
            appearance90.BorderColor = System.Drawing.Color.Silver;
            appearance90.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboExpectProcessCode.DisplayLayout.Override.CellAppearance = appearance90;
            this.uComboExpectProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboExpectProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance91.BackColor = System.Drawing.SystemColors.Control;
            appearance91.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance91.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance91.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance91.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboExpectProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance91;
            appearance92.TextHAlignAsString = "Left";
            this.uComboExpectProcessCode.DisplayLayout.Override.HeaderAppearance = appearance92;
            this.uComboExpectProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboExpectProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            appearance93.BorderColor = System.Drawing.Color.Silver;
            this.uComboExpectProcessCode.DisplayLayout.Override.RowAppearance = appearance93;
            this.uComboExpectProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance94.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboExpectProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance94;
            this.uComboExpectProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboExpectProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboExpectProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboExpectProcessCode.Location = new System.Drawing.Point(380, 84);
            this.uComboExpectProcessCode.Name = "uComboExpectProcessCode";
            this.uComboExpectProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboExpectProcessCode.Size = new System.Drawing.Size(150, 22);
            this.uComboExpectProcessCode.TabIndex = 57;
            this.uComboExpectProcessCode.Text = "ultraCombo1";
            this.uComboExpectProcessCode.RowSelected += new Infragistics.Win.UltraWinGrid.RowSelectedEventHandler(this.uComboExpectProcessCode_RowSelected);
            this.uComboExpectProcessCode.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboExpectProcessCode_BeforeDropDown);
            // 
            // uComboAriseProcessCode
            // 
            this.uComboAriseProcessCode.CheckedListSettings.CheckStateMember = "";
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uComboAriseProcessCode.DisplayLayout.Appearance = appearance95;
            this.uComboAriseProcessCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboAriseProcessCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance96.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance96.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance96.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance96.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.Appearance = appearance96;
            appearance97.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance97;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance98.BackColor2 = System.Drawing.SystemColors.Control;
            appearance98.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance98.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uComboAriseProcessCode.DisplayLayout.GroupByBox.PromptAppearance = appearance98;
            this.uComboAriseProcessCode.DisplayLayout.MaxColScrollRegions = 1;
            this.uComboAriseProcessCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance99.BackColor = System.Drawing.SystemColors.Window;
            appearance99.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uComboAriseProcessCode.DisplayLayout.Override.ActiveCellAppearance = appearance99;
            appearance100.BackColor = System.Drawing.SystemColors.Highlight;
            appearance100.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uComboAriseProcessCode.DisplayLayout.Override.ActiveRowAppearance = appearance100;
            this.uComboAriseProcessCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uComboAriseProcessCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance101.BackColor = System.Drawing.SystemColors.Window;
            this.uComboAriseProcessCode.DisplayLayout.Override.CardAreaAppearance = appearance101;
            appearance102.BorderColor = System.Drawing.Color.Silver;
            appearance102.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uComboAriseProcessCode.DisplayLayout.Override.CellAppearance = appearance102;
            this.uComboAriseProcessCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uComboAriseProcessCode.DisplayLayout.Override.CellPadding = 0;
            appearance103.BackColor = System.Drawing.SystemColors.Control;
            appearance103.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance103.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance103.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance103.BorderColor = System.Drawing.SystemColors.Window;
            this.uComboAriseProcessCode.DisplayLayout.Override.GroupByRowAppearance = appearance103;
            appearance104.TextHAlignAsString = "Left";
            this.uComboAriseProcessCode.DisplayLayout.Override.HeaderAppearance = appearance104;
            this.uComboAriseProcessCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uComboAriseProcessCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance105.BackColor = System.Drawing.SystemColors.Window;
            appearance105.BorderColor = System.Drawing.Color.Silver;
            this.uComboAriseProcessCode.DisplayLayout.Override.RowAppearance = appearance105;
            this.uComboAriseProcessCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance106.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uComboAriseProcessCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance106;
            this.uComboAriseProcessCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uComboAriseProcessCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uComboAriseProcessCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uComboAriseProcessCode.Location = new System.Drawing.Point(380, 60);
            this.uComboAriseProcessCode.Name = "uComboAriseProcessCode";
            this.uComboAriseProcessCode.PreferredDropDownSize = new System.Drawing.Size(0, 0);
            this.uComboAriseProcessCode.Size = new System.Drawing.Size(150, 22);
            this.uComboAriseProcessCode.TabIndex = 56;
            this.uComboAriseProcessCode.Text = "ultraCombo1";
            // 
            // uTextPackage
            // 
            appearance107.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance107;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(644, 36);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(150, 21);
            this.uTextPackage.TabIndex = 54;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(540, 36);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackage.TabIndex = 55;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uTextPublishComplete
            // 
            this.uTextPublishComplete.Location = new System.Drawing.Point(956, 204);
            this.uTextPublishComplete.Name = "uTextPublishComplete";
            this.uTextPublishComplete.ReadOnly = true;
            this.uTextPublishComplete.Size = new System.Drawing.Size(21, 21);
            this.uTextPublishComplete.TabIndex = 53;
            this.uTextPublishComplete.Visible = false;
            // 
            // uTextMESTFlag
            // 
            this.uTextMESTFlag.Location = new System.Drawing.Point(980, 204);
            this.uTextMESTFlag.Name = "uTextMESTFlag";
            this.uTextMESTFlag.ReadOnly = true;
            this.uTextMESTFlag.Size = new System.Drawing.Size(21, 21);
            this.uTextMESTFlag.TabIndex = 52;
            this.uTextMESTFlag.Visible = false;
            // 
            // uTextCustomerProductSpec
            // 
            appearance108.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Appearance = appearance108;
            this.uTextCustomerProductSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerProductSpec.Location = new System.Drawing.Point(380, 36);
            this.uTextCustomerProductSpec.Name = "uTextCustomerProductSpec";
            this.uTextCustomerProductSpec.ReadOnly = true;
            this.uTextCustomerProductSpec.Size = new System.Drawing.Size(150, 21);
            this.uTextCustomerProductSpec.TabIndex = 12;
            // 
            // uLabelCustomerProductSpec
            // 
            this.uLabelCustomerProductSpec.Location = new System.Drawing.Point(276, 36);
            this.uLabelCustomerProductSpec.Name = "uLabelCustomerProductSpec";
            this.uLabelCustomerProductSpec.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomerProductSpec.TabIndex = 51;
            this.uLabelCustomerProductSpec.Text = "ultraLabel1";
            // 
            // uCheckPublishFlag
            // 
            this.uCheckPublishFlag.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uCheckPublishFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckPublishFlag.Location = new System.Drawing.Point(484, 204);
            this.uCheckPublishFlag.Name = "uCheckPublishFlag";
            this.uCheckPublishFlag.Size = new System.Drawing.Size(16, 20);
            this.uCheckPublishFlag.TabIndex = 39;
            this.uCheckPublishFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelPublishFlag
            // 
            this.uLabelPublishFlag.Location = new System.Drawing.Point(380, 204);
            this.uLabelPublishFlag.Name = "uLabelPublishFlag";
            this.uLabelPublishFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelPublishFlag.TabIndex = 49;
            this.uLabelPublishFlag.Text = "ultraLabel1";
            // 
            // uTextMESWorkUserID
            // 
            appearance109.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESWorkUserID.Appearance = appearance109;
            this.uTextMESWorkUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMESWorkUserID.Location = new System.Drawing.Point(1004, 204);
            this.uTextMESWorkUserID.Name = "uTextMESWorkUserID";
            this.uTextMESWorkUserID.ReadOnly = true;
            this.uTextMESWorkUserID.Size = new System.Drawing.Size(21, 21);
            this.uTextMESWorkUserID.TabIndex = 48;
            this.uTextMESWorkUserID.Visible = false;
            // 
            // uDateAriseTime
            // 
            appearance110.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseTime.Appearance = appearance110;
            this.uDateAriseTime.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseTime.DateTime = new System.DateTime(2012, 10, 8, 0, 0, 0, 0);
            this.uDateAriseTime.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateAriseTime.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateAriseTime.Location = new System.Drawing.Point(932, 204);
            this.uDateAriseTime.MaskInput = "{LOC}hh:mm:ss";
            this.uDateAriseTime.Name = "uDateAriseTime";
            this.uDateAriseTime.Size = new System.Drawing.Size(20, 21);
            this.uDateAriseTime.TabIndex = 40;
            this.uDateAriseTime.Value = new System.DateTime(2012, 10, 8, 0, 0, 0, 0);
            this.uDateAriseTime.Visible = false;
            // 
            // uNumFaultQty
            // 
            editorButton12.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton12.Text = "0";
            this.uNumFaultQty.ButtonsLeft.Add(editorButton12);
            spinEditorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumFaultQty.ButtonsRight.Add(spinEditorButton7);
            this.uNumFaultQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumFaultQty.Location = new System.Drawing.Point(116, 132);
            this.uNumFaultQty.MaskInput = "nnnnn";
            this.uNumFaultQty.MinValue = 0;
            this.uNumFaultQty.Name = "uNumFaultQty";
            this.uNumFaultQty.Size = new System.Drawing.Size(110, 21);
            this.uNumFaultQty.TabIndex = 32;
            this.uNumFaultQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumFaultQty_EditorSpinButtonClick);
            this.uNumFaultQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumFaultQty_EditorButtonClick);
            // 
            // uNumInspectQty
            // 
            editorButton13.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton13.Text = "0";
            this.uNumInspectQty.ButtonsLeft.Add(editorButton13);
            spinEditorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumInspectQty.ButtonsRight.Add(spinEditorButton8);
            this.uNumInspectQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumInspectQty.Location = new System.Drawing.Point(244, 132);
            this.uNumInspectQty.MaskInput = "nnnnn";
            this.uNumInspectQty.MinValue = 0;
            this.uNumInspectQty.Name = "uNumInspectQty";
            this.uNumInspectQty.Size = new System.Drawing.Size(110, 21);
            this.uNumInspectQty.TabIndex = 30;
            this.uNumInspectQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumInspectQty_EditorSpinButtonClick);
            this.uNumInspectQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumInspectQty_EditorButtonClick);
            // 
            // uNumQty
            // 
            editorButton14.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton14.Text = "0";
            this.uNumQty.ButtonsLeft.Add(editorButton14);
            spinEditorButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumQty.ButtonsRight.Add(spinEditorButton9);
            this.uNumQty.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumQty.Location = new System.Drawing.Point(116, 108);
            this.uNumQty.MaskInput = "nnnnnnnnnn";
            this.uNumQty.Name = "uNumQty";
            this.uNumQty.Size = new System.Drawing.Size(110, 21);
            this.uNumQty.TabIndex = 28;
            this.uNumQty.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumQty_EditorSpinButtonClick);
            this.uNumQty.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumQty_EditorButtonClick);
            // 
            // uCheckWorkStepEquipFlag
            // 
            this.uCheckWorkStepEquipFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckWorkStepEquipFlag.Location = new System.Drawing.Point(220, 228);
            this.uCheckWorkStepEquipFlag.Name = "uCheckWorkStepEquipFlag";
            this.uCheckWorkStepEquipFlag.Size = new System.Drawing.Size(100, 20);
            this.uCheckWorkStepEquipFlag.TabIndex = 37;
            this.uCheckWorkStepEquipFlag.Text = "장비 재검";
            this.uCheckWorkStepEquipFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckWorkStepMaterialFlag
            // 
            appearance111.BackColorDisabled = System.Drawing.Color.Transparent;
            appearance111.ForeColorDisabled = System.Drawing.Color.Black;
            this.uCheckWorkStepMaterialFlag.Appearance = appearance111;
            this.uCheckWorkStepMaterialFlag.Checked = true;
            this.uCheckWorkStepMaterialFlag.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uCheckWorkStepMaterialFlag.Enabled = false;
            this.uCheckWorkStepMaterialFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckWorkStepMaterialFlag.Location = new System.Drawing.Point(116, 228);
            this.uCheckWorkStepMaterialFlag.Name = "uCheckWorkStepMaterialFlag";
            this.uCheckWorkStepMaterialFlag.Size = new System.Drawing.Size(100, 20);
            this.uCheckWorkStepMaterialFlag.TabIndex = 36;
            this.uCheckWorkStepMaterialFlag.Text = "자재 재검";
            this.uCheckWorkStepMaterialFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelWorkStep
            // 
            this.uLabelWorkStep.Location = new System.Drawing.Point(12, 228);
            this.uLabelWorkStep.Name = "uLabelWorkStep";
            this.uLabelWorkStep.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkStep.TabIndex = 31;
            this.uLabelWorkStep.Text = "ultraLabel1";
            // 
            // uComboInspectFaultTypeCode
            // 
            this.uComboInspectFaultTypeCode.Location = new System.Drawing.Point(908, 180);
            this.uComboInspectFaultTypeCode.Name = "uComboInspectFaultTypeCode";
            this.uComboInspectFaultTypeCode.Size = new System.Drawing.Size(32, 21);
            this.uComboInspectFaultTypeCode.TabIndex = 34;
            this.uComboInspectFaultTypeCode.Visible = false;
            // 
            // uLabelInspectFaultType
            // 
            this.uLabelInspectFaultType.Location = new System.Drawing.Point(884, 180);
            this.uLabelInspectFaultType.Name = "uLabelInspectFaultType";
            this.uLabelInspectFaultType.Size = new System.Drawing.Size(20, 20);
            this.uLabelInspectFaultType.TabIndex = 29;
            this.uLabelInspectFaultType.Text = "ultraLabel1";
            this.uLabelInspectFaultType.Visible = false;
            // 
            // uLabelInspectQty
            // 
            this.uLabelInspectQty.Location = new System.Drawing.Point(12, 132);
            this.uLabelInspectQty.Name = "uLabelInspectQty";
            this.uLabelInspectQty.Size = new System.Drawing.Size(100, 20);
            this.uLabelInspectQty.TabIndex = 27;
            this.uLabelInspectQty.Text = "ultraLabel1";
            // 
            // uLabelQty
            // 
            this.uLabelQty.Location = new System.Drawing.Point(12, 108);
            this.uLabelQty.Name = "uLabelQty";
            this.uLabelQty.Size = new System.Drawing.Size(100, 20);
            this.uLabelQty.TabIndex = 25;
            this.uLabelQty.Text = "ultraLabel1";
            // 
            // uDateAriseDate
            // 
            appearance112.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseDate.Appearance = appearance112;
            this.uDateAriseDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAriseDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateAriseDate.Location = new System.Drawing.Point(908, 204);
            this.uDateAriseDate.Name = "uDateAriseDate";
            this.uDateAriseDate.Size = new System.Drawing.Size(20, 21);
            this.uDateAriseDate.TabIndex = 18;
            this.uDateAriseDate.Visible = false;
            // 
            // uLabelAriseDate
            // 
            this.uLabelAriseDate.Location = new System.Drawing.Point(884, 204);
            this.uLabelAriseDate.Name = "uLabelAriseDate";
            this.uLabelAriseDate.Size = new System.Drawing.Size(20, 20);
            this.uLabelAriseDate.TabIndex = 23;
            this.uLabelAriseDate.Text = "ultraLabel1";
            this.uLabelAriseDate.Visible = false;
            // 
            // uLabelExpectProcess
            // 
            this.uLabelExpectProcess.Location = new System.Drawing.Point(276, 84);
            this.uLabelExpectProcess.Name = "uLabelExpectProcess";
            this.uLabelExpectProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelExpectProcess.TabIndex = 17;
            this.uLabelExpectProcess.Text = "ultraLabel1";
            // 
            // uLabelAriseEquip
            // 
            this.uLabelAriseEquip.Location = new System.Drawing.Point(540, 60);
            this.uLabelAriseEquip.Name = "uLabelAriseEquip";
            this.uLabelAriseEquip.Size = new System.Drawing.Size(100, 20);
            this.uLabelAriseEquip.TabIndex = 15;
            this.uLabelAriseEquip.Text = "ultraLabel1";
            // 
            // uLabelAriseProcess
            // 
            this.uLabelAriseProcess.Location = new System.Drawing.Point(276, 60);
            this.uLabelAriseProcess.Name = "uLabelAriseProcess";
            this.uLabelAriseProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelAriseProcess.TabIndex = 13;
            this.uLabelAriseProcess.Text = "ultraLabel1";
            // 
            // uTextProductCode
            // 
            appearance113.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance113;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(644, 12);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(150, 21);
            this.uTextProductCode.TabIndex = 9;
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(540, 12);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelProduct.TabIndex = 8;
            this.uLabelProduct.Text = "ultraLabel1";
            // 
            // uTextCustomerName
            // 
            appearance114.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance114;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(116, 36);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(150, 21);
            this.uTextCustomerName.TabIndex = 7;
            // 
            // uLabelCustomerName
            // 
            this.uLabelCustomerName.Location = new System.Drawing.Point(12, 36);
            this.uLabelCustomerName.Name = "uLabelCustomerName";
            this.uLabelCustomerName.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomerName.TabIndex = 6;
            this.uLabelCustomerName.Text = "ultraLabel1";
            // 
            // uTextLotNo
            // 
            appearance115.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.Appearance = appearance115;
            this.uTextLotNo.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLotNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLotNo.Location = new System.Drawing.Point(116, 60);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(150, 21);
            this.uTextLotNo.TabIndex = 5;
            this.uTextLotNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextLotNo_KeyDown);
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(12, 60);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelLotNo.TabIndex = 4;
            this.uLabelLotNo.Text = "ultraLabel1";
            // 
            // uLabelGubun
            // 
            this.uLabelGubun.Location = new System.Drawing.Point(276, 12);
            this.uLabelGubun.Name = "uLabelGubun";
            this.uLabelGubun.Size = new System.Drawing.Size(100, 20);
            this.uLabelGubun.TabIndex = 2;
            this.uLabelGubun.Text = "ultraLabel1";
            // 
            // uTextQCNNo
            // 
            appearance116.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQCNNo.Appearance = appearance116;
            this.uTextQCNNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextQCNNo.Location = new System.Drawing.Point(116, 12);
            this.uTextQCNNo.Name = "uTextQCNNo";
            this.uTextQCNNo.ReadOnly = true;
            this.uTextQCNNo.Size = new System.Drawing.Size(150, 21);
            this.uTextQCNNo.TabIndex = 3;
            // 
            // uLabelQCNNo
            // 
            this.uLabelQCNNo.Location = new System.Drawing.Point(12, 12);
            this.uLabelQCNNo.Name = "uLabelQCNNo";
            this.uLabelQCNNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelQCNNo.TabIndex = 2;
            this.uLabelQCNNo.Text = "ultraLabel1";
            // 
            // uComboPlantCode
            // 
            this.uComboPlantCode.Location = new System.Drawing.Point(900, 228);
            this.uComboPlantCode.Name = "uComboPlantCode";
            this.uComboPlantCode.Size = new System.Drawing.Size(28, 21);
            this.uComboPlantCode.TabIndex = 1;
            this.uComboPlantCode.Visible = false;
            this.uComboPlantCode.ValueChanged += new System.EventHandler(this.uComboPlantCode_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(876, 228);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 720);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 720);
            this.uGroupBoxContentsArea.TabIndex = 6;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // uGridQCNList
            // 
            appearance117.BackColor = System.Drawing.SystemColors.Window;
            appearance117.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridQCNList.DisplayLayout.Appearance = appearance117;
            this.uGridQCNList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridQCNList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance118.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance118.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance118.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance118.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.GroupByBox.Appearance = appearance118;
            appearance119.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance119;
            this.uGridQCNList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance120.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance120.BackColor2 = System.Drawing.SystemColors.Control;
            appearance120.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance120.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridQCNList.DisplayLayout.GroupByBox.PromptAppearance = appearance120;
            this.uGridQCNList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridQCNList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance121.BackColor = System.Drawing.SystemColors.Window;
            appearance121.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridQCNList.DisplayLayout.Override.ActiveCellAppearance = appearance121;
            appearance122.BackColor = System.Drawing.SystemColors.Highlight;
            appearance122.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridQCNList.DisplayLayout.Override.ActiveRowAppearance = appearance122;
            this.uGridQCNList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridQCNList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance123.BackColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.Override.CardAreaAppearance = appearance123;
            appearance124.BorderColor = System.Drawing.Color.Silver;
            appearance124.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridQCNList.DisplayLayout.Override.CellAppearance = appearance124;
            this.uGridQCNList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridQCNList.DisplayLayout.Override.CellPadding = 0;
            appearance125.BackColor = System.Drawing.SystemColors.Control;
            appearance125.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance125.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance125.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance125.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridQCNList.DisplayLayout.Override.GroupByRowAppearance = appearance125;
            appearance126.TextHAlignAsString = "Left";
            this.uGridQCNList.DisplayLayout.Override.HeaderAppearance = appearance126;
            this.uGridQCNList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridQCNList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance127.BackColor = System.Drawing.SystemColors.Window;
            appearance127.BorderColor = System.Drawing.Color.Silver;
            this.uGridQCNList.DisplayLayout.Override.RowAppearance = appearance127;
            this.uGridQCNList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance128.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridQCNList.DisplayLayout.Override.TemplateAddRowAppearance = appearance128;
            this.uGridQCNList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridQCNList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridQCNList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridQCNList.Location = new System.Drawing.Point(0, 40);
            this.uGridQCNList.Name = "uGridQCNList";
            this.uGridQCNList.Size = new System.Drawing.Size(1070, 800);
            this.uGridQCNList.TabIndex = 7;
            this.uGridQCNList.Text = "ultraGrid1";
            // 
            // frmINSZ0010_S
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGridQCNList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmINSZ0010_S";
            this.Load += new System.EventHandler(this.frmINSZ0010_Load);
            this.Activated += new System.EventHandler(this.frmINSZ0010_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmINSZ0010_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSerachAriseDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchAriseDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchQCNNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlantCode)).EndInit();
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridAffectLotList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            this.uGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckActionFlag1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupQCNUser)).EndInit();
            this.uGroupQCNUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxFaulty)).EndInit();
            this.uGroupBoxFaulty.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFaultType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxStdInfo)).EndInit();
            this.uGroupBoxStdInfo.ResumeLayout(false);
            this.uGroupBoxStdInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSYSTEM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProgram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboHandler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestFaultQty3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestInspectQty3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestFaultQty2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestInspectQty2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestFaultQty1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumRetestInspectQty1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboP_QcnItr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFaultImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridExpectEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotHoldState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotProcessState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboExpectProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAriseProcessCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPublishComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESTFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckPublishFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMESWorkUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumFaultQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInspectQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepEquipFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWorkStepMaterialFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFaultTypeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAriseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQCNNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridQCNList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProduct;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcessCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchQCNNo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSerachAriseDateTo;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchAriseDateFrom;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchAriseDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchQCNNo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupQCNUser;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQCNUser;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxFaulty;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxStdInfo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSYSTEM;
        private Infragistics.Win.Misc.UltraLabel uLabelSYSTEM;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProgram;
        private Infragistics.Win.Misc.UltraLabel uLabelProgram;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboHandler;
        private Infragistics.Win.Misc.UltraLabel uLabelHandler;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRetestFaultQty3;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRetestInspectQty3;
        private Infragistics.Win.Misc.UltraLabel uLabelRetest3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRetestFaultQty2;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRetestInspectQty2;
        private Infragistics.Win.Misc.UltraLabel uLabelRetest2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRetestFaultQty1;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumRetestInspectQty1;
        private Infragistics.Win.Misc.UltraLabel uLabelRetest1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboP_QcnItr;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFaultImg;
        private Infragistics.Win.Misc.UltraLabel uLabelFaultImg;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridExpectEquipList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqSeq;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReqNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotHoldState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotProcessState;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboAriseEquipCode;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboExpectProcessCode;
        private Infragistics.Win.UltraWinGrid.UltraCombo uComboAriseProcessCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPublishComplete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESTFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerProductSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerProductSpec;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckPublishFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelPublishFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMESWorkUserID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAriseTime;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumFaultQty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumInspectQty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumQty;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckWorkStepEquipFlag;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckWorkStepMaterialFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkStep;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectFaultTypeCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectFaultType;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectQty;
        private Infragistics.Win.Misc.UltraLabel uLabelQty;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAriseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseDate;
        private Infragistics.Win.Misc.UltraLabel uLabelExpectProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelAriseProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelGubun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextQCNNo;
        private Infragistics.Win.Misc.UltraLabel uLabelQCNNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlantCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridQCNList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRowFaulty;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFaultType;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkUser;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag9;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag8;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag7;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag6;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag5;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag4;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag3;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckActionFlag1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridAffectLotList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDept;
        private Infragistics.Win.Misc.UltraLabel uLabelDept;
    }
}