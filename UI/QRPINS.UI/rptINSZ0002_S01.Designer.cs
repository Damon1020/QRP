﻿namespace QRPINS.UI
{
    /// <summary>
    /// Summary description for rptINSZ0002_S01.
    /// </summary>
    partial class rptINSZ0002_S01
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptINSZ0002_S01));
            this.lblResult = new DataDynamics.ActiveReports.Label();
            this.lblGubun = new DataDynamics.ActiveReports.Label();
            this.label1 = new DataDynamics.ActiveReports.Label();
            this.label2 = new DataDynamics.ActiveReports.Label();
            this.label3 = new DataDynamics.ActiveReports.Label();
            this.label4 = new DataDynamics.ActiveReports.Label();
            this.label5 = new DataDynamics.ActiveReports.Label();
            this.label6 = new DataDynamics.ActiveReports.Label();
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.txtGubun = new DataDynamics.ActiveReports.TextBox();
            this.txtInspectQty = new DataDynamics.ActiveReports.TextBox();
            this.txtBox = new DataDynamics.ActiveReports.TextBox();
            this.txtFaulty = new DataDynamics.ActiveReports.TextBox();
            this.txtResult = new DataDynamics.ActiveReports.TextBox();
            this.txtEtc = new DataDynamics.ActiveReports.TextBox();
            this.txtFaultyQty = new DataDynamics.ActiveReports.TextBox();
            this.groupHeader = new DataDynamics.ActiveReports.GroupHeader();
            this.groupFooter = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInspectQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaulty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaultyQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // lblResult
            // 
            this.lblResult.Height = 0.2F;
            this.lblResult.HyperLink = null;
            this.lblResult.Left = 0F;
            this.lblResult.Name = "lblResult";
            this.lblResult.Style = "font-size: 10pt; font-weight: bold; text-align: center; vertical-align: middle";
            this.lblResult.Text = "2. 검사 결과";
            this.lblResult.Top = 0F;
            this.lblResult.Width = 1.125F;
            // 
            // lblGubun
            // 
            this.lblGubun.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblGubun.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.lblGubun.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblGubun.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.lblGubun.Height = 0.22F;
            this.lblGubun.HyperLink = null;
            this.lblGubun.Left = 0F;
            this.lblGubun.Name = "lblGubun";
            this.lblGubun.Style = "font-weight: normal; text-align: center; text-justify: distribute-all-lines; vert" +
                "ical-align: middle";
            this.lblGubun.Text = "구분";
            this.lblGubun.Top = 0.2F;
            this.lblGubun.Width = 1F;
            // 
            // label1
            // 
            this.label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.label1.Height = 0.22F;
            this.label1.HyperLink = null;
            this.label1.Left = 1F;
            this.label1.Name = "label1";
            this.label1.Style = "font-weight: normal; text-align: center; text-justify: distribute-all-lines; vert" +
                "ical-align: middle";
            this.label1.Text = "시료수";
            this.label1.Top = 0.2F;
            this.label1.Width = 0.7400001F;
            // 
            // label2
            // 
            this.label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.label2.Height = 0.22F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.74F;
            this.label2.Name = "label2";
            this.label2.Style = "font-weight: normal; text-align: center; text-justify: distribute-all-lines; vert" +
                "ical-align: middle";
            this.label2.Text = "판정 기준";
            this.label2.Top = 0.2F;
            this.label2.Width = 0.796F;
            // 
            // label3
            // 
            this.label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.label3.Height = 0.22F;
            this.label3.HyperLink = null;
            this.label3.Left = 3.167F;
            this.label3.Name = "label3";
            this.label3.Style = "font-weight: normal; text-align: center; text-justify: distribute-all-lines; vert" +
                "ical-align: middle";
            this.label3.Text = "불량내용";
            this.label3.Top = 0.2F;
            this.label3.Width = 1.905F;
            // 
            // label4
            // 
            this.label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.label4.Height = 0.22F;
            this.label4.HyperLink = null;
            this.label4.Left = 5.072F;
            this.label4.Name = "label4";
            this.label4.Style = "font-weight: normal; text-align: center; text-justify: distribute-all-lines; vert" +
                "ical-align: middle";
            this.label4.Text = "판정결과";
            this.label4.Top = 0.2F;
            this.label4.Width = 0.7399998F;
            // 
            // label5
            // 
            this.label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.label5.Height = 0.22F;
            this.label5.HyperLink = null;
            this.label5.Left = 5.812F;
            this.label5.Name = "label5";
            this.label5.Style = "font-weight: normal; text-align: center; text-justify: distribute-all-lines; vert" +
                "ical-align: middle";
            this.label5.Text = "비고";
            this.label5.Top = 0.2F;
            this.label5.Width = 1.467999F;
            // 
            // label6
            // 
            this.label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.ExtraThickSolid;
            this.label6.Height = 0.22F;
            this.label6.HyperLink = null;
            this.label6.Left = 2.536F;
            this.label6.Name = "label6";
            this.label6.Style = "font-weight: normal; text-align: center; text-justify: distribute-all-lines; vert" +
                "ical-align: middle";
            this.label6.Text = "불량수";
            this.label6.Top = 0.2F;
            this.label6.Width = 0.631F;
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtGubun,
            this.txtInspectQty,
            this.txtBox,
            this.txtFaulty,
            this.txtResult,
            this.txtEtc,
            this.txtFaultyQty});
            this.detail.Height = 0.397F;
            this.detail.Name = "detail";
            // 
            // txtGubun
            // 
            this.txtGubun.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGubun.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.ThickSolid;
            this.txtGubun.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGubun.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtGubun.DataField = "InspectTypeName";
            this.txtGubun.Height = 0.397F;
            this.txtGubun.Left = 0F;
            this.txtGubun.Name = "txtGubun";
            this.txtGubun.Style = "font-size: 9pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtGubun.Text = null;
            this.txtGubun.Top = 0F;
            this.txtGubun.Width = 1F;
            // 
            // txtInspectQty
            // 
            this.txtInspectQty.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInspectQty.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInspectQty.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInspectQty.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtInspectQty.DataField = "SampleSize";
            this.txtInspectQty.Height = 0.397F;
            this.txtInspectQty.Left = 1F;
            this.txtInspectQty.Name = "txtInspectQty";
            this.txtInspectQty.Style = "font-size: 9pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtInspectQty.Text = null;
            this.txtInspectQty.Top = 0F;
            this.txtInspectQty.Width = 0.7400001F;
            // 
            // txtBox
            // 
            this.txtBox.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtBox.DataField = "Stand";
            this.txtBox.Height = 0.397F;
            this.txtBox.Left = 1.74F;
            this.txtBox.Name = "txtBox";
            this.txtBox.Style = "font-size: 9pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtBox.Text = null;
            this.txtBox.Top = 0F;
            this.txtBox.Width = 0.796F;
            // 
            // txtFaulty
            // 
            this.txtFaulty.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaulty.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaulty.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaulty.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaulty.DataField = "FaultTypeName";
            this.txtFaulty.Height = 0.397F;
            this.txtFaulty.Left = 3.167F;
            this.txtFaulty.Name = "txtFaulty";
            this.txtFaulty.Style = "font-size: 9pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtFaulty.Text = null;
            this.txtFaulty.Top = 0F;
            this.txtFaulty.Width = 1.905F;
            // 
            // txtResult
            // 
            this.txtResult.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtResult.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtResult.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtResult.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtResult.DataField = "InspectResultFlag";
            this.txtResult.Height = 0.397F;
            this.txtResult.Left = 5.072F;
            this.txtResult.Name = "txtResult";
            this.txtResult.Style = "font-size: 9pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtResult.Text = null;
            this.txtResult.Top = 0F;
            this.txtResult.Width = 0.7399998F;
            // 
            // txtEtc
            // 
            this.txtEtc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtEtc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtEtc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtEtc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtEtc.DataField = "EtcDesc";
            this.txtEtc.Height = 0.397F;
            this.txtEtc.Left = 5.812F;
            this.txtEtc.Name = "txtEtc";
            this.txtEtc.Style = "font-size: 9pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtEtc.Text = null;
            this.txtEtc.Top = 0F;
            this.txtEtc.Width = 1.467999F;
            // 
            // txtFaultyQty
            // 
            this.txtFaultyQty.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaultyQty.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaultyQty.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaultyQty.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtFaultyQty.DataField = "FaultQty";
            this.txtFaultyQty.Height = 0.397F;
            this.txtFaultyQty.Left = 2.536F;
            this.txtFaultyQty.Name = "txtFaultyQty";
            this.txtFaultyQty.Style = "font-size: 9pt; text-align: center; text-justify: distribute-all-lines; vertical-" +
                "align: middle";
            this.txtFaultyQty.Text = null;
            this.txtFaultyQty.Top = 0F;
            this.txtFaultyQty.Width = 0.631F;
            // 
            // groupHeader
            // 
            this.groupHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label5,
            this.lblGubun,
            this.label1,
            this.label2,
            this.label3,
            this.label4,
            this.lblResult,
            this.label6});
            this.groupHeader.Height = 0.42F;
            this.groupHeader.Name = "groupHeader";
            // 
            // groupFooter
            // 
            this.groupFooter.Height = 0F;
            this.groupFooter.Name = "groupFooter";
            // 
            // rptINSZ0002_S01
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.280001F;
            this.Sections.Add(this.groupHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInspectQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaulty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaultyQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.Label lblResult;
        private DataDynamics.ActiveReports.Label lblGubun;
        private DataDynamics.ActiveReports.Label label1;
        private DataDynamics.ActiveReports.Label label2;
        private DataDynamics.ActiveReports.Label label3;
        private DataDynamics.ActiveReports.Label label4;
        private DataDynamics.ActiveReports.Label label5;
        private DataDynamics.ActiveReports.TextBox txtGubun;
        private DataDynamics.ActiveReports.TextBox txtInspectQty;
        private DataDynamics.ActiveReports.TextBox txtBox;
        private DataDynamics.ActiveReports.TextBox txtFaulty;
        private DataDynamics.ActiveReports.TextBox txtResult;
        private DataDynamics.ActiveReports.TextBox txtEtc;
        private DataDynamics.ActiveReports.Label label6;
        private DataDynamics.ActiveReports.TextBox txtFaultyQty;
        private DataDynamics.ActiveReports.GroupHeader groupHeader;
        private DataDynamics.ActiveReports.GroupFooter groupFooter;
    }
}
