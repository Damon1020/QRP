﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 수입검사관리                                          */
/* 프로그램ID   : frmINS0004.cs                                         */
/* 프로그램명   : 수입검사등록 / 조회                                   */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-24 : 기능 추가 (이종호)                       */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using QRPDB;
using System.Data.SqlClient;

namespace QRPINS.UI
{
    public partial class frmINS0004 : Form, IToolbar
    {
        ////Infragistics.Win.UltraWinToolTip.UltraToolTipManager uToolTipManager = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager();

        #region 전역변수
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 

        // AQL 적용인지 비적용인지를 알기위한 Bool변수(true : AQL 적용 / false : AQL 비적용)
        private bool m_bolCheckAQL = false;

        // 합격, 불합격 판정갯수 저장하기 위한 전역변수
        private int m_intAcceptCount = 0;
        private int m_intRejectCount = 0;
        private double m_dblAQLSampleSize = 0.0;

        // 자재입고조회(양산품) 화면에서 넘겨받을 변수 속성
        private string m_strPlantCode;
        private string m_strVendorCode;
        private string m_strMaterialCode;
        private string m_strSpecNo;
        private string m_strMoldSeq;
        private string m_strGRDate;
        private string m_strGRNo;        
        private string m_strMoveFormName;
        private string m_strReqNo;
        private string m_strReqSeq;
        private string m_strFloorPlan;
        private string m_strReceiptTime;

        public String PlantCode
        {
            get { return m_strPlantCode; }
            set { m_strPlantCode = value; }
        }

        public String VendorCode
        {
            get { return m_strVendorCode; }
            set { m_strVendorCode = value; }
        }

        public String MaterialCode
        {
            get { return m_strMaterialCode; }
            set { m_strMaterialCode = value; }
        }

        public string SpecNo
        {
            get { return m_strSpecNo; }
            set { m_strSpecNo = value; }
        }

        public string MoldSeq
        {
            get { return m_strMoldSeq; }
            set { m_strMoldSeq = value; }
        }

        public String GRDate
        {
            get { return m_strGRDate; }
            set { m_strGRDate = value; }
        }

        public string GRNo
        {
            get { return m_strGRNo; }
            set { m_strGRNo = value; }
        }

        public string MoveFormName
        {
            get { return m_strMoveFormName; }
            set { m_strMoveFormName = value; }
        }

        public string ReqNo
        {
            get { return m_strReqNo; }
            set { m_strReqNo = value; }
        }

        public string ReqSeq
        {
            get { return m_strReqSeq; }
            set { m_strReqSeq = value; }
        }

        public string FloorPlan
        {
            get { return m_strFloorPlan; }
            set { m_strFloorPlan = value; }
        }

        public string ReceipTime
        {
            get { return m_strReceiptTime; }
            set { m_strReceiptTime = value; }
        }
        #endregion

        public frmINS0004()
        {
            InitializeComponent();
        }

        private void frmINS0004_Activated(object sender, EventArgs e)
        {
            // 툴바 활성화 여부 설정
            QRPBrowser InitToolBar = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            InitToolBar.mfActiveToolBar(this.ParentForm, true, true, false, false, true, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINS0004_Load(object sender, EventArgs e)
        {
            Size size = new Size(1070, 850);
            this.MinimumSize = size;

            ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);
            // Title 설정
            titleArea.mfSetLabelText("수입검사등록 / 조회", m_SysRes.GetString("SYS_FONTNAME"), 12);

            // 컨트롤 초기화
            SetRunMode();
            SetToolAuth();
            InitEtc();
            InitTab();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();

            this.uDateInspectTime.Value = DateTime.Now.ToString("HH:mm:ss");        // 24시표현


            if (m_strMoveFormName != null || m_strMoveFormName != string.Empty)
            {
                QRPCOM.QRPGLO.QRPBrowser brw = new QRPBrowser();
                brw.mfSetFormLanguage(this);
            }

            if (m_strMoveFormName == "frmINSZ0003")
            {
                CommonControl cControl = new CommonControl();
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                // Tab 명 변경
                if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0004"))
                {
                    uTabMenu.Tabs["QRPINS" + "," + "frmINS0004"].Text = titleArea.TextName;
                }

                titleArea.TextName = titleArea.TextName + "(" + uTabMenu.Tabs["QRPINS,frmINSZ0003"].Text + ")";

                // 조회 Method 호출
                Get_INSMaterialGR();

                this.uGroupBoxContentsArea.Expanded = true;
            }
            else if (MoveFormName == "frmINSZ0002")
            {
                CommonControl cControl = new CommonControl();
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                // Tab 명 변경
                if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0004"))
                {
                    uTabMenu.Tabs["QRPINS" + "," + "frmINS0004"].Text = titleArea.TextName;
                }
                titleArea.TextName = titleArea.TextName + "(" + uTabMenu.Tabs["QRPINS,frmINSZ0002"].Text + ")";

                //titleArea.TextName = titleArea.TextName + "(신규자재 인증등록 이동)";

                // 신규자재에서 처음 넘어온 경우
                if (ReqNo.Equals(string.Empty) && ReqSeq.Equals(string.Empty))
                {
                    // 조회 Method 호출
                    Get_INSMaterialGR();
                }
                else
                {
                    // 이미 저장된 상태에서 화면이동한 경우
                    // 수입검사 테이블에 저장된 정보 조회
                    SearchInspectReqHDetail(PlantCode, ReqNo, ReqSeq);
                    SearchInspectReqLot(PlantCode, ReqNo, ReqSeq);
                    SearchInspectReqItem(PlantCode, ReqNo, ReqSeq);
                    SearchInspectReqItemSampling(PlantCode, ReqNo, ReqSeq);
                }

                this.uGroupBoxContentsArea.Expanded = true;
            }
            else if (MoveFormName == "frmINS0017")
            {

                //Point point = new Point(0, 0);
                //this.Location = point;
                titleArea.TextName = titleArea.TextName + "(원자재이상발생 이동)";

                // 이미 저장된 상태에서 화면이동한 경우
                // 수입검사 테이블에 저장된 정보 조회
                SearchInspectReqHDetail(PlantCode, ReqNo, ReqSeq);
                SearchInspectReqLot(PlantCode, ReqNo, ReqSeq);
                SearchInspectReqItem(PlantCode, ReqNo, ReqSeq);
                SearchInspectReqItemSampling(PlantCode, ReqNo, ReqSeq);
                

                this.uGroupBoxContentsArea.Expanded = true;
            
            }
            else
            {
                this.uGroupBoxContentsArea.Expanded = false;
            }

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    //MessageBox.Show(this.Tag.ToString());
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmINS0004_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                ResourceSet m_SysRes = new ResourceSet(SysRes.SystemInfoRes);

                // 기본값 설정
                this.uTextInspectUserID.Text = m_SysRes.GetString("SYS_USERID");
                this.uTextInspectUserName.Text = m_SysRes.GetString("SYS_USERNAME");

                this.uDateSearchFromInspectDate.Value = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                this.uDateSearchToInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");


                // 최대입력길이 설정
                this.uTextSearchMaterialCode.MaxLength = 20;
                this.uTextSearchVendorCode.MaxLength = 10;
                this.uTextSearchLotNo.MaxLength = 50;
                this.uTextFaultDesc.MaxLength = 200;
                this.uTextInspectUserID.MaxLength = 20;
                this.uTextReviewUserID.MaxLength = 20;
                this.uTextAdmitUserID.MaxLength = 20;
                this.uTextEtcDescQA.MaxLength = 200;

                //////승인자 이름, ID 고정, 수정불가
                ////this.uTextAdmitUserID.ReadOnly = true;
                ////this.uTextAdmitUserID.Text = "ST03420A";
                ////this.uTextAdmitUserName.Text = "조철희";

                // TextBox 정렬 지정
                this.uTextGRQty.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextLotCount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextShipmentCount.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                this.uTextSamplingLot.Appearance.TextHAlign = Infragistics.Win.HAlign.Right;

                this.uTextSearchMaterialCode.AlwaysInEditMode = true;
                this.uTextSearchVendorCode.AlwaysInEditMode = true;

                //this.uTextSearchMaterialName.ShowOverflowIndicator = true;
                //this.uTextSearchVendorCode.ShowOverflowIndicator = true;

                this.uButtonExcelUpload.Hide();
                this.uButtonExcelUpload.Enabled = false;

                this.uDateInspectTime.Value = null;

                this.uDateTimeReceiptTime.ReadOnly = true;
                this.uDateReceiptDate.ReadOnly = true;

                this.uTextSamplingLot.ReadOnly = false;
                this.uTextSamplingLot.Appearance.BackColor = Color.Empty;

                this.uOptionPassFailFlag.ValueChanged += new EventHandler(uOptionPassFailFlag_ValueChanged);
                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
                this.uDateInspectDate.ValueChanged += new EventHandler(uDateInspectDate_ValueChanged);
                this.uDateInspectTime.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(uDateInspectTime_EditorSpinButtonClick);
                this.uTextAdmitUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextAdmitUserID_EditorButtonClick);
                this.uTextAdmitUserID.KeyDown += new KeyEventHandler(uTextAdmitUserID_KeyDown);
                this.uTextInspectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextInspectUserID_EditorButtonClick);
                this.uTextInspectUserID.KeyDown += new KeyEventHandler(uTextInspectUserID_KeyDown);
                this.uTextReviewUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextReviewUserID_EditorButtonClick);
                this.uTextReviewUserID.KeyDown += new KeyEventHandler(uTextReviewUserID_KeyDown);
                this.uTextSamplingLot.KeyPress += new KeyPressEventHandler(uTextSamplingLot_KeyPress);
                this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSearchMaterialCode_EditorButtonClick);
                this.uTextSearchMaterialCode.KeyDown += new KeyEventHandler(uTextSearchMaterialCode_KeyDown);
                this.uTextSearchVendorCode.KeyDown += new KeyEventHandler(uTextSearchVendorCode_KeyDown);
                this.uTextSearchVendorCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(uTextSearchVendorCode_EditorButtonClick);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));

                // 전수검사탭 히든
                this.uTab.Tabs[2].Visible = false;

                this.uTab.Tabs[0].VisibleIndex = 1;
                this.uTab.Tabs[1].VisibleIndex = 0;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectDate, "입고일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchInspectResultFlag, "검사결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterialGroup, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReqNo, "관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelGRQty, "수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMoldSeq, "금형차수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpecNo, "도면번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMaterialGrade, "검사등급", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomer, "거래처", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGRDate, "입고일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFloorPlanNo, "도면No", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotCount, "Lot수량", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelShipmentCount, "ShipCount", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.ultraLabel1, "Sampling Lot", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelReceiptDate, "접수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelJudge, "합부판정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInspectResult, "검사처리결과", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFaultContent, "불량내용", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelICPCheck, "ICP Check", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelEtcQA, "비고(QA)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTestUser, "검사자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReviewUser, "검토자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAcceptUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.ultraButton1, "검사 Lot 적용", m_resSys.GetString("SYS_FONTNAME"), null);
                wButton.mfSetButton(this.uButtonMoveAbnormal, "원자재이상발생 이동", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonExcelUpload, "Excel Upload", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                this.uButtonExcelUpload.Hide();
                this.uButtonExcelUpload.Enabled = false;

                this.uButtonMoveAbnormal.Hide();
                this.uButtonMoveAbnormal.Enabled = false;

                this.uButtonExcelUpload.Click += new EventHandler(uButtonExcelUpload_Click);
                this.uButtonMoveAbnormal.Click += new EventHandler(uButtonMoveAbnormal_Click);
                this.ultraButton1.Click += new EventHandler(ultraButton1_Click);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // PlantComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchPlant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // 검사결과
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtInspectResult = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchInspectResultFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "전체", "ComCode", "ComCodeName", dtInspectResult);

                // 자재종류
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsum = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsConsum);

                DataTable dtConsum = clsConsum.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchMaterialGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "ConsumableTypeCode", "ConsumableTypeName", dtConsum);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 조회 그리드
                // 수입검사 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridInspectList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ReqNumber", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ReqNo", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ReqSeq", "관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 4
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "GRDate", "입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ConsumableTypeCode", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ConsumableTypeName", "자재종류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridInspectList, 0, "MaterialGroupName", "자재그룹", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "MoldSeq", "금형차수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 70, false, false, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                //wGrid.mfSetGridColumn(this.uGridInspectList, 0, "MaterialSpec1", "규격1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "FloorPlanNo", "도면No", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "SpecNo", "Rev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "TotalQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "n,nnn,nnn,nnn", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "SamplingLotCount", "Lot수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "MaterialGrade", "검사등급", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ShipmentCount", "ShipmentCount", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnn,nnn", "0");
                
                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "PassFailFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "AdmitDate", "승인일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "EtcDescQA", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "WMSTFlag", "WMS전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "CompleteFlag", "완료여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ReqDateTime", "요청일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "ReceiptDateTime", "접수일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectList, 0, "InspectDateTime", "판정일시", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set PontSize
                this.uGridInspectList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridInspectList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                #region Lot Grid
                // LotNo Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridLot, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridLot, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "ReqLotSeq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "GRNo", "입고번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "MoLotNo", "모 LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "COCFilePath", "COC", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "COCFile", "COCFile", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "LotSize", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "ManuFactureDate", "제조일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGridLot, 0, "ExpirationDate", "유효기간", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                ////wGrid.mfSetGridColumn(this.uGridLot, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                // Set FontSize
                this.uGridLot.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridLot.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드 헤더 체크박스 없애기
                this.uGridLot.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                // MaskInput 설정
                this.uGridLot.DisplayLayout.Bands[0].Columns["LotSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                #endregion

                #region InspectItem Grid
                // 검사결과 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridReqItem, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "ReqNo", "의뢰번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "ReqSeq", "의뢰순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridReqItem, 0, "ReqLotSeq", "의뢰Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //wGrid.mfSetGridColumn(this.uGridReqItem, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnn", "0");

                //wGrid.mfSetGridColumn(this.uGridReqItem, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "SampleSize", "검사수량", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "SpecDesc", "규격설명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGridReqItem, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqItem, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGridReqItem, 0, "AcceptCount", "합격판정갯수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                ////wGrid.mfSetGridColumn(this.uGridReqItem, 0, "RejectCount", "불합격판정갯수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                // Set FontSize
                this.uGridReqItem.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridReqItem.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGridReqItem.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                #endregion

                #region Sampling Item Grid
                // 샘플링검사 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridReqSampling, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "ReqNo", "의뢰번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "ReqSeq", "의뢰순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 4
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "ReqLotSeq", "의뢰Lot순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "ReqItemSeq", "검사항목순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                //                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid3, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "GrandSampleSize", "S/S", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "GrandEveryTarget", "S/S 대상", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "SampleSize", "검사수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "FaultQty", "불량수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "Point", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "UnitName", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "LowerSpec", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                //wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "UpperSpec", "USL", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-10.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "SpecRange", "R", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "Mean", "평균", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "MaxValue", "MAX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "MinValue", "Min", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "DataRange", "Range", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "StdDev", "StDev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "Cp", "Cp", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "Cpk", "Cpk", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:-15.5}", "0.0");

                wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "InspectResultFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "OK");

                //wGrid.mfSetGridColumn(this.uGridReqSampling, 0, "TargetIngFlag", "Ship단위처리여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                // Set PontSize
                this.uGridReqSampling.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridReqSampling.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // Set DataErrorInfo 
                this.uGridReqSampling.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.RowsAndCells;
                this.uGridReqSampling.DisplayLayout.Override.DataErrorCellAppearance.ForeColor = Color.Red;
                this.uGridReqSampling.DisplayLayout.Override.DataErrorRowAppearance.BackColor = Color.Salmon;

                this.uGridReqSampling.DisplayLayout.Bands[0].Columns["GrandSampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridReqSampling.DisplayLayout.Bands[0].Columns["SampleSize"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridReqSampling.DisplayLayout.Bands[0].Columns["FaultQty"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uGridReqSampling.DisplayLayout.Bands[0].Columns["Point"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;

                //Key Mapping Clear
                this.uGridReqSampling.KeyActionMappings.Clear();
                #endregion

                #region 전수검사
                // 전수검사 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid4, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid4, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "시험항목", "시험항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "검사항목", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "검사조건", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "규격", "규격", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "LSL", "LSL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "Nom.", "Nom.", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "USL", "USL", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "R", "R", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid4.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid4.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid5, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid5, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid5, 0, "측정값", "측정값", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid5, 0, "합/부", "합/부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid5.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid5.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                #endregion

                // 검사결과 드랍다운 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtComCode = clsComCode.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));
                // 헤더
                wGrid.mfSetGridColumnValueList(this.uGridInspectList, 0, "PassFailFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);
                // 검사결과
                wGrid.mfSetGridColumnValueList(this.uGridReqItem, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);
                // Lot
                wGrid.mfSetGridColumnValueList(this.uGridLot, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);
                // Sampling
                wGrid.mfSetGridColumnValueList(this.uGridReqSampling, 0, "InspectResultFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtComCode);

                // SpecRange 드랍다운 설정
                dtComCode = clsComCode.mfReadCommonCode("C0032", m_resSys.GetString("SYS_LANG"));
                // 검사결과
                wGrid.mfSetGridColumnValueList(this.uGridReqItem, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);
                // Sampling
                wGrid.mfSetGridColumnValueList(this.uGridReqSampling, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);

                // Sample 대상
                dtComCode = clsComCode.mfReadCommonCode("C0018", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridReqSampling, 0, "GrandEveryTarget", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtComCode);

                // 규격단위
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();

                wGrid.mfSetGridColumnValueList(this.uGridReqItem, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);
                wGrid.mfSetGridColumnValueList(this.uGridReqSampling, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUnit);

                this.uGridInspectList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridInspectList_DoubleClickRow);
                this.uGridInspectList.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGridInspectList_InitializeRow);
                this.uGridLot.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridLot_CellChange);
                this.uGridLot.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridLot_ClickCellButton);
                this.uGridLot.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(uGridLot_DoubleClickCell);
                this.uGridLot.KeyDown += new KeyEventHandler(uGridLot_KeyDown);
                this.uGridReqItem.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGridReqItem_InitializeRow);
                this.uGridReqSampling.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridReqSampling_AfterCellUpdate);
                this.uGridReqSampling.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(uGridReqSampling_InitializeRow);
                this.uGridReqSampling.KeyDown += new KeyEventHandler(uGridReqSampling_KeyDown);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (Convert.ToDateTime(this.uDateSearchFromInspectDate.Value).CompareTo(Convert.ToDateTime(this.uDateSearchToInspectDate.Value)) > 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000205", "M000052", Infragistics.Win.HAlign.Center);
                    return;
                }

                // 검색조건 변수 생성
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strMaterialCode = this.uTextSearchMaterialCode.Text.Trim();;
                String strVendorCode = this.uTextSearchVendorCode.Text.Trim(); ;
                String strFromGRDate = Convert.ToDateTime(this.uDateSearchFromInspectDate.Value).ToString("yyyy-MM-dd");
                String strToGRDate = Convert.ToDateTime(this.uDateSearchToInspectDate.Value).ToString("yyyy-MM-dd");
                string strLotNo = this.uTextSearchLotNo.Text.Trim();
                string strInspectResultFlag = this.uComboSearchInspectResultFlag.Value.ToString();
                string strMaterialGroupCode = this.uComboSearchMaterialGroup.Value.ToString();

                QRPINS.BL.INSIMP.MatInspectReqH clsHeader;
                // 
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                    clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH(m_strDBConn);
                }

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 검색 Method 호출
                DataTable dtSearch = clsHeader.mfReadINSMatInspectReqH(strPlantCode, strMaterialCode, strVendorCode
                                                                    , strMaterialGroupCode, strLotNo, strInspectResultFlag
                                                                    , strFromGRDate, strToGRDate, m_resSys.GetString("SYS_LANG"));

                this.uGridInspectList.DataSource = dtSearch;
                this.uGridInspectList.DataBind();

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 조회결과 없을시 MessageBox 로 알림
                if (dtSearch.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridInspectList, 0);
                }

                // ContentsArea 그룹박스 접힌 상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000397", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else if (this.uCheckCompleteFlag.Enabled == false && this.uCheckCompleteFlag.Checked && this.uTextWMSTransFlag.Text.Equals("T"))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000984", "M000993", Infragistics.Win.HAlign.Center);

                    //mfSearch();
                    return;
                }
                else if (!(this.uGridLot.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M001047", Infragistics.Win.HAlign.Center);

                    return;
                }
                else if (!(this.uGridReqSampling.Rows.Count > 0) && this.uTextReqNo.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M000169", Infragistics.Win.HAlign.Center);

                    return;
                }
                else if (!(this.uGridReqSampling.Rows.Count > 0) && !this.uTextReqNo.Text.Equals(string.Empty))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M001040", Infragistics.Win.HAlign.Center);

                    return;
                }
                else if (this.uOptionPassFailFlag.CheckedIndex == -1)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001032", "M000175", Infragistics.Win.HAlign.Center);

                    return;
                }
                else
                {
                    string strMsg = string.Empty;
                    if (!this.uCheckCompleteFlag.Enabled && !this.uTextWMSTransFlag.Text.Equals("T"))
                    {
                        strMsg = "M001355";
                    }
                    else
                    {
                        strMsg = "M000936";
                    }

                    if (this.uCheckCompleteFlag.Enabled && this.uCheckCompleteFlag.Checked)
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000984", "M000982",
                                        Infragistics.Win.HAlign.Right);
                    }

                    // 저장여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001053", strMsg, Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        // ActiveCell 첫번째 행 첫번째 열로 이동
                        this.uGridReqSampling.ActiveCell = this.uGridReqSampling.Rows[0].Cells[0];

                        int intStart = this.uGridReqSampling.DisplayLayout.Bands[0].Columns["1"].Index;

                        DataTable dtHeader = new DataTable();
                        DataTable dtLot = new DataTable();
                        DataTable dtItem = new DataTable();
                        DataTable dtMeasure = new DataTable();
                        DataTable dtCount = new DataTable();
                        DataTable dtOkNo = new DataTable();
                        DataTable dtDesc = new DataTable();
                        DataTable dtSelect = new DataTable();
                        DataTable dtAML = new DataTable();

                        if (this.uCheckCompleteFlag.Enabled)
                        {
                            // 저장할 DataTable 설정 Method 호출
                            dtHeader = RtnHeaderDataTable();
                            dtLot = RtnLotDataTable();
                            dtItem = RtnItemDataTable();
                            dtMeasure = RtnResultMeasure(intStart);
                            dtCount = RtnResultCount(intStart);
                            dtOkNo = RtnResultOkNg(intStart);
                            dtDesc = RtnResultDesc(intStart);
                            dtSelect = RtnResultSelect(intStart);
                            dtAML = RtnAMLDataTable();
                        }
                        DataTable dtWMS = RtnWMSDataTable();

                        String strErrRtn = "";

                        QRPINS.BL.INSIMP.MatInspectReqH clsHeader;
                        // 
                        if (m_bolDebugMode == false)
                        {
                            // BL 연결
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                            clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH();
                            brwChannel.mfCredentials(clsHeader);
                        }
                        else
                        {
                            clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH(m_strDBConn);
                        }

                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // Method 호출
                        strErrRtn = clsHeader.mfSaveINSMatInspectReqH(dtHeader, dtLot, dtItem, dtMeasure, dtCount, dtOkNo, dtDesc, dtSelect, dtAML
                                                        , m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_LANG"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        String strFullReqNo = this.uTextReqNo.Text;
                        String strReqNo = strFullReqNo.Substring(0, 7);
                        String strReqSeq = strFullReqNo.Substring(7, 4);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            FileUpload(strReqNo, strReqSeq);
                        }

                        // 저장 성공이고 작성완료 체크되었을시 WMS 검사결과 전송
                        // 조건 추가(2012.12.07) : 합격일때만 전송
                        if (ErrRtn.ErrNum == 0 && this.uCheckCompleteFlag.Checked && !this.uTextWMSTransFlag.Text.Equals("T") && this.uOptionPassFailFlag.CheckedIndex.Equals(0))
                        {
                            // 수입검사결과 WMS 전송 메소드 호출
                            //strErrRtn = clsHeader.mfSaveWMSMATInspectReq(dtWMS, this.uTextInspectUserID.Text, m_resSys.GetString("SYS_USERIP"));
                            strErrRtn = mfSaveWMSMATInspectReq(dtWMS, this.uTextInspectUserID.Text, m_resSys.GetString("SYS_USERIP"));
                            // 검사결과
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrMessage.Equals("00") && ErrRtn.ErrNum.Equals(0))
                            {
                                // 전송성공이면 WMSTFlag 업데이트 메소드 호출
                                strErrRtn = clsHeader.mfSaveINSMatInspectReqH_WMSTFlag(dtWMS, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                                // 검사결과
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                // 처리결과에 따른 메세지 박스
                                if (ErrRtn.ErrNum == 0)
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                                    // List Refresh
                                    mfSearch();
                                }
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M000156", "M000751", "M000748",
                                                Infragistics.Win.HAlign.Right);
                            }
                        }
                        else if (ErrRtn.ErrNum.Equals(0) && this.uCheckCompleteFlag.Checked && this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                        {
                            // 저장성공하고 작성완료상태인대 불합격이면 원자재 이상발생 화면으로 이동
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);


                            // 원자재 이상발생 관리 화면을 띄운다
                            frmINS0017 frmINS = new frmINS0017();

                            // 변수전달
                            frmINS.PlantCode = m_strPlantCode;
                            frmINS.ReqNo = strReqNo;
                            frmINS.ReqSeq = strReqSeq;
                            frmINS.MoveFormName = this.Name;

                            CommonControl cControl = new CommonControl();
                            Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                            Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                            //해당화면의 탭이 있는경우 해당 탭 닫기
                            if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0017"))
                            {

                                uTabMenu.Tabs["QRPINS" + "," + "frmINS0017"].Close();

                            }
                            uTabMenu.Tabs.Add("QRPINS" + "," + "frmINS0017", "원자재 이상발생관리");
                            uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINS0017"];

                            //호출시 화면 속성 설정
                            frmINS.AutoScroll = true;
                            frmINS.MdiParent = this.MdiParent;
                            frmINS.ControlBox = false;
                            frmINS.Dock = DockStyle.Fill;
                            frmINS.FormBorderStyle = FormBorderStyle.None;
                            frmINS.WindowState = FormWindowState.Normal;
                            frmINS.Text = "원자재 이상발생관리";
                            frmINS.Show();

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrNum.Equals(0))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                if (ErrRtn.ErrMessage.Equals(string.Empty))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                // 팦업창 Close
                this.MdiParent.Cursor = Cursors.Default;
            }
        }

        [AutoComplete(false)]
        public string mfSaveWMSMATInspectReq(DataTable dtSave, string strUserID, string strUserIP)
        {
            #region 변경전
            ////QRPDB.ORADB oraDB = new QRPDB.ORADB();
            ////TransErrRtn ErrRtn = new TransErrRtn();
            ////try
            ////{
            ////    string strErrRtn = string.Empty;
            ////    string strWMSErrRtn = string.Empty;
            ////    //WMS Oracle DB 연결정보 읽기
            ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsPgm = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
            ////    DataTable dtPgm = clsPgm.mfReadSystemAccessInfoDetail(dtSave.Rows[0]["PlantCode"].ToString(), "S05");
            ////    oraDB.mfSetORADBConnectString(dtPgm.Rows[0]["SystemAddressPath"].ToString(), dtPgm.Rows[0]["AccessID"].ToString(), dtPgm.Rows[0]["AccessPassword"].ToString());
            ////    //oraDB.mfSetORADBConnectString("STS_MES_TEST", "wmsuser", "wms119");

            ////    //WMS Oracle DB연결하기
            ////    oraDB.mfConnect();

            ////    //Transaction 시작
            ////    OracleTransaction trans;
            ////    trans = oraDB.SqlCon.BeginTransaction();

            ////    for (int i = 0; i < dtSave.Rows.Count; i++)
            ////    {
            ////        //WMS 처리 SP 호출
            ////        DataTable dtParam = oraDB.mfSetParamDataTable();
            ////        oraDB.mfAddParamDataRow(dtParam, "V_PLANTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["PlantCode"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_CONSTRACTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["GRNo"].ToString(), 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_LOTNO", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["LotNo"].ToString(), 50);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_RESULTFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERIP", ParameterDirection.Input, OracleDbType.Varchar2, strUserIP, 15);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERID", ParameterDirection.Input, OracleDbType.Varchar2, strUserID, 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_VALIDATIONDATE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_IFFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["IFFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "RTN", ParameterDirection.Output, OracleDbType.Varchar2, 2);

            ////        strErrRtn = oraDB.mfExecTransStoredProc(oraDB.SqlCon, trans, "UP_UPDATE_INSINSPECTRESULT", dtParam);
            ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            ////        //WMS 처리결과에 따른 Transaction 처리
            ////        strWMSErrRtn = ErrRtn.mfGetReturnValue(0);
            ////        if (!strWMSErrRtn.Equals("00"))
            ////        {
            ////            trans.Rollback();
            ////            break;
            ////        }
            ////    }
            ////    if (strWMSErrRtn.Equals("00"))
            ////        trans.Commit();

            ////    clsPgm.Dispose();
            ////    oraDB.Dispose();

            ////    return strErrRtn;
            ////}
            ////catch (Exception ex)
            ////{
            ////    return ex.Message;
            ////}
            ////finally
            ////{
            ////    oraDB.mfDisConnect();
            ////}
            #endregion
            //SQLS sql = new SQLS();
            QRPDB.TransErrRtn ErrRtn = new QRPDB.TransErrRtn();

             string strConnection = "server = 10.61.61.71 ; database= QRP_PSTS_DEV ;uid= qrpusr ;pwd= stsint ";
             SqlConnection SqlCon = new SqlConnection(strConnection);              
               
            try
            {
               // sql.mfConnect();
                SqlCon.Open();

                string strErrRtn = string.Empty;
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = mfSetParamDataTable();
                    mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["GRNo"].ToString(), 20);
                    mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
                    mfAddParamDataRow(dtParam, "@i_strInspectDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectDesc"].ToString(), 50);
                    mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    mfAddParamDataRow(dtParam, "@i_strValidationDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
                    mfAddParamDataRow(dtParam, "@i_strIFFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["IFFlag"].ToString(), 1);
                    mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_INSMatInspectReqH_WMS_Send", dtParam);
                    strErrRtn = mfExecTransStoredProc(SqlCon, "up_Update_INSMatInspectReqH_WMS_Send", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrMessage.Equals("00") || !ErrRtn.ErrNum.Equals(0))
                    {
                        //trans.Rollback();
                        break;
                    }
                }

                //if (ErrRtn.ErrNum.Equals(0))
                //    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //sql.mfDisConnect();
               // sql.Dispose();
                SqlCon.Close();
                SqlCon.Dispose();
                
            }
        }

        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, string strValue, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = strValue;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        public DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(SqlDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;

                dt.Clear();
                dt = null;

                dc = null;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        public string mfExecTransStoredProc(SqlConnection Conn, string strSPName, DataTable dtSPParameter)
        {
            QRPDB.TransErrRtn Result = new QRPDB.TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    //m_SqlCmd.Transaction = Trans;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }

                        m_SqlCmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_SqlCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_SqlCmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_SqlCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_SqlCmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }   

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uTextReqNo.Text == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000394", Infragistics.Win.HAlign.Center);

                    mfSearch();
                    return;
                }
                else
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                        QRPINS.BL.INSIMP.MatInspectReqH clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH();
                        brwChannel.mfCredentials(clsHeader);

                        string strReqNo = this.uTextReqNo.Text.Substring(0, 7);
                        string strReqSeq = this.uTextReqNo.Text.Substring(7, 4);

                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 삭제 Method 호출
                        String strErrRtn = clsHeader.mfDeleteMatInspectReqH(m_strPlantCode, strReqNo, strReqSeq);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);
                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000923",
                                                        Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {
            }
            catch(Exception ex)
            {

            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                ////if (this.uGridInspectList.Rows.Count > 0)
                ////{
                ////    wGrid.mfDownLoadGridToExcel(this.uGridInspectList);
                ////}
                ////else
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                ////                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                                    "M000803", "M000809", "M000332",
                ////                                    Infragistics.Win.HAlign.Right);
                ////}
                if(this.uGridInspectList.Rows.Count > 0)
                    wGrid.mfDownLoadGridToExcel(this.uGridInspectList);

                if (this.uGridReqSampling.Rows.Count > 0)
                    wGrid.mfDownLoadGridToExcel(this.uGridReqSampling);
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region Method...
        /// <summary>
        /// 자재입고조회 화면으로 부터 이동했을때 데이터 조회하여 수입검사/조회 화면에 뿌려주는 Method
        /// </summary>
        private void Get_INSMaterialGR()
        {
            try
            {
                // SystemInfo RecourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MaterialGR), "MaterialGR");
                QRPINS.BL.INSIMP.MaterialGR clsGR = new QRPINS.BL.INSIMP.MaterialGR();
                brwChannel.mfCredentials(clsGR);

                // ProgressBar 생성
                ////QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                ////Thread threadPop = m_ProgressPopup.mfStartThread();
                ////m_ProgressPopup.mfOpenProgressPopup(this, "조회중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // Method 호출
                DataTable dtRtnGR = clsGR.mfReadINSMatInspectReqInit(PlantCode, VendorCode, MaterialCode, GRDate, GRNo, SpecNo, MoldSeq, m_resSys.GetString("SYS_LANG"), MoveFormName);

                // ProgressBar Close
                this.MdiParent.Cursor = Cursors.Default;
                ////m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGridLot.DataSource = dtRtnGR;
                this.uGridLot.DataBind();

                this.uTextPlantName.Text = dtRtnGR.Rows[0]["PlantName"].ToString();
                this.uTextMaterialCode.Text = MaterialCode;
                this.uTextMaterialName.Text = dtRtnGR.Rows[0]["MaterialName"].ToString();
                this.uTextVendorCode.Text = VendorCode;
                this.uTextVendorName.Text = dtRtnGR.Rows[0]["VendorName"].ToString();
                this.uTextSpecNo.Text = SpecNo;
                this.uTextMoldSeq.Text = MoldSeq;
                this.uTextMaterialGradeCode.Text = dtRtnGR.Rows[0]["MaterialGrade"].ToString();
                this.uTextMaterialGradeName.Text = dtRtnGR.Rows[0]["MaterialGradeName"].ToString();
                this.uTextGRDate.Text = dtRtnGR.Rows[0]["GRDate"].ToString();
                this.uTextFloorPlanNo.Text = dtRtnGR.Rows[0]["FloorPlanNo"].ToString();
                this.uTextShipmentCount.Text = dtRtnGR.Rows[0]["ShipmentCount"].ToString();
                //this.uDateTimeReceiptTime.Value = ReceipTime;
                this.uDateReceiptDate.Value = dtRtnGR.Rows[0]["ReceiptDate"].ToString().Substring(0, 10);
                this.uDateTimeReceiptTime.Value = dtRtnGR.Rows[0]["ReceiptDate"].ToString().Substring(11, 8);

                // 총입고수량
                Double dblTotal = 0.0;
                for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                {
                    dblTotal += Convert.ToDouble(this.uGridLot.Rows[i].Cells["LotSize"].Value);
                }
                this.uTextGRQty.Text = Math.Floor(dblTotal).ToString();

                // MoLot의 카운트를 구한다
                int intLotCount = dtRtnGR.DefaultView.ToTable(true, "MoLotNo").Rows.Count;
                this.uTextLotCount.Text = intLotCount.ToString();

                QRPSTA.STAINS.JOJUNG clsJoJung = new QRPSTA.STAINS.JOJUNG();
                // AML AQLFlag 로 구분한다
                if (dtRtnGR.Rows[0]["AQLFlag"].ToString().Equals("F"))
                {
                    //intLotCount = 0;
                    //intLotCount = dtRtnGR.Rows.Count;
                    if (MoveFormName.Equals("frmINSZ0002"))
                        this.uTextSamplingLot.Text = dtRtnGR.Rows.Count.ToString();
                    else
                        this.uTextSamplingLot.Text = clsJoJung.SetLotSamleCountTable(intLotCount).ToString();
                    m_bolCheckAQL = false;
                }
                else
                {
                    m_bolCheckAQL = true;
 
                    intLotCount = 0;
                    for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                    {
                        intLotCount += Convert.ToInt32(this.uGridLot.Rows[i].Cells["LotSize"].Value);
                    }

                    // 검사수준
                    QRPSTA.STAINS.InspectLevel InspectLevel = new QRPSTA.STAINS.InspectLevel();
                    if (dtRtnGR.Rows[0]["InspectLevel"].ToString() == "1")
                        InspectLevel = QRPSTA.STAINS.InspectLevel.S1;
                    else if (dtRtnGR.Rows[0]["InspectLevel"].ToString() == "2")
                        InspectLevel = QRPSTA.STAINS.InspectLevel.S2;
                    else if (dtRtnGR.Rows[0]["InspectLevel"].ToString() == "3")
                        InspectLevel = QRPSTA.STAINS.InspectLevel.S3;
                    else if (dtRtnGR.Rows[0]["InspectLevel"].ToString() == "4")
                        InspectLevel = QRPSTA.STAINS.InspectLevel.S4;
                    else if (dtRtnGR.Rows[0]["InspectLevel"].ToString() == "5")
                        InspectLevel = QRPSTA.STAINS.InspectLevel.G1;
                    else if (dtRtnGR.Rows[0]["InspectLevel"].ToString() == "6")
                        InspectLevel = QRPSTA.STAINS.InspectLevel.G2;
                    else if (dtRtnGR.Rows[0]["InspectLevel"].ToString() == "7")
                        InspectLevel = QRPSTA.STAINS.InspectLevel.G3;


                    // 검사수준
                    QRPSTA.STAINS.JojungInspectionStep InspectStep = new QRPSTA.STAINS.JojungInspectionStep();
                    if (dtRtnGR.Rows[0]["MaterialGrade"].ToString() == "" || dtRtnGR.Rows[0]["MaterialGrade"].ToString() == "1" || 
                        dtRtnGR.Rows[0]["MaterialGrade"].ToString() == "2" || dtRtnGR.Rows[0]["MaterialGrade"].ToString() == "6")
                        InspectStep = QRPSTA.STAINS.JojungInspectionStep.TightInspection;
                    else if (dtRtnGR.Rows[0]["MaterialGrade"].ToString() == "3" || dtRtnGR.Rows[0]["MaterialGrade"].ToString() == "4")
                        InspectStep = QRPSTA.STAINS.JojungInspectionStep.NormalInspection;
                    else if (dtRtnGR.Rows[0]["MaterialGrade"].ToString() == "5")
                        InspectStep = QRPSTA.STAINS.JojungInspectionStep.ReducedInspection;


                    // 검사비율
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                    QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                    brwChannel.mfCredentials(clsCom);

                    DataTable dtCom = clsCom.mfReadCommonCode("C0038", m_resSys.GetString("SYS_LANG"));
                    double dblInspectRate = 0.0;
                    for (int i = 0; i < dtCom.Rows.Count; i++)
                    {
                        if (dtCom.Rows[i]["ComCode"].ToString() == dtRtnGR.Rows[0]["InspectRate"].ToString())
                            dblInspectRate = Convert.ToDouble(dtCom.Rows[i]["ComCodeName"]);
                    }

                    QRPSTA.STAINS.AQLTable AQLTable = new QRPSTA.STAINS.AQLTable();
                    AQLTable = clsJoJung.mfSetAQLTable(intLotCount, InspectLevel, InspectStep, dblInspectRate);

                    // AQL 적용시 SamplingLot 적용 안됨
                    //this.uTextSamplingLot.Text = AQLTable.LotSampleCount.ToString();
                    this.uTextSamplingLot.Text = "1";

                    // 합격, 불합격판정갯수 저장
                    m_intAcceptCount = AQLTable.AcceptCount;
                    m_intRejectCount = AQLTable.RejectCount;
                    m_dblAQLSampleSize = AQLTable.SampleSize;

                    // 검사결과 합격으로
                    this.uOptionPassFailFlag.CheckedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region AQL적용시 검사비율 검사수준 가져오는 메소드
        /////// <summary>
        /////// AQL 적용시 필요한 정보 설정
        /////// </summary>
        ////private void SetAQL()
        ////{
        ////    try
        ////    {
        ////        // SystemInfo RecourceSet
        ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        ////        int intLotCount = this.uGridLot.Rows.Count;

        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AMLHistory), "AMLHistory");
        ////        QRPINS.BL.INSIMP.AMLHistory clsAH = new QRPINS.BL.INSIMP.AMLHistory();
        ////        brwChannel.mfCredentials(clsAH);

        ////        DataTable dtRtn = clsAH.mfReadAMLForAQL(PlantCode, VendorCode, MaterialCode, SpecNo, MoldSeq);

        ////        QRPSTA.STAINS.JOJUNG clsJoJung = new QRPSTA.STAINS.JOJUNG();
        ////        // AML 이력에 (거래처, 자재, SpecNo, 금형차수)가 등록되지 않은경우 => AQL을 적용하지 않은경우
        ////        if (dtRtn.Rows.Count > 0)
        ////        {
        ////            m_bolCheckAQL = true;
        ////            // 검사수준
        ////            QRPSTA.STAINS.InspectLevel InspectLevel = new QRPSTA.STAINS.InspectLevel();
        ////            if (dtRtn.Rows[0]["InspectLevel"].ToString() == "1")
        ////                InspectLevel = QRPSTA.STAINS.InspectLevel.S1;
        ////            else if (dtRtn.Rows[0]["InspectLevel"].ToString() == "2")
        ////                InspectLevel = QRPSTA.STAINS.InspectLevel.S2;
        ////            else if (dtRtn.Rows[0]["InspectLevel"].ToString() == "3")
        ////                InspectLevel = QRPSTA.STAINS.InspectLevel.S3;
        ////            else if (dtRtn.Rows[0]["InspectLevel"].ToString() == "4")
        ////                InspectLevel = QRPSTA.STAINS.InspectLevel.S4;
        ////            else if (dtRtn.Rows[0]["InspectLevel"].ToString() == "5")
        ////                InspectLevel = QRPSTA.STAINS.InspectLevel.G1;
        ////            else if (dtRtn.Rows[0]["InspectLevel"].ToString() == "6")
        ////                InspectLevel = QRPSTA.STAINS.InspectLevel.G2;
        ////            else if (dtRtn.Rows[0]["InspectLevel"].ToString() == "7")
        ////                InspectLevel = QRPSTA.STAINS.InspectLevel.G3;

        ////            // 검사수준
        ////            QRPSTA.STAINS.JojungInspectionStep InspectStep = new QRPSTA.STAINS.JojungInspectionStep();
        ////            if (dtRtn.Rows[0]["MaterialGrade"].ToString() == "" || dtRtn.Rows[0]["MaterialGrade"].ToString() == "1" ||
        ////                dtRtn.Rows[0]["MaterialGrade"].ToString() == "2" || dtRtn.Rows[0]["MaterialGrade"].ToString() == "6")
        ////                InspectStep = QRPSTA.STAINS.JojungInspectionStep.TightInspection;
        ////            else if (dtRtn.Rows[0]["MaterialGrade"].ToString() == "3" || dtRtn.Rows[0]["MaterialGrade"].ToString() == "4")
        ////                InspectStep = QRPSTA.STAINS.JojungInspectionStep.NormalInspection;
        ////            else if (dtRtn.Rows[0]["MaterialGrade"].ToString() == "5")
        ////                InspectStep = QRPSTA.STAINS.JojungInspectionStep.ReducedInspection;

        ////            // 검사비율
        ////            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
        ////            QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
        ////            brwChannel.mfCredentials(clsCom);

        ////            DataTable dtCom = clsCom.mfReadCommonCode("C0038", m_resSys.GetString("SYS_LANG"));
        ////            double dblInspectRate = 0.0;
        ////            for (int i = 0; i < dtCom.Rows.Count; i++)
        ////            {
        ////                if (dtCom.Rows[i]["ComCode"].ToString() == dtRtn.Rows[0]["InspectRate"].ToString())
        ////                    dblInspectRate = Convert.ToDouble(dtCom.Rows[i]["ComCodeName"]);
        ////            }

        ////            QRPSTA.STAINS.AQLTable AQLTable = new QRPSTA.STAINS.AQLTable();
        ////            AQLTable = clsJoJung.mfSetAQLTable(intLotCount, InspectLevel, InspectStep, dblInspectRate);

        ////            this.uTextSamplingLot.Text = AQLTable.LotSampleCount.ToString();

        ////            // 합격, 불합격판정갯수 저장
        ////            m_intAcceptCount = AQLTable.AcceptCount;
        ////            m_intRejectCount = AQLTable.RejectCount;
        ////            m_dblAQLSampleSize = AQLTable.SampleSize;
        ////        }
        ////        else
        ////        {
        ////            this.uTextSamplingLot.Text = clsJoJung.SetLotSamleCountTable(intLotCount).ToString();
        ////            m_bolCheckAQL = false;
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}
        #endregion

        /// <summary>
        /// 헤더정보 저장후 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable RtnHeaderDataTable()
        {
            DataTable dtSaveHeader = new DataTable();
            try
            {
                if (this.uCheckCompleteFlag.Enabled)
                {
                    // 수입검사헤더 BL 연결
                    QRPINS.BL.INSIMP.MatInspectReqH clsHeader;
                    // 
                    if (m_bolDebugMode == false)
                    {
                        // BL 연결
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                        clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH();
                        brwChannel.mfCredentials(clsHeader);
                    }
                    else
                    {
                        clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH(m_strDBConn);
                    }

                    String strFullReqNo = this.uTextReqNo.Text;
                    String strReqNo = "";
                    String strReqSeq = "";
                    if (strFullReqNo.Length > 9)
                    {
                        strReqNo = strFullReqNo.Substring(0, 7);
                        strReqSeq = strFullReqNo.Substring(7, 4);
                    }

                    // DataTable Column 설정
                    dtSaveHeader = clsHeader.mfSetDataInfo();

                    // Lot 정보 그리드에 Lot의 총합을 구한다
                    Double dblTotal = 0.0;
                    for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                    {
                        //if (Convert.ToBoolean(this.uGridLot.Rows[i].Cells["Check"].Value) == true)
                        //{
                        dblTotal += Convert.ToDouble(this.uGridLot.Rows[i].Cells["LotSize"].Value);
                        //}
                    }

                    DataRow drRow = dtSaveHeader.NewRow();
                    drRow["PlantCode"] = m_strPlantCode;
                    drRow["ReqNo"] = strReqNo;
                    drRow["ReqSeq"] = strReqSeq;
                    drRow["ReceiptDate"] = Convert.ToDateTime(this.uDateReceiptDate.Value).ToString("yyyy-MM-dd");
                    drRow["ReceiptTime"] = Convert.ToDateTime(this.uDateTimeReceiptTime.Value).ToString("HH:mm:ss");
                    drRow["MaterialGrade"] = this.uTextMaterialGradeCode.Text;
                    if (this.uTextShipmentCount.Text.Equals(string.Empty))                  // ShipmentCount 저장
                        drRow["InspectCount"] = 0;
                    else
                        drRow["InspectCount"] = Convert.ToInt32(this.uTextShipmentCount.Text);
                    if (this.uOptionPassFailFlag.CheckedIndex != -1)
                        drRow["PassFailFlag"] = this.uOptionPassFailFlag.Value.ToString();
                    if (this.uOptionInspectResultType.CheckedIndex != -1)
                        drRow["InspectResultType"] = this.uOptionInspectResultType.Value.ToString();
                    drRow["FaultDesc"] = this.uTextFaultDesc.Text;
                    drRow["CompleteFlag"] = this.uCheckCompleteFlag.Checked;
                    if (this.uOptionICPCheckFlag.CheckedIndex != -1)
                        drRow["ICPCheckFlag"] = this.uOptionICPCheckFlag.Value.ToString();
                    drRow["TotalQty"] = dblTotal;
                    drRow["SamplingLotCount"] = Convert.ToInt32(this.uTextSamplingLot.Text);
                    drRow["inspectUserID"] = this.uTextInspectUserID.Text;
                    // 검사시간 작성완료 체크후 저장시로 변경
                    if (this.uCheckCompleteFlag.Enabled && this.uCheckCompleteFlag.Checked)
                    {
                        this.uDateInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                        this.uDateInspectTime.Value = DateTime.Now.ToString("HH:mm:ss");
                    }
                    drRow["InspectDate"] = Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd");
                    if(this.uDateInspectTime.Value != null)
                        drRow["InspectTime"] = Convert.ToDateTime(this.uDateInspectTime.Value).ToString("HH:mm:ss");
                    drRow["ReviewUserID"] = this.uTextReviewUserID.Text;
                    drRow["ReviewDate"] = Convert.ToDateTime(this.uDateReviewDate.Value).ToString("yyyy-MM-dd");
                    drRow["AdmitUserID"] = this.uTextAdmitUserID.Text;
                    drRow["AdmitDate"] = Convert.ToDateTime(this.uDateAdmitDate.Value).ToString("yyyy-MM-dd");
                    drRow["EtcDescQA"] = this.uTextEtcDescQA.Text;
                    drRow["WMSTransFlag"] = "";
                    dtSaveHeader.Rows.Add(drRow);
                }

                return dtSaveHeader;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveHeader;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot 정보 저장후 DataTable로 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable RtnLotDataTable()
        {
            DataTable dtSaveLot = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                QRPINS.BL.INSIMP.MatInspectReqLot clsLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                // Column 설정
                dtSaveLot = clsLot.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장
                //if (!this.uTextWMSTransFlag.Text.Equals("T") && this.uCheckCompleteFlag.Enabled)
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                    {
                        //String strReqNoSeq = this.uTextReqNo.Text;
                        drRow = dtSaveLot.NewRow();
                        drRow["PlantCode"] = m_strPlantCode;
                        //drRow["ReqNo"] = strReqNoSeq.Substring(0, 7);
                        //drRow["ReqSeq"] = strReqNoSeq.Substring(7, 4);
                        drRow["ReqLotSeq"] = this.uGridLot.Rows[i].RowSelectorNumber;
                        drRow["GRNo"] = this.uGridLot.Rows[i].Cells["GRNo"].Value.ToString();
                        drRow["LotNo"] = this.uGridLot.Rows[i].Cells["LotNo"].Value.ToString();
                        if (this.uGridLot.Rows[i].Cells["COCFilePath"].Value.ToString().Contains(":\\"))
                        {
                            System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uGridLot.Rows[i].Cells["COCFilePath"].Value.ToString());
                            drRow["COCFilePath"] = fileDoc.Name;
                        }
                        else
                        {
                            drRow["COCFilePath"] = this.uGridLot.Rows[i].Cells["COCFilePath"].Value.ToString();
                        }
                        drRow["VendorCode"] = m_strVendorCode;
                        drRow["MaterialCode"] = m_strMaterialCode;
                        drRow["LotSize"] = this.uGridLot.Rows[i].Cells["LotSize"].Value.ToString();
                        if (Convert.ToBoolean(this.uGridLot.Rows[i].Cells["Check"].Value) == true)
                            drRow["SamplingLotFlag"] = "T";
                        else
                            drRow["SamplingLotFlag"] = "F";
                        drRow["AcceptCount"] = m_intAcceptCount;
                        drRow["RejectCount"] = m_intRejectCount;
                        drRow["AQLSampleSize"] = m_dblAQLSampleSize;
                        if(this.uOptionPassFailFlag.CheckedIndex.Equals(0))
                            drRow["InspectResultFlag"] = "OK";
                        else if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                            drRow["InspectResultFlag"] = "NG";
                        dtSaveLot.Rows.Add(drRow);
                    }
                }
                return dtSaveLot;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveLot;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item정보 DataTable로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable RtnItemDataTable()
        {
            DataTable dtSaveItem = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqItem), "MatInspectReqItem");
                QRPINS.BL.INSIMP.MatInspectReqItem clsItem = new QRPINS.BL.INSIMP.MatInspectReqItem();
                
                dtSaveItem = clsItem.mfSetDataInfo();
                DataRow drRow;
                //if (!this.uTextWMSTransFlag.Text.Equals("T") && this.uCheckCompleteFlag.Enabled)
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        drRow = dtSaveItem.NewRow();

                        drRow["PlantCode"] = this.uGridReqSampling.Rows[i].Cells["PlantCode"].Value.ToString();
                        drRow["ReqNo"] = this.uGridReqSampling.Rows[i].Cells["ReqNo"].Value.ToString();
                        drRow["ReqSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqSeq"].Value.ToString();
                        //drRow["ReqLotSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqLotSeq"].Value;
                        drRow["ReqLotSeq"] = 0;
                        drRow["ReqItemSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqItemSeq"].Value;
                        drRow["Seq"] = i + 1;

                        drRow["SampleSize"] = this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value;
                        drRow["FaultQty"] = this.uGridReqSampling.Rows[i].Cells["FaultQty"].Value;

                        if (this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Value != null)
                            drRow["InspectResultFlag"] = this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString();

                        if (this.uGridReqSampling.Rows[i].Cells["Mean"].Value == null || this.uGridReqSampling.Rows[i].Cells["Mean"].Value == DBNull.Value)
                            drRow["Mean"] = 0.0;
                        else
                            drRow["Mean"] = Math.Round(ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells["Mean"].Value.ToString()), 5, MidpointRounding.AwayFromZero);

                        if (this.uGridReqSampling.Rows[i].Cells["StdDev"].Value == null || this.uGridReqSampling.Rows[i].Cells["StdDev"].Value == DBNull.Value)
                            drRow["StdDev"] = 0.0;
                        else
                            drRow["StdDev"] = Math.Round(ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells["StdDev"].Value.ToString()), 5, MidpointRounding.AwayFromZero);

                        if (this.uGridReqSampling.Rows[i].Cells["MaxValue"].Value == null || this.uGridReqSampling.Rows[i].Cells["MaxValue"].Value == DBNull.Value)
                            drRow["MaxValue"] = 0.0;
                        else
                            drRow["MaxValue"] = Math.Round(ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells["MaxValue"].Value.ToString()), 5, MidpointRounding.AwayFromZero);

                        if (this.uGridReqSampling.Rows[i].Cells["MinValue"].Value == null || this.uGridReqSampling.Rows[i].Cells["MinValue"].Value == DBNull.Value)
                            drRow["MinValue"] = 0.0;
                        else
                            drRow["MinValue"] = Math.Round(ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells["MinValue"].Value.ToString()), 5, MidpointRounding.AwayFromZero);

                        if (this.uGridReqSampling.Rows[i].Cells["DataRange"].Value == null || this.uGridReqSampling.Rows[i].Cells["DataRange"].Value == DBNull.Value)
                            drRow["DataRange"] = 0.0;
                        else
                            drRow["DataRange"] = Math.Round(ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells["DataRange"].Value.ToString()), 5, MidpointRounding.AwayFromZero);

                        if (this.uGridReqSampling.Rows[i].Cells["Cp"].Value == null || this.uGridReqSampling.Rows[i].Cells["Cp"].Value == DBNull.Value)
                            drRow["Cp"] = 0.0;
                        else
                            drRow["Cp"] = Math.Round(ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells["Cp"].Value.ToString()), 5, MidpointRounding.AwayFromZero);

                        if (this.uGridReqSampling.Rows[i].Cells["Cpk"].Value == null || this.uGridReqSampling.Rows[i].Cells["Cpk"].Value == DBNull.Value)
                            drRow["Cpk"] = 0.0;
                        else
                            drRow["Cpk"] = Math.Round(ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells["Cpk"].Value.ToString()), 5, MidpointRounding.AwayFromZero);

                        dtSaveItem.Rows.Add(drRow);

                        #region Sampling 그리드에서 데이터 저장하는것으로 변경
                        ////drRow = dtSaveItem.NewRow();

                        ////drRow["PlantCode"] = this.uGridReqItem.Rows[i].Cells["PlantCode"].Value.ToString();
                        ////drRow["ReqNo"] = this.uGridReqItem.Rows[i].Cells["ReqNo"].Value.ToString();
                        ////drRow["ReqSeq"] = this.uGridReqItem.Rows[i].Cells["ReqSeq"].Value.ToString();
                        //////drRow["ReqLotSeq"] = this.uGridReqItem.Rows[i].Cells["ReqLotSeq"].Value;
                        ////drRow["ReqLotSeq"] = 0;
                        ////drRow["ReqItemSeq"] = this.uGridReqItem.Rows[i].Cells["ReqItemSeq"].Value;
                        ////drRow["Seq"] = i + 1;

                        ////drRow["SampleSize"] = this.uGridReqItem.Rows[i].Cells["SampleSize"].Value;
                        ////drRow["FaultQty"] = this.uGridReqItem.Rows[i].Cells["FaultQty"].Value;

                        ////if (this.uGridReqItem.Rows[i].Cells["InspectResultFlag"].Value != null)
                        ////    drRow["InspectResultFlag"] = this.uGridReqItem.Rows[i].Cells["InspectResultFlag"].Value.ToString();

                        ////if (this.uGridReqItem.Rows[i].Cells["Mean"].Value == null || this.uGridReqItem.Rows[i].Cells["Mean"].Value == DBNull.Value)
                        ////    drRow["Mean"] = 0.0;
                        ////else
                        ////    drRow["Mean"] = this.uGridReqItem.Rows[i].Cells["Mean"].Value;

                        ////if (this.uGridReqItem.Rows[i].Cells["StdDev"].Value == null || this.uGridReqItem.Rows[i].Cells["StdDev"].Value == DBNull.Value)
                        ////    drRow["StdDev"] = 0.0;
                        ////else
                        ////    drRow["StdDev"] = this.uGridReqItem.Rows[i].Cells["StdDev"].Value;

                        ////if (this.uGridReqItem.Rows[i].Cells["MaxValue"].Value == null || this.uGridReqItem.Rows[i].Cells["MaxValue"].Value == DBNull.Value)
                        ////    drRow["MaxValue"] = 0.0;
                        ////else
                        ////    drRow["MaxValue"] = this.uGridReqItem.Rows[i].Cells["MaxValue"].Value;

                        ////if (this.uGridReqItem.Rows[i].Cells["MinValue"].Value == null || this.uGridReqItem.Rows[i].Cells["MinValue"].Value == DBNull.Value)
                        ////    drRow["MinValue"] = 0.0;
                        ////else
                        ////    drRow["MinValue"] = this.uGridReqItem.Rows[i].Cells["MinValue"].Value;

                        ////if (this.uGridReqItem.Rows[i].Cells["DataRange"].Value == null || this.uGridReqItem.Rows[i].Cells["DataRange"].Value == DBNull.Value)
                        ////    drRow["DataRange"] = 0.0;
                        ////else
                        ////    drRow["DataRange"] = this.uGridReqItem.Rows[i].Cells["DataRange"].Value;

                        ////if (this.uGridReqItem.Rows[i].Cells["Cp"].Value == null || this.uGridReqItem.Rows[i].Cells["Cp"].Value == DBNull.Value)
                        ////    drRow["Cp"] = 0.0;
                        ////else
                        ////    drRow["Cp"] = this.uGridReqItem.Rows[i].Cells["Cp"].Value;

                        ////if (this.uGridReqItem.Rows[i].Cells["Cpk"].Value == null || this.uGridReqItem.Rows[i].Cells["Cpk"].Value == DBNull.Value)
                        ////    drRow["Cpk"] = 0.0;
                        ////else
                        ////    drRow["Cpk"] = this.uGridReqItem.Rows[i].Cells["Cpk"].Value;

                        ////dtSaveItem.Rows.Add(drRow);
                        #endregion
                    }
                }
                return dtSaveItem;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSaveItem;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 계량형인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <returns></returns>
        private DataTable RtnResultMeasure(int intStart)
        {
            DataTable dtMeasure = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectResultMeasure), "MatInspectResultMeasure");
                QRPINS.BL.INSIMP.MatInspectResultMeasure clsMeasure = new QRPINS.BL.INSIMP.MatInspectResultMeasure();
                brwChannel.mfCredentials(clsMeasure);

                // Column 설정
                dtMeasure = clsMeasure.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장
                //if (!this.uTextWMSTransFlag.Text.Equals("T") && this.uCheckCompleteFlag.Enabled)
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        //int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value) * Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["Point"].Value);
                        int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value);

                        // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                        if (intSampleSize > 99) intSampleSize = 99;

                        if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "1")
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                if (this.uGridReqSampling.Rows[i].Cells[j].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                {
                                    drRow = dtMeasure.NewRow();

                                    drRow["PlantCode"] = this.uGridReqSampling.Rows[i].Cells["PlantCode"].Value.ToString();
                                    drRow["ReqNo"] = this.uGridReqSampling.Rows[i].Cells["ReqNo"].Value.ToString();
                                    drRow["ReqSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqSeq"].Value.ToString();
                                    //drRow["ReqLotSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqLotSeq"].Value.ToString();
                                    drRow["ReqLotSeq"] = 0;
                                    drRow["ReqItemSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridReqSampling.Rows[i].Cells[j].Value == null || this.uGridReqSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = 0.0;
                                    else
                                        drRow["InspectValue"] = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[j].Value.ToString());
                                    dtMeasure.Rows.Add(drRow);
                                }
                            }
                        }
                    }
                }
                return dtMeasure;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMeasure;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 계수형인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable RtnResultCount(int intStart)
        {
            DataTable dtCount = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectResultCount), "MatInspectResultCount");
                QRPINS.BL.INSIMP.MatInspectResultCount clsCount = new QRPINS.BL.INSIMP.MatInspectResultCount();
                brwChannel.mfCredentials(clsCount);

                // Column 설정
                dtCount = clsCount.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장

                //if (!this.uTextWMSTransFlag.Text.Equals("T") && this.uCheckCompleteFlag.Enabled)
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        //int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value) * Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["Point"].Value);
                        int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value);

                        // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                        if (intSampleSize > 99) intSampleSize = 99;

                        if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "2")
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                if (this.uGridReqSampling.Rows[i].Cells[j].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                {
                                    drRow = dtCount.NewRow();

                                    drRow["PlantCode"] = this.uGridReqSampling.Rows[i].Cells["PlantCode"].Value.ToString();
                                    drRow["ReqNo"] = this.uGridReqSampling.Rows[i].Cells["ReqNo"].Value.ToString();
                                    drRow["ReqSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqSeq"].Value.ToString();
                                    //drRow["ReqLotSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqLotSeq"].Value.ToString();
                                    drRow["ReqLotSeq"] = 0;
                                    drRow["ReqItemSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridReqSampling.Rows[i].Cells[j].Value == null || this.uGridReqSampling.Rows[i].Cells[j].Value == DBNull.Value)
                                        drRow["InspectValue"] = 0.0;
                                    else
                                        drRow["InspectValue"] = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[j].Value.ToString());
                                    dtCount.Rows.Add(drRow);
                                }
                            }
                        }
                    }
                }
                return dtCount;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtCount;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 Ok/Ng인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable RtnResultOkNg(int intStart)
        {
            DataTable dtOkNg = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectResultOkNg), "MatInspectResultOkNg");
                QRPINS.BL.INSIMP.MatInspectResultOkNg clsOkNg = new QRPINS.BL.INSIMP.MatInspectResultOkNg();
                brwChannel.mfCredentials(clsOkNg);

                // Column 설정
                dtOkNg = clsOkNg.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장
                //if (!this.uTextWMSTransFlag.Text.Equals("T") && this.uCheckCompleteFlag.Enabled)
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        //int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value) * Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["Point"].Value);
                        int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value);

                        // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                        if (intSampleSize > 99) intSampleSize = 99;

                        if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "3")
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                if (this.uGridReqSampling.Rows[i].Cells[j].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                {
                                    drRow = dtOkNg.NewRow();

                                    drRow["PlantCode"] = this.uGridReqSampling.Rows[i].Cells["PlantCode"].Value.ToString();
                                    drRow["ReqNo"] = this.uGridReqSampling.Rows[i].Cells["ReqNo"].Value.ToString();
                                    drRow["ReqSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqSeq"].Value.ToString();
                                    //drRow["ReqLotSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqLotSeq"].Value.ToString();
                                    drRow["ReqLotSeq"] = 0;
                                    drRow["ReqItemSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridReqSampling.Rows[i].Cells[j].Value != null)
                                        drRow["InspectValue"] = this.uGridReqSampling.Rows[i].Cells[j].Value.ToString();
                                    dtOkNg.Rows.Add(drRow);
                                }
                            }
                        }
                    }
                }
                return dtOkNg;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtOkNg;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 설명인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable RtnResultDesc(int intStart)
        {
            DataTable dtDesc = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectResultDesc), "MatInspectResultDesc");
                QRPINS.BL.INSIMP.MatInspectResultDesc clsDesc = new QRPINS.BL.INSIMP.MatInspectResultDesc();
                brwChannel.mfCredentials(clsDesc);

                // Column 설정
                dtDesc = clsDesc.mfSetDataInfo();
                DataRow drRow;
                
                // 데이터 저장
                //if (!this.uTextWMSTransFlag.Text.Equals("T") && this.uCheckCompleteFlag.Enabled)
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        //int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value) * Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["Point"].Value);
                        int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value);

                        // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                        if (intSampleSize > 99) intSampleSize = 99;

                        if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "4")
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                if (this.uGridReqSampling.Rows[i].Cells[j].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                {
                                    drRow = dtDesc.NewRow();

                                    drRow["PlantCode"] = this.uGridReqSampling.Rows[i].Cells["PlantCode"].Value.ToString();
                                    drRow["ReqNo"] = this.uGridReqSampling.Rows[i].Cells["ReqNo"].Value.ToString();
                                    drRow["ReqSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqSeq"].Value.ToString();
                                    //drRow["ReqLotSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqLotSeq"].Value.ToString();
                                    drRow["ReqLotSeq"] = 0;
                                    drRow["ReqItemSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridReqSampling.Rows[i].Cells[j].Value != null)
                                        drRow["InspectValue"] = this.uGridReqSampling.Rows[i].Cells[j].Value.ToString();
                                    dtDesc.Rows.Add(drRow);
                                }
                            }
                        }
                    }
                }
                return dtDesc;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDesc;
            }
            finally
            {
            }
        }

        /// <summary>
        /// DataType이 선택인 검사결과 정보를 반환하는 Method
        /// </summary>
        /// <param name="intStart"></param>
        /// <param name="intEnd"></param>
        /// <returns></returns>
        private DataTable RtnResultSelect(int intStart)
        {
            DataTable dtSelect = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectResultSelect), "MatInspectResultSelect");
                QRPINS.BL.INSIMP.MatInspectResultSelect clsSelect = new QRPINS.BL.INSIMP.MatInspectResultSelect();
                brwChannel.mfCredentials(clsSelect);

                // Column 설정
                dtSelect = clsSelect.mfSetDataInfo();
                DataRow drRow;
                // 데이터 저장
                //if (!this.uTextWMSTransFlag.Text.Equals("T") && this.uCheckCompleteFlag.Enabled)
                if (this.uCheckCompleteFlag.Enabled)
                {
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        //int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value) * Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["Point"].Value);
                        int intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value);

                        // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                        if (intSampleSize > 99) intSampleSize = 99;

                        if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "5")
                        {
                            for (int j = intStart; j < intStart + intSampleSize; j++)
                            {
                                if (this.uGridReqSampling.Rows[i].Cells[j].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                {
                                    drRow = dtSelect.NewRow();

                                    drRow["PlantCode"] = this.uGridReqSampling.Rows[i].Cells["PlantCode"].Value.ToString();
                                    drRow["ReqNo"] = this.uGridReqSampling.Rows[i].Cells["ReqNo"].Value.ToString();
                                    drRow["ReqSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqSeq"].Value.ToString();
                                    //drRow["ReqLotSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqLotSeq"].Value.ToString();
                                    drRow["ReqLotSeq"] = 0;
                                    drRow["ReqItemSeq"] = this.uGridReqSampling.Rows[i].Cells["ReqItemSeq"].Value.ToString();
                                    drRow["ReqResultSeq"] = j - intStart + 1;
                                    if (this.uGridReqSampling.Rows[i].Cells[j].Value != null)
                                        drRow["InspectValue"] = this.uGridReqSampling.Rows[i].Cells[j].Value.ToString();
                                    dtSelect.Rows.Add(drRow);
                                }
                            }
                        }
                    }
                }
                return dtSelect;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtSelect;
            }
            finally
            {
            }
        }

        /// <summary>
        /// AML 저장에 필요한 정보 DataTable 로 반환
        /// </summary>
        /// <returns></returns>
        private DataTable RtnAMLDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.AML), "AML");
                QRPINS.BL.INSIMP.AML clsAML = new QRPINS.BL.INSIMP.AML();
                brwChannel.mfCredentials(clsAML);

                dtRtn = clsAML.mfSetDataInfo_InspectReq();

                //if (this.uCheckCompleteFlag.Checked == true && !this.uTextWMSTransFlag.Text.Equals("T"))
                if (this.uCheckCompleteFlag.Enabled && this.uCheckCompleteFlag.Checked)
                {
                    DataRow drRow = dtRtn.NewRow();
                    drRow["PlantCode"] = PlantCode;
                    drRow["ReqNo"] = this.uTextReqNo.Text.Substring(0, 7);
                    drRow["ReqSeq"] = this.uTextReqNo.Text.Substring(7, 4);
                    drRow["MaterialCode"] = this.uTextMaterialCode.Text;
                    drRow["VendorCode"] = VendorCode;
                    drRow["SpecNo"] = this.uTextSpecNo.Text;
                    drRow["MoldSeq"] = this.uTextMoldSeq.Text;
                    drRow["WriteUserID"] = this.uTextInspectUserID.Text;
                    drRow["WriteDate"] = Convert.ToDateTime(this.uDateInspectDate.Value).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(this.uDateInspectTime.Value).ToString("HH:mm:ss");
                    drRow["InspectResult"] = this.uOptionPassFailFlag.Value.ToString();
                    drRow["MaterialGrade"] = this.uTextMaterialGradeCode.Text;
                    drRow["AcceptCount"] = m_intAcceptCount;
                    drRow["RejectCount"] = m_intRejectCount;
                    dtRtn.Rows.Add(drRow);
                }
                return dtRtn;                    
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수입검사결과 WMS에 전송하기 위한 데이터 테이블 설정 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable RtnWMSDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uTextWMSTransFlag.Text.Equals("T"))
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                    QRPINS.BL.INSIMP.MatInspectReqH clsReq = new QRPINS.BL.INSIMP.MatInspectReqH();
                    brwChannel.mfCredentials(clsReq);

                    dtRtn = clsReq.mfSetWMSDataTable();

                    for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                    {
                        DataRow drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = PlantCode;
                        drRow["GRNo"] = this.uGridLot.Rows[i].Cells["GRNo"].Value.ToString();
                        drRow["LotNo"] = this.uGridLot.Rows[i].Cells["LotNo"].Value.ToString();
                        if (this.uOptionPassFailFlag.CheckedIndex.Equals(0))
                            drRow["InspectResultFlag"] = "T";
                        else if (this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                            drRow["InspectResultFlag"] = "F";
                        drRow["ReqNo"] = this.uTextReqNo.Text.Substring(0, 7);
                        drRow["ReqSeq"] = this.uTextReqNo.Text.Substring(7, 4);
                        drRow["IFFlag"] = "I";
                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 거래처 정보 조회 함수
        /// </summary>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        private DataTable GetVendorInfo(String strVendorCode)
        {
            DataTable dtVendor = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

                return dtVendor;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtVendor;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컨트롤 초기화
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uTextPlantName.Text = "";
                this.uTextReqNo.Text = "";
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialName.Text = "";
                this.uTextVendorName.Text = "";
                this.uTextGRQty.Text = "";
                this.uTextSpecNo.Text = "";
                this.uTextMoldSeq.Text = "";
                this.uTextMaterialGradeCode.Text = "";
                this.uTextMaterialGradeName.Text = "";
                this.uTextGRDate.Text = "";
                this.uTextFloorPlanNo.Clear();
                this.uTextShipmentCount.Clear();
                this.uTextLotCount.Clear();
                this.uTextVendorCode.Clear();

                this.uDateReceiptDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uOptionPassFailFlag.CheckedIndex = -1;
                this.uOptionInspectResultType.CheckedIndex = -1;
                this.uCheckCompleteFlag.CheckedValue = false;
                this.uCheckCompleteFlag.Enabled = false;
                this.uTextFaultDesc.Text = "";
                this.uOptionICPCheckFlag.CheckedIndex = -1;
                this.uTextSamplingLot.Text = "";

                this.uOptionPassFailFlag.Enabled = true;
                this.uOptionICPCheckFlag.Enabled = true;

                this.uTextAdmitUserID.Text = "";
                this.uTextAdmitUserName.Text = "";
                this.uDateAdmitDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextReviewUserID.Text = "";
                this.uTextReviewUserName.Text = "";
                this.uDateReviewDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uTextInspectUserID.Text = "";
                this.uTextInspectUserName.Text = "";
                this.uDateInspectDate.Value = DateTime.Now.ToString("yyyy-MM-dd");

                this.uTextWMSTransFlag.Clear();

                this.uButtonMoveAbnormal.Hide();
                this.uButtonMoveAbnormal.Enabled = false;

                m_strGRDate = "";
                m_strMaterialCode = "";
                m_strMoveFormName = "";
                m_strPlantCode = "";
                m_strVendorCode = "";
                m_strSpecNo = "";

                // Lot 그리드 초기화
                while (this.uGridLot.Rows.Count > 0)
                {
                    this.uGridLot.Rows[0].Delete(false);
                }
                // Item 그리드 초기화
                while (this.uGridReqItem.Rows.Count > 0)
                {
                    this.uGridReqItem.Rows[0].Delete(false);
                }
                // Sampling 그리드 초기화
                while (this.uGridReqSampling.Rows.Count > 0)
                {
                    this.uGridReqSampling.Rows[0].Delete(false);
                }

                this.uCheckCompleteFlag.Enabled = true;

                this.uButtonExcelUpload.Hide();
                this.uButtonExcelUpload.Enabled = false;

                this.uDateAdmitDate.ReadOnly = false;
                this.uDateInspectDate.ReadOnly = false;
                this.uDateInspectTime.ReadOnly = false;
                this.uDateReceiptDate.ReadOnly = false;
                this.uDateReviewDate.ReadOnly = false;
                this.uDateTimeReceiptTime.ReadOnly = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더 상세조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        private void SearchInspectReqHDetail(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPINS.BL.INSIMP.MatInspectReqH clsHeader;
                // 
                if (m_bolDebugMode == false)
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                    clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH(m_strDBConn);
                }

                DataTable dtHeader = clsHeader.mfReadInsMatInspectReqHDetail(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                //PlantCode = dtHeader.Rows[0]["PlantCode"].ToString();
                this.uTextPlantName.Text = dtHeader.Rows[0]["PlantName"].ToString();
                this.uTextReqNo.Text = dtHeader.Rows[0]["ReqNumber"].ToString();
                this.uTextMaterialCode.Text = dtHeader.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dtHeader.Rows[0]["MaterialName"].ToString();
                this.uTextVendorCode.Text = dtHeader.Rows[0]["VendorCode"].ToString();
                this.uTextVendorName.Text = dtHeader.Rows[0]["VendorName"].ToString();
                //this.uTextGRQty.Text = dtHeader.Rows[0]["TotalQty"].ToString();
                this.uTextGRQty.Text = Math.Floor(Convert.ToDecimal(dtHeader.Rows[0]["TotalQty"])).ToString();
                this.uTextFloorPlanNo.Text = dtHeader.Rows[0]["FloorPlanNo"].ToString();
                this.uTextSpecNo.Text = dtHeader.Rows[0]["SpecNo"].ToString();
                this.uTextMoldSeq.Text = dtHeader.Rows[0]["MoldSeq"].ToString();
                this.uTextMaterialGradeCode.Text = dtHeader.Rows[0]["MaterialGrade"].ToString();
                this.uTextMaterialGradeName.Text = dtHeader.Rows[0]["MaterialGradeName"].ToString();
                this.uTextGRDate.Text = dtHeader.Rows[0]["GRDate"].ToString();
                this.uDateReceiptDate.Value = dtHeader.Rows[0]["ReceiptDate"].ToString();
                this.uDateTimeReceiptTime.Value = dtHeader.Rows[0]["ReceiptTime"].ToString();
                if (dtHeader.Rows[0]["PassFailFlag"].ToString() != "")
                    this.uOptionPassFailFlag.Value = dtHeader.Rows[0]["PassFailFlag"].ToString();
                if (dtHeader.Rows[0]["InspectResultType"].ToString() != "")
                    this.uOptionInspectResultType.Value = dtHeader.Rows[0]["InspectResultType"].ToString();
                this.uTextFaultDesc.Text = dtHeader.Rows[0]["FaultDesc"].ToString();
                if (dtHeader.Rows[0]["ICPCheckFlag"].ToString() != "")
                    this.uOptionICPCheckFlag.Value = dtHeader.Rows[0]["ICPCheckFlag"].ToString();
                this.uTextInspectUserID.Text = dtHeader.Rows[0]["InspectUserID"].ToString();
                this.uTextInspectUserName.Text = dtHeader.Rows[0]["InspectUserName"].ToString();
                this.uDateInspectDate.Value = dtHeader.Rows[0]["InspectDate"].ToString();
                this.uDateInspectTime.Value = dtHeader.Rows[0]["InspectTime"].ToString();
                this.uTextReviewUserID.Text = dtHeader.Rows[0]["ReviewUserID"].ToString();
                this.uTextReviewUserName.Text = dtHeader.Rows[0]["ReviewUserName"].ToString();
                this.uDateReviewDate.Value = dtHeader.Rows[0]["ReviewDate"].ToString();
                this.uTextAdmitUserID.Text = dtHeader.Rows[0]["AdmitUserID"].ToString();
                this.uTextAdmitUserName.Text = dtHeader.Rows[0]["AdmitUserName"].ToString();
                this.uDateAdmitDate.Value = dtHeader.Rows[0]["AdmitDate"].ToString();
                this.uTextEtcDescQA.Text = dtHeader.Rows[0]["EtcDescQA"].ToString();
                this.uTextWMSTransFlag.Text = dtHeader.Rows[0]["WMSTFlag"].ToString();
                this.uTextSamplingLot.Text = dtHeader.Rows[0]["SamplingLotCount"].ToString();
                this.uTextShipmentCount.Text = dtHeader.Rows[0]["ShipementCount"].ToString();

                if (dtHeader.Rows[0]["CompleteFlag"].ToString() == "T")
                {
                    this.uCheckCompleteFlag.CheckedValue = true;
                    this.uCheckCompleteFlag.Enabled = false;

                    this.uOptionPassFailFlag.Enabled = false;
                    this.uOptionICPCheckFlag.Enabled = false;
                    this.uDateAdmitDate.ReadOnly = true;
                    this.uDateInspectDate.ReadOnly = true;
                    this.uDateInspectTime.ReadOnly = true;
                    this.uDateReceiptDate.ReadOnly = true;
                    this.uDateReviewDate.ReadOnly = true;
                    this.uDateTimeReceiptTime.ReadOnly = true;
                }
                else
                {
                    this.uCheckCompleteFlag.CheckedValue = false;
                    this.uCheckCompleteFlag.Enabled = true;

                    this.uOptionPassFailFlag.Enabled = true;
                    this.uOptionICPCheckFlag.Enabled = true;
                    this.uDateAdmitDate.ReadOnly = false;
                    this.uDateInspectDate.ReadOnly = false;
                    this.uDateInspectTime.ReadOnly = false;
                    this.uDateReceiptDate.ReadOnly = false;
                    this.uDateReviewDate.ReadOnly = false;
                    this.uDateTimeReceiptTime.ReadOnly = false;

                    this.uDateInspectTime.Value = null;
                }

                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled && this.uOptionPassFailFlag.CheckedIndex.Equals(1))
                {
                    this.uButtonMoveAbnormal.Show();
                    this.uButtonMoveAbnormal.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot Table 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        private void SearchInspectReqLot(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                // LotTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqLot), "MatInspectReqLot");
                QRPINS.BL.INSIMP.MatInspectReqLot clsLot = new QRPINS.BL.INSIMP.MatInspectReqLot();
                brwChannel.mfCredentials(clsLot);

                DataTable dtLot = clsLot.mfReadINSMatInspectReqLot(strPlantCode, strReqNo, strReqSeq);

                this.uGridLot.DataSource = dtLot;
                this.uGridLot.DataBind();

                if (dtLot.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridLot, 0);
                }

                m_bolCheckAQL = Convert.ToBoolean(dtLot.Rows[0]["AQLFlag"]);

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    if (dtLot.Rows[i]["SamplingLotFlag"].ToString() == "T")
                        this.uGridLot.Rows[i].Cells["Check"].Value = true;
                }

                this.uTextLotCount.Text = dtLot.DefaultView.ToTable(true, "MoLotNo").Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item Table 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        private void SearchInspectReqItem(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // ItemTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqItem), "MatInspectReqItem");
                QRPINS.BL.INSIMP.MatInspectReqItem clsItem = new QRPINS.BL.INSIMP.MatInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadINSMatInspectReqItem(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                this.uGridReqItem.DataSource = dtItem;
                this.uGridReqItem.DataBind();

                if (dtItem.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridReqItem, 0);

                    m_intAcceptCount = Convert.ToInt32(dtItem.Rows[0]["AcceptCount"]);
                    m_intRejectCount = Convert.ToInt32(dtItem.Rows[0]["RejectCount"]);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Sampling 그리드 데이터 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        private void SearchInspectReqItemSampling(String strPlantCode, String strReqNo, String strReqSeq)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // ItemTable 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqItem), "MatInspectReqItem");
                QRPINS.BL.INSIMP.MatInspectReqItem clsItem = new QRPINS.BL.INSIMP.MatInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadINSMatInspectReqItemSamlpling(strPlantCode, strReqNo, strReqSeq, m_resSys.GetString("SYS_LANG"));

                this.uGridReqSampling.DataSource = dtItem;
                this.uGridReqSampling.DataBind();

                if (this.uGridReqSampling.Rows.Count > 0)
                {
                    // Unbount 컬럼 삭제
                    this.uGridReqSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();

                    // SampleSize 만큼 컬럼생성 Method 호출
                    String[] strLastColKey = { "Mean", "InspectResultFlag" };
                    CreateColumn(this.uGridReqSampling, 0, "SampleSize", "GrandSampleSize", strLastColKey);

                    SetSamplingGridColumn(strPlantCode, strReqNo, strReqSeq);
                }

                if (dtItem.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridReqSampling, 0);

                    m_dblAQLSampleSize = Convert.ToDouble(dtItem.Rows[0]["GrandSampleSize"]);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 지정된 Size만큼 컬럼추가하는 Method
        /// </summary>
        /// <param name="uGrid"> 그리드명 </param>
        /// <param name="intBandIndex"> Band인덱스 </param>
        /// <param name="SizeColKey"> Size가 입력된 컬럼Key </param>
        /// <param name="newColName"> 생성될 컬럼명 </param>
        /// <param name="strMaskInput"> MaskInput </param>
        /// <param name="strDefaultValue"> 기본값 </param>
        /// <param name="strLastIndexColKey"> 생성된 컬럼보다 뒤에 위치해야할 컬럼들의 키가 저장된 배열 </param>
        private void CreateColumn(Infragistics.Win.UltraWinGrid.UltraGrid uGrid
                                , int intBandIndex
                                , String strSampleSizeColKey
                                , String strGradeSizeColKey
                                , String[] strLastIndexColKey
                                )
        {
            try
            {
                // 그리드 이벤트 헤제
                this.uGridReqSampling.EventManager.AllEventsEnabled = false;

                QRPCOM.QRPUI.WinGrid wGrid = new QRPCOM.QRPUI.WinGrid();

                //// SampleSize가 될 최대값 찾기
                //int intSampleMax = 0;
                //int intGradeMax = 0;
                int inttotMax = 0;
                //int intMaxPoint = 0;

                //for (int i = 0; i < uGrid.Rows.Count; i++)
                //{
                //    if (Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) * Convert.ToInt32(uGrid.Rows[i].Cells[strGradeSizeColKey].Value) *
                //        Convert.ToInt32(uGrid.Rows[i].Cells["Point"].Value)> inttotMax)
                //    {
                //        inttotMax = Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) * 
                //                    Convert.ToInt32(uGrid.Rows[i].Cells[strGradeSizeColKey].Value) * 
                //                    Convert.ToInt32(uGrid.Rows[i].Cells["Point"].Value);
                //        intSampleMax = Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                //        intGradeMax = Convert.ToInt32(uGrid.Rows[i].Cells[strGradeSizeColKey].Value);
                //        intMaxPoint = Convert.ToInt32(uGrid.Rows[i].Cells["Point"].Value);
                //    }
                //}

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if (Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) > inttotMax)
                    {
                        inttotMax = Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);
                    }
                }
                
                if (inttotMax > 99)
                    inttotMax = 99;

                // 상세 SampleSize * Grade SampleSize 만큼 컬럼생성
                for (int i = 1; i <= inttotMax; i++)
                {
                    // 컬럼이 이미 존재하는경우(수입검사항목 테이블에 이미 저장되어 있는경우)
                    if (uGrid.DisplayLayout.Bands[intBandIndex].Columns.Exists(i.ToString()))
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Header.Caption = "X" + i.ToString();
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Hidden = false;
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].MaskInput = "{double:-5.5}";
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].DefaultCellValue = 0.0;
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Width = 80;
                    }
                    // 컬럼 신규생성(수입검사항목 테이블에 저장된 데이터가 아직없는경우)
                    else
                    {
                        wGrid.mfSetGridColumn(uGrid, intBandIndex, i.ToString(), "X" + i.ToString(), false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                        , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "{double:-5.5}", "0.0");
                    }
                    this.uGridReqSampling.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].PromptChar = ' ';
                    this.uGridReqSampling.DisplayLayout.Bands[intBandIndex].Columns[i.ToString()].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                }

                // 추가된 컬럼보다 뒤에 있어야 하는 컬럼들이 있으면 뒤로 보냄
                if (strLastIndexColKey.Length > 0)
                {
                    int LastIndex = uGrid.DisplayLayout.Bands[intBandIndex].Columns.Count;
                    for (int i = 0; i < strLastIndexColKey.Length; i++)
                    {
                        uGrid.DisplayLayout.Bands[intBandIndex].Columns[strLastIndexColKey[i]].Header.VisiblePosition = LastIndex + i;
                    }
                }

                // Size 만큼의 Cell만 입력 가능하도록나머지는 편집불가처리
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    //int ActivateNum = Convert.ToInt32(uGrid.Rows[i].Cells[strGradeSizeColKey].Value) *
                    //                  Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value) *
                    //                  Convert.ToInt32(uGrid.Rows[i].Cells["Point"].Value);
                    int ActivateNum = Convert.ToInt32(uGrid.Rows[i].Cells[strSampleSizeColKey].Value);

                    // Xn 컬럼 MAX 99개으로 50초과이면 99부터 시작, 99이하면 intGradeMax * intSampleMax * intMaxPoint 처리
                    if (ActivateNum > 99)
                        ActivateNum = 99;

                    for (int j = 1; j <= inttotMax; j++)
                    {
                        if (j <= ActivateNum)
                        {
                            uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.BackColor = Color.Empty;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                            uGrid.Rows[i].Cells[j.ToString()].Hidden = false;
                        }
                        else
                        {
                            uGrid.Rows[i].Cells[j.ToString()].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.BackColor = Color.Gainsboro;
                            //uGrid.Rows[i].Cells[j.ToString()].Appearance.ForeColor = Color.Empty;
                            uGrid.Rows[i].Cells[j.ToString()].Hidden = true;
                        }
                    }
                }

                // 그리드 이벤트 등록
                this.uGridReqSampling.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 데이터 유형에 맞게 Xn컬럼 설정
        /// </summary>
        private void SetSamplingGridColumn(string strPlantCode, string strReqNo, string strReqSeq)
        {
            try
            {
                // 그리드 이벤트 헤제
                this.uGridReqSampling.EventManager.AllEventsEnabled = false;

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 데이터 유형에 따라 셀 속성 설정
                WinGrid wGrid = new WinGrid();

                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqItem), "MatInspectReqItem");
                QRPINS.BL.INSIMP.MatInspectReqItem clsItem = new QRPINS.BL.INSIMP.MatInspectReqItem();
                brwChannel.mfCredentials(clsItem);

                // 합/부 데이터 테이블
                DataTable dtDataType = clsCom.mfReadCommonCode("C0022", m_resSys.GetString("SYS_LANG"));

                // 선택 DropDown 적용용 DataTable
                int intStart = this.uGridReqSampling.DisplayLayout.Bands[0].Columns["1"].Index;
                int intSampleSize = 0;

                // 데이터 유형이 설명일때 Cell 스타일을 텍스트로 설정하기 위한 구문
                Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings MaskString = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings();
                MaskString.DataType = typeof(String);
                MaskString.MaxLength = 50;
                MaskString.MaskInput = "";

                Infragistics.Win.EmbeddableEditorBase editorString = new Infragistics.Win.EditorWithText(new Infragistics.Win.UltraWinEditors.DefaultEditorOwner(MaskString));

                for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                {
                    //intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value) * 
                    //                Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["GrandSampleSize"].Value) *
                    //                Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["Point"].Value);
                    intSampleSize = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["SampleSize"].Value);

                    // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                    if (intSampleSize > 99) intSampleSize = 99;

                    // 계량
                    if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "1")
                    {
                        this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridReqSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            this.uGridReqSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            if (this.uGridReqSampling.Rows[i].Cells[j].Value == null || this.uGridReqSampling.Rows[i].Cells[j].Value.Equals(DBNull.Value))
                            {
                                this.uGridReqSampling.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // 계수
                    else if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "2")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridReqSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Double;
                            this.uGridReqSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Right;
                            if (this.uGridReqSampling.Rows[i].Cells[j].Value == null || this.uGridReqSampling.Rows[i].Cells[j].Value.Equals(DBNull.Value))
                            {
                                this.uGridReqSampling.Rows[i].Cells[j].Value = "0.0";
                            }
                        }
                    }
                    // OK/NG
                    else if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "3")
                    {
                        //this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        //this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Appearance.BackColor = Color.Gainsboro;

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridReqSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            wGrid.mfSetGridCellValueList(this.uGridReqSampling, i, this.uGridReqSampling.Rows[i].Cells[j].Column.Key, "", "선택", dtDataType);
                            this.uGridReqSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            if (this.uGridReqSampling.Rows[i].Cells[j].Value == null || this.uGridReqSampling.Rows[i].Cells[j].Value.Equals(DBNull.Value) || this.uGridReqSampling.Rows[i].Cells[j].Value.Equals(string.Empty))
                            {
                                this.uGridReqSampling.Rows[i].Cells[j].Value = "OK";
                            }
                        }
                    }
                    // 설명
                    else if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "4")
                    {
                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridReqSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
                            this.uGridReqSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
                            // MaxLength 지정방법;;;;
                            this.uGridReqSampling.Rows[i].Cells[j].Editor = editorString;
                            if (this.uGridReqSampling.Rows[i].Cells[j].Value == null || this.uGridReqSampling.Rows[i].Cells[j].Value.Equals(DBNull.Value))
                            {
                                this.uGridReqSampling.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                    // 선택
                    else if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString() == "5")
                    {
                        // 검사분류/유형/항목/DataType 에 따른 선택항목 조회 Method 호출
                        //int intReqLotSeq = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["ReqLotSeq"].Value);
                        int intReqLotSeq = 0;
                        int intReqItemSeq = Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["ReqItemSeq"].Value);

                        DataTable dtSelect = clsItem.mfReadINSMatInspectReqItem_DataTypeSelect(strPlantCode, strReqNo, strReqSeq, intReqLotSeq, intReqItemSeq, m_resSys.GetString("SYS_LANG"));

                        for (int j = intStart; j < intStart + intSampleSize; j++)
                        {
                            this.uGridReqSampling.Rows[i].Cells[j].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
                            this.uGridReqSampling.Rows[i].Cells[j].Appearance.TextHAlign = Infragistics.Win.HAlign.Center;
                            wGrid.mfSetGridCellValueList(this.uGridReqSampling, i, this.uGridReqSampling.Rows[i].Cells[j].Column.Key, "", "선택", dtSelect);
                            if (this.uGridReqSampling.Rows[i].Cells[j].Value == null || this.uGridReqSampling.Rows[i].Cells[j].Value.Equals(DBNull.Value))
                            {
                                this.uGridReqSampling.Rows[i].Cells[j].Value = "";
                            }
                        }
                    }
                }
                // 그리드 이벤트 등록
                this.uGridReqSampling.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 계수/계량형 결과값 검사 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementMeasureCount(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Instance 객체 생성
                QRPSTA.STABAS clsSTABAS = new QRPSTA.STABAS();
                // 구조체 변수 생성
                QRPSTA.STABasic structSTA = new QRPSTA.STABasic();

                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                //int intSampleSize = Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["GrandSampleSize"].Value) *
                //                            Convert.ToInt32(e.Cell.Row.Cells["Point"].Value);
                int intSampleSize = Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value);

                // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                if (intSampleSize > 99) intSampleSize = 99;

                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                int intLastIndex = intSampleSize + intStart;


                // Double형 배열 생성
                double[] dblXn = new double[intLastIndex - intStart];
                
                // Loop돌며 배열에 값 저장
                for (int i = 0; i < intLastIndex - intStart; i++)
                {
                    int intIndex = intStart + i;
                    double dblValue = 0;
                    if (e.Cell.Row.Cells[intIndex].Value == DBNull.Value)
                    {
                        dblValue = 0;
                    }
                    else
                    {
                        dblValue = Convert.ToDouble(ReturnDecimalValue(e.Cell.Row.Cells[intIndex].Value.ToString()));
                    }
                    dblXn[i] = dblValue;
                }

                // 계산 Method 호출
                bool bolResultFlag;
                double dblLowerSpec, dblUpperSpec;
                string strSpecRange;

                //규격상하한이 있는지 체크하여 합부판정 적용을 판단한다.
                if ((e.Cell.Row.Cells["LowerSpec"].Value == DBNull.Value || e.Cell.Row.Cells["LowerSpec"].Value.ToString() == "") &&
                    (e.Cell.Row.Cells["UpperSpec"].Value == DBNull.Value || e.Cell.Row.Cells["UpperSpec"].Value.ToString() == ""))
                {
                    dblLowerSpec = 0;
                    dblUpperSpec = 0;
                    strSpecRange = "";
                    bolResultFlag = false;
                }
                else
                {
                    if (e.Cell.Row.Cells["LowerSpec"].Value == DBNull.Value || e.Cell.Row.Cells["LowerSpec"].Value.ToString() == "")
                        dblLowerSpec = 0;
                    else
                        dblLowerSpec = Convert.ToDouble(e.Cell.Row.Cells["LowerSpec"].Value);

                    if (e.Cell.Row.Cells["UpperSpec"].Value == DBNull.Value || e.Cell.Row.Cells["UpperSpec"].Value.ToString() == "")
                        dblUpperSpec = 0;
                    else
                        dblUpperSpec = Convert.ToDouble(e.Cell.Row.Cells["UpperSpec"].Value);

                    strSpecRange = e.Cell.Row.Cells["SpecRange"].Value.ToString();
                    bolResultFlag = true;
                }
                structSTA = clsSTABAS.mfCalcBasicStat(dblXn, dblLowerSpec, dblUpperSpec, strSpecRange);

                if (bolResultFlag == true)
                {
                    // 합부판정
                    if (structSTA.AcceptFlag)
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                    else
                        e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                }

                // 불량갯수
                e.Cell.Row.Cells["FaultQty"].Value = structSTA.FalutCount;

                // 평균
                e.Cell.Row.Cells["Mean"].Value = structSTA.Mean;
                // MaxValue
                e.Cell.Row.Cells["MaxValue"].Value = structSTA.Max;
                // MinValue
                e.Cell.Row.Cells["MinValue"].Value = structSTA.Min;
                // Range
                e.Cell.Row.Cells["DataRange"].Value = structSTA.Range;
                // StdDev
                e.Cell.Row.Cells["StdDev"].Value = structSTA.StdDev;

                if (bolResultFlag == true)
                {
                    // Cp
                    e.Cell.Row.Cells["Cp"].Value = structSTA.Cp;
                    // Cpk
                    e.Cell.Row.Cells["Cpk"].Value = structSTA.Cpk;
                }

                for (int i = 0; i < this.uGridReqItem.Rows.Count; i++)
                {
                    //if (Convert.ToInt32(this.uGridReqItem.Rows[i].Cells["ReqLotSeq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["ReqLotSeq"].Value) &&
                    //    Convert.ToInt32(this.uGridReqItem.Rows[i].Cells["ReqItemSeq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["ReqItemSeq"].Value))
                    if (Convert.ToInt32(this.uGridReqItem.Rows[i].Cells["ReqItemSeq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["ReqItemSeq"].Value))
                    {
                        // 평균
                        this.uGridReqItem.Rows[i].Cells["Mean"].Value = structSTA.Mean;
                        // MaxValue
                        this.uGridReqItem.Rows[i].Cells["MaxValue"].Value = structSTA.Max;
                        // MinValue
                        this.uGridReqItem.Rows[i].Cells["MinValue"].Value = structSTA.Min;
                        // Range
                        this.uGridReqItem.Rows[i].Cells["DataRange"].Value = structSTA.Range;
                        // StdDev
                        this.uGridReqItem.Rows[i].Cells["StdDev"].Value = structSTA.StdDev;

                        if (bolResultFlag == true)
                        {
                            // Cp
                            this.uGridReqItem.Rows[i].Cells["Cp"].Value = structSTA.Cp;
                            // Cpk
                            this.uGridReqItem.Rows[i].Cells["Cpk"].Value = structSTA.Cpk;
                            //////// 합/부
                            //////// 합부판정
                            //////if (structSTA.AcceptFlag)
                            //////    this.uGridReqItem.Rows[i].Cells["InspectResultFlag"].Value = "OK";
                            //////else
                            //////    this.uGridReqItem.Rows[i].Cells["InspectResultFlag"].Value = "NG";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// OK/NG 합/부 판정 Method
        /// </summary>
        /// <param name="e"></param>
        private void JudgementOkNg(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                int intStart = e.Cell.Row.Cells["1"].Column.Index;

                //int intSampleSize = Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["GrandSampleSize"].Value) *
                //                            Convert.ToInt32(e.Cell.Row.Cells["Point"].Value);
                int intSampleSize = Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value);

                // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                if (intSampleSize > 99) intSampleSize = 99;

                // Xn 컬럼의 마지막 컬럼 Index를 변수에 저장
                int intLastIndex = intSampleSize + intStart;

                bool bolCheck = true;
                int intFaultCount = 0;
                // Loop 돌며 결과값 검사
                for (int i = intStart; i < intLastIndex; i++)
                {
                    if (e.Cell.Row.Cells[i].Value.ToString() == "NG")
                    {
                        bolCheck = false;
                        intFaultCount += 1;
                    }
                }

                // 합부판정
                if (bolCheck)
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                else
                    e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";

                // 불량수량
                e.Cell.Row.Cells["FaultQty"].Value = intFaultCount;

                //// 검사결과 그리드 업데이트
                //for (int i = 0; i < this.uGridReqItem.Rows.Count; i++)
                //{
                //    //if (Convert.ToInt32(this.uGridReqItem.Rows[i].Cells["ReqLotSeq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["ReqLotSeq"].Value) &&
                //    //    Convert.ToInt32(this.uGridReqItem.Rows[i].Cells["ReqItemSeq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["ReqItemSeq"].Value))
                //    if (Convert.ToInt32(this.uGridReqItem.Rows[i].Cells["ReqItemSeq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["ReqItemSeq"].Value))
                //    {
                //        if (bolCheck)
                //            this.uGridReqItem.Rows[i].Cells["InspectResultFlag"].Value = "OK";
                //        else
                //            this.uGridReqItem.Rows[i].Cells["InspectResultFlag"].Value = "NG";
                //    }
                //}
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Events...
        // ContentsGroupBox 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                ////if (this.uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    Point point = new Point(0, 170);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridInspectList.Height = 60;
                ////}
                ////else
                ////{
                ////    Point point = new Point(0, 825);
                ////    this.uGroupBoxContentsArea.Location = point;
                ////    this.uGridInspectList.Height = 720;
                ////    for (int i = 0; i < this.uGridInspectList.Rows.Count; i++)
                ////    {
                ////        this.uGridInspectList.Rows[i].Fixed = false;
                ////    }

                ////    // Control Clear
                ////    Clear();
                ////}

                if (this.uGroupBoxContentsArea.Expanded)
                {
                    this.uGridInspectList.Rows.FixedRows.Clear();
                    Clear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Lot 그리드에서 Sampling Lot 수 이상 선택 불가능하게 하는 이벤트
        private void uGridLot_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 주석 처리 2012.02.21 Yoon :  샘플링 Lot보다 더 많이 검사가 가능하다고 함.
                // 체크박스 컬럼일때
                ////if (e.Cell.Column.Key == "Check")
                ////{
                ////    // AQL 비적용일경우만 Sampling Lot 적용
                ////    if (m_bolCheckAQL == false)
                ////    {
                ////        int intCount = 0;
                ////        for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                ////        {
                ////            // 체크된 상태이면 카운트 증가
                ////            if (Convert.ToBoolean(this.uGridLot.Rows[i].Cells["Check"].Value) == true)
                ////                intCount++;
                ////        }


                ////        // 카운트가 Sampling Lot 수와 같으면 더이상 체크하지 못하도록 메세지 박스
                ////        Double dblCount = Convert.ToDouble(this.uTextSamplingLot.Text);
                ////        if (dblCount == intCount)
                ////        {
                ////            if (Convert.ToBoolean(e.Cell.Value) == false)
                ////            {
                ////                // SystemInfo RecourceSet
                ////                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                ////                WinMessageBox msg = new WinMessageBox();

                ////                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                                    "에러창", "선택에러", "선택할수 있는 LotNo 의 최대수는 Sampling Lot 수를 넘을수 없습니다.", Infragistics.Win.HAlign.Right);
                ////                e.Cell.Value = false;
                ////            }
                ////        }
                ////    }
                ////}

                // 2012.07.12 Lee
                // Sampling Lot수 Lot체크된거만큼 변경되도록 변경
                if (e.Cell.Column.Key.Equals("Check"))
                {
                    this.uGridLot.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                    int intLotCount = 0;
                    for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                    {
                        // Check된만큼 Count 증가
                        if (Convert.ToBoolean(this.uGridLot.Rows[i].Cells["Check"].Value).Equals(true))
                            intLotCount++;
                    }

                    this.uTextSamplingLot.Text = intLotCount.ToString();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검사 Lot 적용 버튼 누를시 이벤트
        private void ultraButton1_Click(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo RecourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uCheckCompleteFlag.Enabled.Equals(true))
                {
                    // 등록된 수입검사규격서가 있는지 확인
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
                    QRPISO.BL.ISOIMP.MaterialInspectSpecH clsMatInspectH = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
                    brwChannel.mfCredentials(clsMatInspectH);

                    // Method 호출
                    DataTable dtMatInspectH = clsMatInspectH.mfReadISOMaterialInspectSpecHCheck(m_strPlantCode, "", this.uTextMaterialCode.Text, this.uTextSpecNo.Text);

                    if (dtMatInspectH.Rows.Count > 0)
                    {
                        int intCheckCOunt = 0;
                        for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                        {
                            if (this.uGridLot.Rows[i].Cells["Check"].Value.Equals(true))
                            {
                                intCheckCOunt += 1;
                            }
                        }
                        //////// Sampling Lot 수량과 체크된 Lot 수량이 다를경우
                        //////if (Convert.ToInt32(this.uTextSamplingLot.Text) != intCheckCOunt)
                        //////{
                        //////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //////                        , "확인창", "Sampling 수량 확인", "총 " + intCheckCOunt.ToString() + " 개의 Sampling Lot을 선택해야 합니다"
                        //////                        , Infragistics.Win.HAlign.Right);
                        //////    return;
                        //////}
                        if (intCheckCOunt > 0)
                        {
                            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M001060", "M000908", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                            {
                                // 작성완료 체크 해제 상태로
                                this.uCheckCompleteFlag.Checked = false;
                                // 헤더 / Lot 정보를 DataTable에 담아 반환하는 Method 호출
                                DataTable dtSaveHeader = RtnHeaderDataTable();
                                DataTable dtSaveLot = RtnLotDataTable();

                                brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSIMP.MatInspectReqH), "MatInspectReqH");
                                QRPINS.BL.INSIMP.MatInspectReqH clsHeader = new QRPINS.BL.INSIMP.MatInspectReqH();
                                brwChannel.mfCredentials(clsHeader);

                                // 프로그래스 팝업창 생성
                                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                Thread t1 = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                // 저장 Method 호출
                                String strErrRtn = clsHeader.mfSaveINSMatInspectReqH(dtSaveHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtSaveLot);

                                // 팦업창 Close
                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                // 결과 확인
                                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                if (ErrRtn.ErrNum == 0)
                                {
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                    "M001135", "M001037", "M000930",
                                                                    Infragistics.Win.HAlign.Right);

                                    String strReqNo = ErrRtn.mfGetReturnValue(0);
                                    String strReqSeq = ErrRtn.mfGetReturnValue(1);

                                    FileUpload(strReqNo, strReqSeq);

                                    // 그리드 갱신
                                    SearchInspectReqHDetail(m_strPlantCode, strReqNo, strReqSeq);       // 헤더
                                    SearchInspectReqLot(m_strPlantCode, strReqNo, strReqSeq);           // Lot
                                    SearchInspectReqItem(m_strPlantCode, strReqNo, strReqSeq);          // Item
                                    SearchInspectReqItemSampling(m_strPlantCode, strReqNo, strReqSeq);  // Sampling

                                    this.uButtonExcelUpload.Show();
                                    this.uButtonExcelUpload.Enabled = true;

                                    this.uTab.Tabs[1].Selected = true;
                                }
                                else
                                {
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                                }

                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000798", "M000170", "M001059",
                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000798", "M000754", "M000906",
                                                    Infragistics.Win.HAlign.Right);
                    }
                }
                else
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000798", "M000981", "M000993",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 팝업창 이벤트

        // 검색조건 자재코드택스트 박스 버튼 클릭시 팝업창 이벤트

        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        // 검색조건 거래처텍스트 박스 버튼 클릭시 팝업창 이벤트
        private void uTextSearchVendorCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmPOP = new frmPOP0004();
                frmPOP.ShowDialog();

                this.uTextSearchVendorCode.Text = frmPOP.CustomerCode;
                this.uTextSearchVendorName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 시험자 텍스트 박스 버튼 클릭시  팝업창 이벤트
        private void uTextInspectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                    return;
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (PlantCode == null || PlantCode == "")
                {
                    PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (m_strPlantCode != frmPOP.PlantCode)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M000275", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001254", m_resSys.GetString("SYS_LANG")) + this.uTextPlantName.Text + msg.GetMessge_Text("M000001", m_resSys.GetString("SYS_FONTNAME"))
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextInspectUserID.Text = "";
                    this.uTextInspectUserName.Text = "";
                }
                else
                {
                    this.uTextInspectUserID.Text = frmPOP.UserID;
                    this.uTextInspectUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검토자 텍스트 박스 버튼 클릭시  팝업창 이벤트
        private void uTextReviewUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                    return;

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (m_strPlantCode != frmPOP.PlantCode)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M000275", m_resSys.GetString("SYS_LANG"))
                                                , msg.GetMessge_Text("M001254", m_resSys.GetString("SYS_LANG")) + this.uTextPlantName.Text + msg.GetMessge_Text("M000001", m_resSys.GetString("SYS_FONTNAME"))
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextReviewUserID.Text = "";
                    this.uTextReviewUserName.Text = "";
                }
                else
                {
                    this.uTextReviewUserID.Text = frmPOP.UserID;
                    this.uTextReviewUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 승인자 텍스트 박스 버튼 클릭시  팝업창 이벤트
        private void uTextAdmitUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //승인자 고정으로 인한 주석처리 --> PSTS 고정없음
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                if (PlantCode == null || PlantCode == "")
                {
                    PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = PlantCode;
                frmPOP.ShowDialog();

                if (m_strPlantCode != frmPOP.PlantCode)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    string strLang = m_resSys.GetString("SYS_LANG");
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M000798", strLang)
                                                , msg.GetMessge_Text("M000275", strLang)
                                                , msg.GetMessge_Text("M001254", strLang) + this.uTextPlantName.Text + msg.GetMessge_Text("M000001", strLang)
                                                , Infragistics.Win.HAlign.Right);

                    this.uTextAdmitUserID.Text = "";
                    this.uTextAdmitUserName.Text = "";
                }
                else
                {
                    this.uTextAdmitUserID.Text = frmPOP.UserID;
                    this.uTextAdmitUserName.Text = frmPOP.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 자재, 거래처, 사용자 TextBox에서 키다운 이벤트

        // 검색조건 자재코드 키다운 이벤트
        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }

                                ////Infragistics.Win.UltraWinToolTip.UltraToolTipInfo uTooltip = uToolTipManager.GetUltraToolTip(this.uTextSearchMaterialName);
                                ////uTooltip.ToolTipText = this.uTextSearchMaterialName.Text;
                                ////uTooltip.ToolTipImage = Infragistics.Win.ToolTipImage.Info;
                                ////uTooltip.Appearance.BackColor = Color.White;
                                ////uTooltip.Appearance.BackColor2 = Color.LightSkyBlue;
                                ////uTooltip.Appearance.BackGradientStyle = Infragistics.Win.GradientStyle.Circular;
                                ////uTooltip.ToolTipTitle = "자재명";
                                ////uTooltip.ToolTipTitleAppearance.ForeColor = Color.Red;
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.SelectedText == this.uTextSearchMaterialCode.Text)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 거래처 텍스트박스 키다운 이벤트
        private void uTextSearchVendorCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchVendorCode.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strVendorCode = this.uTextSearchVendorCode.Text;

                        // UserName 검색 함수 호출
                        DataTable dtVendor = GetVendorInfo(strVendorCode);

                        if (dtVendor.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtVendor.Rows.Count; i++)
                            {
                                this.uTextSearchVendorName.Text = dtVendor.Rows[i]["VendorName"].ToString();
                            }
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000168",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextSearchVendorCode.Text = "";
                            this.uTextSearchVendorName.Text = "";
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchVendorCode.TextLength <= 1 || this.uTextSearchVendorCode.SelectedText == this.uTextSearchVendorCode.Text)
                    {
                        this.uTextSearchVendorName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 시험자 텍스트박스 키다운 이벤트
        private void uTextInspectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                    return;

                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextInspectUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextInspectUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(m_strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextInspectUserID.Text = "";
                            this.uTextInspectUserName.Text = "";
                        }
                        else
                        {
                            this.uTextInspectUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextInspectUserID.TextLength <= 1 || this.uTextInspectUserID.SelectedText == this.uTextInspectUserID.Text)
                    {
                        this.uTextInspectUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextReviewUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                    return;

                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextReviewUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextReviewUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(m_strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextReviewUserID.Text = "";
                            this.uTextReviewUserName.Text = "";
                        }
                        else
                        {
                            this.uTextReviewUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextReviewUserID.TextLength <= 1 || this.uTextReviewUserID.SelectedText == this.uTextReviewUserID.Text)
                    {
                        this.uTextReviewUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 승인자 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextAdmitUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //승인자 고정으로 인한 주석처리 --> PSTS 고정없음
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextAdmitUserID.Text != "")
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        String strWriteID = this.uTextAdmitUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(m_strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextAdmitUserID.Text = "";
                            this.uTextAdmitUserName.Text = "";
                        }
                        else
                        {
                            this.uTextAdmitUserName.Text = strRtnUserName;
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextAdmitUserID.TextLength <= 1 || this.uTextAdmitUserID.SelectedText == this.uTextAdmitUserID.Text)
                    {
                        this.uTextAdmitUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 리스트 더블클릭시 상세정보 조회
        private void uGridInspectList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // 더블클릭된 행 고정
                e.Row.Fixed = true;

                // ContentsArea 펼침 상태로
                this.uGroupBoxContentsArea.Expanded = true;

                // 검색조건 변수 설정
                String strReqNo = e.Row.Cells["ReqNo"].Value.ToString();
                String strReqSeq = e.Row.Cells["ReqSeq"].Value.ToString();

                PlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                VendorCode = e.Row.Cells["VendorCode"].Value.ToString();
                MaterialCode = e.Row.Cells["MaterialCode"].Value.ToString();
                MoldSeq = e.Row.Cells["MoldSeq"].Value.ToString();

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 헤더 상세조회 Method 호출
                SearchInspectReqHDetail(PlantCode, strReqNo, strReqSeq);

                // Lot Grid 데이터 조회 Method 호출
                SearchInspectReqLot(PlantCode, strReqNo, strReqSeq);

                // Item Grid 데이터 조회 Method 호출
                SearchInspectReqItem(PlantCode, strReqNo, strReqSeq);

                // Sampling Grid 데이터 조회 Method 호출
                SearchInspectReqItemSampling(PlantCode, strReqNo, strReqSeq);

                this.uTab.Tabs[1].Selected = true;

                if (!this.uCheckCompleteFlag.Enabled)
                {
                    this.uGridReqSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    this.uGridLot.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;

                    this.uButtonExcelUpload.Hide();
                    this.uButtonExcelUpload.Enabled = false;
                }
                else
                {
                    this.uGridReqSampling.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    this.uGridLot.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                    this.uButtonExcelUpload.Show();
                    this.uButtonExcelUpload.Enabled = true;
                }

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 자동으로 합불판정
        private void uGridReqSampling_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == DBNull.Value || e.Cell.Value == null)
                {
                    e.Cell.Value = e.Cell.OriginalValue;
                    return;
                }

                this.uGridReqSampling.EventManager.AllEventsEnabled = false;
                // 입력한 셀이 Xn 컬럼일때데이터유형이
                if (e.Cell.Column.Header.Caption.Contains("X") && !e.Cell.Column.Key.Equals("MAX"))
                {
                    // 입력줄의  계량/계수인경우
                    if (e.Cell.Row.Cells["DataType"].Value.ToString() == "1")// || e.Cell.Row.Cells["DataType"].Value.ToString() == "2")
                    {
                        JudgementMeasureCount(e);

                        // 2012.03.06 추가내용 (측정값으로 자동 계산)       // 높이가 계량형으로 바뀌지 않는경우 if문 밖으로 이동
                        if (e.Cell.Row.Cells["InspectItemCode"].Value.ToString().Equals("II01020501") ||
                            e.Cell.Row.Cells["InspectItemCode"].Value.ToString().Equals("II01020502") ||
                            e.Cell.Row.Cells["InspectItemCode"].Value.ToString().Equals("II01020503"))
                        {
                            decimal dblDiameter = 0;
                            decimal dblHeight = 0;
                            decimal dblWeight = 0;
                            int intDensityRowIndex = 0;

                            for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                            {
                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020501"))
                                    dblDiameter = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[e.Cell.Column.Key].Value.ToString());

                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020502"))
                                    dblHeight = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[e.Cell.Column.Key].Value.ToString());

                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020503"))
                                    dblWeight = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[e.Cell.Column.Key].Value.ToString());

                                // EMC_TABLET DENSITY 항목인경우
                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020504"))
                                    intDensityRowIndex = i;
                            }

                            if (!dblDiameter.Equals(0) && !dblHeight.Equals(0))
                                this.uGridReqSampling.Rows[intDensityRowIndex].Cells[e.Cell.Column.Key].Value = dblWeight / ((dblDiameter/2) * (dblDiameter/2) * dblHeight * Convert.ToDecimal(Math.PI));
                        }
                    }
                    // 입력줄의 데이터 유형이 Ok/Ng 인경우
                    else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "3")
                    {
                        JudgementOkNg(e);
                    }
                    else if (e.Cell.Row.Cells["DataType"].Value.ToString() == "4")
                    {
                        // 2012.03.06 추가내용 (측정값으로 자동 계산)       // 높이가 계량형으로 바뀌지 않는경우 if문 밖으로 이동
                        if (e.Cell.Row.Cells["InspectItemCode"].Value.ToString().Equals("II01020502"))
                        {
                            decimal dblDiameter = 0;
                            decimal dblHeight = 0;
                            decimal dblWeight = 0;
                            int intDensityRowIndex = 0;

                            for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                            {
                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020501"))
                                    dblDiameter = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[e.Cell.Column.Key].Value.ToString());

                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020502"))
                                    dblHeight = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[e.Cell.Column.Key].Value.ToString());

                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020503"))
                                    dblWeight = ReturnDecimalValue(this.uGridReqSampling.Rows[i].Cells[e.Cell.Column.Key].Value.ToString());

                                // EMC_TABLET DENSITY 항목인경우
                                if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals("II01020504"))
                                    intDensityRowIndex = i;
                            }

                            if (!dblDiameter.Equals(0) && !dblHeight.Equals(0))
                            {
                                this.uGridReqSampling.Rows[intDensityRowIndex].Cells[e.Cell.Column.Key].Value = dblWeight / ((dblDiameter / 2) * (dblDiameter / 2) * dblHeight * Convert.ToDecimal(Math.PI));
                                Infragistics.Win.UltraWinGrid.CellEventArgs eDes = new Infragistics.Win.UltraWinGrid.CellEventArgs(this.uGridReqSampling.Rows[intDensityRowIndex].Cells[e.Cell.Column.Key]);
                                JudgementMeasureCount(eDes);
                            }

                            JudgementMeasureCount(e);
                        }
                    }

                    #region Ship 단위 처리부분 주석처리
                    //////// SampleSize 단위가 Ship 일때
                    //////if (e.Cell.Row.Cells["GrandEveryTarget"].Value.ToString().Equals("2"))
                    //////{
                    //////    // 이미 진행되었는지판단
                    //////    if (Convert.ToBoolean(e.Cell.Row.Cells["TargetIngFlag"].Value) == false)
                    //////    {
                    //////        int intStart = this.uGridReqSampling.DisplayLayout.Bands[0].Columns["1"].Index;
                    //////        int intSampleSize = 0;

                    //////        intSampleSize = Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["GrandSampleSize"].Value) *
                    //////                        Convert.ToInt32(e.Cell.Row.Cells["Point"].Value);

                    //////        // Xn 컬럼 MAX 50개으로 50초과이면 50부터 시작, 50이하면 SampleSize * GrandSampleSize * Point 처리
                    //////        if (intSampleSize > 99) intSampleSize = 99;

                    //////        e.Cell.Row.Cells["TargetIngFlag"].Value = true;

                    //////        for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    //////        {
                    //////            // Lot별로 검사항목이 같은것들 검사값 입력필드 수정불가 상태로 만든다
                    //////            if (this.uGridReqSampling.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals(e.Cell.Row.Cells["InspectItemCode"].Value.ToString()))
                    //////            {
                    //////                // 현재행은 건너뛴다
                    //////                if (e.Cell.Row.Index.Equals(this.uGridReqSampling.Rows[i].Index))
                    //////                    continue;

                    //////                //if (this.uGridReqSampling.Rows[i].Cells[intStart].Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                    //////                //    break;

                    //////                for (int j = intStart; j < intStart + intSampleSize; j++)
                    //////                {
                    //////                    this.uGridReqSampling.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    //////                    this.uGridReqSampling.Rows[i].Cells[j].Appearance.BackColor = Color.Gainsboro;
                    //////                    this.uGridReqSampling.Rows[i].Cells[j].Appearance.ForeColor = Color.Gainsboro;
                    //////                }
                    //////            }
                    //////        }
                    //////    }
                    //////}
                    #endregion
                }
                // 검사결과 컬럼 Update시 헤더 자동 합부판정 
                else if(e.Cell.Column.Key == "InspectResultFlag")
                {
                    if (e.Cell.Value.Equals("NG"))
                    {
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        e.Cell.Row.Cells["FaultQty"].Value = 1;
                    }
                    else if (e.Cell.Value.Equals("OK"))
                    {
                        e.Cell.Row.Cells["InspectIngFlag"].Value = true;
                        e.Cell.Row.Cells["FaultQty"].Value = 0;
                    }
                }
                // 불량수량이 0보다 크면 검사결과 불량판정
                else if (e.Cell.Column.Key.Equals("FaultQty"))
                {
                    //int intSampleSize = Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value) * Convert.ToInt32(e.Cell.Row.Cells["GrandSampleSize"].Value) *
                    //                        Convert.ToInt32(e.Cell.Row.Cells["Point"].Value);
                    int intSampleSize = Convert.ToInt32(e.Cell.Row.Cells["SampleSize"].Value);

                    //if (intSampleSize > 99)
                    //    intSampleSize = 99;
                    if (Convert.ToInt32(e.Cell.Value) <= intSampleSize)
                    {
                        if (e.Cell.Value.Equals(null))
                        {
                            e.Cell.Value = 0;
                        }
                        else if (Convert.ToInt32(e.Cell.Value) > 0)
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "NG";
                            this.uOptionPassFailFlag.CheckedIndex = 1;
                        }
                        else if (Convert.ToInt32(e.Cell.Value).Equals(0))
                        {
                            e.Cell.Row.Cells["InspectResultFlag"].Value = "OK";
                        }

                        if (e.Cell.Row.Cells["DataType"].Value.ToString().Equals("3"))
                        {
                            // Xn 컬럼의 시작 컬럼 Index를 변수에 저장
                            int intStart = e.Cell.Row.Cells["1"].Column.Index;

                            if (intSampleSize > 99)
                                intSampleSize = 99;
                            // Xn컬럼의 마지막 컬럼 Index저장
                            int intLastIndex = intSampleSize + intStart;

                            for (int j = intStart; j < intLastIndex; j++)
                            {
                                if (e.Cell.Row.Cells[j].Column.Index < Convert.ToInt32(e.Cell.Value) + intStart)
                                    e.Cell.Row.Cells[j].Value = "NG";
                                else
                                    e.Cell.Row.Cells[j].Value = "OK";
                            }
                        }
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M000879", "M000610", "M000896"
                                                                , Infragistics.Win.HAlign.Center);
                        e.Cell.Value = e.Cell.OriginalValue;
                    }
                }
                else if (e.Cell.Column.Key.Equals("SampleSize"))
                {
                    if (Convert.ToInt32(e.Cell.Value) >= Convert.ToInt32(e.Cell.Row.Cells["FaultQty"].Value))
                    {
                        //this.uGridReqSampling.DisplayLayout.Bands[0].Columns.ClearUnbound();          // 저장된 컬럼 바인딩 된 상태라 삭제가 안됨;;;

                        // 기존 Xn 컬럼 삭제
                        // 원래 SampleSize 값으로 최대 Xn 을 구한다
                        //int intOldSampleSize = Convert.ToInt32(e.Cell.OriginalValue) * Convert.ToInt32(e.Cell.Row.Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(e.Cell.Row.Cells["Point"].Value);

                        //int intNewSampleSize = Convert.ToInt32(e.Cell.Value) * Convert.ToInt32(e.Cell.Row.Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(e.Cell.Row.Cells["Point"].Value);

                        int intOldSampleSize = Convert.ToInt32(e.Cell.OriginalValue);
                        int intNewSampleSize = Convert.ToInt32(e.Cell.Value);

                        if (intOldSampleSize > 99)
                            intOldSampleSize = 99;

                        if (intNewSampleSize > 99)
                            intNewSampleSize = 99;

                        for (int i = intNewSampleSize + 1; i <= intOldSampleSize; i++)
                        {
                            if(this.uGridReqSampling.DisplayLayout.Bands[0].Columns.Exists(i.ToString()))
                                this.uGridReqSampling.DisplayLayout.Bands[0].Columns[i.ToString()].Hidden = true;
                        }

                        // 컬럼생성 메소드 호출
                        String[] strLastColKey = { "Mean", "SpecRange" };
                        CreateColumn(this.uGridReqSampling, 0, "SampleSize", "GrandSampleSize", strLastColKey);

                        // 데이터 유형에 따른 Xn 컬럼 설정 메소드 호출
                        SetSamplingGridColumn(e.Cell.Row.Cells["PlantCode"].Value.ToString()
                                            , e.Cell.Row.Cells["ReqNo"].Value.ToString()
                                            , e.Cell.Row.Cells["ReqSeq"].Value.ToString());
                    }
                    else
                    {
                        WinMessageBox msg = new WinMessageBox();
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M000879", "M000182", "M000888"
                                                                , Infragistics.Win.HAlign.Center);
                        e.Cell.Value = e.Cell.OriginalValue;
                    }
                }

                // 헤더 검사결과 업데이트
                // AQL 비적용
                if (m_bolCheckAQL == false)
                {
                    int intCount = 0;
                    // 합격판정 컬럼의 값중 하나라도 불합격이 있으면 헤더에 합부판정 불합격으로 바꿔준다
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        // 불합격일경우 카운트 증가
                        if (this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount++;
                        }
                    }

                    // 하나라도 불량일경우 헤더 합부판정 불합격
                    if (intCount > 0)
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 1;
                    }
                    else
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }
                }
                // AQL 적용
                else
                {
                    int intCount = 0;
                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        if (this.uGridReqSampling.Rows[i].Cells["InspectResultFlag"].Value.ToString() == "NG")
                        {
                            intCount++;
                        }
                    }

                    // 불합격 검사항목수가 불합격 판정개수이상이면 헤더 합부판정 불합격으로
                    if (intCount >= m_intRejectCount)
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 1;
                    }
                    else
                    {
                        this.uOptionPassFailFlag.CheckedIndex = 0;
                    }
                }

                // 검사결과 그리드
                for (int i = 0; i < this.uGridReqItem.Rows.Count; i++)
                {
                    if (Convert.ToInt32(this.uGridReqItem.Rows[i].Cells["ReqItemSeq"].Value) == Convert.ToInt32(e.Cell.Row.Cells["ReqItemSeq"].Value))
                    {
                        // 합부판정
                        this.uGridReqItem.Rows[i].Cells["InspectResultFlag"].Value = e.Cell.Row.Cells["InspectResultFlag"].Value;
                        this.uGridReqItem.Rows[i].Cells["SampleSize"].Value = e.Cell.Row.Cells["SampleSize"].Value;
                        this.uGridReqItem.Rows[i].Cells["FaultQty"].Value = e.Cell.Row.Cells["FaultQty"].Value;
                        break;
                    }
                }

                this.uGridReqSampling.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        // 검사시간 스핀버튼 이벤트
        private void uDateInspectTime_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                    return;

                Infragistics.Win.UltraWinEditors.EditorButtonEventArgs ed = sender as Infragistics.Win.UltraWinEditors.EditorButtonEventArgs;
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    uDateInspectTime.Focus();
                    uDateInspectTime.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    uDateInspectTime.Focus();
                    uDateInspectTime.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 작성완료 체크시 수정불가 메세지 띄우기 --- 주석 윤경희 11.10
        //private void uCheckCompleteFlag_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (this.uCheckCompleteFlag.Checked)
        //        {
        //            if (this.uOptionICPCheckFlag.CheckedIndex.Equals(-1))
        //            {
        //                // SystemInfor ResourceSet
        //                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //                WinMessageBox msg = new WinMessageBox();

        //                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                                    "확인창", "검사결과 확인", "검사결과를 선택해 주세요",
        //                                    Infragistics.Win.HAlign.Right);
                        
        //                this.uCheckCompleteFlag.Checked = false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}

        private void frmINS0004_Resize(object sender, EventArgs e)
        {
            ////try
            ////{
            ////    if (this.Width > 1070)
            ////    {
            ////        uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
            ////    }
            ////    else
            ////    {
            ////        uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            ////    }

            ////}
            ////catch (System.Exception ex)
            ////{
            ////    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            ////    frmErr.ShowDialog();
            ////}
            ////finally
            ////{
            ////}
        }

        private void uGridReqSampling_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string rowError = string.Empty;
                string cellError = string.Empty;

                decimal dblFaultQty = Convert.ToDecimal(e.Row.Cells["FaultQty"].Value);
                string strInspectResultFlag = e.Row.Cells["InspectResultFlag"].Value.ToString();

                if (dblFaultQty > 0m)
                {
                    rowError = "Row Contains Error.";
                    cellError = "FaultQty";
                }
                else if (strInspectResultFlag.Equals("NG"))
                {
                    rowError = "Row Contains Error.";
                    //cellError = "InspectResultFlag";
                }

                DataRowView drv = (DataRowView)e.Row.ListObject;
                drv.Row.RowError = rowError;
                drv.Row.SetColumnError("FaultQty", cellError);

                if ((Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0) && Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value).Equals(0)) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                // 데이터 유형이 계량형일때 Xn 값 불량이면 폰트색 변경
                if (e.Row.Cells["DataType"].Value.ToString().Equals("1"))
                {
                    if (e.Row.Cells.Exists("1"))
                    {
                        //int intSampleSize = Convert.ToInt32(e.Row.Cells["SampleSize"].Value) * Convert.ToInt32(e.Row.Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(e.Row.Cells["Point"].Value);
                        int intSampleSize = Convert.ToInt32(e.Row.Cells["SampleSize"].Value);

                        if (intSampleSize > 99)
                            intSampleSize = 99;

                        for (int j = 1; j <= intSampleSize; j++)
                        {
                            if (e.Row.Cells.Exists(j.ToString()))
                            {
                                if (e.Row.Cells[j.ToString()].Value == null || e.Row.Cells[j.ToString()].Value == DBNull.Value)
                                    return;

                                if (Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value).Equals(0m))
                                {
                                    if (ReturnDecimalValue(e.Row.Cells[j.ToString()].Value.ToString()) < Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                                else
                                {
                                    if (ReturnDecimalValue(e.Row.Cells[j.ToString()].Value.ToString()) > ReturnDecimalValue(e.Row.Cells["UpperSpec"].Value.ToString()) ||
                                            ReturnDecimalValue(e.Row.Cells[j.ToString()].Value.ToString()) < ReturnDecimalValue(e.Row.Cells["LowerSpec"].Value.ToString()))
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        e.Row.Cells[j.ToString()].Appearance.ForeColor = Color.Black;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridReqItem_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (!e.Row.Cells["InspectResultFlag"].Value.ToString().Equals("NG"))
                {
                    e.Row.Appearance.BackColor = Color.Empty;
                }
                else
                {
                    e.Row.Appearance.BackColor = Color.Salmon;
                }

                if (!(Convert.ToDecimal(e.Row.Cells["UpperSpec"].Value) + Convert.ToDecimal(e.Row.Cells["LowerSpec"].Value) > 0) && e.Row.Cells["SpecRange"].Value.ToString().Equals(string.Empty))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Hidden = true;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Hidden = true;
                }

                if (Convert.ToInt32(e.Row.Cells["FaultQty"].Value) > 0)
                {
                    e.Row.Cells["FaultQty"].Appearance.ForeColor = Color.Red;
                }
                else
                {
                    e.Row.Cells["FaultQty"].Appearance.ForeColor = Color.Black;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridReqSampling_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = uGridReqSampling == null ? null : uGridReqSampling.ActiveCell;
                if (activeCell == null) return;

                int intRowIndex = uGridReqSampling.ActiveCell.Row.Index;

                //검사수 입력하고 엔터누르면 불량수 란으로 이동
                if (activeCell.Column.Key.Equals("SampleSize"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridReqSampling.Rows[activeCell.Row.Index].Cells["FaultQty"];
                        this.uGridReqSampling.ActiveCell = nextCell;
                        this.uGridReqSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //불량수 입력하면 X1으로 이동
                else if (activeCell.Column.Key.Equals("FaultQty"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridReqSampling.Rows[activeCell.Row.Index].Cells["1"];
                        this.uGridReqSampling.ActiveCell = nextCell;
                        this.uGridReqSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridReqSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridReqSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }

                //X1~Xn 셀 입력하면 옆란 이동 또는 아래란 이동.
                else if (activeCell.Column.Header.Caption.Contains("X"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        //엔터를 치면 다음 셀로 이동
                        int intNextCellKey = Convert.ToInt32(activeCell.Column.Key) + 1;
                        string strNextCellKey = intNextCellKey.ToString();

                        //다음 셀이 최대 SampleSize를 벗어나면 다음 행의 X1으로 이동처리
                        //int intSampleSize = (Convert.ToInt32(uGridReqSampling.Rows[intRowIndex].Cells["SampleSize"].Value) * 
                        //                    Convert.ToInt32(uGridReqSampling.Rows[intRowIndex].Cells["GrandSampleSize"].Value) *
                        //                    Convert.ToInt32(uGridReqSampling.Rows[intRowIndex].Cells["Point"].Value));
                        int intSampleSize = Convert.ToInt32(uGridReqSampling.Rows[intRowIndex].Cells["SampleSize"].Value);
                        if (intSampleSize > 99)
                            intSampleSize = 99;
                        if (intNextCellKey > intSampleSize)
                        {
                            intRowIndex = intRowIndex + 1;
                            //다음 행이 마지막 행을 넘어가면 첫번째 행으로 이동
                            if (intRowIndex > this.uGridReqSampling.Rows.Count - 1)
                                intRowIndex = 0;
                            intNextCellKey = 1;
                            if (this.uGridReqSampling.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))
                                strNextCellKey = "1";
                            else
                                strNextCellKey = "SampleSize";
                        }

                        //Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridReqSampling.Rows[intRowIndex].Cells[intNextCellKey.ToString()];
                        Infragistics.Win.UltraWinGrid.UltraGridCell nextCell = this.uGridReqSampling.Rows[intRowIndex].Cells[strNextCellKey];

                        //다음 셀이 편집불가면 다음 행 검사수로 이동처리
                        if (nextCell.Activation == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        {
                            ////if (this.uGridReqSampling.ActiveCell.Row.Index < this.uGridReqSampling.Rows.Count - 1)
                            ////    nextCell = this.uGridReqSampling.Rows[intRowIndex + 1].Cells["SampleSize"];
                            ////else
                            ////    nextCell = this.uGridReqSampling.Rows[0].Cells["SampleSize"];

                            if (this.uGridReqSampling.Rows[intRowIndex].Cells["DataType"].Value.ToString().Equals("1"))        // 계량형인경우 X1컬럼으로 이동
                            {
                                if (this.uGridReqSampling.ActiveCell.Row.Index < this.uGridReqSampling.Rows.Count - 1)
                                    nextCell = this.uGridReqSampling.Rows[intRowIndex + 1].Cells["1"];
                                else
                                    nextCell = this.uGridReqSampling.Rows[0].Cells["1"];
                            }
                            else
                            {
                                if (this.uGridReqSampling.ActiveCell.Row.Index < this.uGridReqSampling.Rows.Count - 1)
                                    nextCell = this.uGridReqSampling.Rows[intRowIndex + 1].Cells["SampleSize"];
                                else
                                    nextCell = this.uGridReqSampling.Rows[0].Cells["SampleSize"];
                            }
                        }

                        //다음 셀을 지정하고 DropDown셀인 경우 DropDown이 펼치도록 처리
                        this.uGridReqSampling.ActiveCell = nextCell;
                        if (nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                            || nextCell.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList)
                            this.uGridReqSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        else
                            this.uGridReqSampling.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridInspectList_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["WMSTFlag"].Value.ToString().Equals("F")
                    && e.Row.Cells["CompleteFlag"].Value.ToString().Equals("T")
                    && e.Row.Cells["PassFailFlag"].Value.ToString().Equals("OK"))
                {
                    e.Row.Appearance.BackColor = Color.Salmon;
                }
                else
                {
                    e.Row.Appearance.BackColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uGridLot_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("COCFilePath"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strFileName = openFile.FileName;
                        if (strFileName.Contains("#"))
                        {
                            //SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001150", "M001142",
                                                Infragistics.Win.HAlign.Right);
                            return;
                        }
                        else
                        {
                            e.Cell.Value = strFileName;
                        }
                    }
                }
                else if (e.Cell.Column.Key.Equals("COCFile"))
                {
                    if (!this.uTextReqNo.Text.Equals(string.Empty))
                    {
                        String strFullReqNo = this.uTextReqNo.Text;
                        String strReqNo = string.Empty;
                        String strReqSeq = string.Empty;
                        if (strFullReqNo.Length > 9)
                        {
                            strReqNo = strFullReqNo.Substring(0, 7);
                            strReqSeq = strFullReqNo.Substring(7, 4);
                        }

                        frmINS0004P1 frmPOP = new frmINS0004P1();
                        frmPOP.PlantCode = PlantCode;
                        frmPOP.ReqNo = strReqNo;
                        frmPOP.ReqSeq = strReqSeq;
                        frmPOP.ReqLotSeq = Convert.ToInt32(e.Cell.Row.Cells["ReqLotSeq"].Value);
                        frmPOP.CompleteFlag = this.uCheckCompleteFlag.Checked;

                        frmPOP.ShowDialog();

                        e.Cell.Value = frmPOP.COCFileCount.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 첨부파일 업로드 메소드
        /// </summary>
        private void FileUpload(string strReqNo, string strReqSeq)
        {
            try
            {
                //File Upload
                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();

                for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                {
                    if (this.uGridLot.Rows[i].Cells["COCFilePath"].Value.ToString().Contains(":\\"))
                    {
                        System.IO.FileInfo fileDoc = new System.IO.FileInfo(this.uGridLot.Rows[i].Cells["COCFilePath"].Value.ToString());
                        string strUploadFile = fileDoc.DirectoryName + "\\" + PlantCode + "-"
                                                                            + strReqNo + "-"
                                                                            + strReqSeq + "-"
                                                                            + this.uGridLot.Rows[i].RowSelectorNumber.ToString() + "-"
                                                                            + fileDoc.Name;

                        if (System.IO.File.Exists(strUploadFile))
                            System.IO.File.Delete(strUploadFile);

                        System.IO.File.Copy(this.uGridLot.Rows[i].Cells["COCFilePath"].Value.ToString(), strUploadFile);
                        arrFile.Add(strUploadFile);
                    }
                }

                if (arrFile.Count > 0)
                {
                    //화일서버 연결정보 가져오기
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0025");

                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();

                    for (int i = 0; i < arrFile.Count; i++)
                    {
                        System.IO.File.Delete(arrFile[i].ToString());
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridLot_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("COCFilePath"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty) && !e.Cell.Value.ToString().Contains(":\\"))
                    {
                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";

                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            //화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                            //첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0025");

                            //첨부파일 Download하기
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                            arrFile.Add(e.Cell.Value.ToString());

                            ////////Download정보 설정
                            //////string strExePath = Application.ExecutablePath;
                            //////int intPos = strExePath.LastIndexOf(@"\");
                            //////strExePath = strExePath.Substring(0, intPos + 1);

                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder,
                                                                   dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + "\\" + e.Cell.Value.ToString());
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 첨부파일 
        private void uGridLot_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uGridLot.ActiveCell == null)
                    return;
                else if (this.uGridLot.ActiveCell.Column.Key.Equals("COCFilePath"))
                {
                    if (this.uCheckCompleteFlag.Enabled)
                    {
                        if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                        {
                            if (!this.uGridLot.ActiveCell.Value.ToString().Contains(":\\") && !this.uGridLot.ActiveCell.Value.ToString().Equals(string.Empty))
                            {
                                //화일서버 연결정보 가져오기
                                QRPBrowser brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                                brwChannel.mfCredentials(clsSysAccess);
                                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                                //첨부파일 저장경로정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                                brwChannel.mfCredentials(clsSysFilePath);
                                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(PlantCode, "D0025");

                                //첨부파일 Download하기
                                frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                System.Collections.ArrayList arrFile = new System.Collections.ArrayList();
                                //arrFile.Add(dtFilePath.Rows[0]["ServerPath"].ToString() + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridLot.ActiveCell.Value.ToString());
                                arrFile.Add(dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + this.uGridLot.ActiveCell.Value.ToString());

                                fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrFile
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                fileAtt.mfFileUploadNoProgView();

                                this.uGridLot.ActiveCell.Value = string.Empty;
                                this.uGridLot.ActiveCell.SetValue(string.Empty, false);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 원자재 이상발생 이동 버튼 클릭 이벤트
        private void uButtonMoveAbnormal_Click(object sender, EventArgs e)
        {
            try
            {
                // 원자재 이상발생 관리 화면을 띄운다
                frmINS0017 frmINS = new frmINS0017();
                
                string strReqNo = this.uTextReqNo.Text.Substring(0, 7);
                string strReqSeq = this.uTextReqNo.Text.Substring(7, 4);

                // 변수전달
                frmINS.PlantCode = PlantCode;
                frmINS.ReqNo = strReqNo;
                frmINS.ReqSeq = strReqSeq;
                frmINS.MoveFormName = this.Name;

                CommonControl cControl = new CommonControl();
                Control ctrl = cControl.mfFindControlRecursive(this.MdiParent, "uTabMenu");
                Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu = (Infragistics.Win.UltraWinTabControl.UltraTabControl)ctrl;
                //해당화면의 탭이 있는경우 해당 탭 닫기
                if (uTabMenu.Tabs.Exists("QRPINS" + "," + "frmINS0017"))
                {

                    uTabMenu.Tabs["QRPINS" + "," + "frmINS0017"].Close();

                }
                uTabMenu.Tabs.Add("QRPINS" + "," + "frmINS0017", "원자재 이상발생관리");
                uTabMenu.SelectedTab = uTabMenu.Tabs["QRPINS,frmINS0017"];

                //호출시 화면 속성 설정
                frmINS.AutoScroll = true;
                frmINS.MdiParent = this.MdiParent;
                frmINS.ControlBox = false;
                frmINS.Dock = DockStyle.Fill;
                frmINS.FormBorderStyle = FormBorderStyle.None;
                frmINS.WindowState = FormWindowState.Normal;
                frmINS.Text = "원자재 이상발생관리";
                frmINS.Show();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
        }


        #region Excel Upload

        private void ImportExcelData_InGrid(Infragistics.Win.UltraWinGrid.UltraGrid uGrid, DataTable dtExcelData_Origin)
        {
            try
            {                
                ((DataTable)uGrid.DataSource).AcceptChanges();
                DataTable dtGridData = (DataTable)uGrid.DataSource;
                dtExcelData_Origin.Columns.Add("t_Seq", typeof(Int32));
                DataTable dtExcelCol = dtExcelData_Origin.DefaultView.ToTable(true, "REPORT");

                // 컬럼명 변경
                ////dtExcelData.Columns["OK/NG"].ColumnName = "InspectResultFlag";
                ////dtExcelData.Columns["REPORT"].ColumnName = "InspectItemName";
                ////dtExcelData.Columns["NOMINAL"].ColumnName = "NOMINAL";
                ////////dtExcelData.Columns["TOL+"].ColumnName = "UpperSpec";
                ////////dtExcelData.Columns["TOL-"].ColumnName = "LowerSpec";
                ////dtExcelData.Columns["MIN"].ColumnName = "MinValue";
                ////dtExcelData.Columns["MAX"].ColumnName = "MaxValue";
                ////dtExcelData.Columns["RANGE"].ColumnName = "DataRange";
                ////dtExcelData.Columns["AVG"].ColumnName = "Mean";
                ////dtExcelData.Columns["STDEV"].ColumnName = "StdDev";
                ////dtExcelData.Columns["CP"].ColumnName = "Cp";
                ////dtExcelData.Columns["CPK"].ColumnName = "Cpk";

                ////dtExcelData.Columns.Remove("LABEL");
                ////dtExcelData.Columns.Remove("NOMINAL");

                DataTable dtSeq = new DataTable();
                dtSeq.Columns.Add("t_Seq", typeof(Int32));
                dtSeq.Columns.Add("REPORT", typeof(string));
                dtSeq.Columns.Add("X1", typeof(decimal));

                // 검사항목별 순번지정
                DataRow[] _drs;
                for (int i = 0; i < dtExcelCol.Rows.Count; i++)
                {
                    _drs = dtExcelData_Origin.Select("REPORT = '" + dtExcelCol.Rows[i]["REPORT"].ToString() + "'");
                    int int_Seq = 1;
                    foreach (DataRow _dr in _drs)
                    {
                        _dr["t_Seq"] = int_Seq;
                        dtSeq.ImportRow(_dr);
                        int_Seq++;
                    }
                }

                // 컬럼명 변경
                dtSeq.Columns["REPORT"].ColumnName = "InspectItemName";
                dtSeq.Columns["X1"].ColumnName = "Value";


                // 순번의 최대값 찾기
                Int32 intMaxSeq = Convert.ToInt32(dtSeq.Compute("MAX(t_Seq)", string.Empty));
                // X1~Xn 형태의 테이블 생성
                DataTable dtNewExcelData = new DataTable();
                dtNewExcelData.Columns.Add("InspectItemName", typeof(string));
                dtNewExcelData.Columns.Add("SampleSize", typeof(Int32));
                for (int i = 1; i <= intMaxSeq; i++)
                {
                    dtNewExcelData.Columns.Add(i.ToString(), typeof(decimal));
                    dtNewExcelData.Columns[i].DefaultValue = 0.0;
                }

                DataRow drInspetItem;
                for (int i = 0; i < dtExcelCol.Rows.Count; i++)
                {
                    drInspetItem = dtNewExcelData.NewRow();

                    _drs = dtSeq.Select("InspectItemName = '" + dtExcelCol.Rows[i]["REPORT"].ToString() + "'");

                    drInspetItem["InspectItemName"] = dtExcelCol.Rows[i]["REPORT"].ToString();
                    drInspetItem["SampleSize"] = _drs.Max(r => (int) r["t_Seq"]);
                    for (int j = 0; j < _drs.Length; j++)
                    {
                        drInspetItem[(j + 1).ToString()] = _drs[j]["Value"];
                    }
                    //dtNewExcelData.ImportRow(drInspetItem);
                    dtNewExcelData.Rows.Add(drInspetItem);
                }

                ////for (int i = 0; i < dtExcelData_Origin.Columns.Count; i++)
                ////{
                ////    if (dtExcelData_Origin.Columns.Contains("X" + i.ToString()))
                ////        dtExcelData_Origin.Columns["X" + i.ToString()].ColumnName = i.ToString();
                ////}

                for (int i = 0; i < dtGridData.Rows.Count; i++)
                {
                    if (dtGridData.Rows[i]["DataType"].ToString().Equals("1"))
                    {
                        for (int j = 0; j < dtNewExcelData.Rows.Count; j++)
                        {
                            if (dtGridData.Rows[i]["InspectItemName"].ToString() == dtNewExcelData.Rows[j]["InspectItemName"].ToString())
                            {
                                for (int a = 0; a < dtNewExcelData.Columns.Count; a++)
                                {
                                    // 컬럼명이 숫자인지 판단
                                    bool bolCheck = true;
                                    for (int b = 0; dtNewExcelData.Columns[a].ColumnName.Length > b; b++)
                                    {
                                        if (!char.IsDigit(dtNewExcelData.Columns[a].ColumnName, b))
                                        {
                                            bolCheck = false;
                                            break;
                                        }
                                    }

                                    if (bolCheck)
                                    {
                                        if (!dtGridData.Columns.Contains(dtNewExcelData.Columns[a].ColumnName))
                                        {
                                            dtGridData.Columns.Add(dtNewExcelData.Columns[a].ColumnName, typeof(string));
                                            dtGridData.Columns[dtNewExcelData.Columns[a].ColumnName].DefaultValue = string.Empty;
                                        }
                                    }

                                    if (dtGridData.Columns.Contains(dtNewExcelData.Columns[a].ColumnName))
                                    {
                                        dtGridData.Rows[i][dtNewExcelData.Columns[a].ColumnName] = dtNewExcelData.Rows[j][dtNewExcelData.Columns[a].ColumnName];
                                    }
                                }
                                //////// 현재행 삭제
                                //////dtExcelData.Rows[j].Delete();
                                //////j--;
                                break;
                            }
                        }
                    }
                }

                uGrid.DataSource = dtGridData;
                uGrid.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        ///    Excel 파일의 형태를 반환한다.
        ///    -2 : Error  
        ///    -1 : 엑셀파일아님
        ///     0 : 97-2003 엑셀 파일 (xls)
        ///     1 : 2007 이상 파일 (xlsx)
        /// </summary>
        /// <param name="XlsFile">Excel File 명 전체 경로</param>
        public int ExcelFileType(string XlsFile)
        {
            byte[,] ExcelHeader = {
                { 0xD0, 0xCF, 0x11, 0xE0, 0xA1 }, // XLS  File Header
                { 0x50, 0x4B, 0x03, 0x04, 0x14 }  // XLSX File Header
            };

            // result -2=error, -1=not excel , 0=xls , 1=xlsx
            int result = -1;

            // 
            System.IO.FileStream FS = new System.IO.FileStream(XlsFile, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);

            try
            {
                byte[] FH = new byte[5];

                FS.Read(FH, 0, 5);

                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (FH[j] != ExcelHeader[i, j]) break;
                        else if (j == 4) result = i;
                    }
                    if (result >= 0) break;
                }
            }
            catch
            {
                result = (-2);
            }
            finally
            {
                FS.Close();
            }
            return result;
        }

        /// <summary>
        ///    Excel 파일을 DataSet 으로 변환하여 반환
        /// </summary>
        /// <param name="FileName">Excel File 명 PullPath</param>
        /// <param name="UseHeader">첫번째 줄을 Field 명으로 사용할 것이지 여부</param>
        private DataSet OpenExcel_DataSet(string FileName, bool UseHeader, string strSheetName)
        {
            DataSet DS = null;

            // 확장명 XLS (Excel 97~2003 용)
            string ConnectStrFrm_Excel97_2003 =
                "Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=\"{0}\";" +
                "Mode=ReadWrite|Share Deny None;" +
                "Extended Properties='Excel 8.0; HDR={1}; IMEX={2}';" +
                "Persist Security Info=False";

            // 확장명 XLSX (Excel 2007 이상용)
            string ConnectStrFrm_Excel =
                "Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=\"{0}\";" +
                "Mode=ReadWrite|Share Deny None;" +
                "Extended Properties='Excel 12.0; HDR={1}; IMEX={2}';" +
                "Persist Security Info=False";

            string[] HDROpt = { "NO", "YES" };
            string HDR = "";
            string ConnStr = "";

            if (UseHeader)
                HDR = HDROpt[1];
            else
                HDR = HDROpt[0];

            int ExcelType = ExcelFileType(FileName);

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            switch (ExcelType)
            {
                case (-2): throw new Exception(FileName + msg.GetMessge_Text("M001538", m_resSys.GetString("SYS_LANG"))); // "의 형식검사중 오류가 발생하였습니다."
                case (-1): throw new Exception(FileName + msg.GetMessge_Text("M001539", m_resSys.GetString("SYS_LANG"))); // "은 엑셀 파일형식이 아닙니다."
                case (0):
                    ConnStr = string.Format(ConnectStrFrm_Excel97_2003, FileName, HDR, "1");
                    break;
                case (1):
                    ConnStr = string.Format(ConnectStrFrm_Excel, FileName, HDR, "1");
                    break;
            }

            System.Data.OleDb.OleDbConnection OleDBConn = null;
            System.Data.OleDb.OleDbDataAdapter OleDBAdap = null;
            DataTable Schema;

            try
            {

                // 엑셀 Sheet 별로 데이터셋으로 받아오기
                OleDBConn = new System.Data.OleDb.OleDbConnection(ConnStr);
                OleDBConn.Open();

                Schema = OleDBConn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                DS = new DataSet();

                foreach (DataRow DR in Schema.Rows)
                {
                    OleDBAdap = new System.Data.OleDb.OleDbDataAdapter(DR["TABLE_NAME"].ToString(), OleDBConn);

                    OleDBAdap.SelectCommand.CommandType = CommandType.TableDirect;
                    OleDBAdap.AcceptChangesDuringFill = false;

                    string TableName = DR["TABLE_NAME"].ToString().Replace("$", String.Empty).Replace("'", String.Empty);

                    if (DR["TABLE_NAME"].ToString().Contains("$")) OleDBAdap.Fill(DS, TableName);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                if (OleDBConn != null)
                    OleDBConn.Close();
            }
            return DS;
        }

        /// <summary>
        ///    Excel 파일을 DataSet 으로 변환하여 반환
        /// </summary>
        /// <param name="FileName">Excel File 명 PullPath</param>
        /// <param name="UseHeader">첫번째 줄을 Field 명으로 사용할 것이지 여부</param>
        private DataTable OpenExcel_DataTable(string FileName, bool UseHeader, string strSheetName)
        {
            // 확장명 XLS (Excel 97~2003 용)
            string ConnectStrFrm_Excel97_2003 =
                "Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=\"{0}\";" +
                "Mode=ReadWrite|Share Deny None;" +
                "Extended Properties='Excel 8.0; HDR={1}; IMEX={2}';" +
                "Persist Security Info=False";

            // 확장명 XLSX (Excel 2007 이상용)
            string ConnectStrFrm_Excel =
                "Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=\"{0}\";" +
                "Mode=ReadWrite|Share Deny None;" +
                "Extended Properties='Excel 12.0; HDR={1}; IMEX={2}';" +
                "Persist Security Info=False";

            string[] HDROpt = { "NO", "YES" };
            string HDR = "";
            string ConnStr = "";

            if (UseHeader)
                HDR = HDROpt[1];
            else
                HDR = HDROpt[0];

            int ExcelType = ExcelFileType(FileName);

            switch (ExcelType)
            {
                case (-2): throw new Exception(FileName + "의 형식검사중 오류가 발생하였습니다.");
                case (-1): throw new Exception(FileName + "은 엑셀 파일형식이 아닙니다.");
                case (0):
                    ConnStr = string.Format(ConnectStrFrm_Excel97_2003, FileName, HDR, "1");
                    break;
                case (1):
                    ConnStr = string.Format(ConnectStrFrm_Excel, FileName, HDR, "1");
                    break;
            }

            System.Data.OleDb.OleDbConnection excelConnection = null;
            System.Data.OleDb.OleDbCommand dbCommand = null;
            System.Data.OleDb.OleDbDataAdapter dataAdapter = null;

            DataTable dtExcelData = new DataTable();

            try
            {

                excelConnection = new System.Data.OleDb.OleDbConnection(ConnStr);
                excelConnection.Open();

                string strSQL = "select * from [Sheet2$]";
                dbCommand = new System.Data.OleDb.OleDbCommand(strSQL, excelConnection);
                dataAdapter = new System.Data.OleDb.OleDbDataAdapter(dbCommand);

                dataAdapter.Fill(dtExcelData);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                dataAdapter.Dispose();
                dbCommand.Dispose();

                if (excelConnection != null)
                    excelConnection.Close();

                excelConnection.Dispose();
            }

            return dtExcelData;
        }

        #endregion

        private void uButtonExcelUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCompleteFlag.Checked && !this.uCheckCompleteFlag.Enabled)
                    return;

                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "All Excel Files (*.xls, *.xlsx)|*.xls;*.xlsx|Excel FIles(97-2003) (*.xls)|*.xls|Excel FIles (*.xlsx)|*.xlsx|All Files|*.*";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strFileName = openFile.FileName;

                    openFile.Dispose();

                    DataTable dtExcelData = OpenExcel_DataTable(strFileName, true, "Sheet2");

                    this.uGridReqSampling.EventManager.AllEventsEnabled = false;
                    ImportExcelData_InGrid(this.uGridReqSampling, dtExcelData);
                    this.uGridReqSampling.EventManager.AllEventsEnabled = true;


                    // 컬럼생성 메소드 호출
                    String[] strLastColKey = { "Mean", "SpecRange" };
                    CreateColumn(this.uGridReqSampling, 0, "SampleSize", "GrandSampleSize", strLastColKey);

                    for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                    {
                        if (this.uGridReqSampling.Rows[i].Cells["DataType"].Value.ToString().Equals("1"))
                        {
                            for (int j = 0; j < this.uGridReqSampling.DisplayLayout.Bands[0].Columns.Count; j++)
                            {
                                if (this.uGridReqSampling.Rows[i].Band.Columns[j].Header.Caption.Contains("X") && !this.uGridReqSampling.Rows[i].Band.Columns[j].Header.Column.Key.Equals("MaxValue"))
                                {
                                    ////if (this.uGridReqSampling.Rows[i].Cells[this.uGridReqSampling.Rows[i].Band.Columns[j].Header.Column.Key.ToString()].Value != null || 
                                    ////    !this.uGridReqSampling.Rows[i].Cells[this.uGridReqSampling.Rows[i].Band.Columns[j].Header.Column.Key.ToString()].Value.Equals(DBNull.Value))
                                    if(this.uGridReqSampling.Rows[i].Cells[this.uGridReqSampling.Rows[i].Band.Columns[j].Header.Column.Key.ToString()].Value != null)
                                    {
                                        Infragistics.Win.UltraWinGrid.CellEventArgs evt = new Infragistics.Win.UltraWinGrid.CellEventArgs(this.uGridReqSampling.Rows[i].Cells[this.uGridReqSampling.Rows[i].Band.Columns[j].Header.Column.Key.ToString()]);
                                        uGridReqSampling_AfterCellUpdate(uGridReqSampling, evt);
                                    }
                                }
                            }

                            Infragistics.Win.UltraWinGrid.InitializeRowEventArgs evtRow = new Infragistics.Win.UltraWinGrid.InitializeRowEventArgs(this.uGridReqSampling.Rows[i], false);
                            uGridReqSampling_InitializeRow(this.uGridReqSampling, evtRow);
                        }   
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }

        private void uOptionPassFailFlag_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.uOptionPassFailFlag.HandleDestroyed += new EventHandler(uOptionPassFailFlag_ValueChanged);

                if (this.uOptionPassFailFlag.CheckedIndex.Equals(-1))
                    return;
                else if (!(this.uGridReqSampling.Rows.Count>0))
                    return;

                if (this.uCheckCompleteFlag.Enabled)
                {
                    int intFaultrowCount = 0;

                    if (this.uOptionPassFailFlag.CheckedItem.DataValue.Equals("NG"))
                    {
                        for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                        {
                            if(Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["FaultQty"].Value)>0)
                                intFaultrowCount += 1;
                        }

                        if (intFaultrowCount.Equals(0))
                        {
                            //SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000611"
                                                , "M001356",
                                                Infragistics.Win.HAlign.Right);

                            this.uOptionPassFailFlag.CheckedIndex = 0;
                        }
                    }
                    else if (this.uOptionPassFailFlag.CheckedItem.DataValue.Equals("OK"))
                    {
                        for (int i = 0; i < this.uGridReqSampling.Rows.Count; i++)
                        {
                            if (Convert.ToInt32(this.uGridReqSampling.Rows[i].Cells["FaultQty"].Value) > 0)
                                intFaultrowCount += 1;
                        }

                        if (!intFaultrowCount.Equals(0))
                        {
                            //SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            WinMessageBox msg = new WinMessageBox();

                            if (this.uOptionPassFailFlag.Value.ToString() != "NG")
                            {
                                this.uOptionPassFailFlag.Value = "NG";

                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000611"
                                                    , "M001357",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                this.uOptionPassFailFlag.HandleCreated += new EventHandler(uOptionPassFailFlag_ValueChanged);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사일 변경시 승인일을 검사일과 동일하게 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uDateInspectDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uDateInspectDate.Value.Equals(null) || this.uDateInspectDate.Value.ToString().Equals(string.Empty))
                {
                    this.uDateAdmitDate.Value = this.uDateInspectDate.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo();
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextSamplingLot_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                {
                    e.Handled = true;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
