﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

//Using Add
using System.Data;

namespace QRPINS.UI
{
    /// <summary>
    /// Summary description for rptINSZ0002_S02.
    /// </summary>
    public partial class rptINSZ0002_S02 : DataDynamics.ActiveReports.ActiveReport
    {

        public rptINSZ0002_S02()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            
        }
        public rptINSZ0002_S02(DataTable dtData,bool bolTitle)
        {
            //DataTable dtCopy = new DataTable();
            try
            {
                //
                // Required for Windows Form Designer support
                //
                string[] strTop = new string[9];
                if (dtData.Rows.Count > 0)
                {
                    //dtCopy = dtData.Copy();
                    // Title 설정
                    foreach (DataColumn dc in dtData.Columns)
                    {

                        if (dc.ColumnName.Contains("Title"))
                        {
                            strTop[0] = dtData.Rows[0]["Title"].ToString();
                            continue;
                        }

                        int intIndex = Convert.ToInt32(dc.ColumnName.Replace('#', ' ').Trim());
                        strTop[intIndex] = dtData.Rows[0][dc.ColumnName].ToString();
                        continue;

                    }
                    DataRow drRow = dtData.Rows[0];
                    dtData.Rows.Remove(drRow);

                    this.DataSource = dtData;

                    InitializeComponent();

                    this.txtTopTitle.Text = strTop[0];
                    this.txtTopX1.Text = strTop[1];
                    this.txtTopX2.Text = strTop[2];
                    this.txtTopX3.Text = strTop[3];
                    this.txtTopX4.Text = strTop[4];
                    this.txtTopX5.Text = strTop[5];
                    this.txtTopX6.Text = strTop[6];
                    this.txtTopX7.Text = strTop[7];
                    this.txtTopX8.Text = strTop[8];
                }
                else
                {
                    this.DataSource = dtData;

                    InitializeComponent();
                }

                this.lbllTitle.Visible = bolTitle;
            }
            catch (Exception ex)
            {
            }

        }

    }
}
