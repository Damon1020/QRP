﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질검사관리                                          */
/* 모듈(분류)명 : OCAP관리                                              */
/* 프로그램ID   : frmQATZ0014.cs                                        */
/* 프로그램명   : OCAP관리                                              */
/* 작성자       : 윤경희                                                */
/* 작성일자     : 2011-09-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPINS.UI
{
    public partial class frmINSZ0014 : Form, IToolbar
    {
        // Resource 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmINSZ0014()
        {
            InitializeComponent();
        }

        private void frmINSZ0014_Activated(object sender, EventArgs e)
        {
            //툴바설정
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmINSZ0014_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("OCAP List", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitEtc();
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquip, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchReasonDateTIme, "발생일", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchArea
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // Process Combo
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridOCAP, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "OCAPNo", "OCAPNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "OCAPType", "OCAPType", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "CustomerCode", "고객코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "CustomerName", "고객", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "ReasonProcessCode", "발생공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "ReasonProcessName", "발생공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "ReasonDateTime", "발생일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd hh:mm:ss", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "WorkUserID", "작업자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "WorkUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "ActionProcessCode", "조치공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "ActionProcessName", "조치공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "ActionDesc", "조치내용", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "PassFailFlag", "검사결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "InspectUserID", "검사자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridOCAP, 0, "InspectUserName", "작업자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridOCAP.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridOCAP.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;               
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기타 컨트롤 초기화
        /// </summary>
        private void InitEtc()
        {
            try
            {
                this.uDateSearchFromReasonDateTIme.Value = DateTime.Now.ToString("yyyy-MM-dd");
                this.uDateSearchToReasonDateTIme.Value = DateTime.Now.ToString("yyyy-MM-dd");

                this.uLabelSearchEquip.Visible = false;
                this.uTextSearchEquipCode.Visible = false;
                this.uTextSearchEquipName.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        // 조회
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uDateSearchFromReasonDateTIme.Value == null || this.uDateSearchFromReasonDateTIme.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000217", "M000835"
                        , Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromReasonDateTIme.DropDown();
                    return;
                }
                else if (this.uDateSearchToReasonDateTIme.Value == null || this.uDateSearchToReasonDateTIme.Value == DBNull.Value)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000217", "M000836"
                        , Infragistics.Win.HAlign.Right);

                    this.uDateSearchToReasonDateTIme.DropDown();
                    return;
                }
                else if (Convert.ToDateTime(this.uDateSearchFromReasonDateTIme.Value).CompareTo(Convert.ToDateTime(this.uDateSearchToReasonDateTIme.Value)) > 0)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000217", "M000434"
                        , Infragistics.Win.HAlign.Right);

                    this.uDateSearchFromReasonDateTIme.DropDown();
                    return;
                }
                else
                {
                    // 매개변수 설정
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strProcessCode = this.uComboSearchProcess.Value.ToString();
                    string strEquipCode = this.uTextSearchEquipCode.Text;
                    string strLotNo = this.uTextSearchLotNo.Text;
                    string strFromReasonDate = Convert.ToDateTime(this.uDateSearchFromReasonDateTIme.Value).ToString("yyyy-MM-dd");
                    string strToReasonDate = Convert.ToDateTime(this.uDateSearchToReasonDateTIme.Value).ToString("yyyy-MM-dd");

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.OCAP), "OCAP");
                    QRPINS.BL.INSSTS.OCAP clsOCAP = new QRPINS.BL.INSSTS.OCAP();
                    brwChannel.mfCredentials(clsOCAP);

                    // 프로그래스바 생성
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 메소드 호출
                    DataTable dtSearch = clsOCAP.mfReadINSProcOCAP(strPlantCode, strProcessCode, strEquipCode, strLotNo, strFromReasonDate, strToReasonDate, m_resSys.GetString("SYS_LANG"));

                    this.uGridOCAP.DataSource = dtSearch;
                    this.uGridOCAP.DataBind();

                    // POPUP창 Close
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 조회결과 없을시 MessageBox 로 알림
                    if (dtSearch.Rows.Count == 0)
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    else
                    {
                        WinGrid grd = new WinGrid();
                        grd.mfSetAutoResizeColWidth(this.uGridOCAP, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        // 엑셀
        public void mfExcel()
        {
            try
            {
                if (this.uGridOCAP.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridOCAP);
                }
                else
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000332",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 1.검색조건 Event, Method
        // 설비 팝업창

        private void uTextSearchEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0005 frmPOP = new frmPOP0005();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchEquipCode.Text = frmPOP.EquipCode;
                this.uTextSearchEquipName.Text = frmPOP.EquipName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 : 공정정보 Load (공장 선택 시 )
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                String strPlantCode = uComboSearchPlant.Value.ToString();
                DataTable dtProcess = new DataTable();

                // ProcessComboBox Clear
                this.uComboSearchProcess.Items.Clear();

                if (strPlantCode != "")
                {
                    // ProcessComboBox
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }
                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                        , "ProcessCode", "ProcessName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion        

    }
}
    