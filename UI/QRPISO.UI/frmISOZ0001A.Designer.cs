﻿namespace QRPISO.UI
{
    partial class frmISOZ0001A
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISOZ0001A));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchDistributeRequestToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchDistributeRequestFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDistributeRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDistributeCompanyName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchDistributeCompanyID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDistributeCompany = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchStandardNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStandardNum = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uDateDistributeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDistributeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDept = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDistributeRequestUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDistributeRequestUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDistributeRequestUser = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDistributeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserID)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchDistributeRequestToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchDistributeRequestFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDistributeRequestDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDistributeCompanyName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDistributeCompanyID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDistributeCompany);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uDateSearchDistributeRequestToDate
            // 
            this.uDateSearchDistributeRequestToDate.Location = new System.Drawing.Point(604, 36);
            this.uDateSearchDistributeRequestToDate.Name = "uDateSearchDistributeRequestToDate";
            this.uDateSearchDistributeRequestToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchDistributeRequestToDate.TabIndex = 24;
            // 
            // ultraLabel2
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance2;
            this.ultraLabel2.Location = new System.Drawing.Point(588, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel2.TabIndex = 23;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchDistributeRequestFromDate
            // 
            this.uDateSearchDistributeRequestFromDate.Location = new System.Drawing.Point(484, 36);
            this.uDateSearchDistributeRequestFromDate.Name = "uDateSearchDistributeRequestFromDate";
            this.uDateSearchDistributeRequestFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchDistributeRequestFromDate.TabIndex = 22;
            // 
            // uLabelSearchDistributeRequestDate
            // 
            this.uLabelSearchDistributeRequestDate.Location = new System.Drawing.Point(380, 36);
            this.uLabelSearchDistributeRequestDate.Name = "uLabelSearchDistributeRequestDate";
            this.uLabelSearchDistributeRequestDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDistributeRequestDate.TabIndex = 21;
            this.uLabelSearchDistributeRequestDate.Text = "ultraLabel6";
            // 
            // uTextSearchDistributeCompanyName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDistributeCompanyName.Appearance = appearance4;
            this.uTextSearchDistributeCompanyName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDistributeCompanyName.Location = new System.Drawing.Point(218, 36);
            this.uTextSearchDistributeCompanyName.Name = "uTextSearchDistributeCompanyName";
            this.uTextSearchDistributeCompanyName.ReadOnly = true;
            this.uTextSearchDistributeCompanyName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDistributeCompanyName.TabIndex = 20;
            // 
            // uTextSearchDistributeCompanyID
            // 
            appearance3.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchDistributeCompanyID.ButtonsRight.Add(editorButton1);
            this.uTextSearchDistributeCompanyID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchDistributeCompanyID.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchDistributeCompanyID.Name = "uTextSearchDistributeCompanyID";
            this.uTextSearchDistributeCompanyID.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDistributeCompanyID.TabIndex = 19;
            this.uTextSearchDistributeCompanyID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchDistributeCompanyID_KeyDown);
            this.uTextSearchDistributeCompanyID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchDistributeCompanyID_EditorButtonClick);
            // 
            // uLabelSearchDistributeCompany
            // 
            this.uLabelSearchDistributeCompany.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchDistributeCompany.Name = "uLabelSearchDistributeCompany";
            this.uLabelSearchDistributeCompany.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDistributeCompany.TabIndex = 18;
            this.uLabelSearchDistributeCompany.Text = "ultraLabel1";
            // 
            // uTextSearchTitle
            // 
            this.uTextSearchTitle.Location = new System.Drawing.Point(643, 12);
            this.uTextSearchTitle.Name = "uTextSearchTitle";
            this.uTextSearchTitle.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchTitle.TabIndex = 17;
            // 
            // uTextSearchStandardNum
            // 
            this.uTextSearchStandardNum.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchStandardNum.Name = "uTextSearchStandardNum";
            this.uTextSearchStandardNum.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchStandardNum.TabIndex = 16;
            // 
            // uLabelSearchTitle
            // 
            this.uLabelSearchTitle.Location = new System.Drawing.Point(539, 12);
            this.uLabelSearchTitle.Name = "uLabelSearchTitle";
            this.uLabelSearchTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTitle.TabIndex = 12;
            this.uLabelSearchTitle.Text = "ultraLabel1";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(908, 36);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchProcess.TabIndex = 11;
            this.uComboSearchProcess.Text = "ultraComboEditor6";
            // 
            // uLabelSearchStandardNum
            // 
            this.uLabelSearchStandardNum.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchStandardNum.Name = "uLabelSearchStandardNum";
            this.uLabelSearchStandardNum.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStandardNum.TabIndex = 8;
            this.uLabelSearchStandardNum.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uGrid1);
            this.uGroupBox1.Controls.Add(this.uDateDistributeDate);
            this.uGroupBox1.Controls.Add(this.uLabelDistributeDate);
            this.uGroupBox1.Controls.Add(this.uTextDept);
            this.uGroupBox1.Controls.Add(this.uTextDistributeRequestUserName);
            this.uGroupBox1.Controls.Add(this.uTextDistributeRequestUserID);
            this.uGroupBox1.Controls.Add(this.uLabelDistributeRequestUser);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 100);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1064, 740);
            this.uGroupBox1.TabIndex = 3;
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance5;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance17;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance18;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 40);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1044, 692);
            this.uGrid1.TabIndex = 23;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGrid1_InitializeLayout);
            // 
            // uDateDistributeDate
            // 
            this.uDateDistributeDate.Location = new System.Drawing.Point(532, 12);
            this.uDateDistributeDate.Name = "uDateDistributeDate";
            this.uDateDistributeDate.Size = new System.Drawing.Size(100, 21);
            this.uDateDistributeDate.TabIndex = 22;
            // 
            // uLabelDistributeDate
            // 
            this.uLabelDistributeDate.Location = new System.Drawing.Point(428, 12);
            this.uLabelDistributeDate.Name = "uLabelDistributeDate";
            this.uLabelDistributeDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelDistributeDate.TabIndex = 21;
            this.uLabelDistributeDate.Text = "ultraLabel1";
            // 
            // uTextDept
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDept.Appearance = appearance15;
            this.uTextDept.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDept.Location = new System.Drawing.Point(320, 12);
            this.uTextDept.Name = "uTextDept";
            this.uTextDept.ReadOnly = true;
            this.uTextDept.Size = new System.Drawing.Size(100, 21);
            this.uTextDept.TabIndex = 20;
            // 
            // uTextDistributeRequestUserName
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDistributeRequestUserName.Appearance = appearance29;
            this.uTextDistributeRequestUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDistributeRequestUserName.Location = new System.Drawing.Point(218, 12);
            this.uTextDistributeRequestUserName.Name = "uTextDistributeRequestUserName";
            this.uTextDistributeRequestUserName.ReadOnly = true;
            this.uTextDistributeRequestUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextDistributeRequestUserName.TabIndex = 19;
            // 
            // uTextDistributeRequestUserID
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDistributeRequestUserID.Appearance = appearance16;
            this.uTextDistributeRequestUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance19.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance19;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDistributeRequestUserID.ButtonsRight.Add(editorButton2);
            this.uTextDistributeRequestUserID.Location = new System.Drawing.Point(116, 12);
            this.uTextDistributeRequestUserID.Name = "uTextDistributeRequestUserID";
            this.uTextDistributeRequestUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextDistributeRequestUserID.TabIndex = 18;
            this.uTextDistributeRequestUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDistributeRequestUserID_KeyDown);
            this.uTextDistributeRequestUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDistributeRequestUserID_EditorButtonClick);
            // 
            // uLabelDistributeRequestUser
            // 
            this.uLabelDistributeRequestUser.Location = new System.Drawing.Point(12, 12);
            this.uLabelDistributeRequestUser.Name = "uLabelDistributeRequestUser";
            this.uLabelDistributeRequestUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelDistributeRequestUser.TabIndex = 17;
            this.uLabelDistributeRequestUser.Text = "ultraLabel1";
            // 
            // frmISOZ0001A
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISOZ0001A";
            this.Load += new System.EventHandler(this.frmISOZ0001A_Load);
            this.Activated += new System.EventHandler(this.frmISOZ0001A_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISOZ0001A_FormClosing);
            this.Resize += new System.EventHandler(this.frmISOZ0001A_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDistributeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserID)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStandardNum;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStandardNum;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchDistributeRequestToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchDistributeRequestFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDistributeRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDistributeCompanyName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDistributeCompanyID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDistributeCompany;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDistributeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDept;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeRequestUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeRequestUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeRequestUser;
    }
}