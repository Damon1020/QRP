﻿namespace QRPISO.UI
{
    partial class frmISOZ0001
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISOZ0001));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchStandardNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchStandardNum = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchSmall = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMiddle = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchLarge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchRevisionNum = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchRevisionNum = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonAdd = new Infragistics.Win.Misc.UltraButton();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextHiddenDeptCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDistributeRequestUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uDateDistributeRequestDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDistributeRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDept = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDistributeRequestUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDistributeRequestUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDistributeNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDistributeNum = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRevisionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHiddenDeptCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDistributeRequestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeNum)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(804, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcess.TabIndex = 18;
            this.uLabelSearchProcess.Text = "ultraLabel1";
            // 
            // uTextSearchTitle
            // 
            this.uTextSearchTitle.Location = new System.Drawing.Point(380, 36);
            this.uTextSearchTitle.Name = "uTextSearchTitle";
            this.uTextSearchTitle.Size = new System.Drawing.Size(414, 21);
            this.uTextSearchTitle.TabIndex = 17;
            // 
            // uTextSearchStandardNum
            // 
            this.uTextSearchStandardNum.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchStandardNum.Name = "uTextSearchStandardNum";
            this.uTextSearchStandardNum.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchStandardNum.TabIndex = 16;
            // 
            // uLabelSearchTitle
            // 
            this.uLabelSearchTitle.Location = new System.Drawing.Point(276, 36);
            this.uLabelSearchTitle.Name = "uLabelSearchTitle";
            this.uLabelSearchTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTitle.TabIndex = 12;
            this.uLabelSearchTitle.Text = "ultraLabel1";
            // 
            // uLabelSearchStandardNum
            // 
            this.uLabelSearchStandardNum.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchStandardNum.Name = "uLabelSearchStandardNum";
            this.uLabelSearchStandardNum.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStandardNum.TabIndex = 8;
            this.uLabelSearchStandardNum.Text = "ultraLabel1";
            // 
            // uComboSearchSmall
            // 
            this.uComboSearchSmall.Location = new System.Drawing.Point(908, 12);
            this.uComboSearchSmall.Name = "uComboSearchSmall";
            this.uComboSearchSmall.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchSmall.TabIndex = 7;
            this.uComboSearchSmall.Text = "ultraComboEditor1";
            // 
            // uLabelSearchSmall
            // 
            this.uLabelSearchSmall.Location = new System.Drawing.Point(804, 12);
            this.uLabelSearchSmall.Name = "uLabelSearchSmall";
            this.uLabelSearchSmall.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchSmall.TabIndex = 6;
            this.uLabelSearchSmall.Text = "ultraLabel1";
            // 
            // uComboSearchMiddle
            // 
            this.uComboSearchMiddle.Location = new System.Drawing.Point(644, 12);
            this.uComboSearchMiddle.Name = "uComboSearchMiddle";
            this.uComboSearchMiddle.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchMiddle.TabIndex = 5;
            this.uComboSearchMiddle.Text = "ultraComboEditor1";
            this.uComboSearchMiddle.ValueChanged += new System.EventHandler(this.uComboSearchMiddle_ValueChanged);
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(908, 36);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchProcess.TabIndex = 11;
            this.uComboSearchProcess.Text = "ultraComboEditor6";
            // 
            // uLabelSearchMiddle
            // 
            this.uLabelSearchMiddle.Location = new System.Drawing.Point(540, 12);
            this.uLabelSearchMiddle.Name = "uLabelSearchMiddle";
            this.uLabelSearchMiddle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMiddle.TabIndex = 4;
            this.uLabelSearchMiddle.Text = "ultraLabel1";
            // 
            // uComboSearchLarge
            // 
            this.uComboSearchLarge.Location = new System.Drawing.Point(380, 12);
            this.uComboSearchLarge.Name = "uComboSearchLarge";
            this.uComboSearchLarge.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchLarge.TabIndex = 3;
            this.uComboSearchLarge.Text = "ultraComboEditor1";
            this.uComboSearchLarge.ValueChanged += new System.EventHandler(this.uComboSearchLarge_ValueChanged);
            // 
            // uLabelSearchLarge
            // 
            this.uLabelSearchLarge.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchLarge.Name = "uLabelSearchLarge";
            this.uLabelSearchLarge.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLarge.TabIndex = 2;
            this.uLabelSearchLarge.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uComboSearchRevisionNum
            // 
            this.uComboSearchRevisionNum.Location = new System.Drawing.Point(924, 108);
            this.uComboSearchRevisionNum.Name = "uComboSearchRevisionNum";
            this.uComboSearchRevisionNum.Size = new System.Drawing.Size(24, 21);
            this.uComboSearchRevisionNum.TabIndex = 15;
            this.uComboSearchRevisionNum.Text = "ultraComboEditor1";
            this.uComboSearchRevisionNum.Visible = false;
            // 
            // uLabelSearchRevisionNum
            // 
            this.uLabelSearchRevisionNum.Location = new System.Drawing.Point(916, 108);
            this.uLabelSearchRevisionNum.Name = "uLabelSearchRevisionNum";
            this.uLabelSearchRevisionNum.Size = new System.Drawing.Size(12, 20);
            this.uLabelSearchRevisionNum.TabIndex = 14;
            this.uLabelSearchRevisionNum.Text = "ultraLabel1";
            this.uLabelSearchRevisionNum.Visible = false;
            // 
            // uButtonAdd
            // 
            this.uButtonAdd.Location = new System.Drawing.Point(12, 112);
            this.uButtonAdd.Name = "uButtonAdd";
            this.uButtonAdd.Size = new System.Drawing.Size(88, 28);
            this.uButtonAdd.TabIndex = 2;
            this.uButtonAdd.Text = "ultraButton1";
            this.uButtonAdd.Click += new System.EventHandler(this.uButtonAdd_Click);
            // 
            // uGrid1
            // 
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance17;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance18.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance18.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance18.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance18;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance19;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance20.BackColor2 = System.Drawing.SystemColors.Control;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance20;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance21;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance23;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            appearance24.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance24;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance26.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance27;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance28;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(16, 144);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1044, 308);
            this.uGrid1.TabIndex = 3;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid1_ClickCell);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uTextHiddenDeptCode);
            this.uGroupBox1.Controls.Add(this.uTextDistributeRequestUserID);
            this.uGroupBox1.Controls.Add(this.uGrid2);
            this.uGroupBox1.Controls.Add(this.uButtonDelete);
            this.uGroupBox1.Controls.Add(this.uDateDistributeRequestDate);
            this.uGroupBox1.Controls.Add(this.uLabelDistributeRequestDate);
            this.uGroupBox1.Controls.Add(this.uTextDept);
            this.uGroupBox1.Controls.Add(this.uTextDistributeRequestUserName);
            this.uGroupBox1.Controls.Add(this.uLabelDistributeRequestUser);
            this.uGroupBox1.Controls.Add(this.uTextDistributeNum);
            this.uGroupBox1.Controls.Add(this.uLabelDistributeNum);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 456);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1052, 392);
            this.uGroupBox1.TabIndex = 4;
            // 
            // uTextHiddenDeptCode
            // 
            this.uTextHiddenDeptCode.Location = new System.Drawing.Point(1004, 16);
            this.uTextHiddenDeptCode.Name = "uTextHiddenDeptCode";
            this.uTextHiddenDeptCode.Size = new System.Drawing.Size(36, 21);
            this.uTextHiddenDeptCode.TabIndex = 57;
            this.uTextHiddenDeptCode.Visible = false;
            // 
            // uTextDistributeRequestUserID
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDistributeRequestUserID.Appearance = appearance3;
            this.uTextDistributeRequestUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance2.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDistributeRequestUserID.ButtonsRight.Add(editorButton1);
            this.uTextDistributeRequestUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDistributeRequestUserID.Location = new System.Drawing.Point(380, 12);
            this.uTextDistributeRequestUserID.Name = "uTextDistributeRequestUserID";
            this.uTextDistributeRequestUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextDistributeRequestUserID.TabIndex = 56;
            this.uTextDistributeRequestUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDistributeRequestUserID_KeyDown);
            this.uTextDistributeRequestUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDistributeRequestUserID_EditorButtonClick);
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            appearance31.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance31;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance32;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 68);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1032, 316);
            this.uGrid2.TabIndex = 18;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGrid2.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_ClickCellButton);
            this.uGrid2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uGrid2_KeyPress);
            this.uGrid2.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGrid2_InitializeLayout);
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 36);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 17;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uDateDistributeRequestDate
            // 
            this.uDateDistributeRequestDate.Location = new System.Drawing.Point(796, 12);
            this.uDateDistributeRequestDate.Name = "uDateDistributeRequestDate";
            this.uDateDistributeRequestDate.Size = new System.Drawing.Size(100, 21);
            this.uDateDistributeRequestDate.TabIndex = 16;
            // 
            // uLabelDistributeRequestDate
            // 
            this.uLabelDistributeRequestDate.Location = new System.Drawing.Point(692, 12);
            this.uLabelDistributeRequestDate.Name = "uLabelDistributeRequestDate";
            this.uLabelDistributeRequestDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelDistributeRequestDate.TabIndex = 15;
            this.uLabelDistributeRequestDate.Text = "ultraLabel1";
            // 
            // uTextDept
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDept.Appearance = appearance15;
            this.uTextDept.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDept.Location = new System.Drawing.Point(584, 12);
            this.uTextDept.Name = "uTextDept";
            this.uTextDept.ReadOnly = true;
            this.uTextDept.Size = new System.Drawing.Size(100, 21);
            this.uTextDept.TabIndex = 14;
            // 
            // uTextDistributeRequestUserName
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDistributeRequestUserName.Appearance = appearance29;
            this.uTextDistributeRequestUserName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextDistributeRequestUserName.Location = new System.Drawing.Point(482, 12);
            this.uTextDistributeRequestUserName.Name = "uTextDistributeRequestUserName";
            this.uTextDistributeRequestUserName.ReadOnly = true;
            this.uTextDistributeRequestUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextDistributeRequestUserName.TabIndex = 13;
            // 
            // uLabelDistributeRequestUser
            // 
            this.uLabelDistributeRequestUser.Location = new System.Drawing.Point(276, 12);
            this.uLabelDistributeRequestUser.Name = "uLabelDistributeRequestUser";
            this.uLabelDistributeRequestUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelDistributeRequestUser.TabIndex = 11;
            this.uLabelDistributeRequestUser.Text = "ultraLabel1";
            // 
            // uTextDistributeNum
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDistributeNum.Appearance = appearance14;
            this.uTextDistributeNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDistributeNum.Location = new System.Drawing.Point(116, 12);
            this.uTextDistributeNum.Name = "uTextDistributeNum";
            this.uTextDistributeNum.ReadOnly = true;
            this.uTextDistributeNum.Size = new System.Drawing.Size(150, 21);
            this.uTextDistributeNum.TabIndex = 10;
            // 
            // uLabelDistributeNum
            // 
            this.uLabelDistributeNum.Location = new System.Drawing.Point(12, 12);
            this.uLabelDistributeNum.Name = "uLabelDistributeNum";
            this.uLabelDistributeNum.Size = new System.Drawing.Size(100, 20);
            this.uLabelDistributeNum.TabIndex = 9;
            this.uLabelDistributeNum.Text = "ultraLabel1";
            // 
            // frmISOZ0001
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uComboSearchRevisionNum);
            this.Controls.Add(this.uButtonAdd);
            this.Controls.Add(this.uLabelSearchRevisionNum);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISOZ0001";
            this.Load += new System.EventHandler(this.frmISOZ0001_Load);
            this.Activated += new System.EventHandler(this.frmISOZ0001_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISOZ0001_FormClosing);
            this.Resize += new System.EventHandler(this.frmISOZ0001_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRevisionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHiddenDeptCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDistributeRequestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeRequestUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStandardNum;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchRevisionNum;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRevisionNum;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStandardNum;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchSmall;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMiddle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLarge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLarge;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraButton uButtonAdd;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeNum;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeNum;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDistributeRequestDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDept;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeRequestUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeRequestUser;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeRequestUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHiddenDeptCode;
    }
}