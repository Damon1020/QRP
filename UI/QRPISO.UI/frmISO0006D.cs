﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 수입검사규격서                                        */
/* 프로그램ID   : frmISO0006D.cs                                        */
/* 프로그램명   : 수입검사규격서 조회                                   */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPISO.UI
{
    public partial class frmISO0006D : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //Debug모드를 위한 변수

        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";
        private bool m_bolCopy = false;
        private bool m_bolErr = false;

        public frmISO0006D()
        {
            InitializeComponent();
        }

        private void frmISO0006D_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("수입검사규격서 등록/조회", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetRunMode();
            SetToolAuth();
            InitGrid();
            InitTab();
            InitLabel();
            InitButton();
            InitComboBox();
            InitTextBox();

            // ContentsArea 접힌 상태로
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    //MessageBox.Show(this.Tag.ToString());
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0006D_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitTextBox()
        {
            try
            {
                // TextBox 최대 입력길이 조절
                this.uTextMaterialCode.MaxLength = 20;
                this.uTextSearchMaterialCode.MaxLength = 20;
                this.uTextWriteUserID.MaxLength = 20;
                this.uTextEtc.MaxLength = 100;
                this.uTextSpec.MaxLength = 50;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchConsumableType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "등록자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCreateDate, "등록일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelInitPeriod, "초도품기간", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelInitPeriodUnitCode, "초도품기간 단위", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSpecNo, "Rev", m_resSys.GetString("SYS_FONTNAME"), true, true);
                //wLabel.mfSetLabel(this.uLabelMaterialGroup, "자재그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //wLabel.mfSetLabel(this.uLabelMaterialType, "자재유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSpec, "규격", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelConsumableType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelEtc1, "비고1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc2, "비고2", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc3, "비고3", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc4, "비고4", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonCopy, "복사", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);

                wButton.mfSetButton(this.uButtonDelete_2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonCopy_2, "복사", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // DB로부터 데이터 가져오는 Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // ComboBox 설정 메소드
                // SearchArea PlantComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // ContentsArea PlantComboBox
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0045", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboInitPeriodUnitCode, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtCom);


                // 검색조건 : 자재종류 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsumableType = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsConsumableType);

                DataTable dtCt = clsConsumableType.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));
                WinComboEditor combo = new WinComboEditor();
                combo.mfSetComboEditor(this.uComboSearchConsumableType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "ConsumableTypeCode", "ConsumableTypeName", dtCt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 수입검사규격서
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정 
                wGrid.mfSetGridColumn(this.uGrid, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////wGrid.mfSetGridColumn(this.uGrid, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ConsumableTypeCode", "자재종류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "FloorPlanNo", "도면No", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "SpecNo", "Rev", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteID", "생성자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteDate", "생성일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                // 상세정보 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectGroupCode", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectTypeCode", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectItemCode", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LowerSpec", "규격하한", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnn.nnnnn", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UpperSpec", "규격상한", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnn.nnnnn", "0.0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "SpecRange", "범위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Point", "Point", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "CompareFlag", "비교여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "SpecDesc", "규격설명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MeasureToolCode", "측정기기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                // 등급그리드
                this.uGridInspectGrade.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;

                wGrid.mfInitGeneralGrid(this.uGridInspectGrade, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //uGridInspectGrade.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                //uGridInspectGrade.DisplayLayout.Override.AllowRowLayoutColMoving = Infragistics.Win.Layout.GridBagLayoutAllowMoving.AllowAll;
                string strLang = m_resSys.GetString("SYS_LANG");
                string strEvery = "";
                string strShip = "";
                if (strLang.Equals("KOR"))
                { strEvery = "매입고시"; strShip = "Ship단위"; }
                else if (strLang.Equals("CHN"))
                { strEvery = "每入库时"; strShip = "Ship单位"; }
                else
                { strEvery = "매입고시"; strShip = "Ship단위"; }

                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup1 = wGrid.mfSetGridGroup(this.uGridInspectGrade, 0, "Every", strEvery, 5, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroup2 = wGrid.mfSetGridGroup(this.uGridInspectGrade, 0, "Ship", strShip, 8, 0, 5, 2, false);

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false", 0, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "InspectGroupCode", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, true, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "InspectTypeCode", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "InspectGradeCode", "검사등급", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, true, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "EverySampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0", 0, 0, 1, 1, uGroup1);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "EveryUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 1, 0, 1, 1, uGroup1);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "EveryTarget", "SampleSize대상", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 2, 0, 1, 1, uGroup1);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "ShipPeriod", "검사주기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0", 0, 0, 1, 1, uGroup2);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "ShipPeriodUnitCode", "주기단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 1, 0, 1, 1, uGroup2);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "ShipSampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0", 2, 0, 1, 1, uGroup2);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "ShipUnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 3, 0, 1, 1, uGroup2);

                wGrid.mfSetGridColumn(this.uGridInspectGrade, 0, "ShipTarget", "SampleSize대상", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 4, 0, 1, 1, uGroup2);

                // Set Font Size
                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridInspectGrade.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridInspectGrade.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGrid1, 0);
                wGrid.mfAddRowGrid(this.uGridInspectGrade, 0);

                // DropDown 설정
                DataTable dt = new DataTable();

                // 단위
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);
                dt = clsUnit.mfReadMASUnitCombo();

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "EveryUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "ShipUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);


                // 데이터 유형
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);
                dt = clsComCode.mfReadCommonCode("C0002", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "DataType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                // 검사등급
                dt = clsComCode.mfReadCommonCode("C0013", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "InspectGradeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                // 주기단위
                dt = clsComCode.mfReadCommonCode("C0005", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "ShipPeriodUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                // Sample대상
                dt = clsComCode.mfReadCommonCode("C0018", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "EveryTarget", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "ShipTarget", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                // 범위
                dt = clsComCode.mfReadCommonCode("C0032", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                dt = new DataTable();
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "InspectTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "InspectItemCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "InspectTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                ////////// 중복방지를 위한 바인딩
                ////////dt = new DataTable();
                ////////dt.Columns.Add("Seq", typeof(Int32));
                ////////dt.Columns.Add("InspectGroupCode", typeof(String));
                ////////dt.Columns.Add("InspectTypeCode", typeof(String));
                ////////dt.Columns.Add("InspectItemCode", typeof(String));
                ////////dt.Columns.Add("InspectCondition", typeof(String));
                ////////dt.Columns.Add("Method", typeof(String));
                ////////dt.Columns.Add("SpecDesc", typeof(String));
                ////////dt.Columns.Add("MeasureToolCode", typeof(String));
                ////////dt.Columns.Add("UpperSpec", typeof(decimal));
                ////////dt.Columns.Add("LowerSpec", typeof(decimal));
                ////////dt.Columns.Add("SpecRange", typeof(String));
                ////////dt.Columns.Add("SpecUnitCode", typeof(string));
                ////////dt.Columns.Add("Point", typeof(Int32));
                ////////dt.Columns.Add("SampleSize", typeof(decimal));
                ////////dt.Columns.Add("UnitCode", typeof(String));
                ////////dt.Columns.Add("CompareFlag", typeof(String));
                ////////dt.Columns.Add("DataType", typeof(String));
                ////////dt.Columns.Add("EtcDesc", typeof(String));

                ////////DataColumn[] dc = new DataColumn[3];
                ////////dc[0] = dt.Columns["InspectGroupCode"];
                ////////dc[1] = dt.Columns["InspectTypeCode"];
                ////////dc[2] = dt.Columns["InspectItemCode"];

                ////////dt.PrimaryKey = dc;

                ////////this.uGrid1.DataSource = dt;
                ////////this.uGrid1.DataBind();

                //DataTable dtGrade = new DataTable();
                //dtGrade.Columns.Add("Seq", typeof(Int32));
                //dtGrade.Columns.Add("InspectGroupCode", typeof(String));
                //dtGrade.Columns.Add("InspectTypeCode", typeof(String));
                //dtGrade.Columns.Add("InspectGradeCode", typeof(String));
                //dtGrade.Columns.Add("EverySampleSize", typeof(decimal));
                //dtGrade.Columns.Add("EveryUnitCode", typeof(String));
                //dtGrade.Columns.Add("EveryTarget", typeof(String));
                //dtGrade.Columns.Add("ShipPeriod", typeof(Int32));
                //dtGrade.Columns.Add("ShipPeriodUnitCode", typeof(String));
                //dtGrade.Columns.Add("ShipSampleSize", typeof(decimal));
                //dtGrade.Columns.Add("ShipUnitCode", typeof(String));
                //dtGrade.Columns.Add("ShipTarget", typeof(String));

                //dc = new DataColumn[3];
                //dc[0] = dtGrade.Columns["InspectGroupCode"];
                //dc[1] = dtGrade.Columns["InspectTypeCode"];
                //dc[2] = dtGrade.Columns["InspectGradeCode"];

                //dtGrade.PrimaryKey = dc;
                //this.uGridInspectGrade.DataSource = dtGrade;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 조회
        /// </summary>
        /// // 검색조건 자재종류 추가에 따른 검색메소드 수정 2011-11-02 / 이종호 //
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // 검색조건용 변수
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strMaterialCode = this.uTextSearchMaterialCode.Text;

                // 프로그래스 팝업창 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPISO.BL.ISOIMP.MaterialInspectSpecH clsHeader;
                // BL 연결
                if (m_bolDebugMode == false)
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
                    clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
                    brwChannel.mfCredentials(clsHeader);
                }
                else
                {
                    clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH(m_strDBConn);
                }

                string strConsumableType = this.uComboSearchConsumableType.Value.ToString();

                // 조회 Method 호출
                DataTable dtHeader = clsHeader.mfReadISOMaterialInspectSpecH(strPlantCode, "", strConsumableType, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                // Grid에 Binding
                this.uGrid.DataSource = dtHeader;
                this.uGrid.DataBind();

                WinGrid wGrid = new WinGrid();

                // ContentsArea 접은 상태로
                this.uGroupBoxContentsArea.Expanded = false;

                // 팝업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 결과 확인
                if (dtHeader.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    wGrid.mfSetAutoResizeColWidth(this.uGrid, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                QRPBrowser brwChannel = new QRPBrowser();

                DataRow drRow;

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    //this.uGroupBoxContentsArea.Expanded = true;
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001016", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    string strLang = m_resSys.GetString("SYS_LANG");
                    string strFont = m_resSys.GetString("SYS_FONTNAME");

                    // 필수입력사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uTextWriteUserID.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M000386", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uTextWriteUserID.Focus();
                        return;
                    }
                    else if (this.uTextMaterialCode.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M000975", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uTextMaterialCode.Focus();
                        return;
                    }
                    else if (this.uTextSpecNo.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M000114", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uTextMaterialCode.Focus();
                        return;
                    }
                    else if (this.uNumInitPeriod.Value.ToString() == "0")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001152", Infragistics.Win.HAlign.Right);

                        this.uNumInitPeriod.Focus();
                        return;
                    }
                    else if (this.uComboInitPeriodUnitCode.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001151", Infragistics.Win.HAlign.Right);

                        this.uComboInitPeriodUnitCode.DropDown();
                        return;
                    }
                    else
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        // 상세정보 저장
                        // 상세정보를 저장하기 위한 DataTable Column 설정
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecD), "MaterialInspectSpecD");
                        QRPISO.BL.ISOIMP.MaterialInspectSpecD clsDetail = new QRPISO.BL.ISOIMP.MaterialInspectSpecD();
                        brwChannel.mfCredentials(clsDetail);

                        DataTable dtSaveDetail = clsDetail.mfSetDataInfo();
                        // 상세정보 필수 입력사항 확인
                        if (this.uGrid1.Rows.Count > 0)
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            // 저장일시
                            if (this.uGrid1.Rows[i].Hidden == false)
                            {
                                if (this.uGrid1.Rows[i].Cells["InspectGroupCode"].Value.ToString() == "")
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000586", strLang)
                                                        , Infragistics.Win.HAlign.Right);
                                    // Set Focus
                                    this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["InspectGroupCode"];
                                    this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                else if (this.uGrid1.Rows[i].Cells["InspectTypeCode"].Value.ToString() == "")
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000587", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                    // Set Focus
                                    this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["InspectTypeCode"];
                                    this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                else if (this.uGrid1.Rows[i].Cells["InspectItemCode"].Value.ToString() == "")
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000588", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                    // Set Focus
                                    this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["InspectItemCode"];
                                    this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                else
                                {
                                    // 상세정보 저장
                                    drRow = dtSaveDetail.NewRow();

                                    drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                    drRow["VersionNum"] = 1;
                                    drRow["ItemNum"] = this.uGrid1.Rows[i].RowSelectorNumber;
                                    drRow["Seq"] = this.uGrid1.Rows[i].RowSelectorNumber;
                                    drRow["InspectGroupCode"] = this.uGrid1.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                                    drRow["InspectTypeCode"] = this.uGrid1.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                                    drRow["InspectItemCode"] = this.uGrid1.Rows[i].Cells["InspectItemCode"].Value.ToString();
                                    drRow["InspectCondition"] = this.uGrid1.Rows[i].Cells["InspectCondition"].Value.ToString();
                                    drRow["Method"] = this.uGrid1.Rows[i].Cells["Method"].Value.ToString();
                                    drRow["SpecDesc"] = this.uGrid1.Rows[i].Cells["SpecDesc"].Value.ToString();
                                    drRow["MeasureToolCode"] = this.uGrid1.Rows[i].Cells["MeasureToolCode"].Value.ToString();
                                    drRow["UpperSpec"] = ReturnDecimalValue(this.uGrid1.Rows[i].Cells["UpperSpec"].Value.ToString());
                                    drRow["LowerSpec"] = ReturnDecimalValue(this.uGrid1.Rows[i].Cells["LowerSpec"].Value.ToString());
                                    drRow["SpecRange"] = this.uGrid1.Rows[i].Cells["SpecRange"].Value.ToString();
                                    drRow["SpecUnitCode"] = this.uGrid1.Rows[i].Cells["SpecUnitCode"].Value.ToString();
                                    drRow["Point"] = ReturnIntValue(this.uGrid1.Rows[i].Cells["Point"].Value.ToString());
                                    drRow["SampleSize"] = ReturnDecimalValue(this.uGrid1.Rows[i].Cells["SampleSize"].Value.ToString());
                                    drRow["UnitCode"] = this.uGrid1.Rows[i].Cells["UnitCode"].Value.ToString();
                                    if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["CompareFlag"].Value) == true)
                                        drRow["CompareFlag"] = "T";
                                    else
                                        drRow["CompareFlag"] = "F";
                                    drRow["DataType"] = this.uGrid1.Rows[i].Cells["DataType"].Value.ToString();
                                    drRow["EtcDesc"] = this.uGrid1.Rows[i].Cells["EtcDesc"].Value.ToString();

                                    dtSaveDetail.Rows.Add(drRow);
                                }
                            }
                        }
                        // 등급 정보 저장
                        // 등급 정보를 저장할 DataTable 컬럼 설정
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecGrade), "MaterialInspectSpecGrade");
                        QRPISO.BL.ISOIMP.MaterialInspectSpecGrade clsGrade = new QRPISO.BL.ISOIMP.MaterialInspectSpecGrade();
                        brwChannel.mfCredentials(clsGrade);

                        DataTable dtGrade = clsGrade.mfSetDataInfo();

                        // 등급정보 필수 입력사항 확인
                        if (this.uGridInspectGrade.Rows.Count > 0)
                            this.uGridInspectGrade.ActiveCell = this.uGridInspectGrade.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridInspectGrade.Rows.Count; i++)
                        {
                            // 저장 필수입력사항 확인
                            if (this.uGridInspectGrade.Rows[i].Hidden == false)
                            {
                                if (this.uGridInspectGrade.Rows[i].Cells["InspectGroupCode"].Value.ToString() == "")
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000586", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                    // Set Focus
                                    this.uGridInspectGrade.ActiveCell = this.uGridInspectGrade.Rows[i].Cells["InspectGroupCode"];
                                    this.uGridInspectGrade.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                else if (this.uGridInspectGrade.Rows[i].Cells["InspectTypeCode"].Value.ToString() == "")
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                       , (i + 1).ToString() + msg.GetMessge_Text("M000587", strLang)
                                                       , Infragistics.Win.HAlign.Right);

                                    // Set Focus
                                    this.uGridInspectGrade.ActiveCell = this.uGridInspectGrade.Rows[i].Cells["InspectTypeCode"];
                                    this.uGridInspectGrade.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                else if (this.uGridInspectGrade.Rows[i].Cells["InspectGradeCode"].Value.ToString() == "")
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000585", strLang)
                                                        , Infragistics.Win.HAlign.Right);

                                    // Set Focus
                                    this.uGridInspectGrade.ActiveCell = this.uGridInspectGrade.Rows[i].Cells["InspectGradeCode"];
                                    this.uGridInspectGrade.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                else if ((ReturnDecimalValue(this.uGridInspectGrade.Rows[i].Cells["EverySampleSize"].Value.ToString()) == 0m ||
                                        this.uGridInspectGrade.Rows[i].Cells["EveryUnitCode"].Value.ToString() == "" ||
                                        this.uGridInspectGrade.Rows[i].Cells["EveryTarget"].Value.ToString() == "") &&
                                        (ReturnDecimalValue(this.uGridInspectGrade.Rows[i].Cells["ShipSampleSize"].Value.ToString()) == 0m ||
                                        this.uGridInspectGrade.Rows[i].Cells["ShipUnitCode"].Value.ToString() == "" ||
                                        this.uGridInspectGrade.Rows[i].Cells["ShipTarget"].Value.ToString() == ""))
                                {
                                    Result = msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                       , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001230", strLang)
                                                       , (i + 1).ToString() + msg.GetMessge_Text("M000590", strLang)
                                                       , Infragistics.Win.HAlign.Right);

                                    // Set Focus
                                    this.uGridInspectGrade.ActiveCell = this.uGridInspectGrade.Rows[i].Cells["EverySampleSize"];
                                    this.uGridInspectGrade.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                else
                                {
                                    drRow = dtGrade.NewRow();

                                    drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                    drRow["VersionNum"] = 1;
                                    drRow["Seq"] = this.uGridInspectGrade.Rows[i].RowSelectorNumber;
                                    drRow["InspectGroupCode"] = this.uGridInspectGrade.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                                    drRow["InspectTypeCode"] = this.uGridInspectGrade.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                                    drRow["InspectGradeCode"] = this.uGridInspectGrade.Rows[i].Cells["InspectGradeCode"].Value.ToString();
                                    drRow["EverySampleSize"] = ReturnDecimalValue(this.uGridInspectGrade.Rows[i].Cells["EverySampleSize"].Value.ToString());
                                    drRow["EveryUnitCode"] = this.uGridInspectGrade.Rows[i].Cells["EveryUnitCode"].Value.ToString();
                                    drRow["EveryTarget"] = this.uGridInspectGrade.Rows[i].Cells["EveryTarget"].Value.ToString();
                                    drRow["ShipPeriod"] = ReturnIntValue(this.uGridInspectGrade.Rows[i].Cells["ShipPeriod"].Value.ToString());
                                    drRow["ShipPeriodUnitCode"] = this.uGridInspectGrade.Rows[i].Cells["ShipPeriodUnitCode"].Value.ToString();
                                    drRow["ShipSampleSize"] = ReturnDecimalValue(this.uGridInspectGrade.Rows[i].Cells["ShipSampleSize"].Value.ToString());
                                    drRow["ShipUnitCode"] = this.uGridInspectGrade.Rows[i].Cells["ShipUnitCode"].Value.ToString();
                                    drRow["ShipTarget"] = this.uGridInspectGrade.Rows[i].Cells["ShipTarget"].Value.ToString();

                                    dtGrade.Rows.Add(drRow);
                                }
                            }
                        }
                        // 저장할 상세정보가 있을경우만 저장
                        if (dtSaveDetail.Rows.Count > 0 && dtGrade.Rows.Count > 0)
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
                            QRPISO.BL.ISOIMP.MaterialInspectSpecH clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
                            brwChannel.mfCredentials(clsHeader);

                            DataTable dtHeader = clsHeader.mfSetDataInfo();

                            String strStdNumberSeq = this.uTextStdNumber.Text;

                            // 헤더정보 저장
                            drRow = dtHeader.NewRow();
                            drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                            if (strStdNumberSeq == "")
                            {
                                drRow["StdNumber"] = "";
                                drRow["StdSeq"] = "";
                            }
                            else
                            {
                                drRow["StdNumber"] = strStdNumberSeq.Substring(0, 9);
                                drRow["StdSeq"] = strStdNumberSeq.Substring(9, 4);
                            }
                            drRow["VersionNum"] = 1;
                            drRow["MaterialCode"] = this.uTextMaterialCode.Text;
                            drRow["SpecNo"] = this.uTextSpecNo.Text;
                            drRow["WriteID"] = this.uTextWriteUserID.Text;
                            drRow["WriteDate"] = this.uDateCreateDate.Value.ToString();
                            drRow["EtcDesc"] = this.uTextEtc.Text;
                            drRow["EtcDesc1"] = this.RichTextEtc1.GetDocumentText();
                            drRow["EtcDesc2"] = this.RichTextEtc2.GetDocumentText();
                            drRow["EtcDesc3"] = this.RichTextEtc3.GetDocumentText();
                            drRow["EtcDesc4"] = this.RichTextEtc4.GetDocumentText();
                            drRow["InitPeriod"] = this.uNumInitPeriod.Value.ToString();
                            drRow["InitPeriodUnitCode"] = this.uComboInitPeriodUnitCode.Value.ToString();
                            dtHeader.Rows.Add(drRow);

                            // 저장여부를 묻는다
                            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                            {
                                // 프로그래스 팝업창 생성
                                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                Thread t1 = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                String strErrRtn = clsHeader.mfSaveISOMaterialInspectSpecH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtSaveDetail, dtGrade);

                                // 팦업창 Close
                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);
                                System.Windows.Forms.DialogResult result;

                                // 결과 검사
                                TransErrRtn ErrRtn = new TransErrRtn();
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum == 0)
                                {
                                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                 "M001135", "M001037", "M000930", Infragistics.Win.HAlign.Right);

                                    // List 갱신
                                    mfSearch();
                                }
                                else
                                {
                                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                 "M001135", "M001037", "M000953.", Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000652", "M001054", Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000632", "M000633", Infragistics.Win.HAlign.Right);
                }
                else
                {

                    // 필수입력사항 확인
                    if (this.uTextStdNumber.Text == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000335", Infragistics.Win.HAlign.Center);

                        this.uGroupBoxContentsArea.Expanded = false;
                        return;
                    }
                    else if (this.uComboPlant.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        this.uComboPlant.DropDown();
                        return;
                    }
                    else
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000650", "M000675", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
                            QRPISO.BL.ISOIMP.MaterialInspectSpecH clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
                            brwChannel.mfCredentials(clsHeader);

                            // DataTable 컬럼 설정
                            DataTable dtHeader = clsHeader.mfSetDataInfo();

                            DataRow drRow = dtHeader.NewRow();

                            String strStdNumberSeq = this.uTextStdNumber.Text;

                            drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                            drRow["StdNumber"] = strStdNumberSeq.Substring(0, 9);
                            drRow["StdSeq"] = strStdNumberSeq.Substring(9, 4);

                            dtHeader.Rows.Add(drRow);

                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string rtMSG = clsHeader.mfDeleteISOMaterialInspectSpecH(dtHeader);

                            // Decoding //
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                            // 처리로직 끝 //

                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            // 삭제성공여부
                            if (ErrRtn.ErrNum == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000638", "M000926",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000638", "M000925",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                // ContentsArea 가 접힌 상태이면 펼침상태로
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    // 이미 펼쳐진 상태면 입력가능한 Control 초기화
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGrid.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGrid);
                    if (this.uGrid1.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGrid1);
                    }
                    if (this.uGridInspectGrade.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridInspectGrade);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000753",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method...
        /// <summary>
        /// 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strFullStdNumber"> 표준번호 + 표준순번 </param>
        private void Search_HeaderD(String strPlantCode, String strFullStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();

                // 헤더 상세정보 조회
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
                QRPISO.BL.ISOIMP.MaterialInspectSpecH clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtHeader = clsHeader.mfReadISOMaterialInspectSpecHDetail(strPlantCode, strStdNumber, strStdSeq, m_resSys.GetString("SYS_LANG"));

                // 각 컨트롤에 데이터 적용
                this.uTextStdNumber.Text = dtHeader.Rows[0]["StdNumber"].ToString();
                ////this.uTextWriteUserID.Text = dtHeader.Rows[0]["WriteID"].ToString();
                ////this.uTextWriteUserName.Text = dtHeader.Rows[0]["UserName"].ToString();
                ////this.uDateCreateDate.Value = dtHeader.Rows[0]["WriteDate"].ToString();

                this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uDateCreateDate.Value = DateTime.Now;

                this.uComboPlant.Value = dtHeader.Rows[0]["PlantCode"].ToString();
                this.uTextMaterialCode.Text = dtHeader.Rows[0]["MaterialCode"].ToString();
                this.uTextMaterialName.Text = dtHeader.Rows[0]["MaterialName"].ToString();

                this.uTextMaterialGroup.Text = dtHeader.Rows[0]["MaterialGroupCode"].ToString();
                this.uTextMaterialType.Text = dtHeader.Rows[0]["MaterialTypeCode"].ToString();
                this.uTextConsumableType.Text = dtHeader.Rows[0]["ConsumableTypeCode"].ToString();

                this.uTextSpecNo.Text = dtHeader.Rows[0]["SpecNo"].ToString();
                this.uTextSpec.Text = dtHeader.Rows[0]["Spec"].ToString();
                this.uTextEtc.Text = dtHeader.Rows[0]["EtcDesc"].ToString();
                this.RichTextEtc1.SetDocumentText(dtHeader.Rows[0]["EtcDesc1"].ToString());
                this.RichTextEtc2.SetDocumentText(dtHeader.Rows[0]["EtcDesc2"].ToString());
                this.RichTextEtc3.SetDocumentText(dtHeader.Rows[0]["EtcDesc3"].ToString());
                this.RichTextEtc4.SetDocumentText(dtHeader.Rows[0]["EtcDesc4"].ToString());
                this.uNumInitPeriod.Value = Convert.ToInt32(dtHeader.Rows[0]["InitPeriod"]);
                this.uComboInitPeriodUnitCode.Value = dtHeader.Rows[0]["InitPeriodUnitCode"].ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strFullStdNumber"> 표준번호+표준번호순번 </param>
        private void Search_Detail(String strPlantCode, String strFullStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 표준번호와 표준번호순번으로 구분
                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                // 상세정보 조회
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecD), "MaterialInspectSpecD");
                QRPISO.BL.ISOIMP.MaterialInspectSpecD clsDetail = new QRPISO.BL.ISOIMP.MaterialInspectSpecD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtDetail = clsDetail.mfReadISOMaterialInspectSpecD(strPlantCode, strStdNumber, strStdSeq);

                // 검사항목 중복방지 키설정
                ////////DataColumn[] dc = new DataColumn[3];
                ////////dc[0] = dtDetail.Columns["InspectGroupCode"];
                ////////dc[1] = dtDetail.Columns["InspectTypeCode"];
                ////////dc[2] = dtDetail.Columns["InspectItemCode"];

                ////////dtDetail.PrimaryKey = dc;

                this.uGrid1.DataSource = dtDetail;
                this.uGrid1.DataBind();

                // 검사유형
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(clsIType);
                DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, string.Empty, m_resSys.GetString("SYS_LANG"));

                // 검사항목
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                QRPMAS.BL.MASQUA.InspectItem clsIItem = new QRPMAS.BL.MASQUA.InspectItem();
                brwChannel.mfCredentials(clsIItem);
                DataTable dtInspectItem = clsIItem.mfReadMASInspectItemCombo(strPlantCode, string.Empty, string.Empty, m_resSys.GetString("SYS_LANG"));

                // DropDown 설정
                // 상세 그리드에 드랍다운박스 설정
                WinGrid wGrid = new WinGrid();
                for (int j = 0; j < this.uGrid1.Rows.Count; j++)
                {
                    // 변수
                    String strInspectGroupCode = this.uGrid1.Rows[j].Cells["InspectGroupCode"].Value.ToString();
                    String strInspectTypeoCode = this.uGrid1.Rows[j].Cells["InspectTypeCode"].Value.ToString();

                    //셀에서 선택한 검사그룹에 따라 검사유형 나오게 하기
                    DataRow[] drInspectType = dtInspectType.Select("InspectGroupCode = '" + strInspectGroupCode + "'");
                    DataTable dtTypeSelect = new DataTable();
                    dtTypeSelect.Columns.Add("InspectTypeCode", typeof(string));
                    dtTypeSelect.Columns.Add("InspectTypeName", typeof(string));
                    dtTypeSelect.Clear();

                    foreach (DataRow dr in drInspectType)
                    {
                        DataRow _dr = dtTypeSelect.NewRow();
                        for (int i = 0; i < 2; i++)
                        {
                            _dr[i] = dr[i];
                        }
                        dtTypeSelect.Rows.Add(_dr);
                    }
                    wGrid.mfSetGridCellValueList(this.uGrid1, j, "InspectTypeCode", "", "선택", dtTypeSelect);

                    //셀에서 선택한 검사그룹, 검사유형에 따라 검사항목 나오게 하기
                    DataRow[] drInspectItem = dtInspectItem.Select("InspectGroupCode = '" + strInspectGroupCode + "' AND InspectTypeCode = '" + strInspectTypeoCode + "'");
                    DataTable dtItemSelect = new DataTable();
                    dtItemSelect.Columns.Add("InspectItemCode", typeof(string));
                    dtItemSelect.Columns.Add("InspectItemName", typeof(string));
                    dtItemSelect.Clear();

                    foreach (DataRow dr in drInspectItem)
                    {
                        DataRow _dr = dtItemSelect.NewRow();
                        for (int i = 0; i < 2; i++)
                        {
                            _dr[i] = dr[i];
                        }
                        dtItemSelect.Rows.Add(_dr);
                    }
                    wGrid.mfSetGridCellValueList(this.uGrid1, j, "InspectItemCode", "", "선택", dtItemSelect);
                }
                if (dtDetail.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 등급정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strFullStdNumber"> 표준번호+표준번호순번 </param>
        private void Search_Grade(String strPlantCode, String strFullStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 표준번호와 표준번호순번으로 구분
                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecGrade), "MaterialInspectSpecGrade");
                QRPISO.BL.ISOIMP.MaterialInspectSpecGrade clsGrade = new QRPISO.BL.ISOIMP.MaterialInspectSpecGrade();
                brwChannel.mfCredentials(clsGrade);

                DataTable dtGrade = clsGrade.mfReadMaterialInspectSpecGrade(strPlantCode, strStdNumber, strStdSeq);

                //DataColumn[] dc = new DataColumn[3];
                //dc[0] = dtGrade.Columns["InspectGroupCode"];
                //dc[1] = dtGrade.Columns["InspectTypeCode"];
                //dc[2] = dtGrade.Columns["InspectGradeCode"];

                //dtGrade.PrimaryKey = dc;

                this.uGridInspectGrade.DataSource = dtGrade;
                this.uGridInspectGrade.DataBind();

                // DropDown 설정
                // 검사유형

                for (int i = 0; i < this.uGridInspectGrade.Rows.Count; i++)
                {
                    // 변수

                    String strInspectGroupCode = this.uGridInspectGrade.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                    String strInspectTypeoCode = this.uGridInspectGrade.Rows[i].Cells["InspectTypeCode"].Value.ToString();

                    // 검사유형

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(clsIType);

                    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));

                    wGrid.mfSetGridCellValueList(this.uGridInspectGrade, i, "InspectTypeCode", "", "선택", dtInspectType);
                }

                if (dtGrade.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridInspectGrade, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// DB에 이미 표준번호가 있는지 확인하는 Method
        /// </summary>
        private void CheckStdNumber()
        {
            Boolean bolCheck = false;
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();
                //String strVendorCode = this.uTextVendorCode.Text;
                String strMaterialCode = this.uTextMaterialCode.Text;
                String strSpecNo = this.uTextSpecNo.Text;

                // Method 호출에 필요한 모든 정보가 입력되었을때
                if (strPlantCode != "" && strMaterialCode != "" && strSpecNo != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
                    QRPISO.BL.ISOIMP.MaterialInspectSpecH clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
                    brwChannel.mfCredentials(clsHeader);

                    // 기존에 존재하는 표준번호가 있는지 확인하는 Method 호출
                    DataTable dt = clsHeader.mfReadISOMaterialInspectSpecHCheck(strPlantCode, "", strMaterialCode, strSpecNo);

                    if (dt.Rows.Count > 0)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001208", "M000349",
                                            Infragistics.Win.HAlign.Right);

                        // ProgressPopup 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            this.uTextStdNumber.Text = dt.Rows[i]["StdNumber"].ToString() + dt.Rows[i]["StdSeq"].ToString();
                            this.uTextWriteUserID.Text = dt.Rows[i]["WriteID"].ToString();
                            this.uDateCreateDate.Value = dt.Rows[i]["WriteDate"].ToString();
                            this.uTextEtc.Text = dt.Rows[i]["EtcDesc"].ToString();
                            this.RichTextEtc1.SetDocumentText(dt.Rows[i]["EtcDesc1"].ToString());
                            this.RichTextEtc2.SetDocumentText(dt.Rows[i]["EtcDesc2"].ToString());
                            this.RichTextEtc3.SetDocumentText(dt.Rows[i]["EtcDesc3"].ToString());
                            this.RichTextEtc4.SetDocumentText(dt.Rows[i]["EtcDesc4"].ToString());
                            this.uNumInitPeriod.Value = Convert.ToInt32(dt.Rows[i]["InitPeriod"]);
                            this.uComboInitPeriodUnitCode.Value = dt.Rows[i]["InitPeriodUnitCode"].ToString();
                        }

                        // 상세/등급정보 조회
                        String strFullStdNumber = this.uTextStdNumber.Text;
                        Search_Detail(strPlantCode, strFullStdNumber);
                        Search_Grade(strPlantCode, strFullStdNumber);

                        // PK 편집 불가 상태로

                        this.uComboPlant.Enabled = false;

                        bolCheck = true;

                        // 팝업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                    // 등록된 표준번호가 없는경우 입력창 초기화

                    if (bolCheck == false)
                    {
                        this.uComboPlant.Enabled = true;
                        this.uTextStdNumber.Text = "";
                        while (this.uGrid1.Rows.Count > 0)
                        {
                            this.uGrid1.Rows[0].Delete(false);
                        }
                        while (this.uGridInspectGrade.Rows.Count > 0)
                        {
                            this.uGridInspectGrade.Rows[0].Delete(false);
                        }
                        this.uTextEtc.Text = "";
                        this.RichTextEtc1.Clear();
                        this.RichTextEtc2.Clear();
                        this.RichTextEtc3.Clear();
                        this.RichTextEtc4.Clear();
                        this.uNumInitPeriod.Value = 0;
                        this.uComboInitPeriodUnitCode.Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtMaterial;
            }
            finally
            {
            }
        }

        /////// <summary>
        /////// 거래처 정보 조회 함수
        /////// </summary>
        /////// <param name="strVendorCode"> 거래처코드 </param>
        /////// <returns></returns>
        ////private DataTable GetVendorInfo(String strVendorCode)
        ////{
        ////    DataTable dtVendor = new DataTable();
        ////    try
        ////    {
        ////        // SystemInfo ResourceSet
        ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
        ////        QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
        ////        brwChannel.mfCredentials(clsVendor);

        ////        dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, m_resSys.GetString("SYS_LANG"));

        ////        return dtVendor;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return dtVendor;
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        /// <summary>
        /// Control Clear Method
        /// </summary>
        private void Clear()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 등록자에 로그인 사용자 ID, Name 적용
                String strWriteID = m_resSys.GetString("SYS_USERID");
                String strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                // ID로 UserName정보 가져오는 Method 호출
                String strWriteName = m_resSys.GetString("SYS_USERNAME");

                this.uTextWriteUserID.Text = strWriteID;
                this.uTextWriteUserName.Text = strWriteName;

                this.uTextStdNumber.Text = "";
                this.uDateCreateDate.Value = DateTime.Now;
                this.uComboPlant.Value = strPlantCode;
                this.uTextMaterialCode.Text = "";
                this.uTextMaterialName.Text = "";
                this.uTextMaterialGroup.Text = "";
                this.uTextMaterialType.Text = "";
                this.uTextConsumableType.Text = "";
                this.uTextSpecNo.Text = "";
                this.uTextSpec.Text = "";
                this.uTextEtc.Text = "";
                this.RichTextEtc1.Clear();
                this.RichTextEtc2.Clear();
                this.RichTextEtc3.Clear();
                this.RichTextEtc4.Clear();
                this.uNumInitPeriod.Value = 0;
                this.uComboInitPeriodUnitCode.Value = "";

                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }

                while (this.uGridInspectGrade.Rows.Count > 0)
                {
                    this.uGridInspectGrade.Rows[0].Delete(false);
                }

                // PK 편집 가능 상태로
                this.uComboPlant.Enabled = true;
                this.uTextSpecNo.Enabled = true;
                this.uTab.Tabs[0].Selected = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Event...
        // ContentsGroupBox 상태변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 720;

                    for (int i = 0; i < uGrid.Rows.Count; i++)
                    {
                        uGrid.Rows[i].Fixed = false;
                    }

                    // Control Clear
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 셀 업데이트 이벤트
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                // 빈행 자동삭제
                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                // 규격 상한/하한 입력에 따라 범위 Cell 값 지정
                if (e.Cell.Column.Key.Equals("UpperSpec"))
                {
                    if (e.Cell.Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Value.ToString()))
                    {
                        if (e.Cell.Row.Cells["LowerSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["LowerSpec"].Value.ToString()))
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "";
                        }
                        else
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "U";
                        }
                    }
                    else if (e.Cell.Row.Cells["LowerSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["LowerSpec"].Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "L";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["LowerSpec"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["LowerSpec"].Value.ToString()) > ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000323"
                            , "M000324", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        return;
                    }
                }
                else if (e.Cell.Column.Key.Equals("LowerSpec"))
                {
                    if (e.Cell.Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Value.ToString()))
                    {
                        if (e.Cell.Row.Cells["UpperSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["UpperSpec"].Value.ToString()))
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "";
                        }
                        else
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "L";
                        }
                    }
                    else if (e.Cell.Row.Cells["UpperSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["UpperSpec"].Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "U";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["UpperSpec"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["UpperSpec"].Value.ToString()) < ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000323"
                            , "M000324", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        return;
                    }
                }

                // 검사유형 DropDown 설정
                else if (e.Cell.Column.Key == "InspectGroupCode")
                {
                    // 변수
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(clsIType);

                    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));

                    wGrid.mfSetGridCellValueList(this.uGrid1, e.Cell.Row.Index, "InspectTypeCode", "", "선택", dtInspectType);
                    e.Cell.Row.Cells["InspectTypeCode"].Value = "";

                    //e.Cell.Row.Cells["InspectTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }

                // 검사항목 DropDown 설정
                else if (e.Cell.Column.Key == "InspectTypeCode")
                {
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                    String strInspectTypeoCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(clsItem);

                    DataTable dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, strInspectGroupCode, strInspectTypeoCode, m_resSys.GetString("SYS_LANG"));

                    //셀에서 선택한 검사그룹, 검사유형에 따라 검사항목 나오게 하기
                    DataRow[] drInspectItem = dtItem.Select("InspectGroupCode = '" + strInspectGroupCode + "' AND InspectTypeCode = '" + strInspectTypeoCode + "'");
                    DataTable dtItemSelect = new DataTable();
                    dtItemSelect.Columns.Add("InspectItemCode", typeof(string));
                    dtItemSelect.Columns.Add("InspectItemName", typeof(string));
                    dtItemSelect.Clear();

                    foreach (DataRow dr in drInspectItem)
                    {
                        DataRow _dr = dtItemSelect.NewRow();
                        for (int i = 0; i < 2; i++)
                        {
                            _dr[i] = dr[i];
                        }
                        dtItemSelect.Rows.Add(_dr);
                    }
                    wGrid.mfSetGridCellValueList(this.uGrid1, e.Cell.Row.Index, "InspectItemCode", "", "선택", dtItemSelect);
                    //e.Cell.Row.Cells["InspectTypeItem"].Value = "";

                    //e.Cell.Row.Cells["InspectItemCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }

                // 검사항목 선택시 데이터 유형 자동 선택
                else if (e.Cell.Column.Key == "InspectItemCode")
                {
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (this.uGrid1.Rows[i].Cells["InspectGroupCode"].Value.ToString().Equals(e.Cell.Row.Cells["InspectGroupCode"].Value.ToString()) &&
                            this.uGrid1.Rows[i].Cells["InspectTypeCode"].Value.ToString().Equals(e.Cell.Row.Cells["InspectTypeCode"].Value.ToString()) &&
                            this.uGrid1.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals(e.Cell.Row.Cells["InspectItemCode"].Value.ToString()) &&
                            this.uGrid1.Rows[i].RowSelectorNumber != e.Cell.Row.RowSelectorNumber)
                        {
                            string strLang = m_resSys.GetString("SYS_LANG");
                            string strFont = m_resSys.GetString("SYS_FONTNAME");

                            msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                 , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001122", strLang)
                                 , e.Cell.Row.Cells["InspectItemCode"].Text + msg.GetMessge_Text("M000192", strLang)
                                 , Infragistics.Win.HAlign.Right);

                            e.Cell.Row.Delete(false);
                            return;
                        }
                    }

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                    String strInspectTypeCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();
                    String strInspectItemCode = e.Cell.Value.ToString();

                    // 검색조건

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(clsItem);

                    DataTable dtItem = clsItem.mfReadMASInspectItemDetail(strPlantCode, strInspectItemCode, m_resSys.GetString("SYS_LANG"));

                    if (strInspectItemCode != "")
                        // 데이터 유형 필드에 값 적용
                        e.Cell.Row.Cells["DataType"].Value = dtItem.Rows[0]["DataType"].ToString();
                    else
                        e.Cell.Row.Cells["DataType"].Value = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀에 변경이 일어났을시 이벤트
        private void uGrid1_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                DialogResult Result = new DialogResult();

                if (this.uTextMaterialCode.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000975", Infragistics.Win.HAlign.Right);
                    this.uTextMaterialCode.Focus();
                    return;
                }

                if (this.uTextSpecNo.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000122", Infragistics.Win.HAlign.Right);
                    this.uTextSpecNo.Focus();
                    return;
                }

                // RowSelector Image 설정
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제버튼 이벤트
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                    {
                        //this.uGrid1.Rows[i].Hidden = true;
                        this.uGrid1.Rows[i].Delete(false);
                        i--;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDelete_2_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridInspectGrade.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridInspectGrade.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridInspectGrade.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장 콤보박스에 따라 검사분류 / 계측기를 설정하기 위한 이벤트
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                DataTable dtInspectGroup = new DataTable();
                DataTable dtMeasureTool = new DataTable();
                DataTable dtConType = new DataTable();
                DataTable dtItem = new DataTable();

                if (this.uComboPlant.Value.ToString() != "")
                {
                    ////if (this.uGrid1.Rows.Count > 0)
                    ////{
                    ////WinMessageBox msg = new WinMessageBox();
                    ////// 공장콤보박스 변경에 따른 그리드 초기화를 진행할것인지 묻는 메세지 박스
                    ////if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                    ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    ////                    "확인창", "변경확인", "공장 콤보박스를 변경하면 상세정보 그리드가 초기화 됩니다, 초기화 하시겠습니까?",
                    ////                    Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    ////{
                    String strPlantCode = this.uComboPlant.Value.ToString();

                    // 공장코드 변경시 그리드 데이터 모두 삭제
                    while (this.uGrid1.Rows.Count > 0)
                    {
                        this.uGrid1.Rows[0].Delete(false);
                    }

                    while (this.uGridInspectGrade.Rows.Count > 0)
                    {
                        this.uGridInspectGrade.Rows[0].Delete(false);
                    }
                    //this.uGrid1.DisplayLayout.Bands[0].Columns["InspectGroupCode"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;


                    // 검사분류
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                    QRPMAS.BL.MASQUA.InspectGroup clsIspectGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                    brwChannel.mfCredentials(clsIspectGroup);

                    dtInspectGroup = clsIspectGroup.mfReadMASInspectGroupCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //////Infragistics.Win.ValueList uValueList = new Infragistics.Win.ValueList();
                    //////uValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DisplayText;
                    //////uValueList.ValueListItems.Add("", "선택");
                    //////for (int i = 0; i < dtInspectGroup.Rows.Count; i++)
                    //////{
                    //////    uValueList.ValueListItems.Add(dtInspectGroup.Rows[i]["InspectGroupCode"].ToString(), dtInspectGroup.Rows[i]["InspectGroupName"].ToString());
                    //////}
                    //////uValueList.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;
                    //////uValueList.DropDownListAlignment = Infragistics.Win.DropDownListAlignment.Center;
                    ////////uValueList.DropDownResizeHandleStyleResolved = Infragistics.Win.DropDownResizeHandleStyle.Default;
                    //////// DropDown 리스트 각Item의 높이 설정
                    //////uValueList.ItemHeight = 15;
                    //////// Dropdown 리스트에 한번에 보여지는 최대 Item 갯수;
                    //////uValueList.MaxDropDownItems = 2;

                    //////this.uGrid1.DisplayLayout.Bands[0].Columns["InspectGroupCode"].ValueList = uValueList;

                    // 계측기
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                    QRPMAS.BL.MASQUA.MeasureTool clsMeasureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                    brwChannel.mfCredentials(clsMeasureTool);

                    dtMeasureTool = clsMeasureTool.mfReadMASMeasureToolCombo(strPlantCode, "", m_resSys.GetString("SYS_LANG"));

                    ////uValueList = new Infragistics.Win.ValueList();
                    ////uValueList.DisplayStyle = Infragistics.Win.ValueListDisplayStyle.DisplayText;
                    ////uValueList.ValueListItems.Add("", "선택");
                    ////for (int i = 0; i < dtMeasureTool.Rows.Count; i++)
                    ////{
                    ////    uValueList.ValueListItems.Add(dtMeasureTool.Rows[i]["MeasureToolCode"].ToString(), dtMeasureTool.Rows[i]["MeasureToolName"].ToString());
                    ////}
                    ////uValueList.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                    ////this.uGrid1.DisplayLayout.Bands[0].Columns["MeasureToolCode"].ValueList = uValueList;
                    ////    }
                    ////    else
                    ////    {
                    ////        // 공장콤보박스 다시 원래 값으로 돌리는 구문 추가...
                    ////    }
                    ////////}

                    // 검사항목 DropDown 설정
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(clsItem);

                    dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, string.Empty, string.Empty, m_resSys.GetString("SYS_LANG"));
                }

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtInspectGroup);
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "MeasureToolCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtMeasureTool);
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "InspectItemCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtItem);

                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtInspectGroup);
                wGrid.mfSetGridColumnValueList(this.uGridInspectGrade, 0, "InspectItemCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtItem);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자에서 엔터버튼 누를시 등록자명 가져오는 이벤트
        private void uTextCreateUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteUserID.Text == "")
                    {
                        this.uTextWriteUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strWriteID = this.uTextWriteUserID.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                            if (strRtnUserName == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextWriteUserID.Text = "";
                                this.uTextWriteUserName.Text = "";
                            }
                            else
                            {
                                this.uTextWriteUserName.Text = strRtnUserName;
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteUserID.Text.Length <= 1 || this.uTextWriteUserID.SelectedText == this.uTextWriteUserID.Text)
                    {
                        this.uTextWriteUserName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        ////// 등록자 코드 TextBox 를 벗어날때
        ////private void uTextCreateUserID_AfterExitEditMode(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        ////// SystemInfor ResourceSet
        ////        ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        ////        ////WinMessageBox msg = new WinMessageBox();

        ////        ////// 공장 선택 확인
        ////        ////if (this.uComboPlant.Value.ToString() == "")
        ////        ////{
        ////        ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////        ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////        ////                    "확인창", "입력확인", "공장을 선택해주세요.",
        ////        ////                    Infragistics.Win.HAlign.Right);

        ////        ////    this.uComboPlant.DropDown();
        ////        ////}
        ////        ////else
        ////        ////{
        ////        ////    String strPlantCode = this.uComboPlant.Value.ToString();
        ////        ////    String strWriteID = this.uTextWriteUserID.Text;

        ////        ////    // UserName 검색 함수 호출
        ////        ////    String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

        ////        ////    if (strRtnUserName == "")
        ////        ////    {
        ////        ////        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////        ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////        ////                    "확인창", "입력확인", "사용자를 찾을수 없습니다",
        ////        ////                    Infragistics.Win.HAlign.Right);

        ////        ////        this.uTextWriteUserID.Text = "";
        ////        ////        this.uTextWriteUserName.Text = "";
        ////        ////    }
        ////        ////    else
        ////        ////    {
        ////        ////        this.uTextWriteUserName.Text = strRtnUserName;
        ////        ////    }
        ////        ////}
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        // 자재코드 TextBox 에서 엔터버튼 누를시 자재명 가져오는 이벤트
        private void uTextMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextMaterialCode.Text == "")
                    {
                        this.uTextMaterialName.Text = "";
                        this.uTextMaterialGroup.Text = "";
                        this.uTextMaterialType.Text = "";
                        this.uTextSpec.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strMaterialCode = this.uTextMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                    this.uTextMaterialGroup.Text = dtMaterial.Rows[i]["MaterialGroupName"].ToString();
                                    this.uTextMaterialType.Text = dtMaterial.Rows[i]["MaterialTypeName"].ToString();
                                    this.uTextSpec.Text = dtMaterial.Rows[i]["Spec"].ToString();
                                }
                                // 등록된 표준번호가 존재하는지 검사

                                CheckStdNumber();
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextMaterialCode.Text = "";
                                this.uTextMaterialName.Text = "";
                                this.uTextMaterialGroup.Text = "";
                                this.uTextMaterialType.Text = "";
                                this.uTextSpec.Text = "";
                            }
                        }
                    }
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextMaterialCode.Text.Length <= 1 || this.uTextMaterialCode.SelectedText == this.uTextMaterialCode.Text)
                    {
                        this.uTextMaterialCode.Text = "";
                        this.uTextMaterialName.Text = "";
                        this.uTextMaterialGroup.Text = "";
                        this.uTextMaterialType.Text = "";
                        this.uTextSpec.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재텍스트박스에서 엔터키 입력시 자재명 가져오는 이벤트
        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text == "")
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        // 공장 선택 확인
                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            // UserName 검색 함수 호출
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000971",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialCode.Text = "";
                                this.uTextSearchMaterialName.Text = "";
                            }
                        }
                    }
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.Text.Length <= 1 || this.uTextSearchMaterialCode.SelectedText == this.uTextSearchMaterialCode.Text)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        ////// 검색 거래처코드가 공백일때 거래처명 공백으로 처리
        ////private void uTextSearchVendorCode_AfterExitEditMode(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        if (this.uTextSearchVendorCode.Text == "")
        ////        {
        ////            this.uTextSearchVendorName.Text = "";
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        ////private void uTextSearchMaterialCode_AfterExitEditMode(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        if (this.uTextSearchMaterialCode.Text == "")
        ////        {
        ////            this.uTextSearchMaterialName.Text = "";
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        ////// 자재코드 TextBox를 벗어날때
        ////private void uTextMaterialCode_AfterExitEditMode(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        if (this.uTextMaterialCode.Text == "")
        ////        {
        ////            this.uTextMaterialName.Text = "";
        ////            this.uTextMaterialGroup.Text = "";
        ////            this.uTextMaterialType.Text = "";
        ////            this.uTextSpec.Text = "";
        ////        }

        ////        ////// SystemInfor ResourceSet
        ////        ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        ////        ////WinMessageBox msg = new WinMessageBox();

        ////        ////// 공장 선택 확인
        ////        ////if (this.uComboPlant.Value.ToString() == "")
        ////        ////{
        ////        ////    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////        ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////        ////                    "확인창", "입력확인", "공장을 선택해주세요.",
        ////        ////                    Infragistics.Win.HAlign.Right);

        ////        ////    this.uComboPlant.DropDown();
        ////        ////}
        ////        ////else
        ////        ////{
        ////        ////    String strPlantCode = this.uComboPlant.Value.ToString();
        ////        ////    String strMaterialCode = this.uTextMaterialCode.Text;

        ////        ////    // UserName 검색 함수 호출
        ////        ////    DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

        ////        ////    if (dtMaterial.Rows.Count > 0)
        ////        ////    {
        ////        ////        for (int i = 0; i < dtMaterial.Rows.Count; i++)
        ////        ////        {
        ////        ////            this.uTextMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
        ////        ////            this.uTextMaterialGroup.Text = dtMaterial.Rows[i]["MaterialGroupName"].ToString();
        ////        ////            this.uTextMaterialType.Text = dtMaterial.Rows[i]["MaterialTypeName"].ToString();
        ////        ////            this.uTextSpec.Text = dtMaterial.Rows[i]["Spec"].ToString();
        ////        ////        }
        ////        ////        // 등록된 표준번호가 존재하는지 검사

        ////        ////        Boolean bolcheck = CheckStdNumber();
        ////        ////    }
        ////        ////    else
        ////        ////    {
        ////        ////        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////        ////                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////        ////                    "확인창", "입력확인", "자재정보를 찾을수 없습니다",
        ////        ////                    Infragistics.Win.HAlign.Right);

        ////        ////        this.uTextMaterialCode.Text = "";
        ////        ////        this.uTextMaterialName.Text = "";
        ////        ////        this.uTextMaterialGroup.Text = "";
        ////        ////        this.uTextMaterialType.Text = "";
        ////        ////        this.uTextSpec.Text = "";
        ////        ////    }
        ////        ////}
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        // 헤더 그리드 더블클릭 이벤트
        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 클릭된 행 고정
                e.Row.Fixed = true;

                // 검색조건 변수
                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strFullStdNumber = e.Row.Cells["StdNumber"].Value.ToString();

                if (strFullStdNumber.Length > 12)
                {
                    // 헤더 상세정보 조회
                    Search_HeaderD(strPlantCode, strFullStdNumber);

                    // 상세정보 조회
                    Search_Detail(strPlantCode, strFullStdNumber);

                    // 등급정보 조회
                    Search_Grade(strPlantCode, strFullStdNumber);

                    // PK 편집불가 상태로
                    this.uComboPlant.Enabled = false;
                    this.uTextSpecNo.Enabled = false;
                }
                else
                {
                    this.uComboPlant.Enabled = false;
                    this.uComboPlant.Value = strPlantCode;
                    this.uTextMaterialCode.Text = e.Row.Cells["MaterialCode"].Value.ToString();
                    this.uTextMaterialName.Text = e.Row.Cells["Materialname"].Value.ToString();
                    this.uDateCreateDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                    this.uTextWriteUserID.Text = m_resSys.GetString("SYS_USERID");
                    this.uTextWriteUserName.Text = m_resSys.GetString("SYS_USERNAME");
                    this.uTextConsumableType.Text = e.Row.Cells["ConsumableTypeCode"].Value.ToString();
                    this.uTextSpec.Text = e.Row.Cells["Spec"].Value.ToString();
                }

                // ContentsArea 펼침 상태로
                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자재텍스트박스 Edit버튼 클릭 이벤트
        private void uTextMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextMaterialName.Text = frmPOP.MaterialName;
                this.uTextMaterialGroup.Text = frmPOP.MaterialGroupName;
                this.uTextMaterialType.Text = frmPOP.MaterialTypeName;
                this.uTextSpec.Text = frmPOP.Spec;
                this.uComboPlant.Value = frmPOP.PlantCode;

                CheckStdNumber();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색조건 자재텍스트 박스 Edit버튼 클릭 이벤트
        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty) || uComboSearchPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.PlantCode = uComboSearchPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;
                this.uComboSearchPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자 텍스트 박스 버튼 클릭시 팝업창 띄우는 이벤트
        private void uTextWriteUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                this.uTextWriteUserID.Text = frmPOP.UserID;
                this.uTextWriteUserName.Text = frmPOP.UserName;
                this.uComboPlant.Value = frmPOP.PlantCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장 선택 안됐을때 상세 그리드의 드랍다운 리스트를 누르면 공장선택하라고 메세지 박스 띄우는 이벤트
        private void uGrid1_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (e.Cell.Column.Key == "InspectGroupCode")
                {
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uComboPlant.DropDown();
                    }
                }
                else if (e.Cell.Column.Key == "InspectTypeCode")
                {
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uComboPlant.DropDown();
                    }
                    else if (e.Cell.Row.Cells["InspectGroupCode"].Value.ToString() == "")
                    {
                        int intSeq = e.Cell.Row.Index;
                        SetInspectGroupCode1(intSeq);

                        this.uGrid1.ActiveCell = e.Cell;
                        this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        //Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //    , "M001264", "M000962", "M000178", Infragistics.Win.HAlign.Right);

                        //this.uGrid1.ActiveCell = e.Cell.Row.Cells["InspectGroupCode"];
                        //this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                }
                else if (e.Cell.Column.Key == "InspectItemCode")
                    if (e.Cell.Column.Key == "InspectItemCode")
                    {
                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                            // Focus
                            this.uComboPlant.DropDown();
                        }
                        else if (e.Cell.Row.Cells["InspectGroupCode"].Value.ToString() == "")
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000962", "M000178", Infragistics.Win.HAlign.Right);

                            this.uGrid1.ActiveCell = e.Cell.Row.Cells["InspectGroupCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        }
                        else if (e.Cell.Row.Cells["InspectTypeCode"].Value.ToString() == "")
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M000962", "M000184", Infragistics.Win.HAlign.Right);

                            this.uGrid1.ActiveCell = e.Cell.Row.Cells["InspectTypeCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        }
                    }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등급 그리드 셀 업데이트 이벤트
        private void uGridInspectGrade_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 검사유형 DropDown 설정
                if (e.Cell.Column.Key == "InspectGroupCode")
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinGrid wGrid = new WinGrid();

                    // 변수
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(clsIType);

                    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));

                    wGrid.mfSetGridCellValueList(this.uGridInspectGrade, e.Cell.Row.Index, "InspectTypeCode", "", "선택", dtInspectType);

                    //e.Cell.Row.Cells["InspectTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등급 그리드 DropDown List 선택시 이벤트
        private void uGridInspectGrade_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (e.Cell.Column.Key == "InspectGroupCode")
                {
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uComboPlant.DropDown();
                    }
                }
                else if (e.Cell.Column.Key == "InspectTypeCode")
                {
                    int intSeq = e.Cell.Row.Index;
                    SetInspectGroupCode2(intSeq);

                    this.uGridInspectGrade.ActiveCell = e.Cell;
                    this.uGridInspectGrade.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    //if (this.uComboPlant.Value.ToString() == "")
                    //{
                    //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //        , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                    //    // Focus
                    //    this.uComboPlant.DropDown();
                    //}
                    //else if (e.Cell.Row.Cells["InspectGroupCode"].Value.ToString() == "")
                    //{
                    //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //        , "M001264", "M000962", "M000178", Infragistics.Win.HAlign.Right);

                    //    this.uGridInspectGrade.ActiveCell = e.Cell.Row.Cells["InspectGroupCode"];
                    //    this.uGridInspectGrade.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    //}
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridInspectGrade_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                DialogResult Result = new DialogResult();

                if (this.uTextMaterialCode.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000962", "M000975", Infragistics.Win.HAlign.Right);
                    this.uTextMaterialCode.Focus();
                    return;
                }

                if (this.uTextSpecNo.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000962", "M000122", Infragistics.Win.HAlign.Right);
                    this.uTextSpecNo.Focus();
                    return;
                }

                // RowSelector Image 설정
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0006D_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        // 수입검사 규격서 탭 복사버튼
        private void uButtonCopy_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                DialogResult Result = new DialogResult();

                if (this.uTextMaterialCode.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000962", "M000975", Infragistics.Win.HAlign.Right);
                    this.uTextMaterialCode.Focus();
                    return;
                }

                if (this.uTextSpecNo.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000962", "M000122", Infragistics.Win.HAlign.Right);
                    this.uTextSpecNo.Focus();
                    return;
                }

                frmPOP0013 frmPOP = new frmPOP0013();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                DataTable dtSpecD = frmPOP.dtRtn;

                //this.uGrid1.EventManager.AllEventsEnabled = false;

                if (dtSpecD.Rows.Count.Equals(0))
                {
                    return;
                }
                else
                {
                    m_bolCopy = true;

                    for (int i = 0; i < dtSpecD.Rows.Count; i++)
                    {
                        uGrid1.DisplayLayout.Bands[0].AddNew();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["InspectGroupCode"].Value = dtSpecD.Rows[i]["InspectGroupCode"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["InspectTypeCode"].Value = dtSpecD.Rows[i]["InspectTypeCode"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["InspectItemCode"].Value = dtSpecD.Rows[i]["InspectItemCode"].ToString();
                        if (!dtSpecD.Rows[i]["LowerSpec"].ToString().Equals(string.Empty))
                            uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["LowerSpec"].Value = Convert.ToDecimal(dtSpecD.Rows[i]["LowerSpec"].ToString());
                        if (!dtSpecD.Rows[i]["UpperSpec"].ToString().Equals(string.Empty))
                            uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["UpperSpec"].Value = Convert.ToDecimal(dtSpecD.Rows[i]["UpperSpec"].ToString());
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["SpecRange"].Value = dtSpecD.Rows[i]["SpecRange"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["SpecUnitCode"].Value = dtSpecD.Rows[i]["SpecUnitCode"].ToString();
                        if (!dtSpecD.Rows[i]["Point"].ToString().Equals(string.Empty))
                            uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["Point"].Value = Convert.ToInt32(dtSpecD.Rows[i]["Point"].ToString());
                        if (!dtSpecD.Rows[i]["SampleSize"].ToString().Equals(string.Empty))
                            uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["SampleSize"].Value = Convert.ToDouble(dtSpecD.Rows[i]["SampleSize"].ToString());
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["UnitCode"].Value = dtSpecD.Rows[i]["UnitCode"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["CompareFlag"].Value = dtSpecD.Rows[i]["CompareFlag"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["DataType"].Value = dtSpecD.Rows[i]["DataType"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["EtcDesc"].Value = dtSpecD.Rows[i]["EtcDesc"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["InspectCondition"].Value = dtSpecD.Rows[i]["InspectCondition"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["Method"].Value = dtSpecD.Rows[i]["Method"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["SpecDesc"].Value = dtSpecD.Rows[i]["SpecDesc"].ToString();
                        uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["MeasureToolCode"].Value = dtSpecD.Rows[i]["MeasureToolCode"].ToString();

                        for (int j = 0; j < this.uGrid1.Rows.Count; j++)
                        {
                            string strPk1 = this.uGrid1.Rows[j].Cells["InspectGroupCode"].Value.ToString();
                            string strPk2 = this.uGrid1.Rows[j].Cells["InspectTypeCode"].Value.ToString();
                            string strPk3 = this.uGrid1.Rows[j].Cells["InspectItemCode"].Value.ToString();
                            for (int k = 0; k < this.uGrid1.Rows.Count; k++)
                            {
                                if (this.uGrid1.Rows[k].Cells["InspectGroupCode"].Value.ToString().Equals(strPk1) &&
                                    this.uGrid1.Rows[k].Cells["InspectTypeCode"].Value.ToString().Equals(strPk2) &&
                                    this.uGrid1.Rows[k].Cells["InspectItemCode"].Value.ToString().Equals(strPk3) && j != k)
                                {
                                    string strLang = m_resSys.GetString("SYS_LANG");
                                    string strFont = m_resSys.GetString("SYS_FONTNAME");

                                    msg.mfSetMessageBox(MessageBoxType.Error, strFont, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001122", strLang)
                                        , this.uGrid1.Rows[k].Cells["InspectItemCode"].Text + msg.GetMessge_Text("M000192", strLang)
                                        , Infragistics.Win.HAlign.Right);

                                    this.uGrid1.Rows[k].Delete(false);
                                }
                            }
                        }
                    }
                    uGrid1.DisplayLayout.Bands[0].AddNew();
                }

                //this.uGrid1.EventManager.AllEventsEnabled = true;
                m_bolCopy = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등급별 S/S 정보 복사 버튼
        private void uButtonCopy_2_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0) || this.uComboPlant.SelectedIndex.Equals(-1))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                DialogResult Result = new DialogResult();

                if (this.uTextMaterialCode.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000962", "M000975", Infragistics.Win.HAlign.Right);
                    this.uTextMaterialCode.Focus();
                    return;
                }

                if (this.uTextSpecNo.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000962", "M000122", Infragistics.Win.HAlign.Right);
                    this.uTextSpecNo.Focus();
                    return;
                }

                frmPOP0013G frmPOP = new frmPOP0013G();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                string strPlantCode = frmPOP.PlantCode;
                string strFullStdNumber = frmPOP.StdNumber;

                //this.uComboPlant.Value = strPlantCode;

                if (!strPlantCode.Equals(string.Empty) && !strFullStdNumber.Equals(string.Empty))
                {
                    //상세정보 메소드 호출
                    Search_Grade(strPlantCode, strFullStdNumber);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // SpecNo POPUP 창
        private void uTextSpecNo_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (this.uComboPlant.Value.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextMaterialCode.Text.ToString() == "")
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000881", "M000975", Infragistics.Win.HAlign.Right);
                    this.uTextMaterialCode.Focus();
                    return;
                }
                else
                {
                    frmPOP0015 frmPOP = new frmPOP0015();
                    frmPOP.PlantCode = this.uComboPlant.Value.ToString();
                    frmPOP.MaterialCode = this.uTextMaterialCode.Text;
                    frmPOP.ShowDialog();

                    this.uComboPlant.Value = frmPOP.PlantCode;
                    this.uTextMaterialCode.Text = frmPOP.MaterialCode;
                    if (frmPOP.StdNumber == null)
                        this.uTextStdNumber.Text = "";
                    else
                        this.uTextStdNumber.Text = frmPOP.StdNumber + frmPOP.StdSeq;
                    this.uTextSpecNo.Text = frmPOP.SpecNo;

                    CheckStdNumber();
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 초도품기간 Numeric에디터 0버튼 클릭시 이벤트
        private void uNumInitPeriod_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Numeric 에디터 스핀버튼 이벤트
        private void uNumInitPeriod_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    this.uNumInitPeriod.Focus();
                    this.uNumInitPeriod.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.UpKeyAction, false, false);
                }
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem)
                {
                    this.uNumInitPeriod.Focus();
                    this.uNumInitPeriod.PerformAction(Infragistics.Win.UltraWinMaskedEdit.MaskedEditAction.DownKeyAction, false, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }

        /// <summary>
        /// Int형 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">int로 반환받을 값</param>
        /// <returns></returns>
        private int ReturnIntValue(string value)
        {
            int result = 0;
            if (int.TryParse(value, out result))
                return result;
            else
                return 0;
        }

        private void uGrid1_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            try
            {
                ////if (e.ErrorType.ToString().Equals("Data"))
                ////{
                ////    e.Cancel = true;
                ////    m_bolErr = true;
                ////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                ////    DialogResult Result = new DialogResult();
                ////    WinMessageBox msg = new WinMessageBox();

                ////    //e.DataErrorInfo.Row.Refresh(Infragistics.Win.UltraWinGrid.RefreshRow.ReloadData);

                ////    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////                        , "M001264", "M001122", e.DataErrorInfo.Row.Cells["InspectItemCode"].Text + "M000192", Infragistics.Win.HAlign.Right);

                ////    ////e.DataErrorInfo.Row.Cells["InspectItemCode"].SetValue(string.Empty, false);
                ////    ////e.DataErrorInfo.Row.Cells["InspectTypeCOde"].SetValue(string.Empty, false);
                ////    ////e.DataErrorInfo.Row.Cells["InspectGroupCode"].SetValue(string.Empty, false);
                ////    if (m_bolCopy.Equals(false))
                ////        e.DataErrorInfo.Row.Delete(false);

                ////}             
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0006D_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 규격번호란을 떠날 때 PK중복체크 처리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSpecNo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.uGrid1.Rows.Count == 0)
                    CheckStdNumber();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        private void uGrid1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["SpecRange"].Value != null)
                {
                    if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                    {
                        e.Row.Cells["UpperSpec"].Value = DBNull.Value;
                    }
                    else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                    {
                        e.Row.Cells["LowerSpec"].Value = DBNull.Value;
                    }
                    else if (e.Row.Cells["SpecRange"].Value.ToString().Equals(String.Empty))
                    {
                        if (ReturnDecimalValue(e.Row.Cells["LowerSpec"].Value.ToString()) == 0m && ReturnDecimalValue(e.Row.Cells["UpperSpec"].Value.ToString()) == 0m)
                        {
                            e.Row.Cells["UpperSpec"].Value = DBNull.Value;
                            e.Row.Cells["LowerSpec"].Value = DBNull.Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }
        /// <summary>
        /// 검사분류 수입검사로 고정, 검사구분 컬럼은 Hidden처리
        /// </summary>
        /// <param name="intSeq"></param>
        private void SetInspectGroupCode1(int intSeq)
        {
            try
            {
                this.uGrid1.Rows[intSeq].Cells["InspectGroupCode"].Value = "IG01";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void SetInspectGroupCode2(int intSeq)
        {
            try
            {
                this.uGridInspectGrade.Rows[intSeq].Cells["InspectGroupCode"].Value = "IG01";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
