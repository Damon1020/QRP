﻿namespace QRPISO.UI
{
    partial class frmISO0002
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0002));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchMiddleClassify = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMiddleClassify = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchLargeClassify = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchLargeClassify = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSmallClassifyNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSmallClassifyNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSmallClassifyNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSmallClassifyNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSmallClassifyName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSmallClassifyName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSmallClassify = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSmallClassifyCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMiddleClassify = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMiddleClassify = new Infragistics.Win.Misc.UltraLabel();
            this.uComboLargeClassify = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelLargeClassify = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddleClassify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeClassify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassifyNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassifyNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassifyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMiddleClassify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboLargeClassify)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMiddleClassify);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMiddleClassify);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLargeClassify);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLargeClassify);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchMiddleClassify
            // 
            this.uComboSearchMiddleClassify.Location = new System.Drawing.Point(628, 12);
            this.uComboSearchMiddleClassify.Name = "uComboSearchMiddleClassify";
            this.uComboSearchMiddleClassify.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchMiddleClassify.TabIndex = 5;
            this.uComboSearchMiddleClassify.Text = "ultraComboEditor1";
            // 
            // uLabelSearchMiddleClassify
            // 
            this.uLabelSearchMiddleClassify.Location = new System.Drawing.Point(524, 12);
            this.uLabelSearchMiddleClassify.Name = "uLabelSearchMiddleClassify";
            this.uLabelSearchMiddleClassify.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMiddleClassify.TabIndex = 4;
            this.uLabelSearchMiddleClassify.Text = "ultraLabel1";
            // 
            // uComboSearchLargeClassify
            // 
            this.uComboSearchLargeClassify.Location = new System.Drawing.Point(372, 12);
            this.uComboSearchLargeClassify.Name = "uComboSearchLargeClassify";
            this.uComboSearchLargeClassify.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchLargeClassify.TabIndex = 3;
            this.uComboSearchLargeClassify.Text = "ultraComboEditor1";
            this.uComboSearchLargeClassify.ValueChanged += new System.EventHandler(this.uComboSearchLargeClassify_ValueChanged);
            // 
            // uLabelSearchLargeClassify
            // 
            this.uLabelSearchLargeClassify.Location = new System.Drawing.Point(268, 12);
            this.uLabelSearchLargeClassify.Name = "uLabelSearchLargeClassify";
            this.uLabelSearchLargeClassify.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLargeClassify.TabIndex = 2;
            this.uLabelSearchLargeClassify.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance2;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 2;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSmallClassifyNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSmallClassifyNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSmallClassifyNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSmallClassifyNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSmallClassifyName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSmallClassifyName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSmallClassify);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSmallClassifyCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboMiddleClassify);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMiddleClassify);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboLargeClassify);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLargeClassify);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uComboPlant
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance15;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Location = new System.Drawing.Point(168, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 17;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(150, 20);
            this.uLabelPlant.TabIndex = 16;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(168, 84);
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(150, 21);
            this.uComboUseFlag.TabIndex = 15;
            this.uComboUseFlag.Text = "ultraComboEditor1";
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(12, 84);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(150, 20);
            this.uLabelUseFlag.TabIndex = 14;
            this.uLabelUseFlag.Text = "ultraLabel2";
            // 
            // uTextSmallClassifyNameEn
            // 
            this.uTextSmallClassifyNameEn.Location = new System.Drawing.Point(800, 60);
            this.uTextSmallClassifyNameEn.Name = "uTextSmallClassifyNameEn";
            this.uTextSmallClassifyNameEn.Size = new System.Drawing.Size(150, 21);
            this.uTextSmallClassifyNameEn.TabIndex = 11;
            // 
            // uLabelSmallClassifyNameEn
            // 
            this.uLabelSmallClassifyNameEn.Location = new System.Drawing.Point(644, 60);
            this.uLabelSmallClassifyNameEn.Name = "uLabelSmallClassifyNameEn";
            this.uLabelSmallClassifyNameEn.Size = new System.Drawing.Size(150, 20);
            this.uLabelSmallClassifyNameEn.TabIndex = 10;
            this.uLabelSmallClassifyNameEn.Text = "ultraLabel3";
            // 
            // uTextSmallClassifyNameCh
            // 
            this.uTextSmallClassifyNameCh.Location = new System.Drawing.Point(484, 60);
            this.uTextSmallClassifyNameCh.Name = "uTextSmallClassifyNameCh";
            this.uTextSmallClassifyNameCh.Size = new System.Drawing.Size(150, 21);
            this.uTextSmallClassifyNameCh.TabIndex = 9;
            // 
            // uLabelSmallClassifyNameCh
            // 
            this.uLabelSmallClassifyNameCh.Location = new System.Drawing.Point(328, 60);
            this.uLabelSmallClassifyNameCh.Name = "uLabelSmallClassifyNameCh";
            this.uLabelSmallClassifyNameCh.Size = new System.Drawing.Size(150, 20);
            this.uLabelSmallClassifyNameCh.TabIndex = 8;
            this.uLabelSmallClassifyNameCh.Text = "ultraLabel2";
            // 
            // uTextSmallClassifyName
            // 
            appearance16.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSmallClassifyName.Appearance = appearance16;
            this.uTextSmallClassifyName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSmallClassifyName.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uTextSmallClassifyName.Location = new System.Drawing.Point(168, 60);
            this.uTextSmallClassifyName.Name = "uTextSmallClassifyName";
            this.uTextSmallClassifyName.Size = new System.Drawing.Size(150, 19);
            this.uTextSmallClassifyName.TabIndex = 7;
            // 
            // uLabelSmallClassifyName
            // 
            this.uLabelSmallClassifyName.Location = new System.Drawing.Point(12, 60);
            this.uLabelSmallClassifyName.Name = "uLabelSmallClassifyName";
            this.uLabelSmallClassifyName.Size = new System.Drawing.Size(150, 20);
            this.uLabelSmallClassifyName.TabIndex = 6;
            this.uLabelSmallClassifyName.Text = "ultraLabel1";
            // 
            // uTextSmallClassify
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSmallClassify.Appearance = appearance14;
            this.uTextSmallClassify.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSmallClassify.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            this.uTextSmallClassify.Location = new System.Drawing.Point(168, 36);
            this.uTextSmallClassify.Name = "uTextSmallClassify";
            this.uTextSmallClassify.Size = new System.Drawing.Size(150, 19);
            this.uTextSmallClassify.TabIndex = 5;
            // 
            // uLabelSmallClassifyCode
            // 
            this.uLabelSmallClassifyCode.Location = new System.Drawing.Point(12, 36);
            this.uLabelSmallClassifyCode.Name = "uLabelSmallClassifyCode";
            this.uLabelSmallClassifyCode.Size = new System.Drawing.Size(150, 20);
            this.uLabelSmallClassifyCode.TabIndex = 4;
            this.uLabelSmallClassifyCode.Text = "ultraLabel1";
            // 
            // uComboMiddleClassify
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboMiddleClassify.Appearance = appearance18;
            this.uComboMiddleClassify.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboMiddleClassify.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboMiddleClassify.Location = new System.Drawing.Point(800, 12);
            this.uComboMiddleClassify.Name = "uComboMiddleClassify";
            this.uComboMiddleClassify.Size = new System.Drawing.Size(150, 21);
            this.uComboMiddleClassify.TabIndex = 3;
            this.uComboMiddleClassify.Text = "ultraComboEditor1";
            // 
            // uLabelMiddleClassify
            // 
            this.uLabelMiddleClassify.Location = new System.Drawing.Point(644, 12);
            this.uLabelMiddleClassify.Name = "uLabelMiddleClassify";
            this.uLabelMiddleClassify.Size = new System.Drawing.Size(150, 20);
            this.uLabelMiddleClassify.TabIndex = 2;
            this.uLabelMiddleClassify.Text = "ultraLabel1";
            // 
            // uComboLargeClassify
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboLargeClassify.Appearance = appearance17;
            this.uComboLargeClassify.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboLargeClassify.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboLargeClassify.Location = new System.Drawing.Point(484, 12);
            this.uComboLargeClassify.Name = "uComboLargeClassify";
            this.uComboLargeClassify.Size = new System.Drawing.Size(150, 21);
            this.uComboLargeClassify.TabIndex = 1;
            this.uComboLargeClassify.Text = "ultraComboEditor1";
            this.uComboLargeClassify.ValueChanged += new System.EventHandler(this.uComboLargeClassify_ValueChanged);
            // 
            // uLabelLargeClassify
            // 
            this.uLabelLargeClassify.Location = new System.Drawing.Point(328, 12);
            this.uLabelLargeClassify.Name = "uLabelLargeClassify";
            this.uLabelLargeClassify.Size = new System.Drawing.Size(150, 20);
            this.uLabelLargeClassify.TabIndex = 0;
            this.uLabelLargeClassify.Text = "ultraLabel1";
            // 
            // frmISO0002
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0002";
            this.Load += new System.EventHandler(this.frmISO0002_Load);
            this.Activated += new System.EventHandler(this.frmISO0002_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISO0002_FormClosing);
            this.Resize += new System.EventHandler(this.frmISO0002_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddleClassify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeClassify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassifyNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassifyNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassifyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmallClassify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMiddleClassify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboLargeClassify)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMiddleClassify;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMiddleClassify;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLargeClassify;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLargeClassify;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelLargeClassify;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSmallClassifyName;
        private Infragistics.Win.Misc.UltraLabel uLabelSmallClassifyName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSmallClassify;
        private Infragistics.Win.Misc.UltraLabel uLabelSmallClassifyCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMiddleClassify;
        private Infragistics.Win.Misc.UltraLabel uLabelMiddleClassify;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboLargeClassify;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSmallClassifyNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelSmallClassifyNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSmallClassifyNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelSmallClassifyNameCh;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
    }
}