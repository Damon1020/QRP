﻿namespace QRPISO.UI
{
    partial class frmISO0006D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0006D));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonCopy = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridInspectGrade = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonCopy_2 = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete_2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.RichTextEtc4 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc4 = new Infragistics.Win.Misc.UltraLabel();
            this.RichTextEtc3 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc3 = new Infragistics.Win.Misc.UltraLabel();
            this.RichTextEtc2 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc2 = new Infragistics.Win.Misc.UltraLabel();
            this.RichTextEtc1 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uComboInitPeriodUnitCode = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInitPeriodUnitCode = new Infragistics.Win.Misc.UltraLabel();
            this.uNumInitPeriod = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelInitPeriod = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCreateDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelConsumableType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextConsumableType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSpecNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpecNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialGroup = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchConsumableType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchConsumableType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInspectGrade)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInitPeriodUnitCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInitPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConsumableType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonCopy);
            this.ultraTabPageControl1.Controls.Add(this.uButtonDelete);
            this.ultraTabPageControl1.Controls.Add(this.uGrid1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1036, 506);
            // 
            // uButtonCopy
            // 
            this.uButtonCopy.Location = new System.Drawing.Point(108, 12);
            this.uButtonCopy.Name = "uButtonCopy";
            this.uButtonCopy.Size = new System.Drawing.Size(88, 28);
            this.uButtonCopy.TabIndex = 7;
            this.uButtonCopy.Text = "ultraButton1";
            this.uButtonCopy.Click += new System.EventHandler(this.uButtonCopy_Click);
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 12);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 6;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance13;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance29;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance42.BackColor2 = System.Drawing.SystemColors.Control;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance45;
            appearance46.BackColor = System.Drawing.SystemColors.Highlight;
            appearance46.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance46;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance48;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            appearance49.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance49;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance50.BackColor = System.Drawing.SystemColors.Control;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance50;
            appearance51.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance51;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance53;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance54;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 44);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1016, 456);
            this.uGrid1.TabIndex = 3;
            this.uGrid1.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGrid1_Error);
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGrid1_InitializeRow);
            this.uGrid1.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGrid1_BeforeCellListDropDown);
            this.uGrid1.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_CellChange);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGridInspectGrade);
            this.ultraTabPageControl3.Controls.Add(this.uButtonCopy_2);
            this.ultraTabPageControl3.Controls.Add(this.uButtonDelete_2);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1036, 506);
            // 
            // uGridInspectGrade
            // 
            this.uGridInspectGrade.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInspectGrade.DisplayLayout.Appearance = appearance20;
            this.uGridInspectGrade.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInspectGrade.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInspectGrade.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInspectGrade.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.uGridInspectGrade.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInspectGrade.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            this.uGridInspectGrade.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInspectGrade.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInspectGrade.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInspectGrade.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.uGridInspectGrade.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInspectGrade.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInspectGrade.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInspectGrade.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridInspectGrade.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInspectGrade.DisplayLayout.Override.CellPadding = 0;
            appearance25.BackColor = System.Drawing.SystemColors.Control;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInspectGrade.DisplayLayout.Override.GroupByRowAppearance = appearance25;
            appearance27.TextHAlignAsString = "Left";
            this.uGridInspectGrade.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.uGridInspectGrade.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInspectGrade.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            this.uGridInspectGrade.DisplayLayout.Override.RowAppearance = appearance26;
            this.uGridInspectGrade.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInspectGrade.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridInspectGrade.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInspectGrade.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInspectGrade.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInspectGrade.Location = new System.Drawing.Point(12, 44);
            this.uGridInspectGrade.Name = "uGridInspectGrade";
            this.uGridInspectGrade.Size = new System.Drawing.Size(1016, 456);
            this.uGridInspectGrade.TabIndex = 10;
            this.uGridInspectGrade.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridInspectGrade_AfterCellUpdate);
            this.uGridInspectGrade.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridInspectGrade_BeforeCellListDropDown);
            this.uGridInspectGrade.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridInspectGrade_CellChange);
            // 
            // uButtonCopy_2
            // 
            this.uButtonCopy_2.Location = new System.Drawing.Point(108, 12);
            this.uButtonCopy_2.Name = "uButtonCopy_2";
            this.uButtonCopy_2.Size = new System.Drawing.Size(88, 28);
            this.uButtonCopy_2.TabIndex = 9;
            this.uButtonCopy_2.Text = "ultraButton1";
            this.uButtonCopy_2.Click += new System.EventHandler(this.uButtonCopy_2_Click);
            // 
            // uButtonDelete_2
            // 
            this.uButtonDelete_2.Location = new System.Drawing.Point(12, 12);
            this.uButtonDelete_2.Name = "uButtonDelete_2";
            this.uButtonDelete_2.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete_2.TabIndex = 8;
            this.uButtonDelete_2.Text = "ultraButton1";
            this.uButtonDelete_2.Click += new System.EventHandler(this.uButtonDelete_2_Click);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc4);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc4);
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc3);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc3);
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc2);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc2);
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc1);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc1);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1036, 506);
            // 
            // RichTextEtc4
            // 
            this.RichTextEtc4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc4.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc4.Location = new System.Drawing.Point(524, 280);
            this.RichTextEtc4.Name = "RichTextEtc4";
            this.RichTextEtc4.Size = new System.Drawing.Size(505, 216);
            this.RichTextEtc4.TabIndex = 56;
            // 
            // uLabelEtc4
            // 
            this.uLabelEtc4.Location = new System.Drawing.Point(524, 256);
            this.uLabelEtc4.Name = "uLabelEtc4";
            this.uLabelEtc4.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc4.TabIndex = 55;
            this.uLabelEtc4.Text = "ultraLabel1";
            // 
            // RichTextEtc3
            // 
            this.RichTextEtc3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc3.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc3.Location = new System.Drawing.Point(524, 36);
            this.RichTextEtc3.Name = "RichTextEtc3";
            this.RichTextEtc3.Size = new System.Drawing.Size(505, 216);
            this.RichTextEtc3.TabIndex = 54;
            // 
            // uLabelEtc3
            // 
            this.uLabelEtc3.Location = new System.Drawing.Point(524, 12);
            this.uLabelEtc3.Name = "uLabelEtc3";
            this.uLabelEtc3.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc3.TabIndex = 53;
            this.uLabelEtc3.Text = "ultraLabel1";
            // 
            // RichTextEtc2
            // 
            this.RichTextEtc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc2.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc2.Location = new System.Drawing.Point(12, 280);
            this.RichTextEtc2.Name = "RichTextEtc2";
            this.RichTextEtc2.Size = new System.Drawing.Size(505, 216);
            this.RichTextEtc2.TabIndex = 48;
            // 
            // uLabelEtc2
            // 
            this.uLabelEtc2.Location = new System.Drawing.Point(12, 256);
            this.uLabelEtc2.Name = "uLabelEtc2";
            this.uLabelEtc2.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc2.TabIndex = 47;
            this.uLabelEtc2.Text = "ultraLabel1";
            // 
            // RichTextEtc1
            // 
            this.RichTextEtc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc1.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc1.Location = new System.Drawing.Point(12, 36);
            this.RichTextEtc1.Name = "RichTextEtc1";
            this.RichTextEtc1.Size = new System.Drawing.Size(505, 216);
            this.RichTextEtc1.TabIndex = 46;
            // 
            // uLabelEtc1
            // 
            this.uLabelEtc1.Location = new System.Drawing.Point(12, 12);
            this.uLabelEtc1.Name = "uLabelEtc1";
            this.uLabelEtc1.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc1.TabIndex = 45;
            this.uLabelEtc1.Text = "ultraLabel1";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 8;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboInitPeriodUnitCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInitPeriodUnitCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uNumInitPeriod);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInitPeriod);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStdNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uComboInitPeriodUnitCode
            // 
            this.uComboInitPeriodUnitCode.Location = new System.Drawing.Point(380, 60);
            this.uComboInitPeriodUnitCode.Name = "uComboInitPeriodUnitCode";
            this.uComboInitPeriodUnitCode.Size = new System.Drawing.Size(144, 21);
            this.uComboInitPeriodUnitCode.TabIndex = 79;
            this.uComboInitPeriodUnitCode.Text = "ultraComboEditor1";
            // 
            // uLabelInitPeriodUnitCode
            // 
            this.uLabelInitPeriodUnitCode.Location = new System.Drawing.Point(225, 59);
            this.uLabelInitPeriodUnitCode.Name = "uLabelInitPeriodUnitCode";
            this.uLabelInitPeriodUnitCode.Size = new System.Drawing.Size(151, 20);
            this.uLabelInitPeriodUnitCode.TabIndex = 78;
            this.uLabelInitPeriodUnitCode.Text = "ultraLabel2";
            // 
            // uNumInitPeriod
            // 
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Text = "0";
            this.uNumInitPeriod.ButtonsLeft.Add(editorButton1);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumInitPeriod.ButtonsRight.Add(spinEditorButton1);
            this.uNumInitPeriod.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uNumInitPeriod.Location = new System.Drawing.Point(116, 60);
            this.uNumInitPeriod.MaskInput = "nnnnn";
            this.uNumInitPeriod.Name = "uNumInitPeriod";
            this.uNumInitPeriod.PromptChar = ' ';
            this.uNumInitPeriod.Size = new System.Drawing.Size(100, 21);
            this.uNumInitPeriod.TabIndex = 77;
            this.uNumInitPeriod.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumInitPeriod_EditorSpinButtonClick);
            this.uNumInitPeriod.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumInitPeriod_EditorButtonClick);
            // 
            // uLabelInitPeriod
            // 
            this.uLabelInitPeriod.Location = new System.Drawing.Point(12, 59);
            this.uLabelInitPeriod.Name = "uLabelInitPeriod";
            this.uLabelInitPeriod.Size = new System.Drawing.Size(100, 20);
            this.uLabelInitPeriod.TabIndex = 76;
            this.uLabelInitPeriod.Text = "ultraLabel1";
            // 
            // uDateCreateDate
            // 
            this.uDateCreateDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCreateDate.Location = new System.Drawing.Point(696, 12);
            this.uDateCreateDate.Name = "uDateCreateDate";
            this.uDateCreateDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCreateDate.TabIndex = 5;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(592, 12);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateDate.TabIndex = 75;
            this.uLabelCreateDate.Text = "ultraLabel1";
            // 
            // uTextWriteUserName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Appearance = appearance3;
            this.uTextWriteUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteUserName.Location = new System.Drawing.Point(481, 12);
            this.uTextWriteUserName.Name = "uTextWriteUserName";
            this.uTextWriteUserName.ReadOnly = true;
            this.uTextWriteUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteUserName.TabIndex = 74;
            // 
            // uTextWriteUserID
            // 
            appearance7.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteUserID.Appearance = appearance7;
            this.uTextWriteUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance9.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance9;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteUserID.ButtonsRight.Add(editorButton2);
            this.uTextWriteUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteUserID.Location = new System.Drawing.Point(380, 12);
            this.uTextWriteUserID.Name = "uTextWriteUserID";
            this.uTextWriteUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteUserID.TabIndex = 4;
            this.uTextWriteUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCreateUserID_KeyDown);
            this.uTextWriteUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteUserID_EditorButtonClick);
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(276, 12);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateUser.TabIndex = 72;
            this.uLabelCreateUser.Text = "ultraLabel1";
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Controls.Add(this.ultraTabPageControl3);
            this.uTab.Location = new System.Drawing.Point(12, 160);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(1040, 532);
            this.uTab.TabIndex = 58;
            ultraTab3.Key = "Detail";
            ultraTab3.TabPage = this.ultraTabPageControl1;
            ultraTab3.Text = "수입검사규격서 상세정보";
            ultraTab1.Key = "Grade";
            ultraTab1.TabPage = this.ultraTabPageControl3;
            ultraTab1.Text = "등급별 S/S정보";
            ultraTab4.Key = "Etc";
            ultraTab4.TabPage = this.ultraTabPageControl2;
            ultraTab4.Text = "비고";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab3,
            ultraTab1,
            ultraTab4});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1036, 506);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uLabelConsumableType);
            this.uGroupBox2.Controls.Add(this.uTextConsumableType);
            this.uGroupBox2.Controls.Add(this.uTextSpecNo);
            this.uGroupBox2.Controls.Add(this.uLabelSpecNo);
            this.uGroupBox2.Controls.Add(this.uTextMaterialName);
            this.uGroupBox2.Controls.Add(this.uTextMaterialCode);
            this.uGroupBox2.Controls.Add(this.uComboPlant);
            this.uGroupBox2.Controls.Add(this.uTextSpec);
            this.uGroupBox2.Controls.Add(this.uLabelSpec);
            this.uGroupBox2.Controls.Add(this.uTextMaterialType);
            this.uGroupBox2.Controls.Add(this.uTextMaterialGroup);
            this.uGroupBox2.Controls.Add(this.uLabelMaterial);
            this.uGroupBox2.Controls.Add(this.uLabelPlant);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 88);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 64);
            this.uGroupBox2.TabIndex = 57;
            // 
            // uLabelConsumableType
            // 
            this.uLabelConsumableType.Location = new System.Drawing.Point(740, 36);
            this.uLabelConsumableType.Name = "uLabelConsumableType";
            this.uLabelConsumableType.Size = new System.Drawing.Size(100, 20);
            this.uLabelConsumableType.TabIndex = 80;
            this.uLabelConsumableType.Text = "ultraLabel1";
            // 
            // uTextConsumableType
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextConsumableType.Appearance = appearance15;
            this.uTextConsumableType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextConsumableType.Location = new System.Drawing.Point(844, 36);
            this.uTextConsumableType.Name = "uTextConsumableType";
            this.uTextConsumableType.ReadOnly = true;
            this.uTextConsumableType.Size = new System.Drawing.Size(187, 21);
            this.uTextConsumableType.TabIndex = 79;
            // 
            // uTextSpecNo
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSpecNo.Appearance = appearance4;
            this.uTextSpecNo.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextSpecNo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSpecNo.Location = new System.Drawing.Point(564, 36);
            this.uTextSpecNo.Name = "uTextSpecNo";
            this.uTextSpecNo.Size = new System.Drawing.Size(100, 21);
            this.uTextSpecNo.TabIndex = 78;
            this.uTextSpecNo.Leave += new System.EventHandler(this.uTextSpecNo_Leave);
            this.uTextSpecNo.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSpecNo_EditorButtonClick);
            // 
            // uLabelSpecNo
            // 
            this.uLabelSpecNo.Location = new System.Drawing.Point(460, 36);
            this.uLabelSpecNo.Name = "uLabelSpecNo";
            this.uLabelSpecNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpecNo.TabIndex = 77;
            this.uLabelSpecNo.Text = "ultraLabel1";
            // 
            // uTextMaterialName
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Appearance = appearance6;
            this.uTextMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialName.Location = new System.Drawing.Point(665, 12);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.ReadOnly = true;
            this.uTextMaterialName.Size = new System.Drawing.Size(367, 21);
            this.uTextMaterialName.TabIndex = 76;
            // 
            // uTextMaterialCode
            // 
            appearance41.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMaterialCode.Appearance = appearance41;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance44.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance44.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance44;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextMaterialCode.ButtonsRight.Add(editorButton3);
            this.uTextMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextMaterialCode.Location = new System.Drawing.Point(564, 12);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.Size = new System.Drawing.Size(100, 21);
            this.uTextMaterialCode.TabIndex = 8;
            this.uTextMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextMaterialCode_KeyDown);
            this.uTextMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextMaterialCode_EditorButtonClick);
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 6;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uTextSpec
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance5;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(116, 36);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(334, 21);
            this.uTextSpec.TabIndex = 45;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 36);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpec.TabIndex = 44;
            this.uLabelSpec.Text = "ultraLabel1";
            // 
            // uTextMaterialType
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialType.Appearance = appearance14;
            this.uTextMaterialType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialType.Location = new System.Drawing.Point(968, 36);
            this.uTextMaterialType.Name = "uTextMaterialType";
            this.uTextMaterialType.ReadOnly = true;
            this.uTextMaterialType.Size = new System.Drawing.Size(64, 21);
            this.uTextMaterialType.TabIndex = 43;
            // 
            // uTextMaterialGroup
            // 
            appearance52.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGroup.Appearance = appearance52;
            this.uTextMaterialGroup.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGroup.Location = new System.Drawing.Point(901, 36);
            this.uTextMaterialGroup.Name = "uTextMaterialGroup";
            this.uTextMaterialGroup.ReadOnly = true;
            this.uTextMaterialGroup.Size = new System.Drawing.Size(64, 21);
            this.uTextMaterialGroup.TabIndex = 41;
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(460, 12);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterial.TabIndex = 37;
            this.uLabelMaterial.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 33;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uTextEtc
            // 
            this.uTextEtc.Location = new System.Drawing.Point(116, 36);
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.Size = new System.Drawing.Size(928, 21);
            this.uTextEtc.TabIndex = 56;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 36);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc.TabIndex = 55;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uTextStdNumber
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance1;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(116, 12);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(150, 21);
            this.uTextStdNumber.TabIndex = 36;
            // 
            // uLabelStandardNo
            // 
            this.uLabelStandardNo.Location = new System.Drawing.Point(12, 12);
            this.uLabelStandardNo.Name = "uLabelStandardNo";
            this.uLabelStandardNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelStandardNo.TabIndex = 35;
            this.uLabelStandardNo.Text = "ultraLabel1";
            // 
            // uGrid
            // 
            this.uGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance47;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance31.BackColor2 = System.Drawing.SystemColors.Control;
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance31;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance32;
            appearance33.BackColor = System.Drawing.SystemColors.Highlight;
            appearance33.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance33;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance34;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            appearance35.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance35;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance39.BackColor = System.Drawing.SystemColors.Control;
            appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance39.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance39;
            appearance40.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Override.HeaderAppearance = appearance40;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid.Location = new System.Drawing.Point(0, 80);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1070, 760);
            this.uGrid.TabIndex = 7;
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance37;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // uComboSearchConsumableType
            // 
            this.uComboSearchConsumableType.Location = new System.Drawing.Point(388, 12);
            this.uComboSearchConsumableType.MaxLength = 50;
            this.uComboSearchConsumableType.Name = "uComboSearchConsumableType";
            this.uComboSearchConsumableType.Size = new System.Drawing.Size(141, 21);
            this.uComboSearchConsumableType.TabIndex = 8;
            // 
            // uLabelSearchConsumableType
            // 
            this.uLabelSearchConsumableType.Location = new System.Drawing.Point(284, 12);
            this.uLabelSearchConsumableType.Name = "uLabelSearchConsumableType";
            this.uLabelSearchConsumableType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchConsumableType.TabIndex = 9;
            this.uLabelSearchConsumableType.Text = "ultraLabel1";
            // 
            // uTextSearchMaterialCode
            // 
            appearance38.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance38.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance38;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton4);
            this.uTextSearchMaterialCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(652, 12);
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMaterialCode.TabIndex = 3;
            this.uTextSearchMaterialCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchMaterialCode_KeyDown);
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick);
            // 
            // uTextSearchMaterialName
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Appearance = appearance12;
            this.uTextSearchMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(754, 12);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.ReadOnly = true;
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMaterialName.TabIndex = 7;
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(548, 12);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterial.TabIndex = 4;
            this.uLabelSearchMaterial.Text = "ultraLabel2";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmISO0006D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0006D";
            this.Text = "frmISO0006D";
            this.Load += new System.EventHandler(this.frmISO0006D_Load);
            this.Activated += new System.EventHandler(this.frmISO0006D_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISO0006D_FormClosing);
            this.Resize += new System.EventHandler(this.frmISO0006D_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridInspectGrade)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInitPeriodUnitCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumInitPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextConsumableType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpecNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private QRPUserControl.RichTextEditor RichTextEtc4;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc4;
        private QRPUserControl.RichTextEditor RichTextEtc3;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc3;
        private QRPUserControl.RichTextEditor RichTextEtc2;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc2;
        private QRPUserControl.RichTextEditor RichTextEtc1;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCreateDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraButton uButtonCopy;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInspectGrade;
        private Infragistics.Win.Misc.UltraButton uButtonCopy_2;
        private Infragistics.Win.Misc.UltraButton uButtonDelete_2;
        private Infragistics.Win.Misc.UltraLabel uLabelSpecNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpecNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInitPeriodUnitCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInitPeriodUnitCode;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumInitPeriod;
        private Infragistics.Win.Misc.UltraLabel uLabelInitPeriod;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchConsumableType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchConsumableType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextConsumableType;
        private Infragistics.Win.Misc.UltraLabel uLabelConsumableType;
    }
}