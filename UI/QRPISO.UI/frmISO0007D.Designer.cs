﻿namespace QRPISO.UI
{
    partial class frmISO0007D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("Find");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0007D));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonCopy = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGridPRCDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.RichTextEtc4 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc4 = new Infragistics.Win.Misc.UltraLabel();
            this.RichTextEtc3 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc3 = new Infragistics.Win.Misc.UltraLabel();
            this.RichTextEtc2 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc2 = new Infragistics.Win.Misc.UltraLabel();
            this.RichTextEtc1 = new QRPUserControl.RichTextEditor();
            this.uLabelEtc1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uLabelStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.uGridPRCHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPacakge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPRCDetail)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPRCHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonCopy);
            this.ultraTabPageControl1.Controls.Add(this.uButtonDelete);
            this.ultraTabPageControl1.Controls.Add(this.uGridPRCDetail);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1036, 562);
            // 
            // uButtonCopy
            // 
            this.uButtonCopy.Location = new System.Drawing.Point(108, 12);
            this.uButtonCopy.Name = "uButtonCopy";
            this.uButtonCopy.Size = new System.Drawing.Size(88, 28);
            this.uButtonCopy.TabIndex = 7;
            this.uButtonCopy.Text = "ultraButton1";
            this.uButtonCopy.Click += new System.EventHandler(this.uButtonCopy_Click);
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 12);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 6;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uGridPRCDetail
            // 
            this.uGridPRCDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPRCDetail.DisplayLayout.Appearance = appearance1;
            this.uGridPRCDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPRCDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPRCDetail.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPRCDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridPRCDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPRCDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridPRCDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPRCDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPRCDetail.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPRCDetail.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridPRCDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPRCDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPRCDetail.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPRCDetail.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridPRCDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPRCDetail.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPRCDetail.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridPRCDetail.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridPRCDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPRCDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridPRCDetail.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridPRCDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPRCDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridPRCDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPRCDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPRCDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPRCDetail.Location = new System.Drawing.Point(12, 44);
            this.uGridPRCDetail.Name = "uGridPRCDetail";
            this.uGridPRCDetail.Size = new System.Drawing.Size(1016, 516);
            this.uGridPRCDetail.TabIndex = 3;
            this.uGridPRCDetail.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridPRCDetail_Error);
            this.uGridPRCDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPRCDetail_AfterCellUpdate);
            this.uGridPRCDetail.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.uGridPRCDetail_InitializeRow);
            this.uGridPRCDetail.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPRCDetail_AfterCellListCloseUp);
            this.uGridPRCDetail.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridPRCDetail_BeforeCellListDropDown);
            this.uGridPRCDetail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPRCDetail_CellChange);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc4);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc4);
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc3);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc3);
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc2);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc2);
            this.ultraTabPageControl2.Controls.Add(this.RichTextEtc1);
            this.ultraTabPageControl2.Controls.Add(this.uLabelEtc1);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1036, 562);
            // 
            // RichTextEtc4
            // 
            this.RichTextEtc4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc4.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc4.Location = new System.Drawing.Point(524, 300);
            this.RichTextEtc4.Name = "RichTextEtc4";
            this.RichTextEtc4.Size = new System.Drawing.Size(505, 232);
            this.RichTextEtc4.TabIndex = 56;
            // 
            // uLabelEtc4
            // 
            this.uLabelEtc4.Location = new System.Drawing.Point(524, 276);
            this.uLabelEtc4.Name = "uLabelEtc4";
            this.uLabelEtc4.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc4.TabIndex = 55;
            this.uLabelEtc4.Text = "ultraLabel1";
            // 
            // RichTextEtc3
            // 
            this.RichTextEtc3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc3.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc3.Location = new System.Drawing.Point(524, 36);
            this.RichTextEtc3.Name = "RichTextEtc3";
            this.RichTextEtc3.Size = new System.Drawing.Size(505, 232);
            this.RichTextEtc3.TabIndex = 54;
            // 
            // uLabelEtc3
            // 
            this.uLabelEtc3.Location = new System.Drawing.Point(524, 12);
            this.uLabelEtc3.Name = "uLabelEtc3";
            this.uLabelEtc3.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc3.TabIndex = 53;
            this.uLabelEtc3.Text = "ultraLabel1";
            // 
            // RichTextEtc2
            // 
            this.RichTextEtc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc2.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc2.Location = new System.Drawing.Point(12, 300);
            this.RichTextEtc2.Name = "RichTextEtc2";
            this.RichTextEtc2.Size = new System.Drawing.Size(505, 232);
            this.RichTextEtc2.TabIndex = 48;
            // 
            // uLabelEtc2
            // 
            this.uLabelEtc2.Location = new System.Drawing.Point(12, 276);
            this.uLabelEtc2.Name = "uLabelEtc2";
            this.uLabelEtc2.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc2.TabIndex = 47;
            this.uLabelEtc2.Text = "ultraLabel1";
            // 
            // RichTextEtc1
            // 
            this.RichTextEtc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextEtc1.FontSize = QRPUserControl.FontSize.Three;
            this.RichTextEtc1.Location = new System.Drawing.Point(12, 36);
            this.RichTextEtc1.Name = "RichTextEtc1";
            this.RichTextEtc1.Size = new System.Drawing.Size(505, 232);
            this.RichTextEtc1.TabIndex = 46;
            // 
            // uLabelEtc1
            // 
            this.uLabelEtc1.Location = new System.Drawing.Point(12, 12);
            this.uLabelEtc1.Name = "uLabelEtc1";
            this.uLabelEtc1.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc1.TabIndex = 45;
            this.uLabelEtc1.Text = "ultraLabel1";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 8;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCustomerName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCustomerCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCustomer);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStdNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextCustomerName
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance13;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(821, 13);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(100, 21);
            this.uTextCustomerName.TabIndex = 86;
            // 
            // uTextCustomerCode
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomerCode.Appearance = appearance14;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance15.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Key = "Find";
            this.uTextCustomerCode.ButtonsRight.Add(editorButton1);
            this.uTextCustomerCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCustomerCode.Location = new System.Drawing.Point(719, 13);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.Size = new System.Drawing.Size(100, 21);
            this.uTextCustomerCode.TabIndex = 85;
            this.uTextCustomerCode.ValueChanged += new System.EventHandler(this.uTextCustomerCode_ValueChanged);
            this.uTextCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCustomerCode_KeyDown);
            this.uTextCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCustomerCode_EditorButtonClick);
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(613, 13);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomer.TabIndex = 84;
            this.uLabelCustomer.Text = "ultraLabel1";
            // 
            // uComboPackage
            // 
            this.uComboPackage.Location = new System.Drawing.Point(719, 40);
            this.uComboPackage.Name = "uComboPackage";
            this.uComboPackage.Size = new System.Drawing.Size(202, 21);
            this.uComboPackage.TabIndex = 83;
            this.uComboPackage.Text = "ultraComboEditor2";
            this.uComboPackage.ValueChanged += new System.EventHandler(this.uComboPackage_ValueChanged);
            this.uComboPackage.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboPackage_BeforeDropDown);
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(436, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(128, 21);
            this.uComboPlant.TabIndex = 82;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(613, 40);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackage.TabIndex = 81;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(332, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 80;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(116, 68);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(744, 21);
            this.uTextEtcDesc.TabIndex = 78;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 68);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtcDesc.TabIndex = 77;
            this.uLabelEtcDesc.Text = "ultraLabel1";
            // 
            // uDateWriteDate
            // 
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(436, 40);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDate.TabIndex = 76;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(332, 40);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateDate.TabIndex = 75;
            this.uLabelCreateDate.Text = "ultraLabel1";
            // 
            // uTextWriteName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance16;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(217, 40);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteName.TabIndex = 74;
            // 
            // uTextWriteID
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteID.Appearance = appearance17;
            this.uTextWriteID.BackColor = System.Drawing.Color.PowderBlue;
            appearance18.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance18.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance18;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteID.ButtonsRight.Add(editorButton2);
            this.uTextWriteID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteID.Location = new System.Drawing.Point(116, 40);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteID.TabIndex = 73;
            this.uTextWriteID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteID_KeyDown);
            this.uTextWriteID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(12, 40);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateUser.TabIndex = 72;
            this.uLabelCreateUser.Text = "ultraLabel1";
            // 
            // uTextStdNumber
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance19;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(116, 12);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(150, 21);
            this.uTextStdNumber.TabIndex = 71;
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Location = new System.Drawing.Point(12, 100);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(1040, 588);
            this.uTab.TabIndex = 58;
            ultraTab1.Key = "Detail";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "공정검사규격서 상세정보";
            ultraTab2.Key = "Etc";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "비고";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1036, 562);
            // 
            // uLabelStandardNo
            // 
            this.uLabelStandardNo.Location = new System.Drawing.Point(12, 12);
            this.uLabelStandardNo.Name = "uLabelStandardNo";
            this.uLabelStandardNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelStandardNo.TabIndex = 35;
            this.uLabelStandardNo.Text = "ultraLabel1";
            // 
            // uGridPRCHeader
            // 
            this.uGridPRCHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPRCHeader.DisplayLayout.Appearance = appearance20;
            this.uGridPRCHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPRCHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPRCHeader.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPRCHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGridPRCHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPRCHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGridPRCHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPRCHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPRCHeader.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPRCHeader.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGridPRCHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPRCHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPRCHeader.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            appearance27.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPRCHeader.DisplayLayout.Override.CellAppearance = appearance27;
            this.uGridPRCHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPRCHeader.DisplayLayout.Override.CellPadding = 0;
            appearance28.BackColor = System.Drawing.SystemColors.Control;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPRCHeader.DisplayLayout.Override.GroupByRowAppearance = appearance28;
            appearance29.TextHAlignAsString = "Left";
            this.uGridPRCHeader.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGridPRCHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPRCHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.uGridPRCHeader.DisplayLayout.Override.RowAppearance = appearance30;
            this.uGridPRCHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPRCHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance31;
            this.uGridPRCHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPRCHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPRCHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPRCHeader.Location = new System.Drawing.Point(0, 80);
            this.uGridPRCHeader.Name = "uGridPRCHeader";
            this.uGridPRCHeader.Size = new System.Drawing.Size(1060, 760);
            this.uGridPRCHeader.TabIndex = 7;
            this.uGridPRCHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPRCHeader_DoubleClickRow);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance32;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPacakge);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // uTextSearchCustomerName
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance33;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(492, 12);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerName.TabIndex = 89;
            // 
            // uTextSearchCustomerCode
            // 
            appearance34.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance34;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomerCode.ButtonsRight.Add(editorButton3);
            this.uTextSearchCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchCustomerCode.Location = new System.Drawing.Point(390, 12);
            this.uTextSearchCustomerCode.Name = "uTextSearchCustomerCode";
            this.uTextSearchCustomerCode.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchCustomerCode.TabIndex = 88;
            this.uTextSearchCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchCustomerCode_KeyDown);
            this.uTextSearchCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomerCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(284, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 87;
            this.uLabelSearchCustomer.Text = "ultraLabel2";
            // 
            // uComboSearchPacakge
            // 
            this.uComboSearchPacakge.Location = new System.Drawing.Point(716, 12);
            this.uComboSearchPacakge.Name = "uComboSearchPacakge";
            this.uComboSearchPacakge.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPacakge.TabIndex = 16;
            this.uComboSearchPacakge.Text = "ultraComboEditor2";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(612, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 15;
            this.uLabelSearchPackage.Text = "ultraLabel2";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmISO0007D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridPRCHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0007D";
            this.Text = "frmISO0007D";
            this.Load += new System.EventHandler(this.frmISO0007D_Load);
            this.Activated += new System.EventHandler(this.frmISO0007D_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISO0007D_FormClosing);
            this.Resize += new System.EventHandler(this.frmISO0007D_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPRCDetail)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPRCHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPRCDetail;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private QRPUserControl.RichTextEditor RichTextEtc4;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc4;
        private QRPUserControl.RichTextEditor RichTextEtc3;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc3;
        private QRPUserControl.RichTextEditor RichTextEtc2;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc2;
        private QRPUserControl.RichTextEditor RichTextEtc1;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc1;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPRCHeader;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraButton uButtonCopy;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPacakge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
    }
}