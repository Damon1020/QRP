﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서관리                                          */
/* 프로그램ID   : frmISOZ0001A.cs                                       */
/* 프로그램명   : 표준문서 배포승인                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPISO.UI
{
    public partial class frmISOZ0001A : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 

        QRPGlobal SysRes = new QRPGlobal();

        public frmISOZ0001A()
        {
            InitializeComponent();
        }

        private void frmISOZ0001A_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISOZ0001A_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 배포승인", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Contorol 초기화
            SetToolAuth();
            InitLabel();
            InitComboBox();
            initValue();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }


        #region 컨트롤 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void initValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //this.uDateSearchDistributeRequestFromDate.Value = DateTime.Now.AddDays(-7);
                //this.uDateSearchDistributeRequestToDate.Value = DateTime.Now;
                //this.uTextSearchDistributeCompanyID.Text = "";
                //this.uTextSearchDistributeCompanyName.Text = "";
                //this.uTextSearchStandardNum.Text = "";
                //this.uTextSearchTitle.Text = "";
                //this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE"); // "";

                this.uTextSearchDistributeCompanyID.MaxLength = 20;
                this.uTextSearchStandardNum.MaxLength = 100;


                this.uComboSearchProcess.Visible = false;
                this.uComboSearchProcess.Value = "";
                this.uTextDistributeRequestUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextDept.Text = m_resSys.GetString("SYS_DEPTNAME");
                this.uTextDistributeRequestUserName.Text = m_resSys.GetString("SYS_USERNAME");

                if (this.uGrid1.Rows.Count > 0)
                {
                    while (this.uGrid1.Rows.Count > 0)
                    {
                        this.uGrid1.Rows[0].Delete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNum, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDistributeCompany, "배포업체", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDistributeRequestDate, "배포요청일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelDistributeRequestUser, "배포승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDistributeDate, "배포승인일", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // Search Process ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo("", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false"); 

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqNo", "배포관리번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqSeq", "배포순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqDate", "배포요청일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqUserID", "배포요청자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqUserName", "배포요청자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
 
                wGrid.mfSetGridColumn(this.uGrid1, 0, "AdmitVersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "VersionNum", "이전개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistDeptCode", "배포부서코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistDeptName", "배포부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistNumber", "배포부수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistVendorCode", "배포업체코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistVendorName", "배포업체", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ReqDesc", "요청사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //// 공백줄 추가
                //wGrid.mfAddRowGrid(this.uGrid1, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                QRPISO.BL.ISODOC.ISODocDistReqD ReqD;
               
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistReqD), "ISODocDistReqD");
                    ReqD = new QRPISO.BL.ISODOC.ISODocDistReqD();
                    brwChannel.mfCredentials(ReqD);
            
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strStdNumber = this.uTextSearchStandardNum.Text;
                String strProcessCode = this.uComboSearchProcess.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;
                String strDistVendorCode = this.uTextSearchDistributeCompanyID.Text;
                String strDistReqDateFrom = Convert.ToDateTime(this.uDateSearchDistributeRequestFromDate.Value).ToString("yyyy-MM-dd");
                String strDistReqDateTo = Convert.ToDateTime(this.uDateSearchDistributeRequestToDate.Value).ToString("yyyy-MM-dd");


                DataTable dt = ReqD.mfReadISODocDistReqD(strPlantCode, strStdNumber, strProcessCode, strDocTitle, strDistVendorCode
                                                            , strDistReqDateFrom, strDistReqDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                this.uTextDistributeRequestUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextDistributeRequestUserName.Text = m_resSys.GetString("SYS_USERNAME");
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }

                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uTextDistributeRequestUserID.Text.Equals("") || this.uTextDistributeRequestUserName.Text.Equals(""))
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                , "M001135", "M001037", "M000770", Infragistics.Win.HAlign.Right);

                    this.uTextDistributeRequestUserID.Focus();
                    return;
                }
                else
                {
                    DataTable dtISODocDistReqD = new DataTable();
                    DataTable dtSaveCollect = new DataTable();
                    DataRow row;

                    QRPBrowser brwChannel = new QRPBrowser();
                    QRPISO.BL.ISODOC.ISODocDistReqD ReqD = new QRPISO.BL.ISODOC.ISODocDistReqD();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistReqD), "ISODocDistReqD");
                    brwChannel.mfCredentials(ReqD);

                    dtISODocDistReqD = ReqD.mfSetDataInfo();
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        //if (this.uGrid1.Rows[i].Cells["Check"].Value.ToString() == "true")
                        {
                            row = dtISODocDistReqD.NewRow();

                            row["PlantCode"] = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DistReqNo"] = this.uGrid1.Rows[i].Cells["DistReqNo"].Value.ToString();
                            row["DistReqSeq"] = this.uGrid1.Rows[i].Cells["DistReqSeq"].Value;
                            row["StdNumber"] = this.uGrid1.Rows[i].Cells["StdNumber"].Value.ToString();
                            row["VersionNum"] = this.uGrid1.Rows[i].Cells["VersionNum"].Value;
                            row["DistDeptCode"] = this.uGrid1.Rows[i].Cells["DistDeptCode"].Value.ToString();
                            row["DistNumber"] = this.uGrid1.Rows[i].Cells["DistNumber"].Value.ToString();
                            row["DistVendorCode"] = this.uGrid1.Rows[i].Cells["DistVendorCode"].Value.ToString();
                            row["ReqDesc"] = this.uGrid1.Rows[i].Cells["ReqDesc"].Value.ToString();
                            row["AdmitUserID"] = this.uTextDistributeRequestUserID.Text;
                            row["AdmitDate"] = this.uDateDistributeDate.Value.ToString();
                            row["AdmitStatus"] = "FN";
                            dtISODocDistReqD.Rows.Add(row);

                            //배포, 회수 정보

                            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistCollect), "ISODocDistCollect");
                            QRPISO.BL.ISODOC.ISODocDistCollect DCollect = new QRPISO.BL.ISODOC.ISODocDistCollect();
                            brwChannel.mfCredentials(DCollect);

                            dtSaveCollect = DCollect.mfSetDataInfo();
                            row = dtSaveCollect.NewRow();
                            row["PlantCode"] = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DistReqNo"] = this.uGrid1.Rows[i].Cells["DistReqNo"].Value.ToString();
                            row["DistReqSeq"] = this.uGrid1.Rows[i].Cells["DistReqSeq"].Value;
                            //row["Seq"] = this.uGrid1.Rows[i].RowSelectorNumber;
                            row["Number"] = 0;
                            dtSaveCollect.Rows.Add(row);
                        }
                    }
                    if (dtISODocDistReqD.Rows.Count > 0)
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000771", "M000934"
                                                    , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {

                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            //처리로직
                            string rtMSG = ReqD.mfSaveISODocDistReqDForApprove(dtISODocDistReqD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveCollect);

                            //decoding
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                            //처리로직 끝
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            //메시지 박스
                            System.Windows.Forms.DialogResult result;
                            if (ErrRtn.ErrNum == 0)
                            {
                                for (int i = 0; i < dtISODocDistReqD.Rows.Count; i++)
                                {
                                    if (!dtISODocDistReqD.Rows[i]["DistVendorCode"].ToString().Equals(string.Empty))
                                    {
                                        string strPlantCode = dtISODocDistReqD.Rows[i]["PlantCode"].ToString();
                                        string strStdNumber = dtISODocDistReqD.Rows[i]["StdNumber"].ToString();
                                        string strVersionNum = dtISODocDistReqD.Rows[i]["VersionNum"].ToString();
                                        string strDistVendorCode = dtISODocDistReqD.Rows[i]["DistVendorCode"].ToString();
                                        string strDistReqNo = dtISODocDistReqD.Rows[i]["DistReqNo"].ToString();
                                        SendMail(strPlantCode, strStdNumber, strVersionNum, strDistVendorCode, strDistReqNo);
                                    }
                                }

                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000927",
                                                       Infragistics.Win.HAlign.Right);

                                initValue();
                                mfSearch();
                            }

                            else
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000935",
                                                        Infragistics.Win.HAlign.Right);
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        private void uTextSearchDistributeCompanyID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //uTextSearchDistributeCompanyID
                frmPOP0004 frmVendor = new frmPOP0004();
                frmVendor.ShowDialog();
                this.uTextSearchDistributeCompanyID.Text = frmVendor.CustomerCode;
                this.uTextSearchDistributeCompanyName.Text = frmVendor.CustomerName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
            
        }

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {

            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                this.uComboSearchProcess.Items.Clear();

                strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                           , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                           , Infragistics.Win.HAlign.Center, "", "", "선택", "ProcessCode", "ProcessName", dtProcess);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void frmISOZ0001A_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void frmISOZ0001A_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBox1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextDistributeRequestUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                frmUser.ShowDialog();

                this.uTextDistributeRequestUserID.Text = frmUser.UserID;
                this.uTextDistributeRequestUserName.Text = frmUser.UserName;
                this.uTextDept.Text = frmUser.DeptName;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uTextDistributeRequestUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUsr = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUsr);

                    String strPlantCode = this.uComboSearchPlant.Value.ToString();
                    String strUserID = this.uTextDistributeRequestUserID.Text;

                    DataTable dtUsr = clsUsr.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUsr.Rows.Count > 0)
                    {
                        this.uTextDistributeRequestUserName.Text = dtUsr.Rows[0]["UserName"].ToString();
                        this.uTextDept.Text = dtUsr.Rows[0]["DeptName"].ToString();
                    }
                    else
                    {
                        DialogResult DResult = new DialogResult();
                        WinMessageBox msg = new WinMessageBox();
                        DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                    , "M001135", "M001115", "M000615", Infragistics.Win.HAlign.Left);
                    }
                }
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextDistributeRequestUserName.Text = "";
                    this.uTextDept.Text = "";
                    this.uTextDistributeRequestUserID.Text = "";
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //배포부수 1000단위 자릿수 표시
        private void uGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["DistNumber"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["DistNumber"].MaskInput = "nnn,nnn,nnn";
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        //배포업체 검색
        private void uTextSearchDistributeCompanyID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextSearchDistributeCompanyName.Text = "";
                    this.uTextSearchDistributeCompanyID.Text = "";
                }
            }
            catch(Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 메일 발송 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">표준문서 번호</param>
        /// <param name="strVersionNum">개정번호</param>
        /// <param name="strDistVendorCode">배포처코드</param>
        /// <param name="strDistReqNo">배포번호</param>
        private void SendMail(string strPlantCode, string strStdNumber, string strVersionNum, string strDistVendorCode, string strDistReqNo)
        {
            try
            {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    
                    //첨부문서를 위한 FileServer 연결정보
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //첨부화일 저장경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysTargetFilePath);
                    DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0006");

                    //메일 발송 파일 복사 경로
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFileDestPath);
                    DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                    //첨부할 파일 검색
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                    QRPISO.BL.ISODOC.ISODocFile clsFile = new QRPISO.BL.ISODOC.ISODocFile();
                    brwChannel.mfCredentials(clsFile);
                    DataTable dtFileAtt = clsFile.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));

                    QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                    ArrayList arrFile = new ArrayList();
                    ArrayList arrAttachFile = new ArrayList();
                    ArrayList arrFileNonPath = new ArrayList();
                    
                    //메일 주소 검색    DistVendorCode
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    DataTable dtVendor = clsVendor.mfReadVendorP(strDistVendorCode);

                    string strMailTitle = "표준문서 배포요청 최종 승인";
                    string strMailDesc = "표준번호 : " + strStdNumber + "/ 개정번호 : " + strVersionNum + "/ 배포번호 : " + strDistReqNo + "\r\n" + "배포요청이 최종 승인 돼었습니다.";

                    for(int i = 0; i < dtVendor.Rows.Count; i++)
                    {
                        for(int j = 0; j < dtFileAtt.Rows.Count; j++)
                        {
                            string strDestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + dtFileAtt.Rows[i]["FileName"].ToString();
                            string strTargetFIle = "ISODocFile/" + strStdNumber + "-" + strVersionNum + "/" + dtFileAtt.Rows[i]["FileName"].ToString();
                            string strNonPathFile = dtFileAtt.Rows[i]["FileName"].ToString();

                            arrAttachFile.Add(strTargetFIle);
                            arrFile.Add(strDestFile);
                            arrFileNonPath.Add(strNonPathFile);
                        }
                        fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                                            , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                            , arrAttachFile
                                                                            , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                                            , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                        QRPCOM.BL.Mail clsMail = new QRPCOM.BL.Mail();
                        brwChannel.mfCredentials(clsMail);

                        if (!dtVendor.Rows[i]["Email"].ToString().Equals(string.Empty))
                        {
                            if (arrFile.Count != 0)
                            {
                                fileAtt.mfFileUpload_NonAssync();
                            }
                            bool boolRtn = clsMail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_EMAIL"), m_resSys.GetString("SYS_USERNAME")
                            , dtVendor.Rows[i]["Email"].ToString(), strMailTitle, strMailDesc, arrFileNonPath);
                            if (!boolRtn)
                            {
                                WinMessageBox msg = new WinMessageBox();

                                msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M000083"
                                                                        , "M000365"
                                                                        , "M000404", Infragistics.Win.HAlign.Right);

                                return;

                            }
                        }
                    }
                }
            
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
