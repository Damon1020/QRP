﻿namespace QRPISO.UI
{
    partial class frmPOP0014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.uButtonSearch = new Infragistics.Win.Misc.UltraButton();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uGridList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPacakge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uGridSpecD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSpecD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // uButtonSearch
            // 
            this.uButtonSearch.Location = new System.Drawing.Point(949, 5);
            this.uButtonSearch.Name = "uButtonSearch";
            this.uButtonSearch.Size = new System.Drawing.Size(88, 28);
            this.uButtonSearch.TabIndex = 48;
            this.uButtonSearch.Text = "검색";
            this.uButtonSearch.Click += new System.EventHandler(this.uButtonSearch_Click);
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(859, 381);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 50;
            this.uButtonOK.Text = "확인";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(951, 381);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 49;
            this.uButtonClose.Text = "닫기";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uGridList
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridList.DisplayLayout.Appearance = appearance4;
            this.uGridList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGridList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridList.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.uGridList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridList.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridList.DisplayLayout.Override.CellAppearance = appearance5;
            this.uGridList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridList.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridList.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.uGridList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGridList.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGridList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridList.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.uGridList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridList.Location = new System.Drawing.Point(7, 41);
            this.uGridList.Name = "uGridList";
            this.uGridList.Size = new System.Drawing.Size(445, 336);
            this.uGridList.TabIndex = 47;
            this.uGridList.Text = "ultraGrid1";
            this.uGridList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridList_DoubleClickRow);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(115, 13);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(108, 21);
            this.uComboSearchPlant.TabIndex = 43;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(11, 13);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 42;
            this.uLabelSearchPlant.Text = "공장";
            // 
            // uComboSearchPacakge
            // 
            this.uComboSearchPacakge.Location = new System.Drawing.Point(640, 12);
            this.uComboSearchPacakge.Name = "uComboSearchPacakge";
            this.uComboSearchPacakge.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPacakge.TabIndex = 52;
            this.uComboSearchPacakge.Text = "ultraComboEditor2";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(536, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 51;
            this.uLabelSearchPackage.Text = "ultraLabel2";
            // 
            // uGridSpecD
            // 
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSpecD.DisplayLayout.Appearance = appearance16;
            this.uGridSpecD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSpecD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSpecD.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSpecD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridSpecD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSpecD.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.uGridSpecD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSpecD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSpecD.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSpecD.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridSpecD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSpecD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSpecD.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSpecD.DisplayLayout.Override.CellAppearance = appearance17;
            this.uGridSpecD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSpecD.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSpecD.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance23.TextHAlignAsString = "Left";
            this.uGridSpecD.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridSpecD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSpecD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.uGridSpecD.DisplayLayout.Override.RowAppearance = appearance22;
            this.uGridSpecD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSpecD.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.uGridSpecD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSpecD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSpecD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSpecD.Location = new System.Drawing.Point(456, 41);
            this.uGridSpecD.Name = "uGridSpecD";
            this.uGridSpecD.Size = new System.Drawing.Size(582, 336);
            this.uGridSpecD.TabIndex = 53;
            this.uGridSpecD.Text = "ultraGrid1";
            this.uGridSpecD.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGridSpecD_ClickCell);
            // 
            // uComboCustomer
            // 
            this.uComboCustomer.Location = new System.Drawing.Point(356, 12);
            this.uComboCustomer.Name = "uComboCustomer";
            this.uComboCustomer.Size = new System.Drawing.Size(144, 21);
            this.uComboCustomer.TabIndex = 55;
            this.uComboCustomer.Text = "ultraComboEditor2";
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(252, 12);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomer.TabIndex = 54;
            this.uLabelCustomer.Text = "ultraLabel2";
            // 
            // frmPOP0014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 414);
            this.Controls.Add(this.uComboCustomer);
            this.Controls.Add(this.uLabelCustomer);
            this.Controls.Add(this.uGridSpecD);
            this.Controls.Add(this.uComboSearchPacakge);
            this.Controls.Add(this.uLabelSearchPackage);
            this.Controls.Add(this.uButtonSearch);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uGridList);
            this.Controls.Add(this.uComboSearchPlant);
            this.Controls.Add(this.uLabelSearchPlant);
            this.Name = "frmPOP0014";
            this.Text = "공정검사규격서 조회";
            this.Load += new System.EventHandler(this.frmPOP0014_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSpecD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton uButtonSearch;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPacakge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSpecD;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
    }
}