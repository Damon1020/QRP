﻿namespace QRPISO.UI
{
    partial class frmISOZ0001D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISOZ0001D));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchDistributeRequestToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchDistributeRequestFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDistributeRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDistributeCompanyName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchDistributeCompanyID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDistributeCompany = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchStandardNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStandardNum = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextGRWComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRWComment = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCompleteDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextApplyToDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextApplyFromDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPadSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPadSize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChipSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChipSize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMakeDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakeUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMake = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid5 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDown = new Infragistics.Win.Misc.UltraButton();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCompleteDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelApplyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocKeyword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocKeyword = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApproveDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApproveDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApproveUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextApproveUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApproveUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextFrom = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFrom = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChangeItem = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChangeItem = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevDiscardReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevDiscardReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevDiscardDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevDiscardDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevDiscardUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRevDiscardUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevDiscardUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevDiscardDiv = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevDiscardDiv = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSmall = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMiddle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLarge = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevisionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStdDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStdDocNum = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).BeginInit();
            this.uGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDocNum)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchDistributeRequestToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchDistributeRequestFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDistributeRequestDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDistributeCompanyName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDistributeCompanyID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDistributeCompany);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextProcess);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uDateSearchDistributeRequestToDate
            // 
            this.uDateSearchDistributeRequestToDate.Location = new System.Drawing.Point(604, 36);
            this.uDateSearchDistributeRequestToDate.Name = "uDateSearchDistributeRequestToDate";
            this.uDateSearchDistributeRequestToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchDistributeRequestToDate.TabIndex = 24;
            // 
            // ultraLabel2
            // 
            appearance39.TextHAlignAsString = "Center";
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance39;
            this.ultraLabel2.Location = new System.Drawing.Point(588, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel2.TabIndex = 23;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchDistributeRequestFromDate
            // 
            this.uDateSearchDistributeRequestFromDate.Location = new System.Drawing.Point(484, 36);
            this.uDateSearchDistributeRequestFromDate.Name = "uDateSearchDistributeRequestFromDate";
            this.uDateSearchDistributeRequestFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchDistributeRequestFromDate.TabIndex = 22;
            // 
            // uLabelSearchDistributeRequestDate
            // 
            this.uLabelSearchDistributeRequestDate.Location = new System.Drawing.Point(380, 36);
            this.uLabelSearchDistributeRequestDate.Name = "uLabelSearchDistributeRequestDate";
            this.uLabelSearchDistributeRequestDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDistributeRequestDate.TabIndex = 21;
            this.uLabelSearchDistributeRequestDate.Text = "ultraLabel6";
            // 
            // uTextSearchDistributeCompanyName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDistributeCompanyName.Appearance = appearance4;
            this.uTextSearchDistributeCompanyName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDistributeCompanyName.Location = new System.Drawing.Point(218, 36);
            this.uTextSearchDistributeCompanyName.Name = "uTextSearchDistributeCompanyName";
            this.uTextSearchDistributeCompanyName.ReadOnly = true;
            this.uTextSearchDistributeCompanyName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDistributeCompanyName.TabIndex = 20;
            // 
            // uTextSearchDistributeCompanyID
            // 
            appearance3.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchDistributeCompanyID.ButtonsRight.Add(editorButton1);
            this.uTextSearchDistributeCompanyID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchDistributeCompanyID.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchDistributeCompanyID.Name = "uTextSearchDistributeCompanyID";
            this.uTextSearchDistributeCompanyID.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDistributeCompanyID.TabIndex = 19;
            this.uTextSearchDistributeCompanyID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchDistributeCompanyID_EditorButtonClick);
            // 
            // uLabelSearchDistributeCompany
            // 
            this.uLabelSearchDistributeCompany.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchDistributeCompany.Name = "uLabelSearchDistributeCompany";
            this.uLabelSearchDistributeCompany.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDistributeCompany.TabIndex = 18;
            this.uLabelSearchDistributeCompany.Text = "ultraLabel1";
            // 
            // uTextSearchTitle
            // 
            this.uTextSearchTitle.Location = new System.Drawing.Point(644, 12);
            this.uTextSearchTitle.Name = "uTextSearchTitle";
            this.uTextSearchTitle.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchTitle.TabIndex = 17;
            // 
            // uTextSearchStandardNum
            // 
            this.uTextSearchStandardNum.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchStandardNum.Name = "uTextSearchStandardNum";
            this.uTextSearchStandardNum.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchStandardNum.TabIndex = 16;
            // 
            // uLabelSearchTitle
            // 
            this.uLabelSearchTitle.Location = new System.Drawing.Point(540, 12);
            this.uLabelSearchTitle.Name = "uLabelSearchTitle";
            this.uLabelSearchTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTitle.TabIndex = 12;
            this.uLabelSearchTitle.Text = "ultraLabel1";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(908, 36);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchProcess.TabIndex = 11;
            this.uComboSearchProcess.Text = "ultraComboEditor6";
            // 
            // uLabelSearchStandardNum
            // 
            this.uLabelSearchStandardNum.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchStandardNum.Name = "uLabelSearchStandardNum";
            this.uLabelSearchStandardNum.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStandardNum.TabIndex = 8;
            this.uLabelSearchStandardNum.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uTextProcess
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Appearance = appearance38;
            this.uTextProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Location = new System.Drawing.Point(1040, 12);
            this.uTextProcess.Name = "uTextProcess";
            this.uTextProcess.ReadOnly = true;
            this.uTextProcess.Size = new System.Drawing.Size(18, 21);
            this.uTextProcess.TabIndex = 26;
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance95;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance96.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance96;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance97;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 720);
            this.uGrid1.TabIndex = 4;
            this.uGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid1_DoubleClickRow);
            this.uGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGrid1_InitializeLayout);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 5;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextGRWComment);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelGRWComment);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCompleteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApplyToDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApplyFromDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPadSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPadSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextChipSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelChipSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakeDeptName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakeUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMake);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox5);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCompleteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraLabel1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelApplyDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDocKeyword);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDocKeyword);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTitle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTitle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDocState);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDocState);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApproveDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelApproveDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApproveUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApproveUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelApproveUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextFrom);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFrom);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextChangeItem);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelChangeItem);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevDiscardReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevDiscardReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevDiscardDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevDiscardDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevDiscardUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevDiscardUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevDiscardUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevDiscardDiv);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevDiscardDiv);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSmall);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSmall);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMiddle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMiddle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLarge);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLarge);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevisionNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStdDocNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStdDocNum);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextGRWComment
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRWComment.Appearance = appearance19;
            this.uTextGRWComment.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRWComment.Location = new System.Drawing.Point(148, 202);
            this.uTextGRWComment.Name = "uTextGRWComment";
            this.uTextGRWComment.ReadOnly = true;
            this.uTextGRWComment.Size = new System.Drawing.Size(202, 21);
            this.uTextGRWComment.TabIndex = 126;
            // 
            // uLabelGRWComment
            // 
            this.uLabelGRWComment.Location = new System.Drawing.Point(12, 202);
            this.uLabelGRWComment.Name = "uLabelGRWComment";
            this.uLabelGRWComment.Size = new System.Drawing.Size(130, 20);
            this.uLabelGRWComment.TabIndex = 125;
            this.uLabelGRWComment.Text = "ultraLabel1";
            // 
            // uTextCompleteDate
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteDate.Appearance = appearance37;
            this.uTextCompleteDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCompleteDate.Location = new System.Drawing.Point(856, 108);
            this.uTextCompleteDate.Name = "uTextCompleteDate";
            this.uTextCompleteDate.ReadOnly = true;
            this.uTextCompleteDate.Size = new System.Drawing.Size(100, 21);
            this.uTextCompleteDate.TabIndex = 124;
            // 
            // uTextApplyToDate
            // 
            appearance91.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApplyToDate.Appearance = appearance91;
            this.uTextApplyToDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApplyToDate.Location = new System.Drawing.Point(268, 156);
            this.uTextApplyToDate.Name = "uTextApplyToDate";
            this.uTextApplyToDate.ReadOnly = true;
            this.uTextApplyToDate.Size = new System.Drawing.Size(100, 21);
            this.uTextApplyToDate.TabIndex = 123;
            // 
            // uTextApplyFromDate
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApplyFromDate.Appearance = appearance48;
            this.uTextApplyFromDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApplyFromDate.Location = new System.Drawing.Point(148, 156);
            this.uTextApplyFromDate.Name = "uTextApplyFromDate";
            this.uTextApplyFromDate.ReadOnly = true;
            this.uTextApplyFromDate.Size = new System.Drawing.Size(100, 21);
            this.uTextApplyFromDate.TabIndex = 122;
            // 
            // uTextPadSize
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPadSize.Appearance = appearance5;
            this.uTextPadSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPadSize.Location = new System.Drawing.Point(856, 224);
            this.uTextPadSize.Name = "uTextPadSize";
            this.uTextPadSize.ReadOnly = true;
            this.uTextPadSize.Size = new System.Drawing.Size(202, 21);
            this.uTextPadSize.TabIndex = 121;
            // 
            // uLabelPadSize
            // 
            this.uLabelPadSize.Location = new System.Drawing.Point(720, 224);
            this.uLabelPadSize.Name = "uLabelPadSize";
            this.uLabelPadSize.Size = new System.Drawing.Size(130, 20);
            this.uLabelPadSize.TabIndex = 120;
            this.uLabelPadSize.Text = "ultraLabel1";
            // 
            // uTextChipSize
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChipSize.Appearance = appearance14;
            this.uTextChipSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChipSize.Location = new System.Drawing.Point(512, 224);
            this.uTextChipSize.Name = "uTextChipSize";
            this.uTextChipSize.ReadOnly = true;
            this.uTextChipSize.Size = new System.Drawing.Size(202, 21);
            this.uTextChipSize.TabIndex = 119;
            // 
            // uLabelChipSize
            // 
            this.uLabelChipSize.Location = new System.Drawing.Point(376, 224);
            this.uLabelChipSize.Name = "uLabelChipSize";
            this.uLabelChipSize.Size = new System.Drawing.Size(130, 20);
            this.uLabelChipSize.TabIndex = 118;
            this.uLabelChipSize.Text = "ultraLabel1";
            // 
            // uTextPackage
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance15;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(148, 224);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(202, 21);
            this.uTextPackage.TabIndex = 117;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(12, 224);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(130, 20);
            this.uLabelPackage.TabIndex = 116;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uTextMakeDeptName
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeDeptName.Appearance = appearance46;
            this.uTextMakeDeptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeDeptName.Location = new System.Drawing.Point(957, 60);
            this.uTextMakeDeptName.Name = "uTextMakeDeptName";
            this.uTextMakeDeptName.ReadOnly = true;
            this.uTextMakeDeptName.Size = new System.Drawing.Size(100, 21);
            this.uTextMakeDeptName.TabIndex = 114;
            // 
            // uTextMakeUserName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeUserName.Appearance = appearance17;
            this.uTextMakeUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeUserName.Location = new System.Drawing.Point(856, 60);
            this.uTextMakeUserName.Name = "uTextMakeUserName";
            this.uTextMakeUserName.ReadOnly = true;
            this.uTextMakeUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextMakeUserName.TabIndex = 113;
            // 
            // uLabelMake
            // 
            this.uLabelMake.Location = new System.Drawing.Point(720, 60);
            this.uLabelMake.Name = "uLabelMake";
            this.uLabelMake.Size = new System.Drawing.Size(130, 20);
            this.uLabelMake.TabIndex = 112;
            this.uLabelMake.Text = "ultraLabel1";
            // 
            // uGroupBox5
            // 
            this.uGroupBox5.Controls.Add(this.uGrid5);
            this.uGroupBox5.Location = new System.Drawing.Point(1040, 172);
            this.uGroupBox5.Name = "uGroupBox5";
            this.uGroupBox5.Size = new System.Drawing.Size(36, 28);
            this.uGroupBox5.TabIndex = 75;
            this.uGroupBox5.Text = "ultraGroupBox1";
            this.uGroupBox5.Visible = false;
            // 
            // uGrid5
            // 
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            appearance79.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid5.DisplayLayout.Appearance = appearance79;
            this.uGrid5.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid5.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance80.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance80.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance80.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.GroupByBox.Appearance = appearance80;
            appearance81.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid5.DisplayLayout.GroupByBox.BandLabelAppearance = appearance81;
            this.uGrid5.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance82.BackColor2 = System.Drawing.SystemColors.Control;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance82.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid5.DisplayLayout.GroupByBox.PromptAppearance = appearance82;
            this.uGrid5.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid5.DisplayLayout.MaxRowScrollRegions = 1;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid5.DisplayLayout.Override.ActiveCellAppearance = appearance83;
            appearance84.BackColor = System.Drawing.SystemColors.Highlight;
            appearance84.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid5.DisplayLayout.Override.ActiveRowAppearance = appearance84;
            this.uGrid5.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid5.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.Override.CardAreaAppearance = appearance85;
            appearance86.BorderColor = System.Drawing.Color.Silver;
            appearance86.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid5.DisplayLayout.Override.CellAppearance = appearance86;
            this.uGrid5.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid5.DisplayLayout.Override.CellPadding = 0;
            appearance87.BackColor = System.Drawing.SystemColors.Control;
            appearance87.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance87.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance87.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance87.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid5.DisplayLayout.Override.GroupByRowAppearance = appearance87;
            appearance88.TextHAlignAsString = "Left";
            this.uGrid5.DisplayLayout.Override.HeaderAppearance = appearance88;
            this.uGrid5.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid5.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.BorderColor = System.Drawing.Color.Silver;
            this.uGrid5.DisplayLayout.Override.RowAppearance = appearance89;
            this.uGrid5.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid5.DisplayLayout.Override.TemplateAddRowAppearance = appearance90;
            this.uGrid5.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid5.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid5.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid5.Location = new System.Drawing.Point(12, 32);
            this.uGrid5.Name = "uGrid5";
            this.uGrid5.Size = new System.Drawing.Size(16, 12);
            this.uGrid5.TabIndex = 0;
            this.uGrid5.Text = "ultraGrid1";
            this.uGrid5.Visible = false;
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Controls.Add(this.uGrid4);
            this.uGroupBox4.Location = new System.Drawing.Point(538, 280);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(520, 184);
            this.uGroupBox4.TabIndex = 74;
            this.uGroupBox4.Text = "ultraGroupBox1";
            // 
            // uGrid4
            // 
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4.DisplayLayout.Appearance = appearance70;
            this.uGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance67.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance67.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance67.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.GroupByBox.Appearance = appearance67;
            appearance68.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance68;
            this.uGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance69.BackColor2 = System.Drawing.SystemColors.Control;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance69;
            this.uGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance78;
            appearance73.BackColor = System.Drawing.SystemColors.Highlight;
            appearance73.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance73;
            this.uGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.CardAreaAppearance = appearance72;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            appearance71.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4.DisplayLayout.Override.CellAppearance = appearance71;
            this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance75.BackColor = System.Drawing.SystemColors.Control;
            appearance75.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance75.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance75.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance75;
            appearance77.TextHAlignAsString = "Left";
            this.uGrid4.DisplayLayout.Override.HeaderAppearance = appearance77;
            this.uGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            appearance76.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4.DisplayLayout.Override.RowAppearance = appearance76;
            this.uGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance74.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance74;
            this.uGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4.Location = new System.Drawing.Point(12, 24);
            this.uGrid4.Name = "uGrid4";
            this.uGrid4.Size = new System.Drawing.Size(496, 156);
            this.uGrid4.TabIndex = 0;
            this.uGrid4.Text = "ultraGrid1";
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uGrid3);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 468);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1045, 184);
            this.uGroupBox2.TabIndex = 73;
            this.uGroupBox2.Text = "ultraGroupBox2";
            // 
            // uGrid3
            // 
            this.uGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance43;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance44.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance44;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance45;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance93.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance93.BackColor2 = System.Drawing.SystemColors.Control;
            appearance93.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance93.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance93;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance47;
            appearance98.BackColor = System.Drawing.SystemColors.Highlight;
            appearance98.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance98;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance49;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            appearance50.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance50;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance51.BackColor = System.Drawing.SystemColors.Control;
            appearance51.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance51.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance51.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance51;
            appearance52.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance52;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance53;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance54;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(12, 24);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(1023, 156);
            this.uGrid3.TabIndex = 1;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uButtonDown);
            this.uGroupBox1.Controls.Add(this.uGrid2);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 280);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(520, 184);
            this.uGroupBox1.TabIndex = 72;
            this.uGroupBox1.Text = "ultraGroupBox1";
            // 
            // uButtonDown
            // 
            this.uButtonDown.Location = new System.Drawing.Point(420, 28);
            this.uButtonDown.Name = "uButtonDown";
            this.uButtonDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonDown.TabIndex = 104;
            this.uButtonDown.Text = "ultraButton1";
            this.uButtonDown.Click += new System.EventHandler(this.uButtonDown_Click);
            // 
            // uGrid2
            // 
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance55;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance56;
            appearance57.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance57;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance58.BackColor2 = System.Drawing.SystemColors.Control;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance58;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance59;
            appearance60.BackColor = System.Drawing.SystemColors.Highlight;
            appearance60.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance60;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance61;
            appearance62.BorderColor = System.Drawing.Color.Silver;
            appearance62.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance62;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance63.BackColor = System.Drawing.SystemColors.Control;
            appearance63.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance63.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance63.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance63.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance63;
            appearance64.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance64;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance65;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance66.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance66;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 60);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(496, 120);
            this.uGrid2.TabIndex = 0;
            this.uGrid2.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid2_DoubleClickCell);
            // 
            // uTextEtc
            // 
            appearance99.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtc.Appearance = appearance99;
            this.uTextEtc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtc.Location = new System.Drawing.Point(148, 180);
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.ReadOnly = true;
            this.uTextEtc.Size = new System.Drawing.Size(202, 21);
            this.uTextEtc.TabIndex = 71;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 180);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(130, 20);
            this.uLabelEtc.TabIndex = 70;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uLabelCompleteDate
            // 
            this.uLabelCompleteDate.Location = new System.Drawing.Point(720, 108);
            this.uLabelCompleteDate.Name = "uLabelCompleteDate";
            this.uLabelCompleteDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelCompleteDate.TabIndex = 68;
            this.uLabelCompleteDate.Text = "ultraLabel6";
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Location = new System.Drawing.Point(252, 156);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel1.TabIndex = 66;
            this.ultraLabel1.Text = "~";
            // 
            // uLabelApplyDate
            // 
            this.uLabelApplyDate.Location = new System.Drawing.Point(12, 156);
            this.uLabelApplyDate.Name = "uLabelApplyDate";
            this.uLabelApplyDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelApplyDate.TabIndex = 64;
            this.uLabelApplyDate.Text = "ultraLabel6";
            // 
            // uTextDocKeyword
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocKeyword.Appearance = appearance40;
            this.uTextDocKeyword.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocKeyword.Location = new System.Drawing.Point(1048, 104);
            this.uTextDocKeyword.Name = "uTextDocKeyword";
            this.uTextDocKeyword.ReadOnly = true;
            this.uTextDocKeyword.Size = new System.Drawing.Size(8, 21);
            this.uTextDocKeyword.TabIndex = 63;
            this.uTextDocKeyword.Visible = false;
            // 
            // uLabelDocKeyword
            // 
            this.uLabelDocKeyword.Location = new System.Drawing.Point(1036, 104);
            this.uLabelDocKeyword.Name = "uLabelDocKeyword";
            this.uLabelDocKeyword.Size = new System.Drawing.Size(12, 20);
            this.uLabelDocKeyword.TabIndex = 62;
            this.uLabelDocKeyword.Text = "ultraLabel1";
            this.uLabelDocKeyword.Visible = false;
            // 
            // uTextTitle
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTitle.Appearance = appearance41;
            this.uTextTitle.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTitle.Location = new System.Drawing.Point(512, 36);
            this.uTextTitle.Name = "uTextTitle";
            this.uTextTitle.ReadOnly = true;
            this.uTextTitle.Size = new System.Drawing.Size(471, 21);
            this.uTextTitle.TabIndex = 61;
            // 
            // uLabelTitle
            // 
            this.uLabelTitle.Location = new System.Drawing.Point(376, 36);
            this.uLabelTitle.Name = "uLabelTitle";
            this.uLabelTitle.Size = new System.Drawing.Size(130, 20);
            this.uLabelTitle.TabIndex = 60;
            this.uLabelTitle.Text = "ultraLabel1";
            // 
            // uTextDocState
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Appearance = appearance42;
            this.uTextDocState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Location = new System.Drawing.Point(148, 132);
            this.uTextDocState.Name = "uTextDocState";
            this.uTextDocState.ReadOnly = true;
            this.uTextDocState.Size = new System.Drawing.Size(150, 21);
            this.uTextDocState.TabIndex = 59;
            // 
            // uLabelDocState
            // 
            this.uLabelDocState.Location = new System.Drawing.Point(12, 132);
            this.uLabelDocState.Name = "uLabelDocState";
            this.uLabelDocState.Size = new System.Drawing.Size(130, 20);
            this.uLabelDocState.TabIndex = 58;
            this.uLabelDocState.Text = "ultraLabel1";
            // 
            // uTextApproveDate
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveDate.Appearance = appearance30;
            this.uTextApproveDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveDate.Location = new System.Drawing.Point(512, 246);
            this.uTextApproveDate.Name = "uTextApproveDate";
            this.uTextApproveDate.ReadOnly = true;
            this.uTextApproveDate.Size = new System.Drawing.Size(100, 21);
            this.uTextApproveDate.TabIndex = 57;
            // 
            // uLabelApproveDate
            // 
            this.uLabelApproveDate.Location = new System.Drawing.Point(376, 246);
            this.uLabelApproveDate.Name = "uLabelApproveDate";
            this.uLabelApproveDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelApproveDate.TabIndex = 56;
            this.uLabelApproveDate.Text = "ultraLabel1";
            // 
            // uTextApproveUserName
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Appearance = appearance27;
            this.uTextApproveUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Location = new System.Drawing.Point(250, 246);
            this.uTextApproveUserName.Name = "uTextApproveUserName";
            this.uTextApproveUserName.ReadOnly = true;
            this.uTextApproveUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextApproveUserName.TabIndex = 55;
            // 
            // uTextApproveUserID
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserID.Appearance = appearance28;
            this.uTextApproveUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserID.Location = new System.Drawing.Point(148, 246);
            this.uTextApproveUserID.Name = "uTextApproveUserID";
            this.uTextApproveUserID.ReadOnly = true;
            this.uTextApproveUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextApproveUserID.TabIndex = 54;
            // 
            // uLabelApproveUser
            // 
            this.uLabelApproveUser.Location = new System.Drawing.Point(12, 246);
            this.uLabelApproveUser.Name = "uLabelApproveUser";
            this.uLabelApproveUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelApproveUser.TabIndex = 53;
            this.uLabelApproveUser.Text = "ultraLabel1";
            // 
            // uTextTo
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTo.Appearance = appearance31;
            this.uTextTo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTo.Location = new System.Drawing.Point(512, 178);
            this.uTextTo.Multiline = true;
            this.uTextTo.Name = "uTextTo";
            this.uTextTo.ReadOnly = true;
            this.uTextTo.Size = new System.Drawing.Size(472, 44);
            this.uTextTo.TabIndex = 52;
            // 
            // uLabelTo
            // 
            this.uLabelTo.Location = new System.Drawing.Point(376, 178);
            this.uLabelTo.Name = "uLabelTo";
            this.uLabelTo.Size = new System.Drawing.Size(130, 20);
            this.uLabelTo.TabIndex = 51;
            this.uLabelTo.Text = "ultraLabel1";
            // 
            // uTextFrom
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFrom.Appearance = appearance20;
            this.uTextFrom.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFrom.Location = new System.Drawing.Point(512, 132);
            this.uTextFrom.Multiline = true;
            this.uTextFrom.Name = "uTextFrom";
            this.uTextFrom.ReadOnly = true;
            this.uTextFrom.Size = new System.Drawing.Size(472, 44);
            this.uTextFrom.TabIndex = 50;
            // 
            // uLabelFrom
            // 
            this.uLabelFrom.Location = new System.Drawing.Point(376, 132);
            this.uLabelFrom.Name = "uLabelFrom";
            this.uLabelFrom.Size = new System.Drawing.Size(130, 20);
            this.uLabelFrom.TabIndex = 49;
            this.uLabelFrom.Text = "ultraLabel1";
            // 
            // uTextChangeItem
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeItem.Appearance = appearance18;
            this.uTextChangeItem.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeItem.Location = new System.Drawing.Point(512, 108);
            this.uTextChangeItem.Name = "uTextChangeItem";
            this.uTextChangeItem.ReadOnly = true;
            this.uTextChangeItem.Size = new System.Drawing.Size(150, 21);
            this.uTextChangeItem.TabIndex = 48;
            // 
            // uLabelChangeItem
            // 
            this.uLabelChangeItem.Location = new System.Drawing.Point(376, 108);
            this.uLabelChangeItem.Name = "uLabelChangeItem";
            this.uLabelChangeItem.Size = new System.Drawing.Size(130, 20);
            this.uLabelChangeItem.TabIndex = 47;
            this.uLabelChangeItem.Text = "ultraLabel1";
            // 
            // uTextRevDiscardReason
            // 
            appearance94.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardReason.Appearance = appearance94;
            this.uTextRevDiscardReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardReason.Location = new System.Drawing.Point(148, 108);
            this.uTextRevDiscardReason.Name = "uTextRevDiscardReason";
            this.uTextRevDiscardReason.ReadOnly = true;
            this.uTextRevDiscardReason.Size = new System.Drawing.Size(150, 21);
            this.uTextRevDiscardReason.TabIndex = 46;
            // 
            // uLabelRevDiscardReason
            // 
            this.uLabelRevDiscardReason.Location = new System.Drawing.Point(12, 108);
            this.uLabelRevDiscardReason.Name = "uLabelRevDiscardReason";
            this.uLabelRevDiscardReason.Size = new System.Drawing.Size(130, 20);
            this.uLabelRevDiscardReason.TabIndex = 45;
            this.uLabelRevDiscardReason.Text = "ultraLabel1";
            // 
            // uTextRevDiscardDate
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardDate.Appearance = appearance16;
            this.uTextRevDiscardDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardDate.Location = new System.Drawing.Point(856, 84);
            this.uTextRevDiscardDate.Name = "uTextRevDiscardDate";
            this.uTextRevDiscardDate.ReadOnly = true;
            this.uTextRevDiscardDate.Size = new System.Drawing.Size(150, 21);
            this.uTextRevDiscardDate.TabIndex = 44;
            // 
            // uLabelRevDiscardDate
            // 
            this.uLabelRevDiscardDate.Location = new System.Drawing.Point(720, 84);
            this.uLabelRevDiscardDate.Name = "uLabelRevDiscardDate";
            this.uLabelRevDiscardDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelRevDiscardDate.TabIndex = 43;
            this.uLabelRevDiscardDate.Text = "ultraLabel1";
            // 
            // uTextRevDiscardUserName
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardUserName.Appearance = appearance29;
            this.uTextRevDiscardUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardUserName.Location = new System.Drawing.Point(614, 84);
            this.uTextRevDiscardUserName.Name = "uTextRevDiscardUserName";
            this.uTextRevDiscardUserName.ReadOnly = true;
            this.uTextRevDiscardUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRevDiscardUserName.TabIndex = 42;
            // 
            // uTextRevDiscardUserID
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardUserID.Appearance = appearance25;
            this.uTextRevDiscardUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardUserID.Location = new System.Drawing.Point(512, 84);
            this.uTextRevDiscardUserID.Name = "uTextRevDiscardUserID";
            this.uTextRevDiscardUserID.ReadOnly = true;
            this.uTextRevDiscardUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRevDiscardUserID.TabIndex = 41;
            // 
            // uLabelRevDiscardUser
            // 
            this.uLabelRevDiscardUser.Location = new System.Drawing.Point(376, 84);
            this.uLabelRevDiscardUser.Name = "uLabelRevDiscardUser";
            this.uLabelRevDiscardUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelRevDiscardUser.TabIndex = 40;
            this.uLabelRevDiscardUser.Text = "ultraLabel1";
            // 
            // uTextRevDiscardDiv
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardDiv.Appearance = appearance32;
            this.uTextRevDiscardDiv.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevDiscardDiv.Location = new System.Drawing.Point(148, 84);
            this.uTextRevDiscardDiv.Name = "uTextRevDiscardDiv";
            this.uTextRevDiscardDiv.ReadOnly = true;
            this.uTextRevDiscardDiv.Size = new System.Drawing.Size(150, 21);
            this.uTextRevDiscardDiv.TabIndex = 39;
            // 
            // uLabelRevDiscardDiv
            // 
            this.uLabelRevDiscardDiv.Location = new System.Drawing.Point(12, 84);
            this.uLabelRevDiscardDiv.Name = "uLabelRevDiscardDiv";
            this.uLabelRevDiscardDiv.Size = new System.Drawing.Size(130, 20);
            this.uLabelRevDiscardDiv.TabIndex = 38;
            this.uLabelRevDiscardDiv.Text = "ultraLabel1";
            // 
            // uTextCreateDate
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Appearance = appearance33;
            this.uTextCreateDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Location = new System.Drawing.Point(512, 60);
            this.uTextCreateDate.Name = "uTextCreateDate";
            this.uTextCreateDate.ReadOnly = true;
            this.uTextCreateDate.Size = new System.Drawing.Size(150, 21);
            this.uTextCreateDate.TabIndex = 37;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(376, 60);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateDate.TabIndex = 36;
            this.uLabelCreateDate.Text = "ultraLabel1";
            // 
            // uTextCreateUserName
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Appearance = appearance34;
            this.uTextCreateUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Location = new System.Drawing.Point(250, 60);
            this.uTextCreateUserName.Name = "uTextCreateUserName";
            this.uTextCreateUserName.ReadOnly = true;
            this.uTextCreateUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserName.TabIndex = 35;
            // 
            // uTextCreateUserID
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Appearance = appearance35;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Location = new System.Drawing.Point(148, 60);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.ReadOnly = true;
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 34;
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(12, 60);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateUser.TabIndex = 33;
            this.uLabelCreateUser.Text = "ultraLabel1";
            // 
            // uTextSmall
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSmall.Appearance = appearance36;
            this.uTextSmall.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSmall.Location = new System.Drawing.Point(856, 12);
            this.uTextSmall.Name = "uTextSmall";
            this.uTextSmall.ReadOnly = true;
            this.uTextSmall.Size = new System.Drawing.Size(150, 21);
            this.uTextSmall.TabIndex = 32;
            // 
            // uLabelSmall
            // 
            this.uLabelSmall.Location = new System.Drawing.Point(720, 12);
            this.uLabelSmall.Name = "uLabelSmall";
            this.uLabelSmall.Size = new System.Drawing.Size(130, 20);
            this.uLabelSmall.TabIndex = 31;
            this.uLabelSmall.Text = "ultraLabel1";
            // 
            // uTextMiddle
            // 
            appearance92.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMiddle.Appearance = appearance92;
            this.uTextMiddle.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMiddle.Location = new System.Drawing.Point(512, 12);
            this.uTextMiddle.Name = "uTextMiddle";
            this.uTextMiddle.ReadOnly = true;
            this.uTextMiddle.Size = new System.Drawing.Size(150, 21);
            this.uTextMiddle.TabIndex = 30;
            // 
            // uLabelMiddle
            // 
            this.uLabelMiddle.Location = new System.Drawing.Point(376, 12);
            this.uLabelMiddle.Name = "uLabelMiddle";
            this.uLabelMiddle.Size = new System.Drawing.Size(130, 20);
            this.uLabelMiddle.TabIndex = 29;
            this.uLabelMiddle.Text = "ultraLabel1";
            // 
            // uTextLarge
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLarge.Appearance = appearance26;
            this.uTextLarge.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLarge.Location = new System.Drawing.Point(148, 12);
            this.uTextLarge.Name = "uTextLarge";
            this.uTextLarge.ReadOnly = true;
            this.uTextLarge.Size = new System.Drawing.Size(150, 21);
            this.uTextLarge.TabIndex = 28;
            // 
            // uLabelLarge
            // 
            this.uLabelLarge.Location = new System.Drawing.Point(12, 12);
            this.uLabelLarge.Name = "uLabelLarge";
            this.uLabelLarge.Size = new System.Drawing.Size(130, 20);
            this.uLabelLarge.TabIndex = 27;
            this.uLabelLarge.Text = "ultraLabel1";
            // 
            // uTextPlant
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance21;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(1036, 132);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(12, 21);
            this.uTextPlant.TabIndex = 24;
            this.uTextPlant.Visible = false;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(1044, 132);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(10, 20);
            this.uLabelPlant.TabIndex = 23;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // uTextRevisionNum
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionNum.Appearance = appearance22;
            this.uTextRevisionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionNum.Location = new System.Drawing.Point(262, 36);
            this.uTextRevisionNum.Name = "uTextRevisionNum";
            this.uTextRevisionNum.ReadOnly = true;
            this.uTextRevisionNum.Size = new System.Drawing.Size(89, 21);
            this.uTextRevisionNum.TabIndex = 22;
            // 
            // uTextStdDocNum
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdDocNum.Appearance = appearance23;
            this.uTextStdDocNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdDocNum.Location = new System.Drawing.Point(148, 36);
            this.uTextStdDocNum.Name = "uTextStdDocNum";
            this.uTextStdDocNum.ReadOnly = true;
            this.uTextStdDocNum.Size = new System.Drawing.Size(112, 21);
            this.uTextStdDocNum.TabIndex = 20;
            // 
            // uLabelStdDocNum
            // 
            this.uLabelStdDocNum.Location = new System.Drawing.Point(12, 36);
            this.uLabelStdDocNum.Name = "uLabelStdDocNum";
            this.uLabelStdDocNum.Size = new System.Drawing.Size(130, 20);
            this.uLabelStdDocNum.TabIndex = 19;
            this.uLabelStdDocNum.Text = "ultraLabel1";
            // 
            // frmISOZ0001D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISOZ0001D";
            this.Load += new System.EventHandler(this.frmISOZ0001D_Load);
            this.Activated += new System.EventHandler(this.frmISOZ0001D_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISOZ0001D_FormClosing);
            this.Resize += new System.EventHandler(this.frmISOZ0001D_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApplyFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).EndInit();
            this.uGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevDiscardDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdDocNum)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchDistributeRequestToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchDistributeRequestFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDistributeRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDistributeCompanyName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDistributeCompanyID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDistributeCompany;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStandardNum;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStandardNum;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelStdDocNum;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelSmall;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelMiddle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLarge;
        private Infragistics.Win.Misc.UltraLabel uLabelLarge;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdDocNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevDiscardDiv;
        private Infragistics.Win.Misc.UltraLabel uLabelRevDiscardDiv;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocState;
        private Infragistics.Win.Misc.UltraLabel uLabelDocState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveDate;
        private Infragistics.Win.Misc.UltraLabel uLabelApproveDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelApproveUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTo;
        private Infragistics.Win.Misc.UltraLabel uLabelTo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFrom;
        private Infragistics.Win.Misc.UltraLabel uLabelFrom;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeItem;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeItem;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevDiscardReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRevDiscardReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevDiscardDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRevDiscardDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevDiscardUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevDiscardUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRevDiscardUser;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelCompleteDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel uLabelApplyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocKeyword;
        private Infragistics.Win.Misc.UltraLabel uLabelDocKeyword;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelTitle;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid5;
        private Infragistics.Win.Misc.UltraButton uButtonDown;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeDeptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelMake;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPadSize;
        private Infragistics.Win.Misc.UltraLabel uLabelPadSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChipSize;
        private Infragistics.Win.Misc.UltraLabel uLabelChipSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCompleteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApplyToDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApplyFromDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRWComment;
        private Infragistics.Win.Misc.UltraLabel uLabelGRWComment;
    }
}