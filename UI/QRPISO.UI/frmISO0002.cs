﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서 관리                                         */
/* 프로그램ID   : frmISO0002.cs                                         */
/* 프로그램명   : 표준문서 소분류 정보                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPISO.UI
{
    public partial class frmISO0002 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = ""; 


        QRPGlobal SysRes = new QRPGlobal();

        public frmISO0002()
        {
            InitializeComponent();
        }

        private void frmISO0002_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISO0002_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 소분류 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetRunMode();
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            MessageBox.Show(this.Tag.ToString());
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
         }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLargeClassify, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMiddleClassify, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLargeClassify, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMiddleClassify, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSmallClassifyCode, "소분류코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSmallClassifyName, "소분류명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSmallClassifyNameCh, "소분류명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSmallClassifyNameEn, "소분류명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장 콤보박스
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SearchArea
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);


                // ContentsArea
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);



                // 사용여부 콤보박스
                // UseFlag ComboBox
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                dt = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboUseFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "T", "", "선택"
                    , "ComCode", "ComCodeName", dt);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeCode", "대분류", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeName", "대분류", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MTypeCode", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MTypeName", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "STypeCode", "소분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "STypeName", "소분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "STypeNameCh", "소분류명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "STypeNameEn", "소분류명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                // DropDown 박스 설정
                // 공장열

                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);


                // 대분류


                // 중분류


                // 사용여부
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);
                dt = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                // 그리드 컬럼에 콤보박스 추가
                wGrid.mfSetGridColumnValueList(uGrid1, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE"); // "";
                this.uComboLargeClassify.Value = "";
                this.uComboMiddleClassify.Value = "";
                this.uTextSmallClassify.Text = "";
                this.uTextSmallClassify.MaxLength = 10;
                this.uTextSmallClassifyName.Text = "";
                this.uTextSmallClassifyName.MaxLength = 50;
                this.uTextSmallClassifyNameCh.Text = "";
                this.uTextSmallClassifyNameCh.MaxLength = 50;
                this.uTextSmallClassifyNameEn.Text = "";
                this.uTextSmallClassifyNameEn.MaxLength = 50;
                this.uComboUseFlag.Value = "T";
                this.uComboLargeClassify.Appearance.BackColor = Color.PowderBlue;
                this.uComboMiddleClassify.Appearance.BackColor = Color.PowderBlue;


                //컴포넌트 수정 가능
                //컴퍼넌트 수정 불가 처리
                this.uComboPlant.ReadOnly = false;
                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uComboLargeClassify.ReadOnly = false;
                this.uComboLargeClassify.Appearance.BackColor = Color.PowderBlue;
                this.uComboMiddleClassify.ReadOnly = false;
                this.uComboMiddleClassify.Appearance.BackColor = Color.PowderBlue;
                this.uTextSmallClassify.ReadOnly = false;
                this.uTextSmallClassify.Appearance.BackColor = Color.PowderBlue;
                this.uTextSmallClassifyName.ReadOnly = false;
                this.uTextSmallClassifyNameCh.ReadOnly = false;
                this.uTextSmallClassifyNameEn.ReadOnly = false;
                this.uComboUseFlag.ReadOnly = false;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPopup = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
                QRPISO.BL.ISODOC.ISODocSType isoDocSType = new QRPISO.BL.ISODOC.ISODocSType();
                
                //Parameter
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLargeClassify.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddleClassify.Value.ToString();

                //함수 호출
                DataTable dt = isoDocSType.mfReadISODocSType(strPlantCode, strLTypeCode, strMTypeCode, m_resSys.GetString("SYS_LANG"));
                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                       , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;

                    InitValue();
                }

            }
        }

        public void mfSave()
        {
            try
            {
                //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //BL호출
                QRPISO.BL.ISODOC.ISODocSType isoDocSType;
                if (m_bolDebugMode == false)
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
                    isoDocSType = new QRPISO.BL.ISODOC.ISODocSType();
                    brwChannel.mfCredentials(isoDocSType);
                }
                else
                    isoDocSType = new QRPISO.BL.ISODOC.ISODocSType(m_strDBConn);
                   
                //저장 함수 호출 매개 변수 DataTable
                DataTable dtSType = isoDocSType.mfSetDateInfo();
                DialogResult DResult = new DialogResult();

                    
                //필수 입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                      , "M001230", "M000279", Infragistics.Win.HAlign.Center);

                    uComboPlant.DropDown();

                }
                else if(this.uComboLargeClassify.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                      , "M001230", "M000372", Infragistics.Win.HAlign.Center);

                    uComboLargeClassify.DropDown();
                }
                else if(this.uComboMiddleClassify.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                      , "M001230", "M001126", Infragistics.Win.HAlign.Center);

                    uComboMiddleClassify.DropDown();
                }
                else if(this.uTextSmallClassify.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                      , "M001230", "M000726", Infragistics.Win.HAlign.Center);

                    uTextSmallClassify.Focus();
                }
                else if( this.uTextSmallClassifyName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                      , "M001230", "M000724", Infragistics.Win.HAlign.Center);

                    uTextSmallClassifyName.Focus();
                }
                else
                {
                    DataRow drSType = dtSType.NewRow();
                    drSType["PlantCode"] = this.uComboPlant.Value.ToString();
                    drSType["LTypeCode"] = this.uComboLargeClassify.Value.ToString();
                    drSType["MTypeCode"] = this.uComboMiddleClassify.Value.ToString();
                    drSType["STypeCode"] = this.uTextSmallClassify.Text;
                    drSType["STypeName"] = this.uTextSmallClassifyName.Text;
                    drSType["STypeNameCh"] = this.uTextSmallClassifyNameCh.Text;
                    drSType["STypeNameEn"] = this.uTextSmallClassifyNameEn.Text;
                    drSType["UseFlag"] = this.uComboUseFlag.Value.ToString();
                    dtSType.Rows.Add(drSType);
                }

                if (dtSType.Rows.Count > 0)
                {
                    if (this.uComboPlant.ReadOnly == false)
                    {
                        DataTable dtCheckSTypeCode = isoDocSType.mfReadISODocSTypeCombo(this.uComboPlant.Value.ToString(), this.uComboLargeClassify.Value.ToString()
                                                                                , this.uComboMiddleClassify.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                        for (int i = 0; i < dtCheckSTypeCode.Rows.Count; i++)
                        {
                            if (dtCheckSTypeCode.Rows[i]["STypeCode"].ToString() == this.uTextSmallClassify.Text)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                                    , "M000725", "M000849", Infragistics.Win.HAlign.Center);

                                this.uTextSmallClassify.Focus();
                                return;
                            }
                        }
                    }

                    //콤보박스 선택값 Validation Check//////////
                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this)) return;
                    ///////////////////////////////////////////

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar uProgressPopup = new QRPProgressBar();
                        Thread uth = uProgressPopup.mfStartThread();
                        uProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //저장함수 호출

                        String strISODocSType = isoDocSType.mfSaveISODocSType(dtSType, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strISODocSType);
                        //END

                        this.MdiParent.Cursor = Cursors.Default;
                        uProgressPopup.mfCloseProgressPopup(this);

                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000930"
                            , Infragistics.Win.HAlign.Right);

                            InitValue();
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                          , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000953"
                          , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
            
            //BL호출
            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
            QRPISO.BL.ISODOC.ISODocSType isoDocSType = new QRPISO.BL.ISODOC.ISODocSType();
            brwChannel.mfCredentials(isoDocSType);

            //Data Table 설정
            DataTable dtISODocSType = isoDocSType.mfSetDateInfo();
            

            //check 삭제
           
                //필수 입력사항 확인
                if (this.uGroupBoxContentsArea.Expanded == false || this.uTextSmallClassify.Text == "" || this.uComboMiddleClassify.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001230", "M000639", Infragistics.Win.HAlign.Center);

                    return;
                }
                else
                {
                    DataRow drISODocSType = dtISODocSType.NewRow();
                    drISODocSType["PlantCode"] = this.uComboPlant.Value.ToString();
                    drISODocSType["LTypeCode"] = this.uComboLargeClassify.Value.ToString();
                    drISODocSType["MTypeCode"] = this.uComboMiddleClassify.Value.ToString();
                    drISODocSType["STypeCode"] = this.uTextSmallClassify.Text;
                    dtISODocSType.Rows.Add(drISODocSType);
                }
            
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                    , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread thread = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리로직


                    //삭제 메소드 호출
                    string rtMSG = isoDocSType.mfDeleteISODocSType(dtISODocSType);

                    //Decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                    //로직 끝

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    DialogResult DResult = new DialogResult();
                    //삭제 성공 여부
                    if(ErrRtn.ErrNum == 0)
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001135", "M000638", "M000926",
                        Infragistics.Win.HAlign.Right);

                        InitValue();
                        mfSearch();
                    }
                    else
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001135", "M000638", "M000923",
                        Infragistics.Win.HAlign.Right);
                    }
                    
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;

                    InitValue();
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화

                else
                {
                    InitValue();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 55;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 760;

                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtLType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchLargeClassify.Items.Clear();

                if (strPlantCode != "")
                {
                    //대분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(clsLType);

                    dtLType = clsLType.mfReadISODocLTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchLargeClassify, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "LTypeCode", "LTypeName", dtLType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchLargeClassify_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtMType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLargeClassify.Value.ToString();
                
                this.uComboSearchMiddleClassify.Items.Clear();

                if (strLTypeCode != "")
                {
                    //중분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                    QRPISO.BL.ISODOC.ISODocMType clsMType = new QRPISO.BL.ISODOC.ISODocMType();
                    brwChannel.mfCredentials(clsMType);

                    dtMType = clsMType.mfReadISODocMTypeCombo(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMiddleClassify, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "MTypeCode", "MTypeName", dtMType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtLType = new DataTable();

                String strPlantCode = this.uComboPlant.Value.ToString();

                this.uComboLargeClassify.Items.Clear();

                if (strPlantCode != "")
                {
                    //대분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(clsLType);

                    dtLType = clsLType.mfReadISODocLTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboLargeClassify, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "LTypeCode", "LTypeName", dtLType);
                this.uComboLargeClassify.Appearance.BackColor = Color.PowderBlue;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboLargeClassify_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtMType = new DataTable();

                String strPlantCode = this.uComboPlant.Value.ToString();
                String strLTypeCode = this.uComboLargeClassify.Value.ToString();

                this.uComboMiddleClassify.Items.Clear();

                if (strLTypeCode != "")
                {
                    //중분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                    QRPISO.BL.ISODOC.ISODocMType clsMType = new QRPISO.BL.ISODOC.ISODocMType();
                    brwChannel.mfCredentials(clsMType);

                    dtMType = clsMType.mfReadISODocMTypeCombo(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboMiddleClassify, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "MTypeCode", "MTypeName", dtMType);

                this.uComboMiddleClassify.Appearance.BackColor = Color.PowderBlue;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;

                    e.Cell.Row.Fixed = true;
                }
                //그리드값을 컴퍼넌트에 대입
                this.uComboPlant.Value = e.Cell.Row.Cells["PlantCode"].Value;
                this.uComboLargeClassify.Value = e.Cell.Row.Cells["LTypeCode"].Value;
                this.uComboMiddleClassify.Value = e.Cell.Row.Cells["MTypeCode"].Value;
                this.uTextSmallClassify.Text = e.Cell.Row.Cells["STypeCode"].Value.ToString();
                this.uTextSmallClassifyName.Text = e.Cell.Row.Cells["STypeName"].Value.ToString();
                this.uTextSmallClassifyNameCh.Text = e.Cell.Row.Cells["STypeNameCh"].Value.ToString();
                this.uTextSmallClassifyNameEn.Text = e.Cell.Row.Cells["STypeNameEn"].Value.ToString();

                //컴퍼넌트 수정 불가 처리
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                this.uComboLargeClassify.ReadOnly = true;
                this.uComboLargeClassify.Appearance.BackColor = Color.Gainsboro;
                this.uComboMiddleClassify.ReadOnly = true;
                this.uComboMiddleClassify.Appearance.BackColor = Color.Gainsboro;
                this.uTextSmallClassify.ReadOnly = true;
                this.uTextSmallClassify.Appearance.BackColor = Color.Gainsboro;
                //this.uTextSmallClassifyName.ReadOnly = true;
                //this.uTextSmallClassifyNameCh.ReadOnly = true;
                //this.uTextSmallClassifyNameEn.ReadOnly = true;
                //this.uComboUseFlag.ReadOnly = true;


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {

            }
        }

        private void frmISO0002_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void frmISO0002_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
