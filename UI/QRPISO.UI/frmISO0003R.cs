﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서 관리                                         */
/* 프로그램ID   : frmISO0003R.cs                                        */
/* 프로그램명   : 표준문서 개정/폐기                                    */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPISO.UI
{
    public partial class frmISO0003R : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";

        //첨부파일 다운로드, 이전 개정 폐기를 위한 개정전 개정번호 저장을 위한 전역변수
        private String staticVersionNum = "";
        private String strAdmitStatusCode;

        private bool m_bolAutoNumbering = false;        //선택한 대분류에 따른 자동채번 여부 체크
        private string m_LargeTypeCode = ""; // 문서 선택시 대분류코드 저장

        QRPGlobal SysRes = new QRPGlobal();

        //개정번호 임시 저장
        String strRevText = "";

        private string PlantCode { get; set; }

        public frmISO0003R()
        {
            InitializeComponent();
        }

        private void frmISO0003R_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISO0003R_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 개정/폐기", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            //SetRunMode();
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitValue();

            // 그룹박스 접은 상태로



            this.uGroupBoxContentsArea.Expanded = false;
        }


        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    MessageBox.Show(this.Tag.ToString());
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }

                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method
        /// <summary>
        /// GroupBox 초기화


        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "배포업체", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "적용범위", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.LIST, "결재선", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);



                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화


        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRegDate, "등록일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVersionCheck, "구버전포함", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStandardDocNo, "표준문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevisionNo, "개정번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "기안자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateDate, "기안일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisSeparation, "개정/폐기구분", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRev_DisUser, "개정/폐기자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRev_DisDate, "개정/폐기일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelRev_DisReason, "개정/폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelChangeItem, "변경항목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelFrom, "변경 전", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelTo, "변경 후", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelApproveUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelDocState, "문서상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDocKeyword, "고객사표준", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApplyDate, "적용일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReturnReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMakeUse, "작성자/부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevUserName, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGRWComment, "영문제목", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChipSize, "ChipSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPadSize, "PadSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화



        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow3, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow4, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화



        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // Process ComboBox
                // Bl 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo("", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ProcessCode", "ProcessName", dtProcess);


                // 개정/폐기 구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dt = clsComCode.mfReadCommonCode("C0010", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboRev_DisSeparation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화



        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid, 0, "LTypeName", "대분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "MTypeName", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "AdmitVersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "VersionNum", "기존개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DisplayUserName", "재 / 개정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteName", "기안자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteID", "기안자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseDate", "제 / 개정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DisplayDate", "제 / 개정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DisplayReason", "제 / 개정사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseReason", "제 / 개정사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //문서상태 추가 AdmitStatus
                wGrid.mfSetGridColumn(this.uGrid, 0, "AdmitStatus", "문서상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2000
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteDate", "기안일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseID", "개정자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseName", "개정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                //그리드 추가

                wGrid.mfSetGridColumn(this.uGrid, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid, 0, "ApplyDateFrom", "적용일From", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid, 0, "ApplyDateTo", "적용일To", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "LTypeCode", "대분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid, 0, "MTypeCode", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid, 0, "STypeCode", "소분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 첨부파일 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DWFlag", "DW여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 배포업체 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorCode", "업체코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorName", "업체명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "PersonName", "담당자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid2, 0, "Position", "직위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Hp", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "###-####-####", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Email", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 적용범위 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "ProcessGroup", "공정그룹", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcessGroup);

                DataTable dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid3, 0, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtProcessGroup);


                // 합의 담당자 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid4, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid4, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserID", "담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserName", "담당자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGrid1, 0);
                wGrid.mfAddRowGrid(this.uGrid2, 0);
                wGrid.mfAddRowGrid(this.uGrid3, 0);
                wGrid.mfAddRowGrid(this.uGrid4, 0);

                // Set Font Size
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid4.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid4.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                //// 그리드 내의 콤보박스 처리 ////

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화



        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                PlantCode = string.Empty;
                m_LargeTypeCode = "";
                this.uTextInnerAdmitVersionNum.Text = "";
                this.uTextAdmitVersionNum.Text = "";

                this.uTextApproveUserID.Text = "";
                this.uTextApproveUserID.MaxLength = 20;
                this.uTextApproveUserName.Text = "";
                this.uTextApproveUserName.MaxLength = 20;
                this.uTextChangeItem.Text = "";
                this.uTextChangeItem.MaxLength = 500;
                this.uTextCreateDate.Text = "";
                this.uTextCreateDate.MaxLength = 10;
                this.uTextCreateUserID.Text = "";
                this.uTextCreateUserID.MaxLength = 20;
                this.uTextCreateUserName.Text = "";
                this.uTextCreateUserName.MaxLength = 20;
                this.uTextDocKeyword.Text = "";
                this.uTextDocState.Text = "";
                this.uTextEtc.Text = "";
                this.uTextEtc.MaxLength = 100;
                this.uTextFrom.Text = "";
                this.uTextFrom.MaxLength = 1000;
                this.uTextLarge.Text = "";
                this.uTextLarge.MaxLength = 10;
                this.uTextMiddle.Text = "";
                this.uTextLarge.MaxLength = 10;
                this.uTextPlant.Text = "";
                this.uTextPlant.MaxLength = 10;
                this.uTextProcess.Text = "";
                this.uTextReturnReason.Text = "";
                this.uTextReturnReason.MaxLength = 100;
                this.uTextGRWComment.Text = "";
                this.uTextGRWComment.MaxLength = 1000;

                this.uTextRev_DisReason.Text = "";
                this.uTextRev_DisReason.MaxLength = 2000;
                this.uTextRev_DisReason.ReadOnly = false;
                this.uTextRev_DisReason.Appearance.BackColor = Color.PowderBlue;

                this.uTextRev_DisUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextRev_DisUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextRevisionNo.Text = "";
                this.uTextRevisionNo.MaxLength = 50;
                //this.uTextSearchStandardNo.Text = "";
                this.uTextSearchStandardNo.MaxLength = 100;
                //this.uTextSearchTitle.Text = "";
                this.uTextSearchTitle.MaxLength = 2000;
                this.uTextSmall.Text = "";
                this.uTextSmall.MaxLength = 10;
                this.uTextStandardDocNo.Text = "";
                this.uTextStandardDocNo.MaxLength = 100;
                this.uTextTitle.Text = "";
                this.uTextTitle.MaxLength = 2000;
                this.uTextTo.Text = "";
                this.uTextTo.MaxLength = 1000;
                this.uTextMakeUserName.Text = "";
                this.uTextMakeUserName.MaxLength = 40;
                this.uTextMakeDeptName.Text = "";
                this.uTextMakeDeptName.MaxLength = 40;
                this.uTextPackage.Text = "";
                this.uTextPackage.MaxLength = 40;
                this.uTextChipSize.Text = "";
                this.uTextChipSize.MaxLength = 100;
                this.uTextPadSize.Text = "";
                this.uTextPadSize.MaxLength = 100;
                this.uTextRevUserName.Text = "";
                this.uTextRevUserName.MaxLength = 20;

                this.uDateApplyFromDate.Value = DateTime.Now;
                this.uDateApplyToDate.Value = DateTime.Now.AddDays(7);
                this.uDateCompleteDate.Value = DateTime.Now.AddDays(7);
                this.uDateRev_DisDate.Value = DateTime.Now;
                this.uDateSearchRegFromDate.Value = DateTime.Now.AddDays(-7);
                this.uDateSearchRegToDate.Value = DateTime.Now;

                this.uComboRev_DisSeparation.ReadOnly = false;
                this.uComboRev_DisSeparation.Value = "";
                this.uComboRev_DisSeparation.Appearance.BackColor = Color.PowderBlue;
                //this.uComboSearchLarge.Value = "";
                //this.uComboSearchMiddle.Value = "";
                //this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE"); //"";
                //this.uComboSearchProcess.Value = "";
                //this.uComboSearchSmall.Value = "";

                this.uTextChangeItem.ReadOnly = false;
                this.uTextFrom.ReadOnly = false;
                this.uTextTo.ReadOnly = false;

                //스크롤바
                this.uTextFrom.Scrollbars = ScrollBars.Vertical;
                this.uTextFrom.SelectionStart = uTextFrom.Text.Length;
                this.uTextFrom.ScrollToCaret();

                this.uTextTo.Scrollbars = ScrollBars.Vertical;
                this.uTextTo.SelectionStart = uTextTo.Text.Length;
                this.uTextTo.ScrollToCaret();


                //공정 히든
                this.uComboSearchProcess.Visible = false;
                this.uTextProcess.Visible = false;
                this.uDateCompleteDate.Visible = false;

                if (this.uGrid1.Rows.Count > 0)
                {
                    while (this.uGrid1.Rows.Count > 0)
                    {
                        this.uGrid1.Rows[0].Delete(false);
                    }
                }

                if (this.uGrid2.Rows.Count > 0)
                {
                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #region 문서상태에 따라서 수정불가 / 수정가능 상태로 변경
        //수정불가
        private void SetDisWritable()
        {
            try
            {
                this.uTextAdmitVersionNum.ReadOnly = true;
                this.uTextAdmitVersionNum.Appearance.BackColor = Color.Gainsboro;

                this.uTextTitle.ReadOnly = true;
                this.uTextTitle.Appearance.BackColor = Color.Gainsboro;
                this.uComboRev_DisSeparation.ReadOnly = true;
                this.uComboRev_DisSeparation.Appearance.BackColor = Color.Gainsboro;
                this.uTextRev_DisUserID.ReadOnly = true;
                this.uTextRev_DisUserID.Appearance.BackColor = Color.Gainsboro;
                this.uDateRev_DisDate.ReadOnly = true;
                this.uDateRev_DisDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextRev_DisReason.ReadOnly = true;
                this.uTextRev_DisReason.Appearance.BackColor = Color.Gainsboro;
                this.uTextApproveUserID.ReadOnly = true;
                this.uTextApproveUserID.Appearance.BackColor = Color.Gainsboro;
                this.uTextRevUserName.ReadOnly = true;
                this.uTextRevUserName.Appearance.BackColor = Color.Gainsboro;
                this.uTextChangeItem.ReadOnly = true;
                this.uTextChangeItem.Appearance.BackColor = Color.Gainsboro;
                this.uTextFrom.ReadOnly = true;
                this.uTextFrom.Appearance.BackColor = Color.Gainsboro;
                this.uTextTo.ReadOnly = true;
                this.uTextTo.Appearance.BackColor = Color.Gainsboro;
                this.uDateApplyFromDate.ReadOnly = true;
                this.uDateApplyToDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateApplyToDate.ReadOnly = true;
                this.uDateApplyToDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextEtc.ReadOnly = true;
                this.uTextEtc.Appearance.BackColor = Color.Gainsboro;
                this.uTextGRWComment.ReadOnly = true;
                this.uTextGRWComment.Appearance.BackColor = Color.Gainsboro;
                this.uTextReturnReason.ReadOnly = true;
                this.uTextReturnReason.Appearance.BackColor = Color.Gainsboro;
                this.uTextPackage.ReadOnly = true;
                this.uTextPackage.Appearance.BackColor = Color.Gainsboro;
                this.uTextChipSize.ReadOnly = true;
                this.uTextChipSize.Appearance.BackColor = Color.Gainsboro;
                this.uTextPadSize.ReadOnly = true;
                this.uTextPadSize.Appearance.BackColor = Color.Gainsboro;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //그리드 변경
        private void GridDiswritable()
        {
            try
            {
                WinGrid wGrid = new WinGrid();

                this.uGrid1.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGrid2.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGrid3.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGrid4.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid1.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid1.Rows[i].Cells["FileTitle"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid1.Rows[i].Cells["DWFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid1.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid2.Rows[i].Cells["VendorCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid2.Rows[i].Cells["VendorName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid2.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid2.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                {
                    this.uGrid3.Rows[i].Cells["ProcessGroup"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid3.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                {
                    this.uGrid4.Rows[i].Cells["AgreeUserName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid4.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //수정 가능
        private void SetWritable()
        {
            try
            {
                this.uTextTitle.ReadOnly = false;
                this.uTextTitle.Appearance.BackColor = Color.White;
                this.uComboRev_DisSeparation.ReadOnly = false;
                this.uComboRev_DisSeparation.Appearance.BackColor = Color.PowderBlue;
                this.uTextRev_DisUserID.ReadOnly = false;
                this.uTextRev_DisUserID.Appearance.BackColor = Color.PowderBlue;
                this.uDateRev_DisDate.ReadOnly = false;
                this.uDateRev_DisDate.Appearance.BackColor = Color.PowderBlue;
                this.uTextRev_DisReason.ReadOnly = false;
                this.uTextRev_DisReason.Appearance.BackColor = Color.PowderBlue;
                this.uTextApproveUserID.ReadOnly = false;
                this.uTextApproveUserID.Appearance.BackColor = Color.PowderBlue;
                this.uTextRevUserName.ReadOnly = false;
                this.uTextRevUserName.Appearance.BackColor = Color.White;
                this.uTextChangeItem.ReadOnly = false;
                this.uTextChangeItem.Appearance.BackColor = Color.PowderBlue;
                this.uTextFrom.ReadOnly = false;
                this.uTextFrom.Appearance.BackColor = Color.PowderBlue;
                this.uTextTo.ReadOnly = false;
                this.uTextTo.Appearance.BackColor = Color.PowderBlue;
                this.uDateApplyFromDate.ReadOnly = false; ;
                this.uDateApplyToDate.Appearance.BackColor = Color.White;
                this.uDateApplyToDate.ReadOnly = false;
                this.uDateApplyToDate.Appearance.BackColor = Color.White;
                this.uTextEtc.ReadOnly = false;
                this.uTextEtc.Appearance.BackColor = Color.White;
                this.uTextGRWComment.ReadOnly = false;
                this.uTextGRWComment.BackColor = Color.White;
                this.uTextPackage.ReadOnly = false;
                this.uTextPackage.Appearance.BackColor = Color.White;
                this.uTextChipSize.ReadOnly = false;
                this.uTextChipSize.Appearance.BackColor = Color.White;
                this.uTextPadSize.ReadOnly = false;
                this.uTextPadSize.Appearance.BackColor = Color.White;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //그리드 수정 가능
        private void GridWritable()
        {
            try
            {
                WinGrid wGrid = new WinGrid();

                this.uGrid1.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGrid2.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGrid3.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGrid4.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;

                //// 빈줄추가
                wGrid.mfAddRowGrid(this.uGrid1, 0);
                wGrid.mfAddRowGrid(this.uGrid2, 0);
                wGrid.mfAddRowGrid(this.uGrid3, 0);
                wGrid.mfAddRowGrid(this.uGrid4, 0);

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid1.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid1.Rows[i].Cells["FileTitle"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid1.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid2.Rows[i].Cells["VendorCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid2.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid2.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                {
                    this.uGrid3.Rows[i].Cells["ProcessGroup"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid3.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                {
                    this.uGrid4.Rows[i].Cells["AgreeUserID"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid4.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }

            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion


        #endregion

        #region ToolBar 메소드



        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(isoDocH);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();
                String strSTypCode = this.uComboSearchSmall.Value.ToString();
                String strStdNumber = this.uTextSearchStandardNo.Text;
                String strProcessCode = this.uComboSearchProcess.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;
                String strApplyDateFrom = Convert.ToDateTime(this.uDateSearchRegFromDate.Value).ToString("yyyy-MM-dd");
                String strApplyDateTo = Convert.ToDateTime(this.uDateSearchRegToDate.Value).ToString("yyyy-MM-dd");
                String strCheckVersion = "";
                if (this.uCheckVersion.Checked == true)
                {
                    strCheckVersion = "T";
                }
                else
                {
                    strCheckVersion = "F";
                }

                //호출
                DataTable dt = isoDocH.mfReadISODocHForDisuse(strPlantCode, strLTypeCode, strMTypeCode, strSTypCode, strStdNumber, strProcessCode, strDocTitle
                                                                , strCheckVersion, strApplyDateFrom, strApplyDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGrid.DataSource = dt;
                this.uGrid.DataBind();

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }
                //else
                //{
                //    WinGrid grd = new WinGrid();
                //    grd.mfSetAutoResizeColWidth(this.uGrid, 0);
                //}

                if (uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                DataTable dtISODocH = new DataTable();
                DataTable dtSaveVendor = new DataTable();
                DataTable dtDelVendor = new DataTable();
                DataTable dtSaveDept = new DataTable();
                DataTable dtDelDept = new DataTable();
                DataTable dtSaveAgreeUser = new DataTable();
                DataTable dtDelAgreeUser = new DataTable();
                DataTable dtSaveFile = new DataTable();
                DataTable dtDelFile = new DataTable();
                DataRow row;

                //// 결재선 팝업창
                QRPCOM.UI.frmCOM0012 frm = new QRPCOM.UI.frmCOM0012();
                DialogResult _dr = new DialogResult();

                #region 필수확인

                if (strAdmitStatusCode == "AR")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001021", "M000766", Infragistics.Win.HAlign.Center);
                    return;
                }
                int checkTitle = 0;
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (this.uGrid1.Rows[i].Cells["FileTitle"].Value.ToString() == "")
                    {
                        checkTitle++;
                    }
                }
                if (checkTitle > 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001228", "M001140", Infragistics.Win.HAlign.Center);

                    return;
                }
                if (this.uTextRev_DisUserID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001228", "M000162", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextRev_DisUserID.Focus();
                    return;
                }
                else if (this.uComboRev_DisSeparation.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001228", "M000161", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uComboRev_DisSeparation.DropDown();
                    return;
                }

                else if (this.uTextRev_DisReason.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001229", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextRev_DisReason.Focus();
                    return;
                }
                else if (this.uComboRev_DisSeparation.Value.ToString().Equals("1") && this.uTextChangeItem.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000596", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextChangeItem.Focus();
                    return;
                }
                else if (this.uComboRev_DisSeparation.Value.ToString().Equals("1") && this.uTextFrom.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000594", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextFrom.Focus();
                    return;
                }
                else if (this.uComboRev_DisSeparation.Value.ToString().Equals("1") && this.uTextTo.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000598", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextTo.Focus();
                    return;
                }
                else if (this.uDateRev_DisDate.Value.ToString() == "" || this.uDateRev_DisDate.Value == null)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000243", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uDateRev_DisDate.DropDown();
                    return;
                }
                else if (this.uTextAdmitVersionNum.ReadOnly == false && this.uTextAdmitVersionNum.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                   , "M001264", "M001228", "M000164", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextRevisionNo.Focus();
                    return;
                }
                else if (this.uTextAdmitVersionNum.ReadOnly == false && this.uTextAdmitVersionNum.Text != "")
                {
                    //개정인 경우만 개정번호 중복체크 한다.
                    if (this.uComboRev_DisSeparation.Value.ToString() == "1")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                        QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                        brwChannel.mfCredentials(isoDocH);

                        DataTable dtCheckStdNum = isoDocH.mfReadISODocHForReWrite(uTextPlant.Text, uTextStandardDocNo.Text, uTextAdmitVersionNum.Text, m_resSys.GetString("SYS_LANG"));
                        if (dtCheckStdNum.Rows.Count > 0)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001228", "M000347", Infragistics.Win.HAlign.Center);
                            //Focus
                            this.uTextAdmitVersionNum.Focus();
                            return;
                        }
                    }
                }
                int countIndex = 0;
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    //첨부파일이 없을경우 저장 불가

                    if (this.uGrid1.Rows[i].Hidden == false && this.uGrid1.Rows[i].CellAppearance.BackColor != Color.BlueViolet)
                    {
                        countIndex++;
                    }

                    if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("/")
                        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("#")
                        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("*")
                        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("<")
                        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(">"))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }
                if (countIndex == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001150", "M001149", "M001143", Infragistics.Win.HAlign.Right);

                    return;
                }
                #endregion
                //else
                //{

                //콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                ///////////////////////////////////////////
                //개정시 자동폐기 문제로 중간저장 삭제
                //DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.ReqAgreeSave, 500, 500,
                //    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //    "M001264", "M001053", "M000936",
                //    Infragistics.Win.HAlign.Right);

                DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    "M001264", "M001053", "M000936",
                    Infragistics.Win.HAlign.Right);
                //그룹웨어 전송Flag 확인
                QRPBrowser brwChannel1 = new QRPBrowser();
                brwChannel1.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                brwChannel1.mfCredentials(clsLType);

                DataTable dtLType = clsLType.mfReadISODocLTypeGRWFlag(PlantCode, m_LargeTypeCode);

                //if (DResult == DialogResult.Yes || DResult == DialogResult.No)
                if (DResult == DialogResult.Yes)
                {
                    #region 결재선 팝업창 호출 중간저장일경우, 고객사 표준일 경우는 호출하지 않는다.
                    ////결재선 팝업창 호출
                    if (dtLType.Rows[0]["GRWFlag"].ToString().Equals("Y"))
                    {
                        frm.WriteID = this.uTextRev_DisUserID.Text;
                        frm.WriteName = this.uTextRev_DisUserName.Text;
                        //frm.ApprovalUserID = "ST00176B";
                        //frm.ApprovalUserName = "전영미";

                        if (DResult == DialogResult.Yes)
                        {
                            _dr = frm.ShowDialog();
                            if (_dr == DialogResult.No || _dr == DialogResult.Cancel)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M001030", Infragistics.Win.HAlign.Center);

                                return;
                            }
                        }
                    }
                    #endregion

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    #region 데이터
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                    QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                    brwChannel.mfCredentials(isoDocH);

                    //FileServer 연결
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");


                    //첨부파일 이름(배열의 i번째 파일명이 Grid1의 i번째 파일명)
                    String[] strFile = new String[this.uGrid1.Rows.Count];
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty) && this.uGrid1.Rows[i].Hidden == false)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037"
                                , this.uGrid1.Rows[i].RowSelectorNumber + "M000591", Infragistics.Win.HAlign.Right);

                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["FileName"];
                            return;
                        }
                        else
                        {
                            if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo fileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                strFile[i] = fileDoc.Name;

                            }
                            else
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString() == string.Empty && this.uGrid1.Rows[i].Appearance.FontData.Underline == Infragistics.Win.DefaultableBoolean.False)
                                {
                                    String fName = this.uGrid1.Rows[i].Cells["FileName"].Value.ToString();
                                    String[] splitName = fName.Split(new char[] { '-' });
                                    if (splitName.Length > 5)
                                    {
                                        for (int j = 4; j < splitName.Length; j++)
                                        {
                                            if (!j.Equals(splitName.Length - 1))
                                                strFile[i] = strFile[i] + splitName[j] + "-";
                                            else
                                                strFile[i] = strFile[i] + splitName[j];
                                        }
                                    }
                                    else
                                    {
                                        strFile[i] = splitName[4];
                                    }
                                }
                            }
                        }
                    }
                    //개정번호(채번X 개정시 : TextBox값, 채번X 폐기시 : TextBox값, 채번o 개정시 : 개정번호의 최대값, 채번o 폐기시 : TextBox값)
                    //String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                    //String strStdNumber = this.uTextStandardDocNo.Text;
                    String MaxVersionNum = "";

                    //개정번호 입력불가(자동채번이거나 작성중 또는 승인요청인 경우)인 경우
                    if (this.uTextAdmitVersionNum.ReadOnly == true)
                    {
                        //개정번호가 없는 경우 : 자동채번
                        if (uTextAdmitVersionNum.Text == "")
                        {
                            //승인, 개정인 경우 = 최대개정번호+1
                            if (this.uComboRev_DisSeparation.Value.ToString().Equals("1") && strAdmitStatusCode.Equals("FN"))
                            {
                                String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                                String strStdNumber = this.uTextStandardDocNo.Text;
                                DataTable dtMaxVersion = isoDocH.mfReadMaxVersion(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), this.uTextStandardDocNo.Text);

                                int intLastVersion = Convert.ToInt32(dtMaxVersion.Rows[0]["VersionNum"].ToString()) + 1;

                                MaxVersionNum = intLastVersion.ToString("d3");

                            }
                            else
                            {
                                MaxVersionNum = this.uTextRevisionNo.Text;
                            }
                        }
                        //개정번호가 있는 경우
                        else
                        {
                            MaxVersionNum = this.uTextAdmitVersionNum.Text;
                        }
                    }
                    //개정번호 입력 가능한 경우
                    else
                    {
                        MaxVersionNum = this.uTextAdmitVersionNum.Text;

                        ////개정인 경우
                        //if (this.uComboRev_DisSeparation.Value.ToString().Equals("1"))
                        //{
                        //    if (this.uTextAdmitVersionNum.Text == "")
                        //    {
                        //        MaxVersionNum = this.uTextRevisionNo.Text;  //선택한 이전 개정번호
                        //    }
                        //    else
                        //    {
                        //        MaxVersionNum = this.uTextAdmitVersionNum.Text;
                        //    }
                        //}
                        //else
                        //{
                        //    MaxVersionNum = strRevText;  //선택한 이전 개정번호
                        //}
                    }

                    dtISODocH = isoDocH.mfSetDateInfo();

                    row = dtISODocH.NewRow();
                    row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                    row["StdNumber"] = this.uTextStandardDocNo.Text;
                    row["VersionNum"] = MaxVersionNum;
                    row["ProcessCode"] = this.uGrid.ActiveRow.Cells["ProcessCode"].Value.ToString();
                    row["LTypeCode"] = this.uGrid.ActiveRow.Cells["LTypeCode"].Value.ToString();
                    row["MTypeCode"] = this.uGrid.ActiveRow.Cells["MTypeCode"].Value.ToString();
                    row["STypeCode"] = this.uGrid.ActiveRow.Cells["STypeCode"].Value.ToString();
                    //row["MakeUserName"] = this.uTextMakeUserName.Text;
                    if (this.uComboRev_DisSeparation.Value.ToString().Equals("1"))
                    {
                        row["MakeUserName"] = this.uTextMakeUserName.Text;
                    }
                    else
                    {
                        row["MakeUserName"] = this.uTextRev_DisUserName.Text;
                    }
                    row["MakeDeptName"] = this.uTextMakeDeptName.Text;
                    //row["WriteID"] = this.uTextRev_DisUserID.Text;
                    if (this.uComboRev_DisSeparation.Value.ToString().Equals("1"))
                        row["WriteID"] = this.uTextRev_DisUserID.Text;
                    else
                        row["WriteID"] = this.uTextCreateUserID.Text;
                    //row["WriteDate"] = this.uTextCreateDate.Text;
                    row["AdmitID"] = this.uTextApproveUserID.Text;
                    row["AdmitDate"] = "";
                    row["RevDisuseType"] = this.uComboRev_DisSeparation.Value;
                    if (this.uComboRev_DisSeparation.Value.ToString() == "1")
                    {
                        row["RevDisuseDate"] = Convert.ToDateTime(this.uDateRev_DisDate.Value).ToString("yyyy-MM-dd");
                        row["WriteDate"] = this.uDateRev_DisDate.Value.ToString();
                        row["RevDisuseReason"] = this.uTextRev_DisReason.Text;
                    }
                    else
                    {
                        row["DisuseDate"] = Convert.ToDateTime(this.uDateRev_DisDate.Value).ToString("yyyy-MM-dd");
                        row["WriteDate"] = this.uDateRev_DisDate.Value.ToString();
                        row["DisuseReason"] = this.uTextRev_DisReason.Text;
                    }
                    row["RevDisuseID"] = this.uTextRev_DisUserID.Text;
                    row["ChangeItem"] = this.uTextChangeItem.Text;
                    row["FromDesc"] = this.uTextFrom.Text;
                    row["ToDesc"] = this.uTextTo.Text;
                    row["DocTitle"] = this.uTextTitle.Text;
                    row["DocKeyword"] = this.uTextDocKeyword.Text;
                    row["DocDesc"] = this.uTextEtc.Text;
                    row["GRWComment"] = this.uTextGRWComment.Text;
                    row["ApplyDateFrom"] = this.uDateApplyFromDate.Value.ToString();
                    row["ApplyDateTo"] = this.uDateApplyToDate.Value.ToString();

                    //요기서 에러가 나서 null인 경우 체크 추가(2011.12.16.Ryu)
                    if (this.uDateCompleteDate.Value == null)
                        row["CompleteDate"] = "";
                    else
                        row["CompleteDate"] = this.uDateCompleteDate.Value.ToString();

                    row["ReturnReason"] = this.uTextReturnReason.Text;
                    row["Package"] = this.uTextPackage.Text;
                    row["ChipSize"] = this.uTextChipSize.Text;
                    row["PadSize"] = this.uTextPadSize.Text;

                    if (DResult == DialogResult.Yes)
                    {
                        row["AdmitStatus"] = "AR";
                    }

                    else if (DResult == DialogResult.No)
                    {
                        row["AdmitStatus"] = "WR";
                    }
                    else
                    {
                        return;
                    }

                    dtISODocH.Rows.Add(row);

                    //////Vendor//////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                    QRPISO.BL.ISODOC.ISODocVendor Vendor = new QRPISO.BL.ISODOC.ISODocVendor();
                    brwChannel.mfCredentials(Vendor);

                    dtSaveVendor = Vendor.mfSetDateInfo();
                    dtDelVendor = Vendor.mfSetDateInfo();

                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                        if (this.uGrid2.Rows[i].Hidden == false)
                        {
                            row = dtSaveVendor.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = MaxVersionNum;
                            row["Seq"] = this.uGrid2.Rows[i].RowSelectorNumber;
                            row["VendorCode"] = this.uGrid2.Rows[i].Cells["VendorCode"].Value.ToString();
                            row["DeptName"] = this.uGrid2.Rows[i].Cells["DeptName"].Value.ToString();
                            row["PersonName"] = this.uGrid2.Rows[i].Cells["PersonName"].Value.ToString(); ;
                            row["Hp"] = this.uGrid2.Rows[i].Cells["Hp"].Value.ToString();
                            row["Email"] = this.uGrid2.Rows[i].Cells["Email"].Value.ToString();
                            row["EtcDesc"] = this.uGrid2.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveVendor.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelVendor.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = staticVersionNum;
                            row["Seq"] = this.uGrid2.Rows[i].Cells["Seq"].Value;

                            dtDelVendor.Rows.Add(row);
                        }
                    }

                    /////AgreeUser//////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                    QRPISO.BL.ISODOC.ISODocAgreeUser AgreeUser = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                    brwChannel.mfCredentials(AgreeUser);

                    dtSaveAgreeUser = AgreeUser.mfSetDateInfo();
                    dtDelAgreeUser = AgreeUser.mfSetDateInfo();

                    for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                    {
                        this.uGrid4.ActiveCell = this.uGrid4.Rows[0].Cells[0];
                        if (this.uGrid4.Rows[i].Hidden == false)
                        {
                            row = dtSaveAgreeUser.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = MaxVersionNum;
                            row["AgreeUserID"] = this.uGrid4.Rows[i].Cells["AgreeUserID"].Value.ToString();
                            row["EtcDesc"] = this.uGrid4.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveAgreeUser.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelAgreeUser.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["Version"] = staticVersionNum;
                            row["AgreeUserID"] = uGrid4.Rows[i].Cells["AgreeUserID"].Value.ToString();

                            dtDelAgreeUser.Rows.Add(row);
                        }
                    }

                    ///////Dept////////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                    QRPISO.BL.ISODOC.ISODocDept Dept = new QRPISO.BL.ISODOC.ISODocDept();
                    brwChannel.mfCredentials(Dept);

                    dtSaveDept = Dept.mfSetDateInfo();
                    dtDelDept = Dept.mfSetDateInfo();

                    for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                    {
                        this.uGrid3.ActiveCell = this.uGrid3.Rows[0].Cells[0];
                        if (this.uGrid3.Rows[i].Hidden == false)
                        {
                            row = dtSaveDept.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = MaxVersionNum;
                            row["ProcessGroup"] = this.uGrid3.Rows[i].Cells["ProcessGroup"].Value.ToString();
                            row["EtcDesc"] = this.uGrid3.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveDept.Rows.Add(row);

                        }
                        else
                        {
                            row = dtDelDept.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = staticVersionNum;
                            row["ProcessGroup"] = this.uGrid3.Rows[i].Cells["ProcessGroup"].Value.ToString();
                            dtDelDept.Rows.Add(row);
                        }
                    }
                    #endregion

                    #region 파일
                    /////////////////File//////////////////////////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                    QRPISO.BL.ISODOC.ISODocFile DFile = new QRPISO.BL.ISODOC.ISODocFile();
                    brwChannel.mfCredentials(DFile);

                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (strAdmitStatusCode.Equals("FN") && !this.uGrid1.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            this.uGrid1.Rows[i].Hidden = true;
                        }

                        if (this.uGrid1.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            if (this.uGrid1.Rows[i].Hidden == false)
                            {
                                if (this.uGrid1.Rows[i].RowSelectorNumber.Equals(1))
                                {
                                    this.uGrid1.Rows[i].Cells["Seq"].Value = "1";
                                }
                                else
                                {
                                    this.uGrid1.Rows[i].Cells["Seq"].Value = Convert.ToString(Convert.ToInt32(this.uGrid1.Rows[i - 1].Cells["Seq"].Value.ToString()) + 1);
                                }
                            }
                        }
                    }

                    dtSaveFile = DFile.mfSetDatainfo();
                    dtDelFile = DFile.mfSetDatainfo();

                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {

                        this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];
                        if (this.uGrid1.Rows[i].Hidden == false)
                        {
                            row = dtSaveFile.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = MaxVersionNum;
                            row["Seq"] = Convert.ToInt32(this.uGrid1.Rows[i].Cells["Seq"].Value.ToString());
                            row["FileTitle"] = this.uGrid1.Rows[i].Cells["FileTitle"].Value.ToString();
                            row["FileName"] = strFile[i];

                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                            {
                                row["DWFlag"] = "T";
                            }
                            else
                            {
                                row["DWFlag"] = "F";
                            }

                            row["EtcDesc"] = this.uGrid1.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveFile.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelFile.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = MaxVersionNum;
                            row["Seq"] = this.uGrid1.Rows[i].Cells["Seq"].Value;
                            dtDelFile.Rows.Add(row);
                        }
                    }
                    #endregion

                    //메시지 박스
                    System.Windows.Forms.DialogResult result;

                    //처리로직
                    //저장 함수 호출
                    string rtMSG = isoDocH.mfSaveISODocHForDisuse(dtISODocH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveVendor, dtDelVendor
                                                                   , dtSaveAgreeUser, dtDelAgreeUser, dtSaveDept, dtDelDept, dtSaveFile, dtDelFile);

                    //decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                    //처리로직끝
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    string StdNumber = string.Empty;

                    if (this.uComboRev_DisSeparation.ReadOnly == true)
                    {
                        StdNumber = this.uTextStandardDocNo.Text;
                    }
                    else
                    {
                        StdNumber = ErrRtn.mfGetReturnValue(0);
                    }

                    String CreateFolderName = StdNumber + "-" + MaxVersionNum;
                    if (ErrRtn.ErrNum == 0)
                    {
                        #region  파일 업로드
                        //File UpLoad
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        frmCOMFileAttach fileCopy = new frmCOMFileAttach();
                        ArrayList arrAtt = new ArrayList();
                        ////ArrayList arrCopy = new ArrayList();
                        ////ArrayList arrTarget = new ArrayList();

                        //파일 업로드 먼저

                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (this.uGrid1.Rows[i].Hidden == false)
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                                {
                                    FileInfo fileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                    string strUploadFile = fileDoc.DirectoryName + "\\" + StdNumber + "-"
                                                            + Convert.ToString(MaxVersionNum)
                                                            + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-"
                                                            + this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString() + "-" + fileDoc.Name;

                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);

                                    File.Copy(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString(), strUploadFile);
                                    arrAtt.Add(strUploadFile);
                                }
                                else
                                {
                                    FileInfo fileDoc = new FileInfo(strFile[i]);

                                    String fName = StdNumber + "-" + MaxVersionNum + "-"
                                                    + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-" + this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString() + "-" + strFile[i];


                                    ////String DestFile = "ISODocFile/" + this.uTextStandardDocNo.Text + "-" + MaxVersionNum + "/" + fName;
                                    ////String targetFile = "ISODocFile/" + this.uTextStandardDocNo.Text + "-" + this.uTextInnerAdmitVersionNum.Text + "/"
                                    ////                        + this.uTextStandardDocNo.Text + "-" + this.uTextInnerAdmitVersionNum.Text + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString()
                                    ////                        + "-" + this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString() + "-" + 
                                    ////                        strFile[i];

                                    ////arrCopy.Add(DestFile);
                                    ////arrTarget.Add(targetFile);
                                }
                            }
                        }
                        //새로 추가된 파일을 업로드
                        fileAtt.mfInitSetSystemFileInfo(arrAtt, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                      dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                      dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + CreateFolderName,
                                                                      dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                      dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        if (arrAtt.Count != 0)
                        {
                            fileAtt.mfFileUpload_NonAssync();
                        }
                        //////기존 파일을 복사
                        ////fileCopy.mfInitSetSystemFileCopyInfo(arrCopy
                        ////                                        , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                        ////                                        , arrTarget
                        ////                                        , dtFilePath.Rows[0]["ServerPath"].ToString()
                        ////                                        , "ISODocFile\\" + CreateFolderName
                        ////                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                        ////                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        ////if (arrCopy.Count != 0)
                        ////{
                        ////    fileCopy.mfFileUpload_NonAssync();
                        ////}
                        #endregion

                        #region 그룹웨어 전송 중간저장일 경우, 대분류가 고객사 표준일 경우는 전송하지 않는다.
                        //////// 테스트 코드 -- 업로드시 주석 처리
                        ////frm.dtSendLine.Rows[0]["CD_USERKEY"] = "tica100";
                        ////frm.dtSendLine.Rows[1]["CD_USERKEY"] = "tica100";
                        ////frm.dtSendLine.Rows[2]["CD_USERKEY"] = "mes_pjt12";
                        ////
                        if (DResult == DialogResult.Yes)
                        {
                            if (dtLType.Rows[0]["GRWFlag"].ToString().Equals("Y"))
                            {
                                dtISODocH.Rows[0]["StdNumber"] = StdNumber;
                                DataTable dtRtn = isoDocH.mfISODOCGRWApproval(dtISODocH, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), frm.Comment);
                                //DataTable dtRtn = mfISODOCGRWApproval(dtISODocH, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                                if (dtRtn == null) // 실패
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001135", "M001037", "M000328", Infragistics.Win.HAlign.Center);

                                    return;
                                }
                                else if (dtRtn.Rows.Count.Equals(0)) // 실패
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001135", "M001037", "M000328", Infragistics.Win.HAlign.Center);

                                    return;
                                }
                                else if (!dtRtn.Rows[0]["CD_CODE"].Equals("00")) // 실패
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001135", "M001037", "M000328", Infragistics.Win.HAlign.Center);

                                    return;
                                }
                            }
                            else
                            {
                                //public String mfSaveISODocHAutoDisus(DataTable dtISODocH, string strUserIP, string strUserID)
                                string stRtn = isoDocH.mfSaveISODocHAutoDisus(dtISODocH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                                //TransErrRtn ErrRtn = new TransErrRtn();
                                //ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                                ErrRtn = ErrRtn.mfDecodingErrMessage(stRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                         Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001536", "M001537",
                                         "M001535", Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                        #endregion

                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001135", "M001037", "M000930",
                                               Infragistics.Win.HAlign.Right);

                        InitValue();
                        GridWritable();
                        mfSearch();
                        this.uGroupBoxContentsArea.Expanded = false;

                    }

                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                    }

                }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 테스트용 사용금지
        //////////public DataTable mfISODOCGRWApproval(DataTable dtISODocH, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        //////////{
        //////////    // LegacyKey, DocNo 생성시 "||" ( double pipe ) 로 구분

        //////////    DataTable dtRtn = new DataTable();

        //////////    try
        //////////    {
        //////////        QRPBrowser brwChannel = new QRPBrowser();
        //////////        brwChannel.mfRegisterChannel(typeof(QRPGRW.IF.QRPGRW), "QRPGRW");
        //////////        QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
        //////////        brwChannel.mfCredentials(grw);



        //////////        // LegacyKey 
        //////////        string strPlantCode = dtISODocH.Rows[0]["PlantCode"].ToString();
        //////////        string strStdNumber = dtISODocH.Rows[0]["StdNumber"].ToString();
        //////////        string strVersionNum = dtISODocH.Rows[0]["VersionNum"].ToString();
        //////////        string strLegacyKey = strPlantCode + "||" + strStdNumber + "||" + strVersionNum;

        //////////        // 첨부파일 조회
        //////////        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
        //////////        QRPISO.BL.ISODOC.ISODocFile file = new QRPISO.BL.ISODOC.ISODocFile();
        //////////        brwChannel.mfCredentials(file);

        //////////        DataTable dtFile = file.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, "KOR");

        //////////        // Brains 전송용 DataTable
        //////////        DataTable dtFileinfo = grw.mfSetFileInfoDataTable();

        //////////        // 표준문서 기본 파일경로 
        //////////        string strFilePath = "ISODocFile\\";

        //////////        // Brains 전송용 DataTable 만들기
        //////////        foreach (DataRow dr in dtFile.Rows)
        //////////        {
        //////////            DataRow _dr = dtFileinfo.NewRow();
        //////////            _dr["PlantCode"] = dr["PlantCode"].ToString();
        //////////            _dr["NM_LOCATION"] = strFilePath + strStdNumber + "-" + strVersionNum;  // 표준문서 기본 경로 + 문서별 경로
        //////////            _dr["NM_FILE"] = dr["FileName"].ToString();                             // 파일명
        //////////            dtFileinfo.Rows.Add(_dr);
        //////////        }

        //////////        if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals(string.Empty)) // 생성
        //////////        {
        //////////            string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
        //////////            string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
        //////////            string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
        //////////            string strComment = dtISODocH.Rows[0]["DocDesc"].ToString();

        //////////            dtRtn = grw.CreateISODOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strDocUser, strComment, dtFormInfo, dtFileinfo, strUserIP, strUserID);
        //////////        }
        //////////        else if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals("1")) // 개정
        //////////        {
        //////////            string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
        //////////            string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
        //////////            string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
        //////////            string strComment = dtISODocH.Rows[0]["DocDesc"].ToString();
        //////////            string strDocChange = dtISODocH.Rows[0]["ChangeItem"].ToString();
        //////////            string strFrom = dtISODocH.Rows[0]["FromDesc"].ToString();
        //////////            string strTo = dtISODocH.Rows[0]["ToDesc"].ToString();

        //////////            dtRtn = grw.ModifyISIDOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strDocUser, strComment, strDocChange, strFrom, strTo, dtFormInfo, dtFileinfo, strUserIP, strUserID);
        //////////        }
        //////////        else if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals("2")) // 폐기
        //////////        {
        //////////            string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
        //////////            string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
        //////////            string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
        //////////            string strComment = dtISODocH.Rows[0]["DocDesc"].ToString();
        //////////            string strReason = dtISODocH.Rows[0]["DisuseReason"].ToString();

        //////////            dtRtn = grw.CancelISODOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strReason, strDocUser, strComment, dtFormInfo, dtFileinfo, strUserIP, strUserID);
        //////////        }
        //////////    }
        //////////    catch (Exception ex)
        //////////    {
        //////////        return dtRtn;
        //////////        throw (ex);
        //////////    }
        //////////    return dtRtn;
        //////////}
        #endregion

        public void mfDelete()
        {
            try
            {
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //WinMessageBox msg = new WinMessageBox();
                //DialogResult DResult = new DialogResult();
                ////표준문서등록에서 "작성중"이거나 "반려"중인 거은 "삭제"가 가능하도록 처리
                //if (this.uGroupBoxContentsArea.Expanded == false)
                //{
                //    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                    "처리결과", "저장처리결과", "삭제할 문서를 선택해주세요",
                //                                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                //else if (strAdmitStatusCode.Equals("AR"))
                //{
                //    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                    "처리결과", "저장처리결과", "승인요청문서는 삭제할수 없습니다.",
                //                                    Infragistics.Win.HAlign.Right);
                //    return;
                //}
                //else if (strAdmitStatusCode.Equals("FN"))
                //{
                //    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                   "처리결과", "저장처리결과", "승인완료문서는 삭제살수 없습니다.",
                //                                   Infragistics.Win.HAlign.Right);
                //    return;
                //}
                //else
                //{
                //    QRPBrowser brwChannel = new QRPBrowser();
                //    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                //    QRPISO.BL.ISODOC.ISODocH clsDocH = new QRPISO.BL.ISODOC.ISODocH();
                //    brwChannel.mfCredentials(clsDocH);

                //    String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                //    String strStdNumber = this.uTextStandardDocNo.Text;

                //    DataTable dtDelDoc = clsDocH.mfSetDateInfo();
                //    DataRow dr;

                //    dr = dtDelDoc.NewRow();

                //    dr["PlantCode"] = strPlantCode;
                //    dr["StdNumber"] = strStdNumber;
                //    dr["VersionNum"] = uTextRevisionNo.Text;

                //    dtDelDoc.Rows.Add(dr);

                //    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                              "확인창", "삭제확인", "선택한 표준문서를 삭제하겠습니까?",
                //                              Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                //    {
                //        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //        Thread t1 = m_ProgressPopup.mfStartThread();
                //        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                //        this.MdiParent.Cursor = Cursors.WaitCursor;

                //        string rtMSG = clsDocH.mfDeleteISODocH(dtDelDoc);

                //        //Decoding
                //        TransErrRtn ErrRtn = new TransErrRtn();
                //        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                //        this.MdiParent.Cursor = Cursors.Default;
                //        m_ProgressPopup.mfCloseProgressPopup(this);

                //        if (ErrRtn.ErrNum == 0)
                //        {
                //            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                          "처리결과", "삭제처리결과", "입력한 정보를 성공적으로 삭제했습니다.",
                //                          Infragistics.Win.HAlign.Right);
                //            InitValue();
                //            mfSearch();
                //        }
                //        else
                //        {
                //            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                          "처리결과", "삭제처리결과", "입력한 정보를 삭제하지 못했습니다.",
                //                          Infragistics.Win.HAlign.Right);
                //        }
                //    }
                //}
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
                InitValue();
                SetWritable();
                GridWritable();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트들...
        // ContentsGroupBox 상태변화 이벤트



        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 160);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 40;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 720;

                    for (int i = 0; i < uGrid.Rows.Count; i++)
                    {
                        uGrid.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        // 첨부파일 그리드 행삭제 버튼 이벤트

        private void uButtonDeleteRow1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid1.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 배포업체 그리드 행삭제 버튼 이벤트

        private void uButtonDeleteRow2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid2.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow3_Click(object sender, EventArgs e)
        {
            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid3.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid3.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow4_Click(object sender, EventArgs e)
        {
            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid4.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid4.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자동 행삭제와 수정시 rowselector 이미지를 변하게 하기 위한 이벤트

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
                if (e.Cell.Column.Key == "FileName")
                {
                    if (e.Cell.Value.ToString().Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(e.Cell.Value.ToString());
                        e.Cell.Row.Cells["FileTitle"].Value = fileDoc.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자동 행삭제와 수정시 rowselector 이미지를 변하게 하기 위한 이벤트

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                //배포업체 검색시 담당자 검색 이벤트
                if (e.Cell.Column.Key.Equals("VendorCode"))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();

                    //VendorP 검색 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    DataTable dtVendorP = new DataTable();

                    dtVendorP = clsVendor.mfReadVendorPCombo(this.uGrid2.ActiveRow.Cells["VendorCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    WinGrid wGrid = new WinGrid();

                    e.Cell.Row.Cells["PersonName"].Column.Layout.ValueLists.Clear();
                    String strDropDownValue = "Seq,PersonName,DeptName,Hp,EMail,EtcDesc";
                    String strDropDownText = "순번,담당자,부서,전화번호,E-Mail,비고";

                    wGrid.mfSetGridCellValueGridList(this.uGrid2, 0, e.Cell.Row.Index, "PersonName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                        , strDropDownValue, strDropDownText, "Seq", "PersonName", dtVendorP);
                    if (dtVendorP.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                           , "M001264", "M000367", "M001249", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid3, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid4_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid4, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion



        //공장코드 변경시 대분류, 공정 콤보박스 변경
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinComboEditor wComboLType = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                DataTable dtLType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProcess.Items.Clear();
                this.uComboSearchLarge.Items.Clear();

                strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(clsLType);

                    dtLType = clsLType.mfReadISODocLTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
                wCombo.mfSetComboEditor(this.uComboSearchLarge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                       , Infragistics.Win.HAlign.Left, "", "", "선택", "LTypeCode", "LTypeName", dtLType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //대분류 콤보박스 변화시 중분류 콤보박스 초기화 및 중분류 콤보박스 정의
        private void uComboSearchLarge_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtMType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();

                this.uComboSearchMiddle.Items.Clear();

                if (strLTypeCode != "")
                {
                    //중분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                    QRPISO.BL.ISODOC.ISODocMType clsMType = new QRPISO.BL.ISODOC.ISODocMType();
                    brwChannel.mfCredentials(clsMType);

                    dtMType = clsMType.mfReadISODocMTypeCombo(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMiddle, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "MTypeCode", "MTypeName", dtMType);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        //중분류 콤보박스 변화시 소분류 콤보박스 초기화 및 소분류 콤보박스 정의
        private void uComboSearchMiddle_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtSType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();

                this.uComboSearchSmall.Items.Clear();

                if (strMTypeCode != "")
                {
                    //소분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
                    QRPISO.BL.ISODOC.ISODocSType clsSType = new QRPISO.BL.ISODOC.ISODocSType();
                    brwChannel.mfCredentials(clsSType);

                    dtSType = clsSType.mfReadISODocSTypeCombo(strPlantCode, strLTypeCode, strMTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchSmall, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "STypeCode", "STypeName", dtSType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //개정 / 폐기자 버튼 클릭 이벤트
        private void uTextRev_DisUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    if (PlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M001204",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }

                    frmPOP0011 UserPop = new frmPOP0011();
                    UserPop.PlantCode = PlantCode;
                    UserPop.ShowDialog();

                    this.uTextRev_DisUserID.Text = UserPop.UserID;
                    this.uTextRev_DisUserName.Text = UserPop.UserName;
                    this.uTextRevUserName.Text = UserPop.UserName;
                    this.uTextMakeUserName.Text = UserPop.UserName;
                    this.uTextMakeDeptName.Text = UserPop.DeptName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        //승인자 버튼 클릭 이벤트
        private void uTextApproveUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    if (PlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M001204",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 UserPop = new frmPOP0011();
                    UserPop.PlantCode = PlantCode;
                    UserPop.ShowDialog();

                    this.uTextApproveUserID.Text = UserPop.UserID;
                    this.uTextApproveUserName.Text = UserPop.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드 더블클릭시 검색
        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                e.Row.Fixed = true;
                InitValue();
                GridWritable();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPopup = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //함수호출
                QRPBrowser brwChannel = new QRPBrowser();

                //Header
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH header = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(header);
                //AgreeUser
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                QRPISO.BL.ISODOC.ISODocAgreeUser agreeUser = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                brwChannel.mfCredentials(agreeUser);
                //Dept
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                QRPISO.BL.ISODOC.ISODocDept dept = new QRPISO.BL.ISODOC.ISODocDept();
                brwChannel.mfCredentials(dept);
                //Vendor
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                QRPISO.BL.ISODOC.ISODocVendor vendor = new QRPISO.BL.ISODOC.ISODocVendor();
                brwChannel.mfCredentials(vendor);
                //File
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                QRPISO.BL.ISODOC.ISODocFile file = new QRPISO.BL.ISODOC.ISODocFile();
                brwChannel.mfCredentials(file);


                //헤더용
                String strVersionNum = this.uGrid.ActiveCell.Row.Cells["VersionNum"].Value.ToString();
                //공통인수
                String strPlantCode = this.uGrid.ActiveCell.Row.Cells["PlantCode"].Value.ToString();
                String strStdNumber = this.uGrid.ActiveRow.Cells["StdNumber"].Value.ToString();
                PlantCode = strPlantCode;

                //저장 프로세스용 대분류코드 저장
                m_LargeTypeCode = this.uGrid.ActiveRow.Cells["LTypeCode"].Value.ToString();

                //Header     
                DataTable dtHeader = header.mfReadISODocHForRevision(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));

                staticVersionNum = dtHeader.Rows[0]["VersionNum"].ToString();

                this.uTextStandardDocNo.Text = dtHeader.Rows[0]["StdNumber"].ToString();
                this.uTextPlant.Text = dtHeader.Rows[0]["PlantCode"].ToString();
                //시스템내부 개정번호 (PK용)
                this.uTextRevisionNo.Text = dtHeader.Rows[0]["VersionNum"].ToString();

                //승인시에 채번되는 개정번호
                this.uTextInnerAdmitVersionNum.Text = dtHeader.Rows[0]["AdmitVersionNum"].ToString();

                this.uTextLarge.Text = dtHeader.Rows[0]["LTypeName"].ToString();
                this.uTextMiddle.Text = dtHeader.Rows[0]["MTypeName"].ToString();
                this.uTextSmall.Text = dtHeader.Rows[0]["STypeName"].ToString();
                this.uTextMakeUserName.Text = dtHeader.Rows[0]["MakeUserName"].ToString();
                this.uTextMakeDeptName.Text = dtHeader.Rows[0]["MakeDeptName"].ToString();
                this.uTextCreateUserID.Text = dtHeader.Rows[0]["WriteID"].ToString();
                this.uTextCreateUserName.Text = dtHeader.Rows[0]["WriteName"].ToString();
                this.uTextCreateDate.Text = dtHeader.Rows[0]["WriteDate"].ToString();
                this.uComboRev_DisSeparation.Value = dtHeader.Rows[0]["RevDisuseType"].ToString();

                //개정/폐기 콤보에 따라 개정번호에 대한 속성이 바뀌므로 개정폐기 콤보값을 적용한 후 개정번호를 넣는다.
                if (dtHeader.Rows[0]["AdmitStatusCode"].ToString() == "WR" || dtHeader.Rows[0]["AdmitStatusCode"].ToString() == "RE")
                    this.uTextAdmitVersionNum.Text = dtHeader.Rows[0]["VersionNum"].ToString();
                //this.uTextAdmitVersionNum.Text = dtHeader.Rows[0]["AdmitVersionNum"].ToString();

                if (dtHeader.Rows[0]["RevDisuseType"].ToString() == "1")
                {
                    this.uTextRev_DisReason.Text = dtHeader.Rows[0]["RevDisuseReason"].ToString();
                    this.uDateRev_DisDate.Value = dtHeader.Rows[0]["RevDisuseDate"].ToString();
                }
                else
                {
                    this.uTextRev_DisReason.Text = dtHeader.Rows[0]["DisuseReason"].ToString();
                    this.uDateRev_DisDate.Value = dtHeader.Rows[0]["DisuseDate"].ToString();
                }
                //정상적으로 저장시는 개정일이 저장됨
                if (this.uDateRev_DisDate.Value == null)
                {
                    this.uDateRev_DisDate.Value = DateTime.Now;
                }
                this.uTextRev_DisReason.Text = dtHeader.Rows[0]["RevDisuseReason"].ToString();
                this.uTextChangeItem.Text = dtHeader.Rows[0]["ChangeItem"].ToString();
                this.uTextFrom.Text = dtHeader.Rows[0]["FromDesc"].ToString();
                this.uTextTo.Text = dtHeader.Rows[0]["ToDesc"].ToString();
                this.uTextGRWComment.Text = dtHeader.Rows[0]["GRWComment"].ToString();
                this.uTextApproveUserID.Text = dtHeader.Rows[0]["AdmitID"].ToString();
                this.uTextApproveUserName.Text = dtHeader.Rows[0]["AdmitName"].ToString();
                this.uTextDocState.Text = dtHeader.Rows[0]["AdmitStatus"].ToString();
                this.uTextTitle.Text = dtHeader.Rows[0]["DocTitle"].ToString();
                this.uTextDocKeyword.Text = dtHeader.Rows[0]["DocKeyword"].ToString();
                this.uDateApplyFromDate.Value = dtHeader.Rows[0]["ApplyDateFrom"].ToString();
                if (this.uDateApplyFromDate.Value == null)
                    this.uDateApplyFromDate.Value = DateTime.Now;
                this.uDateApplyToDate.Value = dtHeader.Rows[0]["ApplyDateTo"].ToString();
                if (this.uDateApplyToDate.Value == null)
                    this.uDateApplyToDate.Value = DateTime.Now;
                this.uDateCompleteDate.Value = dtHeader.Rows[0]["CompleteDate"].ToString();
                this.uTextEtc.Text = dtHeader.Rows[0]["DocDesc"].ToString();
                this.uTextPackage.Text = dtHeader.Rows[0]["Package"].ToString();
                this.uTextChipSize.Text = dtHeader.Rows[0]["ChipSize"].ToString();
                this.uTextPadSize.Text = dtHeader.Rows[0]["PadSize"].ToString();
                strAdmitStatusCode = dtHeader.Rows[0]["AdmitStatusCode"].ToString();
                if (strAdmitStatusCode.Equals("RE"))
                    strAdmitStatusCode = "WR";

                WinGrid grd = new WinGrid();
                //AgreeUser Start//
                DataTable dtAgree = agreeUser.mfReadISODocAgreeUser(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid4.DataSource = dtAgree;
                this.uGrid4.DataBind();
                if (dtAgree.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid4, 0);
                //AGreeUser End//

                //DeptStart//
                DataTable dtDept = dept.mfReadISODocDept(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid3.DataSource = dtDept;
                this.uGrid3.DataBind();
                if (dtDept.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid3, 0);
                //Dept End//

                //Vendor Start//
                DataTable dtVendor = vendor.mfReadISODocVendor(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                DataTable dtVendorG = vendor.mfReadISODocVendorGird(m_resSys.GetString("SYS_LANG"));
                this.uGrid2.DataSource = dtVendor;
                this.uGrid2.DataBind();
                if (dtVendor.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                //Vendor End//

                //File Start//
                DataTable dtFile = file.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid1.DataSource = dtFile;
                this.uGrid1.DataBind();
                if (dtFile.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);

                for (int i = 0; i < dtFile.Rows.Count; i++)
                {
                    if (dtFile.Rows[i]["DWFlag"].ToString() == "T")
                    {
                        this.uGrid1.Rows[i].Cells["DWFlag"].Value = true;
                    }
                    else
                    {
                        this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                    }
                }

                //대분류 검색, 자동채번이 false일경우 개정번호 입력제한 풀기
                String strPlant = dtHeader.Rows[0]["PlantCode"].ToString();
                String strLTypeCode = dtHeader.Rows[0]["LTypeCode"].ToString();
                String strCheck = checkReadonly(strPlant, strLTypeCode);
                //자동채번이 안되는 경우 개정번호 입력가능
                if (strCheck.Equals("Y"))
                {
                    m_bolAutoNumbering = false;
                    this.uTextAdmitVersionNum.ReadOnly = false;
                    this.uTextAdmitVersionNum.Appearance.BackColor = Color.PowderBlue;
                }
                //자동채번이 되는 경우 개정번호 입력불가능
                else
                {
                    m_bolAutoNumbering = true;
                    this.uTextAdmitVersionNum.ReadOnly = true;
                    this.uTextAdmitVersionNum.Appearance.BackColor = Color.Gainsboro;
                }

                //결재상태에 따라 컨트롤 설정
                //작성중인 경우
                if (strAdmitStatusCode.Equals("WR") || strAdmitStatusCode.Equals("RE"))
                {
                    //작성중인 문서의 경우   개정 / 폐기상태, 문서제목을 변경할수 없다.
                    this.uTextAdmitVersionNum.ReadOnly = true;
                    this.uTextAdmitVersionNum.Appearance.BackColor = Color.Gainsboro;

                    this.uComboRev_DisSeparation.Value = dtHeader.Rows[0]["RevDisuseType"].ToString();
                    this.uComboRev_DisSeparation.ReadOnly = true;
                    this.uComboRev_DisSeparation.Appearance.BackColor = Color.Gainsboro;

                    this.uTextTitle.ReadOnly = true;
                    this.uTextTitle.Appearance.BackColor = Color.Gainsboro;
                }
                //승인요청인 경우
                else if (strAdmitStatusCode.Equals("AR"))
                {
                    //전체 수정 불가로 변경(승인요청된 문서의 경우는 수정 불가)
                    SetDisWritable();
                    GridDiswritable();
                }

                strRevText = this.uTextRevisionNo.Text;
                //File End//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGroupBoxContentsArea.Expanded = true;

                //고객사 표준인 경우만 ChipSize, PadSize, Package 보이도록 처리
                if (e.Row.Cells["LTypeCode"].Value.ToString().Equals("Cust_Spec"))
                {
                    this.uTextChipSize.ReadOnly = false;
                    this.uTextChipSize.Appearance.BackColor = Color.White;
                    this.uTextChipSize.Visible = true;
                    this.uLabelChipSize.Visible = true;
                    this.uTextPadSize.ReadOnly = false;
                    this.uTextPadSize.Appearance.BackColor = Color.White;
                    this.uTextPadSize.Visible = true;
                    this.uLabelPadSize.Visible = true;
                    this.uTextPackage.ReadOnly = false;
                    this.uTextPackage.Appearance.BackColor = Color.White;
                    this.uTextPackage.Visible = true;
                    this.uLabelPackage.Visible = true;
                }
                else
                {
                    this.uTextChipSize.Visible = false;
                    this.uTextPackage.Visible = false;
                    this.uTextPadSize.Visible = false;
                    this.uLabelChipSize.Visible = false;
                    this.uLabelPackage.Visible = false;
                    this.uLabelPadSize.Visible = false;
                }

                //임시표준인 경우 적용일자에서 ToDate를 보이게 처리
                if (!e.Row.Cells["LTypeCode"].Value.ToString().Equals("ECN"))
                {
                    this.uLabelApplyDate.Visible = false;
                    this.uDateApplyFromDate.Visible = false;
                    this.uDateApplyToDate.Visible = false;
                    this.ultraLabel3.Visible = false;
                }
                else
                {
                    this.uLabelApplyDate.Visible = true;
                    this.uDateApplyFromDate.Visible = true;
                    this.uDateApplyToDate.Visible = true;
                    this.ultraLabel3.Visible = true;
                }

                //대분류가 고객사 표준일경우 작성자 수정 가능
                if (e.Row.Cells["LTypeCode"].Value.ToString().Equals("Cust_Spec"))
                {
                    this.uTextRevUserName.Text = m_resSys.GetString("SYS_USERNAME");
                    this.uTextRevUserName.Appearance.BackColor = Color.White;
                    this.uTextRevUserName.ReadOnly = false;
                }
                else
                {
                    this.uTextRevUserName.Text = m_resSys.GetString("SYS_USERNAME");
                    this.uTextRevUserName.Appearance.BackColor = Color.Gainsboro;
                    this.uTextRevUserName.ReadOnly = true;
                }
                //검색문서가 승인 완료인경우(개정을 하기 위해 들어왔을때)는 FROM, TO, 변경항목, 개정/폐기 사유, 개정/폐기 구분, 전자결제Comment를 초기화
                //검색문서가 작성중, 승인요청중, 반려일 경우(승인되지 않은 문서) FROM, TO, 변경항목, 개정/폐기사유, 개정/폐기 구분, 전자결제Comment를 표시해줌
                if (dtHeader.Rows[0]["AdmitStatusCode"].ToString() == "FN")
                {
                    this.uTextFrom.Text = "";
                    this.uTextTo.Text = "";
                    this.uTextChangeItem.Text = "";
                    this.uTextRev_DisReason.Text = "";
                    this.uComboRev_DisSeparation.Value = "";
                    ////this.uTextGRWComment.Text = "";
                }
                if (this.uTextRev_DisUserID.Visible = true && !this.uTextRev_DisUserID.Text.Equals(string.Empty))
                {
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);
                    DataTable dtUser = clsUser.mfReadSYSUser(dtHeader.Rows[0]["PlantCode"].ToString(), this.uTextRev_DisUserID.Text, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextDeptName.Text = dtUser.Rows[0]["DeptName"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// FileDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid1.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\")
                            || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        String strAdmitVerNum = "";
                        if (strAdmitStatusCode.Equals("FN"))
                            strAdmitVerNum = this.uTextInnerAdmitVersionNum.Text;
                        else
                            strAdmitVerNum = this.uTextRevisionNo.Text;

                        String FolderName = this.uTextStandardDocNo.Text + "-" + strAdmitVerNum;

                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                                {
                                    arrFile.Add(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + FolderName,
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        if (arrFile.Count != 0)
                        {
                            fileAtt.ShowDialog();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        public DataTable dtGetUserInfo(String strPlantCode, String strUserID)
        {
            DataTable dtUser = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtUser;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtUser;
            }
            finally
            {
            }
        }



        private void uTextRev_DisUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uTextRev_DisUserID.Text == "")
                        {
                            this.uTextRev_DisUserName.Text = "";
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            WinMessageBox msg = new WinMessageBox();


                            String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            String strWriteID = this.uTextRev_DisUserID.Text;

                            // UserName 검색 함수 호출
                            //String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);
                            DataTable dtUserInfo = dtGetUserInfo(strPlantCode, strWriteID);

                            if (dtUserInfo.Rows.Count == 0)
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextRev_DisUserID.Text = "";
                                this.uTextRev_DisUserName.Text = "";
                                this.uTextRevUserName.Text = "";
                            }
                            else
                            {
                                this.uTextRev_DisUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                                this.uTextRevUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                                this.uTextMakeUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                                this.uTextMakeDeptName.Text = dtUserInfo.Rows[0]["DeptName"].ToString();
                            }
                        }
                    }

                    if (e.KeyCode == Keys.Back)
                    {
                        if (this.uTextRev_DisUserID.Text.Length <= 1 || this.uTextRev_DisUserID.SelectedText == this.uTextRev_DisUserID.Text)
                        {
                            this.uTextRev_DisUserName.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextApproveUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uTextApproveUserID.Text == "")
                        {
                            this.uTextApproveUserName.Text = "";
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            WinMessageBox msg = new WinMessageBox();


                            String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            String strWriteID = this.uTextApproveUserID.Text;

                            // UserName 검색 함수 호출
                            DataTable dtUserInfo = dtGetUserInfo(strPlantCode, strWriteID);

                            if (dtUserInfo.Rows.Count == 0)
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextApproveUserID.Text = "";
                                this.uTextApproveUserName.Text = "";
                            }
                            else
                            {
                                this.uTextApproveUserName.Text = dtUserInfo.Rows[0]["UserName"].ToString();
                            }
                        }
                    }

                    if (e.KeyCode == Keys.Back)
                    {
                        if (this.uTextApproveUserID.Text.Length <= 1 || this.uTextApproveUserID.SelectedText == this.uTextApproveUserID.Text)
                        {
                            this.uTextApproveUserName.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGrid1_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    if (this.uGrid1.Rows[0].Cells["FileTitle"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                    {
                        System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                        openFile.Filter = "All files (*.*)|*.*";
                        openFile.FilterIndex = 1;
                        openFile.RestoreDirectory = true;

                        if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            string strImageFile = openFile.FileName;
                            e.Cell.Value = strImageFile;

                            QRPGlobal grdImg = new QRPGlobal();
                            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGrid1_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "DWFlag")
                {

                    if (Convert.ToBoolean(e.Cell.Value) == true)
                    {
                        e.Cell.Value = false;
                    }
                    else
                    {
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                            {
                                this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                                e.Cell.Value = true;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        private void uGrid2_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (this.strAdmitStatusCode == null)
                    return;
                else if (PlantCode == null)
                    return;

                if (!strAdmitStatusCode.Equals("AR"))
                {
                    frmPOP0020 frmVendor = new frmPOP0020();

                    frmVendor.PlantCode = PlantCode;
                    frmVendor.ShowDialog();

                    this.uGrid2.ActiveRow.Cells["VendorCode"].Value = frmVendor.VendorCode;
                    this.uGrid2.ActiveRow.Cells["VendorName"].Value = frmVendor.VendorName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGrid4_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (!strAdmitStatusCode.Equals("AR"))
                {
                    if (PlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = PlantCode;
                    frmUser.ShowDialog();

                    this.uGrid4.ActiveRow.Cells["AgreeUserID"].Value = frmUser.UserID;
                    this.uGrid4.ActiveRow.Cells["AgreeUserName"].Value = frmUser.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.Cell.Column.ToString() == "FileName")
                {
                    if (e.Cell.Row.Cells["FileName"].Value.ToString() == "" || e.Cell.Row.Cells["FileName"].Value.ToString().Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000356",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (e.Cell.Row.Cells["FileName"].Value.ToString().Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000357",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        String strAdmitVerNum = "";
                        if (strAdmitStatusCode.Equals("FN"))
                            strAdmitVerNum = this.uTextInnerAdmitVersionNum.Text;
                        else
                            strAdmitVerNum = this.uTextRevisionNo.Text;

                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        arrFile.Add(e.Cell.Value.ToString());
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (this.uTextStandardDocNo.Text + "-" + strAdmitVerNum),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        fileAtt.ShowDialog();
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //배포업체 Cell콤보 선택시 나머지 삽입 이벤트
        private void uGrid2_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                String strKey = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                String strValue = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);

                String strVendorCode = e.Cell.Row.Cells["VendorCode"].Value.ToString();
                int intSeq = Convert.ToInt32(strKey);

                DataTable dtVendorP = clsVendor.mfReadVendorPForGrid(strVendorCode, intSeq);

                e.Cell.Row.Cells["PersonName"].Value = dtVendorP.Rows[0]["PersonName"].ToString();
                e.Cell.Row.Cells["DeptName"].Value = dtVendorP.Rows[0]["DeptName"].ToString();
                e.Cell.Row.Cells["Hp"].Value = dtVendorP.Rows[0]["Hp"].ToString();
                e.Cell.Row.Cells["Email"].Value = dtVendorP.Rows[0]["Email"].ToString();
                e.Cell.Row.Cells["EtcDesc"].Value = dtVendorP.Rows[0]["EtcDesc"].ToString();
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 개정 / 폐기 구분에 따른 변경항목, 변경전, 변경후 필수입력 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboRev_DisSeparation_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                //개정인 경우
                if (this.uComboRev_DisSeparation.Value.ToString().Equals("1"))
                {
                    this.uTextChangeItem.Appearance.BackColor = Color.PowderBlue;
                    this.uTextChangeItem.Visible = true;
                    this.uTextFrom.Appearance.BackColor = Color.PowderBlue;
                    this.uTextFrom.Visible = true;
                    this.uTextTo.Appearance.BackColor = Color.PowderBlue;
                    this.uTextTo.Visible = true;

                    //자동채번이 가능 한 경우 개정번호 비활성화
                    if (m_bolAutoNumbering == true)
                    {
                        this.uTextAdmitVersionNum.ReadOnly = true;
                        this.uTextAdmitVersionNum.Appearance.BackColor = Color.Gainsboro;
                    }
                    //자동채번이 불가능 한 경우 개정번호 필수 활성화
                    else
                    {
                        this.uTextAdmitVersionNum.ReadOnly = false;
                        this.uTextAdmitVersionNum.Appearance.BackColor = Color.PowderBlue;
                    }
                    this.uTextAdmitVersionNum.Text = "";

                    this.uLabelChangeItem.Visible = true;
                    this.uLabelFrom.Visible = true;
                    this.uLabelTo.Visible = true;
                    this.uDateRev_DisDate.Value = DateTime.Now;

                    // 개정일 경우 이전 개정번호의 문서에 첨부한 파일인지, 개정자가 업로드할 첨부파일인지 구분
                    // 이전 개정번호의 문서일 경우는 그리드의 색이 변하고, UnderLine이 생긴다
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (!this.uGrid1.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            this.uGrid1.Rows[i].CellAppearance.BackColor = Color.BlueViolet;
                            this.uGrid1.Rows[i].CellAppearance.FontData.Underline = Infragistics.Win.DefaultableBoolean.True;
                        }
                    }
                }
                //폐기인 경우
                else if (this.uComboRev_DisSeparation.Value.ToString().Equals("2"))
                {
                    this.uLabelChangeItem.Visible = false;
                    this.uLabelFrom.Visible = false;
                    this.uLabelTo.Visible = false;
                    this.uTextChangeItem.Visible = false;
                    this.uTextFrom.Visible = false;
                    this.uTextTo.Visible = false;
                    this.uDateRev_DisDate.Value = DateTime.Now;
                    this.uTextAdmitVersionNum.ReadOnly = false;
                    this.uTextAdmitVersionNum.Appearance.BackColor = Color.Gainsboro;
                    this.uTextAdmitVersionNum.Text = strRevText;

                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (!this.uGrid1.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            this.uGrid1.Rows[i].CellAppearance.BackColor = DefaultBackColor;
                            this.uGrid1.Rows[i].CellAppearance.FontData.Underline = Infragistics.Win.DefaultableBoolean.Default;
                        }
                    }
                }
                else
                {
                    this.uDateRev_DisDate.Value = "";
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (!this.uGrid1.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            this.uGrid1.Rows[i].CellAppearance.BackColor = DefaultBackColor;
                            this.uGrid1.Rows[i].CellAppearance.FontData.Underline = Infragistics.Win.DefaultableBoolean.Default;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmISO0003R_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 개정번호 자동채번 여부 검사
        /// </summary>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        private String checkReadonly(String strPlantCode, String strLTypeCode)
        {
            String strCheckVer = "";
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH clsDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(clsDocH);

                DataTable dtCheck = clsDocH.mfReadLTypeUse(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));

                if (dtCheck.Rows.Count > 0)
                {
                    //자동채번이 안되는 경우
                    strCheckVer = "Y";
                }
                else
                {
                    //자동채번이 되는 경우
                    strCheckVer = "N";
                }
                return strCheckVer;
            }
            catch (Exception ex)
            {
                return strCheckVer;
            }
            finally
            { }
        }

        private void uComboRev_DisSeparation_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboRev_DisSeparation.Value.Equals("1"))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                    QRPISO.BL.ISODOC.ISODocH clsDocH = new QRPISO.BL.ISODOC.ISODocH();
                    brwChannel.mfCredentials(clsDocH);

                    String strPlantCode = this.uComboSearchPlant.Value.ToString();
                    String strStdNumber = this.uTextSearchStandardNo.Text;

                    DataTable dtAds = clsDocH.mfReadISODocHAdmitstatus(strPlantCode, strStdNumber);

                    if (dtAds.Rows.Count > 0)
                    {
                        //DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //, "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                        WinMessageBox msg = new WinMessageBox();

                        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "중복확인", "개정가능 여부확인",
                            "선택하신 문서는 이미 개정중입니다", Infragistics.Win.HAlign.Right);

                        this.uComboRev_DisSeparation.Value = string.Empty;

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
