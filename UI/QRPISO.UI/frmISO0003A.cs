﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서 관리                                         */
/* 프로그램ID   : frmISO0003A.cs                                        */
/* 프로그램명   : 표준문서 승인                                         */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPISO.UI
{
    public partial class frmISO0003A : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        //개정전 개정번호를 저장하기 위한 전역변수
        String m_staticVersionNum = "";
        //개정 / 폐기구분을 저장하기 위한 전역변수
        String m_strRevDisuseType = "";

        private string PlantCode
        {
            get;
            set;
        }

        public frmISO0003A()
        {
            InitializeComponent();
        }

        private void frmISO0003A_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISO0003A_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 승인", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
            InitButton();

            // 그룹박스 접은 상태로

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        #region 초기화 Method

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "배포업체", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "적용범위", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.LIST, "결재선", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchAdmitStatus, "승인상태", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStandardDocNo, "표준문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "기안자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateDate, "기안일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisSeparation, "개정/폐기구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisUser, "개정/폐기자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisDate, "개정/폐기일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisReason, "개정/폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChangeItem, "변경항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFrom, "변경 전", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTo, "변경 후", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApproveUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelApproveDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelDocState, "문서상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDocKeyword, "문서 Keyword", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApplyDate, "적용일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelReturnReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMake, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGRWComment, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChipSize, "ChipSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPadSize, "PadSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // SerachArea
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // SearchAdmitStatus
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtAdmitStatus = clsComCode.mfReadCommonCode("C0009", m_resSys.GetString("SYS_LANG"));

                //승인완료(FN)를 제외하고 받아온다
                DataTable dtAdmit = new DataTable();
                dtAdmit.Columns.Add("ComCode", typeof(string));
                dtAdmit.Columns.Add("ComCodeName", typeof(string));
                DataRow dr;

                if (dtAdmitStatus.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAdmitStatus.Rows.Count; i++)
                    {
                        if (!dtAdmitStatus.Rows[i]["ComCode"].ToString().Equals("FN"))
                        {
                            dr = dtAdmit.NewRow();

                            dr["ComCode"] = dtAdmitStatus.Rows[i]["ComCode"].ToString();
                            dr["ComCodeName"] = dtAdmitStatus.Rows[i]["ComCodeName"].ToString();

                            dtAdmit.Rows.Add(dr);
                        }
                    }
                }

                wCombo.mfSetComboEditor(this.uComboSearchAdmitStatus, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ComCode", "ComCodeName", dtAdmit);

                // Process ComboBox
                // Bl 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo("", m_resSys.GetString("SYS_LANG"));

                // SearchArea
                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid, 0, "LTypeName", "대분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "MTypeName", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "AdmitVersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteID", "기안자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteName", "기안자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteDate", "기안일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "AdmitStatus", "문서상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 2000
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseID", "개정자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseName", "개정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseDate", "개정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseReason", "개정사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 2000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //추가
                wGrid.mfSetGridColumn(this.uGrid, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 첨부파일 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 40, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DWFlag", "DW여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 배포업체 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorName", "거래처", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "PersonName", "담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Hp", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "###-####-####", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Email", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 적용범위 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                //wGrid.mfSetGridColumn(this.uGrid3, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGrid3, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "ProcessGroup", "공정그룹", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcessGroup);

                DataTable dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid3, 0, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcessGroup);




                // 합의담당자 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid4, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                //wGrid.mfSetGridColumn(this.uGrid4, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGrid4, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserID", "담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserName", "담당자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////// 빈줄추가
                ////wGrid.mfAddRowGrid(this.uGrid1, 0);
                ////wGrid.mfAddRowGrid(this.uGrid2, 0);

                // Set Font Size
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid4.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid4.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                //// 그리드 내의 콤보박스 처리 ////

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextStandardDocNo.Text = "";
                this.uTextStandardDocNo.MaxLength = 100;
                this.uTextRevisionNo.Text = "";
                this.uTextRevisionNo.MaxLength = 50;
                this.uTextCreateUserID.Text = "";
                this.uTextCreateUserID.MaxLength = 20;
                this.uTextCreateUserName.Text = "";
                this.uTextCreateUserID.MaxLength = 20;

                this.uTextRev_DisUserID.Text = "";
                this.uTextRev_DisUserID.MaxLength = 20;
                this.uTextRev_DisUserName.Text = "";
                this.uTextRev_DisUserName.MaxLength = 20;
                this.uTextRev_DisReason.Text = "";
                this.uTextRev_DisReason.MaxLength = 2000;
                this.uTextChangeItem.Text = "";
                this.uTextChangeItem.MaxLength = 500;
                this.uTextFrom.Text = "";
                this.uTextFrom.MaxLength = 1000;
                this.uTextTo.Text = "";
                this.uTextTo.MaxLength = 1000;
                this.uTextApproveUserID.Text = "";
                this.uTextApproveUserID.MaxLength = 20;
                this.uTextApproveUserName.Text = "";
                this.uTextApproveUserName.MaxLength = 20;
                this.uDateApproveDate.Value = DateTime.Now;

                this.uTextDocState.Text = "";
                this.uTextTitle.Text = "";
                this.uTextTitle.MaxLength = 2000;
                this.uTextDocKeyword.Text = "";
                this.uTextEtc.Text = "";
                this.uTextTitle.MaxLength = 100;
                this.uTextGRWComment.Text = "";
                this.uTextGRWComment.MaxLength = 1000;
                this.uTextDeptName.ReadOnly = true;
                this.uTextDeptName.Text = "";

                //////추가//////
                this.uTextPlant.Text = "";
                PlantCode = string.Empty;
                this.uTextProcess.Text = "";
                this.uTextSmall.Text = "";
                this.uTextMiddle.Text = "";
                this.uTextLarge.Text = "";
                this.uTextRev_DisUserID.Text = "";
                this.uTextCompleteDate.Text = "";
                this.uTextCreateDate.Text = "";
                this.uTextApplyFromDate.Text = "";
                this.uTextApplyToDate.Text = "";
                this.uTextPackage.Text = "";
                this.uTextPackage.MaxLength = 40;
                this.uTextChipSize.Text = "";
                this.uTextChipSize.MaxLength = 100;
                this.uTextPadSize.Text = "";
                this.uTextPadSize.MaxLength = 100;
                m_strRevDisuseType = "";


                //스크롤바
                this.uTextFrom.Scrollbars = ScrollBars.Vertical;
                this.uTextFrom.SelectionStart = uTextFrom.Text.Length;
                this.uTextFrom.ScrollToCaret();

                this.uTextTo.Scrollbars = ScrollBars.Vertical;
                this.uTextTo.SelectionStart = uTextTo.Text.Length;
                this.uTextTo.ScrollToCaret();

                if (this.uGrid1.Rows.Count > 0)
                {
                    while (this.uGrid1.Rows.Count > 0)
                    {
                        this.uGrid1.Rows[0].Delete(false);
                    }
                }
                if (this.uGrid2.Rows.Count > 0)
                {
                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                }
                if (this.uGrid3.Rows.Count > 0)
                {
                    while (this.uGrid3.Rows.Count > 0)
                    {
                        this.uGrid3.Rows[0].Delete(false);
                    }
                }
                if (this.uGrid4.Rows.Count > 0)
                {
                    while (this.uGrid4.Rows.Count > 0)
                    {
                        this.uGrid4.Rows[0].Delete(false);
                    }
                }

                //공정 히든
                this.uComboSearchProcess.Visible = false;
                this.uTextProcess.Visible = false;
                this.uTextCompleteDate.Visible = false;

                //승인자 승인일 수정 불가(용도변경)
                this.uTextApproveUserID.ReadOnly = true;
                this.uTextApproveUserID.Appearance.BackColor = Color.Gainsboro;
                this.uDateApproveDate.ReadOnly = true;
                this.uDateApproveDate.Appearance.BackColor = Color.Gainsboro;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar 메소드

        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(isoDocH);

                InitValue();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;
                String strStdNumber = this.uTextSearchStandardNo.Text;
                String strAdmitStatus = this.uComboSearchAdmitStatus.Value.ToString();

                //호출
                DataTable dt = isoDocH.mfReadISODocH(strPlantCode, strDocTitle, strStdNumber, strAdmitStatus, m_resSys.GetString("SYS_LANG"));

                this.uGrid.DataSource = dt;
                this.uGrid.DataBind();


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid, 0);
                }

                if (uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {

            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                System.Windows.Forms.DialogResult result;
                DataTable dtISODocH = new DataTable();
                DataTable dtSaveVendor = new DataTable();
                DataTable dtSaveDept = new DataTable();
                DataTable dtSaveAgreeUser = new DataTable();
                DataTable dtSaveFile = new DataTable();
                DataRow row;

                // 결재선 팝업창
                //QRPCOM.UI.frmCOM0012 frm = new QRPCOM.UI.frmCOM0012();
                //DialogResult _dr = new DialogResult();

                if (this.uTextApproveUserID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
              , "M001264", "M001230", "M000769", Infragistics.Win.HAlign.Center);

                    //Focus
                    this.uTextApproveUserID.Focus();
                    return;
                }
                else
                {
                    QRPBrowser brwChannel = new QRPBrowser();

                    //FileServer 연결
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                    //BL 호출
                    DialogResult dialogresult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000771", "M000934"
                                        , Infragistics.Win.HAlign.Right);

                    //결재선 팝업창 호출
                    //_dr = frm.ShowDialog();
                    //if (_dr == DialogResult.No || _dr == DialogResult.Cancel)
                    //    return;

                    //저장
                    if (dialogresult == DialogResult.Yes)
                    {

                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                        QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                        brwChannel.mfCredentials(isoDocH);

                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                        QRPISO.BL.ISODOC.ISODocFile isoFile = new QRPISO.BL.ISODOC.ISODocFile();
                        brwChannel.mfCredentials(isoFile);

                        dtISODocH = isoDocH.mfSetDateInfo();
                        dtISODocH.Columns.Add("RevDisuseTypeCode", typeof(string));
                        dtSaveFile = isoFile.mfSetDatainfo();

                        row = dtISODocH.NewRow();
                        row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                        row["StdNumber"] = this.uTextStandardDocNo.Text;
                        row["VersionNum"] = this.uTextRevisionNo.Text;
                        row["AdmitVersionNum"] = this.uTextAdmitVersionNum.Text;

                        row["ProcessCode"] = this.uGrid.ActiveRow.Cells["ProcessCode"].Value.ToString();
                        row["LTypeCode"] = this.uGrid.ActiveRow.Cells["LTypeCode"].Value.ToString();
                        row["MTypeCode"] = this.uGrid.ActiveRow.Cells["MTypeCode"].Value.ToString();
                        row["STypeCode"] = this.uGrid.ActiveRow.Cells["STypeCode"].Value.ToString();
                        row["MakeUserName"] = this.uTextMakeUserName.Text;
                        row["MakeDeptName"] = this.uTextMakeDeptName.Text;
                        row["WriteID"] = this.uTextCreateUserID.Text;
                        row["WriteDate"] = this.uTextCreateDate.Text;
                        row["AdmitID"] = this.uTextApproveUserID.Text;
                        row["AdmitDate"] = this.uDateApproveDate.Value.ToString();
                        row["RevDisuseType"] = this.uTextRev_DisSeparation.Text;

                        row["RevDisuseID"] = this.uTextRev_DisUserID.Text;

                        if (m_strRevDisuseType.Equals("1"))
                        {
                            row["RevDisuseDate"] = this.uTextRev_DisDate.Value.ToString();
                            row["RevDisuseReason"] = this.uTextRev_DisReason.Text;
                            row["RevDisuseTypeCode"] = "1";
                        }
                        else if (m_strRevDisuseType.Equals("2"))
                        {
                            row["DisuseDate"] = this.uTextRev_DisDate.Value.ToString();
                            row["DisuseReason"] = this.uTextRev_DisReason.Text;
                            row["RevDisuseTypeCode"] = "2";
                        }
                        else
                        {
                            row["RevDisuseTypeCode"] = "";
                        }

                        row["ChangeItem"] = this.uTextChangeItem.Text;
                        row["FromDesc"] = this.uTextFrom.Text;
                        row["ToDesc"] = this.uTextTo.Text;
                        row["DocTitle"] = this.uTextTitle.Text;
                        row["DocKeyword"] = this.uTextDocKeyword.Text;
                        row["DocDesc"] = this.uTextEtc.Text;
                        row["ApplyDateFrom"] = this.uTextApplyFromDate.Text;
                        row["ApplyDateTo"] = this.uTextApplyToDate.Text;
                        row["CompleteDate"] = this.uTextCompleteDate.Text;
                        row["ReturnReason"] = this.uTextReturnReason.Text;
                        row["Package"] = this.uTextPackage.Text;
                        row["ChipSize"] = this.uTextChipSize.Text;
                        row["PadSize"] = this.uTextPadSize.Text;

                        if (dialogresult == DialogResult.Yes)
                        {
                            row["AdmitStatus"] = "FN";
                        }
                        else
                        {
                            return;
                        }

                        dtISODocH.Rows.Add(row);


                        //새로 복사할 파일의 이름(표준번호-개정번호-Seq -FileName)
                        String[] strFile = new String[this.uGrid1.Rows.Count];
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo fileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                strFile[i] = fileDoc.Name;

                            }
                            else
                            {
                                String fName = this.uGrid1.Rows[i].Cells["FileName"].Value.ToString();
                                String[] splitName = fName.Split(new char[] { '-' });
                                if (splitName.Length > 5)
                                {
                                    for (int j = 4; j < splitName.Length; j++)
                                    {
                                        if (!j.Equals(splitName.Length - 1))
                                            strFile[i] = strFile[i] + splitName[j] + "-";
                                        else
                                            strFile[i] = strFile[i] + splitName[j];
                                    }
                                }
                                else
                                {
                                    strFile[i] = splitName[4];
                                }
                            }
                        }
                        int DWCheck = 0;
                        //첨부파일 그리드 저장
                        if (dialogresult == DialogResult.Yes)
                        {
                            for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                            {
                                row = dtSaveFile.NewRow();

                                row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                                row["StdNumber"] = this.uTextStandardDocNo.Text;
                                row["VersionNum"] = this.uTextRevisionNo.Text;
                                row["Seq"] = this.uGrid1.Rows[i].Cells["Seq"].Value;
                                row["FileTitle"] = this.uGrid1.Rows[i].Cells["FileTitle"].Value.ToString();
                                row["FileName"] = strFile[i];
                                if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                                {
                                    row["DWFlag"] = "T";
                                    DWCheck++;
                                }
                                else
                                {
                                    row["DWFlag"] = "F";
                                }
                                row["EtcDesc"] = this.uGrid1.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtSaveFile.Rows.Add(row);
                            }
                        }
                        //개정한 문서의 경우 이전 버전의 문서는 폐기처리한다.
                        if (this.uTextRev_DisUserID.Text != "" && dialogresult == DialogResult.Yes && m_strRevDisuseType == "1")
                        {
                            DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001189", "M001037", "M000856",
                                                       Infragistics.Win.HAlign.Right);
                            if (DResult == DialogResult.Yes)
                            {
                                DataTable dtDisuse = isoDocH.mfSetDateInfo();

                                row = dtDisuse.NewRow();
                                row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                                row["StdNumber"] = this.uTextStandardDocNo.Text;
                                row["VersionNum"] = m_staticVersionNum;
                                row["RevDisuseID"] = this.uTextRev_DisUserID.Text;
                                row["DisuseDate"] = this.uDateApproveDate.Value.ToString();

                                dtDisuse.Rows.Add(row);

                                string strDisuse = isoDocH.mfSaveISODocHAutoDisus(dtDisuse, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                                TransErrRtn DisuseErrRtn = new TransErrRtn();
                                DisuseErrRtn = DisuseErrRtn.mfDecodingErrMessage(strDisuse);

                                if (DisuseErrRtn.ErrNum != 0)
                                {
                                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000935",
                                                        Infragistics.Win.HAlign.Right);
                                    return;
                                }
                                else
                                {
                                    string strRevDisuseDate = this.uDateApproveDate.Value.ToString();
                                    DataTable dtISOSendDoc = isoDocH.mfReadISODocH_SendLine(PlantCode, this.uTextStandardDocNo.Text, strRevDisuseDate);
                                    if (dtISODocH.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtISOSendDoc.Rows.Count; i++)
                                        {
                                            string strPlantCode = dtISOSendDoc.Rows[i]["PlantCode"].ToString();
                                            string strVersionNum = dtISOSendDoc.Rows[i]["StdNumber"].ToString();
                                            DataTable dtSendLine = isoDocH.mfReadISODocHTextBox(strPlantCode, this.uTextStandardDocNo.Text
                                                                                        , dtISOSendDoc.Rows[i]["VersionNum"].ToString(), m_resSys.GetString("SYS_LANG"));

                                            SendMail(strPlantCode, this.uTextStandardDocNo.Text, dtISOSendDoc.Rows[i]["VersionNum"].ToString(), dtSendLine);
                                        }
                                    }
                                }
                            }
                        }

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //DW폴더내 최신버전의 첨부파일 검색
                        String strVersion = this.uTextRevisionNo.Text;
                        String StdNumber = this.uTextStandardDocNo.Text;
                        DataTable dtLastFile = isoFile.mfReadISODocFileForDelFile(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), StdNumber, strVersion);
                        String strLastVersionFile = "";
                        if (dtLastFile.Rows.Count > 0)
                        {
                            strLastVersionFile = dtLastFile.Rows[0]["FileName"].ToString();
                        }
                        //처리로직
                        //저장 함수 호출
                        string rtMSG = isoDocH.mfSaveISODocHApprove(dtISODocH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveFile);

                        //decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        String strAdmitVersionNum = this.uTextAdmitVersionNum.Text;
                        //처리로직끝
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                        //메시지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            //File UpLoad
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrAdmitFile = new ArrayList();                                              //승인문서의 첨부파일 복사할 경로(+파일명)
                            ArrayList arrReqFile = new ArrayList();                                                 //승인요청문서의 첨부파일 경로(+파일명)
                            ArrayList arrDelDWFile = new ArrayList();                                             //삭제할 기존 DW파일 경로(+파일명)
                            ArrayList arrCopyDWFile = new ArrayList();                                          //새로 복사할 승인문서의 DW파일 경로(+파일명)
                            ArrayList arrDWTargetFile = new ArrayList();

                            if (dialogresult == DialogResult.Yes)
                            {
                                //승인요청시 VersionNum과 AdminVersionNum이 다를경우 정상적으로 저장
                                if (strVersion != strAdmitVersionNum || dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals(String.Empty))
                                {
                                    //승인요청문서의 파일을 확정된 개정번호로 복사(첨부파일이 있는 경우)
                                    if (this.uGrid1.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < 1; i++) //this.uGrid1.Rows.Count; i++)
                                        {
                                            FileInfo ReqfileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                            //String strAdmitFileName = "ISODocFile/" + StdNumber + "-" + strAdmitVersionNum + "/"
                                            //                            + StdNumber + "-" + strAdmitVersionNum + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-" + dtISODocH.Rows[0]["PlantCode"].ToString() + "-"
                                            //                            + strFile[i];
                                            //String strTargetFile = "ISODocFile/" + StdNumber + "-" + strVersion + "/" + ReqfileDoc;

                                            string strAdmitFileName = @dtFilePath.Rows[0]["ServerPath"].ToString() + "ISODocFile\\" + StdNumber + "-" + strAdmitVersionNum;
                                            string strTargetFile = @dtFilePath.Rows[0]["ServerPath"].ToString() + "ISODocFile\\" + StdNumber + "-" + strVersion;
                                            arrAdmitFile.Add(strAdmitFileName); //복사될 파일 경로
                                            arrReqFile.Add(strTargetFile);          //복사할 파일 경로
                                        }

                                        fileAtt.mfInitSetSystemFileMoveInfo(arrAdmitFile
                                                                                            , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                                            , arrReqFile
                                                                                            , dtFilePath.Rows[0]["ServerPath"].ToString()
                                                                                            , "ISODocFile\\" + StdNumber + "-" + strAdmitVersionNum //+ "\\"
                                                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                        if (arrAdmitFile.Count > 0)
                                        {
                                            fileAtt.mfFileUploadNoProgView();
                                            //fileAtt.ShowDialog();
                                        }
                                        ////기존 DW파일 삭제
                                        //brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                                        //QRPISO.BL.ISODOC.ISODocFile iFile = new QRPISO.BL.ISODOC.ISODocFile();
                                        //brwChannel.mfCredentials(iFile);

                                        String strPlantCode = dtISODocH.Rows[0]["PlantCode"].ToString();

                                        //DataTable dtFileName = iFile.mfReadISODocFileForDelFile(strPlantCode, StdNumber, strVersion);
                                        //if (dtFileName.Rows.Count > 0)
                                        //{                                        
                                        //    //for (int i = 0; i < dtFileName.Rows.Count; i++)
                                        //    //{
                                        //    //    String strDelDW = "ISODocFile\\DW\\" + dtFileName.Rows[i]["FileName"].ToString();
                                        //    //    arrDelDWFile.Add(strDelDW);
                                        //    //}
                                        //    String strDelDW = "";
                                        //    if (dtFileName.Rows.Count == 1)
                                        //    {
                                        //        strDelDW = "ISODocFile\\DW\\" + dtFileName.Rows[0]["FileName"].ToString();
                                        //    }
                                        //    else if (dtFileName.Rows.Count > 1)
                                        //    {
                                        //        strDelDW = "ISODocFile\\DW\\" + dtFileName.Rows[1]["FileName"].ToString();
                                        //    }
                                        //    arrDelDWFile.Add(strDelDW);

                                        //    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                        //                                                        , arrDelDWFile
                                        //                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                        //                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                                        //    fileAtt.mfFileUploadNoProgView();
                                        //}
                                        if (DWCheck > 0 && dtLastFile.Rows.Count > 0)
                                        {
                                            arrDelDWFile.Add("ISODocFile\\DW\\" + strLastVersionFile);

                                            fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                                                , arrDelDWFile
                                                                                                , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                                                , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                                            fileAtt.mfFileUploadNoProgView();
                                        }
                                        ////새로운 DW파일 복사
                                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                                        {
                                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                                            {
                                                //FileInfo fileDoc = new FileInfo(StdNumber + "-" + strAdmitVersionNum + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + strFile[i]);

                                                String DestFile = "ISODocFile/DW/" + StdNumber + "-" + strAdmitVersionNum + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-" + strPlantCode + "-" + strFile[i];
                                                String targetFile = "ISODocFile/" + StdNumber + "-" + strAdmitVersionNum + "/"
                                                                        + StdNumber + "-" + strAdmitVersionNum + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-" + strPlantCode + "-" + strFile[i];

                                                arrCopyDWFile.Add(DestFile); // DW폴더로 복사할 파일 경로 + 이름
                                                arrDWTargetFile.Add(targetFile); //DW파일의 원본 파일 경로 + 이름
                                            }
                                        }
                                        fileAtt.mfInitSetSystemFileCopyInfo(arrCopyDWFile
                                                                                            , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                                            , arrDWTargetFile
                                                                                            , dtFilePath.Rows[0]["ServerPath"].ToString()
                                                                                            , "ISODocFile\\DW\\"
                                                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                        if (arrCopyDWFile.Count > 0)
                                        {
                                            fileAtt.mfFileUploadNoProgView();
                                        }
                                    }
                                    //Web에서 Move처리하면서 화일명을 바꾸기때문에 아래 줄 필요없음
                                    ////////if (arrCopyDWFile.Count > 0)
                                    ////////{
                                    ////////    fileAtt.mfFileUploadNoProgView();
                                    ////////}
                                    //////////승인요청문서의 첨부파일 삭제
                                    ////////fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                    ////////                                                        , arrReqFile
                                    ////////                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                    ////////                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                    ////////fileAtt.mfFileUploadNoProgView();
                                }
                                else
                                {
                                    ////VersionNum과 AdmitVerionNum이 같을경우 복사, 삭제시 원본 복사본의 구분이 없어지고 삭제되므로 복사 / 삭제 수행 안하도록 처리
                                    //frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                                    //ArrayList arrDelDWFile = new ArrayList();                                             //삭제할 기존 DW파일 경로(+파일명)
                                    //ArrayList arrCopyDWFile = new ArrayList();                                          //새로 복사할 승인문서의 DW파일 경로(+파일명)
                                    //ArrayList arrDWTargetFile = new ArrayList();                                        //복사할 파일 경로(+파일명)
                                    ArrayList arrDelFile = new ArrayList();                                                   //삭제할 파일 경로(+파일명)
                                    ////기존 DW파일 삭제, 신규 DW파일 업로드
                                    ////기존 DW파일 삭제
                                    //기존 DW파일 삭제
                                    if (DWCheck > 0 && dtLastFile.Rows.Count > 0)
                                    {
                                        arrDelDWFile.Add("ISODocFile\\DW\\" + strLastVersionFile);
                                        fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                                            , arrDelDWFile
                                                                                            , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                                            , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                                        fileAtt.mfFileUploadNoProgView();
                                    }
                                    //brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                                    //QRPISO.BL.ISODOC.ISODocFile iFile = new QRPISO.BL.ISODOC.ISODocFile();
                                    //brwChannel.mfCredentials(iFile);

                                    String strPlantCode = dtISODocH.Rows[0]["PlantCode"].ToString();

                                    //DataTable dtFileName = iFile.mfReadISODocFileForDelFile(strPlantCode, StdNumber, strVersion);
                                    //if (dtFileName.Rows.Count > 0)
                                    //{
                                    //    //for (int i = 0; i < dtFileName.Rows.Count; i++)
                                    //    //{
                                    //    //    String strDelDW = "ISODocFile\\DW\\" + dtFileName.Rows[i]["FileName"].ToString();
                                    //    //    arrDelDWFile.Add(strDelDW);
                                    //    //}
                                    //    String strDelDW = "";
                                    //    if (dtFileName.Rows.Count == 1)
                                    //    {
                                    //        strDelDW = "ISODocFile\\DW\\" + dtFileName.Rows[0]["FileName"].ToString();
                                    //    }
                                    //    else if (dtFileName.Rows.Count > 1)
                                    //    {
                                    //        strDelDW = "ISODocFile\\DW\\" + dtFileName.Rows[1]["FileName"].ToString();
                                    //    }
                                    //    arrDelDWFile.Add(strDelDW);

                                    //    fileAtt.mfInitSetSystemFileDeleteInfo(dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                    //                                                        , arrDelDWFile
                                    //                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                    //                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());

                                    //    fileAtt.mfFileUploadNoProgView();
                                    //}
                                    ////새로운 DW파일 복사
                                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                                    {
                                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                                        {
                                            //FileInfo fileDoc = new FileInfo(StdNumber + "-" + strAdmitVersionNum + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + strFile[i]);

                                            String DestFile = "ISODocFile/DW/" + StdNumber + "-" + strAdmitVersionNum + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-" + strPlantCode + "-" + strFile[i];
                                            String targetFile = "ISODocFile/" + StdNumber + "-" + strAdmitVersionNum + "/"
                                                                    + StdNumber + "-" + strAdmitVersionNum + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-" + strPlantCode + "-" + strFile[i];

                                            arrCopyDWFile.Add(DestFile); // DW폴더로 복사할 파일 경로 + 이름
                                            arrDWTargetFile.Add(targetFile); //DW파일의 원본 파일 경로 + 이름
                                        }
                                    }
                                    fileAtt.mfInitSetSystemFileCopyInfo(arrCopyDWFile
                                                                                        , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                                        , arrDWTargetFile
                                                                                        , dtFilePath.Rows[0]["ServerPath"].ToString()
                                                                                        , "ISODocFile\\DW\\"
                                                                                        , dtSysAccess.Rows[0]["AccessID"].ToString()
                                                                                        , dtSysAccess.Rows[0]["AccessPassword"].ToString());
                                    if (arrCopyDWFile.Count > 0)
                                    {
                                        fileAtt.mfFileUploadNoProgView();
                                    }
                                }
                            }
                            if (dialogresult == DialogResult.Yes)
                            {
                                //테스트 코드
                                //frm.dtSendLine.Rows[0]["CD_USERKEY"] = "tica100";
                                //frm.dtSendLine.Rows[0]["UserName"] = "이종민";

                                //파일까지 처리 완료 후 Brains에 전송
                                //DataTable dtRtn = isoDocH.mfISODOCGRWApproval(dtISODocH, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                                //if(!dtRtn.Rows[0]["CD_CODE"].Equals("00"))
                                //{
                                //실패
                                //}
                                SendMail(PlantCode, StdNumber, strVersion, dtISODocH);

                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000927",
                                                       Infragistics.Win.HAlign.Right);
                            }

                            else if (dialogresult == DialogResult.No)
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000921",
                                                       Infragistics.Win.HAlign.Right);
                            }
                            InitValue();
                            this.uGroupBoxContentsArea.Expanded = false;

                            // 리스트 갱신
                            mfSearch();

                        }



                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000935",
                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화

                else
                {
                    InitValue();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        // ContentsGroupBox 상태변화 이벤트

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 720;

                    for (int i = 0; i < uGrid.Rows.Count; i++)
                    {
                        uGrid.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProcess.Items.Clear();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uGrid_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                e.Row.Fixed = true;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPopup = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //함수호출
                QRPBrowser brwChannel = new QRPBrowser();

                //Header
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH header = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(header);
                //AgreeUser
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                QRPISO.BL.ISODOC.ISODocAgreeUser agreeUser = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                brwChannel.mfCredentials(agreeUser);
                //Dept
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                QRPISO.BL.ISODOC.ISODocDept dept = new QRPISO.BL.ISODOC.ISODocDept();
                brwChannel.mfCredentials(dept);
                //Vendor
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                QRPISO.BL.ISODOC.ISODocVendor vendor = new QRPISO.BL.ISODOC.ISODocVendor();
                brwChannel.mfCredentials(vendor);
                //File
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                QRPISO.BL.ISODOC.ISODocFile file = new QRPISO.BL.ISODOC.ISODocFile();
                brwChannel.mfCredentials(file);

                //헤더용
                String strVersionNum = this.uGrid.ActiveCell.Row.Cells["VersionNum"].Value.ToString();
                //공통인수
                String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                String strStdNumber = this.uGrid.ActiveRow.Cells["StdNumber"].Value.ToString();
                PlantCode = strPlantCode;

                //Header
                DataTable dtHeader = header.mfReadISODocHTextBox(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uTextStandardDocNo.Text = dtHeader.Rows[0]["StdNumber"].ToString();
                this.uTextPlant.Text = dtHeader.Rows[0]["PlantName"].ToString();
                this.uTextRevisionNo.Text = dtHeader.Rows[0]["VersionNum"].ToString();
                //if (dtHeader.Rows[0]["LTypeCode"].ToString().Equals("Cust_Spec") || dtHeader.Rows[0]["LTypeCode"].ToString().Equals("CP"))
                //{
                //    this.uTextAdmitVersionNum.Text = dtHeader.Rows[0]["VersionNum"].ToString();
                //}
                //else
                //{
                //    this.uTextAdmitVersionNum.Text = "";
                //}
                this.uTextAdmitVersionNum.Text = dtHeader.Rows[0]["AdmitVersionNum"].ToString();
                this.uTextLarge.Text = dtHeader.Rows[0]["LTypeName"].ToString();
                this.uTextMiddle.Text = dtHeader.Rows[0]["MTypeName"].ToString();
                this.uTextSmall.Text = dtHeader.Rows[0]["STypeName"].ToString();
                this.uTextMakeUserName.Text = dtHeader.Rows[0]["MakeUserName"].ToString();
                this.uTextMakeDeptName.Text = dtHeader.Rows[0]["MakeDeptName"].ToString();
                this.uTextCreateUserID.Text = dtHeader.Rows[0]["WriteID"].ToString();
                this.uTextCreateUserName.Text = dtHeader.Rows[0]["WriteName"].ToString();
                this.uTextCreateDate.Text = dtHeader.Rows[0]["WriteDate"].ToString();
                this.uTextRev_DisSeparation.Text = dtHeader.Rows[0]["RevDisuseType"].ToString();
                this.uTextRev_DisUserID.Text = dtHeader.Rows[0]["RevDisuseID"].ToString();
                this.uTextRev_DisUserName.Text = dtHeader.Rows[0]["RevDisuseName"].ToString();
                this.uTextChangeItem.Text = dtHeader.Rows[0]["ChangeItem"].ToString();
                this.uTextFrom.Text = dtHeader.Rows[0]["FromDesc"].ToString();
                this.uTextTo.Text = dtHeader.Rows[0]["ToDesc"].ToString();
                this.uTextApproveUserID.Text = dtHeader.Rows[0]["AdmitID"].ToString();
                this.uTextApproveUserName.Text = dtHeader.Rows[0]["AdmitName"].ToString();
                this.uDateApproveDate.Value = dtHeader.Rows[0]["AdmitDate"].ToString();
                this.uTextDocState.Text = dtHeader.Rows[0]["AdmitStatus"].ToString();
                this.uTextTitle.Text = dtHeader.Rows[0]["DocTitle"].ToString();
                this.uTextDocKeyword.Text = dtHeader.Rows[0]["DocKeyword"].ToString();
                this.uTextApplyFromDate.Text = dtHeader.Rows[0]["ApplyDateFrom"].ToString();
                this.uTextApplyToDate.Text = dtHeader.Rows[0]["ApplyDateTo"].ToString();
                this.uTextCompleteDate.Text = dtHeader.Rows[0]["CompleteDate"].ToString();
                this.uTextEtc.Text = dtHeader.Rows[0]["DocDesc"].ToString();
                this.uTextGRWComment.Text = dtHeader.Rows[0]["GRWComment"].ToString();
                this.uTextPackage.Text = dtHeader.Rows[0]["Package"].ToString();
                this.uTextChipSize.Text = dtHeader.Rows[0]["ChipSize"].ToString();
                this.uTextPadSize.Text = dtHeader.Rows[0]["PadSize"].ToString();

                m_staticVersionNum = dtHeader.Rows[0]["VersionNum"].ToString();
                m_strRevDisuseType = dtHeader.Rows[0]["RevDisuseTypeCode"].ToString();


                if (m_strRevDisuseType == "1")
                {
                    this.uTextRev_DisReason.Text = dtHeader.Rows[0]["RevDisuseReason"].ToString();
                    this.uTextRev_DisDate.Value = dtHeader.Rows[0]["RevDisuseDate"].ToString();
                }
                else
                {
                    this.uTextRev_DisReason.Text = dtHeader.Rows[0]["DisuseReason"].ToString();
                    this.uTextRev_DisDate.Value = dtHeader.Rows[0]["DisuseDate"].ToString();
                }


                if (dtHeader.Rows[0]["LTYpeCode"].ToString().Equals("ECN"))
                {
                    this.uTextApplyToDate.Visible = true;
                    this.ultraLabel3.Visible = true;
                }
                else
                {
                    this.uTextApplyToDate.Visible = false;
                    this.ultraLabel3.Visible = false;
                }
                WinGrid grd = new WinGrid();

                //AgreeUser Start//
                DataTable dtAgree = agreeUser.mfReadISODocAgreeUser(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid4.DataSource = dtAgree;
                this.uGrid4.DataBind();
                if (dtAgree.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid4, 0);
                //AGreeUser End//

                //DeptStart//
                DataTable dtDept = dept.mfReadISODocDept(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid3.DataSource = dtDept;
                this.uGrid3.DataBind();
                if (dtDept.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid3, 0);
                //Dept End//

                //Vendor Start//
                DataTable dtVendor = vendor.mfReadISODocVendor(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid2.DataSource = dtVendor;
                this.uGrid2.DataBind();
                if (dtVendor.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                //Vendor End//

                //File Start//
                DataTable dtFile = file.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid1.DataSource = dtFile;
                this.uGrid1.DataBind();
                if (dtFile.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);

                for (int i = 0; i < dtFile.Rows.Count; i++)
                {
                    if (dtFile.Rows[i]["DWFlag"].ToString() == "T")
                    {
                        this.uGrid1.Rows[i].Cells["DWFlag"].Value = true;
                    }
                    else
                    {
                        this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                    }
                }

                if (!this.uTextRev_DisUserID.Text.Equals(string.Empty))
                {
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    string strUserID = this.uTextRev_DisUserID.Text;

                    DataTable dtUserInfo = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), strUserID, m_resSys.GetString("SYS_LANG"));

                    if (dtUserInfo.Rows.Count > 0)
                    {
                        this.uTextDeptName.Text = dtUserInfo.Rows[0]["DeptName"].ToString();
                    }
                }

                //File End//


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (this.uTextStandardDocNo.Text == "" && this.uGrid1.Rows.Count == 0 && this.uGrid2.Rows.Count == 0 && this.uGrid3.Rows.Count == 0 && this.uGrid4.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                       , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }


                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextApproveUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M001203",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                frmPOP0011 UserPop = new frmPOP0011();
                UserPop.PlantCode = PlantCode;
                UserPop.ShowDialog();

                this.uTextApproveUserID.Text = UserPop.UserID;
                this.uTextApproveUserName.Text = UserPop.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uButtonDown_Click(object sender, EventArgs e)
        {

            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid1.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\")
                            || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001147",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();



                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                                {
                                    arrFile.Add(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (this.uTextStandardDocNo.Text + "-" + this.uTextRevisionNo.Text),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());


                        if (arrFile.Count != 0)
                        {
                            fileAtt.ShowDialog();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }
        private void uTextApproveUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextApproveUserID.Text == "")
                    {
                        this.uTextApproveUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();


                        String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                        String strWriteID = this.uTextApproveUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextApproveUserID.Text = "";
                            this.uTextApproveUserName.Text = "";
                        }
                        else
                        {
                            this.uTextApproveUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0003A_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.Cell.Column.ToString() == "FileName")
                {
                    if (e.Cell.Value.ToString() == "" || e.Cell.ToString().Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000356",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();


                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);


                        arrFile.Add(e.Cell.Value.ToString());
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (this.uTextStandardDocNo.Text + "-" + this.uTextRevisionNo.Text),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        fileAtt.ShowDialog();
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmISO0003A_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //승인시 메일 발송
        private void SendMail(string strPlantCode, string strStdNumber, string strVersionNum, DataTable dtHeader)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                //첨부문서를 위한 FileServer 연결정보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(PlantCode, "S02");

                //첨부화일 저장경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysTargetFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysTargetFilePath);
                DataTable dtTargetFilePath = clsSysTargetFilePath.mfReadSystemFilePathDetail(PlantCode, "D0006");

                //메일 발송 파일 복사 경로
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFileDestPath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFileDestPath);
                DataTable dtDestFilePath = clsSysFileDestPath.mfReadSystemFilePathDetail(PlantCode, "D0022");


                //첨부할 파일 검색
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                QRPISO.BL.ISODOC.ISODocFile clsFile = new QRPISO.BL.ISODOC.ISODocFile();
                brwChannel.mfCredentials(clsFile);
                DataTable dtFileAtt = clsFile.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));

                //메일 주소 받기
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                QRPISO.BL.ISODOC.ISODocVendor clsVendor = new QRPISO.BL.ISODOC.ISODocVendor();
                brwChannel.mfCredentials(clsVendor);

                //첨부문서 FileServer로 보내기
                QRPCOM.frmWebClientFile fileAtt = new QRPCOM.frmWebClientFile();
                ArrayList arrFile = new ArrayList();
                ArrayList arrAttachFile = new ArrayList();
                ArrayList arrFileNonPath = new ArrayList();

                DataTable dtVendor = clsVendor.mfReadISODocVendor(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));

                string strMailTitle = "";
                string strMailDesc = "";
                ////문서 상태(신규 / 개정 / 폐기) 확인 후 메일 제목, 내용 선택
                //if (dtHeader.Rows[0]["RevDisuseTypeCode"].ToString().Equals("2"))
                //{
                //    strMailTitle = "표준문서 폐기에 의한 알림";
                //    strMailDesc = "해당 표준문서 폐기";
                //}
                //else if (dtHeader.Rows[0]["RevDisuseTypeCode"].ToString().Equals("1"))
                //{
                //    strMailTitle = "표준문서 개정에 의한 알림";
                //    strMailDesc = "해당 표준문서 개정";
                //}
                //else
                //{
                //    strMailTitle = "표준문서 재정에 의한 알림";
                //    strMailDesc = "해당 표준문서 재정";
                //}
                string strDocTitle = dtHeader.Rows[0]["DocTitle"].ToString();
                string strAdmitVersionNum = dtHeader.Rows[0]["AdmitVersionNum"].ToString();
                strMailTitle = "표준문서 : " + strDocTitle + "최종 승인";
                strMailDesc = "문서번호 : " + strStdNumber + "/ 개정번호 :" + strAdmitVersionNum + " / 제목 : " + strDocTitle + "\r\n" + "문서가 최종승인 돼었습니다.";

                for (int i = 0; i < dtFileAtt.Rows.Count; i++)
                {
                    string DestFile = dtDestFilePath.Rows[0]["FolderName"].ToString() + "/" + dtFileAtt.Rows[i]["FileName"].ToString();
                    string TargetFile = "ISODocFile/" + strStdNumber + "-" + strVersionNum + "/" + dtFileAtt.Rows[i]["FileName"].ToString();
                    string NonPathFile = dtFileAtt.Rows[i]["FileName"].ToString();

                    arrAttachFile.Add(TargetFile);
                    arrFile.Add(DestFile);
                    arrFileNonPath.Add(NonPathFile);
                }
                fileAtt.mfInitSetSystemFileCopyInfo(arrFile
                                                                    , dtSysAccess.Rows[0]["SystemAddressPath"].ToString()
                                                                    , arrAttachFile
                                                                    , dtTargetFilePath.Rows[0]["ServerPath"].ToString()
                                                                    , dtDestFilePath.Rows[0]["FolderName"].ToString()
                                                                    , dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());

                ////메일보내기
                brwChannel.mfRegisterChannel(typeof(QRPCOM.BL.Mail), "Mail");
                QRPCOM.BL.Mail clsMail = new QRPCOM.BL.Mail();
                brwChannel.mfCredentials(clsMail);

                for (int i = 0; i < dtVendor.Rows.Count; i++)
                {
                    if (!dtVendor.Rows[i]["Email"].ToString().Equals(string.Empty))
                    {
                        if (arrFile.Count != 0)
                        {
                            fileAtt.mfFileUpload_NonAssync();
                        }

                        bool bolRtn = clsMail.mfSendSMTPMail(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_EMAIL"), m_resSys.GetString("SYS_USERNAME")
                            , dtVendor.Rows[i]["Email"].ToString(), strMailTitle, strMailDesc, arrFileNonPath);
                        if (!bolRtn)
                        {
                            WinMessageBox msg = new WinMessageBox();

                            msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M000083", "담당자 메일전송 확인"
                                    , "M000404",
                                    Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
