﻿namespace QRPISO.UI
{
    partial class frmISO0003R
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0003R));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uCheckVersion = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelVersionCheck = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchRegToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchRegFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchRegDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSearchStandardNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchSmall = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMiddle = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchLarge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextDocKeyword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocKeyword = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPadSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPadSize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChipSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChipSize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReturnReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReturnReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateApplyToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateApplyFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelApplyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApproveUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextApproveUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApproveUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextFrom = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFrom = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChangeItem = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChangeItem = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRev_DisReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRev_DisReason = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRev_DisDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRev_DisDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRev_DisUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRev_DisUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRev_DisUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboRev_DisSeparation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelRev_DisSeparation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextGRWComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevUserName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextInnerAdmitVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRevUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRWComment = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakeDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMakeUse = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMakeUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSmall = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMiddle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLarge = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevisionNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStandardDocNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardDocNo = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow3 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow2 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow4 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDown = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteRow1 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uDateCompleteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelRevisionNo = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRev_DisDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRev_DisSeparation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInnerAdmitVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardDocNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckVersion);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelVersionCheck);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchRegToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchRegFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRegDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStandardNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStandardNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uCheckVersion
            // 
            this.uCheckVersion.Location = new System.Drawing.Point(1024, 12);
            this.uCheckVersion.Name = "uCheckVersion";
            this.uCheckVersion.Size = new System.Drawing.Size(16, 20);
            this.uCheckVersion.TabIndex = 33;
            this.uCheckVersion.Visible = false;
            // 
            // uLabelVersionCheck
            // 
            this.uLabelVersionCheck.Location = new System.Drawing.Point(1012, 12);
            this.uLabelVersionCheck.Name = "uLabelVersionCheck";
            this.uLabelVersionCheck.Size = new System.Drawing.Size(16, 20);
            this.uLabelVersionCheck.TabIndex = 32;
            this.uLabelVersionCheck.Text = "ultraLabel1";
            this.uLabelVersionCheck.Visible = false;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(1032, 40);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel2.TabIndex = 31;
            this.ultraLabel2.Text = "~";
            this.ultraLabel2.Visible = false;
            // 
            // uDateSearchRegToDate
            // 
            this.uDateSearchRegToDate.Location = new System.Drawing.Point(1048, 36);
            this.uDateSearchRegToDate.Name = "uDateSearchRegToDate";
            this.uDateSearchRegToDate.Size = new System.Drawing.Size(16, 21);
            this.uDateSearchRegToDate.TabIndex = 30;
            this.uDateSearchRegToDate.Visible = false;
            // 
            // uDateSearchRegFromDate
            // 
            this.uDateSearchRegFromDate.Location = new System.Drawing.Point(1016, 36);
            this.uDateSearchRegFromDate.Name = "uDateSearchRegFromDate";
            this.uDateSearchRegFromDate.Size = new System.Drawing.Size(16, 21);
            this.uDateSearchRegFromDate.TabIndex = 29;
            this.uDateSearchRegFromDate.Visible = false;
            // 
            // uLabelSearchRegDate
            // 
            this.uLabelSearchRegDate.Location = new System.Drawing.Point(996, 36);
            this.uLabelSearchRegDate.Name = "uLabelSearchRegDate";
            this.uLabelSearchRegDate.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchRegDate.TabIndex = 14;
            this.uLabelSearchRegDate.Text = "ultraLabel1";
            this.uLabelSearchRegDate.Visible = false;
            // 
            // uTextSearchTitle
            // 
            this.uTextSearchTitle.Location = new System.Drawing.Point(356, 36);
            this.uTextSearchTitle.Name = "uTextSearchTitle";
            this.uTextSearchTitle.Size = new System.Drawing.Size(382, 21);
            this.uTextSearchTitle.TabIndex = 13;
            // 
            // uLabelSearchTitle
            // 
            this.uLabelSearchTitle.Location = new System.Drawing.Point(252, 36);
            this.uLabelSearchTitle.Name = "uLabelSearchTitle";
            this.uLabelSearchTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTitle.TabIndex = 12;
            this.uLabelSearchTitle.Text = "ultraLabel1";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(1044, 12);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchProcess.TabIndex = 11;
            this.uComboSearchProcess.Text = "ultraComboEditor1";
            // 
            // uTextSearchStandardNo
            // 
            this.uTextSearchStandardNo.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchStandardNo.Name = "uTextSearchStandardNo";
            this.uTextSearchStandardNo.Size = new System.Drawing.Size(120, 21);
            this.uTextSearchStandardNo.TabIndex = 9;
            // 
            // uLabelSearchStandardNo
            // 
            this.uLabelSearchStandardNo.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchStandardNo.Name = "uLabelSearchStandardNo";
            this.uLabelSearchStandardNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStandardNo.TabIndex = 8;
            this.uLabelSearchStandardNo.Text = "ultraLabel1";
            // 
            // uComboSearchSmall
            // 
            this.uComboSearchSmall.Location = new System.Drawing.Point(860, 12);
            this.uComboSearchSmall.Name = "uComboSearchSmall";
            this.uComboSearchSmall.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchSmall.TabIndex = 7;
            this.uComboSearchSmall.Text = "ultraComboEditor1";
            // 
            // uLabelSearchSmall
            // 
            this.uLabelSearchSmall.Location = new System.Drawing.Point(756, 12);
            this.uLabelSearchSmall.Name = "uLabelSearchSmall";
            this.uLabelSearchSmall.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchSmall.TabIndex = 6;
            this.uLabelSearchSmall.Text = "ultraLabel1";
            // 
            // uComboSearchMiddle
            // 
            this.uComboSearchMiddle.Location = new System.Drawing.Point(608, 12);
            this.uComboSearchMiddle.Name = "uComboSearchMiddle";
            this.uComboSearchMiddle.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchMiddle.TabIndex = 5;
            this.uComboSearchMiddle.Text = "ultraComboEditor1";
            this.uComboSearchMiddle.ValueChanged += new System.EventHandler(this.uComboSearchMiddle_ValueChanged);
            // 
            // uLabelSearchMiddle
            // 
            this.uLabelSearchMiddle.Location = new System.Drawing.Point(504, 12);
            this.uLabelSearchMiddle.Name = "uLabelSearchMiddle";
            this.uLabelSearchMiddle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMiddle.TabIndex = 4;
            this.uLabelSearchMiddle.Text = "ultraLabel1";
            // 
            // uComboSearchLarge
            // 
            this.uComboSearchLarge.Location = new System.Drawing.Point(356, 12);
            this.uComboSearchLarge.Name = "uComboSearchLarge";
            this.uComboSearchLarge.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchLarge.TabIndex = 3;
            this.uComboSearchLarge.Text = "ultraComboEditor1";
            this.uComboSearchLarge.ValueChanged += new System.EventHandler(this.uComboSearchLarge_ValueChanged);
            // 
            // uLabelSearchLarge
            // 
            this.uLabelSearchLarge.Location = new System.Drawing.Point(252, 12);
            this.uLabelSearchLarge.Name = "uLabelSearchLarge";
            this.uLabelSearchLarge.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLarge.TabIndex = 2;
            this.uLabelSearchLarge.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid
            // 
            this.uGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance2;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid.Location = new System.Drawing.Point(0, 100);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1070, 720);
            this.uGrid.TabIndex = 2;
            this.uGrid.Text = "ultraGrid1";
            this.uGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProcess);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateCompleteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevisionNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uTextDocKeyword);
            this.ultraGroupBox2.Controls.Add(this.uLabelDocKeyword);
            this.ultraGroupBox2.Controls.Add(this.uTextDeptName);
            this.ultraGroupBox2.Controls.Add(this.uTextPadSize);
            this.ultraGroupBox2.Controls.Add(this.uLabelPadSize);
            this.ultraGroupBox2.Controls.Add(this.uTextChipSize);
            this.ultraGroupBox2.Controls.Add(this.uLabelChipSize);
            this.ultraGroupBox2.Controls.Add(this.uTextPackage);
            this.ultraGroupBox2.Controls.Add(this.uLabelPackage);
            this.ultraGroupBox2.Controls.Add(this.uTextReturnReason);
            this.ultraGroupBox2.Controls.Add(this.uLabelReturnReason);
            this.ultraGroupBox2.Controls.Add(this.uTextPlant);
            this.ultraGroupBox2.Controls.Add(this.uTextEtc);
            this.ultraGroupBox2.Controls.Add(this.uLabelEtc);
            this.ultraGroupBox2.Controls.Add(this.ultraLabel3);
            this.ultraGroupBox2.Controls.Add(this.uDateApplyToDate);
            this.ultraGroupBox2.Controls.Add(this.uDateApplyFromDate);
            this.ultraGroupBox2.Controls.Add(this.uLabelApplyDate);
            this.ultraGroupBox2.Controls.Add(this.uTextDocState);
            this.ultraGroupBox2.Controls.Add(this.uLabelDocState);
            this.ultraGroupBox2.Controls.Add(this.uTextApproveUserName);
            this.ultraGroupBox2.Controls.Add(this.uTextApproveUserID);
            this.ultraGroupBox2.Controls.Add(this.uLabelApproveUser);
            this.ultraGroupBox2.Controls.Add(this.uTextTo);
            this.ultraGroupBox2.Controls.Add(this.uLabelTo);
            this.ultraGroupBox2.Controls.Add(this.uTextFrom);
            this.ultraGroupBox2.Controls.Add(this.uLabelFrom);
            this.ultraGroupBox2.Controls.Add(this.uTextChangeItem);
            this.ultraGroupBox2.Controls.Add(this.uLabelChangeItem);
            this.ultraGroupBox2.Controls.Add(this.uTextRev_DisReason);
            this.ultraGroupBox2.Controls.Add(this.uLabelRev_DisReason);
            this.ultraGroupBox2.Controls.Add(this.uDateRev_DisDate);
            this.ultraGroupBox2.Controls.Add(this.uLabelRev_DisDate);
            this.ultraGroupBox2.Controls.Add(this.uTextRev_DisUserName);
            this.ultraGroupBox2.Controls.Add(this.uTextRev_DisUserID);
            this.ultraGroupBox2.Controls.Add(this.uLabelRev_DisUser);
            this.ultraGroupBox2.Controls.Add(this.uComboRev_DisSeparation);
            this.ultraGroupBox2.Controls.Add(this.uLabelRev_DisSeparation);
            this.ultraGroupBox2.Controls.Add(this.uLabelPlant);
            this.ultraGroupBox2.Location = new System.Drawing.Point(12, 57);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1051, 204);
            this.ultraGroupBox2.TabIndex = 116;
            // 
            // uTextDocKeyword
            // 
            this.uTextDocKeyword.Location = new System.Drawing.Point(168, 55);
            this.uTextDocKeyword.Name = "uTextDocKeyword";
            this.uTextDocKeyword.Size = new System.Drawing.Size(200, 21);
            this.uTextDocKeyword.TabIndex = 4;
            // 
            // uLabelDocKeyword
            // 
            this.uLabelDocKeyword.Location = new System.Drawing.Point(12, 57);
            this.uLabelDocKeyword.Name = "uLabelDocKeyword";
            this.uLabelDocKeyword.Size = new System.Drawing.Size(150, 20);
            this.uLabelDocKeyword.TabIndex = 154;
            this.uLabelDocKeyword.Text = "ultraLabel1";
            // 
            // uTextDeptName
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptName.Appearance = appearance14;
            this.uTextDeptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptName.Location = new System.Drawing.Point(768, 8);
            this.uTextDeptName.Name = "uTextDeptName";
            this.uTextDeptName.ReadOnly = true;
            this.uTextDeptName.Size = new System.Drawing.Size(100, 21);
            this.uTextDeptName.TabIndex = 153;
            // 
            // uTextPadSize
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPadSize.Appearance = appearance15;
            this.uTextPadSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPadSize.Location = new System.Drawing.Point(168, 124);
            this.uTextPadSize.Name = "uTextPadSize";
            this.uTextPadSize.ReadOnly = true;
            this.uTextPadSize.Size = new System.Drawing.Size(200, 21);
            this.uTextPadSize.TabIndex = 7;
            // 
            // uLabelPadSize
            // 
            this.uLabelPadSize.Location = new System.Drawing.Point(12, 127);
            this.uLabelPadSize.Name = "uLabelPadSize";
            this.uLabelPadSize.Size = new System.Drawing.Size(150, 20);
            this.uLabelPadSize.TabIndex = 149;
            this.uLabelPadSize.Text = "ultraLabel1";
            // 
            // uTextChipSize
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChipSize.Appearance = appearance16;
            this.uTextChipSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChipSize.Location = new System.Drawing.Point(168, 101);
            this.uTextChipSize.Name = "uTextChipSize";
            this.uTextChipSize.ReadOnly = true;
            this.uTextChipSize.Size = new System.Drawing.Size(200, 21);
            this.uTextChipSize.TabIndex = 6;
            // 
            // uLabelChipSize
            // 
            this.uLabelChipSize.Location = new System.Drawing.Point(12, 104);
            this.uLabelChipSize.Name = "uLabelChipSize";
            this.uLabelChipSize.Size = new System.Drawing.Size(150, 20);
            this.uLabelChipSize.TabIndex = 147;
            this.uLabelChipSize.Text = "ultraLabel1";
            // 
            // uTextPackage
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance17;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(168, 78);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(200, 21);
            this.uTextPackage.TabIndex = 5;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(12, 81);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(150, 20);
            this.uLabelPackage.TabIndex = 145;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uTextReturnReason
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnReason.Appearance = appearance18;
            this.uTextReturnReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnReason.Location = new System.Drawing.Point(960, 8);
            this.uTextReturnReason.Name = "uTextReturnReason";
            this.uTextReturnReason.ReadOnly = true;
            this.uTextReturnReason.Size = new System.Drawing.Size(12, 21);
            this.uTextReturnReason.TabIndex = 144;
            this.uTextReturnReason.Visible = false;
            // 
            // uLabelReturnReason
            // 
            this.uLabelReturnReason.Location = new System.Drawing.Point(948, 8);
            this.uLabelReturnReason.Name = "uLabelReturnReason";
            this.uLabelReturnReason.Size = new System.Drawing.Size(8, 20);
            this.uLabelReturnReason.TabIndex = 143;
            this.uLabelReturnReason.Text = "ultraLabel1";
            this.uLabelReturnReason.Visible = false;
            // 
            // uTextPlant
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance19;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(916, 6);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(16, 21);
            this.uTextPlant.TabIndex = 142;
            this.uTextPlant.Visible = false;
            // 
            // uTextEtc
            // 
            this.uTextEtc.Location = new System.Drawing.Point(564, 169);
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.Size = new System.Drawing.Size(476, 21);
            this.uTextEtc.TabIndex = 14;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(408, 169);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(150, 20);
            this.uLabelEtc.TabIndex = 140;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(277, 152);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel3.TabIndex = 139;
            this.ultraLabel3.Text = "~";
            // 
            // uDateApplyToDate
            // 
            this.uDateApplyToDate.Location = new System.Drawing.Point(297, 148);
            this.uDateApplyToDate.Name = "uDateApplyToDate";
            this.uDateApplyToDate.Size = new System.Drawing.Size(104, 21);
            this.uDateApplyToDate.TabIndex = 9;
            // 
            // uDateApplyFromDate
            // 
            this.uDateApplyFromDate.Location = new System.Drawing.Point(168, 148);
            this.uDateApplyFromDate.Name = "uDateApplyFromDate";
            this.uDateApplyFromDate.Size = new System.Drawing.Size(104, 21);
            this.uDateApplyFromDate.TabIndex = 8;
            // 
            // uLabelApplyDate
            // 
            this.uLabelApplyDate.Location = new System.Drawing.Point(12, 151);
            this.uLabelApplyDate.Name = "uLabelApplyDate";
            this.uLabelApplyDate.Size = new System.Drawing.Size(150, 20);
            this.uLabelApplyDate.TabIndex = 136;
            this.uLabelApplyDate.Text = "ultraLabel1";
            // 
            // uTextDocState
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Appearance = appearance20;
            this.uTextDocState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Location = new System.Drawing.Point(168, 32);
            this.uTextDocState.Name = "uTextDocState";
            this.uTextDocState.ReadOnly = true;
            this.uTextDocState.Size = new System.Drawing.Size(200, 21);
            this.uTextDocState.TabIndex = 3;
            // 
            // uLabelDocState
            // 
            this.uLabelDocState.Location = new System.Drawing.Point(12, 34);
            this.uLabelDocState.Name = "uLabelDocState";
            this.uLabelDocState.Size = new System.Drawing.Size(150, 20);
            this.uLabelDocState.TabIndex = 134;
            this.uLabelDocState.Text = "ultraLabel1";
            // 
            // uTextApproveUserName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Appearance = appearance21;
            this.uTextApproveUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Location = new System.Drawing.Point(1024, 12);
            this.uTextApproveUserName.Name = "uTextApproveUserName";
            this.uTextApproveUserName.ReadOnly = true;
            this.uTextApproveUserName.Size = new System.Drawing.Size(15, 21);
            this.uTextApproveUserName.TabIndex = 133;
            this.uTextApproveUserName.Visible = false;
            // 
            // uTextApproveUserID
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextApproveUserID.Appearance = appearance22;
            this.uTextApproveUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance23.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance23.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance23;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextApproveUserID.ButtonsRight.Add(editorButton1);
            this.uTextApproveUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextApproveUserID.Location = new System.Drawing.Point(1012, 12);
            this.uTextApproveUserID.Name = "uTextApproveUserID";
            this.uTextApproveUserID.Size = new System.Drawing.Size(15, 21);
            this.uTextApproveUserID.TabIndex = 132;
            this.uTextApproveUserID.Visible = false;
            this.uTextApproveUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextApproveUserID_KeyDown);
            this.uTextApproveUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextApproveUserID_EditorButtonClick);
            // 
            // uLabelApproveUser
            // 
            this.uLabelApproveUser.Location = new System.Drawing.Point(992, 12);
            this.uLabelApproveUser.Name = "uLabelApproveUser";
            this.uLabelApproveUser.Size = new System.Drawing.Size(15, 20);
            this.uLabelApproveUser.TabIndex = 131;
            this.uLabelApproveUser.Text = "ultraLabel1";
            this.uLabelApproveUser.Visible = false;
            // 
            // uTextTo
            // 
            appearance24.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTo.Appearance = appearance24;
            this.uTextTo.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextTo.Location = new System.Drawing.Point(564, 126);
            this.uTextTo.Multiline = true;
            this.uTextTo.Name = "uTextTo";
            this.uTextTo.Size = new System.Drawing.Size(476, 41);
            this.uTextTo.TabIndex = 13;
            // 
            // uLabelTo
            // 
            this.uLabelTo.Location = new System.Drawing.Point(408, 126);
            this.uLabelTo.Name = "uLabelTo";
            this.uLabelTo.Size = new System.Drawing.Size(150, 20);
            this.uLabelTo.TabIndex = 129;
            this.uLabelTo.Text = "ultraLabel1";
            // 
            // uTextFrom
            // 
            appearance25.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextFrom.Appearance = appearance25;
            this.uTextFrom.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextFrom.Location = new System.Drawing.Point(564, 78);
            this.uTextFrom.Multiline = true;
            this.uTextFrom.Name = "uTextFrom";
            this.uTextFrom.Size = new System.Drawing.Size(476, 45);
            this.uTextFrom.TabIndex = 12;
            // 
            // uLabelFrom
            // 
            this.uLabelFrom.Location = new System.Drawing.Point(408, 78);
            this.uLabelFrom.Name = "uLabelFrom";
            this.uLabelFrom.Size = new System.Drawing.Size(150, 20);
            this.uLabelFrom.TabIndex = 127;
            this.uLabelFrom.Text = "ultraLabel1";
            // 
            // uTextChangeItem
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextChangeItem.Appearance = appearance26;
            this.uTextChangeItem.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextChangeItem.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextChangeItem.Location = new System.Drawing.Point(564, 55);
            this.uTextChangeItem.Name = "uTextChangeItem";
            this.uTextChangeItem.Size = new System.Drawing.Size(476, 21);
            this.uTextChangeItem.TabIndex = 11;
            // 
            // uLabelChangeItem
            // 
            this.uLabelChangeItem.Location = new System.Drawing.Point(408, 55);
            this.uLabelChangeItem.Name = "uLabelChangeItem";
            this.uLabelChangeItem.Size = new System.Drawing.Size(150, 20);
            this.uLabelChangeItem.TabIndex = 125;
            this.uLabelChangeItem.Text = "ultraLabel1";
            // 
            // uTextRev_DisReason
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRev_DisReason.Appearance = appearance27;
            this.uTextRev_DisReason.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRev_DisReason.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRev_DisReason.Location = new System.Drawing.Point(564, 32);
            this.uTextRev_DisReason.Name = "uTextRev_DisReason";
            this.uTextRev_DisReason.Size = new System.Drawing.Size(476, 21);
            this.uTextRev_DisReason.TabIndex = 10;
            // 
            // uLabelRev_DisReason
            // 
            this.uLabelRev_DisReason.Location = new System.Drawing.Point(408, 32);
            this.uLabelRev_DisReason.Name = "uLabelRev_DisReason";
            this.uLabelRev_DisReason.Size = new System.Drawing.Size(150, 20);
            this.uLabelRev_DisReason.TabIndex = 123;
            this.uLabelRev_DisReason.Text = "ultraLabel1";
            // 
            // uDateRev_DisDate
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRev_DisDate.Appearance = appearance28;
            this.uDateRev_DisDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateRev_DisDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateRev_DisDate.Location = new System.Drawing.Point(848, 6);
            this.uDateRev_DisDate.Name = "uDateRev_DisDate";
            this.uDateRev_DisDate.Size = new System.Drawing.Size(16, 21);
            this.uDateRev_DisDate.TabIndex = 11;
            this.uDateRev_DisDate.Visible = false;
            // 
            // uLabelRev_DisDate
            // 
            this.uLabelRev_DisDate.Location = new System.Drawing.Point(832, 4);
            this.uLabelRev_DisDate.Name = "uLabelRev_DisDate";
            this.uLabelRev_DisDate.Size = new System.Drawing.Size(16, 20);
            this.uLabelRev_DisDate.TabIndex = 121;
            this.uLabelRev_DisDate.Text = "ultraLabel1";
            this.uLabelRev_DisDate.Visible = false;
            // 
            // uTextRev_DisUserName
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRev_DisUserName.Appearance = appearance29;
            this.uTextRev_DisUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRev_DisUserName.Location = new System.Drawing.Point(665, 8);
            this.uTextRev_DisUserName.Name = "uTextRev_DisUserName";
            this.uTextRev_DisUserName.ReadOnly = true;
            this.uTextRev_DisUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRev_DisUserName.TabIndex = 10;
            // 
            // uTextRev_DisUserID
            // 
            appearance30.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRev_DisUserID.Appearance = appearance30;
            this.uTextRev_DisUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance31.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance31.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance31;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRev_DisUserID.ButtonsRight.Add(editorButton2);
            this.uTextRev_DisUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRev_DisUserID.Location = new System.Drawing.Point(564, 8);
            this.uTextRev_DisUserID.Name = "uTextRev_DisUserID";
            this.uTextRev_DisUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRev_DisUserID.TabIndex = 2;
            this.uTextRev_DisUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRev_DisUserID_KeyDown);
            this.uTextRev_DisUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRev_DisUserID_EditorButtonClick);
            // 
            // uLabelRev_DisUser
            // 
            this.uLabelRev_DisUser.Location = new System.Drawing.Point(408, 8);
            this.uLabelRev_DisUser.Name = "uLabelRev_DisUser";
            this.uLabelRev_DisUser.Size = new System.Drawing.Size(150, 20);
            this.uLabelRev_DisUser.TabIndex = 118;
            this.uLabelRev_DisUser.Text = "ultraLabel1";
            // 
            // uComboRev_DisSeparation
            // 
            this.uComboRev_DisSeparation.Location = new System.Drawing.Point(168, 8);
            this.uComboRev_DisSeparation.Name = "uComboRev_DisSeparation";
            this.uComboRev_DisSeparation.Size = new System.Drawing.Size(200, 21);
            this.uComboRev_DisSeparation.TabIndex = 1;
            this.uComboRev_DisSeparation.Text = "ultraComboEditor1";
            this.uComboRev_DisSeparation.AfterCloseUp += new System.EventHandler(this.uComboRev_DisSeparation_AfterCloseUp);
            this.uComboRev_DisSeparation.ValueChanged += new System.EventHandler(this.uComboRev_DisSeparation_ValueChanged);
            // 
            // uLabelRev_DisSeparation
            // 
            this.uLabelRev_DisSeparation.Location = new System.Drawing.Point(12, 10);
            this.uLabelRev_DisSeparation.Name = "uLabelRev_DisSeparation";
            this.uLabelRev_DisSeparation.Size = new System.Drawing.Size(150, 20);
            this.uLabelRev_DisSeparation.TabIndex = 116;
            this.uLabelRev_DisSeparation.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(904, 4);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(12, 20);
            this.uLabelPlant.TabIndex = 115;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uTextGRWComment);
            this.ultraGroupBox1.Controls.Add(this.uLabelRevUserName);
            this.ultraGroupBox1.Controls.Add(this.uTextInnerAdmitVersionNum);
            this.ultraGroupBox1.Controls.Add(this.uTextRevUserName);
            this.ultraGroupBox1.Controls.Add(this.uLabelGRWComment);
            this.ultraGroupBox1.Controls.Add(this.uTextAdmitVersionNum);
            this.ultraGroupBox1.Controls.Add(this.uTextMakeDeptName);
            this.ultraGroupBox1.Controls.Add(this.uLabelMakeUse);
            this.ultraGroupBox1.Controls.Add(this.uTextMakeUserName);
            this.ultraGroupBox1.Controls.Add(this.uTextCreateDate);
            this.ultraGroupBox1.Controls.Add(this.uTextSmall);
            this.ultraGroupBox1.Controls.Add(this.uTextMiddle);
            this.ultraGroupBox1.Controls.Add(this.uTextLarge);
            this.ultraGroupBox1.Controls.Add(this.uTextTitle);
            this.ultraGroupBox1.Controls.Add(this.uLabelTitle);
            this.ultraGroupBox1.Controls.Add(this.uTextCreateUserID);
            this.ultraGroupBox1.Controls.Add(this.uLabelCreateDate);
            this.ultraGroupBox1.Controls.Add(this.uTextCreateUserName);
            this.ultraGroupBox1.Controls.Add(this.uLabelCreateUser);
            this.ultraGroupBox1.Controls.Add(this.uLabelSmall);
            this.ultraGroupBox1.Controls.Add(this.uLabelMiddle);
            this.ultraGroupBox1.Controls.Add(this.uLabelLarge);
            this.ultraGroupBox1.Controls.Add(this.uTextRevisionNo);
            this.ultraGroupBox1.Controls.Add(this.uTextStandardDocNo);
            this.ultraGroupBox1.Controls.Add(this.uLabelStandardDocNo);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1051, 56);
            this.ultraGroupBox1.TabIndex = 115;
            // 
            // uTextGRWComment
            // 
            this.uTextGRWComment.Location = new System.Drawing.Point(560, 28);
            this.uTextGRWComment.Name = "uTextGRWComment";
            this.uTextGRWComment.Size = new System.Drawing.Size(480, 21);
            this.uTextGRWComment.TabIndex = 19;
            // 
            // uLabelRevUserName
            // 
            this.uLabelRevUserName.Location = new System.Drawing.Point(776, 8);
            this.uLabelRevUserName.Name = "uLabelRevUserName";
            this.uLabelRevUserName.Size = new System.Drawing.Size(8, 16);
            this.uLabelRevUserName.TabIndex = 152;
            this.uLabelRevUserName.Text = "ultraLabel1";
            this.uLabelRevUserName.Visible = false;
            // 
            // uTextInnerAdmitVersionNum
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInnerAdmitVersionNum.Appearance = appearance32;
            this.uTextInnerAdmitVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInnerAdmitVersionNum.Location = new System.Drawing.Point(328, 32);
            this.uTextInnerAdmitVersionNum.Name = "uTextInnerAdmitVersionNum";
            this.uTextInnerAdmitVersionNum.ReadOnly = true;
            this.uTextInnerAdmitVersionNum.Size = new System.Drawing.Size(12, 21);
            this.uTextInnerAdmitVersionNum.TabIndex = 2;
            this.uTextInnerAdmitVersionNum.Visible = false;
            // 
            // uTextRevUserName
            // 
            appearance33.BackColor = System.Drawing.Color.White;
            this.uTextRevUserName.Appearance = appearance33;
            this.uTextRevUserName.BackColor = System.Drawing.Color.White;
            this.uTextRevUserName.Location = new System.Drawing.Point(788, 8);
            this.uTextRevUserName.Name = "uTextRevUserName";
            this.uTextRevUserName.Size = new System.Drawing.Size(8, 21);
            this.uTextRevUserName.TabIndex = 12;
            this.uTextRevUserName.Visible = false;
            // 
            // uLabelGRWComment
            // 
            this.uLabelGRWComment.Location = new System.Drawing.Point(404, 32);
            this.uLabelGRWComment.Name = "uLabelGRWComment";
            this.uLabelGRWComment.Size = new System.Drawing.Size(150, 20);
            this.uLabelGRWComment.TabIndex = 153;
            this.uLabelGRWComment.Text = "ultraLabel1";
            // 
            // uTextAdmitVersionNum
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitVersionNum.Appearance = appearance34;
            this.uTextAdmitVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitVersionNum.Location = new System.Drawing.Point(168, 32);
            this.uTextAdmitVersionNum.Name = "uTextAdmitVersionNum";
            this.uTextAdmitVersionNum.ReadOnly = true;
            this.uTextAdmitVersionNum.Size = new System.Drawing.Size(228, 21);
            this.uTextAdmitVersionNum.TabIndex = 6;
            // 
            // uTextMakeDeptName
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeDeptName.Appearance = appearance35;
            this.uTextMakeDeptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeDeptName.Location = new System.Drawing.Point(952, 28);
            this.uTextMakeDeptName.Name = "uTextMakeDeptName";
            this.uTextMakeDeptName.ReadOnly = true;
            this.uTextMakeDeptName.Size = new System.Drawing.Size(14, 21);
            this.uTextMakeDeptName.TabIndex = 104;
            this.uTextMakeDeptName.Visible = false;
            // 
            // uLabelMakeUse
            // 
            this.uLabelMakeUse.Location = new System.Drawing.Point(1012, 32);
            this.uLabelMakeUse.Name = "uLabelMakeUse";
            this.uLabelMakeUse.Size = new System.Drawing.Size(17, 20);
            this.uLabelMakeUse.TabIndex = 103;
            this.uLabelMakeUse.Text = "ultraLabel1";
            this.uLabelMakeUse.Visible = false;
            // 
            // uTextMakeUserName
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeUserName.Appearance = appearance36;
            this.uTextMakeUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeUserName.Location = new System.Drawing.Point(1008, 4);
            this.uTextMakeUserName.Name = "uTextMakeUserName";
            this.uTextMakeUserName.ReadOnly = true;
            this.uTextMakeUserName.Size = new System.Drawing.Size(17, 21);
            this.uTextMakeUserName.TabIndex = 102;
            this.uTextMakeUserName.Visible = false;
            // 
            // uTextCreateDate
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Appearance = appearance37;
            this.uTextCreateDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Location = new System.Drawing.Point(1000, 32);
            this.uTextCreateDate.Name = "uTextCreateDate";
            this.uTextCreateDate.ReadOnly = true;
            this.uTextCreateDate.Size = new System.Drawing.Size(9, 21);
            this.uTextCreateDate.TabIndex = 101;
            this.uTextCreateDate.Visible = false;
            // 
            // uTextSmall
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSmall.Appearance = appearance38;
            this.uTextSmall.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSmall.Location = new System.Drawing.Point(892, 16);
            this.uTextSmall.Name = "uTextSmall";
            this.uTextSmall.ReadOnly = true;
            this.uTextSmall.Size = new System.Drawing.Size(24, 21);
            this.uTextSmall.TabIndex = 4;
            this.uTextSmall.Visible = false;
            // 
            // uTextMiddle
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMiddle.Appearance = appearance39;
            this.uTextMiddle.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMiddle.Location = new System.Drawing.Point(872, 14);
            this.uTextMiddle.Name = "uTextMiddle";
            this.uTextMiddle.ReadOnly = true;
            this.uTextMiddle.Size = new System.Drawing.Size(24, 21);
            this.uTextMiddle.TabIndex = 3;
            this.uTextMiddle.Visible = false;
            // 
            // uTextLarge
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLarge.Appearance = appearance40;
            this.uTextLarge.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLarge.Location = new System.Drawing.Point(836, 12);
            this.uTextLarge.Name = "uTextLarge";
            this.uTextLarge.ReadOnly = true;
            this.uTextLarge.Size = new System.Drawing.Size(24, 21);
            this.uTextLarge.TabIndex = 1;
            this.uTextLarge.Visible = false;
            // 
            // uTextTitle
            // 
            this.uTextTitle.Location = new System.Drawing.Point(560, 4);
            this.uTextTitle.Name = "uTextTitle";
            this.uTextTitle.Size = new System.Drawing.Size(480, 21);
            this.uTextTitle.TabIndex = 7;
            // 
            // uLabelTitle
            // 
            this.uLabelTitle.Location = new System.Drawing.Point(404, 8);
            this.uLabelTitle.Name = "uLabelTitle";
            this.uLabelTitle.Size = new System.Drawing.Size(150, 20);
            this.uLabelTitle.TabIndex = 96;
            this.uLabelTitle.Text = "ultraLabel1";
            // 
            // uTextCreateUserID
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Appearance = appearance41;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Location = new System.Drawing.Point(972, 28);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.ReadOnly = true;
            this.uTextCreateUserID.Size = new System.Drawing.Size(16, 21);
            this.uTextCreateUserID.TabIndex = 95;
            this.uTextCreateUserID.Visible = false;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(996, 4);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(17, 20);
            this.uLabelCreateDate.TabIndex = 94;
            this.uLabelCreateDate.Text = "ultraLabel1";
            this.uLabelCreateDate.Visible = false;
            // 
            // uTextCreateUserName
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Appearance = appearance42;
            this.uTextCreateUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Location = new System.Drawing.Point(972, 4);
            this.uTextCreateUserName.Name = "uTextCreateUserName";
            this.uTextCreateUserName.ReadOnly = true;
            this.uTextCreateUserName.Size = new System.Drawing.Size(16, 21);
            this.uTextCreateUserName.TabIndex = 93;
            this.uTextCreateUserName.Visible = false;
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(964, 4);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(15, 20);
            this.uLabelCreateUser.TabIndex = 92;
            this.uLabelCreateUser.Text = "ultraLabel1";
            this.uLabelCreateUser.Visible = false;
            // 
            // uLabelSmall
            // 
            this.uLabelSmall.Location = new System.Drawing.Point(888, 16);
            this.uLabelSmall.Name = "uLabelSmall";
            this.uLabelSmall.Size = new System.Drawing.Size(24, 20);
            this.uLabelSmall.TabIndex = 91;
            this.uLabelSmall.Text = "ultraLabel1";
            this.uLabelSmall.Visible = false;
            // 
            // uLabelMiddle
            // 
            this.uLabelMiddle.Location = new System.Drawing.Point(848, 12);
            this.uLabelMiddle.Name = "uLabelMiddle";
            this.uLabelMiddle.Size = new System.Drawing.Size(24, 20);
            this.uLabelMiddle.TabIndex = 90;
            this.uLabelMiddle.Text = "ultraLabel1";
            this.uLabelMiddle.Visible = false;
            // 
            // uLabelLarge
            // 
            this.uLabelLarge.Location = new System.Drawing.Point(812, 12);
            this.uLabelLarge.Name = "uLabelLarge";
            this.uLabelLarge.Size = new System.Drawing.Size(24, 20);
            this.uLabelLarge.TabIndex = 89;
            this.uLabelLarge.Text = "ultraLabel1";
            this.uLabelLarge.Visible = false;
            // 
            // uTextRevisionNo
            // 
            appearance43.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionNo.Appearance = appearance43;
            this.uTextRevisionNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionNo.Location = new System.Drawing.Point(320, 32);
            this.uTextRevisionNo.Name = "uTextRevisionNo";
            this.uTextRevisionNo.ReadOnly = true;
            this.uTextRevisionNo.Size = new System.Drawing.Size(20, 21);
            this.uTextRevisionNo.TabIndex = 100;
            this.uTextRevisionNo.Visible = false;
            // 
            // uTextStandardDocNo
            // 
            appearance44.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardDocNo.Appearance = appearance44;
            this.uTextStandardDocNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardDocNo.Location = new System.Drawing.Point(168, 8);
            this.uTextStandardDocNo.Name = "uTextStandardDocNo";
            this.uTextStandardDocNo.ReadOnly = true;
            this.uTextStandardDocNo.Size = new System.Drawing.Size(228, 21);
            this.uTextStandardDocNo.TabIndex = 5;
            // 
            // uLabelStandardDocNo
            // 
            this.uLabelStandardDocNo.Location = new System.Drawing.Point(12, 8);
            this.uLabelStandardDocNo.Name = "uLabelStandardDocNo";
            this.uLabelStandardDocNo.Size = new System.Drawing.Size(150, 20);
            this.uLabelStandardDocNo.TabIndex = 86;
            this.uLabelStandardDocNo.Text = "ultraLabel1";
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uButtonDeleteRow3);
            this.uGroupBox3.Controls.Add(this.uGrid3);
            this.uGroupBox3.Location = new System.Drawing.Point(548, 264);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(516, 196);
            this.uGroupBox3.TabIndex = 79;
            // 
            // uButtonDeleteRow3
            // 
            this.uButtonDeleteRow3.Location = new System.Drawing.Point(12, 24);
            this.uButtonDeleteRow3.Name = "uButtonDeleteRow3";
            this.uButtonDeleteRow3.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow3.TabIndex = 39;
            this.uButtonDeleteRow3.Text = "ultraButton1";
            this.uButtonDeleteRow3.Click += new System.EventHandler(this.uButtonDeleteRow3_Click);
            // 
            // uGrid3
            // 
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance45;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance46.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance46.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance46;
            appearance47.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance47;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance48.BackColor2 = System.Drawing.SystemColors.Control;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance48;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance49;
            appearance50.BackColor = System.Drawing.SystemColors.Highlight;
            appearance50.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance50;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance51;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            appearance52.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance52;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance53.BackColor = System.Drawing.SystemColors.Control;
            appearance53.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance53.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance53.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance53;
            appearance54.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance54;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance55;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance56.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance56;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(12, 60);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(492, 124);
            this.uGrid3.TabIndex = 37;
            this.uGrid3.Text = "ultraGrid2";
            this.uGrid3.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid3_AfterCellUpdate);
            // 
            // uTextProcess
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Appearance = appearance57;
            this.uTextProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Location = new System.Drawing.Point(1036, 8);
            this.uTextProcess.Name = "uTextProcess";
            this.uTextProcess.ReadOnly = true;
            this.uTextProcess.Size = new System.Drawing.Size(22, 21);
            this.uTextProcess.TabIndex = 74;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uButtonDeleteRow2);
            this.uGroupBox2.Controls.Add(this.uGrid2);
            this.uGroupBox2.Controls.Add(this.uGroupBox4);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 468);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1052, 196);
            this.uGroupBox2.TabIndex = 72;
            // 
            // uButtonDeleteRow2
            // 
            this.uButtonDeleteRow2.Location = new System.Drawing.Point(12, 24);
            this.uButtonDeleteRow2.Name = "uButtonDeleteRow2";
            this.uButtonDeleteRow2.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow2.TabIndex = 39;
            this.uButtonDeleteRow2.Text = "ultraButton1";
            this.uButtonDeleteRow2.Click += new System.EventHandler(this.uButtonDeleteRow2_Click);
            // 
            // uGrid2
            // 
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance58;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance59.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance59.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance59;
            appearance60.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance60;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance61.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance61.BackColor2 = System.Drawing.SystemColors.Control;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance61.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance61;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance62.BackColor = System.Drawing.SystemColors.Window;
            appearance62.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance62;
            appearance63.BackColor = System.Drawing.SystemColors.Highlight;
            appearance63.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance63;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance64;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            appearance65.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance65;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance66.BackColor = System.Drawing.SystemColors.Control;
            appearance66.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance66.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance66.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance66.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance66;
            appearance67.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance67;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance68;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance69;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 60);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1028, 124);
            this.uGrid2.TabIndex = 37;
            this.uGrid2.Text = "ultraGrid2";
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGrid2.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_CellListSelect);
            this.uGrid2.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_ClickCellButton);
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Controls.Add(this.uButtonDeleteRow4);
            this.uGroupBox4.Controls.Add(this.uGrid4);
            this.uGroupBox4.Location = new System.Drawing.Point(600, 8);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(92, 48);
            this.uGroupBox4.TabIndex = 82;
            this.uGroupBox4.Visible = false;
            // 
            // uButtonDeleteRow4
            // 
            this.uButtonDeleteRow4.Location = new System.Drawing.Point(8, 4);
            this.uButtonDeleteRow4.Name = "uButtonDeleteRow4";
            this.uButtonDeleteRow4.Size = new System.Drawing.Size(52, 20);
            this.uButtonDeleteRow4.TabIndex = 39;
            this.uButtonDeleteRow4.Text = "ultraButton1";
            this.uButtonDeleteRow4.Visible = false;
            this.uButtonDeleteRow4.Click += new System.EventHandler(this.uButtonDeleteRow4_Click);
            // 
            // uGrid4
            // 
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4.DisplayLayout.Appearance = appearance70;
            this.uGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance71.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance71.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance71.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.GroupByBox.Appearance = appearance71;
            appearance72.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance72;
            this.uGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance73.BackColor2 = System.Drawing.SystemColors.Control;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance73;
            this.uGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance74;
            appearance75.BackColor = System.Drawing.SystemColors.Highlight;
            appearance75.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance75;
            this.uGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.CardAreaAppearance = appearance76;
            appearance77.BorderColor = System.Drawing.Color.Silver;
            appearance77.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4.DisplayLayout.Override.CellAppearance = appearance77;
            this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance78.BackColor = System.Drawing.SystemColors.Control;
            appearance78.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance78.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance78;
            appearance79.TextHAlignAsString = "Left";
            this.uGrid4.DisplayLayout.Override.HeaderAppearance = appearance79;
            this.uGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4.DisplayLayout.Override.RowAppearance = appearance80;
            this.uGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance81;
            this.uGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4.Location = new System.Drawing.Point(8, 28);
            this.uGrid4.Name = "uGrid4";
            this.uGrid4.Size = new System.Drawing.Size(56, 20);
            this.uGrid4.TabIndex = 37;
            this.uGrid4.Text = "ultraGrid2";
            this.uGrid4.Visible = false;
            this.uGrid4.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid4_AfterCellUpdate);
            this.uGrid4.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid4_ClickCellButton);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uButtonDown);
            this.uGroupBox1.Controls.Add(this.uButtonDeleteRow1);
            this.uGroupBox1.Controls.Add(this.uGrid1);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 264);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(516, 196);
            this.uGroupBox1.TabIndex = 71;
            // 
            // uButtonDown
            // 
            this.uButtonDown.Location = new System.Drawing.Point(417, 24);
            this.uButtonDown.Name = "uButtonDown";
            this.uButtonDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonDown.TabIndex = 38;
            this.uButtonDown.Text = "ultraButton1";
            this.uButtonDown.Click += new System.EventHandler(this.uButtonDown_Click);
            // 
            // uButtonDeleteRow1
            // 
            this.uButtonDeleteRow1.Location = new System.Drawing.Point(12, 24);
            this.uButtonDeleteRow1.Name = "uButtonDeleteRow1";
            this.uButtonDeleteRow1.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow1.TabIndex = 37;
            this.uButtonDeleteRow1.Text = "ultraButton1";
            this.uButtonDeleteRow1.Click += new System.EventHandler(this.uButtonDeleteRow1_Click);
            // 
            // uGrid1
            // 
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance82;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance83.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance83.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance83.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance83;
            appearance84.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance84;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance85.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance85.BackColor2 = System.Drawing.SystemColors.Control;
            appearance85.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance85.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance85;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance86.BackColor = System.Drawing.SystemColors.Window;
            appearance86.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance86;
            appearance87.BackColor = System.Drawing.SystemColors.Highlight;
            appearance87.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance87;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance88.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance88;
            appearance89.BorderColor = System.Drawing.Color.Silver;
            appearance89.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance89;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance90.BackColor = System.Drawing.SystemColors.Control;
            appearance90.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance90.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance90;
            appearance91.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance91;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance92.BackColor = System.Drawing.SystemColors.Window;
            appearance92.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance92;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance93.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance93;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 60);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(492, 124);
            this.uGrid1.TabIndex = 35;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_ClickCellButton);
            this.uGrid1.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_CellChange);
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uDateCompleteDate
            // 
            this.uDateCompleteDate.Location = new System.Drawing.Point(672, 4);
            this.uDateCompleteDate.Name = "uDateCompleteDate";
            this.uDateCompleteDate.Size = new System.Drawing.Size(24, 21);
            this.uDateCompleteDate.TabIndex = 68;
            // 
            // uLabelRevisionNo
            // 
            this.uLabelRevisionNo.Location = new System.Drawing.Point(1040, 84);
            this.uLabelRevisionNo.Name = "uLabelRevisionNo";
            this.uLabelRevisionNo.Size = new System.Drawing.Size(20, 20);
            this.uLabelRevisionNo.TabIndex = 21;
            this.uLabelRevisionNo.Text = "ultraLabel1";
            this.uLabelRevisionNo.Visible = false;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmISO0003R
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0003R";
            this.Load += new System.EventHandler(this.frmISO0003R_Load);
            this.Activated += new System.EventHandler(this.frmISO0003R_Activated);
            this.Resize += new System.EventHandler(this.frmISO0003R_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRev_DisDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRev_DisSeparation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInnerAdmitVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardDocNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStandardNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchSmall;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMiddle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLarge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLarge;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRegDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTitle;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchRegToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchRegFromDate;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionNo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCompleteDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcess;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4;
        private Infragistics.Win.Misc.UltraButton uButtonDown;
        private Infragistics.Win.Misc.UltraLabel uLabelVersionCheck;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckVersion;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPadSize;
        private Infragistics.Win.Misc.UltraLabel uLabelPadSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChipSize;
        private Infragistics.Win.Misc.UltraLabel uLabelChipSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnReason;
        private Infragistics.Win.Misc.UltraLabel uLabelReturnReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateApplyToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateApplyFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelApplyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocState;
        private Infragistics.Win.Misc.UltraLabel uLabelDocState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelApproveUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTo;
        private Infragistics.Win.Misc.UltraLabel uLabelTo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFrom;
        private Infragistics.Win.Misc.UltraLabel uLabelFrom;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeItem;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeItem;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRev_DisReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisReason;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRev_DisDate;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRev_DisUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRev_DisUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRev_DisSeparation;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisSeparation;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeDeptName;
        private Infragistics.Win.Misc.UltraLabel uLabelMakeUse;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSmall;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMiddle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLarge;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.Misc.UltraLabel uLabelSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelLarge;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardDocNo;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardDocNo;
        private Infragistics.Win.Misc.UltraLabel uLabelRevUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitVersionNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInnerAdmitVersionNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRWComment;
        private Infragistics.Win.Misc.UltraLabel uLabelGRWComment;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDeptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocKeyword;
        private Infragistics.Win.Misc.UltraLabel uLabelDocKeyword;
    }
}