﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서관리                                          */
/* 프로그램ID   : frmISOZ0001.cs                                        */
/* 프로그램명   : 표준문서 배포요청                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinGrid;


namespace QRPISO.UI
{
    public partial class frmISOZ0001 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";
        String m_strPlantCode = "";

        QRPGlobal SysRes = new QRPGlobal();

        public frmISOZ0001()
        {
            InitializeComponent();
        }

        private void frmISOZ0001_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISOZ0001_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 배포요청", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Contorol 초기화
            SetToolAuth();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitValue();

            //QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            //grd.mfLoadGridColumnProperty(this);

        }

  


        #region 컨트롤 초기화 Method

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNum, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRevisionNum, "개정차수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelDistributeNum, "배포관리번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDistributeRequestUser, "배포요청자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDistributeRequestDate, "배포요청일", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitValue()
        {
            try
            {
                //Systeminfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //공정 관련 히든

                this.uTextDistributeRequestUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextDistributeRequestUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextDept.Text = m_resSys.GetString("SYS_DEPTNAME");
                this.uTextHiddenDeptCode.Text = m_resSys.GetString("SYS_DEPTCODE");

                //if (this.uGrid1.Rows.Count > 0)
                //{
                //    while (this.uGrid1.Rows.Count > 0)
                //    {
                //        this.uGrid1.Rows[0].Delete(false);
                //    }
                //}

                if (this.uGrid2.Rows.Count > 0)
                {
                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonAdd, "추가", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_AddTable);
                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                InitSearchPlantCombo();
                
                // Search Process ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadMASProcessGroup("", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessGroup", "ComboName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// 공장콤보 초기화
        /// </summary>
        private void InitSearchPlantCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdNumber", "표준번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "AdmitVersionNum", "개정번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "VersionNum", "이전개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "WriteID", "기안자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "WriteName", "기안자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "WriteDate", "기안일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "RevDisuseID", "개정자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "RevDisuseName", "개정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "RevDisuseDate", "개정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "RevDisuseReason", "개정사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeCode", "대분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeName", "대분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MTypeCode", "중분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "MTypeName", "중분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "STypeCode", "소분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "STypeName", "소분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Hidden Grid

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                

                // Set FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 상세그리드
                int intN = 0;
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "StdNumber", "표준번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "AdmitVersionNum", "개정번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VersionNum", "이전개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DistDeptCode", "배포부서코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 20
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DistDeptName", "배포부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                //string.Format("{0:n}")
                wGrid.mfSetGridColumn(this.uGrid2, 0, "DistNumber", "배포부수", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn,nnn,nnn", "0");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DistVendorCode", "배포업체코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DistVendorName", "배포업체", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "ReqDesc", "요청사유", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "RevDisuseID", "개정자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "RevDisuseName", "개정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "LTypeCode", "대분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "LTypeName", "대분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "MTypeCode", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "MTypeName", "중분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "STypeCode", "소분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "STypeName", "소분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //Hidden Grid

                wGrid.mfSetGridColumn(this.uGrid2, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 빈공백줄 추가
                wGrid.mfAddRowGrid(this.uGrid1, 0);
                wGrid.mfAddRowGrid(this.uGrid2, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(isoDocH);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strStdNumber = this.uTextSearchStandardNum.Text;


                String strVersionNum = this.uComboSearchRevisionNum.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();
                String strSTypeCode = this.uComboSearchSmall.Value.ToString();
                String strProcessCode = this.uComboSearchProcess.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;


                DataTable dt = isoDocH.mfReadISODocHForDistReq(strPlantCode, strStdNumber, strVersionNum, strLTypeCode, strMTypeCode, strSTypeCode, strProcessCode, strDocTitle
                                                                ,m_resSys.GetString("SYS_LANG"));

                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                int checkHidden = 0;
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        checkHidden++;
                    }
                }

                if (this.uTextDistributeRequestUserID.Text.Equals("") || this.uTextDistributeRequestUserName.Text.Equals(""))
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M001037", "M000447", Infragistics.Win.HAlign.Right);

                    this.uTextDistributeRequestUserID.Focus();
                    return;
                }
                //하단 그리드에 배포요청문서가 없을경우(삭제버튼을 눌러 Hidden처리된 문서는 처리하지 않는다.)
                else if (this.uGrid2.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M001037", "M000445", Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (checkHidden == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001135", "M001037", "M000445", Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    DataTable dtISODocDIstReqH = new DataTable();
                    DataTable dtSaveH = new DataTable();
                    DataTable dtSaveD = new DataTable();
                    DataRow row;

                    QRPBrowser brwChannel = new QRPBrowser();
                    QRPISO.BL.ISODOC.ISODocDistReqD ReqD;

                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        for (int j = 0; j < this.uGrid2.Rows.Count; j++)
                        {
                            if (i != j)
                            {
                                if (this.uGrid2.Rows[i].Hidden == false && this.uGrid2.Rows[j].Hidden == false)
                                {
                                    if (this.uGrid2.Rows[i].Cells["StdNumber"].Value.ToString() == this.uGrid2.Rows[j].Cells["StdNumber"].Value.ToString() &&
                                        this.uGrid2.Rows[i].Cells["AdmitVersionNum"].Value.ToString() == this.uGrid2.Rows[j].Cells["AdmitVersionNum"].Value.ToString() &&
                                        this.uGrid2.Rows[i].Cells["DistVendorCode"].Value.ToString() == this.uGrid2.Rows[j].Cells["DistVendorCode"].Value.ToString())
                                    {
                                        DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000881"
                                                                        , this.uGrid2.Rows[i].RowSelectorNumber + "M000820" + this.uGrid2.Rows[j].RowSelectorNumber + "M001352", Infragistics.Win.HAlign.Right);
                                        return;
                                    }
                                }
                            }
                        }
                    }

                    ///////ISODocDistReqD//////////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistReqD), "ISODocDistReqD");
                    ReqD = new QRPISO.BL.ISODOC.ISODocDistReqD();
                    brwChannel.mfCredentials(ReqD);

                    dtSaveD = ReqD.mfSetDataInfo();

                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        if (this.uGrid2.Rows[i].Hidden == false)
                        {
                            this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                            if (this.uGrid2.Rows[i].Cells["DistNumber"].Value.ToString() == "0" || this.uGrid2.Rows[i].Cells["DistNumber"].Value.ToString().Equals(string.Empty) || this.uGrid2.Rows[i].Cells["DistNumber"].Value == null)
                            {

                                msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M001037", this.uGrid2.Rows[i].RowSelectorNumber + "M000504",
                                                           Infragistics.Win.HAlign.Right);

                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["DistNumber"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                return;
                            }
                            else if (this.uGrid2.Rows[i].Cells["DistDeptCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                          "M001135", "M001037", this.uGrid2.Rows[i].RowSelectorNumber + "M000503",
                                                          Infragistics.Win.HAlign.Right);

                                this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["DistDeptName"];
                                this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                                return;
                            }
                            row = dtSaveD.NewRow();
                            row["PlantCode"] = this.uGrid2.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DistReqSeq"] = this.uGrid2.Rows[i].RowSelectorNumber;
                            row["StdNumber"] = this.uGrid2.Rows[i].Cells["StdNumber"].Value.ToString();
                            row["VersionNum"] = this.uGrid2.Rows[i].Cells["VersionNum"].Value;
                            row["DistDeptCode"] = this.uGrid2.Rows[i].Cells["DistDeptCode"].Value.ToString();
                            row["DistNumber"] = this.uGrid2.Rows[i].Cells["DistNumber"].Value;
                            row["DistVendorCode"] = this.uGrid2.Rows[i].Cells["DistVendorCode"].Value.ToString();
                            row["ReqDesc"] = this.uGrid2.Rows[i].Cells["ReqDesc"].Value.ToString();
                            row["AdmitUserID"] = "";
                            row["AdmitDate"] = "";
                            row["AdmitStatus"] = "AR";

                            dtSaveD.Rows.Add(row);
                        }
                    }


                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistReqH), "ISODocDistReqH");
                    QRPISO.BL.ISODOC.ISODocDistReqH ReqH = new QRPISO.BL.ISODOC.ISODocDistReqH();
                    brwChannel.mfCredentials(ReqH);

                    dtISODocDIstReqH = ReqH.mfSetDataInfo();

                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                    row = dtISODocDIstReqH.NewRow();

                    row["PlantCode"] = m_strPlantCode;
                    row["DistReqUserID"] = this.uTextDistributeRequestUserID.Text;
                    row["DistReqDate"] = this.uDateDistributeRequestDate.Value.ToString();

                    dtISODocDIstReqH.Rows.Add(row);


                    //저장
                    if (dtISODocDIstReqH.Rows.Count > 0)
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                    , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {

                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            
                            //처리로직
                            string rtMSG = ReqH.mfSaveISODocDistReqH(dtISODocDIstReqH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveD);

                            //decoding
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                            //처리로직끝
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            //메시지 박스
                            System.Windows.Forms.DialogResult result;
                            if (ErrRtn.ErrNum == 0)
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000930",
                                                       Infragistics.Win.HAlign.Right);

                                if (this.uGrid2.Rows.Count > 0)
                                {
                                    while (this.uGrid2.Rows.Count > 0)
                                    {
                                        this.uGrid2.Rows[0].Delete(false);
                                    }
                                }
                                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                                {
                                    if (Convert.ToBoolean(uGrid1.Rows[i].Cells["Check"].Value) == true)
                                    {
                                        uGrid1.Rows[i].Cells["Check"].Value = false;
                                    }
                                }
                            }



                            else
                            {
                                result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000953",
                                                        Infragistics.Win.HAlign.Right);
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    //DataTable에 문서가 없는 경우 처리
                    else if (dtISODocDIstReqH.Rows.Count == 0)
                    {
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001135", "M001037", "M000445", Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitValue();
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region Events...
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                e.Cell.Row.Cells["DistDeptCode"].Value = this.uTextHiddenDeptCode.Text;
                e.Cell.Row.Cells["DistDeptName"].Value = this.uTextDept.Text;
                //if (e.Cell.Column.Key == "DistNumber")
                //{
                //    //e.Cell.Value = string.Format("{0:n}", e.Cell.Value.ToString());
                //}
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid2.Rows[i].Hidden = true;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinComboEditor wComboLType = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                DataTable dtLType = new DataTable();
                DataTable dtVersion = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProcess.Items.Clear();
                this.uComboSearchLarge.Items.Clear();
                this.uComboSearchRevisionNum.Items.Clear();

                strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadMASProcessGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(clsLType);
                    dtLType = clsLType.mfReadISODocLTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                    QRPISO.BL.ISODOC.ISODocH clsVersionNum = new QRPISO.BL.ISODOC.ISODocH();
                    dtVersion = clsVersionNum.mfReadISODocHForVersionCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessGroup", "ComboName", dtProcess);
                wCombo.mfSetComboEditor(this.uComboSearchLarge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                       , Infragistics.Win.HAlign.Left, "", "", "선택", "LTypeCode", "LTypeName", dtLType);
                wCombo.mfSetComboEditor(this.uComboSearchRevisionNum, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                       , Infragistics.Win.HAlign.Left, "", "", "전체", "VersionNum", "VersionNum1", dtVersion);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchLarge_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtMType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();

                this.uComboSearchMiddle.Items.Clear();

                if (strLTypeCode != "")
                {
                    //중분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                    QRPISO.BL.ISODOC.ISODocMType clsMType = new QRPISO.BL.ISODOC.ISODocMType();
                    brwChannel.mfCredentials(clsMType);

                    dtMType = clsMType.mfReadISODocMTypeCombo(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMiddle, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "MTypeCode", "MTypeName", dtMType);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchMiddle_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtSType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();

                this.uComboSearchSmall.Items.Clear();

                if (strMTypeCode != "")
                {
                    //소분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
                    QRPISO.BL.ISODOC.ISODocSType clsSType = new QRPISO.BL.ISODOC.ISODocSType();
                    brwChannel.mfCredentials(clsSType);

                    dtSType = clsSType.mfReadISODocSTypeCombo(strPlantCode, strLTypeCode, strMTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchSmall, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "STypeCode", "STypeName", dtSType);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                    {
                        uGrid2.DisplayLayout.Bands[0].AddNew();

                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["PlantCode"].Value = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["VersionNum"].Value = this.uGrid1.Rows[i].Cells["VersionNum"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["AdmitVersionNum"].Value = this.uGrid1.Rows[i].Cells["AdmitVersionNum"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["StdNumber"].Value = this.uGrid1.Rows[i].Cells["StdNumber"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["LTypeCode"].Value = this.uGrid1.Rows[i].Cells["LTypeCode"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["LTypeName"].Value = this.uGrid1.Rows[i].Cells["LTypeName"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["MTypeCode"].Value = this.uGrid1.Rows[i].Cells["MTypeCode"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["MTypeName"].Value = this.uGrid1.Rows[i].Cells["MTypeName"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["STypeCode"].Value = this.uGrid1.Rows[i].Cells["STypeCode"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["STypeName"].Value = this.uGrid1.Rows[i].Cells["STypeName"].Value.ToString();
                        this.uGrid2.Rows[uGrid2.Rows.Count - 1].Cells["DocTitle"].Value = this.uGrid1.Rows[i].Cells["DocTitle"].Value.ToString();
                        //저장시 사용할 공장코드
                        m_strPlantCode = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                    }
                }
                uGrid2.DisplayLayout.Bands[0].AddNew();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "DistVendorName")
                {
                    this.uGrid2.EventManager.AllEventsEnabled = false;
                    frmPOP0004 frmVendor = new frmPOP0004();
                    frmVendor.ShowDialog();
                    this.uGrid2.ActiveRow.Cells["DistVendorCode"].Value = frmVendor.CustomerCode;
                    this.uGrid2.ActiveRow.Cells["DistVendorName"].Value = frmVendor.CustomerName;
                    this.uGrid2.EventManager.AllEventsEnabled = true;
                }
                else if(e.Cell.Column.Key == "DistDeptName")
                {
                    this.uGrid2.EventManager.AllEventsEnabled = false;
                    frmPOP0010 frmDept = new frmPOP0010();
                    frmDept.PlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    frmDept.ShowDialog();

                    this.uGrid2.ActiveRow.Cells["DistDeptCode"].Value = frmDept.DeptCode;
                    this.uGrid2.ActiveRow.Cells["DistDeptName"].Value = frmDept.DeptName;

                    this.uGrid2.EventManager.AllEventsEnabled = true;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISOZ0001_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void uGrid2_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (this.uGrid2.ActiveCell.Column.ToString() == "DistNumber")
                {
                    if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }


        //배포요청자 검색버튼 클릭 이벤트
        private void uTextDistributeRequestUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = m_resSys.GetString("SYS_PLANTCODE");
                frmUser.ShowDialog();

                this.uTextDistributeRequestUserID.Text = frmUser.UserID;
                this.uTextDistributeRequestUserName.Text = frmUser.UserName;
                this.uTextDept.Text = frmUser.DeptName;
                this.uTextHiddenDeptCode.Text = frmUser.DeptCode;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //배포요청자 엔터키 검색 이벤트
        private void uTextDistributeRequestUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uComboSearchPlant.Value.ToString() == "")
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M000202", "M000266",
                                                   Infragistics.Win.HAlign.Right);
                        this.uComboSearchPlant.DropDown();
                        return;
                    }
                    else if(this.uTextDistributeRequestUserID.Text == "")
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M000202", "M000448",
                                                   Infragistics.Win.HAlign.Right);

                        this.uTextDistributeRequestUserID.Focus();

                        return;
                    }
                    else
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        String strPlantCode = this.uComboSearchPlant.Value.ToString();
                        String strUserID = this.uTextDistributeRequestUserID.Text;

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            this.uTextDistributeRequestUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                            this.uTextDept.Text = dtUser.Rows[0]["DeptName"].ToString();
                            this.uTextHiddenDeptCode.Text = dtUser.Rows[0]["DeptCode"].ToString();
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M000202", "M000912",
                                                       Infragistics.Win.HAlign.Right);
                            //사용자가 없을시 텍스트박스 초기화
                            this.uTextDistributeRequestUserID.Text = "";
                            this.uTextDistributeRequestUserName.Text = "";
                            this.uTextDept.Text = "";
                        }
                    }
                }

                if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextDistributeRequestUserName.Text = "";
                    this.uTextDept.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmISOZ0001_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGrid1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                    uGroupBox1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGrid1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    uGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        ///  RowSelectNumber 없애면서 그리드 수정이 불가능 하므로 클릭 이벤트로 체크박스 처리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid1_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("Check"))
                {
                    if (Convert.ToBoolean(e.Cell.Row.Cells["Check"].Value) == false)
                        e.Cell.Row.Cells["Check"].Value = true;
                    else
                        e.Cell.Row.Cells["Check"].Value = false;
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //배포부수 1000단위 자릿수 표시
        private void uGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["DistNumber"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["DistNumber"].MaskInput = "nnn,nnn,nnn";
            }
            catch(Exception ex)
            { }
            finally
            { }
        }
    }
}
