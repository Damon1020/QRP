﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 수입검사규격서 UI                                     */
/* 모듈(분류)명 : 수입검사규격서 POPUP                                  */
/* 프로그램ID   : frmPOP0013.cs                                         */
/* 프로그램명   : 수입검사규격서 상세 POPUP                             */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPISO.UI
{
    public partial class frmPOP0013 : Form
    {
        public frmPOP0013()
        {
            InitializeComponent();
        }
        // 리소소 호출
        QRPGlobal SysRes = new QRPGlobal();

        // 규격서정보 속성
        private string strPlantCode;
        private string strStdNumber;
        private string strStdSeq;
        private string strMaterialCode;
        private DataTable dtSpecD = new DataTable();

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string StdNumber
        {
            get { return strStdNumber; }
            set { strStdNumber = value; }
        }

        public string StdSeq
        {
            get { return strStdSeq; }
            set { strStdSeq = value; }
        }

        public string ProductCode
        {
            get { return strMaterialCode; }
            set { strMaterialCode = value; }
        }

        public DataTable dtRtn
        {
            get { return dtSpecD; }
            set { dtSpecD = value; }
        }

        private void frmPOP0013_Load(object sender, EventArgs e)
        {
            strStdNumber = "";
            strStdSeq = "";

            //초기화 메소드 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
            brw.mfSetFormLanguage(this);

        }

        #region 컨트롤 초기화

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchMaterial, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화


        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dt);

                this.uComboSearchPlant.ReadOnly = true;
                this.uComboSearchPlant.Appearance.BackColor = Color.Gainsboro;
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드


                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "MaterialCode", "자재코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "MaterialName", "자재명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfInitGeneralGrid(this.uGridItem, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumn(this.uGridItem, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false,0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //grd.mfSetGridColumn(this.uGridItem, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridItem, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "LowerSpec", "규격하한", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnn.nnnnn", "0.0");

                grd.mfSetGridColumn(this.uGridItem, 0, "UpperSpec", "규격상한", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnn.nnnnn", "0.0");

                grd.mfSetGridColumn(this.uGridItem, 0, "SpecRange", "범위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "Point", "Point", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly,80,false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridItem, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridItem, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "CompareFlag", "비교여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridItem, 0, "DataType", "데이터유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false,false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "SpecDesc", "규격설명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "MeasureToolCode", "측정기기코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150,false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridItem, 0, "MeasureToolName", "측정기기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridItem.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridItem.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 버튼초기화


        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 조회
        /// </summary>
        private void Search()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            this.Cursor = Cursors.WaitCursor;

            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecH), "MaterialInspectSpecH");
            QRPISO.BL.ISOIMP.MaterialInspectSpecH clsHeader = new QRPISO.BL.ISOIMP.MaterialInspectSpecH();
            brwChannel.mfCredentials(clsHeader);

            string strPlantCode = this.uComboSearchPlant.Value.ToString();
            string strMaterialCode = this.uTextSearchMaterialCode.Text.ToString();

            DataTable dtHeader = clsHeader.mfReadISOMaterialInspectSpecH(strPlantCode, "", "", strMaterialCode,  m_resSys.GetString("SYS_LANG"));

            this.uGridList.DataSource = dtHeader;
            this.uGridList.DataBind();

            this.Cursor = Cursors.Default;

            DialogResult DResult = new DialogResult();
            WinMessageBox msg = new WinMessageBox();
            if (dtHeader.Rows.Count == 0)
                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
        }

        // 검색버튼

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        // 닫기버튼
        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // 확인버튼
        private void uButtonOK_Click(object sender, EventArgs e)
        {
            //strPlantCode = this.uGridList.ActiveRow.Cells["PlantCode"].Text.ToString();
            //strStdNumber = this.uGridList.ActiveRow.Cells["StdNumber"].Text.ToString();
            //this.Close();
            try
            {
                dtSpecD.Columns.Add("InspectGroupCode", typeof(string));
                dtSpecD.Columns.Add("InspectTypeCode", typeof(string));
                dtSpecD.Columns.Add("InspectItemCode", typeof(string));
                dtSpecD.Columns.Add("LowerSpec", typeof(double));
                dtSpecD.Columns.Add("UpperSpec", typeof(double));
                dtSpecD.Columns.Add("SpecRange", typeof(string));
                dtSpecD.Columns.Add("SpecUnitCode", typeof(string));
                dtSpecD.Columns.Add("Point", typeof(int));
                dtSpecD.Columns.Add("SampleSize", typeof(double));
                dtSpecD.Columns.Add("UnitCode", typeof(string));
                dtSpecD.Columns.Add("CompareFlag", typeof(string));
                dtSpecD.Columns.Add("DataType", typeof(string));
                dtSpecD.Columns.Add("EtcDesc", typeof(string));
                dtSpecD.Columns.Add("InspectCondition", typeof(string));
                dtSpecD.Columns.Add("Method", typeof(string));
                dtSpecD.Columns.Add("SpecDesc", typeof(string));
                dtSpecD.Columns.Add("MeasureToolCode", typeof(string));

                DataRow dr;

                for(int i = 0; i < this.uGridItem.Rows.Count; i++)
                {
                    if (this.uGridItem.Rows[i].Cells["Check"].Value.Equals(true))
                    {
                        dr = dtSpecD.NewRow();

                        dr["InspectGroupCode"] = this.uGridItem.Rows[i].Cells["InspectGroupCode"].Value;
                        dr["InspectTypeCode"] = this.uGridItem.Rows[i].Cells["InspectTypeCode"].Value;
                        dr["InspectItemCode"] = this.uGridItem.Rows[i].Cells["InspectItemCode"].Value;
                        dr["LowerSpec"] = this.uGridItem.Rows[i].Cells["LowerSpec"].Value;
                        dr["UpperSpec"] = this.uGridItem.Rows[i].Cells["UpperSpec"].Value;
                        dr["SpecRange"] = this.uGridItem.Rows[i].Cells["SpecRange"].Value;
                        dr["SpecUnitCode"] = this.uGridItem.Rows[i].Cells["SpecUnitCode"].Value;
                        dr["Point"] = this.uGridItem.Rows[i].Cells["Point"].Value;
                        dr["SampleSize"] = this.uGridItem.Rows[i].Cells["SampleSize"].Value;
                        dr["UnitCode"] = this.uGridItem.Rows[i].Cells["UnitCode"].Value;
                        dr["CompareFlag"] = this.uGridItem.Rows[i].Cells["CompareFlag"].Value;
                        dr["DataType"] = this.uGridItem.Rows[i].Cells["DataType"].Value;
                        dr["EtcDesc"] = this.uGridItem.Rows[i].Cells["EtcDesc"].Value;
                        dr["InspectCondition"] = this.uGridItem.Rows[i].Cells["InspectCondition"].Value;
                        dr["Method"] = this.uGridItem.Rows[i].Cells["Method"].Value;
                        dr["SpecDesc"] = this.uGridItem.Rows[i].Cells["SpecDesc"].Value;
                        dr["MeasureToolCode"] = this.uGridItem.Rows[i].Cells["MeasureToolCode"].Value;

                        dtRtn.Rows.Add(dr);
                    }
                }
                dtRtn = dtSpecD.Copy();
                this.Close();

                //dtRtn = ReturnTable.Copy();
                //this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            //strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
            //strStdNumber = e.Row.Cells["StdNumber"].Text.ToString();
            //this.Close();
            try
            {
                if (!e.Row.Cells["StdNumber"].Value.ToString().Equals(string.Empty))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOIMP.MaterialInspectSpecD), "MaterialInspectSpecD");
                    QRPISO.BL.ISOIMP.MaterialInspectSpecD clsSpecD = new QRPISO.BL.ISOIMP.MaterialInspectSpecD();
                    brwChannel.mfCredentials(clsSpecD);
                    strPlantCode = e.Row.Cells["PlantCode"].Text.ToString();
                    string strStdNumber = e.Row.Cells["StdNumber"].Value.ToString().Substring(0, 9);
                    string strStdSeq = e.Row.Cells["StdNumber"].Value.ToString().Substring(9, 4);

                    DataTable dtSpecD = clsSpecD.mfReadISOMaterialInspectSpecD(strPlantCode, strStdNumber, strStdSeq, m_resSys.GetString("SYS_LANG"));

                    this.uGridItem.DataSource = dtSpecD;
                    this.uGridItem.DataBind();
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 검색조건 : 자재팝업창

        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0001 frmPOP = new frmPOP0001();
                frmPOP.ShowDialog();
                this.uTextSearchMaterialCode.Text = frmPOP.MaterialCode;
                this.uTextSearchMaterialName.Text = frmPOP.MaterialName;

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextSearchMaterialCode.Text == "")
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                    else
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboSearchPlant.Value.ToString() == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboSearchPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboSearchPlant.Value.ToString();
                            String strMaterialCode = this.uTextSearchMaterialCode.Text;

                            //제품명 조회 메소드 호출하여 처리결과 정보를 리턴 받는다.
                            DataTable dtMaterial = GetMaterialInfo(strPlantCode, strMaterialCode);

                            if (dtMaterial.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                                {
                                    this.uTextSearchMaterialName.Text = dtMaterial.Rows[i]["MaterialName"].ToString();
                                }
                            }
                            else
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000966",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextSearchMaterialName.Text = "";
                                this.uTextSearchMaterialCode.Text = "";
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextSearchMaterialCode.TextLength <= 1 || this.uTextSearchMaterialCode.Text == this.uTextSearchMaterialCode.SelectedText)
                    {
                        this.uTextSearchMaterialName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        private DataTable GetMaterialInfo(String strPlantCode, String strMaterialCode)
        {
            DataTable dtMaterial = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(clsMaterial);

                dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                return dtMaterial;
            }
            catch (Exception ex)
            {
                return dtMaterial;
            }
            finally
            {
            }
        }
        //Check 컬럼 클릭시 선택 / 해제
        private void uGridItem_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("Check"))
                {
                    if (e.Cell.Value.Equals(true))
                        e.Cell.Value = e.Cell.Value = false;
                    else
                        e.Cell.Value = e.Cell.Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
