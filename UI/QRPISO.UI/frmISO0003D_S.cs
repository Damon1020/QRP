﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서 관리                                         */
/* 프로그램ID   : frmISO0003D.cs                                        */
/* 프로그램명   : 표준문서 조회                                         */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPISO.UI
{
    public partial class frmISO0003D_S : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        private string PlantCode { get; set; }

        public frmISO0003D_S()
        {
            InitializeComponent();
        }

        //대분류코드
        string m_strLTypeCode = string.Empty;

        private void frmISO0003D_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
            // 테스트용, 원래 신규 안됨 //
            //toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true);
        }

        private void frmISO0003D_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 조회", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitValue();
            InitButton();

            // 그룹박스 접은 상태로

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "배포업체", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "적용범위", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.LIST, "결재선", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 버튼 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
                wButton.mfSetButton(this.uButtonDelFile, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDelDept, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDelVendor, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDelAgreeUser, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonDelAgreeUser.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRegDate, "등록일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDiscard, "개정/폐기여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVersionCheck, "폐기문서포함", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNo, "LotNo", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelStandardDocNo, "표준문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "기안자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateDate, "기안일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisSeparation, "개정/폐기구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisUser, "개정/폐기자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisDate, "개정/폐기일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRev_DisReason, "개정/폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChangeItem, "변경항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFrom, "변경 전", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTo, "변경 후", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApproveUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApproveDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDocState, "문서상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDocKeyword, "문서 Keyword", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApplyDate, "적용일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelReturnReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMake, "작성자 / 부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChipSize, "ChipSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPadSize, "PadSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcess, "공정그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGRWComment, "Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // Process ComboBox
                // Bl 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadMASProcessGroup("", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ProcessGroup", "ComboName", dtProcess);


                // 개정/폐기 구분 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dt = clsComCode.mfReadCommonCode("C0010", m_resSys.GetString("SYS_LANG"));

                ////if (dt.Rows.Count > 0)
                ////{
                ////    for (int i = 0; i < dt.Rows.Count; i++)
                ////    {
                ////        if (dt.Rows[i]["ComCode"].ToString().Equals("1") || dt.Rows[i]["ComCode"].ToString().Equals(string.Empty))
                ////        {
                ////            dt.Rows[i]["ComCodeName"] = "적용";
                ////        }
                ////    }
                ////}

                wCombo.mfSetComboEditor(this.uComboSearchDiscard, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);


                wCombo.mfSetComboEditor(this.uComboRev_DisSeparation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid, 0, "LTypeName", "대분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "MTypeName", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "AdmitVersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "VersionNum", "기존개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "SpecView", "Spec View", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 200
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DWFile", "DWFile", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseName", "재 / 개정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseDate", "재 / 개정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseReason", "개정사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DisplayUserName", "재 / 개정자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DisplayDate", "재 / 개정일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DisplayReason", "개정사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 200
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseType", "개정/폐기구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "AdmitStatus", "문서상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteID", "기안자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteName", "기안자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "WriteDate", "기안일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseID", "개정자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "RevDisuseType", "개정 / 폐기여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "DocDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "LTypeCode", "대분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "MTypeCode", "중분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "STypeCode", "소분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                // 첨부파일 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DWFlag", "DW여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 배포업체 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //// 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorCode", "업체코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorName", "업체이름", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "PersonName", "담당자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Hp", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Email", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 적용범위 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "ProcessGroup", "공정그룹", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcessGroup);

                DataTable dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid3, 0, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcessGroup);


                // 적용범위 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid4, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid4, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserID", "담당자명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserName", "담장자 이름", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");



                // Set Font Size
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid4.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid4.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                //// 그리드 내의 콤보박스 처리 ////

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //검색부분 텍스트박스, 콤보박스 초기화
                //////this.uComboSearchPlant.Text = m_resSys.GetString("SYS_PLANTCODE"); // "";
                //////this.uComboSearchLarge.Text = "";
                //////this.uComboSearchMiddle.Text = "";
                //////this.uComboSearchSmall.Text = "";
                //////this.uTextSearchStandardNo.Text = "";
                //////this.uComboSearchProcess.Value = "";
                //////this.uTextSearchTitle.Text = "";
                //////this.uDateSearchRegFromDate.Value = DateTime.Now.AddDays(-7);
                //////this.uDateSearchRegToDate.Value = DateTime.Now;
                //////this.uComboSearchDiscard.Value = "";
                //////this.uTextLotNo.Text = "";

                //확장 그룹박스 부분 텍스트박스, 콤보박스 초기화
                PlantCode = string.Empty;
                this.uTextStandardDocNo.Text = "";
                this.uTextStandardDocNo.MaxLength = 100;
                this.uTextRevisionNo.Text = "";
                this.uTextRevisionNo.MaxLength = 50;
                this.uTextPlant.Text = "";
                this.uTextPlant.MaxLength = 10;
                this.uTextProcess.Text = "";
                this.uTextLarge.Text = "";
                this.uTextLarge.MaxLength = 10;
                this.uTextMiddle.Text = "";
                this.uTextMiddle.MaxLength = 10;
                this.uTextSmall.Text = "";
                this.uTextSmall.MaxLength = 10;
                this.uTextCreateUserID.Text = "";
                this.uTextCreateUserID.MaxLength = 20;
                this.uTextCreateUserName.Text = "";
                this.uTextCreateUserName.MaxLength = 20;
                //this.uTextCreateDate.Text = "";
                //this.uTextCreateDate.MaxLength = 10;
                this.uDateCreateDate.Value = DateTime.Now;
                this.uComboRev_DisSeparation.Value = "";
                this.uTextRev_DisUserID.Text = "";
                this.uTextRev_DisUserID.MaxLength = 20;
                this.uTextRev_DisUserName.Text = "";
                this.uTextRev_DisUserName.MaxLength = 20;
                //this.uTextRev_DisDate.Text = "";
                //this.uTextRev_DisDate.MaxLength = 10;
                this.uDateRev_DisDate.Value = DateTime.Now;
                this.uTextRev_DisReason.Text = "";
                this.uTextRev_DisReason.MaxLength = 2000;
                this.uTextChangeItem.Text = "";
                this.uTextChangeItem.MaxLength = 500;
                this.uTextFrom.Text = "";
                this.uTextFrom.MaxLength = 1000;
                this.uTextTo.Text = "";
                this.uTextTo.MaxLength = 1000;
                this.uTextApproveUserID.Text = "";
                this.uTextApproveUserID.MaxLength = 20;
                this.uTextApproveUserName.Text = "";
                this.uTextApproveUserName.MaxLength = 20;
                this.uTextApproveDate.Text = "";
                this.uTextApproveDate.MaxLength = 10;
                this.uTextDocState.Text = "";
                this.uTextTitle.Text = "";
                this.uTextTitle.MaxLength = 2000;
                this.uTextDocKeyword.Text = "";
                //this.uTextApplyFromDate.Value = DateTime.Now;
                //this.uTextApplyToDate.Value = DateTime.Now.AddDays(30);
                this.uDateApplyDateFrom.Value = DateTime.Now;
                this.uDateApplyDateTo.Value = DateTime.Now.AddMonths(1);
                this.uDateCompleteDate.Value = DateTime.Now.AddDays(30);
                this.uTextEtc.Text = "";
                this.uTextEtc.MaxLength = 100;
                this.uTextGRWComment.Text = "";
                this.uTextGRWComment.MaxLength = 1000;
                this.uTextReturnReason.Text = "";
                this.uTextReturnReason.MaxLength = 100;
                this.uTextPackage.Text = "";
                this.uTextPackage.MaxLength = 40;
                this.uTextChipSize.Text = "";
                this.uTextChipSize.MaxLength = 100;
                this.uTextPadSize.Text = "";
                this.uTextPadSize.MaxLength = 100;
                this.uTextMakeDeptName.Text = "";


                //스크롤바
                this.uTextFrom.Scrollbars = ScrollBars.Vertical;
                this.uTextFrom.SelectionStart = uTextFrom.Text.Length;
                this.uTextFrom.ScrollToCaret();

                this.uTextTo.Scrollbars = ScrollBars.Vertical;
                this.uTextTo.SelectionStart = uTextTo.Text.Length;
                this.uTextTo.ScrollToCaret();


                while (this.uGrid1.Rows.Count > 0)
                {
                    this.uGrid1.Rows[0].Delete(false);
                }
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }
                while (this.uGrid3.Rows.Count > 0)
                {
                    this.uGrid3.Rows[0].Delete(false);
                }
                while (this.uGrid4.Rows.Count > 0)
                {
                    this.uGrid4.Rows[0].Delete(false);
                }
                //공정 히든처리
                this.uTextProcess.Visible = false;
                this.uDateCompleteDate.Visible = false;
                ////this.uLabelDocKeyword.Visible = false;
                ////this.uTextDocKeyword.Visible = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //텍스트박스 및 그리드 작성 가능 상태로 변환
        public void InitValueWritable()
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                this.uTextApproveDate.ReadOnly = false;
                this.uTextApproveDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextApproveUserID.ReadOnly = true;
                this.uTextApproveUserID.Appearance.BackColor = Color.Gainsboro;
                this.uTextApproveUserName.ReadOnly = true;
                this.uTextApproveUserName.Appearance.BackColor = Color.Gainsboro;
                this.uTextChangeItem.ReadOnly = false;
                this.uTextChangeItem.Appearance.BackColor = Color.White;
                this.uTextCreateUserID.ReadOnly = false;
                this.uTextCreateUserID.Appearance.BackColor = Color.White;
                this.uTextCreateUserName.ReadOnly = true;
                this.uTextCreateUserName.Appearance.BackColor = Color.PowderBlue;
                this.uTextDocKeyword.ReadOnly = false;
                this.uTextDocKeyword.Appearance.BackColor = Color.White;
                this.uTextEtc.ReadOnly = false;
                this.uTextEtc.Appearance.BackColor = Color.White;
                this.uTextFrom.ReadOnly = false;
                this.uTextFrom.Appearance.BackColor = Color.White;
                this.uTextTo.ReadOnly = false;
                this.uTextTo.Appearance.BackColor = Color.White;
                this.uTextTitle.ReadOnly = true;
                this.uTextTitle.Appearance.BackColor = Color.Gainsboro;
                this.uTextReturnReason.ReadOnly = false;
                this.uTextReturnReason.Appearance.BackColor = Color.White;
                //this.uTextRev_DisDate.ReadOnly = true;
                //this.uTextRev_DisDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateRev_DisDate.ReadOnly = false;
                this.uDateRev_DisDate.Appearance.BackColor = Color.White;
                this.uTextRev_DisReason.ReadOnly = false;
                this.uTextRev_DisReason.Appearance.BackColor = Color.White;
                this.uComboRev_DisSeparation.ReadOnly = true;
                this.uComboRev_DisSeparation.Appearance.BackColor = Color.Gainsboro;
                this.uTextRev_DisUserID.ReadOnly = false;
                this.uTextRev_DisUserID.Appearance.BackColor = Color.White;
                this.uTextRev_DisUserName.ReadOnly = true;
                this.uTextRev_DisUserName.Appearance.BackColor = Color.PowderBlue;
                this.uTextPackage.ReadOnly = false;
                this.uTextPackage.Appearance.BackColor = Color.White;
                this.uTextPadSize.ReadOnly = false;
                this.uTextPadSize.Appearance.BackColor = Color.White;
                this.uTextChipSize.ReadOnly = false;
                this.uTextChipSize.Appearance.BackColor = Color.White;
                this.uTextGRWComment.ReadOnly = false;
                this.uTextGRWComment.Appearance.BackColor = Color.White;
                this.uTextMakeUserName.ReadOnly = false;
                this.uTextMakeUserName.Appearance.BackColor = Color.White;
                this.uTextMakeDeptName.ReadOnly = false;
                this.uTextMakeDeptName.Appearance.BackColor = Color.White;

                //this.uButtonDelAgreeUser.Visible = true;
                this.uButtonDelDept.Visible = true;
                this.uButtonDelFile.Visible = true;
                this.uButtonDelVendor.Visible = true;

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid1.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid1.Rows[i].Cells["FileTitle"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid1.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid2.Rows[i].Cells["VendorCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid2.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid2.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                {
                    this.uGrid3.Rows[i].Cells["DeptCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid3.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
                for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                {
                    this.uGrid4.Rows[i].Cells["AgreeUserID"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    this.uGrid4.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }

                this.uGrid1.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGrid2.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGrid3.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                this.uGrid4.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;

                //// 빈줄추가
                wGrid.mfAddRowGrid(this.uGrid1, 0);
                wGrid.mfAddRowGrid(this.uGrid2, 0);
                wGrid.mfAddRowGrid(this.uGrid3, 0);
                wGrid.mfAddRowGrid(this.uGrid4, 0);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //작성 불가 상태
        public void initValueDiswritable()
        {
            try
            {
                WinGrid wGrid = new WinGrid();

                this.uGrid1.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGrid2.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGrid3.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                this.uGrid4.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                this.uTextApproveDate.ReadOnly = true;
                this.uTextApproveDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextApproveUserID.ReadOnly = true;
                this.uTextApproveUserID.Appearance.BackColor = Color.Gainsboro;
                this.uTextApproveUserName.ReadOnly = true;
                this.uTextApproveUserName.Appearance.BackColor = Color.Gainsboro;
                this.uTextChangeItem.ReadOnly = true;
                this.uTextChangeItem.Appearance.BackColor = Color.Gainsboro;
                this.uTextCreateUserID.ReadOnly = true;
                this.uTextCreateUserID.Appearance.BackColor = Color.Gainsboro;
                this.uTextCreateUserName.ReadOnly = true;
                this.uTextCreateUserName.Appearance.BackColor = Color.Gainsboro;
                this.uTextDocKeyword.ReadOnly = true;
                this.uTextDocKeyword.Appearance.BackColor = Color.Gainsboro;
                this.uTextEtc.ReadOnly = true;
                this.uTextEtc.Appearance.BackColor = Color.Gainsboro;
                this.uTextFrom.ReadOnly = true;
                this.uTextFrom.Appearance.BackColor = Color.Gainsboro;
                this.uTextTo.ReadOnly = true;
                this.uTextTo.Appearance.BackColor = Color.Gainsboro;
                this.uTextTitle.ReadOnly = true;
                this.uTextTitle.Appearance.BackColor = Color.Gainsboro;
                this.uTextReturnReason.ReadOnly = true;
                this.uTextReturnReason.Appearance.BackColor = Color.Gainsboro;
                //this.uTextRev_DisDate.ReadOnly = true;
                //this.uTextRev_DisDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateRev_DisDate.ReadOnly = true;
                this.uDateRev_DisDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextRev_DisReason.ReadOnly = true;
                this.uTextRev_DisReason.Appearance.BackColor = Color.Gainsboro;
                this.uComboRev_DisSeparation.ReadOnly = true;
                this.uComboRev_DisSeparation.Appearance.BackColor = Color.Gainsboro;
                this.uTextRev_DisUserID.ReadOnly = true;
                this.uTextRev_DisUserID.Appearance.BackColor = Color.Gainsboro;
                this.uTextRev_DisUserName.ReadOnly = true;
                this.uTextRev_DisUserName.Appearance.BackColor = Color.Gainsboro;
                this.uTextPackage.ReadOnly = true;
                this.uTextPackage.Appearance.BackColor = Color.Gainsboro;
                this.uTextPadSize.ReadOnly = true;
                this.uTextPadSize.Appearance.BackColor = Color.Gainsboro;
                this.uTextChipSize.ReadOnly = true;
                this.uTextChipSize.Appearance.BackColor = Color.Gainsboro;

                //this.uButtonDelAgreeUser.Visible = false;
                //this.uButtonDelDept.Visible = false;
                //this.uButtonDelFile.Visible = false;
                //this.uButtonDelVendor.Visible = false;

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    this.uGrid1.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid1.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid1.Rows[i].Cells["FileTitle"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid1.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid2.Rows[i].Cells["VendorCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid2.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid2.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                {
                    this.uGrid3.Rows[i].Cells["DeptCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid3.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
                for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                {
                    this.uGrid4.Rows[i].Cells["AgreeUserID"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    this.uGrid4.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar 메소드

        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                InitValue();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(isoDocH);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();
                String strSTypCode = this.uComboSearchSmall.Value.ToString();
                String strStdNumber = this.uTextSearchStandardNo.Text;
                String strProcessGroup = this.uComboSearchProcess.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;
                String strApplyDateFrom = Convert.ToDateTime(this.uDateSearchRegFromDate.Value).ToString("yyyy-MM-dd");
                String strApplyDateTo = Convert.ToDateTime(this.uDateSearchRegToDate.Value).ToString("yyyy-MM-dd");
                String strAdmitStatus = this.uComboSearchDiscard.Value.ToString();
                String strVersionCheck = "";
                if (this.uCheckVersion.Checked == true)
                {
                    strVersionCheck = "T";
                }
                else
                {
                    strVersionCheck = "F";
                }

                //호출  
                DataTable dt = isoDocH.mfReadISODocHForSearch(strPlantCode, strLTypeCode, strMTypeCode, strSTypCode, strStdNumber, strProcessGroup, strDocTitle, strVersionCheck
                                                                , strApplyDateFrom, strApplyDateTo, strAdmitStatus, m_resSys.GetString("SYS_LANG"));

                this.uGrid.DataSource = dt;
                this.uGrid.DataBind();

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid, 0);
                }

                if (uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();
            try
            {
                if (this.uComboRev_DisSeparation.Value.ToString().Equals("2"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                , "M001264", "M001053", "M001192", Infragistics.Win.HAlign.Right);

                    return;
                }
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("/")
                                || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("#")
                                || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("*")
                                || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("<")
                                || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(">"))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                        return;
                    }
                }

                DataTable dtISODocH = new DataTable();
                DataTable dtSaveVendor = new DataTable();
                DataTable dtDelVendor = new DataTable();
                DataTable dtSaveDept = new DataTable();
                DataTable dtDelDept = new DataTable();
                DataTable dtSaveAgreeUser = new DataTable();
                DataTable dtDelAgreeUser = new DataTable();
                DataTable dtSaveFile = new DataTable();
                DataTable dtDelFile = new DataTable();
                DataRow row;


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(isoDocH);

                //FileServer 연결
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                brwChannel.mfCredentials(clsSysFilePath);
                DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                    , Infragistics.Win.HAlign.Right);
                if (DResult == DialogResult.Yes)
                {
                    dtISODocH = isoDocH.mfSetDateInfo();

                    row = dtISODocH.NewRow();
                    row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                    row["StdNumber"] = this.uTextStandardDocNo.Text;
                    row["VersionNum"] = this.uTextRevisionNo.Text;
                    row["AdmitVersionNum"] = this.uTextAdmitVersionNum.Text;
                    row["ProcessCode"] = this.uGrid.ActiveRow.Cells["ProcessCode"].Value.ToString();
                    row["LTypeCode"] = this.uGrid.ActiveRow.Cells["LTypeCode"].Value.ToString();
                    row["MTypeCode"] = this.uGrid.ActiveRow.Cells["MTypeCode"].Value.ToString();
                    row["STypeCode"] = this.uGrid.ActiveRow.Cells["STypeCode"].Value.ToString();
                    row["MakeUserName"] = this.uTextMakeUserName.Text;
                    row["MakeDeptName"] = this.uTextMakeDeptName.Text;
                    row["WriteID"] = this.uTextCreateUserID.Text;
                    //row["WriteDate"] = this.uTextCreateDate.Text;
                    if (this.uDateCreateDate.Value != null)
                    {
                        row["WriteDate"] = this.uDateCreateDate.Value.ToString();
                    }
                    row["AdmitID"] = this.uTextApproveUserID.Text;
                    row["AdmitDate"] = this.uTextApproveDate.Text;
                    row["RevDisuseType"] = this.uComboRev_DisSeparation.Value.ToString();
                    row["RevDisuseID"] = this.uTextRev_DisUserID.Text;
                    if (this.uComboRev_DisSeparation.Value.ToString() == "1")
                    {
                        //row["RevDisuseDate"] = this.uTextRev_DisDate.Text;
                        if (this.uDateRev_DisDate.Value != null)
                        {
                            row["RevDisuseDate"] = this.uDateRev_DisDate.Value.ToString();
                        }
                        row["RevDisuseReason"] = this.uTextRev_DisReason.Text;
                    }
                    else if (this.uComboRev_DisSeparation.Value.ToString() == "2")
                    {
                        //row["DisuseDate"] = this.uTextRev_DisDate.Text;
                        if (this.uDateRev_DisDate.Value != null)
                        {
                            row["DisuseDate"] = this.uDateRev_DisDate.Value.ToString();
                        }
                        row["DisuseReason"] = this.uTextRev_DisReason.Text;
                    }
                    row["ChangeItem"] = this.uTextChangeItem.Text;
                    row["FromDesc"] = this.uTextFrom.Text;
                    row["ToDesc"] = this.uTextTo.Text;
                    row["DocTitle"] = this.uTextTitle.Text;
                    row["DocKeyword"] = this.uTextDocKeyword.Text;
                    row["DocDesc"] = this.uTextEtc.Text;
                    //row["ApplyDateFrom"] = this.uTextApplyFromDate.Text;
                    //row["ApplyDateTo"] = this.uTextApplyToDate.Text;
                    if (this.uDateApplyDateFrom.Value != null)
                    {
                        row["ApplyDateFrom"] = this.uDateApplyDateFrom.Value.ToString();
                    }
                    if (this.uDateApplyDateTo.Visible == false && this.uDateApplyDateTo.Value != null)
                    {
                        row["ApplyDateTo"] = this.uDateApplyDateTo.Value.ToString();
                    }
                    else
                    {
                        row["ApplyDateTo"] = "";
                    }
                    //row["CompleteDate"] = this.uDateCompleteDate.Value.ToString();
                    row["ReturnReason"] = this.uTextReturnReason.Text;
                    row["Package"] = this.uTextPackage.Text;
                    row["ChipSize"] = this.uTextChipSize.Text;
                    row["PadSize"] = this.uTextPadSize.Text;
                    row["AdmitStatus"] = "FN";


                    //if (DResult == DialogResult.Yes)
                    //{
                    //    row["AdmitStatus"] = "AR";
                    //}

                    //else if (DResult == DialogResult.No)
                    //{
                    //    row["AdmitStatus"] = "WR";
                    //}
                    //else
                    //{
                    //    return;
                    //}

                    dtISODocH.Rows.Add(row);


                    //////Vendor//////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                    QRPISO.BL.ISODOC.ISODocVendor Vendor = new QRPISO.BL.ISODOC.ISODocVendor();
                    brwChannel.mfCredentials(Vendor);

                    dtSaveVendor = Vendor.mfSetDateInfo();
                    dtDelVendor = Vendor.mfSetDateInfo();

                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                        if (this.uGrid2.Rows[i].Hidden == false)
                        {
                            row = dtSaveVendor.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["Seq"] = Convert.ToInt32(this.uGrid2.Rows[i].Cells["Seq"].Value);
                            row["VendorCode"] = this.uGrid2.Rows[i].Cells["VendorCode"].Value.ToString();
                            row["DeptName"] = this.uGrid2.Rows[i].Cells["DeptName"].Value.ToString();
                            row["PersonName"] = this.uGrid2.Rows[i].Cells["PersonName"].Value.ToString(); ;
                            row["Hp"] = this.uGrid2.Rows[i].Cells["Hp"].Value.ToString();
                            row["Email"] = this.uGrid2.Rows[i].Cells["Email"].Value.ToString();
                            row["EtcDesc"] = this.uGrid2.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveVendor.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelVendor.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["Seq"] = Convert.ToInt32(this.uGrid2.Rows[i].Cells["Seq"].Value);

                            dtDelVendor.Rows.Add(row);
                        }
                    }



                    /////AgreeUser//////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                    QRPISO.BL.ISODOC.ISODocAgreeUser AgreeUser = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                    brwChannel.mfCredentials(AgreeUser);

                    dtSaveAgreeUser = AgreeUser.mfSetDateInfo();
                    dtDelAgreeUser = AgreeUser.mfSetDateInfo();

                    for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                    {
                        this.uGrid4.ActiveCell = this.uGrid4.Rows[0].Cells[0];
                        if (this.uGrid4.Rows[i].Hidden == false)
                        {
                            row = dtSaveAgreeUser.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["AgreeUserID"] = this.uGrid4.Rows[i].Cells["AgreeUserID"].Value.ToString();
                            row["EtcDesc"] = this.uGrid4.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveAgreeUser.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelAgreeUser.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["AgreeUserID"] = uGrid4.Rows[i].Cells["AgreeUserID"].Value.ToString();

                            dtDelAgreeUser.Rows.Add(row);
                        }
                    }


                    ///////Dept////////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                    QRPISO.BL.ISODOC.ISODocDept Dept = new QRPISO.BL.ISODOC.ISODocDept();
                    brwChannel.mfCredentials(Dept);

                    dtSaveDept = Dept.mfSetDateInfo();
                    dtDelDept = Dept.mfSetDateInfo();

                    for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                    {
                        this.uGrid3.ActiveCell = this.uGrid3.Rows[0].Cells[0];
                        if (this.uGrid3.Rows[i].Hidden == false)
                        {
                            row = dtSaveDept.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["ProcessGroup"] = this.uGrid3.Rows[i].Cells["ProcessGroup"].Value.ToString();
                            row["EtcDesc"] = this.uGrid3.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveDept.Rows.Add(row);

                        }
                        else
                        {
                            row = dtDelDept.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["ProcessGroup"] = this.uGrid3.Rows[i].Cells["ProcessGroup"].Value.ToString();
                            dtDelDept.Rows.Add(row);
                        }
                    }


                    /////////////////File//////////////////////////
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                    QRPISO.BL.ISODOC.ISODocFile DFile = new QRPISO.BL.ISODOC.ISODocFile();
                    brwChannel.mfCredentials(DFile);

                    //파일명 일괄적으로 변경
                    String[] strFile = new String[this.uGrid1.Rows.Count];
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty) && this.uGrid1.Rows[i].Hidden == false)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037"
                                , this.uGrid1.Rows[i].RowSelectorNumber + "M000530", Infragistics.Win.HAlign.Right);

                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["FileName"];
                            return;
                        }
                        else
                        {
                            if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                            {
                                FileInfo fileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                strFile[i] = fileDoc.Name;

                            }
                            else
                            {
                                if (!this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty))
                                {
                                    String fName = this.uGrid1.Rows[i].Cells["FileName"].Value.ToString();
                                    String[] splitName = fName.Split(new char[] { '-' });

                                    if (splitName.Length > 5)
                                    {
                                        for (int j = 4; j < splitName.Length; j++)
                                        {
                                            if (!j.Equals(splitName.Length - 1))
                                                strFile[i] = strFile[i] + splitName[j] + "-";
                                            else
                                                strFile[i] = strFile[i] + splitName[j];
                                        }
                                    }
                                    else
                                    {
                                        strFile[i] = splitName[4];
                                    }
                                }
                            }
                        }
                    }

                    //Seq 생성
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (this.uGrid1.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                        {
                            if (this.uGrid1.Rows[i].Hidden == false)
                            {
                                if (this.uGrid1.Rows[i].RowSelectorNumber.Equals(1))
                                {
                                    this.uGrid1.Rows[i].Cells["Seq"].Value = "1";
                                }
                                else
                                {
                                    this.uGrid1.Rows[i].Cells["Seq"].Value = Convert.ToString(Convert.ToInt32(this.uGrid1.Rows[i - 1].Cells["Seq"].Value.ToString()) + 1);
                                }
                            }
                        }
                    }

                    dtSaveFile = DFile.mfSetDatainfo();
                    dtDelFile = DFile.mfSetDatainfo();

                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {

                        this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];
                        if (this.uGrid1.Rows[i].Hidden == false)
                        {
                            row = dtSaveFile.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["Seq"] = this.uGrid1.Rows[i].Cells["Seq"].Value;
                            row["FileTitle"] = this.uGrid1.Rows[i].Cells["FileTitle"].Value.ToString();
                            row["FileName"] = strFile[i];
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                            {
                                row["DWFlag"] = "T";
                            }
                            else
                            {
                                row["DWFlag"] = "F";
                            }
                            row["EtcDesc"] = this.uGrid1.Rows[i].Cells["EtcDesc"].Value.ToString();

                            dtSaveFile.Rows.Add(row);
                        }
                        else
                        {
                            row = dtDelFile.NewRow();
                            row["PlantCode"] = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                            row["StdNumber"] = this.uTextStandardDocNo.Text;
                            row["VersionNum"] = this.uTextRevisionNo.Text;
                            row["Seq"] = Convert.ToInt32(this.uGrid1.Rows[i].Cells["Seq"].Value);
                            dtDelFile.Rows.Add(row);
                        }

                    }
                    //고객사 표준이 아닐경우 첨부파일 그리드를 전부 삭제했을경우 저장 불가
                    if (dtSaveFile.Rows.Count == 0 && DResult == DialogResult.Yes && m_strLTypeCode != "Cust_Spec")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M001143", Infragistics.Win.HAlign.Center);

                        return;
                    }

                    String versionNum = this.uTextRevisionNo.Text;
                    String FolderName = this.uTextStandardDocNo.Text + "-" + Convert.ToString(versionNum);

                    //프로그레스바
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;
                    //메시지 박스
                    System.Windows.Forms.DialogResult result;

                    //처리로직
                    //저장 함수 호출
                    string rtMSG = isoDocH.mfSaveISODocHForDisuse(dtISODocH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveVendor, dtDelVendor
                                                                   , dtSaveAgreeUser, dtDelAgreeUser, dtSaveDept, dtDelDept, dtSaveFile, dtDelFile);

                    //decoding
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                    //처리로직끝
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum == 0)
                    {
                        //File UpLoad
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        //파일 이름 변경
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (this.uGrid1.Rows[i].Hidden == false)
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                                {
                                    FileInfo fileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                    string strUploadFile = fileDoc.DirectoryName + "\\" + this.uTextStandardDocNo.Text + "-"
                                                         + this.uTextRevisionNo.Text
                                                         + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-"
                                                         + this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString() + "-" + fileDoc.Name;


                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);

                                    File.Copy(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString(), strUploadFile);
                                    arrFile.Add(strUploadFile);
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                   dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + FolderName,
                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        if (arrFile.Count != 0)
                        {
                            fileAtt.ShowDialog();
                        }

                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001135", "M001037", "M000930",
                                               Infragistics.Win.HAlign.Right);
                        mfSearch();
                        InitValue();

                        this.uGroupBoxContentsArea.Expanded = false;
                    }



                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                    }

                }


                //else if (this.uTextDocState.Text == "승인요청" || uTextDocState.Text == "승인완료")
                //{
                //    if (this.uTextApproveUserID.Text == "")
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                            , "확인창", "필수입력사항 확인", "승인자 ID를 입력해주세요", Infragistics.Win.HAlign.Center);

                //        //Focus
                //        this.uTextApproveUserID.Focus();
                //        return;
                //    }
                //    else
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                            , "확인창", "필수입력사항 확인", "승인완료 / 승인요청중인 문서는 저장이 불가능합니다", Infragistics.Win.HAlign.Center);
                //        return;
                //    }
                //}
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화

                else
                {
                    InitValue();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                //처리로직
                WinGrid grd = new WinGrid();
                //엑셀저장함수호출
                grd.mfDownLoadGridToExcel(this.uGrid);
            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        // ContentsGroupBox 상태변화 이벤트

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 720;

                    for (int i = 0; i < uGrid.Rows.Count; i++)
                    {
                        uGrid.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        //공장 콤보박스 변경시, 프로세스, 대분류 콤보박스 상태변경
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinComboEditor wComboLType = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                DataTable dtLType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProcess.Items.Clear();
                this.uComboSearchLarge.Items.Clear();

                strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadMASProcessGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(clsLType);

                    dtLType = clsLType.mfReadISODocLTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessGroup", "ComboName", dtProcess);
                wCombo.mfSetComboEditor(this.uComboSearchLarge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                       , Infragistics.Win.HAlign.Left, "", "", "선택", "LTypeCode", "LTypeName", dtLType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //대분류 콤보박스 변경시 중분류 콤보박스 변경
        private void uComboSearchLarge_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtMType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();

                this.uComboSearchMiddle.Items.Clear();

                if (strLTypeCode != "")
                {
                    //중분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                    QRPISO.BL.ISODOC.ISODocMType clsMType = new QRPISO.BL.ISODOC.ISODocMType();
                    brwChannel.mfCredentials(clsMType);

                    dtMType = clsMType.mfReadISODocMTypeCombo(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMiddle, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "MTypeCode", "MTypeName", dtMType);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //중분류 변경시 소분류 박스 변경
        private void uComboSearchMiddle_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtSType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();

                this.uComboSearchSmall.Items.Clear();

                if (strMTypeCode != "")
                {
                    //소분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
                    QRPISO.BL.ISODOC.ISODocSType clsSType = new QRPISO.BL.ISODOC.ISODocSType();
                    brwChannel.mfCredentials(clsSType);

                    dtSType = clsSType.mfReadISODocSTypeCombo(strPlantCode, strLTypeCode, strMTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchSmall, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "STypeCode", "STypeName", dtSType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void uGrid1_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "All files (*.*)|*.*";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    e.Cell.Value = strImageFile;

                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                frmPOP0020 frmVendor = new frmPOP0020();
                frmVendor.PlantCode = PlantCode;
                frmVendor.ShowDialog();
                this.uGrid2.ActiveRow.Cells["VendorCode"].Value = frmVendor.VendorCode;
                this.uGrid2.ActiveRow.Cells["VendorName"].Value = frmVendor.VendorName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uGrid4_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M001204",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uComboRev_DisSeparation.ReadOnly == false)
                {
                    frmPOP0011 frmAUser = new frmPOP0011();
                    frmAUser.PlantCode = PlantCode;
                    frmAUser.ShowDialog();

                    this.uGrid4.ActiveRow.Cells["AgreeUserID"].Value = frmAUser.UserID;
                    this.uGrid4.ActiveRow.Cells["AgreeUserName"].Value = frmAUser.UserName;
                }
                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uTextCreateUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M001204",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uTextCreateUserID.ReadOnly == false)
                {
                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = PlantCode;
                    frmUser.ShowDialog();

                    this.uTextCreateUserID.Text = frmUser.UserID;
                    this.uTextCreateUserName.Text = frmUser.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextApproveUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M001204",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                if (this.uTextApproveUserID.ReadOnly == false)
                {
                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = PlantCode;
                    frmUser.ShowDialog();

                    this.uTextApproveUserID.Text = frmUser.UserID;
                    this.uTextApproveUserName.Text = frmUser.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextRev_DisUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M001204",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                if (this.uTextRev_DisUserID.ReadOnly == false)
                {
                    frmPOP0011 frmUser = new frmPOP0011();
                    frmUser.PlantCode = PlantCode;
                    frmUser.ShowDialog();

                    this.uTextRev_DisUserID.Text = frmUser.UserID;
                    this.uTextRev_DisUserName.Text = frmUser.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }



        private void uTextCreateUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextCreateUserID.Text == "")
                    {
                        this.uTextCreateUserID.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();


                        String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                        String strWriteID = this.uTextCreateUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextCreateUserID.Text = "";
                            this.uTextCreateUserName.Text = "";
                        }
                        else
                        {
                            this.uTextCreateUserName.Text = strRtnUserName;
                        }

                    }
                }
                else if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
                {
                    this.uTextCreateUserID.Text = "";
                    this.uTextCreateUserName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextRev_DisUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextRev_DisUserID.Text == "")
                    {
                        this.uTextRev_DisUserID.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();


                        String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                        String strWriteID = this.uTextRev_DisUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextRev_DisUserID.Text = "";
                            this.uTextRev_DisUserName.Text = "";
                        }
                        else
                        {
                            this.uTextRev_DisUserName.Text = strRtnUserName;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextApproveUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextApproveUserID.Text == "")
                    {
                        this.uTextApproveUserID.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();


                        String strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                        String strWriteID = this.uTextApproveUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextApproveUserID.Text = "";
                            this.uTextApproveUserName.Text = "";
                        }
                        else
                        {
                            this.uTextApproveUserName.Text = strRtnUserName;
                        }

                    }
                }
                else if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                {
                    this.uTextApproveUserID.Text = "";
                    this.uTextApproveUserName.Text = "";
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //private void uButtonDelFile_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if(this.uTextEtc.ReadOnly == false)
        //        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
        //        {
        //            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
        //            {
        //                this.uGrid1.Rows[i].Hidden = true;
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {
        //    }
        //}

        //private void uButtonDelVendor_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (this.uTextEtc.ReadOnly == false)
        //        {
        //            for (int i = 0; i < this.uGrid2.Rows.Count; i++)
        //            {
        //                if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
        //                {
        //                    this.uGrid2.Rows[i].Hidden = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { }
        //}

        //private void uButtonDelDept_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (this.uTextEtc.ReadOnly == false)
        //        {
        //            for (int i = 0; i < this.uGrid3.Rows.Count; i++)
        //            {
        //                if (Convert.ToBoolean(this.uGrid3.Rows[i].Cells["Check"].Value) == true)
        //                {
        //                    this.uGrid3.Rows[i].Hidden = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { }
        //}

        //private void uButtonDelAgreeUser_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (this.uTextEtc.ReadOnly == false)
        //        {
        //            for (int i = 0; i < this.uGrid4.Rows.Count; i++)
        //            {
        //                if (Convert.ToBoolean(this.uGrid4.Rows[i].Cells["Check"].Value) == true)
        //                {
        //                    this.uGrid4.Rows[i].Hidden = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    { }
        //}

        private void uGrid1_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key == "DWFlag")
                {

                    if (Convert.ToBoolean(e.Cell.Value) == true)
                    {
                        e.Cell.Value = false;
                    }
                    else
                    {
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                            {
                                this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                                e.Cell.Value = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid1.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\")
                            || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001147",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //저장된 폴더 경로 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                        //저장된 하위 폴더값
                        String FolderName = this.uTextStandardDocNo.Text + "-" + this.uTextAdmitVersionNum.Text;

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                                {
                                    arrFile.Add(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + FolderName,
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0003D_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }



        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.Cell.Column.ToString() == "FileName")
                {
                    if (e.Cell.ToString() == "" || e.Cell.ToString().Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000356",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {

                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();


                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        arrFile.Add(e.Cell.Value.ToString());
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (this.uTextStandardDocNo.Text + "-" + this.uTextAdmitVersionNum.Text),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        fileAtt.ShowDialog();
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("VendorCode") || e.Cell.Column.Key.Equals("VendorName"))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();

                    //VendorP 검색 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    DataTable dtVendorP = new DataTable();

                    dtVendorP = clsVendor.mfReadVendorPCombo(this.uGrid2.ActiveRow.Cells["VendorCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    WinGrid wGrid = new WinGrid();

                    e.Cell.Row.Cells["PersonName"].Column.Layout.ValueLists.Clear();
                    String strDropDownValue = "Seq,PersonName,DeptName,Hp,EMail,EtcDesc";
                    String strDropDownText = "순번,담당자,부서,전화번호,E-Mail,비고";

                    wGrid.mfSetGridCellValueGridList(this.uGrid2, 0, e.Cell.Row.Index, "PersonName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                        , strDropDownValue, strDropDownText, "Seq", "PersonName", dtVendorP);
                    if (dtVendorP.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                           , "M001264", "M000367", "M001249", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 배포업체 담당자 선택 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid2_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                String strKey = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                String strValue = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);

                String strVendorCode = e.Cell.Row.Cells["VendorCode"].Value.ToString();
                int intSeq = Convert.ToInt32(strKey);

                DataTable dtVendorP = clsVendor.mfReadVendorPForGrid(strVendorCode, intSeq);

                e.Cell.Row.Cells["PersonName"].Value = dtVendorP.Rows[0]["PersonName"].ToString();
                e.Cell.Row.Cells["DeptName"].Value = dtVendorP.Rows[0]["DeptName"].ToString();
                e.Cell.Row.Cells["Hp"].Value = dtVendorP.Rows[0]["Hp"].ToString();
                e.Cell.Row.Cells["Email"].Value = dtVendorP.Rows[0]["Email"].ToString();
                e.Cell.Row.Cells["EtcDesc"].Value = dtVendorP.Rows[0]["EtcDesc"].ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmISO0003D_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더그리드 DW파일 셀 더블클릭시 다운로드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //if (e.Cell.Column.Key == "DWFile")
                if (e.Cell.Column.Key == "SpecView")
                {
                    if (e.Cell.Value.ToString().Equals(""))
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default, "M001135"
                                                    , "M001185", "M000358"
                                                    , Infragistics.Win.HAlign.Right);
                        return;
                    }
                    //else if (e.Cell.Row.Cells["RevDisuseType"].Value.ToString().Equals("2"))
                    //{
                    //    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                    //                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default, "처리결과"
                    //                                , "파일 검색결과", "폐기된 문서의 DW첨부파일은 삭제처리합니다"
                    //                                , Infragistics.Win.HAlign.Right);
                    //    return;
                    //}
                    else
                    {
                        String strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                        String strStdNumber = e.Cell.Row.Cells["StdNumber"].Value.ToString();
                        String strVersionNum = e.Cell.Row.Cells["AdmitVersionNum"].Value.ToString();
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0006");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();


                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        //arrFile.Add(e.Cell.Value.ToString());
                        arrFile.Add(e.Cell.Row.Cells["DWFile"].Value.ToString());
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (strStdNumber + "-" + strVersionNum),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        //FileInfo fileDoc = new FileInfo(e.Cell.Value.ToString());
                        FileInfo fileDoc = new FileInfo(e.Cell.Row.Cells["DWFile"].Value.ToString());

                        fileAtt.ShowDialog();
                        //System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Row.Cells["DWFile"].Value.ToString());
                    }
                }
                else
                {
                    InitValue();
                    //e.Row.Fixed = true;
                    e.Cell.Row.Fixed = true;

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPopup = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    WinLabel wLabel = new WinLabel();

                    //대분류코드 저장
                    m_strLTypeCode = e.Cell.Row.Cells["LTypeCode"].Value.ToString();

                    //함수호출
                    QRPBrowser brwChannel = new QRPBrowser();

                    //Header
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                    QRPISO.BL.ISODOC.ISODocH header = new QRPISO.BL.ISODOC.ISODocH();
                    brwChannel.mfCredentials(header);
                    //AgreeUser
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                    QRPISO.BL.ISODOC.ISODocAgreeUser agreeUser = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                    brwChannel.mfCredentials(agreeUser);
                    //Dept
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                    QRPISO.BL.ISODOC.ISODocDept dept = new QRPISO.BL.ISODOC.ISODocDept();
                    brwChannel.mfCredentials(dept);
                    //Vendor
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                    QRPISO.BL.ISODOC.ISODocVendor vendor = new QRPISO.BL.ISODOC.ISODocVendor();
                    brwChannel.mfCredentials(vendor);
                    //File
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                    QRPISO.BL.ISODOC.ISODocFile file = new QRPISO.BL.ISODOC.ISODocFile();
                    brwChannel.mfCredentials(file);


                    //헤더용
                    String strVersionNum = e.Cell.Row.Cells["VersionNum"].Value.ToString();
                    //공통인수
                    String strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    String strStdNumber = e.Cell.Row.Cells["StdNumber"].Value.ToString();

                    PlantCode = strPlantCode;
                    //header
                    DataTable dtHeader = header.mfReadISODocHForReWrite(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));


                    this.uTextStandardDocNo.Text = dtHeader.Rows[0]["StdNumber"].ToString();
                    this.uTextPlant.Text = dtHeader.Rows[0]["PlantName"].ToString();
                    this.uTextRevisionNo.Text = dtHeader.Rows[0]["VersionNum"].ToString();
                    this.uTextAdmitVersionNum.Text = dtHeader.Rows[0]["AdmitVersionNum"].ToString();
                    this.uTextLarge.Text = dtHeader.Rows[0]["LTypeName"].ToString();
                    this.uTextMiddle.Text = dtHeader.Rows[0]["MTypeName"].ToString();
                    this.uTextSmall.Text = dtHeader.Rows[0]["STypeName"].ToString();
                    this.uTextMakeUserName.Text = dtHeader.Rows[0]["MakeUserName"].ToString();

                    //this.uTextCreateDate.Text = dtHeader.Rows[0]["WriteDate"].ToString();
                    this.uDateCreateDate.Value = dtHeader.Rows[0]["WriteDate"].ToString();
                    //this.uComboRev_DisSeparation.Value = dtHeader.Rows[0]["RevDisuseType"].ToString();

                    if (dtHeader.Rows[0]["RevDisuseType"].ToString().Equals("2"))
                    {
                        wLabel.mfSetLabel(this.uLabelCreateUser, "폐기자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                        this.uDateRev_DisDate.Value = dtHeader.Rows[0]["DisuseDate"].ToString();
                        this.uTextRev_DisReason.Text = dtHeader.Rows[0]["DisuseReason"].ToString();
                        this.uTextCreateUserID.Visible = false;
                        this.uTextCreateUserName.Visible = false;
                        this.uTextRev_DisUserID.Text = dtHeader.Rows[0]["RevDisuseID"].ToString();
                        this.uTextRev_DisUserName.Text = dtHeader.Rows[0]["RevDisuseName"].ToString();
                        this.uTextMakeDeptName.Text = dtHeader.Rows[0]["RevDisuseUserDempName"].ToString();
                        this.uTextRev_DisUserID.Visible = true;
                        this.uTextRev_DisUserName.Visible = true;
                        this.uComboRev_DisSeparation.Value = dtHeader.Rows[0]["RevDisuseType"].ToString();
                    }
                    else
                    {
                        wLabel.mfSetLabel(this.uLabelCreateUser, "기안자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                        this.uDateRev_DisDate.Value = dtHeader.Rows[0]["RevDisuseDate"].ToString();
                        this.uTextRev_DisReason.Text = dtHeader.Rows[0]["RevDisuseReason"].ToString();
                        this.uTextCreateUserID.Text = dtHeader.Rows[0]["WriteID"].ToString();
                        this.uTextCreateUserName.Text = dtHeader.Rows[0]["WriteName"].ToString();
                        this.uTextMakeDeptName.Text = dtHeader.Rows[0]["MakeDeptName"].ToString();
                        this.uTextRev_DisUserID.Visible = false;
                        this.uTextRev_DisUserName.Visible = false;
                        this.uTextCreateUserID.Visible = true;
                        this.uTextCreateUserName.Visible = true;
                        this.uComboRev_DisSeparation.Value = "1";
                    }


                    this.uTextChangeItem.Text = dtHeader.Rows[0]["ChangeItem"].ToString();
                    this.uTextFrom.Text = dtHeader.Rows[0]["FromDesc"].ToString();
                    this.uTextTo.Text = dtHeader.Rows[0]["ToDesc"].ToString();
                    this.uTextApproveUserID.Text = dtHeader.Rows[0]["AdmitID"].ToString();
                    this.uTextApproveUserName.Text = dtHeader.Rows[0]["AdmitName"].ToString();
                    this.uTextDocState.Text = dtHeader.Rows[0]["AdmitStatus"].ToString();
                    this.uTextTitle.Text = dtHeader.Rows[0]["DocTitle"].ToString();
                    this.uTextDocKeyword.Text = dtHeader.Rows[0]["DocKeyword"].ToString();
                    //this.uTextApplyFromDate.Text = dtHeader.Rows[0]["ApplyDateFrom"].ToString();
                    //this.uTextApplyToDate.Text = dtHeader.Rows[0]["ApplyDateTo"].ToString();
                    this.uDateApplyDateFrom.Value = dtHeader.Rows[0]["ApplyDateFrom"].ToString();
                    this.uDateApplyDateTo.Value = dtHeader.Rows[0]["ApplyDateTo"].ToString();
                    this.uDateCompleteDate.Value = dtHeader.Rows[0]["CompleteDate"].ToString();
                    this.uTextEtc.Text = dtHeader.Rows[0]["DocDesc"].ToString();
                    this.uTextGRWComment.Text = dtHeader.Rows[0]["GRWComment"].ToString();
                    this.uTextPackage.Text = dtHeader.Rows[0]["Package"].ToString();
                    this.uTextChipSize.Text = dtHeader.Rows[0]["ChipSize"].ToString();
                    this.uTextPadSize.Text = dtHeader.Rows[0]["PadSize"].ToString();
                    this.uTextApproveDate.Text = dtHeader.Rows[0]["AdmitDate"].ToString();

                    WinGrid grd = new WinGrid();

                    ////if (this.uComboRev_DisSeparation.Value.ToString().Equals("2"))
                    ////{
                    ////    this.uGroupBoxContentsArea.Expanded = true;
                    ////    initValueDiswritable();
                    ////}
                    ////else
                    ////{
                    ////    this.uGroupBoxContentsArea.Expanded = true;
                    ////    InitValueWritable();
                    ////}

                    this.uGroupBoxContentsArea.Expanded = true;
                    initValueDiswritable();

                    //AgreeUser Start//
                    DataTable dtAgree = agreeUser.mfReadISODocAgreeUser(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    this.uGrid4.DataSource = dtAgree;
                    this.uGrid4.DataBind();
                    if (dtAgree.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGrid4, 0);
                    //AGreeUser End//

                    //DeptStart//
                    DataTable dtDept = dept.mfReadISODocDept(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    this.uGrid3.DataSource = dtDept;
                    this.uGrid3.DataBind();
                    if (dtDept.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGrid3, 0);
                    //Dept End//

                    //Vendor Start//
                    DataTable dtVendor = vendor.mfReadISODocVendor(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    this.uGrid2.DataSource = dtVendor;
                    this.uGrid2.DataBind();
                    if (dtVendor.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                    //Vendor End//

                    //File Start//
                    DataTable dtFile = file.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    this.uGrid1.DataSource = dtFile;
                    this.uGrid1.DataBind();
                    if (dtFile.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGrid1, 0);

                    for (int i = 0; i < dtFile.Rows.Count; i++)
                    {
                        if (dtFile.Rows[i]["DWFlag"].ToString() == "T")
                        {
                            this.uGrid1.Rows[i].Cells["DWFlag"].Value = true;
                        }
                        else
                        {
                            this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                        }
                    }
                    //File End//

                    //대분류 임시표준일 경우 적용일자To 표시
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (dtHeader.Rows[0]["LTypeCode"].ToString().Equals("ECN"))
                    {
                        this.uLabelApplyDate.Visible = true;
                        this.uDateApplyDateTo.Visible = true;
                        this.uDateApplyDateFrom.Visible = true;
                        this.ultraLabel3.Visible = true;
                    }
                    //임시표준이 아닐경우 적용일자From만 표시
                    else
                    {
                        this.uLabelApplyDate.Visible = false;
                        this.uDateApplyDateTo.Visible = false;
                        this.uDateApplyDateFrom.Visible = false;
                        this.ultraLabel3.Visible = false;
                    }
                    //고객사 표준의 경우 Package, ChipSize, PadSize 보임
                    if (dtHeader.Rows[0]["LTypeCode"].ToString().Equals("Cust_Spec"))
                    {
                        this.uTextPackage.Visible = true;
                        this.uTextChipSize.Visible = true;
                        this.uTextPadSize.Visible = true;

                        this.uLabelPackage.Visible = true;
                        this.uLabelChipSize.Visible = true;
                        this.uLabelPadSize.Visible = true;
                    }
                    //고객사 표준이 아닌경우 Hidden
                    else
                    {
                        this.uTextPadSize.Visible = false;
                        this.uTextPackage.Visible = false;
                        this.uTextChipSize.Visible = false;

                        this.uTextChipSize.Text = "";
                        this.uTextPadSize.Text = "";
                        this.uTextPackage.Text = "";

                        this.uLabelChipSize.Visible = false;
                        this.uLabelPadSize.Visible = false;
                        this.uLabelPackage.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #region 삭제버튼 이벤트
        private void uButtonDelFile_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid1.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonDelDept_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid3.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid3.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonDelVendor_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid2.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

    }
}
