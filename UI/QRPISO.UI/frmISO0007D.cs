﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 공정검사규격서                                        */
/* 프로그램ID   : frmISO0007D.cs                                        */
/* 프로그램명   : 공정검사 규격서 조회                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-09                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPISO.UI
{
    public partial class frmISO0007D : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmISO0007D()
        {
            InitializeComponent();
        }

        private void frmISO0007D_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISO0007D_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("공정검사규격서 등록/조회", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitGrid();
            InitTab();
            InitLabel();
            InitButton();
            InitComboBox();
            InitTextBox();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// TextBox 초기화
        /// </summary>
        private void InitTextBox()
        {
            try
            {
                // TextBox 최대 입력길이 조절                            
                this.uTextWriteID.MaxLength = 20;
                this.uTextEtcDesc.MaxLength = 100;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Tab 초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTabControl wTab = new WinTabControl();

                wTab.mfInitGeneralTabControl(this.uTab, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
                    , Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None
                    , m_resSys.GetString("SYS_FONTNAME"));
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);                
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "작성자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCreateDate, "작성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEtc1, "비고1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc2, "비고2", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc3, "비고3", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc4, "비고4", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonCopy, "복사", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // DB로부터 데이터 가져오는 Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // ComboBox 설정 메소드
                // SearchArea PlantComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // ContentsArea PlantComboBox
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // Package 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                // DB로부터 데이터 가져오는 Method
                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                // 검색조건 : Package 콤보박스                
                wCombo.mfSetComboEditor(this.uComboSearchPacakge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Package", "ComboName", dtPackage);

                // 상세정보 : Package 콤보박스
                wCombo.mfSetComboEditor(this.uComboPackage, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Package", "ComboName", dtPackage);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 공정검사 규격서 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPRCHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "WriteID", "작성자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "WriteDate", "작성일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCHeader, 0, "MDMTFlag", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 상세정보 그리드
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridPRCDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");
                
                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "####", "0");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "ProcessCode1", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "ProcessSeq", "공정순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "####", "0");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "InspectGroupCode", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "InspectTypeCode", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "InspectItemCode", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "Stack", "Stack", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nn", "1");

                //wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "StackSeq", "Stack", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "StackSeq", "Stack", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "Generation", "세대", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "ProcessInspectFlag", "공정검사", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");
                                                               
                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");             

                //wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "LowerSpec", "규격하한", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnn.nnnnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "LowerSpec", "규격하한", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn.nnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "UpperSpec", "규격상한", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn.nnn", "0.0");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "SpecRange", "범위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "SampleSize", "측정Point", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");  //samplesize

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "ProcessInspectSS", "순회검사 S/S", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn.nnn", "0");  //공정 S/S

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "ProductItemSS", "CCS_생산 S/S", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");  //생산Item S/S

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "QualityItemSS", "CCS_품질 S/S", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");  //품질Item S/S

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "UnitCode", "단위명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "InspectPeriod", "검사주기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "PeriodUnitCode", "주기단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "CompareFlag", "비교여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "AQLFlag", "AQL여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "DataType", "데이터 유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "SpecDesc", "규격설명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "MeasureToolCode", "측정기기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                // Set Font Size
                this.uGridPRCHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPRCHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridPRCDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPRCDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridPRCDetail, 0);

                // Grid DropDown Column 설정
                DataTable dt = new DataTable();
                // 단위
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);
                dt = clsUnit.mfReadMASUnitCombo();
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "SpecUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                // 데이터 유형
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);
                dt = clsComCode.mfReadCommonCode("C0002", m_resSys.GetString("SYS_LANG"));
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "DataType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "DataType", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                // 검사주기단위
                dt = clsComCode.mfReadCommonCode("C0005", m_resSys.GetString("SYS_LANG"));
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "PeriodUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "PeriodUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                // 범위
                dt = clsComCode.mfReadCommonCode("C0032", m_resSys.GetString("SYS_LANG"));
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "SpecRange", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                dt = new DataTable();
                wGrid.mfSetGridColumnValueGridList(this.uGridPRCDetail, 0, "ProcessCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "ProcessCode,ProcessName", "공정코드,공정명"
                                                    , "ProcessCode", "ProcessName", dt);
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectItemCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                ////wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "StackSeq", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "Generation", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectItemCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);
                mfSetGridColumnValueList(this.uGridPRCDetail, 0, "Generation", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dt);

                //// 중복방지를 위한 바인딩
                //DataTable dtDetail = new DataTable();
                //dtDetail.Columns.Add("Seq", typeof(Int32));
                //dtDetail.Columns.Add("ProcessCode", typeof(String));
                //dtDetail.Columns.Add("ProcessSeq", typeof(Int32));
                //dtDetail.Columns.Add("InspectGroupCode", typeof(String));
                //dtDetail.Columns.Add("InspectTypeCode", typeof(String));
                //dtDetail.Columns.Add("InspectItemCode", typeof(String));
                //dtDetail.Columns.Add("StackSeq", typeof(String));
                //dtDetail.Columns.Add("Generation", typeof(String));
                //dtDetail.Columns.Add("ProcessInspectFlag", typeof(String));
                //dtDetail.Columns.Add("ProductItemFlag", typeof(String));
                //dtDetail.Columns.Add("QualityItemFlag", typeof(String));
                //dtDetail.Columns.Add("InspectCondition", typeof(String));
                //dtDetail.Columns.Add("Method", typeof(String));
                //dtDetail.Columns.Add("SpecDesc", typeof(String));
                //dtDetail.Columns.Add("MeasureToolCode", typeof(String));
                //dtDetail.Columns.Add("UpperSpec", typeof(Double));
                //dtDetail.Columns.Add("LowerSpec", typeof(Double));
                //dtDetail.Columns.Add("SpecRange", typeof(String));
                //dtDetail.Columns.Add("SampleSize", typeof(Double));
                //dtDetail.Columns.Add("ProcessInspectSS", typeof(Double));
                //dtDetail.Columns.Add("ProductItemSS", typeof(Double));
                //dtDetail.Columns.Add("QualityItemSS", typeof(Double));
                //dtDetail.Columns.Add("UnitCode", typeof(String));
                //dtDetail.Columns.Add("InspectPeriod", typeof(String));
                //dtDetail.Columns.Add("PeriodUnitCode", typeof(String));
                //dtDetail.Columns.Add("CompareFlag", typeof(String));
                //dtDetail.Columns.Add("DataType", typeof(String));
                //dtDetail.Columns.Add("EtcDesc", typeof(String));

                //DataColumn[] dc = new DataColumn[4];
                //dc[0] = dtDetail.Columns["ProcessCode"];
                //dc[1] = dtDetail.Columns["InspectItemCode"];
                //dc[2] = dtDetail.Columns["StackSeq"];
                //dc[3] = dtDetail.Columns["Generation"];

                //dtDetail.PrimaryKey = dc;

                //this.uGridPRCDetail.DataSource = dtDetail;
                //this.uGridPRCDetail.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                // 검색조건 변수

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strPackage = this.uComboSearchPacakge.Value.ToString();
                string strCustomerCode = this.uTextSearchCustomerCode.Text;

                // ProgressPopup 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                brwChannel.mfCredentials(clsHeader);

                // 검색 Method 호출
                DataTable dtHeader = clsHeader.mfReadISOProcessInspectSpecH(strPlantCode, strPackage, strCustomerCode, m_resSys.GetString("SYS_LANG"));

                // Binding
                this.uGridPRCHeader.DataSource = dtHeader;
                this.uGridPRCHeader.DataBind();

                // ContentsArea 접은 상태로

                this.uGroupBoxContentsArea.Expanded = false;

                // 팝업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 결과 확인
                if (dtHeader.Rows.Count == 0)
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridPRCHeader, 0);

                    for (int i = 0; i < this.uGridPRCHeader.Rows.Count; i++)
                    {
                        if (!this.uGridPRCHeader.Rows[i].GetCellValue("MDMTFlag").ToString().Equals("T"))
                            this.uGridPRCHeader.Rows[i].Appearance.BackColor = Color.Salmon;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // Instance 객체 생성
                DialogResult Result = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                QRPBrowser brwChannel = new QRPBrowser();
                DataRow drRow;

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001014", "M001016", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    // 헤더 필수사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        //공장선택
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uComboPackage.Value.ToString() == "")
                    {
                        //Package을 선택해 주세요
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000097", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uComboPackage.DropDown();
                        return;
                    }
                    else if (this.uTextWriteID.Text == "")
                    {
                        //등록자를 입력해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001014", "M000386", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextWriteID.Focus();
                        return;
                    }
                    else
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        // 상세정보를 저장하기 위한 DataTable 컬럼설정
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecD), "ProcessInspectSpecD");
                        QRPISO.BL.ISOPRC.ProcessInspectSpecD clsDetail = new QRPISO.BL.ISOPRC.ProcessInspectSpecD();
                        brwChannel.mfCredentials(clsDetail);

                        DataTable dtDetail = clsDetail.mfSetDataInfo();

                        if (this.uGridPRCDetail.Rows.Count > 0)
                        {
                            this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[0].Cells[0];
                            string strLang = m_resSys.GetString("SYS_LANG");

                            // 상세정보 필수입력사항 확인
                            for (int i = 0; i < this.uGridPRCDetail.Rows.Count; i++)
                            {
                                if (this.uGridPRCDetail.Rows[i].Hidden == false)
                                {
                                    if (this.uGridPRCDetail.Rows[i].Cells["ProcessCode"].Value.ToString() == "")
                                    {
                                        //번째 열의 공정을 선택해 주세요
                                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000482", strLang), Infragistics.Win.HAlign.Right);

                                        // Focus
                                        this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[i].Cells["ProcessCode"];
                                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                    else if (this.uGridPRCDetail.Rows[i].Cells["InspectGroupCode"].Value.ToString() == "")
                                    {
                                        //번째 열의 검사분류를 선택해 주세요
                                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000477", strLang), Infragistics.Win.HAlign.Right);

                                        // Focus
                                        this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[i].Cells["InspectGroupCode"];
                                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                    else if (this.uGridPRCDetail.Rows[i].Cells["InspectTypeCode"].Value.ToString() == "")
                                    {
                                        //번째 열의 검사유형을 선택해 주세요
                                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000478", strLang), Infragistics.Win.HAlign.Right);

                                        // Focus
                                        this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[i].Cells["InspectTypeCode"];
                                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                    else if (this.uGridPRCDetail.Rows[i].Cells["InspectItemCode"].Value.ToString() == "")
                                    {
                                        //"번째 열의 검사항목을 선택해 주세요"
                                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000479", strLang), Infragistics.Win.HAlign.Right);

                                        // Focus
                                        this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[i].Cells["InspectItemCode"];
                                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                        return;
                                    }
                                    else if (this.uGridPRCDetail.Rows[i].Cells["SampleSize"].Value.ToString().Equals(string.Empty) ||
                                             this.uGridPRCDetail.Rows[i].Cells["SampleSize"].Value.ToString().Equals("0"))
                                    {
                                        Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001014", strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M001418",strLang), Infragistics.Win.HAlign.Right);

                                        // Focus
                                        this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[i].Cells["SampleSize"];
                                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }
                                    //else if (this.uGridPRCDetail.Rows[i].Cells["StackSeq"].Value.ToString() == "")
                                    //{
                                    //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    //                    , "확인창", "저장 필수 입력사항 확인", (i + 1).ToString() + "번째 열의 Stack을 선택해 주세요", Infragistics.Win.HAlign.Right);

                                    //    // Focus
                                    //    this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[i].Cells["StackSeq"];
                                    //    this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    //    return;
                                    //}
                                    //else if (this.uGridPRCDetail.Rows[i].Cells["Generation"].Value.ToString() == "")
                                    //{
                                    //    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    //                    , "확인창", "저장 필수 입력사항 확인", (i + 1).ToString() + "번째 열의 세대를 선택해 주세요", Infragistics.Win.HAlign.Right);

                                    //    // Focus
                                    //    this.uGridPRCDetail.ActiveCell = this.uGridPRCDetail.Rows[i].Cells["Generation"];
                                    //    this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    //    return;
                                    //}
                                    else
                                    {
                                        // 상세정보 저장
                                        drRow = dtDetail.NewRow();
                                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                                        drRow["VersionNum"] = 1;
                                        drRow["ItemNum"] = this.uGridPRCDetail.Rows[i].RowSelectorNumber;
                                        drRow["Seq"] = this.uGridPRCDetail.Rows[i].RowSelectorNumber;
                                        drRow["ProcessCode"] = this.uGridPRCDetail.Rows[i].Cells["ProcessCode1"].Value.ToString();
                                        drRow["ProcessSeq"] = this.uGridPRCDetail.Rows[i].RowSelectorNumber;
                                        drRow["InspectGroupCode"] = this.uGridPRCDetail.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                                        drRow["InspectTypeCode"] = this.uGridPRCDetail.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                                        drRow["InspectItemCode"] = this.uGridPRCDetail.Rows[i].Cells["InspectItemCode"].Value.ToString();
                                        drRow["StackSeq"] = this.uGridPRCDetail.Rows[i].Cells["StackSeq"].Value.ToString();
                                        drRow["Generation"] = this.uGridPRCDetail.Rows[i].Cells["Generation"].Value.ToString();
                                        if (Convert.ToBoolean(this.uGridPRCDetail.Rows[i].Cells["ProcessInspectFlag"].Value) == true)
                                            drRow["ProcessInspectFlag"] = "T";
                                        else
                                            drRow["ProcessInspectFlag"] = "F";
                                        if (Convert.ToBoolean(this.uGridPRCDetail.Rows[i].Cells["ProductItemFlag"].Value) == true)
                                            drRow["ProductItemFlag"] = "T";
                                        else
                                            drRow["ProductItemFlag"] = "F";
                                        if (Convert.ToBoolean(this.uGridPRCDetail.Rows[i].Cells["QualityItemFlag"].Value) == true)
                                            drRow["QualityItemFlag"] = "T";
                                        else
                                            drRow["QualityItemFlag"] = "F";
                                        drRow["InspectCondition"] = this.uGridPRCDetail.Rows[i].Cells["InspectCondition"].Value.ToString();
                                        drRow["Method"] = this.uGridPRCDetail.Rows[i].Cells["Method"].Value.ToString();
                                        drRow["SpecDesc"] = this.uGridPRCDetail.Rows[i].Cells["SpecDesc"].Value.ToString();
                                        drRow["MeasureToolCode"] = this.uGridPRCDetail.Rows[i].Cells["MeasureToolCode"].Value.ToString();
                                        drRow["UpperSpec"] = ReturnDecimalValue(this.uGridPRCDetail.Rows[i].Cells["UpperSpec"].Value.ToString());
                                        drRow["LowerSpec"] = ReturnDecimalValue(this.uGridPRCDetail.Rows[i].Cells["LowerSpec"].Value.ToString());
                                        drRow["SpecRange"] = this.uGridPRCDetail.Rows[i].Cells["SpecRange"].Value.ToString();
                                        drRow["SpecUnitCode"] = this.uGridPRCDetail.Rows[i].Cells["SpecUnitCode"].Value.ToString();
                                        drRow["SampleSize"] = ReturnDecimalValue(this.uGridPRCDetail.Rows[i].Cells["SampleSize"].Value.ToString());
                                        drRow["ProcessInspectSS"] = ReturnDecimalValue(this.uGridPRCDetail.Rows[i].Cells["ProcessInspectSS"].Value.ToString());
                                        drRow["ProductItemSS"] = ReturnDecimalValue(this.uGridPRCDetail.Rows[i].Cells["ProductItemSS"].Value.ToString());
                                        drRow["QualityItemSS"] = ReturnDecimalValue(this.uGridPRCDetail.Rows[i].Cells["QualityItemSS"].Value.ToString());
                                        drRow["UnitCode"] = this.uGridPRCDetail.Rows[i].Cells["UnitCode"].Value.ToString();
                                        drRow["InspectPeriod"] = this.uGridPRCDetail.Rows[i].Cells["InspectPeriod"].Value.ToString();
                                        drRow["PeriodUnitCode"] = this.uGridPRCDetail.Rows[i].Cells["PeriodUnitCode"].Value.ToString();

                                        if (Convert.ToBoolean(this.uGridPRCDetail.Rows[i].Cells["CompareFlag"].Value) == true)
                                            drRow["CompareFlag"] = "T";
                                        else
                                            drRow["CompareFlag"] = "F";
                                        drRow["DataType"] = this.uGridPRCDetail.Rows[i].Cells["DataType"].Value.ToString();
                                        drRow["EtcDesc"] = this.uGridPRCDetail.Rows[i].Cells["EtcDesc"].Value.ToString();
                                        drRow["AQLFlag"] = this.uGridPRCDetail.Rows[i].Cells["AQLFlag"].Value.ToString().ToUpper().Substring(0, 1);

                                        string strUpper = string.Format("{0:0n}", drRow["UpperSpec"].ToString());
                                        string strLower = string.Format("{0:0n}", drRow["LowerSpec"].ToString());
                                        //////if (drRow["DataType"].ToString() == "1")
                                        //////{
                                        //////    if (strUpper.Equals("0") && strLower.Equals("0"))
                                        //////    {
                                        //////        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        //////            , "필수 확인", "저장 필수 확인", "최소값과 최대값중 최소한 하나는 입력해야 합니다.", Infragistics.Win.HAlign.Right);

                                        //////        return;
                                        //////    }
                                        //////}
                                        dtDetail.Rows.Add(drRow);
                                    }
                                }
                            }
                        }
                        // 저장할 상세정보가 있는경우만 저장수행
                        if (dtDetail.Rows.Count > 0)
                        {
                            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                            QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                            brwChannel.mfCredentials(clsHeader);

                            DataTable dtHeader = clsHeader.mfSetDataInfo();
                            // 헤더정보 저장

                            drRow = dtHeader.NewRow();
                            drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                            if (this.uTextStdNumber.Text == "")
                            {
                                // 표준번호가 없을경우 공백
                                drRow["StdNumber"] = "";
                                drRow["StdSeq"] = "";
                            }
                            else
                            {
                                // 표준번호가 존재하는경우 표준번호와 순번을 나누어 저장
                                String strFullNumber = this.uTextStdNumber.Text;
                                drRow["StdNumber"] = strFullNumber.Substring(0, 9);
                                drRow["StdSeq"] = strFullNumber.Substring(9, 4);
                            }
                            drRow["VersionNum"] = 1;
                            drRow["Package"] = this.uComboPackage.Value.ToString();
                            drRow["CustomerCode"] = this.uTextCustomerCode.Text;
                            drRow["WriteID"] = this.uTextWriteID.Text;
                            drRow["WriteDate"] = this.uDateWriteDate.Value;
                            drRow["EtcDesc"] = this.uTextEtcDesc.Text;
                            drRow["EtcDesc1"] = this.RichTextEtc1.GetDocumentText();
                            drRow["EtcDesc2"] = this.RichTextEtc2.GetDocumentText();
                            drRow["EtcDesc3"] = this.RichTextEtc3.GetDocumentText();
                            drRow["EtcDesc4"] = this.RichTextEtc4.GetDocumentText();

                            ////string strSendMDM = m_resSys.GetString("SYS_SERVERPATH") == null ? "" : m_resSys.GetString("SYS_SERVERPATH");

                            //////if (strSendMDM.Contains("10.60.60.91") || strSendMDM.Contains("10.60.60.92"))
                            ////drRow["MDM"] = "T";
                            //////else
                            //////    drRow["MDM"] = "F";

                            dtHeader.Rows.Add(drRow);

                            // 저장여부를 묻는다
                            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                            {
                                // 프로그래스 팝업창 생성
                                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                Thread t1 = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                // 삭제를 위한 공장코드 변수
                                String strPlantCode = this.uComboPlant.Value.ToString();

                                // 저장 Method 호출
                                String strErrRtn = clsHeader.mfSaveISOProcessInspectSpecH(dtHeader, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDetail, strPlantCode);

                                // 팦업창 Close
                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);
                                System.Windows.Forms.DialogResult result;

                                // 결과 검사
                                TransErrRtn ErrRtn = new TransErrRtn();
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum == 0)
                                {
                                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                 "M001135", "M001037", "M000930", Infragistics.Win.HAlign.Right);

                                    // List 갱신
                                    mfSearch();
                                }
                                else
                                {
                                    string strMes = "";
                                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                                        strMes = msg.GetMessge_Text("M000953", m_resSys.GetString("SYS_LANG"));
                                    else
                                        strMes = ErrRtn.ErrMessage;

                                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                 msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG")),
                                                                 strMes, Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M000652", "M001054", Infragistics.Win.HAlign.Right);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    //삭제 필수사항을 입력해주세요
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M000628", "M000630", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    if (this.uTextStdNumber.Text == "")
                    {
                        //그리드에서 삭제할 표준번호를 선택해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000335", Infragistics.Win.HAlign.Center);

                        this.uGroupBoxContentsArea.Expanded = false;
                        return;
                    }
                    else if (this.uComboPlant.Value.ToString() == "")
                    {
                        //공장을 선택해주세요
                        Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        this.uComboPlant.DropDown();
                        return;
                    }
                    else
                    {
                        //선택한 정보를 삭제하겠습니까?
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000650", "M000675", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                            QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                            brwChannel.mfCredentials(clsHeader);

                            // 매개변수 설정(공장코드, 표준번호, 표준번호순번)
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strFullStdNumber = this.uTextStdNumber.Text;
                            String strStdNumber = strFullStdNumber.Substring(0, 9);
                            String strStdSeq = strFullStdNumber.Substring(9, 4);

                            // Progress Popup 
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            // Method 호출
                            String rtMSG = clsHeader.mfDeleteISOProcessInspectSpecH(strPlantCode, strStdNumber, strStdSeq);

                            // Decoding //
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                            // 처리로직 끝 //

                            // Progress Popup Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            // 삭제성공여부
                            if (ErrRtn.ErrNum == 0)
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000638", "M000926",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                string strMes = "";
                                if (ErrRtn.ErrMessage.Equals(string.Empty))
                                    strMes = msg.GetMessge_Text("M000925", m_resSys.GetString("SYS_LANG"));
                                else
                                    strMes = ErrRtn.ErrMessage;

                                //입력한 정보를 성공적으로 삭제하지못했습니다.
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M000638", m_resSys.GetString("SYS_LANG")),
                                                    strMes,
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    // ContentsArea 접힌상태를 펼침상태로 변경

                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    // 이미 펼친 상태이면 초기화
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridPRCHeader.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridPRCHeader);

                    if (this.uGridPRCDetail.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridPRCDetail);
                    }
                }
                else
                {
                    //공정검사규격서 그리드에 데이터가 없습니다.
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000288",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Event...
        // ContentsGroupBox 상태변화 이벤트

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridPRCHeader.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridPRCHeader.Height = 720;

                    for (int i = 0; i < uGridPRCHeader.Rows.Count; i++)
                    {
                        uGridPRCHeader.Rows[i].Fixed = false;
                    }

                    // ContentsGroupBox 내의 컨트롤 초기화
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        // 셀 수정시 RowSelector 이미지 변경 이벤트
        private void uGridPRCDetail_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제버튼 이벤트
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridPRCDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridPRCDetail.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridPRCDetail.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        // 사용자 팝업창 생성
        private void uTextWriteID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266.",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.PlantCode = uComboPlant.Value.ToString();

                frmPOP.ShowDialog();

                this.uTextWriteID.Text = frmPOP.UserID;
                this.uTextWriteName.Text = frmPOP.UserName;
                this.uComboPlant.Value = frmPOP.PlantCode;

                Boolean bolCheck = CheckStdNumber();
                if (bolCheck == false)
                {
                    this.uTextStdNumber.Text = "";
                    this.uTextEtcDesc.Text = "";
                    while (this.uGridPRCDetail.Rows.Count > 0)
                    {
                        this.uGridPRCDetail.Rows[0].Delete(false);
                    }
                    this.RichTextEtc1.Clear();
                    this.RichTextEtc2.Clear();
                    this.RichTextEtc3.Clear();
                    this.RichTextEtc4.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 공장선택이 안되었을때 상세 그리드에 드랍다운 컬럼 클릭하면 메세지창 띄워주는 이벤트
        private void uGridPRCDetail_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (e.Cell.Column.Key == "ProcessCode" || e.Cell.Column.Key == "InspectGroupCode")
                {
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        //공장을 선택해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uComboPlant.DropDown();
                    }
                }
                else if (e.Cell.Column.Key == "InspectTypeCode")
                {
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        //공장을 선택해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uComboPlant.DropDown();
                    }
                    else if (e.Cell.Row.Cells["InspectGroupCode"].Value.ToString() == "")
                    {
                        //검사분류를 선택해 주세요
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000261", "M000178", Infragistics.Win.HAlign.Right);

                        this.uGridPRCDetail.ActiveCell = e.Cell.Row.Cells["InspectGroupCode"];
                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                }
                else if (e.Cell.Column.Key == "InspectItemCode")
                {
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        //공장을 선택해 주세요.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M000261", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uComboPlant.DropDown();
                    }
                    else if (e.Cell.Row.Cells["InspectGroupCode"].Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001222", "M000178", Infragistics.Win.HAlign.Right);

                        this.uGridPRCDetail.ActiveCell = e.Cell.Row.Cells["InspectGroupCode"];
                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                    else if (e.Cell.Row.Cells["InspectTypeCode"].Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001222", "M000184", Infragistics.Win.HAlign.Right);

                        this.uGridPRCDetail.ActiveCell = e.Cell.Row.Cells["InspectTypeCode"];
                        this.uGridPRCDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 헤더 그리드 더블클릭 이벤트
        private void uGridPRCHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // ProgressPopup 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 현재행 고정
                e.Row.Fixed = true;

                // 검색조건 변수
                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strFullStdNumber = e.Row.Cells["StdNumber"].Value.ToString();

                // Header 상세 정보 조회 Method 호출
                Search_HeaderD(strPlantCode, strFullStdNumber);

                // 상세정보 그리드 조회 Method 호출
                Search_Detail(strPlantCode, strFullStdNumber);

                // ContentsArea 펼침 상태로
                this.uGroupBoxContentsArea.Expanded = true;

                // PK 편집불가 상태로
                this.uComboPlant.ReadOnly = true;

                // 팝업창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 등록자 텍스트 박스에서 ID입력후 엔터키 입력시 이벤트
        private void uTextWriteID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextWriteID.Text == "")
                    {
                        this.uTextWriteName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();

                        if (this.uComboPlant.Value.ToString() == "")
                        {
                            //공장선택
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000266",
                                            Infragistics.Win.HAlign.Right);

                            this.uComboPlant.DropDown();
                        }
                        else
                        {
                            String strPlantCode = this.uComboPlant.Value.ToString();
                            String strWriteID = this.uTextWriteID.Text;

                            // UserName 검색 함수 호출
                            String strRtnUserName = GetUserName(strPlantCode, strWriteID);

                            if (strRtnUserName == "")
                            {
                                //사용자를 찾을 수 없습니다
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000621",
                                            Infragistics.Win.HAlign.Right);

                                this.uTextWriteName.Text = "";
                                this.uTextWriteID.Text = "";
                            }
                            else
                            {
                                this.uTextWriteName.Text = strRtnUserName;
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.Back)
                {
                    if (this.uTextWriteID.TextLength <= 1 || this.uTextWriteID.Text == this.uTextWriteID.SelectedText)
                    {
                        this.uTextWriteName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method...
        /// <summary>
        /// Header 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strFullStdNumber"> 표준번호(표준번호+표준번호순번) </param>
        private void Search_HeaderD(String strPlantCode, String strFullStdNumber)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 표준번호와 표준번호순번으로 구분
                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                brwChannel.mfCredentials(clsHeader);

                DataTable dtHeaderD = clsHeader.mfReadISOProcessInspectSpecHDetail(strPlantCode, strStdNumber, strStdSeq, m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < dtHeaderD.Rows.Count; i++)
                {
                    // Package 콤보 ValueChanged 이벤트 해제
                    this.uComboPackage.ValueChanged -= new EventHandler(uComboPackage_ValueChanged);
                    this.uTextCustomerCode.ValueChanged -= new EventHandler(uTextCustomerCode_ValueChanged);

                    this.uComboPlant.Value = dtHeaderD.Rows[i]["PlantCode"].ToString();
                    this.uComboPlant.ReadOnly = true;
                    this.uTextStdNumber.Text = dtHeaderD.Rows[i]["StdNumber"].ToString() + dtHeaderD.Rows[i]["StdSeq"].ToString();
                    this.uComboPackage.Value = dtHeaderD.Rows[i]["Package"].ToString();
                    this.uComboPackage.ReadOnly = true;

                    //작성자는 현재 로그인자로 보여준다.
                    this.uTextWriteID.Text = m_resSys.GetString("SYS_USERID");
                    //this.uTextWriteID.Text = dtHeaderD.Rows[i]["WriteID"].ToString();
                    this.uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");
                    //this.uTextWriteName.Text = dtHeaderD.Rows[i]["WriteName"].ToString();

                    //작성일은 현재 날짜를 기본적으로 보여준다.
                    this.uDateWriteDate.Value = DateTime.Now;
                    //this.uDateWriteDate.Value = dtHeaderD.Rows[i]["WriteDate"];

                    this.uTextEtcDesc.Text = dtHeaderD.Rows[i]["EtcDesc"].ToString();

                    this.uTextCustomerCode.Text = dtHeaderD.Rows[i]["CustomerCode"].ToString();
                    this.uTextCustomerName.Text = dtHeaderD.Rows[i]["CustomerName"].ToString();

                    this.RichTextEtc1.SetDocumentText(dtHeaderD.Rows[i]["EtcDesc1"].ToString());
                    this.RichTextEtc2.SetDocumentText(dtHeaderD.Rows[i]["EtcDesc2"].ToString());
                    this.RichTextEtc3.SetDocumentText(dtHeaderD.Rows[i]["EtcDesc3"].ToString());
                    this.RichTextEtc4.SetDocumentText(dtHeaderD.Rows[i]["EtcDesc4"].ToString());

                    // Package 콤보 ValueChanged 이벤트 등록
                    this.uComboPackage.ValueChanged += new EventHandler(uComboPackage_ValueChanged);
                    this.uTextCustomerCode.ValueChanged += new EventHandler(uTextCustomerCode_ValueChanged);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 조회 Mthod
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strFullStdNumber"> 표준번호(표준번호 + 표준번호순번) </param>
        private void Search_Detail(String strPlantCode, String strFullStdNumber)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 표준번호와 표준번호순번으로 구분
                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecD), "ProcessInspectSpecD");
                QRPISO.BL.ISOPRC.ProcessInspectSpecD clsDetail = new QRPISO.BL.ISOPRC.ProcessInspectSpecD();
                brwChannel.mfCredentials(clsDetail);

                DataTable dtDetail = clsDetail.mfReadISOProcessInspectSpecD(strPlantCode, strStdNumber, strStdSeq, m_resSys.GetString("SYS_LANG"));

                //DataColumn[] dc = new DataColumn[4];
                //dc[0] = dtDetail.Columns["ProcessCode"];
                //dc[1] = dtDetail.Columns["InspectItemCode"];
                //dc[2] = dtDetail.Columns["StackSeq"];
                //dc[3] = dtDetail.Columns["Generation"];

                //dtDetail.PrimaryKey = dc;
                WinGrid grd = new WinGrid();

                this.uGridPRCDetail.DataSource = dtDetail;
                this.uGridPRCDetail.DataBind();
                if (dtDetail.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGridPRCDetail, 0);

                // 검사유형
                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(clsIType);

                DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, string.Empty, m_resSys.GetString("SYS_LANG"));

                // 검사항목
                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, string.Empty, string.Empty, m_resSys.GetString("SYS_LANG"));
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectItemCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtItem);

                // DropDown 설정
                for (int i = 0; i < this.uGridPRCDetail.Rows.Count; i++)
                {
                    // 변수
                    string strInspectGroupCode = this.uGridPRCDetail.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                    string strInspectTypeoCode = this.uGridPRCDetail.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                    //string _strPlantCode = this.uGridPRCDetail.Rows[i].Cells["PlantCode"].Value.ToString();

                    //셀에서 선택한 검사그룹에 따라 검사유형 나오게 하기
                    DataRow[] drInspectType = dtInspectType.Select("InspectGroupCode = '" + strInspectGroupCode + "'");
                    DataTable dtInspectTypeSelect = new DataTable();
                    dtInspectTypeSelect.Columns.Add("InspectTypeCode");
                    dtInspectTypeSelect.Columns.Add("InspectTypeName");
                    dtInspectTypeSelect.Clear();

                    foreach (DataRow dr in drInspectType)
                    {
                        DataRow _dr = dtInspectTypeSelect.NewRow();
                        for (int j = 0; j < 2; j++)
                        {
                            _dr[j] = dr[j];
                        }
                        dtInspectTypeSelect.Rows.Add(_dr);
                    }
                    wGrid.mfSetGridCellValueList(this.uGridPRCDetail, i, "InspectTypeCode", "", "선택", dtInspectTypeSelect);

                    //셀에서 선택한 검사그룹, 검사유형에 따라 검사항목 나오게 하기
                    DataRow[] drInspectItem = dtItem.Select("InspectGroupCode = '" + strInspectGroupCode + "' AND InspectTypeCode = '" + strInspectTypeoCode + "'");
                    DataTable dtItemSelect = new DataTable();
                    dtItemSelect.Columns.Add("InspectItemCode", typeof(string));
                    dtItemSelect.Columns.Add("InspectItemName", typeof(string));
                    dtItemSelect.Clear();

                    foreach (DataRow dr in drInspectItem)
                    {
                        DataRow _dr = dtItemSelect.NewRow();
                        for (int j = 0; j < 2; j++)
                        {
                            _dr[j] = dr[j];
                        }
                        dtItemSelect.Rows.Add(_dr);
                    }
                    wGrid.mfSetGridCellValueList(this.uGridPRCDetail, i, "InspectItemCode", "", "선택", dtItemSelect);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 컨트롤 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 등록자에 로그인 사용자 ID, Name 적용
                String strWriteID = m_resSys.GetString("SYS_USERID");
                String strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                // Method 호출
                String strWriteName = m_resSys.GetString("SYS_USERNAME");

                this.uTextWriteID.Text = strWriteID;
                this.uTextWriteName.Text = strWriteName;
                this.uDateWriteDate.Value = DateTime.Now;
                this.uComboPackage.Value = "";
                this.uTextEtcDesc.Text = "";
                this.uTextStdNumber.Text = "";
                this.uComboPlant.Value = strPlantCode;
                this.uComboPlant.ReadOnly = false;
                this.uComboPackage.ReadOnly = false;
                this.RichTextEtc1.Clear();
                this.RichTextEtc2.Clear();
                this.RichTextEtc3.Clear();
                this.RichTextEtc4.Clear();

                // 상세 그리드 삭제
                while (this.uGridPRCDetail.Rows.Count > 0)
                {
                    this.uGridPRCDetail.Rows[0].Delete(false);
                }

                // PK 편집가능상태
                this.uComboPlant.ReadOnly = false;
                this.uTextCustomerCode.ReadOnly = false;
                this.uComboPackage.ReadOnly = false;

                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uTextCustomerCode.Appearance.BackColor = Color.PowderBlue;
                this.uComboPackage.Appearance.BackColor = Color.PowderBlue;

                // 상세정보 탭으로 이동
                this.uTab.Tabs[0].Selected = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// UserName 받는 함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strWriteID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserName(String strPlantCode, String strWriteID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strWriteID, m_resSys.GetString("SYS_LANG"));

                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }

                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 제품에 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProductCode"> 제품코드 </param>
        /// <returns> 제품정보 DataTable </returns>
        private DataTable GetProductInfo(String strPlantCode, String strProductCode)
        {
            DataTable dtProduct = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                dtProduct = clsProduct.mfReadMASMaterialDetail(strPlantCode, strProductCode, m_resSys.GetString("SYS_LANG"));

                return dtProduct;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtProduct;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Customer정보 조회 Method
        /// </summary>
        /// <param name="strCustomerCode"></param>
        /// <returns></returns>
        private DataTable GetCustomerInfo(String strCustomerCode)
        {
            DataTable dtCustomer = new DataTable();
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                return dtCustomer;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtCustomer;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 기존에 등록된 표준번호가 있는지 확인하는 Method
        /// </summary>
        private Boolean CheckStdNumber()
        {
            Boolean bolCheck = false;
            try
            {
                // SystemInfor ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();
                String strPackage = this.uComboPackage.Value.ToString();
                string strCustomerCode = this.uTextCustomerCode.Text;

                // Method 호출에 필요한 모든 정보가 입력되었을때
                if (strPlantCode != "" && strPackage != "" && !strCustomerCode.Equals(string.Empty))
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
                    QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
                    brwChannel.mfCredentials(clsHeader);

                    // 기존에 존재하는 표준번호가 있는지 확인하는 Method 호출
                    DataTable dtHeader = clsHeader.mfReadISOProcessInspectSpecCheck(strPlantCode, strPackage, strCustomerCode);

                    if (dtHeader.Rows.Count > 0)
                    {
                        WinMessageBox msg = new WinMessageBox();

                        //등록된 표준번호가 있습니다.
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001208", "M000383",
                                            Infragistics.Win.HAlign.Right);

                        for (int i = 0; i < dtHeader.Rows.Count; i++)
                        {
                            this.uTextStdNumber.Text = dtHeader.Rows[i]["StdNumber"].ToString() + dtHeader.Rows[i]["StdSeq"].ToString();
                            this.uTextWriteID.Text = dtHeader.Rows[i]["WriteID"].ToString();
                            this.uTextWriteName.Text = dtHeader.Rows[i]["WriteName"].ToString();
                            this.uDateWriteDate.Value = dtHeader.Rows[i]["WriteDate"];
                            this.uTextEtcDesc.Text = dtHeader.Rows[i]["EtcDesc"].ToString();
                            this.RichTextEtc1.SetDocumentText(dtHeader.Rows[i]["EtcDesc1"].ToString());
                            this.RichTextEtc2.SetDocumentText(dtHeader.Rows[i]["EtcDesc2"].ToString());
                            this.RichTextEtc3.SetDocumentText(dtHeader.Rows[i]["EtcDesc3"].ToString());
                            this.RichTextEtc4.SetDocumentText(dtHeader.Rows[i]["EtcDesc4"].ToString());
                        }

                        // 상세정보 조회
                        string strFullStdNumber = this.uTextStdNumber.Text;
                        // Method 호출
                        Search_Detail(strPlantCode, strFullStdNumber);

                        // 공장 콤보박스 편집불가상태로
                        this.uComboPlant.ReadOnly = true;
                        this.uTextCustomerCode.ReadOnly = true;
                        this.uComboPackage.ReadOnly = true;

                        this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                        this.uTextCustomerCode.Appearance.BackColor = Color.Gainsboro;
                        this.uComboPackage.Appearance.BackColor = Color.Gainsboro;

                        bolCheck = true;
                    }
                }
                if (bolCheck == false)
                {
                    this.uComboPlant.ReadOnly = false;
                    this.uTextCustomerCode.ReadOnly = false;
                    this.uComboPackage.ReadOnly = false;
                }
                return bolCheck;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return bolCheck;
            }
            finally
            {
            }
        }
        #endregion

        private void frmISO0007D_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        // 복사버튼 이벤트
        private void uButtonCopy_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPBrowser brwChannel = new QRPBrowser();
            WinGrid grd = new WinGrid();
            WinGrid wGrid = new WinGrid();
            string strPlantCode = this.uComboPlant.Value.ToString();

            try
            {
                if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M000266",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                DialogResult Result = new DialogResult();

                if (this.uComboPackage.Value.ToString() == "")
                {
                    //Package을 선택해 주세요
                    Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001014", "M000097", Infragistics.Win.HAlign.Right);
                    this.uComboPackage.DropDown();
                    return;
                }

                frmPOP0014 frmPOP = new frmPOP0014();
                frmPOP.PlantCode = uComboPlant.Value.ToString();
                frmPOP.ShowDialog();

                //string strPlantCode = frmPOP.PlantCode;
                //string strFullStdNumber = frmPOP.StdNumber;

                //this.uComboPlant.Value = strPlantCode;

                ////상세정보 메소드 호출
                //Search_Detail(strPlantCode, strFullStdNumber);

                DataTable dtDetailRtn = frmPOP.dtRtn;


                //this.uGridReqSampling.EventManager.AllEventsEnabled = false;
                this.uGridPRCDetail.EventManager.AllEventsEnabled = false;

                if (dtDetailRtn.Rows.Count == 0)
                {
                    return;
                }
                else
                {
                    ////((DataTable)this.uGridPRCDetail.DataSource).AcceptChanges();
                    ////DataTable dtOrigin = (DataTable)this.uGridPRCDetail.DataSource;
                    ////if(!(dtOrigin.Rows.Count>0))
                    ////    dtOrigin = dtDetailRtn.Clone();
                    ////dtOrigin.Merge(dtDetailRtn);

                    ////this.uGridPRCDetail.SetDataBinding(dtOrigin, string.Empty);


                    if (this.uGridPRCDetail.Rows.Count.Equals(0))
                    {
                        this.uGridPRCDetail.DataSource = dtDetailRtn;
                        this.uGridPRCDetail.DataBind();
                    }
                    else
                    {
                        for (int i = 0; i < dtDetailRtn.Rows.Count; i++)
                        {
                            uGridPRCDetail.DisplayLayout.Bands[0].AddNew();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["ProcessCode1"].Value = dtDetailRtn.Rows[i]["ProcessCode1"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["ProcessCode"].Value = dtDetailRtn.Rows[i]["ProcessCode"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["ProcessSeq"].Value = dtDetailRtn.Rows[i]["ProcessSeq"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["InspectGroupCode"].Value = dtDetailRtn.Rows[i]["InspectGroupCode"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["InspectTypeCode"].Value = dtDetailRtn.Rows[i]["InspectTypeCode"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["InspectItemCode"].Value = dtDetailRtn.Rows[i]["InspectItemCode"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["StackSeq"].Value = dtDetailRtn.Rows[i]["StackSeq"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["Generation"].Value = dtDetailRtn.Rows[i]["Generation"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["ProcessInspectFlag"].Value = dtDetailRtn.Rows[i]["ProcessInspectFlag"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["ProductItemFlag"].Value = dtDetailRtn.Rows[i]["ProductItemFlag"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["QualityItemFlag"].Value = dtDetailRtn.Rows[i]["QualityItemFlag"].ToString();
                            //if (!dtSpecD.Rows[i]["LowerSpec"].ToString().Equals(string.Empty))
                            //    uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["LowerSpec"].Value = Convert.ToDecimal(dtSpecD.Rows[i]["LowerSpec"].ToString());
                            //if (!dtSpecD.Rows[i]["UpperSpec"].ToString().Equals(string.Empty))
                            //    uGrid1.Rows[uGrid1.Rows.Count - 1].Cells["UpperSpec"].Value = Convert.ToDecimal(dtSpecD.Rows[i]["UpperSpec"].ToString());
                            if (!dtDetailRtn.Rows[i]["LowerSpec"].ToString().Equals(string.Empty))
                                uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["LowerSpec"].Value = dtDetailRtn.Rows[i]["LowerSpec"].ToString();
                            if ((!dtDetailRtn.Rows[i]["UpperSpec"].ToString().Equals(string.Empty)))
                                uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["UpperSpec"].Value = dtDetailRtn.Rows[i]["UpperSpec"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["SpecRange"].Value = dtDetailRtn.Rows[i]["SpecRange"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["SpecUnitCode"].Value = dtDetailRtn.Rows[i]["SpecUnitCode"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["SampleSize"].Value = dtDetailRtn.Rows[i]["SampleSize"].ToString();
                            if (!dtDetailRtn.Rows[i]["ProcessInspectSS"].ToString().Equals(string.Empty))
                                uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["ProcessInspectSS"].Value = dtDetailRtn.Rows[i]["ProcessInspectSS"].ToString();
                            if (!dtDetailRtn.Rows[i]["ProductItemSS"].ToString().Equals(string.Empty))
                                uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["ProductItemSS"].Value = dtDetailRtn.Rows[i]["ProductItemSS"].ToString();
                            if (!dtDetailRtn.Rows[i]["QualityItemSS"].ToString().Equals(string.Empty))
                                uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["QualityItemSS"].Value = dtDetailRtn.Rows[i]["QualityItemSS"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["UnitCode"].Value = dtDetailRtn.Rows[i]["UnitCode"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["InspectPeriod"].Value = dtDetailRtn.Rows[i]["InspectPeriod"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["PeriodUnitCode"].Value = dtDetailRtn.Rows[i]["PeriodUnitCode"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["CompareFlag"].Value = dtDetailRtn.Rows[i]["CompareFlag"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["AQLFlag"].Value = dtDetailRtn.Rows[i]["AQLFlag"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["DataType"].Value = dtDetailRtn.Rows[i]["DataType"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["EtcDesc"].Value = dtDetailRtn.Rows[i]["EtcDesc"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["InspectCondition"].Value = dtDetailRtn.Rows[i]["InspectCondition"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["Method"].Value = dtDetailRtn.Rows[i]["Method"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["SpecDesc"].Value = dtDetailRtn.Rows[i]["SpecDesc"].ToString();
                            uGridPRCDetail.Rows[uGridPRCDetail.Rows.Count - 1].Cells["MeasureToolCode"].Value = dtDetailRtn.Rows[i]["MeasureToolCode"].ToString();
                        }
                        this.uGridPRCDetail.Rows.Band.AddNew();
                    }

                    if (dtDetailRtn.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGridPRCDetail, 0);

                    // 검사유형
                    // BL 연결
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(clsIType);

                    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, string.Empty, m_resSys.GetString("SYS_LANG"));

                    // 검사항목
                    // BL 연결
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(clsItem);

                    DataTable dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, string.Empty, string.Empty, m_resSys.GetString("SYS_LANG"));
                    //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectItemCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtItem);

                    // DropDown 설정
                    for (int i = 0; i < this.uGridPRCDetail.Rows.Count; i++)
                    {
                        // 변수
                        string strInspectGroupCode = this.uGridPRCDetail.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                        string strInspectTypeoCode = this.uGridPRCDetail.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                        //string _strPlantCode = this.uGridPRCDetail.Rows[i].Cells["PlantCode"].Value.ToString();

                        //셀에서 선택한 검사그룹에 따라 검사유형 나오게 하기
                        DataRow[] drInspectType = dtInspectType.Select("InspectGroupCode = '" + strInspectGroupCode + "'");
                        DataTable dtInspectTypeSelect = new DataTable();
                        dtInspectTypeSelect.Columns.Add("InspectTypeCode");
                        dtInspectTypeSelect.Columns.Add("InspectTypeName");
                        dtInspectTypeSelect.Clear();

                        foreach (DataRow dr in drInspectType)
                        {
                            DataRow _dr = dtInspectTypeSelect.NewRow();
                            for (int j = 0; j < 2; j++)
                            {
                                _dr[j] = dr[j];
                            }
                            dtInspectTypeSelect.Rows.Add(_dr);
                        }
                        wGrid.mfSetGridCellValueList(this.uGridPRCDetail, i, "InspectTypeCode", "", "선택", dtInspectTypeSelect);

                        //셀에서 선택한 검사그룹, 검사유형에 따라 검사항목 나오게 하기
                        DataRow[] drInspectItem = dtItem.Select("InspectGroupCode = '" + strInspectGroupCode + "' AND InspectTypeCode = '" + strInspectTypeoCode + "'");
                        DataTable dtItemSelect = new DataTable();
                        dtItemSelect.Columns.Add("InspectItemCode", typeof(string));
                        dtItemSelect.Columns.Add("InspectItemName", typeof(string));
                        dtItemSelect.Clear();

                        foreach (DataRow dr in drInspectItem)
                        {
                            DataRow _dr = dtItemSelect.NewRow();
                            for (int j = 0; j < 2; j++)
                            {
                                _dr[j] = dr[j];
                            }
                            dtItemSelect.Rows.Add(_dr);
                        }
                        wGrid.mfSetGridCellValueList(this.uGridPRCDetail, i, "InspectItemCode", "", "선택", dtItemSelect);
                        if (this.uGridPRCDetail.Rows[i].Cells["ProductItemFlag"].Value.Equals(DBNull.Value) || this.uGridPRCDetail.Rows[i].Cells["ProductItemFlag"].Value.ToString().Equals(string.Empty))
                            this.uGridPRCDetail.Rows[i].Cells["ProductItemFlag"].Value = "False";
                        if (this.uGridPRCDetail.Rows[i].Cells["ProcessInspectFlag"].Value.Equals(DBNull.Value) || this.uGridPRCDetail.Rows[i].Cells["ProcessInspectFlag"].Value.ToString().Equals(string.Empty))
                            this.uGridPRCDetail.Rows[i].Cells["ProcessInspectFlag"].Value = "False";
                        if (this.uGridPRCDetail.Rows[i].Cells["QualityItemFlag"].Value.Equals(DBNull.Value) || this.uGridPRCDetail.Rows[i].Cells["QualityItemFlag"].Value.ToString().Equals(string.Empty))
                            this.uGridPRCDetail.Rows[i].Cells["QualityItemFlag"].Value = "False";

                        //for (int i = 0; i < this.uGridPRCDetail.Rows.Count; i++)
                        //{
                        //    if (this.uGridPRCDetail.Rows[i].Cells["ProcessCode1"].Value.ToString().Equals(e.DataErrorInfo.Row.Cells["ProcessCode1"].Value.ToString())
                        //        && this.uGridPRCDetail.Rows[i].Cells["InspectGroupCode"].Value.ToString().Equals(e.DataErrorInfo.Row.Cells["InspectGroupCode"].Value.ToString())
                        //        && this.uGridPRCDetail.Rows[i].Cells["InspectTypeCode"].Value.ToString().Equals(e.DataErrorInfo.Row.Cells["InspectTypeCode"].Value.ToString())
                        //        && this.uGridPRCDetail.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals(e.DataErrorInfo.Row.Cells["InspectItemCode"].Value.ToString())
                        //        && this.uGridPRCDetail.Rows[i].Cells["StackSeq"].Value.ToString().Equals(e.DataErrorInfo.Row.Cells["StackSeq"].Value.ToString())
                        //        && this.uGridPRCDetail.Rows[i].Cells["Generation"].Value.ToString().Equals(e.DataErrorInfo.Row.Cells["Generation"].Value.ToString()))
                        //    {
                        //        //int intseq = Convert.ToInt32(this.uGridPRCDetail.Rows[i].Cells["Seq"].Value.ToString());
                        //        this.uGridPRCDetail.Rows[i].Appearance.BackColor = Color.LightCoral;
                        //    }
                        //}

                        if (this.uGridPRCDetail.Rows[i].Cells["InspectGroupCode"].Value.ToString().Equals(string.Empty) &&
                                this.uGridPRCDetail.Rows[i].Cells["InspectTypeCode"].Value.ToString().Equals(string.Empty) &&
                                this.uGridPRCDetail.Rows[i].Cells["InspectItemCode"].Value.ToString().Equals(string.Empty))
                        {
                            this.uGridPRCDetail.Rows[i].Hidden = true;
                        }
                    }
                }
                this.uGridPRCDetail.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPRCDetail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 자동 행삭제
                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridPRCDetail, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                // 규격 상한/하한 입력에 따라 범위 Cell 값 지정
                if (e.Cell.Column.Key.Equals("UpperSpec"))
                {
                    if (e.Cell.Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Value.ToString()))
                    {
                        if (e.Cell.Row.Cells["LowerSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["LowerSpec"].Value.ToString()))
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "";
                        }
                        else
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "U";
                        }
                    }
                    else if (e.Cell.Row.Cells["LowerSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["LowerSpec"].Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "L";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["LowerSpec"].Value.ToString()) <= ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["LowerSpec"].Value.ToString()) > ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        //규격상한은 규격하한값보다 커야 합니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000323"
                            , "M000324", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        return;
                    }
                }
                else if (e.Cell.Column.Key.Equals("LowerSpec"))
                {
                    if (e.Cell.Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Value.ToString()))
                    {
                        if (e.Cell.Row.Cells["UpperSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["UpperSpec"].Value.ToString()))
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "";
                        }
                        else
                        {
                            e.Cell.Row.Cells["SpecRange"].Value = "L";
                        }
                    }
                    else if (e.Cell.Row.Cells["UpperSpec"].Value == DBNull.Value || string.IsNullOrEmpty(e.Cell.Row.Cells["UpperSpec"].Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "U";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["UpperSpec"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        e.Cell.Row.Cells["SpecRange"].Value = "";
                    }
                    else if (ReturnDecimalValue(e.Cell.Row.Cells["UpperSpec"].Value.ToString()) < ReturnDecimalValue(e.Cell.Value.ToString()))
                    {
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        //규격상한은 규격하한값보다 커야 합니다.
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000323"
                            , "M000324", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        return;
                    }
                }

                // 검사유형 DropDown 설정
                else if (e.Cell.Column.Key == "InspectGroupCode")
                {
                    // 변수
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType clsIType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(clsIType);

                    DataTable dtInspectType = clsIType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));

                    wGrid.mfSetGridCellValueList(this.uGridPRCDetail, e.Cell.Row.Index, "InspectTypeCode", "", "선택", dtInspectType);

                    //e.Cell.Row.Cells["InspectTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }

                // 검사항목 DropDown 설정
                else if (e.Cell.Column.Key == "InspectTypeCode")
                {
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                    String strInspectTypeoCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(clsItem);

                    DataTable dtItem = clsItem.mfReadMASInspectItemCombo(strPlantCode, strInspectGroupCode, strInspectTypeoCode, m_resSys.GetString("SYS_LANG"));

                    //셀에서 선택한 검사그룹, 검사유형에 따라 검사항목 나오게 하기
                    DataRow[] drInspectItem = dtItem.Select("InspectGroupCode = '" + strInspectGroupCode + "' AND InspectTypeCode = '" + strInspectTypeoCode + "'");
                    DataTable dtItemSelect = new DataTable();
                    dtItemSelect.Columns.Add("InspectItemCode", typeof(string));
                    dtItemSelect.Columns.Add("InspectItemName", typeof(string));
                    dtItemSelect.Clear();

                    foreach (DataRow dr in drInspectItem)
                    {
                        DataRow _dr = dtItemSelect.NewRow();
                        for (int i = 0; i < 2; i++)
                        {
                            _dr[i] = dr[i];
                        }
                        dtItemSelect.Rows.Add(_dr);
                    }
                    wGrid.mfSetGridCellValueList(this.uGridPRCDetail, e.Cell.Row.Index, "InspectItemCode", "", "선택", dtItemSelect);

                    //e.Cell.Row.Cells["InspectItemCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }

                // 검사항목 선택시 데이터 유형 자동 선택
                else if (e.Cell.Column.Key == "InspectItemCode")
                {
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strInspectGroupCode = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                    String strInspectTypeCode = e.Cell.Row.Cells["InspectTypeCode"].Value.ToString();
                    String strInspectItemCode = e.Cell.Value.ToString();

                    // 검색조건

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(clsItem);

                    DataTable dtItem = clsItem.mfReadMASInspectItemDetail(strPlantCode, strInspectItemCode, m_resSys.GetString("SYS_LANG"));

                    // 데이터 유형 필드에 값 적용
                    if (!dtItem.Rows.Count.Equals(0))
                        e.Cell.Row.Cells["DataType"].Value = dtItem.Rows[0]["DataType"].ToString();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                QRPBrowser brwChannel = new QRPBrowser();

                String strPlantCode = this.uComboPlant.Value.ToString();
                DataTable dtProcess = new DataTable();
                DataTable dtInspecrGroup = new DataTable();
                DataTable dtMeasureTool = new DataTable();
                DataTable dtStackSeq = new DataTable();
                DataTable dtGeneration = new DataTable();

                if (strPlantCode != "")
                {
                    // 공장코드 변경시 그리드 데이터 모두 삭제
                    while (this.uGridPRCDetail.Rows.Count > 0)
                    {
                        this.uGridPRCDetail.Rows[0].Delete(false);
                    }

                    // 공정
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 검사분류
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                    QRPMAS.BL.MASQUA.InspectGroup clsInspectGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                    brwChannel.mfCredentials(clsInspectGroup);

                    dtInspecrGroup = clsInspectGroup.mfReadMASInspectGroupCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 계측기
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MeasureTool), "MeasureTool");
                    QRPMAS.BL.MASQUA.MeasureTool clsMeasureTool = new QRPMAS.BL.MASQUA.MeasureTool();
                    brwChannel.mfCredentials(clsMeasureTool);

                    dtMeasureTool = clsMeasureTool.mfReadMASMeasureToolCombo(strPlantCode, "", m_resSys.GetString("SYS_LANG"));

                    // StackSeq, 세대
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);

                    //dtStackSeq = clsProduct.mfReadMASProduct_StackSeq(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    dtGeneration = clsProduct.mfReadMASProduct_Generation(strPlantCode, m_resSys.GetString("SYS_LANG"));

                }
                // 그리드 컬럼에 적용
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "ProcessCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcess);
                this.uGridPRCDetail.DisplayLayout.Bands[0].Columns["ProcessCode"].Layout.ValueLists.Clear();
                wGrid.mfSetGridColumnValueGridList(this.uGridPRCDetail, 0, "ProcessCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "ProcessCode,ProcessName", "공정코드,공정명"
                                                    , "ProcessCode", "ProcessName", dtProcess);
                wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtInspecrGroup);
                wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "MeasureToolCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtMeasureTool);
                //wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "StackSeq", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtStackSeq);
                wGrid.mfSetGridColumnValueList(this.uGridPRCDetail, 0, "Generation", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtGeneration);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m;
        }

        /// <summary>
        /// Int형 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">int로 반환받을 값</param>
        /// <returns></returns>
        private int ReturnIntValue(string value)
        {
            int result = 0;
            if (int.TryParse(value, out result))
                return result;
            else
                return 0;
        }

        private void uGridPRCDetail_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            try
            {
                if (e.ErrorType.ToString().Equals("Data"))
                {
                    e.Cancel = true;

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    DialogResult Result = new DialogResult();
                    WinMessageBox msg = new WinMessageBox();

                    string strLang = m_resSys.GetString("SYS_LANG");
                    //e.DataErrorInfo.Row.Refresh(Infragistics.Win.UltraWinGrid.RefreshRow.ReloadData);

                    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001122", strLang)
                                        , e.DataErrorInfo.Row.Cells["ProcessCode"].Text + msg.GetMessge_Text("M000292", strLang), Infragistics.Win.HAlign.Right);

                    e.DataErrorInfo.Row.Cells["ProcessCode"].SetValue(string.Empty, false);
                    e.DataErrorInfo.Row.Cells["InspectItemCode"].SetValue(string.Empty, false);
                    e.DataErrorInfo.Row.Cells["InspectTypeCOde"].SetValue(string.Empty, false);
                    e.DataErrorInfo.Row.Cells["InspectGroupCode"].SetValue(string.Empty, false);
                    //e.DataErrorInfo.Row.Cells["StackSeq"].SetValue(string.Empty, false);
                    e.DataErrorInfo.Row.Cells["Generation"].SetValue(string.Empty, false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0007D_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 패키지콤보 선택시 이미 등록된 규격서정보 있는지 조회
        private void uComboPackage_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Boolean bolCheck = CheckStdNumber();
                if (bolCheck == false)
                {
                    this.uTextStdNumber.Text = "";
                    this.uTextEtcDesc.Text = "";
                    while (this.uGridPRCDetail.Rows.Count > 0)
                    {
                        this.uGridPRCDetail.Rows[0].Delete(false);
                    }
                    this.RichTextEtc1.Clear();
                    this.RichTextEtc2.Clear();
                    this.RichTextEtc3.Clear();
                    this.RichTextEtc4.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 패키지콤보 선택시 이미 등록된 규격서정보 있는지 조회
        private void uTextCustomerCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Boolean bolCheck = CheckStdNumber();
                if (bolCheck == false)
                {
                    this.uTextStdNumber.Text = "";
                    this.uTextEtcDesc.Text = "";
                    while (this.uGridPRCDetail.Rows.Count > 0)
                    {
                        this.uGridPRCDetail.Rows[0].Delete(false);
                    }
                    this.RichTextEtc1.Clear();
                    this.RichTextEtc2.Clear();
                    this.RichTextEtc3.Clear();
                    this.RichTextEtc4.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPRCDetail_AfterCellListCloseUp(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("ProcessCode"))
                {
                    e.Cell.Row.Cells["ProcessCode1"].Value = e.Cell.ValueListResolved.GetValue(e.Cell.ValueListResolved.SelectedItemIndex);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();

                frmPOP.ShowDialog();

                this.uTextCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextCustomerName.Text = frmPOP.CustomerName;

                Boolean bolCheck = CheckStdNumber();
                if (bolCheck == false)
                {
                    this.uTextStdNumber.Text = "";
                    this.uTextEtcDesc.Text = "";
                    while (this.uGridPRCDetail.Rows.Count > 0)
                    {
                        this.uGridPRCDetail.Rows[0].Delete(false);
                    }
                    this.RichTextEtc1.Clear();
                    this.RichTextEtc2.Clear();
                    this.RichTextEtc3.Clear();
                    this.RichTextEtc4.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboPackage_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uTextCustomerCode.Text.Equals(string.Empty))
                {
                    e.Cancel = true;

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    //고객코드를 입력해 주세요.
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001264", "M000255", "M000256", Infragistics.Win.HAlign.Center);

                    // 이벤트 발생시키기
                    Infragistics.Win.UltraWinEditors.EditorButtonEventArgs ev = new Infragistics.Win.UltraWinEditors.EditorButtonEventArgs(this.uTextCustomerCode.ButtonsRight["Find"], this.uTextCustomerCode);
                    this.uTextCustomerCode_EditorButtonClick(this.uTextCustomerCode, ev);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchCustomerCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0003 frmPOP = new frmPOP0003();

                frmPOP.ShowDialog();

                this.uTextSearchCustomerCode.Text = frmPOP.CustomerCode;
                this.uTextSearchCustomerName.Text = frmPOP.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPRCDetail_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["SpecRange"].Value.ToString().Equals("U"))
                {
                    e.Row.Cells["UpperSpec"].Value = DBNull.Value;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals("L"))
                {
                    e.Row.Cells["LowerSpec"].Value = DBNull.Value;
                }
                else if (e.Row.Cells["SpecRange"].Value.ToString().Equals(String.Empty))
                {
                    if (ReturnDecimalValue(e.Row.Cells["LowerSpec"].Value.ToString()) == 0m && ReturnDecimalValue(e.Row.Cells["UpperSpec"].Value.ToString()) == 0m)
                    {
                        e.Row.Cells["UpperSpec"].Value = DBNull.Value;
                        e.Row.Cells["LowerSpec"].Value = DBNull.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }
        /// <summary>
        /// 고객사 검색 키다운 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSearchCustomerCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();
                if (e.KeyCode.Equals(Keys.Enter))
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                    QRPMAS.BL.MASGEN.Customer clsCustmer = new QRPMAS.BL.MASGEN.Customer();
                    brwChannel.mfCredentials(clsCustmer);

                    string strPlantCode = this.uComboPlant.Value.ToString();
                    string strCustomerCode = this.uTextSearchCustomerCode.Text;

                    DataTable dtCustomerName = clsCustmer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                    if (!dtCustomerName.Rows.Count.Equals(0))
                    {
                        this.uTextSearchCustomerName.Text = dtCustomerName.Rows[0]["CustomerName"].ToString();
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001099", strLang), msg.GetMessge_Text("M001101", strLang), "해당 고객사 코드는 없습니다.", Infragistics.Win.HAlign.Right);
                        this.uTextSearchCustomerCode.Text = string.Empty;
                        this.uTextSearchCustomerName.Text = string.Empty;

                        return;
                    }
                }
                else if (e.KeyCode.Equals(Keys.Delete) || e.KeyCode.Equals(Keys.Back))
                {
                    this.uTextSearchCustomerCode.Text = string.Empty;
                    this.uTextSearchCustomerName.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextCustomerCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (e.KeyCode.Equals(Keys.Enter))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                    QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                    brwChannel.mfCredentials(clsCustomer);

                    string strCustomerCode = this.uTextCustomerCode.Text;

                    DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                    if (!dtCustomer.Rows.Count.Equals(0))
                    {
                        this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();

                        Boolean bolCheck = CheckStdNumber();
                        if (bolCheck == false)
                        {
                            this.uTextStdNumber.Text = "";
                            this.uTextEtcDesc.Text = "";
                            while (this.uGridPRCDetail.Rows.Count > 0)
                            {
                                this.uGridPRCDetail.Rows[0].Delete(false);
                            }
                            this.RichTextEtc1.Clear();
                            this.RichTextEtc2.Clear();
                            this.RichTextEtc3.Clear();
                            this.RichTextEtc4.Clear();
                        }
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001099", "M001101", "M001419", Infragistics.Win.HAlign.Right);

                        this.uTextCustomerCode.Text = string.Empty;
                        this.uTextCustomerName.Text = string.Empty;

                        return;
                    }
                }
                else if (e.KeyCode.Equals(Keys.Delete) || e.KeyCode.Equals(Keys.Back))
                {
                    this.uTextCustomerCode.Text = string.Empty;
                    this.uTextCustomerName.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 컬럼에 콤보박스로 설정하는 함수
        /// </summary>
        /// <param name="Grid">그리드 Object</param>
        /// <param name="intBandIndex"></param>
        /// <param name="strColKey"></param>
        /// <param name="ValueListStyle"></param>
        /// <param name="strTopKey"></param>
        /// <param name="strTopValue"></param>
        /// <param name="dtValueList"></param>
        public void mfSetGridColumnValueList(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                             int intBandIndex,
                                             string strColKey,
                                             Infragistics.Win.ValueListDisplayStyle ValueListStyle,
                                             string strTopKey,
                                             string strTopValue,
                                             System.Data.DataTable dtValueList)
        {
            try
            {
                Infragistics.Win.ValueList uValueList = new Infragistics.Win.ValueList();

                ////ValueList의 Style 지정
                uValueList.DisplayStyle = ValueListStyle;

                // DropDown 리스트 각Item의 높이 설정
                uValueList.ItemHeight = 15;
                // Dropdown 리스트에 한번에 보여지는 최대 Item 갯수;
                uValueList.MaxDropDownItems = 5;

                //Value List에 상단값 설정
                if (strTopKey != "" || strTopValue != "")
                {
                    // TopValue 설정
                    QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string strChgTopVal = string.Empty;
                    switch (strTopValue)
                    {
                        case "전체":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "全部";
                                    break;
                                case "ENG":
                                    strChgTopVal = "ALL";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        case "선택":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "选择";
                                    break;
                                case "ENG":
                                    strChgTopVal = "Select";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        default:
                            strChgTopVal = strTopValue;
                            break;
                    }
                    uValueList.ValueListItems.Add(strTopKey, strChgTopVal);
                }

                //추가적인 Value List 값 설정
                for (int i = 0; i < dtValueList.Rows.Count; i++)
                {
                    uValueList.ValueListItems.Add(dtValueList.Rows[i][0].ToString(), dtValueList.Rows[i][1].ToString());
                }
                uValueList.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                //uValueList.FormatFilteredItems = DefaultableBoolean.True;

                //그리드 콤보에서 Like검색 속성지정
                Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
                Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;

                //그리드 컬럼에 ValueList 지정 
                Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = uValueList;



            }
            catch (Exception ex)
            {
            }
            finally
            {
            }

        }

        
    }
}