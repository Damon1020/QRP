﻿namespace QRPISO.UI
{
    partial class frmISO0003C
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0003C));
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uCheckVersion = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelVersionCheck = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchRegToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchRegFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchRegDate = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSearchStandardNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchSmall = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMiddle = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchLarge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow3 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow4 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow2 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDown = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteRow1 = new Infragistics.Win.Misc.UltraButton();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextGRWComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRWComment = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVersionNum = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMakeDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMakeDeptName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMakeUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMakeUserName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPadsize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPadsize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChipsize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChipSize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextReturnReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ulabelReturnReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApproveUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextApproveUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApproveUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCompleteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateApplyToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateApplyFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelApplyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocKeyword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocKeyword = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocState = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCreateDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSmall = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMiddle = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboLarge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uComboProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStandardDocNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardDocNo = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).BeginInit();
            this.uGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadsize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipsize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardDocNo)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance35;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckVersion);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelVersionCheck);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchRegToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchRegFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRegDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStandardNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStandardNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 8;
            // 
            // uLabelSearchTitle
            // 
            this.uLabelSearchTitle.Location = new System.Drawing.Point(252, 36);
            this.uLabelSearchTitle.Name = "uLabelSearchTitle";
            this.uLabelSearchTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTitle.TabIndex = 34;
            this.uLabelSearchTitle.Text = "ultraLabel1";
            // 
            // uTextSearchTitle
            // 
            this.uTextSearchTitle.Location = new System.Drawing.Point(356, 36);
            this.uTextSearchTitle.Name = "uTextSearchTitle";
            this.uTextSearchTitle.Size = new System.Drawing.Size(382, 21);
            this.uTextSearchTitle.TabIndex = 35;
            // 
            // uCheckVersion
            // 
            this.uCheckVersion.Location = new System.Drawing.Point(1024, 12);
            this.uCheckVersion.Name = "uCheckVersion";
            this.uCheckVersion.Size = new System.Drawing.Size(16, 20);
            this.uCheckVersion.TabIndex = 33;
            this.uCheckVersion.Visible = false;
            // 
            // uLabelVersionCheck
            // 
            this.uLabelVersionCheck.Location = new System.Drawing.Point(1012, 12);
            this.uLabelVersionCheck.Name = "uLabelVersionCheck";
            this.uLabelVersionCheck.Size = new System.Drawing.Size(16, 20);
            this.uLabelVersionCheck.TabIndex = 32;
            this.uLabelVersionCheck.Text = "ultraLabel1";
            this.uLabelVersionCheck.Visible = false;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(1032, 40);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel1.TabIndex = 31;
            this.ultraLabel1.Text = "~";
            this.ultraLabel1.Visible = false;
            // 
            // uDateSearchRegToDate
            // 
            this.uDateSearchRegToDate.Location = new System.Drawing.Point(1048, 36);
            this.uDateSearchRegToDate.Name = "uDateSearchRegToDate";
            this.uDateSearchRegToDate.Size = new System.Drawing.Size(16, 21);
            this.uDateSearchRegToDate.TabIndex = 30;
            this.uDateSearchRegToDate.Visible = false;
            // 
            // uDateSearchRegFromDate
            // 
            this.uDateSearchRegFromDate.Location = new System.Drawing.Point(1016, 36);
            this.uDateSearchRegFromDate.Name = "uDateSearchRegFromDate";
            this.uDateSearchRegFromDate.Size = new System.Drawing.Size(16, 21);
            this.uDateSearchRegFromDate.TabIndex = 29;
            this.uDateSearchRegFromDate.Visible = false;
            // 
            // uLabelSearchRegDate
            // 
            this.uLabelSearchRegDate.Location = new System.Drawing.Point(996, 36);
            this.uLabelSearchRegDate.Name = "uLabelSearchRegDate";
            this.uLabelSearchRegDate.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchRegDate.TabIndex = 14;
            this.uLabelSearchRegDate.Text = "ultraLabel1";
            this.uLabelSearchRegDate.Visible = false;
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(1044, 12);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchProcess.TabIndex = 11;
            this.uComboSearchProcess.Text = "ultraComboEditor1";
            this.uComboSearchProcess.Visible = false;
            // 
            // uTextSearchStandardNo
            // 
            this.uTextSearchStandardNo.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchStandardNo.Name = "uTextSearchStandardNo";
            this.uTextSearchStandardNo.Size = new System.Drawing.Size(120, 21);
            this.uTextSearchStandardNo.TabIndex = 9;
            // 
            // uLabelSearchStandardNo
            // 
            this.uLabelSearchStandardNo.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchStandardNo.Name = "uLabelSearchStandardNo";
            this.uLabelSearchStandardNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStandardNo.TabIndex = 8;
            this.uLabelSearchStandardNo.Text = "ultraLabel1";
            // 
            // uComboSearchSmall
            // 
            this.uComboSearchSmall.Location = new System.Drawing.Point(860, 12);
            this.uComboSearchSmall.Name = "uComboSearchSmall";
            this.uComboSearchSmall.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchSmall.TabIndex = 7;
            this.uComboSearchSmall.Text = "ultraComboEditor1";
            // 
            // uLabelSearchSmall
            // 
            this.uLabelSearchSmall.Location = new System.Drawing.Point(756, 12);
            this.uLabelSearchSmall.Name = "uLabelSearchSmall";
            this.uLabelSearchSmall.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchSmall.TabIndex = 6;
            this.uLabelSearchSmall.Text = "ultraLabel1";
            // 
            // uComboSearchMiddle
            // 
            this.uComboSearchMiddle.Location = new System.Drawing.Point(608, 12);
            this.uComboSearchMiddle.Name = "uComboSearchMiddle";
            this.uComboSearchMiddle.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchMiddle.TabIndex = 5;
            this.uComboSearchMiddle.Text = "ultraComboEditor1";
            this.uComboSearchMiddle.ValueChanged += new System.EventHandler(this.uComboSearchMiddle_ValueChanged);
            // 
            // uLabelSearchMiddle
            // 
            this.uLabelSearchMiddle.Location = new System.Drawing.Point(504, 12);
            this.uLabelSearchMiddle.Name = "uLabelSearchMiddle";
            this.uLabelSearchMiddle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMiddle.TabIndex = 4;
            this.uLabelSearchMiddle.Text = "ultraLabel1";
            // 
            // uComboSearchLarge
            // 
            this.uComboSearchLarge.Location = new System.Drawing.Point(356, 12);
            this.uComboSearchLarge.Name = "uComboSearchLarge";
            this.uComboSearchLarge.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchLarge.TabIndex = 3;
            this.uComboSearchLarge.Text = "ultraComboEditor1";
            this.uComboSearchLarge.ValueChanged += new System.EventHandler(this.uComboSearchLarge_ValueChanged);
            // 
            // uLabelSearchLarge
            // 
            this.uLabelSearchLarge.Location = new System.Drawing.Point(252, 12);
            this.uLabelSearchLarge.Name = "uLabelSearchLarge";
            this.uLabelSearchLarge.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLarge.TabIndex = 2;
            this.uLabelSearchLarge.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridHeader
            // 
            this.uGridHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            appearance72.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHeader.DisplayLayout.Appearance = appearance72;
            this.uGridHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance73.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance73.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance73.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.GroupByBox.Appearance = appearance73;
            appearance74.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance74;
            this.uGridHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance75.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance75.BackColor2 = System.Drawing.SystemColors.Control;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance75;
            this.uGridHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            appearance76.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHeader.DisplayLayout.Override.ActiveCellAppearance = appearance76;
            appearance77.BackColor = System.Drawing.SystemColors.Highlight;
            appearance77.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHeader.DisplayLayout.Override.ActiveRowAppearance = appearance77;
            this.uGridHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.CardAreaAppearance = appearance78;
            appearance79.BorderColor = System.Drawing.Color.Silver;
            appearance79.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHeader.DisplayLayout.Override.CellAppearance = appearance79;
            this.uGridHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHeader.DisplayLayout.Override.CellPadding = 0;
            appearance80.BackColor = System.Drawing.SystemColors.Control;
            appearance80.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance80.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance80.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.GroupByRowAppearance = appearance80;
            appearance81.TextHAlignAsString = "Left";
            this.uGridHeader.DisplayLayout.Override.HeaderAppearance = appearance81;
            this.uGridHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.BorderColor = System.Drawing.Color.Silver;
            this.uGridHeader.DisplayLayout.Override.RowAppearance = appearance82;
            this.uGridHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance83.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance83;
            this.uGridHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHeader.Location = new System.Drawing.Point(0, 100);
            this.uGridHeader.Name = "uGridHeader";
            this.uGridHeader.Size = new System.Drawing.Size(1070, 720);
            this.uGridHeader.TabIndex = 14;
            this.uGridHeader.Text = "ultraGrid1";
            this.uGridHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridHeader_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 15;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Controls.Add(this.uButtonDeleteRow3);
            this.uGroupBox4.Controls.Add(this.uGrid3);
            this.uGroupBox4.Controls.Add(this.uGroupBox5);
            this.uGroupBox4.Location = new System.Drawing.Point(539, 206);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(518, 212);
            this.uGroupBox4.TabIndex = 7;
            // 
            // uButtonDeleteRow3
            // 
            this.uButtonDeleteRow3.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow3.Name = "uButtonDeleteRow3";
            this.uButtonDeleteRow3.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow3.TabIndex = 27;
            this.uButtonDeleteRow3.Text = "ultraButton1";
            this.uButtonDeleteRow3.Click += new System.EventHandler(this.uButtonDeleteRow3_Click);
            // 
            // uGrid3
            // 
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            appearance48.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance48;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance49.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance49.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance49.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance49;
            appearance50.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance50;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance51.BackColor2 = System.Drawing.SystemColors.Control;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance51;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance52;
            appearance53.BackColor = System.Drawing.SystemColors.Highlight;
            appearance53.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance53;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance54;
            appearance55.BorderColor = System.Drawing.Color.Silver;
            appearance55.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance55;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance56.BackColor = System.Drawing.SystemColors.Control;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance56;
            appearance57.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance57;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance58;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance59;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(12, 60);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(490, 143);
            this.uGrid3.TabIndex = 18;
            this.uGrid3.Text = "ultraGrid2";
            this.uGrid3.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid3_AfterCellUpdate);
            // 
            // uGroupBox5
            // 
            this.uGroupBox5.Controls.Add(this.uButtonDeleteRow4);
            this.uGroupBox5.Controls.Add(this.uGrid4);
            this.uGroupBox5.Location = new System.Drawing.Point(141, 18);
            this.uGroupBox5.Name = "uGroupBox5";
            this.uGroupBox5.Size = new System.Drawing.Size(37, 36);
            this.uGroupBox5.TabIndex = 8;
            this.uGroupBox5.Visible = false;
            // 
            // uButtonDeleteRow4
            // 
            this.uButtonDeleteRow4.Location = new System.Drawing.Point(8, 8);
            this.uButtonDeleteRow4.Name = "uButtonDeleteRow4";
            this.uButtonDeleteRow4.Size = new System.Drawing.Size(11, 12);
            this.uButtonDeleteRow4.TabIndex = 29;
            this.uButtonDeleteRow4.Text = "ultraButton1";
            this.uButtonDeleteRow4.Visible = false;
            this.uButtonDeleteRow4.Click += new System.EventHandler(this.uButtonDeleteRow4_Click);
            // 
            // uGrid4
            // 
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4.DisplayLayout.Appearance = appearance8;
            this.uGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.GroupByBox.Appearance = appearance9;
            appearance10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance10;
            this.uGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance11.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance11.BackColor2 = System.Drawing.SystemColors.Control;
            appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance11;
            this.uGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance13.BackColor = System.Drawing.SystemColors.Highlight;
            appearance13.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance13;
            this.uGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.CardAreaAppearance = appearance14;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            appearance15.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4.DisplayLayout.Override.CellAppearance = appearance15;
            this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance16.BackColor = System.Drawing.SystemColors.Control;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance16;
            appearance17.TextHAlignAsString = "Left";
            this.uGrid4.DisplayLayout.Override.HeaderAppearance = appearance17;
            this.uGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4.DisplayLayout.Override.RowAppearance = appearance18;
            this.uGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance19;
            this.uGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4.Location = new System.Drawing.Point(20, 8);
            this.uGrid4.Name = "uGrid4";
            this.uGrid4.Size = new System.Drawing.Size(12, 20);
            this.uGrid4.TabIndex = 30;
            this.uGrid4.Text = "ultraGrid2";
            this.uGrid4.Visible = false;
            this.uGrid4.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid4_AfterCellUpdate);
            this.uGrid4.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid4_ClickCellButton);
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uButtonDeleteRow2);
            this.uGroupBox3.Controls.Add(this.uGrid2);
            this.uGroupBox3.Location = new System.Drawing.Point(14, 420);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1042, 212);
            this.uGroupBox3.TabIndex = 6;
            // 
            // uButtonDeleteRow2
            // 
            this.uButtonDeleteRow2.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow2.Name = "uButtonDeleteRow2";
            this.uButtonDeleteRow2.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow2.TabIndex = 25;
            this.uButtonDeleteRow2.Text = "ultraButton1";
            this.uButtonDeleteRow2.Click += new System.EventHandler(this.uButtonDeleteRow2_Click);
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance7;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance36.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance36.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance36.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance36;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance37;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance38.BackColor2 = System.Drawing.SystemColors.Control;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance38;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance39;
            appearance40.BackColor = System.Drawing.SystemColors.Highlight;
            appearance40.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance40;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance41;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            appearance42.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance42;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance43.BackColor = System.Drawing.SystemColors.Control;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance43;
            appearance44.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance44;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            appearance45.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance45;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance46.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance46;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 59);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1020, 143);
            this.uGrid2.TabIndex = 18;
            this.uGrid2.Text = "ultraGrid2";
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGrid2.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_CellListSelect);
            this.uGrid2.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_ClickCellButton);
            this.uGrid2.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_CellChange);
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uButtonDown);
            this.uGroupBox2.Controls.Add(this.uButtonDeleteRow1);
            this.uGroupBox2.Controls.Add(this.uGrid1);
            this.uGroupBox2.Location = new System.Drawing.Point(14, 206);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(518, 212);
            this.uGroupBox2.TabIndex = 5;
            // 
            // uButtonDown
            // 
            this.uButtonDown.Location = new System.Drawing.Point(420, 28);
            this.uButtonDown.Name = "uButtonDown";
            this.uButtonDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonDown.TabIndex = 25;
            this.uButtonDown.Text = "ultraButton1";
            this.uButtonDown.Click += new System.EventHandler(this.uButtonDown_Click);
            // 
            // uButtonDeleteRow1
            // 
            this.uButtonDeleteRow1.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow1.Name = "uButtonDeleteRow1";
            this.uButtonDeleteRow1.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow1.TabIndex = 23;
            this.uButtonDeleteRow1.Text = "ultraButton1";
            this.uButtonDeleteRow1.Click += new System.EventHandler(this.uButtonDeleteRow1_Click);
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance20;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            appearance27.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance27;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance28.BackColor = System.Drawing.SystemColors.Control;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance28;
            appearance29.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance30;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance31;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 59);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(496, 140);
            this.uGrid1.TabIndex = 18;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGrid1.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid1_ClickCell);
            this.uGrid1.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_ClickCellButton);
            this.uGrid1.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_CellChange);
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uTextGRWComment);
            this.uGroupBox1.Controls.Add(this.uLabelGRWComment);
            this.uGroupBox1.Controls.Add(this.uTextVersionNum);
            this.uGroupBox1.Controls.Add(this.uLabelVersionNum);
            this.uGroupBox1.Controls.Add(this.uTextMakeDeptName);
            this.uGroupBox1.Controls.Add(this.uLabelMakeDeptName);
            this.uGroupBox1.Controls.Add(this.uTextMakeUserName);
            this.uGroupBox1.Controls.Add(this.uLabelMakeUserName);
            this.uGroupBox1.Controls.Add(this.uTextPadsize);
            this.uGroupBox1.Controls.Add(this.uLabelPadsize);
            this.uGroupBox1.Controls.Add(this.uTextPackage);
            this.uGroupBox1.Controls.Add(this.uLabelPackage);
            this.uGroupBox1.Controls.Add(this.uTextChipsize);
            this.uGroupBox1.Controls.Add(this.uLabelChipSize);
            this.uGroupBox1.Controls.Add(this.uTextReturnReason);
            this.uGroupBox1.Controls.Add(this.ulabelReturnReason);
            this.uGroupBox1.Controls.Add(this.uTextApproveUserName);
            this.uGroupBox1.Controls.Add(this.uTextApproveUserID);
            this.uGroupBox1.Controls.Add(this.uLabelApproveUser);
            this.uGroupBox1.Controls.Add(this.uTextEtc);
            this.uGroupBox1.Controls.Add(this.uLabelEtc);
            this.uGroupBox1.Controls.Add(this.uDateCompleteDate);
            this.uGroupBox1.Controls.Add(this.ultraLabel2);
            this.uGroupBox1.Controls.Add(this.uDateApplyToDate);
            this.uGroupBox1.Controls.Add(this.uDateApplyFromDate);
            this.uGroupBox1.Controls.Add(this.uLabelApplyDate);
            this.uGroupBox1.Controls.Add(this.uTextDocKeyword);
            this.uGroupBox1.Controls.Add(this.uLabelDocKeyword);
            this.uGroupBox1.Controls.Add(this.uTextTitle);
            this.uGroupBox1.Controls.Add(this.uLabelTitle);
            this.uGroupBox1.Controls.Add(this.uTextDocState);
            this.uGroupBox1.Controls.Add(this.uLabelDocState);
            this.uGroupBox1.Controls.Add(this.uDateCreateDate);
            this.uGroupBox1.Controls.Add(this.uLabelCreateDate);
            this.uGroupBox1.Controls.Add(this.uTextCreateUserName);
            this.uGroupBox1.Controls.Add(this.uTextCreateUserID);
            this.uGroupBox1.Controls.Add(this.uLabelCreateUser);
            this.uGroupBox1.Controls.Add(this.uComboSmall);
            this.uGroupBox1.Controls.Add(this.uLabelSmall);
            this.uGroupBox1.Controls.Add(this.uComboMiddle);
            this.uGroupBox1.Controls.Add(this.uLabelMiddle);
            this.uGroupBox1.Controls.Add(this.uComboLarge);
            this.uGroupBox1.Controls.Add(this.uLabelLarge);
            this.uGroupBox1.Controls.Add(this.uComboProcess);
            this.uGroupBox1.Controls.Add(this.uComboPlant);
            this.uGroupBox1.Controls.Add(this.uLabelPlant);
            this.uGroupBox1.Controls.Add(this.uTextStandardDocNo);
            this.uGroupBox1.Controls.Add(this.uLabelStandardDocNo);
            this.uGroupBox1.Location = new System.Drawing.Point(13, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1039, 164);
            this.uGroupBox1.TabIndex = 3;
            // 
            // uTextGRWComment
            // 
            this.uTextGRWComment.Location = new System.Drawing.Point(432, 60);
            this.uTextGRWComment.Name = "uTextGRWComment";
            this.uTextGRWComment.Size = new System.Drawing.Size(552, 21);
            this.uTextGRWComment.TabIndex = 8;
            // 
            // uLabelGRWComment
            // 
            this.uLabelGRWComment.Location = new System.Drawing.Point(328, 60);
            this.uLabelGRWComment.Name = "uLabelGRWComment";
            this.uLabelGRWComment.Size = new System.Drawing.Size(100, 20);
            this.uLabelGRWComment.TabIndex = 52;
            this.uLabelGRWComment.Text = "ultraLabel1";
            // 
            // uTextVersionNum
            // 
            this.uTextVersionNum.Location = new System.Drawing.Point(897, 12);
            this.uTextVersionNum.Name = "uTextVersionNum";
            this.uTextVersionNum.Size = new System.Drawing.Size(87, 21);
            this.uTextVersionNum.TabIndex = 6;
            // 
            // uLabelVersionNum
            // 
            this.uLabelVersionNum.Location = new System.Drawing.Point(1008, 52);
            this.uLabelVersionNum.Name = "uLabelVersionNum";
            this.uLabelVersionNum.Size = new System.Drawing.Size(16, 20);
            this.uLabelVersionNum.TabIndex = 50;
            this.uLabelVersionNum.Text = "ultraLabel4";
            this.uLabelVersionNum.Visible = false;
            // 
            // uTextMakeDeptName
            // 
            this.uTextMakeDeptName.Location = new System.Drawing.Point(116, 108);
            this.uTextMakeDeptName.Name = "uTextMakeDeptName";
            this.uTextMakeDeptName.Size = new System.Drawing.Size(204, 21);
            this.uTextMakeDeptName.TabIndex = 999;
            // 
            // uLabelMakeDeptName
            // 
            this.uLabelMakeDeptName.Location = new System.Drawing.Point(644, 64);
            this.uLabelMakeDeptName.Name = "uLabelMakeDeptName";
            this.uLabelMakeDeptName.Size = new System.Drawing.Size(32, 20);
            this.uLabelMakeDeptName.TabIndex = 48;
            this.uLabelMakeDeptName.Text = "ultraLabel4";
            this.uLabelMakeDeptName.Visible = false;
            // 
            // uTextMakeUserName
            // 
            this.uTextMakeUserName.Location = new System.Drawing.Point(1004, 136);
            this.uTextMakeUserName.Name = "uTextMakeUserName";
            this.uTextMakeUserName.Size = new System.Drawing.Size(24, 21);
            this.uTextMakeUserName.TabIndex = 16;
            this.uTextMakeUserName.Visible = false;
            // 
            // uLabelMakeUserName
            // 
            this.uLabelMakeUserName.Location = new System.Drawing.Point(988, 136);
            this.uLabelMakeUserName.Name = "uLabelMakeUserName";
            this.uLabelMakeUserName.Size = new System.Drawing.Size(12, 20);
            this.uLabelMakeUserName.TabIndex = 46;
            this.uLabelMakeUserName.Text = "ultraLabel4";
            this.uLabelMakeUserName.Visible = false;
            // 
            // uTextPadsize
            // 
            this.uTextPadsize.Location = new System.Drawing.Point(870, 132);
            this.uTextPadsize.Name = "uTextPadsize";
            this.uTextPadsize.Size = new System.Drawing.Size(114, 21);
            this.uTextPadsize.TabIndex = 16;
            // 
            // uLabelPadsize
            // 
            this.uLabelPadsize.Location = new System.Drawing.Point(768, 132);
            this.uLabelPadsize.Name = "uLabelPadsize";
            this.uLabelPadsize.Size = new System.Drawing.Size(100, 20);
            this.uLabelPadsize.TabIndex = 44;
            this.uLabelPadsize.Text = "ultraLabel4";
            // 
            // uTextPackage
            // 
            this.uTextPackage.Location = new System.Drawing.Point(650, 132);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.Size = new System.Drawing.Size(114, 21);
            this.uTextPackage.TabIndex = 15;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(548, 132);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackage.TabIndex = 42;
            this.uLabelPackage.Text = "ultraLabel3";
            // 
            // uTextChipsize
            // 
            this.uTextChipsize.Location = new System.Drawing.Point(432, 131);
            this.uTextChipsize.Name = "uTextChipsize";
            this.uTextChipsize.Size = new System.Drawing.Size(114, 21);
            this.uTextChipsize.TabIndex = 14;
            // 
            // uLabelChipSize
            // 
            this.uLabelChipSize.Location = new System.Drawing.Point(328, 131);
            this.uLabelChipSize.Name = "uLabelChipSize";
            this.uLabelChipSize.Size = new System.Drawing.Size(100, 20);
            this.uLabelChipSize.TabIndex = 40;
            this.uLabelChipSize.Text = "ultraLabel1";
            // 
            // uTextReturnReason
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnReason.Appearance = appearance5;
            this.uTextReturnReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnReason.Location = new System.Drawing.Point(1012, 148);
            this.uTextReturnReason.Name = "uTextReturnReason";
            this.uTextReturnReason.ReadOnly = true;
            this.uTextReturnReason.Size = new System.Drawing.Size(12, 21);
            this.uTextReturnReason.TabIndex = 17;
            this.uTextReturnReason.Visible = false;
            // 
            // ulabelReturnReason
            // 
            this.ulabelReturnReason.Location = new System.Drawing.Point(1008, 148);
            this.ulabelReturnReason.Name = "ulabelReturnReason";
            this.ulabelReturnReason.Size = new System.Drawing.Size(8, 20);
            this.ulabelReturnReason.TabIndex = 38;
            this.ulabelReturnReason.Text = "ultraLabel1";
            this.ulabelReturnReason.Visible = false;
            // 
            // uTextApproveUserName
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Appearance = appearance6;
            this.uTextApproveUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Location = new System.Drawing.Point(1012, 8);
            this.uTextApproveUserName.Name = "uTextApproveUserName";
            this.uTextApproveUserName.ReadOnly = true;
            this.uTextApproveUserName.Size = new System.Drawing.Size(20, 21);
            this.uTextApproveUserName.TabIndex = 13;
            this.uTextApproveUserName.Visible = false;
            // 
            // uTextApproveUserID
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextApproveUserID.Appearance = appearance3;
            this.uTextApproveUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance2.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextApproveUserID.ButtonsRight.Add(editorButton1);
            this.uTextApproveUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextApproveUserID.Location = new System.Drawing.Point(1000, 8);
            this.uTextApproveUserID.Name = "uTextApproveUserID";
            this.uTextApproveUserID.Size = new System.Drawing.Size(20, 21);
            this.uTextApproveUserID.TabIndex = 12;
            this.uTextApproveUserID.Visible = false;
            this.uTextApproveUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextApproveUserID_KeyDown);
            this.uTextApproveUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextApproveUserID_EditorButtonClick);
            // 
            // uLabelApproveUser
            // 
            this.uLabelApproveUser.Location = new System.Drawing.Point(988, 8);
            this.uLabelApproveUser.Name = "uLabelApproveUser";
            this.uLabelApproveUser.Size = new System.Drawing.Size(20, 20);
            this.uLabelApproveUser.TabIndex = 35;
            this.uLabelApproveUser.Text = "ultraLabel1";
            this.uLabelApproveUser.Visible = false;
            // 
            // uTextEtc
            // 
            this.uTextEtc.Location = new System.Drawing.Point(432, 84);
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.Size = new System.Drawing.Size(552, 21);
            this.uTextEtc.TabIndex = 9;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(328, 84);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc.TabIndex = 33;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uDateCompleteDate
            // 
            this.uDateCompleteDate.Location = new System.Drawing.Point(996, 100);
            this.uDateCompleteDate.Name = "uDateCompleteDate";
            this.uDateCompleteDate.Size = new System.Drawing.Size(32, 21);
            this.uDateCompleteDate.TabIndex = 30;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(216, 135);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel2.TabIndex = 28;
            this.ultraLabel2.Text = "~";
            // 
            // uDateApplyToDate
            // 
            this.uDateApplyToDate.Location = new System.Drawing.Point(232, 131);
            this.uDateApplyToDate.Name = "uDateApplyToDate";
            this.uDateApplyToDate.Size = new System.Drawing.Size(88, 21);
            this.uDateApplyToDate.TabIndex = 13;
            // 
            // uDateApplyFromDate
            // 
            this.uDateApplyFromDate.Location = new System.Drawing.Point(116, 131);
            this.uDateApplyFromDate.Name = "uDateApplyFromDate";
            this.uDateApplyFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateApplyFromDate.TabIndex = 12;
            // 
            // uLabelApplyDate
            // 
            this.uLabelApplyDate.Location = new System.Drawing.Point(12, 131);
            this.uLabelApplyDate.Name = "uLabelApplyDate";
            this.uLabelApplyDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelApplyDate.TabIndex = 25;
            this.uLabelApplyDate.Text = "ultraLabel1";
            // 
            // uTextDocKeyword
            // 
            this.uTextDocKeyword.Location = new System.Drawing.Point(651, 108);
            this.uTextDocKeyword.Name = "uTextDocKeyword";
            this.uTextDocKeyword.Size = new System.Drawing.Size(114, 21);
            this.uTextDocKeyword.TabIndex = 11;
            // 
            // uLabelDocKeyword
            // 
            this.uLabelDocKeyword.Location = new System.Drawing.Point(548, 108);
            this.uLabelDocKeyword.Name = "uLabelDocKeyword";
            this.uLabelDocKeyword.Size = new System.Drawing.Size(100, 20);
            this.uLabelDocKeyword.TabIndex = 23;
            this.uLabelDocKeyword.Text = "ultraLabel1";
            // 
            // uTextTitle
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTitle.Appearance = appearance1;
            this.uTextTitle.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTitle.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextTitle.Location = new System.Drawing.Point(432, 36);
            this.uTextTitle.Name = "uTextTitle";
            this.uTextTitle.Size = new System.Drawing.Size(552, 21);
            this.uTextTitle.TabIndex = 7;
            // 
            // uLabelTitle
            // 
            this.uLabelTitle.Location = new System.Drawing.Point(328, 36);
            this.uLabelTitle.Name = "uLabelTitle";
            this.uLabelTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelTitle.TabIndex = 21;
            this.uLabelTitle.Text = "ultraLabel1";
            // 
            // uTextDocState
            // 
            appearance47.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Appearance = appearance47;
            this.uTextDocState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Location = new System.Drawing.Point(432, 108);
            this.uTextDocState.Name = "uTextDocState";
            this.uTextDocState.ReadOnly = true;
            this.uTextDocState.Size = new System.Drawing.Size(114, 21);
            this.uTextDocState.TabIndex = 10;
            // 
            // uLabelDocState
            // 
            this.uLabelDocState.Location = new System.Drawing.Point(328, 108);
            this.uLabelDocState.Name = "uLabelDocState";
            this.uLabelDocState.Size = new System.Drawing.Size(100, 20);
            this.uLabelDocState.TabIndex = 19;
            this.uLabelDocState.Text = "ultraLabel1";
            // 
            // uDateCreateDate
            // 
            this.uDateCreateDate.Location = new System.Drawing.Point(1004, 112);
            this.uDateCreateDate.Name = "uDateCreateDate";
            this.uDateCreateDate.Size = new System.Drawing.Size(20, 21);
            this.uDateCreateDate.TabIndex = 11;
            this.uDateCreateDate.Visible = false;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(988, 112);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(20, 20);
            this.uLabelCreateDate.TabIndex = 17;
            this.uLabelCreateDate.Text = "ultraLabel1";
            this.uLabelCreateDate.Visible = false;
            // 
            // uTextCreateUserName
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Appearance = appearance32;
            this.uTextCreateUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Location = new System.Drawing.Point(217, 84);
            this.uTextCreateUserName.Name = "uTextCreateUserName";
            this.uTextCreateUserName.ReadOnly = true;
            this.uTextCreateUserName.Size = new System.Drawing.Size(103, 21);
            this.uTextCreateUserName.TabIndex = 999;
            // 
            // uTextCreateUserID
            // 
            appearance33.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCreateUserID.Appearance = appearance33;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance34.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance34;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCreateUserID.ButtonsRight.Add(editorButton2);
            this.uTextCreateUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010;
            this.uTextCreateUserID.Location = new System.Drawing.Point(116, 84);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 4;
            this.uTextCreateUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCreateUserID_KeyDown);
            this.uTextCreateUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCreateUserID_EditorButtonClick);
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(12, 84);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateUser.TabIndex = 14;
            this.uLabelCreateUser.Text = "ultraLabel1";
            // 
            // uComboSmall
            // 
            this.uComboSmall.Location = new System.Drawing.Point(116, 60);
            this.uComboSmall.Name = "uComboSmall";
            this.uComboSmall.Size = new System.Drawing.Size(204, 21);
            this.uComboSmall.TabIndex = 3;
            this.uComboSmall.Text = "ultraComboEditor1";
            // 
            // uLabelSmall
            // 
            this.uLabelSmall.Location = new System.Drawing.Point(12, 60);
            this.uLabelSmall.Name = "uLabelSmall";
            this.uLabelSmall.Size = new System.Drawing.Size(100, 20);
            this.uLabelSmall.TabIndex = 12;
            this.uLabelSmall.Text = "ultraLabel1";
            // 
            // uComboMiddle
            // 
            this.uComboMiddle.Location = new System.Drawing.Point(116, 36);
            this.uComboMiddle.Name = "uComboMiddle";
            this.uComboMiddle.Size = new System.Drawing.Size(204, 21);
            this.uComboMiddle.TabIndex = 2;
            this.uComboMiddle.Text = "ultraComboEditor1";
            this.uComboMiddle.ValueChanged += new System.EventHandler(this.uComboMiddle_ValueChanged);
            // 
            // uLabelMiddle
            // 
            this.uLabelMiddle.Location = new System.Drawing.Point(12, 36);
            this.uLabelMiddle.Name = "uLabelMiddle";
            this.uLabelMiddle.Size = new System.Drawing.Size(100, 20);
            this.uLabelMiddle.TabIndex = 10;
            this.uLabelMiddle.Text = "ultraLabel1";
            // 
            // uComboLarge
            // 
            this.uComboLarge.Location = new System.Drawing.Point(116, 12);
            this.uComboLarge.Name = "uComboLarge";
            this.uComboLarge.Size = new System.Drawing.Size(204, 21);
            this.uComboLarge.TabIndex = 1;
            this.uComboLarge.Text = "ultraComboEditor1";
            this.uComboLarge.ValueChanged += new System.EventHandler(this.uComboLarge_ValueChanged);
            // 
            // uLabelLarge
            // 
            this.uLabelLarge.Location = new System.Drawing.Point(12, 12);
            this.uLabelLarge.Name = "uLabelLarge";
            this.uLabelLarge.Size = new System.Drawing.Size(100, 20);
            this.uLabelLarge.TabIndex = 8;
            this.uLabelLarge.Text = "ultraLabel1";
            // 
            // uComboProcess
            // 
            this.uComboProcess.Location = new System.Drawing.Point(992, 76);
            this.uComboProcess.Name = "uComboProcess";
            this.uComboProcess.Size = new System.Drawing.Size(34, 21);
            this.uComboProcess.TabIndex = 7;
            this.uComboProcess.Text = "ultraComboEditor1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(1004, 156);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(12, 21);
            this.uComboPlant.TabIndex = 7;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.Visible = false;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(992, 156);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(10, 20);
            this.uLabelPlant.TabIndex = 4;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // uTextStandardDocNo
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardDocNo.Appearance = appearance60;
            this.uTextStandardDocNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardDocNo.Location = new System.Drawing.Point(432, 12);
            this.uTextStandardDocNo.Name = "uTextStandardDocNo";
            this.uTextStandardDocNo.ReadOnly = true;
            this.uTextStandardDocNo.Size = new System.Drawing.Size(464, 21);
            this.uTextStandardDocNo.TabIndex = 5;
            this.uTextStandardDocNo.TabStop = false;
            // 
            // uLabelStandardDocNo
            // 
            this.uLabelStandardDocNo.Location = new System.Drawing.Point(328, 12);
            this.uLabelStandardDocNo.Name = "uLabelStandardDocNo";
            this.uLabelStandardDocNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelStandardDocNo.TabIndex = 0;
            this.uLabelStandardDocNo.Text = "ultraLabel1";
            // 
            // frmISO0003C
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0003C";
            this.Load += new System.EventHandler(this.frmISO0003C_Load);
            this.Activated += new System.EventHandler(this.frmISO0003C_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISO0003C_FormClosing);
            this.Resize += new System.EventHandler(this.frmISO0003C_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox5)).EndInit();
            this.uGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadsize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipsize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardDocNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckVersion;
        private Infragistics.Win.Misc.UltraLabel uLabelVersionCheck;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchRegToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchRegFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRegDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStandardNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchSmall;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMiddle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLarge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLarge;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHeader;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox5;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVersionNum;
        private Infragistics.Win.Misc.UltraLabel uLabelVersionNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeDeptName;
        private Infragistics.Win.Misc.UltraLabel uLabelMakeDeptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelMakeUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPadsize;
        private Infragistics.Win.Misc.UltraLabel uLabelPadsize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChipsize;
        private Infragistics.Win.Misc.UltraLabel uLabelChipSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnReason;
        private Infragistics.Win.Misc.UltraLabel ulabelReturnReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelApproveUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCompleteDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateApplyToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateApplyFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelApplyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocKeyword;
        private Infragistics.Win.Misc.UltraLabel uLabelDocKeyword;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocState;
        private Infragistics.Win.Misc.UltraLabel uLabelDocState;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCreateDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelSmall;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelMiddle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboLarge;
        private Infragistics.Win.Misc.UltraLabel uLabelLarge;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcess;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardDocNo;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardDocNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchTitle;
        private Infragistics.Win.Misc.UltraButton uButtonDown;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRWComment;
        private Infragistics.Win.Misc.UltraLabel uLabelGRWComment;
    }
}