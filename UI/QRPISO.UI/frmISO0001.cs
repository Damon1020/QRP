﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서 관리                                         */
/* 프로그램ID   : frmISO0001.cs                                         */
/* 프로그램명   : 표준문서 대/중분류 정보                               */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPISO.UI
{
    public partial class frmISO0001 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmISO0001()
        {
            InitializeComponent();
        }

        private void frmISO0001_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISO0001_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 대/중분류 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitValue();

            //Search();

            // Contents 그룹박스 접은 상태로

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLargeClassifyCode, "대분류코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLargeClassifyName, "대분류명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLargeClassifyNameCh, "대분류명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLargeClassifyNameEn, "대분류명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAutoStdNumberFlag, "자동채번여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelGrwFlag, "전자결제여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장 콤보박스
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                    , Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

                // 사용여부 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                dt = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboUseFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "T", "", "선택"
                    , "ComCode", "ComCodeName", dt);

                // 자동채번여부 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommon = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommon);

                DataTable dtAuto = clsCommon.mfReadCommonCode("C0026", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboAutoStdNumberFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtAuto);


                //전자결제 여부 Flag
                wCombo.mfSetComboEditor(this.uComboGrwFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dtAuto);


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 대분류 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeCode", "대분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeName", "대분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeNameCh", "대분류명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "LTypeNameEn", "대분류명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "AutoStdNumberFlag", "자동채번여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "GrwFlag", "전자결제 여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 중분류 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGrid2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "MTypeCode", "중분류코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "MTypeName", "중분류명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "MTypeNameCh", "중분류명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "MTypeNameEn", "중분류명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // DropDown 설정
                // 대분류 공장 열

                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtISODocLType = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtISODocLType);

                // 사용여부 열

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);
                dtISODocLType = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid2, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtISODocLType);

                // AddRow
                wGrid.mfAddRowGrid(this.uGrid2, 0);

                // Set FontSize
                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                // 그리드 컬럼에 콤보박스 추가
                wGrid.mfSetGridColumnValueList(uGrid1, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtISODocLType);
                wGrid.mfSetGridColumnValueList(uGrid2, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtISODocLType);

                // 공백줄 추가
                wGrid.mfAddRowGrid(uGrid2, 0);

                // Set FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화

        /// </summary>
        private void InitValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");  //"선택";
                this.uTextLargeClassifyCode.Text = "";
                this.uTextLargeClassifyCode.MaxLength = 10;
                this.uTextLargeClassifyName.Text = "";
                this.uTextLargeClassifyName.MaxLength = 50;  
                this.uTextLargeClassifyNameCh.Text = "";
                this.uTextLargeClassifyNameCh.MaxLength = 50;
                this.uTextLargeClassifyNameEn.Text = "";
                this.uTextLargeClassifyNameEn.MaxLength = 50;
                this.uComboUseFlag.Value = "T";



                //컴퍼넌트 입력불가 해제
                this.uComboPlant.ReadOnly = false;
                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uTextLargeClassifyCode.ReadOnly = false;
                this.uTextLargeClassifyCode.Appearance.BackColor = Color.PowderBlue;
                this.uTextLargeClassifyName.ReadOnly = false;
                this.uTextLargeClassifyNameCh.ReadOnly = false;
                this.uTextLargeClassifyNameEn.ReadOnly = false;
                this.uComboUseFlag.ReadOnly = false;

                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void Search()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                QRPISO.BL.ISODOC.ISODocLType isoDocLType = new QRPISO.BL.ISODOC.ISODocLType();
                brwChannel.mfCredentials(isoDocLType);

                InitValue();

                String strPlantCode = this.uComboPlant.Value.ToString();


                for (int i = 0; i < uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.Rows[i].Cells["MTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid2.Rows[i].Cells["MTypeCode"].Appearance.BackColor = Color.Gainsboro;
                }



                //함수호출
                DataTable dt = isoDocLType.mfReadISODocLType(m_resSys.GetString("SYS_LANG"));
                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();

                if (uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                       , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                QRPISO.BL.ISODOC.ISODocLType isoDocLType = new QRPISO.BL.ISODOC.ISODocLType();
                brwChannel.mfCredentials(isoDocLType);

                InitValue();

                String strPlantCode = this.uComboPlant.Value.ToString();


                for (int i = 0; i < uGrid2.Rows.Count; i++)
                {
                    this.uGrid2.Rows[i].Cells["MTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid2.Rows[i].Cells["MTypeCode"].Appearance.BackColor = Color.Gainsboro;
                }



                //함수호출
                DataTable dt = isoDocLType.mfReadISODocLType(m_resSys.GetString("SYS_LANG"));
                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }

                WinGrid grd = new WinGrid();
                grd.mfSetAutoResizeColWidth(this.uGrid1, 0);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                       , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = false;
                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();


                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001230", "M001028", Infragistics.Win.HAlign.Right);

                }
                else
                {
                    //BL 호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType lType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(lType);


                    DataTable dtISODocLType = new DataTable();
                    DataTable dtSaveMType = new DataTable();
                    DataTable dtDelMType = new DataTable();
                    DataRow row;

                    DataTable dtCheckLType = lType.mfReadISODocLTypeCombo(this.uComboPlant.Value.ToString(),m_resSys.GetString("SYS_LANG"));
                    if (this.uComboPlant.ReadOnly == false)
                    {
                        for (int i = 0; i < dtCheckLType.Rows.Count; i++)
                        {
                            if (dtCheckLType.Rows[i]["LTypeCode"].ToString() == this.uTextLargeClassifyCode.Text)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000371", "M000848", Infragistics.Win.HAlign.Center);

                                this.uTextLargeClassifyCode.Focus();

                                return;
                            }
                        }
                    }

                    //필수 입력 사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Center);

                        //Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uTextLargeClassifyCode.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M000372", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextLargeClassifyCode.Focus();
                        return;
                    }
                    else if (this.uTextLargeClassifyName.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230"
                                        , "M001485", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextLargeClassifyName.Focus();
                        return;
                    }
                    else if (this.uComboAutoStdNumberFlag.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M000963", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboAutoStdNumberFlag.DropDown();
                        return;
                    }
                    else if(this.uComboGrwFlag.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001230", "M001062", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboGrwFlag.DropDown();
                        return;
                    }
                    else
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////


                        dtISODocLType = lType.mfSetDateInfo();

                        row = dtISODocLType.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["LTypeCode"] = this.uTextLargeClassifyCode.Text;
                        row["LTypeName"] = this.uTextLargeClassifyName.Text;
                        row["LTypeNameCh"] = this.uTextLargeClassifyNameCh.Text;
                        row["LTypeNameEn"] = this.uTextLargeClassifyNameEn.Text;
                        row["AutoStdNumberFlag"] = this.uComboAutoStdNumberFlag.Value.ToString();
                        row["GrwFlag"] = this.uComboGrwFlag.Value.ToString();
                        row["UseFlag"] = this.uComboUseFlag.Value.ToString();
                        dtISODocLType.Rows.Add(row);

                        //////중분류////////
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                        QRPISO.BL.ISODOC.ISODocMType mType = new QRPISO.BL.ISODOC.ISODocMType();
                        brwChannel.mfCredentials(mType);

                        dtSaveMType = mType.mfSetDateInfo();
                        dtDelMType = mType.mfSetDateInfo();

                        if (this.uComboPlant.ReadOnly == true)
                        {
                            DataTable dtCheckMType = mType.mfReadISODocMTypeCombo(this.uComboPlant.Value.ToString(), this.uTextLargeClassifyCode.Text, m_resSys.GetString("SYS_LANG"));
                            for (int i = 0; i < dtCheckMType.Rows.Count; i++)
                            {
                                for (int j = 0; j < this.uGrid2.Rows.Count; j++)
                                {
                                    if(this.uGrid2.Rows[j].Cells["MTypeCode"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                                    {
                                        if (dtCheckMType.Rows[i]["MTypeCode"].ToString() == this.uGrid2.Rows[j].Cells["MTypeCode"].Value.ToString())
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001125", m_resSys.GetString("SYS_LANG")), this.uGrid2.Rows[j].RowSelectorNumber + msg.GetMessge_Text("M000572", m_resSys.GetString("SYS_LANG")), Infragistics.Win.HAlign.Center);
                                            // Focus Cell
                                            this.uGrid2.ActiveCell = this.uGrid2.Rows[j].Cells["MTypeCode"];
                                            this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                    }
                                }
                            }
                        }

                        for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                        {
                            this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                            if (this.uGrid2.Rows[i].Hidden == false)
                            {
                                if (this.uGrid2.Rows[i].Cells["MTypeCode"].Value.ToString() == "")
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001230", m_resSys.GetString("SYS_LANG")), this.uGrid2.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000573", m_resSys.GetString("SYS_LANG")), Infragistics.Win.HAlign.Center);

                                    // Focus Cell
                                    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["MTypeCode"];
                                    this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                else if (this.uGrid2.Rows[i].Cells["MTypeName"].Value.ToString() == "")
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001230", m_resSys.GetString("SYS_LANG")), this.uGrid2.Rows[i].RowSelectorNumber + "번째 행의 중분류명을 입력해주세요", Infragistics.Win.HAlign.Center);

                                    // Focus Cell
                                    this.uGrid2.ActiveCell = this.uGrid2.Rows[i].Cells["MTypeName"];
                                    this.uGrid2.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                else
                                {
                                    row = dtSaveMType.NewRow();
                                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                                    row["LTypeCode"] = this.uTextLargeClassifyCode.Text;
                                    row["MTypeCode"] = this.uGrid2.Rows[i].Cells["MTypeCode"].Value.ToString();
                                    row["MTypeName"] = this.uGrid2.Rows[i].Cells["MTypeName"].Value.ToString();
                                    row["MTypeNameCh"] = this.uGrid2.Rows[i].Cells["MTypeNameCh"].Value.ToString();
                                    row["MTypeNameEn"] = this.uGrid2.Rows[i].Cells["MTypeNameEn"].Value.ToString();
                                    //row["Seq"] = this.uGrid2.Rows[i].RowSelectorNumber;
                                    row["UseFlag"] = this.uGrid2.Rows[i].Cells["UseFlag"].Value.ToString();
                                    dtSaveMType.Rows.Add(row);
                                }
                            }
                            else
                            {
                                row = dtDelMType.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                row["LTypeCode"] = this.uTextLargeClassifyCode.Text;
                                row["MTypeCode"] = this.uGrid2.Rows[i].Cells["MTypeCode"].Value.ToString();
                                dtDelMType.Rows.Add(row);
                            }
                        }

                        if (dtISODocLType.Rows.Count > 0)
                        {
                            for (int k = 0; k < dtSaveMType.Rows.Count; k++)
                            {
                                for (int j = 0; j < dtSaveMType.Rows.Count; j++)
                                {
                                    if (k != j)
                                    {
                                        if (dtSaveMType.Rows[k]["MTypeCode"].ToString() == dtSaveMType.Rows[j]["MTypeCode"].ToString())
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                , "M001135", "M001037", "M000959", Infragistics.Win.HAlign.Right);

                                            return;
                                        }
                                    }
                                }
                            }
                            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                            {
                                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                Thread t1 = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                //처리로직
                                //저장 함수 호출
                                string rtMSG = lType.mfSaveISODocLType(dtISODocLType, m_resSys.GetString("SYSY_USERID"), m_resSys.GetString("SYSY_USERIP"), dtSaveMType, dtDelMType);

                                //decoding
                                TransErrRtn ErrRtn = new TransErrRtn();
                                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                                //처리로직 끝

                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                //처리결과에 따른 메세지 박스
                                System.Windows.Forms.DialogResult result;
                                if (ErrRtn.ErrNum == 0)
                                {
                                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001037", "M000930",
                                                       Infragistics.Win.HAlign.Right);
                                    InitValue();
                                    if (this.uGroupBoxContentsArea.Expanded == true)
                                    {
                                        this.uGroupBoxContentsArea.Expanded = false;
                                    }
                                    mfSearch();
                                }
                                else
                                {
                                    string strMes = "";

                                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                                        strMes = msg.GetMessge_Text("M000953", m_resSys.GetString("SYS_LANG"));
                                    else
                                        strMes = ErrRtn.ErrMessage;

                                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG")), strMes,
                                                        Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                }
            }

            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                QRPISO.BL.ISODOC.ISODocLType LType = new QRPISO.BL.ISODOC.ISODocLType();
                brwChannel.mfCredentials(LType);

                String strPlantCode = this.uComboPlant.Value.ToString();
                String strLTypeCode = this.uTextLargeClassifyCode.Text;

                DataTable dtCheck = LType.mfReadISODocHLType(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));


                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000632", "M000639", Infragistics.Win.HAlign.Right);

                }
                else if (dtCheck.Rows.Count > 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M000624", "M001205", Infragistics.Win.HAlign.Center);


                    return;
                }
                else
                {
                    //BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType isoDocLType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(isoDocLType);

                    DataTable dt = isoDocLType.mfSetDateInfo();
                    DataRow row;

                    row = dt.NewRow();
                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                    row["LTypeCode"] = this.uTextLargeClassifyCode.Text;

                    dt.Rows.Add(row);

                    if (dt.Rows.Count > 0)
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000650", "M000687",
                                               Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string rtMSG = isoDocLType.mfDeleteISODocLType(dt);

                            //Decoding
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            DialogResult DResult = new DialogResult();

                            if (ErrRtn.ErrNum == 0)
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001135", "M000638", "M000926",
                                              Infragistics.Win.HAlign.Right);
                                InitValue();
                                mfSearch();
                            }
                            else
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001135", "M000638", "M000923",
                                              Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                    
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우


                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화

                InitValue();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                //처리로직
            WinGrid grd = new WinGrid();
            //엑셀저장함수호출
            grd.mfDownLoadGridToExcel(this.uGrid1);
            }
            catch
            {
            }
            finally
            {
            }
        }

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 110);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 800;

                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }


        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;

                    e.Cell.Row.Fixed = true;
                }



                // 그리드의 값을 컴포넌트에 대입
                this.uComboPlant.Value = e.Cell.Row.Cells["PlantCode"].Value;
                this.uTextLargeClassifyCode.Text = e.Cell.Row.Cells["LTypeCode"].Value.ToString();
                this.uTextLargeClassifyName.Text = e.Cell.Row.Cells["LTypeName"].Value.ToString();
                this.uTextLargeClassifyNameCh.Text = e.Cell.Row.Cells["LTypeNameCh"].Value.ToString();
                this.uTextLargeClassifyNameEn.Text = e.Cell.Row.Cells["LTypeNameEn"].Value.ToString();
                this.uComboAutoStdNumberFlag.Value = e.Cell.Row.Cells["AutoStdNumberFlag"].Value.ToString();
                this.uComboGrwFlag.Value = e.Cell.Row.Cells["GrwFlag"].Value.ToString();

                //컴퍼넌트값 수정 불가
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                this.uTextLargeClassifyCode.ReadOnly = true;
                this.uTextLargeClassifyCode.Appearance.BackColor = Color.Gainsboro;

                SearchMType();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 중분류 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchMType()
        {
            try
            {
                //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();
                String strLTypeCode = this.uTextLargeClassifyCode.Text;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                QRPISO.BL.ISODOC.ISODocMType MTypeCode = new QRPISO.BL.ISODOC.ISODocMType();
                brwChannel.mfCredentials(MTypeCode);

                DataTable dt = MTypeCode.mfReadISODocMType(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                this.uGrid2.DataSource = dt;
                this.uGrid2.DataBind();
                for (int i = 0; i < uGrid2.Rows.Count; i++)
                {

                    this.uGrid2.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid2.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;

                    this.uGrid2.Rows[i].Cells["MTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid2.Rows[i].Cells["MTypeCode"].Appearance.BackColor = Color.Gainsboro;

                }

                if (dt.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제버튼 이벤트

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid2.Rows[i].Hidden = true;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0001_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void frmISO0001_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
