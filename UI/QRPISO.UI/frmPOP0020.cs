﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 표준문서 배포처 POPUP검색                             */
/* 모듈(분류)명 : 표준문서 등록                                         */
/* 프로그램ID   : frmPOP0020.cs                                         */
/* 프로그램명   : 표준문서 배포처 POPUP                                 */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-10-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

namespace QRPISO.UI
{
    public partial class frmPOP0020 : Form
    {

        QRPGlobal SysRes = new QRPGlobal();

        private string strPlantCode;
        private string strPlantName;
        private string strVendorCode;
        private string strVendorName;

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string PlantName
        {
            get { return strPlantName; }
            set { strPlantName = value; }
        }

        public string VendorCode
        {
            get { return strVendorCode; }
            set { strVendorCode = value; }
        }

        public string VendorName
        {
            get { return strVendorName; }
            set { strVendorName = value; }
        }

        public frmPOP0020()
        {
            InitializeComponent();
        }

        private void frmPOP0020_Load(object sender, EventArgs e)
        {
            //this.PlantCode = "";
            this.VendorCode = "";
            this.VendorName = "";

            InitButton();
            InitCombo();
            InitGrid();
            InitLabel();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
            brw.mfSetFormLanguage(this);
        }
        
        /// <summary>
        /// 버튼 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스 초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , PlantCode, "", "전체", "PlantCode", "PlantName", dt);

                this.uComboSearchPlant.Enabled = false;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }


        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "VendorName", "거래처명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid1, 0, "WriteUserName", "작성자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 검색
        /// </summary>
        private void Search()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                QRPISO.BL.ISODOC.ISODocVendor ivendor = new QRPISO.BL.ISODOC.ISODocVendor();
                brwChannel.mfCredentials(ivendor);

                this.Cursor = Cursors.WaitCursor;

                string strPlantCode = uComboSearchPlant.Value.ToString();

                DataTable dt = ivendor.mfReadISODocVendorPopup(strPlantCode, m_resSys.GetString("SYSLANG"));

                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();

                this.Cursor = Cursors.Default;

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();

                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

            }
            catch (Exception ex)
            {

            }
            finally
            { }
        }

        /// <summary>
        /// 검색버튼 클릭시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Search();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGrid1.ActiveRow.Index >= 0)
                {
                    strPlantCode = this.uGrid1.ActiveRow.Cells["PlantCode"].Text.ToString();
                    strPlantName = this.uGrid1.ActiveRow.Cells["PlantName"].Text.ToString();
                    strVendorCode = this.uGrid1.ActiveRow.Cells["VendorCode"].Text.ToString();
                    strVendorName = this.uGrid1.ActiveRow.Cells["VendorName"].ToString();

                    this.Close();
                }
            }
            catch(Exception ex)
            { }
            finally
            { }
        }

        private void uGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            strVendorCode = e.Row.Cells["VendorCode"].Text.ToString();
            strVendorName = e.Row.Cells["VendorName"].Text.ToString();

            this.Close();
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
