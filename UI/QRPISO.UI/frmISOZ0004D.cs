﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : Control Plan 이력 정보(ISOSTS)                        */
/* 프로그램ID   : frmISOZ0004D.cs                                       */
/* 프로그램명   : Control Plan 이력 조회                                */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-18                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPISO.UI
{
    public partial class frmISOZ0004D : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmISOZ0004D()
        {
            InitializeComponent();
        }

        private void frmISOZ0004D_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISOZ0004D_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("Control Plan 이력 조회", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Contorol 초기화
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitTree();
        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProduct, "제품", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabeSearchDrevisionNum, "개정차수", m_resSys.GetString("SYS_FONTNAE"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipProcessGroup, "설비공정그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dt = new DataTable();
                // Customer
                wCombo.mfSetComboEditor(this.uComboSearchCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "CusotomerCode", "CustomerName", dt);

                //Product
                wCombo.mfSetComboEditor(this.uComboSearchProduct, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "CusotomerCode", "CustomerName", dt);

                // 개정차수
                wCombo.mfSetComboEditor(this.uComboSearchRevisionNum, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "CusotomerCode", "CustomerName", dt);

                // Area
                wCombo.mfSetComboEditor(this.uComboSearchArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "CusotomerCode", "CustomerName", dt);

                // Station
                wCombo.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "CusotomerCode", "CustomerName", dt);

                // 설비공정그룹
                wCombo.mfSetComboEditor(this.uComboSearchEquipProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "CusotomerCode", "CustomerName", dt);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tree 초기화

        /// </summary>
        private void InitTree()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinTree wTree = new WinTree();

                // 일반설정
                wTree.mfInitTree(this.uTree, Infragistics.Win.UltraWinTree.UltraTreeDisplayStyle.WindowsVista
                    , Infragistics.Win.UltraWinTree.ViewStyle.FreeForm, Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToLastItem
                    , Infragistics.Win.DefaultableBoolean.False, m_resSys.GetString("SYS_FONTNAME"));

                // 최상위 노드 추가(공정검사, CCS규격서, 공정BOM, 점검항목)
                wTree.mfAddNodeToTree(this.uTree, "InspectProcess", "공정검사", null, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);
                wTree.mfAddNodeToTree(this.uTree, "CCS", "CCS 규격서", null, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);
                wTree.mfAddNodeToTree(this.uTree, "ProcessBOM", "공정BOM", null, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);
                wTree.mfAddNodeToTree(this.uTree, "InspectItem", "점검항목", null, Infragistics.Win.UltraWinTree.NodeStyle.Default, false);

                // 컬럼셋 설정
                Infragistics.Win.UltraWinTree.UltraTreeColumnSet columnset1 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();

                columnset1 = mfSetTreeNodeColumnSet(columnset1, false, "Gubun1", "구분",100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "표준번호1", "표준번호", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "고객1", "고객", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "MaterialCode1", "자재코드", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "MaterialName1", "자재명", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "등록일1", "등록일", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "등록자1", "등록자", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "개정일1", "개정일", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "개정자1", "개정자", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "승인일1", "승인일", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "승인자1", "승인자", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset1 = mfSetTreeNodeColumnSet(columnset1, true, "개정사유1", "개정사유", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////Infragistics.Win.UltraWinTree.UltraTreeColumnSet columnset2 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, false, "Gubun2", "구분", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "표준번호2", "표준번호", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "고객2", "고객", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "MaterialCode2", "자재코드", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "MaterialName2", "자재명", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "등록일2", "등록일", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "등록자2", "등록자", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "개정일2", "개정일", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "개정자2", "개정자", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "승인일2", "승인일", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "승인자2", "승인자", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset2 = mfSetTreeNodeColumnSet(columnset2, true, "개정사유2", "개정사유", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////Infragistics.Win.UltraWinTree.UltraTreeColumnSet columnset3 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, false, "Gubun3", "구분", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "표준번호3", "표준번호", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "고객3", "고객", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "MaterialCode3", "자재코드", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "MaterialName3", "자재명", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "등록일3", "등록일", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "등록자3", "등록자", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "개정일3", "개정일", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "개정자3", "개정자", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "승인일3", "승인일", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "승인자3", "승인자", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                ////columnset3 = mfSetTreeNodeColumnSet(columnset3, true, "개정사유3", "개정사유", 100, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                ////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                ////    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                Infragistics.Win.UltraWinTree.UltraTreeColumnSet columnset4 = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();

                columnset4 = mfSetTreeNodeColumnSet(columnset4, false, "Gubun4", "구분", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "표준번호4", "표준번호", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "Area", "Area", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "Station", "Station", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "설비공정구분", "설비공정구분", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "설비코드", "설비코드", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "설비명", "설비명", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "생성일4", "생성일", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "생성자4", "생성자", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "개정일4", "개정일", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "개정자4", "개정자", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "승인일4", "승인일", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "승인자4", "승인자", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                columnset4 = mfSetTreeNodeColumnSet(columnset4, true, "개정사유4", "개정사유", 100, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes, Infragistics.Win.UltraWinTree.SortType.Default
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinTree.AllowCellEdit.ReadOnly);

                // DataTable dt = new DataTable();
                // 트리노드 하위에 그리드 설정하는 Method 호출
                // mfAddGridToTree(this.uTree, columnset, dt, "InspectProcess,Gubun", "InspectProcess");
                Infragistics.Win.UltraWinTree.UltraTreeNode tempNode1 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
                this.uTree.Nodes["InspectProcess"].Nodes.Override.ColumnSet = columnset1;
                this.uTree.Nodes["InspectProcess"].Nodes.Add(tempNode1);

                Infragistics.Win.UltraWinTree.UltraTreeNode tempNode2 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
                this.uTree.Nodes["CCS"].Nodes.Override.ColumnSet = columnset1;
                this.uTree.Nodes["CCS"].Nodes.Add(tempNode2);

                Infragistics.Win.UltraWinTree.UltraTreeNode tempNode3 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
                this.uTree.Nodes["ProcessBOM"].Nodes.Override.ColumnSet = columnset1;
                this.uTree.Nodes["ProcessBOM"].Nodes.Add(tempNode3);

                Infragistics.Win.UltraWinTree.UltraTreeNode tempNode4 = new Infragistics.Win.UltraWinTree.UltraTreeNode();
                this.uTree.Nodes["InspectItem"].Nodes.Override.ColumnSet = columnset4;
                this.uTree.Nodes["InspectItem"].Nodes.Add(tempNode4);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 그리드 트리 컬럼셋 설정 Method
        /// <summary>
        /// 그리드 트리 컬럼셋 설정 Method
        /// </summary>
        /// <param name="columnset"> columnset 변수명 </param>
        /// <param name="bolVisible"></param>
        /// <param name="strKey"></param>
        /// <param name="strText"></param>
        /// <param name="AllowSorting"></param>
        /// <param name="ColAutoSizeType"></param>
        /// <param name="ColSortType"></param>
        /// <param name="ColHAlign"></param>
        /// <param name="ColVAlign"></param>
        /// <param name="ShowSortIndicator"></param>
        /// <param name="AllowCellEdit"></param>
        /// <returns></returns>
        private Infragistics.Win.UltraWinTree.UltraTreeColumnSet mfSetTreeNodeColumnSet(Infragistics.Win.UltraWinTree.UltraTreeColumnSet columnset
                                                                                        , Boolean bolVisible
                                                                                        , String strKey
                                                                                        , String strText
                                                                                        , int intWidth
                                                                                        , Infragistics.Win.DefaultableBoolean AllowSorting
                                                                                        , Infragistics.Win.UltraWinTree.ColumnAutoSizeMode ColAutoSizeType
                                                                                        , Infragistics.Win.UltraWinTree.SortType ColSortType
                                                                                        , Infragistics.Win.HAlign ColHAlign
                                                                                        , Infragistics.Win.VAlign ColVAlign
                                                                                        , Infragistics.Win.DefaultableBoolean ShowSortIndicator
                                                                                        , Infragistics.Win.UltraWinTree.AllowCellEdit AllowCellEdit)
        {
            try
            {

                Infragistics.Win.UltraWinTree.UltraTreeNodeColumn col = new Infragistics.Win.UltraWinTree.UltraTreeNodeColumn();

                col.Key = strKey;
                col.Text = strText;

                if (col.Key == "UseFlag")
                {
                    Infragistics.Win.ValueList ValueUseFlag = new Infragistics.Win.ValueList();
                    ValueUseFlag.ValueListItems.Add("T", "사용함");
                    ValueUseFlag.ValueListItems.Add("F", "사용안함");
                    col.ValueList = ValueUseFlag;
                }

                col.AllowSorting = AllowSorting; //DefaultableBoolean.Default;
                col.AutoSizeMode = ColAutoSizeType; //Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.Default;

                col.CellAppearance.TextHAlign = ColHAlign; //HAlign.Default;
                col.CellAppearance.TextVAlign = ColVAlign; //VAlign.Default;

                col.ShowSortIndicators = ShowSortIndicator; //DefaultableBoolean.True;
                col.SortType = ColSortType; //Infragistics.Win.UltraWinTree.SortType.Descending;
                col.Visible = bolVisible;
                col.AllowCellEdit = AllowCellEdit;
                col.LayoutInfo.PreferredLabelSize = new Size(intWidth, 0);  // Column Width

                //고정
                col.TipStyleCell = Infragistics.Win.UltraWinTree.TipStyleCell.Show;
                col.HeaderAppearance.BackColor = Color.AliceBlue;
                col.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                columnset.Columns.Add(col);

                return columnset;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return columnset;
            }
            finally
            {
            }
        }
        #endregion

        #region 노드밑에 그리드 추가하는 함수
        public void mfAddGridToTree(Infragistics.Win.UltraWinTree.UltraTree Tree,
                                    Infragistics.Win.UltraWinTree.UltraTreeColumnSet columnset,
                                    System.Data.DataTable dtNodeData,
                                    string strKeyColumn,
                                    string strRelationKeyColumn)
        {
            try
            {
                Infragistics.Win.UltraWinTree.UltraTreeNode tempTreeNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();
                Infragistics.Win.UltraWinTree.UltraTreeNode ParentTreeNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();

                foreach (DataRow row in dtNodeData.Rows)
                {
                    //관계키가 있는 경우 추가하려는 Node의 부모값과 동일한 Key값이 있는지 체크하여 부모Node에 Node를 추가한다.
                    if (strRelationKeyColumn != "")
                    {
                        char[] strSeparator = { ',' };
                        string[] arrKey = strKeyColumn.Split(strSeparator);
                        string[] arrRelKey = strRelationKeyColumn.Split(strSeparator);
                        string strRelKey = "";
                        for (int i = 0; i < arrRelKey.Count(); i++)
                        {
                            if (i == 0)
                                strRelKey = strRelKey + row[arrRelKey[i]].ToString();
                            else
                                strRelKey = strRelKey + strSeparator[0] + row[arrRelKey[i]].ToString();
                        }
                        ParentTreeNode = Tree.GetNodeByKey(strRelKey);
                        ParentTreeNode.Nodes.Override.ColumnSet = columnset;
                        if (ParentTreeNode != null)
                        {
                            Infragistics.Win.UltraWinTree.UltraTreeNode TreeNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();

                            //다중 키인 경우 쉼표를 구분자로 하여 NodeKey를 설정한다.
                            string strNodeKey = "";

                            for (int i = 0; i < arrKey.Count(); i++)
                            {
                                if (i == 0)
                                    strNodeKey = strNodeKey + row[arrKey[i]].ToString();
                                else
                                    strNodeKey = strNodeKey + strSeparator[0] + row[arrKey[i]].ToString();
                            }
                            // 노드 키 설정
                            TreeNode.Key = strNodeKey;
                            TreeNode.Expanded = false;
                            ParentTreeNode.Nodes.Add(TreeNode);

                            // 부모노드에 컬럼셋 설정
                            for (int i = 0; i < columnset.Columns.Count; i++)
                            {
                                TreeNode.Cells[columnset.Columns[i].Key].Value = row[columnset.Columns[i].Key].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion
    }
}
