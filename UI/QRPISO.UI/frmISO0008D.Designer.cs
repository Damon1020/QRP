﻿namespace QRPISO.UI
{
    partial class frmISO0008D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0008D));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonCopy = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGridPPQ = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.richTextNote4 = new QRPUserControl.RichTextEditor();
            this.richTextNote3 = new QRPUserControl.RichTextEditor();
            this.richTextNote2 = new QRPUserControl.RichTextEditor();
            this.richTextNote1 = new QRPUserControl.RichTextEditor();
            this.uLabelNote4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelNote3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelNote2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelNote1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextCustomer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductGroup = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCreateDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStandardNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uGridInfo = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelClient = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPPQ)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonCopy);
            this.ultraTabPageControl1.Controls.Add(this.uButtonDelete);
            this.ultraTabPageControl1.Controls.Add(this.uGridPPQ);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1036, 534);
            // 
            // uButtonCopy
            // 
            this.uButtonCopy.Location = new System.Drawing.Point(108, 12);
            this.uButtonCopy.Name = "uButtonCopy";
            this.uButtonCopy.Size = new System.Drawing.Size(88, 28);
            this.uButtonCopy.TabIndex = 9;
            this.uButtonCopy.Text = "ultraButton1";
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 12);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 8;
            this.uButtonDelete.Text = "ultraButton1";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uGridPPQ
            // 
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPPQ.DisplayLayout.Appearance = appearance18;
            this.uGridPPQ.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPPQ.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance19.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance19.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance19.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPPQ.DisplayLayout.GroupByBox.Appearance = appearance19;
            appearance20.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPPQ.DisplayLayout.GroupByBox.BandLabelAppearance = appearance20;
            this.uGridPPQ.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance21.BackColor2 = System.Drawing.SystemColors.Control;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPPQ.DisplayLayout.GroupByBox.PromptAppearance = appearance21;
            this.uGridPPQ.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPPQ.DisplayLayout.MaxRowScrollRegions = 1;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPPQ.DisplayLayout.Override.ActiveCellAppearance = appearance22;
            appearance23.BackColor = System.Drawing.SystemColors.Highlight;
            appearance23.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPPQ.DisplayLayout.Override.ActiveRowAppearance = appearance23;
            this.uGridPPQ.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPPQ.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPPQ.DisplayLayout.Override.CardAreaAppearance = appearance24;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            appearance25.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPPQ.DisplayLayout.Override.CellAppearance = appearance25;
            this.uGridPPQ.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPPQ.DisplayLayout.Override.CellPadding = 0;
            appearance26.BackColor = System.Drawing.SystemColors.Control;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPPQ.DisplayLayout.Override.GroupByRowAppearance = appearance26;
            appearance27.TextHAlignAsString = "Left";
            this.uGridPPQ.DisplayLayout.Override.HeaderAppearance = appearance27;
            this.uGridPPQ.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPPQ.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            this.uGridPPQ.DisplayLayout.Override.RowAppearance = appearance28;
            this.uGridPPQ.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPPQ.DisplayLayout.Override.TemplateAddRowAppearance = appearance29;
            this.uGridPPQ.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPPQ.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPPQ.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPPQ.Location = new System.Drawing.Point(12, 44);
            this.uGridPPQ.Name = "uGridPPQ";
            this.uGridPPQ.Size = new System.Drawing.Size(1016, 484);
            this.uGridPPQ.TabIndex = 2;
            this.uGridPPQ.Text = "ultraGrid1";
            this.uGridPPQ.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPPQ_AfterCellUpdate);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.richTextNote4);
            this.ultraTabPageControl2.Controls.Add(this.richTextNote3);
            this.ultraTabPageControl2.Controls.Add(this.richTextNote2);
            this.ultraTabPageControl2.Controls.Add(this.richTextNote1);
            this.ultraTabPageControl2.Controls.Add(this.uLabelNote4);
            this.ultraTabPageControl2.Controls.Add(this.uLabelNote3);
            this.ultraTabPageControl2.Controls.Add(this.uLabelNote2);
            this.ultraTabPageControl2.Controls.Add(this.uLabelNote1);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1036, 534);
            // 
            // richTextNote4
            // 
            this.richTextNote4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextNote4.FontSize = QRPUserControl.FontSize.Three;
            this.richTextNote4.Location = new System.Drawing.Point(524, 296);
            this.richTextNote4.Name = "richTextNote4";
            this.richTextNote4.Size = new System.Drawing.Size(500, 228);
            this.richTextNote4.TabIndex = 17;
            // 
            // richTextNote3
            // 
            this.richTextNote3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextNote3.FontSize = QRPUserControl.FontSize.Three;
            this.richTextNote3.Location = new System.Drawing.Point(12, 296);
            this.richTextNote3.Name = "richTextNote3";
            this.richTextNote3.Size = new System.Drawing.Size(500, 228);
            this.richTextNote3.TabIndex = 16;
            // 
            // richTextNote2
            // 
            this.richTextNote2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextNote2.FontSize = QRPUserControl.FontSize.Three;
            this.richTextNote2.Location = new System.Drawing.Point(524, 36);
            this.richTextNote2.Name = "richTextNote2";
            this.richTextNote2.Size = new System.Drawing.Size(500, 228);
            this.richTextNote2.TabIndex = 15;
            // 
            // richTextNote1
            // 
            this.richTextNote1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextNote1.FontSize = QRPUserControl.FontSize.Three;
            this.richTextNote1.Location = new System.Drawing.Point(12, 36);
            this.richTextNote1.Name = "richTextNote1";
            this.richTextNote1.Size = new System.Drawing.Size(500, 228);
            this.richTextNote1.TabIndex = 14;
            // 
            // uLabelNote4
            // 
            this.uLabelNote4.Location = new System.Drawing.Point(528, 272);
            this.uLabelNote4.Name = "uLabelNote4";
            this.uLabelNote4.Size = new System.Drawing.Size(100, 20);
            this.uLabelNote4.TabIndex = 11;
            this.uLabelNote4.Text = "ultraLabel1";
            // 
            // uLabelNote3
            // 
            this.uLabelNote3.Location = new System.Drawing.Point(16, 272);
            this.uLabelNote3.Name = "uLabelNote3";
            this.uLabelNote3.Size = new System.Drawing.Size(100, 20);
            this.uLabelNote3.TabIndex = 10;
            this.uLabelNote3.Text = "ultraLabel1";
            // 
            // uLabelNote2
            // 
            this.uLabelNote2.Location = new System.Drawing.Point(528, 12);
            this.uLabelNote2.Name = "uLabelNote2";
            this.uLabelNote2.Size = new System.Drawing.Size(100, 20);
            this.uLabelNote2.TabIndex = 12;
            this.uLabelNote2.Text = "ultraLabel1";
            // 
            // uLabelNote1
            // 
            this.uLabelNote1.Location = new System.Drawing.Point(12, 12);
            this.uLabelNote1.Name = "uLabelNote1";
            this.uLabelNote1.Size = new System.Drawing.Size(100, 20);
            this.uLabelNote1.TabIndex = 13;
            this.uLabelNote1.Text = "ultraLabel1";
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 11;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTabControl1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox2.Controls.Add(this.uTextCustomer);
            this.uGroupBox2.Controls.Add(this.uComboPlant);
            this.uGroupBox2.Controls.Add(this.uTextSpec);
            this.uGroupBox2.Controls.Add(this.uLabelSpec);
            this.uGroupBox2.Controls.Add(this.uTextProductType);
            this.uGroupBox2.Controls.Add(this.uLabelProductType);
            this.uGroupBox2.Controls.Add(this.uTextProductGroup);
            this.uGroupBox2.Controls.Add(this.uLabelProductGroup);
            this.uGroupBox2.Controls.Add(this.uTextProductName);
            this.uGroupBox2.Controls.Add(this.uTextProductCode);
            this.uGroupBox2.Controls.Add(this.uLabelProduct);
            this.uGroupBox2.Controls.Add(this.uLabelCustomer);
            this.uGroupBox2.Controls.Add(this.uLabelPlant);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 60);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1040, 64);
            this.uGroupBox2.TabIndex = 89;
            // 
            // uTextCustomer
            // 
            appearance43.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomer.Appearance = appearance43;
            this.uTextCustomer.BackColor = System.Drawing.Color.PowderBlue;
            appearance14.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance14.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance14;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCustomer.ButtonsRight.Add(editorButton1);
            this.uTextCustomer.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCustomer.Location = new System.Drawing.Point(380, 12);
            this.uTextCustomer.Name = "uTextCustomer";
            this.uTextCustomer.Size = new System.Drawing.Size(100, 21);
            this.uTextCustomer.TabIndex = 47;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 46;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uTextSpec
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance5;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(644, 36);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(150, 21);
            this.uTextSpec.TabIndex = 45;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(540, 36);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpec.TabIndex = 44;
            this.uLabelSpec.Text = "ultraLabel1";
            // 
            // uTextProductType
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductType.Appearance = appearance15;
            this.uTextProductType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductType.Location = new System.Drawing.Point(380, 36);
            this.uTextProductType.Name = "uTextProductType";
            this.uTextProductType.ReadOnly = true;
            this.uTextProductType.Size = new System.Drawing.Size(150, 21);
            this.uTextProductType.TabIndex = 43;
            // 
            // uLabelProductType
            // 
            this.uLabelProductType.Location = new System.Drawing.Point(276, 36);
            this.uLabelProductType.Name = "uLabelProductType";
            this.uLabelProductType.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductType.TabIndex = 42;
            this.uLabelProductType.Text = "ultraLabel1";
            // 
            // uTextProductGroup
            // 
            appearance49.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductGroup.Appearance = appearance49;
            this.uTextProductGroup.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductGroup.Location = new System.Drawing.Point(116, 36);
            this.uTextProductGroup.Name = "uTextProductGroup";
            this.uTextProductGroup.ReadOnly = true;
            this.uTextProductGroup.Size = new System.Drawing.Size(150, 21);
            this.uTextProductGroup.TabIndex = 41;
            // 
            // uLabelProductGroup
            // 
            this.uLabelProductGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelProductGroup.Name = "uLabelProductGroup";
            this.uLabelProductGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductGroup.TabIndex = 40;
            this.uLabelProductGroup.Text = "ultraLabel1";
            // 
            // uTextProductName
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance6;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(745, 12);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(100, 21);
            this.uTextProductName.TabIndex = 39;
            // 
            // uTextProductCode
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance9;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(644, 12);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(100, 21);
            this.uTextProductCode.TabIndex = 38;
            // 
            // uLabelProduct
            // 
            this.uLabelProduct.Location = new System.Drawing.Point(540, 12);
            this.uLabelProduct.Name = "uLabelProduct";
            this.uLabelProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelProduct.TabIndex = 37;
            this.uLabelProduct.Text = "ultraLabel1";
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(276, 12);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelCustomer.TabIndex = 35;
            this.uLabelCustomer.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 33;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uTextEtc
            // 
            this.uTextEtc.Location = new System.Drawing.Point(116, 36);
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.Size = new System.Drawing.Size(932, 21);
            this.uTextEtc.TabIndex = 88;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 36);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc.TabIndex = 87;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uDateCreateDate
            // 
            this.uDateCreateDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCreateDate.Location = new System.Drawing.Point(696, 12);
            this.uDateCreateDate.Name = "uDateCreateDate";
            this.uDateCreateDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCreateDate.TabIndex = 86;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(592, 12);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateDate.TabIndex = 85;
            this.uLabelCreateDate.Text = "ultraLabel1";
            // 
            // ultraTextEditor1
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Appearance = appearance4;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Location = new System.Drawing.Point(481, 12);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.ReadOnly = true;
            this.ultraTextEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor1.TabIndex = 84;
            // 
            // uTextCreateUserID
            // 
            appearance41.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCreateUserID.Appearance = appearance41;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance44.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance44.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance44;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCreateUserID.ButtonsRight.Add(editorButton2);
            this.uTextCreateUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCreateUserID.Location = new System.Drawing.Point(380, 12);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 83;
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(276, 12);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelCreateUser.TabIndex = 82;
            this.uLabelCreateUser.Text = "ultraLabel1";
            // 
            // uTextStandardNo
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Appearance = appearance1;
            this.uTextStandardNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Location = new System.Drawing.Point(116, 12);
            this.uTextStandardNo.Name = "uTextStandardNo";
            this.uTextStandardNo.ReadOnly = true;
            this.uTextStandardNo.Size = new System.Drawing.Size(150, 21);
            this.uTextStandardNo.TabIndex = 81;
            // 
            // uLabelStandardNo
            // 
            this.uLabelStandardNo.Location = new System.Drawing.Point(12, 12);
            this.uLabelStandardNo.Name = "uLabelStandardNo";
            this.uLabelStandardNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelStandardNo.TabIndex = 80;
            this.uLabelStandardNo.Text = "ultraLabel1";
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(12, 128);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1040, 560);
            this.ultraTabControl1.TabIndex = 4;
            ultraTab7.TabPage = this.ultraTabPageControl1;
            ultraTab7.Text = "PPQ공정평가 규격서 상세정보";
            ultraTab8.TabPage = this.ultraTabPageControl2;
            ultraTab8.Text = "비고";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab7,
            ultraTab8});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1036, 534);
            // 
            // uGridInfo
            // 
            this.uGridInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInfo.DisplayLayout.Appearance = appearance37;
            this.uGridInfo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInfo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInfo.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInfo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.uGridInfo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInfo.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.uGridInfo.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInfo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInfo.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInfo.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridInfo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInfo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInfo.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance3.BorderColor = System.Drawing.Color.Silver;
            appearance3.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInfo.DisplayLayout.Override.CellAppearance = appearance3;
            this.uGridInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInfo.DisplayLayout.Override.CellPadding = 0;
            appearance42.BackColor = System.Drawing.SystemColors.Control;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInfo.DisplayLayout.Override.GroupByRowAppearance = appearance42;
            appearance48.TextHAlignAsString = "Left";
            this.uGridInfo.DisplayLayout.Override.HeaderAppearance = appearance48;
            this.uGridInfo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInfo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGridInfo.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGridInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInfo.DisplayLayout.Override.TemplateAddRowAppearance = appearance45;
            this.uGridInfo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInfo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInfo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInfo.Location = new System.Drawing.Point(0, 80);
            this.uGridInfo.Name = "uGridInfo";
            this.uGridInfo.Size = new System.Drawing.Size(1070, 760);
            this.uGridInfo.TabIndex = 10;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance32;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor11);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor10);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProductCode);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelClient);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor9);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 9;
            // 
            // ultraTextEditor11
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Appearance = appearance33;
            this.ultraTextEditor11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Location = new System.Drawing.Point(792, 12);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.ReadOnly = true;
            this.ultraTextEditor11.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor11.TabIndex = 9;
            // 
            // ultraTextEditor10
            // 
            appearance34.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance34;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor10.ButtonsRight.Add(editorButton3);
            this.ultraTextEditor10.Location = new System.Drawing.Point(688, 12);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor10.TabIndex = 10;
            // 
            // uLabelProductCode
            // 
            this.uLabelProductCode.Location = new System.Drawing.Point(584, 12);
            this.uLabelProductCode.Name = "uLabelProductCode";
            this.uLabelProductCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductCode.TabIndex = 8;
            this.uLabelProductCode.Text = "ultraLabel1";
            // 
            // ultraTextEditor2
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance35;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(476, 12);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor2.TabIndex = 7;
            // 
            // uLabelClient
            // 
            this.uLabelClient.Location = new System.Drawing.Point(268, 12);
            this.uLabelClient.Name = "uLabelClient";
            this.uLabelClient.Size = new System.Drawing.Size(100, 20);
            this.uLabelClient.TabIndex = 5;
            this.uLabelClient.Text = "ultraLabel1";
            // 
            // ultraTextEditor9
            // 
            appearance36.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance36;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor9.ButtonsRight.Add(editorButton4);
            this.ultraTextEditor9.Location = new System.Drawing.Point(372, 12);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor9.TabIndex = 6;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 4;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 3;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 8;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmISO0008D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridInfo);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0008D";
            this.Load += new System.EventHandler(this.frmISO0008D_Load);
            this.Activated += new System.EventHandler(this.frmISO0008D_Activated);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPPQ)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            this.uGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInfo;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.Misc.UltraLabel uLabelProductCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel uLabelClient;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPPQ;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraLabel uLabelNote4;
        private Infragistics.Win.Misc.UltraLabel uLabelNote3;
        private Infragistics.Win.Misc.UltraLabel uLabelNote2;
        private Infragistics.Win.Misc.UltraLabel uLabelNote1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductType;
        private Infragistics.Win.Misc.UltraLabel uLabelProductType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProductGroup;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProduct;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCreateDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNo;
        private Infragistics.Win.Misc.UltraButton uButtonCopy;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private QRPUserControl.RichTextEditor richTextNote4;
        private QRPUserControl.RichTextEditor richTextNote3;
        private QRPUserControl.RichTextEditor richTextNote2;
        private QRPUserControl.RichTextEditor richTextNote1;
    }
}