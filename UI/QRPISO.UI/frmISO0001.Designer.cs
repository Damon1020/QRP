﻿namespace QRPISO.UI
{
    partial class frmISO0001
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0001));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uComboAutoStdNumberFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelAutoStdNumberFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLargeClassifyNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLargeClassifyNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLargeClassifyNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLargeClassifyNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLargeClassifyName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLargeClassifyName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLargeClassifyCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLargeClassifyCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboGrwFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelGrwFlag = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAutoStdNumberFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboGrwFlag)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance1;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 40);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 800);
            this.uGrid1.TabIndex = 1;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 755);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 90);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 755);
            this.uGroupBoxContentsArea.TabIndex = 2;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboGrwFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelGrwFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboAutoStdNumberFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAutoStdNumberFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDeleteRow);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGrid2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLargeClassifyNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLargeClassifyNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLargeClassifyNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLargeClassifyNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLargeClassifyName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLargeClassifyName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLargeClassifyCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLargeClassifyCode);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 735);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uComboAutoStdNumberFlag
            // 
            this.uComboAutoStdNumberFlag.Location = new System.Drawing.Point(380, 60);
            this.uComboAutoStdNumberFlag.Name = "uComboAutoStdNumberFlag";
            this.uComboAutoStdNumberFlag.Size = new System.Drawing.Size(150, 21);
            this.uComboAutoStdNumberFlag.TabIndex = 22;
            this.uComboAutoStdNumberFlag.Text = "ultraComboEditor1";
            // 
            // uLabelAutoStdNumberFlag
            // 
            this.uLabelAutoStdNumberFlag.Location = new System.Drawing.Point(276, 60);
            this.uLabelAutoStdNumberFlag.Name = "uLabelAutoStdNumberFlag";
            this.uLabelAutoStdNumberFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelAutoStdNumberFlag.TabIndex = 21;
            this.uLabelAutoStdNumberFlag.Text = "ultraLabel2";
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 84);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 20;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 19;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 18;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance16;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance17;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance18;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance19.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance19.BackColor2 = System.Drawing.SystemColors.Control;
            appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance19.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance19;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance20;
            appearance21.BackColor = System.Drawing.SystemColors.Highlight;
            appearance21.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance21;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance22;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance23;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance24.BackColor = System.Drawing.SystemColors.Control;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance24;
            appearance25.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance25;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance26;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance27;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 112);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1044, 610);
            this.uGrid2.TabIndex = 12;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(116, 60);
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(150, 21);
            this.uComboUseFlag.TabIndex = 11;
            this.uComboUseFlag.Text = "ultraComboEditor1";
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(12, 60);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelUseFlag.TabIndex = 10;
            this.uLabelUseFlag.Text = "ultraLabel2";
            // 
            // uTextLargeClassifyNameEn
            // 
            this.uTextLargeClassifyNameEn.Location = new System.Drawing.Point(644, 36);
            this.uTextLargeClassifyNameEn.Name = "uTextLargeClassifyNameEn";
            this.uTextLargeClassifyNameEn.Size = new System.Drawing.Size(150, 21);
            this.uTextLargeClassifyNameEn.TabIndex = 7;
            // 
            // uLabelLargeClassifyNameEn
            // 
            this.uLabelLargeClassifyNameEn.Location = new System.Drawing.Point(540, 36);
            this.uLabelLargeClassifyNameEn.Name = "uLabelLargeClassifyNameEn";
            this.uLabelLargeClassifyNameEn.Size = new System.Drawing.Size(100, 20);
            this.uLabelLargeClassifyNameEn.TabIndex = 6;
            this.uLabelLargeClassifyNameEn.Text = "ultraLabel2";
            // 
            // uTextLargeClassifyNameCh
            // 
            this.uTextLargeClassifyNameCh.Location = new System.Drawing.Point(380, 36);
            this.uTextLargeClassifyNameCh.Name = "uTextLargeClassifyNameCh";
            this.uTextLargeClassifyNameCh.Size = new System.Drawing.Size(150, 21);
            this.uTextLargeClassifyNameCh.TabIndex = 5;
            // 
            // uLabelLargeClassifyNameCh
            // 
            this.uLabelLargeClassifyNameCh.Location = new System.Drawing.Point(276, 36);
            this.uLabelLargeClassifyNameCh.Name = "uLabelLargeClassifyNameCh";
            this.uLabelLargeClassifyNameCh.Size = new System.Drawing.Size(100, 20);
            this.uLabelLargeClassifyNameCh.TabIndex = 4;
            this.uLabelLargeClassifyNameCh.Text = "ultraLabel2";
            // 
            // uTextLargeClassifyName
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLargeClassifyName.Appearance = appearance14;
            this.uTextLargeClassifyName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLargeClassifyName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLargeClassifyName.Location = new System.Drawing.Point(116, 36);
            this.uTextLargeClassifyName.Name = "uTextLargeClassifyName";
            this.uTextLargeClassifyName.Size = new System.Drawing.Size(150, 21);
            this.uTextLargeClassifyName.TabIndex = 3;
            // 
            // uLabelLargeClassifyName
            // 
            this.uLabelLargeClassifyName.Location = new System.Drawing.Point(12, 36);
            this.uLabelLargeClassifyName.Name = "uLabelLargeClassifyName";
            this.uLabelLargeClassifyName.Size = new System.Drawing.Size(100, 20);
            this.uLabelLargeClassifyName.TabIndex = 2;
            this.uLabelLargeClassifyName.Text = "ultraLabel1";
            // 
            // uTextLargeClassifyCode
            // 
            appearance13.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLargeClassifyCode.Appearance = appearance13;
            this.uTextLargeClassifyCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextLargeClassifyCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextLargeClassifyCode.Location = new System.Drawing.Point(380, 12);
            this.uTextLargeClassifyCode.Name = "uTextLargeClassifyCode";
            this.uTextLargeClassifyCode.Size = new System.Drawing.Size(150, 21);
            this.uTextLargeClassifyCode.TabIndex = 1;
            // 
            // uLabelLargeClassifyCode
            // 
            this.uLabelLargeClassifyCode.Location = new System.Drawing.Point(276, 12);
            this.uLabelLargeClassifyCode.Name = "uLabelLargeClassifyCode";
            this.uLabelLargeClassifyCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelLargeClassifyCode.TabIndex = 0;
            this.uLabelLargeClassifyCode.Text = "ultraLabel1";
            // 
            // uComboGrwFlag
            // 
            this.uComboGrwFlag.Location = new System.Drawing.Point(644, 60);
            this.uComboGrwFlag.Name = "uComboGrwFlag";
            this.uComboGrwFlag.Size = new System.Drawing.Size(150, 21);
            this.uComboGrwFlag.TabIndex = 24;
            this.uComboGrwFlag.Text = "ultraComboEditor1";
            // 
            // uLabelGrwFlag
            // 
            this.uLabelGrwFlag.Location = new System.Drawing.Point(540, 60);
            this.uLabelGrwFlag.Name = "uLabelGrwFlag";
            this.uLabelGrwFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelGrwFlag.TabIndex = 23;
            this.uLabelGrwFlag.Text = "ultraLabel2";
            // 
            // frmISO0001
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0001";
            this.Load += new System.EventHandler(this.frmISO0001_Load);
            this.Activated += new System.EventHandler(this.frmISO0001_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISO0001_FormClosing);
            this.Resize += new System.EventHandler(this.frmISO0001_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboAutoStdNumberFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLargeClassifyCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboGrwFlag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelLargeClassifyCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLargeClassifyNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelLargeClassifyNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLargeClassifyName;
        private Infragistics.Win.Misc.UltraLabel uLabelLargeClassifyName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLargeClassifyCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLargeClassifyNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelLargeClassifyNameEn;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboAutoStdNumberFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelAutoStdNumberFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboGrwFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelGrwFlag;
    }
}