﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서 관리                                         */
/* 프로그램ID   : frmISO0003C.cs                                        */
/* 프로그램명   : 표준문서등록                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;




namespace QRPISO.UI
{
    public partial class frmISO0003C : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        String m_strAdmitStatus = "";
        String m_strVersionNum = "";
        public frmISO0003C()
        {
            InitializeComponent();
        }

        private void frmISO0003C_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISO0003C_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitButton();
            InitGrid();
            InitComboBox();
            InitValue();

            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox3, GroupBoxType.LIST, "배포업체", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.LIST, "적용범위", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                wGroupBox.mfSetGroupBox(this.uGroupBox5, GroupBoxType.LIST, "결재선", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // Set Font
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox3.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox3.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGroupBox5.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox5.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화


        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStandardDocNo, "표준문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCreateUser, "기안자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCreateDate, "기안일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApproveUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelDocState, "문서상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelDocKeyword, "문서 Keyword", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApplyDate, "적용일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelReturnReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChipSize, "Chipsize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPadsize, "Padsize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMakeUserName, "작성자명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMakeDeptName, "기안부서명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelVersionNum, "개정번호", m_resSys.GetString("SYS_FONTNAMe"), true, false);
                wLabel.mfSetLabel(this.uLabelGRWComment, "결재 Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화


        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow3, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow4, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화


        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Plant ComboBox
                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // Process ComboBox
                // Bl 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo("", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboProcess, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "ProcessCode", "ProcessName", dtProcess);



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화


        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                QRPBrowser brwChannel = new QRPBrowser();

                //Header그리드
                wGrid.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                                                      , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                      , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                                                      , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                                                      , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼 설정
                wGrid.mfSetGridColumn(this.uGridHeader, 0, "LTypeName", "대분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "MTypeName", "중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 0
                                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 300, false, false, 0
                                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "WriteName", "기안자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridHeader, 0, "AdmitStatus", "문서상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 0
                                                        , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                // 검색을 위한 Hidden 컬럼
                wGrid.mfSetGridColumn(this.uGridHeader, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 0
                                                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                                                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 첨부파일 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "1");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, true, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 4000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DWFlag", "File View", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 60, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 배포업체 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "1");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "StdNumber", "문서번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, true, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "VendorName", "거래처이름", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "PersonName", "담당자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Hp", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "###-####-####", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Email", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 250, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 적용범위 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid3, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "StdNumber", "문서번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "1");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "ProcessGroup", "공정그룹", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcessGroup);

                DataTable dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid3, 0, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcessGroup);

                // 합의담당자 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid4, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid4, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "1");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "StdNumber", "문서번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserID", "담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "AgreeUserName", "담당자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGrid1, 0);
                wGrid.mfAddRowGrid(this.uGrid2, 0);
                wGrid.mfAddRowGrid(this.uGrid3, 0);
                wGrid.mfAddRowGrid(this.uGrid4, 0);

                // Set Font Size
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                this.uGrid4.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid4.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 초기화


        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextStandardDocNo.Text = "";
                this.uTextStandardDocNo.MaxLength = 100;
                this.uTextStandardDocNo.ReadOnly = true;
                this.uTextVersionNum.Text = "";
                this.uTextVersionNum.MaxLength = 50;
                this.uTextVersionNum.ReadOnly = true;
                this.uTextVersionNum.Appearance.BackColor = Color.Gainsboro;
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE"); // "";
                this.uComboProcess.Value = "";
                this.uComboProcess.Visible = false;
                this.uComboLarge.Value = "";
                this.uComboMiddle.Value = "";
                this.uComboSmall.Value = "";
                this.uTextCreateUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextCreateUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextCreateUserID.MaxLength = 20;
                this.uTextCreateUserName.MaxLength = 15;
                this.uDateCreateDate.Value = DateTime.Now;
                this.uTextApproveUserID.Text = "";
                this.uTextApproveUserID.MaxLength = 20;
                this.uTextApproveUserName.Text = "";
                this.uTextApproveUserName.MaxLength = 15;
                this.uTextDocState.Text = "";
                this.uTextDocState.MaxLength = 2;
                this.uTextTitle.Appearance.BackColor = Color.PowderBlue;
                this.uTextTitle.Text = "";
                this.uTextTitle.MaxLength = 2000;
                this.uTextDocKeyword.Text = "";
                this.uTextDocKeyword.MaxLength = 50;
                this.uTextPackage.Text = "";
                this.uTextPackage.MaxLength = 40;
                this.uTextChipsize.Text = "";
                this.uTextChipsize.MaxLength = 100;
                this.uTextPadsize.Text = "";
                this.uTextPadsize.MaxLength = 100;
                this.uTextMakeUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextMakeUserName.MaxLength = 40;
                this.uTextMakeDeptName.Text = m_resSys.GetString("SYS_DEPTNAME");
                this.uTextMakeDeptName.MaxLength = 40;
                this.uTextMakeDeptName.Appearance.BackColor = Color.Gainsboro;
                this.uTextMakeDeptName.ReadOnly = true;
                this.uDateApplyFromDate.Value = DateTime.Now.AddDays(1);
                this.uDateApplyToDate.Value = DateTime.Now.AddDays(1);
                this.uDateCompleteDate.Visible = false;
                this.uDateCompleteDate.Value = DateTime.Now.AddDays(30);
                this.uTextEtc.Text = "";
                this.uTextEtc.MaxLength = 50;
                this.uTextGRWComment.Text = "";
                this.uTextGRWComment.MaxLength = 1000;
                this.uComboLarge.Appearance.BackColor = Color.PowderBlue;
                this.uComboLarge.ReadOnly = false;
                this.uComboMiddle.Appearance.BackColor = Color.PowderBlue;
                this.uComboMiddle.ReadOnly = false;
                this.uComboSmall.Appearance.BackColor = Color.White;
                this.uComboSmall.ReadOnly = false;
                this.uComboProcess.Appearance.BackColor = Color.PowderBlue;
                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uComboPlant.ReadOnly = false;

                m_strAdmitStatus = "";


                if (this.uGrid1.Rows.Count > 0)
                {
                    while (this.uGrid1.Rows.Count > 0)
                    {
                        this.uGrid1.Rows[0].Delete(false);
                    }
                }

                if (this.uGrid2.Rows.Count > 0)
                {
                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                }
                if (this.uGrid3.Rows.Count > 0)
                {
                    while (this.uGrid3.Rows.Count > 0)
                    {
                        this.uGrid3.Rows[0].Delete(false);
                    }
                }
                if (this.uGrid4.Rows.Count > 0)
                {
                    while (this.uGrid4.Rows.Count > 0)
                    {
                        this.uGrid4.Rows[0].Delete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 문서상태에 따라서 수정불가 / 수정가능 상태로 변경
        //수정불가
        private void SetDisWritable()
        {
            try
            {
                this.uComboLarge.ReadOnly = true;
                this.uComboLarge.Appearance.BackColor = Color.Gainsboro;
                this.uComboMiddle.ReadOnly = true;
                this.uComboMiddle.Appearance.BackColor = Color.Gainsboro;
                this.uComboSmall.ReadOnly = true;
                this.uComboSmall.Appearance.BackColor = Color.Gainsboro;
                this.uTextTitle.ReadOnly = true;
                this.uTextTitle.Appearance.BackColor = Color.Gainsboro;
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                this.uTextCreateUserID.ReadOnly = true;
                this.uTextCreateUserID.Appearance.BackColor = Color.Gainsboro;
                this.uTextApproveUserID.ReadOnly = true;
                this.uTextApproveUserID.Appearance.BackColor = Color.Gainsboro;
                this.uTextEtc.ReadOnly = true;
                this.uTextEtc.Appearance.BackColor = Color.Gainsboro;
                this.uDateApplyFromDate.ReadOnly = true;
                this.uDateApplyFromDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateApplyToDate.ReadOnly = true;
                this.uDateApplyToDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateCompleteDate.ReadOnly = true;
                this.uDateCompleteDate.Appearance.BackColor = Color.Gainsboro;
                this.uDateCreateDate.ReadOnly = true;
                this.uDateCreateDate.Appearance.BackColor = Color.Gainsboro;
                this.uTextGRWComment.ReadOnly = true;
                this.uTextGRWComment.Appearance.BackColor = Color.Gainsboro;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //그리드 변경
        private void GridDiswritable()
        {
            try
            {
                this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
                this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
                this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
                this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                ////WinGrid wGrid = new WinGrid();

                ////this.uGrid1.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                ////this.uGrid2.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                ////this.uGrid3.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                ////this.uGrid4.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                ////for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                ////{
                ////    this.uGrid1.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid1.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid1.Rows[i].Cells["FileTitle"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid1.Rows[i].Cells["DWFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid1.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////}
                ////for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                ////{
                ////    this.uGrid2.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid2.Rows[i].Cells["VendorCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid2.Rows[i].Cells["VendorName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid2.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid2.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////}
                ////for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                ////{
                ////    this.uGrid3.Rows[i].Cells["ProcessGroup"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid3.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////}
                ////for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                ////{
                ////    this.uGrid4.Rows[i].Cells["AgreeUserName"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////    this.uGrid4.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                ////}
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //수정 가능
        private void SetWritable()
        {
            try
            {
                this.uComboLarge.ReadOnly = false;
                this.uComboLarge.Appearance.BackColor = Color.PowderBlue;
                this.uComboMiddle.ReadOnly = false;
                this.uComboMiddle.Appearance.BackColor = Color.PowderBlue;
                this.uComboSmall.ReadOnly = false;
                this.uComboSmall.Appearance.BackColor = Color.White;
                this.uTextTitle.ReadOnly = false;
                this.uTextTitle.Appearance.BackColor = Color.PowderBlue;
                this.uComboPlant.ReadOnly = false;
                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uTextCreateUserID.ReadOnly = false;
                this.uTextCreateUserID.Appearance.BackColor = Color.PowderBlue;
                this.uTextApproveUserID.ReadOnly = false;
                this.uTextApproveUserID.Appearance.BackColor = Color.PowderBlue;
                this.uTextEtc.ReadOnly = false;
                this.uTextEtc.Appearance.BackColor = Color.White;
                this.uDateApplyFromDate.ReadOnly = false;
                this.uDateApplyFromDate.Appearance.BackColor = Color.White;
                this.uDateApplyToDate.ReadOnly = false;
                this.uDateApplyToDate.Appearance.BackColor = Color.White;
                this.uDateCompleteDate.ReadOnly = false;
                this.uDateCompleteDate.Appearance.BackColor = Color.White;
                this.uDateCreateDate.ReadOnly = false;
                this.uDateCreateDate.Appearance.BackColor = Color.White;
                this.uTextGRWComment.ReadOnly = false;
                this.uTextGRWComment.Appearance.BackColor = Color.White;
                //this.uTextMakeDeptName.ReadOnly = false;
                //this.uTextMakeDeptName.Appearance.BackColor = Color.White;
                //this.uTextMakeUserName.ReadOnly = false;
                //this.uTextMakeUserName.Appearance.BackColor = Color.White;
                //this.uTextPackage.ReadOnly = false;
                //this.uTextPackage.Appearance.BackColor = Color.White;
                //this.uTextPadsize.ReadOnly = false;
                //this.uTextPadsize.Appearance.BackColor = Color.White;
                //this.uTextChipsize.ReadOnly = false;
                //this.uTextChipsize.Appearance.BackColor = Color.White;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        //그리드 수정 가능
        private void GridWritable()
        {
            try
            {
                this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
                this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
                this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
                this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;

                WinGrid wGrid = new WinGrid();
                ////for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                ////{
                ////    this.uGrid1.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid1.Rows[i].Cells["FileName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid1.Rows[i].Cells["FileTitle"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid1.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////}
                ////for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                ////{
                ////    this.uGrid2.Rows[i].Cells["Seq"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid2.Rows[i].Cells["VendorCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid2.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid2.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////}
                ////for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                ////{
                ////    this.uGrid3.Rows[i].Cells["ProcessGroup"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid3.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////}
                ////for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                ////{
                ////    this.uGrid4.Rows[i].Cells["AgreeUserID"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////    this.uGrid4.Rows[i].Cells["EtcDesc"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////}

                ////this.uGrid1.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                ////this.uGrid2.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                ////this.uGrid3.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                ////this.uGrid4.DisplayLayout.Bands[0].Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;

                //// 빈줄추가
                //wGrid.mfAddRowGrid(this.uGrid1, 0);
                //wGrid.mfAddRowGrid(this.uGrid2, 0);
                //wGrid.mfAddRowGrid(this.uGrid3, 0);
                //wGrid.mfAddRowGrid(this.uGrid4, 0);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion


        #region ToolBar 메소드


        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();
                ///BL 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH clsDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(clsDocH);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();
                String strSTypeCode = this.uComboSearchSmall.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;
                String strStdNumber = this.uTextSearchStandardNo.Text;

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtHeader = clsDocH.mfReadISODocHReg(strPlantCode, strStdNumber, strLTypeCode, strMTypeCode, strSTypeCode, strDocTitle, m_resSys.GetString("SYS_LANG"));

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                this.uGridHeader.DataSource = dtHeader;
                this.uGridHeader.DataBind();

                if (dtHeader.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridHeader, 0);
                }
                this.uGroupBoxContentsArea.Expanded = false;

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            titleArea.Focus();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPBrowser brwChannel = new QRPBrowser();
                //헤더 BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH isoDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(isoDocH);

                DataTable dtCheck = new DataTable();
                DataTable dtISODocH = new DataTable();
                DataTable dtSaveVendor = new DataTable();
                DataTable dtDelVendor = new DataTable();
                DataTable dtSaveDept = new DataTable();
                DataTable dtDelDept = new DataTable();
                DataTable dtSaveAgreeUser = new DataTable();
                DataTable dtDelAgreeUser = new DataTable();
                DataTable dtSaveFile = new DataTable();
                DataTable dtDelFile = new DataTable();
                DataRow row;

                //// 결재선 팝업창
                //QRPCOM.UI.frmCOM0012 frm = new QRPCOM.UI.frmCOM0012();
                QRPCOM.UI.frmCOM0013 frm = new QRPCOM.UI.frmCOM0013();
                DialogResult _dr = new DialogResult();

                //개정번호 입력시 사용 가능한 개정번호인지 검색
                DataTable dtCheckStdNum = new DataTable();
                String strVersionCheck = "";
                String strAdmitCheck = "";
                String strRevTypeCheck = "";
                if (this.uTextStandardDocNo.Text != "")
                {
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strVersionNum = this.uTextVersionNum.Text;
                    String strStdNumber = this.uTextStandardDocNo.Text;
                    dtCheckStdNum = isoDocH.mfReadISODocHForReWrite(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    if (dtCheckStdNum.Rows.Count > 0)
                    {
                        strVersionCheck = dtCheckStdNum.Rows[0]["VersionNum"].ToString();
                        strAdmitCheck = dtCheckStdNum.Rows[0]["AdmitStatusCode"].ToString();
                        strRevTypeCheck = dtCheckStdNum.Rows[0]["RevDisuseType"].ToString();
                    }
                }

                #region 필수확인
                int checkfile = 0;
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (this.uGrid1.Rows[i].Cells["FileTitle"].Value.ToString() == "")
                    {
                        checkfile++;
                    }
                    //\ / :  * ? " < > | 
                    //else if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("/")
                    //        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("#")
                    //        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("*")
                    //        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains("<")
                    //        || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(">"))
                    //{
                    //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                        , "저장 확인", "첨부파일 유효성 검사", "첨부파일에 파일명으로 사용할수 없는 특수문자를 포함하고 있습니다.", Infragistics.Win.HAlign.Right);

                    //    return;
                    //}
                }
                //승인요청중인 문서는 수정할 수 없도록 처리
                if (m_strAdmitStatus != "" && m_strAdmitStatus.Equals("AR"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001053", "M000766", Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (checkfile > 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001230", "M001146", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (this.uTextStandardDocNo.ReadOnly == false && this.uTextStandardDocNo.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001230", "M000165", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextStandardDocNo.Focus();
                    return;
                }
                else if (this.uTextVersionNum.ReadOnly == false && this.uTextVersionNum.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                 , "M001264", "M001230", "M001207", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextVersionNum.Focus();
                    return;
                }

                else if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uComboLarge.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001230", "M000370", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uComboLarge.DropDown();
                    return;
                }
                else if (this.uComboMiddle.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001230", "M001124", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uComboMiddle.DropDown();
                    return;
                }
                else if (this.uTextTitle.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001230", "M000409", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextTitle.Focus();
                    return;
                }
                else if (this.uTextCreateUserID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                , "M001264", "M001230", "M001000", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextCreateUserID.Focus();
                    return;
                }
                //else if (this.uTextApproveUserID.Text == "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //    , "M001264", "M001230", "승인자를 입력해주세요", Infragistics.Win.HAlign.Center);

                //    //Focus
                //    this.uTextApproveUserID.Focus();
                //    return;
                //}

                //else if (dtCheckStdNum.Rows.Count > 0 && this.uTextStandardDocNo.Text != "")
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //    , "M001264", "M001230", "입력하신 개정번호는 사용중인 개정번호입니다.", Infragistics.Win.HAlign.Center);

                //    //Focus
                //    this.uTextStandardDocNo.Focus();
                //    return;
                //}
                else if (dtCheckStdNum.Rows.Count > 0 && this.uComboPlant.ReadOnly == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M001230", "M000348", Infragistics.Win.HAlign.Right);

                    //Focus
                    this.uTextStandardDocNo.Focus();
                    return;
                }

                #endregion
                else
                {
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                    DataTable dtLType = clsLType.mfReadISODocLTypeGRWFlag(this.uComboPlant.Value.ToString(), this.uComboLarge.Value.ToString());
                    /////////////////////////////////////전자결재여부(GRWFlag)가 N인 대분류의 기안은 지정된 사원만 가능하도록(邹美娟, 梁琪)////////////////////////////////////////////
                    //if (dtLType.Rows[0]["GRWFlag"].ToString().Equals("N"))
                    //{
                    //    if (!m_resSys.GetString("SYS_USERID").Equals("PS000856") && !m_resSys.GetString("SYS_USERID").Equals("20500106"))
                    //    {
                    //        // XXX 님은 해당 표준문서를 등록하실 수 없습니다.
                    //        msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                    //                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG"))
                    //                            , msg.GetMessge_Text("M001533", m_resSys.GetString("SYS_LANG")), m_resSys.GetString("SYS_USERID") + msg.GetMessge_Text("M001534", m_resSys.GetString("SYS_LANG"))
                    //                            , Infragistics.Win.HAlign.Right);

                    //        return;
                    //    }
                    //}
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //콤보박스 선택값 Validation Check//////////
                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this)) return;
                    ///////////////////////////////////////////

                    DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.ReqAgreeSave, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001053", "M000936",
                        Infragistics.Win.HAlign.Right);

                    if (DResult == DialogResult.Yes || DResult == DialogResult.No)
                    {
                        #region 결재선 팝업창 호출 중간저장일 경우 호출하지 않는다.
                        if (DResult == DialogResult.Yes)
                        {
                            //전자결제 여부가 N일 경우 호출하지 않는다. 전자결제 여부가 N일 경우 해당 사번만 저장 가능하다(ST02886A 김대진, ST00176B 전영미)
                            if (dtLType.Rows[0]["GRWFlag"].ToString().Equals("Y"))
                            {
                                //결재선 팝업창 호출
                                //frm.ApprovalUserID = uTextApproveUserID.Text;
                                //frm.ApprovalUserName = uTextApproveUserName.Text;
                                //frm.ApprovalUserID = "ST00176B";
                                //frm.ApprovalUserName = "전영미";
                                frm.WriteID = uTextCreateUserID.Text;
                                frm.WriteName = uTextCreateUserName.Text;
                                //frm.RIUserID = "ST00176B";
                                //frm.RIUserName = "전영미";

                                _dr = frm.ShowDialog();
                                if (_dr == DialogResult.No || _dr == DialogResult.Cancel)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001030", Infragistics.Win.HAlign.Center);

                                    return;
                                }
                            }
                        }
                        #endregion

                        #region 데이터

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;
                        System.Windows.Forms.DialogResult result;

                        //FileServer 연결
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0006");

                        dtISODocH = isoDocH.mfSetDateInfo();

                        row = dtISODocH.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();

                        if (this.uTextStandardDocNo.ReadOnly == false)
                        {
                            row["VersionNum"] = uTextVersionNum.Text;
                        }
                        else
                        {
                            row["VersionNum"] = "000";
                        }
                        row["ProcessCode"] = this.uComboProcess.Value.ToString();
                        row["StdNumber"] = this.uTextStandardDocNo.Text;
                        row["LTypeCode"] = this.uComboLarge.Value.ToString();
                        row["MTypeCode"] = this.uComboMiddle.Value.ToString();
                        row["STypeCode"] = this.uComboSmall.Value.ToString();
                        row["WriteID"] = this.uTextCreateUserID.Text;
                        row["MakeUserName"] = this.uTextMakeUserName.Text;
                        row["MakeDeptName"] = this.uTextMakeDeptName.Text;
                        row["WriteDate"] = this.uDateCreateDate.Value.ToString();
                        row["AdmitID"] = this.uTextApproveUserID.Text;
                        row["DocTitle"] = this.uTextTitle.Text;
                        row["DocKeyword"] = this.uTextDocKeyword.Text;
                        row["DocDesc"] = this.uTextEtc.Text;
                        row["GRWComment"] = this.uTextGRWComment.Text;
                        //row["GRWComment"] = frm.Comment;
                        row["ApplyDateFrom"] = this.uDateApplyFromDate.Value.ToString();
                        row["ApplyDateTo"] = this.uDateApplyToDate.Value.ToString();
                        row["CompleteDate"] = this.uDateCompleteDate.Value.ToString();
                        row["ReturnReason"] = this.uTextReturnReason.Text;
                        row["Package"] = this.uTextPackage.Text;
                        row["ChipSize"] = this.uTextChipsize.Text;
                        row["PadSize"] = this.uTextPadsize.Text;

                        if (DResult == DialogResult.Yes)
                        {
                            row["AdmitStatus"] = "AR";
                        }

                        else if (DResult == DialogResult.No)
                        {
                            //row["AdmitStatus"] = "WR";
                            row["AdmitStatus"] = "FN";
                        }
                        else
                        {
                            return;
                        }

                        dtISODocH.Rows.Add(row);

                        String strVersion = row["VersionNum"].ToString();

                        //    //////Vendor//////
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                        QRPISO.BL.ISODOC.ISODocVendor Vendor = new QRPISO.BL.ISODOC.ISODocVendor();
                        brwChannel.mfCredentials(Vendor);

                        dtSaveVendor = Vendor.mfSetDateInfo();
                        dtDelVendor = Vendor.mfSetDateInfo();

                        for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                        {
                            this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];
                            if (this.uGrid2.Rows[i].Hidden == false)
                            {
                                row = dtSaveVendor.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                //row["StdNumber"] = StdNumber;

                                row["VersionNum"] = strVersion;

                                row["Seq"] = this.uGrid2.Rows[i].RowSelectorNumber;
                                row["VendorCode"] = this.uGrid2.Rows[i].Cells["VendorCode"].Value.ToString();
                                row["DeptName"] = this.uGrid2.Rows[i].Cells["DeptName"].Value.ToString();
                                row["PersonName"] = this.uGrid2.Rows[i].Cells["PersonName"].Value.ToString();
                                row["Hp"] = this.uGrid2.Rows[i].Cells["Hp"].Value.ToString();
                                row["Email"] = this.uGrid2.Rows[i].Cells["Email"].Value.ToString();
                                row["EtcDesc"] = this.uGrid2.Rows[i].Cells["EtcDesc"].Value.ToString();

                                dtSaveVendor.Rows.Add(row);
                            }
                            else
                            {
                                row = dtDelVendor.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();  //this.uGrid2.ActiveRow.Cells["PlantCode"].Value.ToString();
                                row["StdNumber"] = this.uTextStandardDocNo.Text;
                                row["VersionNum"] = strVersion;
                                row["Seq"] = Convert.ToInt32(this.uGrid2.Rows[i].Cells["Seq"].Value);

                                dtDelVendor.Rows.Add(row);
                            }
                        }

                        /////AgreeUser//////
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                        QRPISO.BL.ISODOC.ISODocAgreeUser AgreeUser = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                        brwChannel.mfCredentials(AgreeUser);

                        dtSaveAgreeUser = AgreeUser.mfSetDateInfo();
                        dtDelAgreeUser = AgreeUser.mfSetDateInfo();

                        for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                        {
                            this.uGrid4.ActiveCell = this.uGrid4.Rows[0].Cells[0];
                            if (this.uGrid4.Rows[i].Hidden == false)
                            {
                                row = dtSaveAgreeUser.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                row["VersionNum"] = strVersion;

                                row["AgreeUserID"] = this.uGrid4.Rows[i].Cells["AgreeUserID"].Value.ToString();
                                row["EtcDesc"] = this.uGrid4.Rows[i].Cells["EtcDesc"].Value.ToString();

                                dtSaveAgreeUser.Rows.Add(row);
                            }
                            else
                            {
                                row = dtDelAgreeUser.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                row["StdNumber"] = this.uTextStandardDocNo.Text;
                                row["VersionNum"] = strVersion;
                                row["AgreeUserID"] = uGrid4.Rows[i].Cells["AgreeUserID"].Value.ToString();

                                dtDelAgreeUser.Rows.Add(row);
                            }
                        }


                        ///////Dept////////
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                        QRPISO.BL.ISODOC.ISODocDept Dept = new QRPISO.BL.ISODOC.ISODocDept();
                        brwChannel.mfCredentials(Dept);

                        dtSaveDept = Dept.mfSetDateInfo();
                        dtDelDept = Dept.mfSetDateInfo();

                        for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                        {
                            this.uGrid3.ActiveCell = this.uGrid3.Rows[0].Cells[0];
                            if (this.uGrid3.Rows[i].Hidden == false)
                            {
                                row = dtSaveDept.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                //row["StdNumber"] = StdNumber;
                                row["VersionNum"] = strVersion;
                                row["ProcessGroup"] = this.uGrid3.Rows[i].Cells["ProcessGroup"].Value.ToString();
                                row["EtcDesc"] = this.uGrid3.Rows[i].Cells["EtcDesc"].Value.ToString();

                                dtSaveDept.Rows.Add(row);
                            }
                            else
                            {
                                row = dtDelDept.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                row["StdNumber"] = this.uTextStandardDocNo.Text;
                                row["VersionNum"] = strVersion;
                                row["ProcessGroup"] = this.uGrid3.Rows[i].Cells["ProcessGroup"].Value.ToString();
                                dtDelDept.Rows.Add(row);
                            }
                        }
                        #endregion

                        #region 파일
                        /////////////////File//////////////////////////
                        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                        QRPISO.BL.ISODOC.ISODocFile DFile = new QRPISO.BL.ISODOC.ISODocFile();
                        brwChannel.mfCredentials(DFile);

                        //파일명 일괄적으로변경
                        String[] strFile = new String[this.uGrid1.Rows.Count];
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty) && this.uGrid1.Rows[i].Hidden == false)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037"
                                    , this.uGrid1.Rows[i].RowSelectorNumber + "M000530", Infragistics.Win.HAlign.Right);

                                this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["FileName"];
                                return;
                            }
                            else
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                                {
                                    FileInfo fileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                    strFile[i] = fileDoc.Name;

                                }
                                else
                                {
                                    if (!this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Equals(string.Empty))
                                    {
                                        String fName = this.uGrid1.Rows[i].Cells["FileName"].Value.ToString();
                                        String[] splitName = fName.Split(new char[] { '-' });

                                        if (splitName.Length > 5)
                                        {
                                            for (int j = 4; j < splitName.Length; j++)
                                            {
                                                if (!j.Equals(splitName.Length - 1))
                                                    strFile[i] = strFile[i] + splitName[j] + "-";
                                                else
                                                    strFile[i] = strFile[i] + splitName[j];
                                            }
                                        }
                                        else
                                        {
                                            strFile[i] = splitName[4];
                                        }
                                    }
                                }
                            }
                        }

                        dtSaveFile = DFile.mfSetDatainfo();
                        dtDelFile = DFile.mfSetDatainfo();
                        //Seq 생성
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (this.uGrid1.Rows[i].Cells["Seq"].Value.ToString().Equals("0"))
                            {
                                if (this.uGrid1.Rows[i].Hidden == false)
                                {
                                    if (this.uGrid1.Rows[i].RowSelectorNumber.Equals(1))
                                    {
                                        this.uGrid1.Rows[i].Cells["Seq"].Value = "1";
                                    }
                                    else
                                    {
                                        this.uGrid1.Rows[i].Cells["Seq"].Value = Convert.ToString(Convert.ToInt32(this.uGrid1.Rows[i - 1].Cells["Seq"].Value.ToString()) + 1);
                                    }
                                }
                            }
                        }
                        //DataTable에 저장값 담기
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {

                            this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];
                            if (this.uGrid1.Rows[i].Hidden == false)
                            {
                                row = dtSaveFile.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                row["VersionNum"] = strVersion;
                                row["Seq"] = this.uGrid1.Rows[i].Cells["Seq"].Value;
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString() == "")
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "M001264", "M001230", "M001140", Infragistics.Win.HAlign.Center);

                                    return;
                                }
                                else
                                {
                                    row["FileTitle"] = this.uGrid1.Rows[i].Cells["FileTitle"].Value.ToString();
                                }
                                row["FileName"] = strFile[i];

                                if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                                {
                                    row["DWFlag"] = "T";
                                }
                                else
                                {
                                    row["DWFlag"] = "F";
                                }
                                row["EtcDesc"] = this.uGrid1.Rows[i].Cells["EtcDesc"].Value.ToString();

                                dtSaveFile.Rows.Add(row);
                            }
                            else
                            {
                                row = dtDelFile.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                row["StdNumber"] = this.uTextStandardDocNo.Text;
                                row["VersionNum"] = strVersion;
                                row["Seq"] = Convert.ToInt32(this.uGrid1.Rows[i].Cells["Seq"].Value);
                                dtDelFile.Rows.Add(row);
                            }
                        }
                        #endregion
                        if (dtSaveFile.Rows.Count == 0 && DResult == DialogResult.Yes && this.uComboLarge.Value.ToString() != "Cust_Spec")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001230", "M001143", Infragistics.Win.HAlign.Center);

                            return;
                        }
                        //처리로직
                        //저장 함수 호출
                        string rtMSG = isoDocH.mfSaveISODocH(dtISODocH, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveVendor, dtDelVendor
                                            , dtSaveAgreeUser, dtDelAgreeUser, dtSaveDept, dtDelDept, dtSaveFile, dtDelFile);

                        //decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                        //처리로직끝
                        //this.MdiParent.Cursor = Cursors.Default;
                        //m_ProgressPopup.mfCloseProgressPopup(this);

                        String strRtnStdNumber;

                        if (this.uTextStandardDocNo.Text == "")
                        {
                            strRtnStdNumber = ErrRtn.mfGetReturnValue(0);
                        }
                        else
                        {
                            strRtnStdNumber = this.uTextStandardDocNo.Text;
                        }

                        //메시지 박스

                        if (ErrRtn.ErrNum == 0)
                        {
                            #region 파일업로드
                            //File UpLoad
                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            //파일 이름 변경
                            for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                            {
                                if (this.uGrid1.Rows[i].Hidden == false)
                                {
                                    if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                                    {
                                        FileInfo fileDoc = new FileInfo(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                        string strUploadFile = fileDoc.DirectoryName + "\\" + strRtnStdNumber + "-"
                                                                + strVersion + "-" + this.uGrid1.Rows[i].Cells["Seq"].Value.ToString() + "-"
                                                                + this.uComboPlant.Value + "-" + fileDoc.Name;

                                        if (File.Exists(strUploadFile))
                                            File.Delete(strUploadFile);

                                        File.Copy(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString(), strUploadFile);
                                        arrFile.Add(strUploadFile);
                                    }
                                }
                            }

                            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (strRtnStdNumber + "-" + strVersion),
                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            if (arrFile.Count != 0)
                            {
                                //fileAtt.ShowDialog();
                                //fileAtt.mfFileUploadNoProgView();
                                fileAtt.mfFileUpload_NonAssync();
                            }

                            #endregion

                            #region 그룹웨어 전송 중간저장일경우는 전자결제 연동하지 않는다.
                            if (DResult == DialogResult.Yes)
                            {
                                //전자결제 여부가 N일경우는 호출하지 않는다.
                                if (dtLType.Rows[0]["GRWFlag"].ToString().Equals("Y"))
                                {
                                    dtISODocH.Rows[0]["StdNumber"] = strRtnStdNumber;
                                    dtISODocH.Rows[0]["GRWComment"] = frm.Comment;

                                    // 테스트 코드 -- 업로드시 주석 처리
                                    ////frm.dtSendLine.Rows[0]["CD_USERKEY"] = "tica100";
                                    ////frm.dtSendLine.Rows[1]["CD_USERKEY"] = "tica100";
                                    ////frm.dtSendLine.Rows[2]["CD_USERKEY"] = "mes_pjt12";

                                    //DataTable dtRtn = isoDocH.mfISODOCGRWApproval(dtISODocH, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), frm.Comment);
                                    DataTable dtRtn = mfISODOCGRWApproval(dtISODocH, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                                    if (dtRtn == null) // 실패
                                    {
                                        this.MdiParent.Cursor = Cursors.Default;
                                        m_ProgressPopup.mfCloseProgressPopup(this);
                                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001135", "M001037", "M000328", Infragistics.Win.HAlign.Center);

                                        return;
                                    }
                                    else if (dtRtn.Rows.Count.Equals(0)) // 실패
                                    {
                                        this.MdiParent.Cursor = Cursors.Default;
                                        m_ProgressPopup.mfCloseProgressPopup(this);
                                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001135", "M001037", "M000328", Infragistics.Win.HAlign.Center);
                                 
                                        return;
                                    }
                                    else if (!dtRtn.Rows[0]["CD_CODE"].Equals("00") || !Convert.ToBoolean(dtRtn.Rows[0]["CD_STATUS"].ToString())) // 실패
                                    {
                                        this.MdiParent.Cursor = Cursors.Default;
                                        m_ProgressPopup.mfCloseProgressPopup(this);
                                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , "M001135", "M001037", dtRtn.Rows[0]["MSG"].ToString(), Infragistics.Win.HAlign.Center);

                                        return;
                                    }
                                }
                            }
                            #endregion

                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                           Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                           "M001135", "M001037", "M000930",
                                                           Infragistics.Win.HAlign.Right);

                            this.uGroupBoxContentsArea.Expanded = false;
                            InitValue();
                            mfSearch();
                        }



                        else
                        {
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M001030",
                                                Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 테스트용 사용금지
        public DataTable mfISODOCGRWApproval(DataTable dtISODocH, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        {
            // LegacyKey, DocNo 생성시 "||" ( double pipe ) 로 구분

            DataTable dtRtn = new DataTable();

            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                //헤더 BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPGRW.IF.QRPGRW), "QRPGRW");
                QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
                brwChannel.mfCredentials(grw);

                // LegacyKey 
                string strPlantCode = dtISODocH.Rows[0]["PlantCode"].ToString();
                string strStdNumber = dtISODocH.Rows[0]["StdNumber"].ToString();
                string strVersionNum = dtISODocH.Rows[0]["VersionNum"].ToString();
                string strLegacyKey = strPlantCode + "||" + strStdNumber + "||" + strVersionNum;

                // 첨부파일 조회
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                QRPISO.BL.ISODOC.ISODocFile file = new QRPISO.BL.ISODOC.ISODocFile();
                brwChannel.mfCredentials(file);

                DataTable dtFile = file.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, "KOR");

                // Brains 전송용 DataTable
                DataTable dtFileinfo = grw.mfSetFileInfoDataTable();

                // 표준문서 기본 파일경로 
                string strFilePath = "ISODocFile\\";

                // Brains 전송용 DataTable 만들기
                foreach (DataRow dr in dtFile.Rows)
                {
                    DataRow _dr = dtFileinfo.NewRow();
                    _dr["PlantCode"] = dr["PlantCode"].ToString();
                    _dr["NM_LOCATION"] = strFilePath + strStdNumber + "-" + strVersionNum;  // 표준문서 기본 경로 + 문서별 경로
                    _dr["NM_FILE"] = dr["FileName"].ToString();                             // 파일명
                    dtFileinfo.Rows.Add(_dr);
                }

                if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals(string.Empty)) // 생성
                {
                    string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
                    string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
                    string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
                    string strComment = dtISODocH.Rows[0]["DocDesc"].ToString();

                    dtRtn = grw.CreateISODOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strDocUser, strComment, dtFormInfo, dtFileinfo, strUserIP, strUserID);
                }
                else if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals("1")) // 개정
                {
                    string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
                    string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
                    string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
                    string strComment = dtISODocH.Rows[0]["DocDesc"].ToString();
                    string strDocChange = dtISODocH.Rows[0]["ChangeItem"].ToString();
                    string strFrom = dtISODocH.Rows[0]["FromDesc"].ToString();
                    string strTo = dtISODocH.Rows[0]["ToDesc"].ToString();

                    dtRtn = grw.ModifyISIDOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strDocUser, strComment, strDocChange, strFrom, strTo, dtFormInfo, dtFileinfo, strUserIP, strUserID);
                }
                else if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals("2")) // 폐기
                {
                    string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
                    string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
                    string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
                    string strComment = dtISODocH.Rows[0]["DocDesc"].ToString();
                    string strReason = dtISODocH.Rows[0]["DisuseReason"].ToString();

                    dtRtn = grw.CancelISODOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strReason, strDocUser, strComment, dtFormInfo, dtFileinfo, strUserIP, strUserID);
                }
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            return dtRtn;
        }
        #endregion

        public void mfDelete()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();
                //표준문서등록에서 "작성중"이거나 "반려"중인 거은 "삭제"가 가능하도록 처리
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000640",
                                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (m_strAdmitStatus.Equals("AR"))
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000764",
                                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                    QRPISO.BL.ISODOC.ISODocH clsDocH = new QRPISO.BL.ISODOC.ISODocH();
                    brwChannel.mfCredentials(clsDocH);

                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strStdNumber = this.uTextStandardDocNo.Text;

                    DataTable dtDelDoc = clsDocH.mfSetDateInfo();
                    DataRow dr;

                    dr = dtDelDoc.NewRow();

                    dr["PlantCode"] = strPlantCode;
                    dr["StdNumber"] = strStdNumber;
                    dr["VersionNum"] = m_strVersionNum;

                    dtDelDoc.Rows.Add(dr);

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001264", "M000650", "M000688",
                                              Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string rtMSG = clsDocH.mfDeleteISODocH(dtDelDoc);

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                          "M001135", "M000638", "M000926",
                                          Infragistics.Win.HAlign.Right);
                            InitValue();
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                          "M001135", "M000638", "M000923",
                                          Infragistics.Win.HAlign.Right);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                InitValue();
                SetWritable();
                GridWritable();
                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                //처리로직
                WinGrid grd = new WinGrid();
                //엑셀저장함수호출
                grd.mfDownLoadGridToExcel(this.uGridHeader);
            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트들...
        // 첨부파일 그리드 행삭제 버튼 이벤트


        private void uButtonDeleteRow1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!m_strAdmitStatus.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid1.Rows[i].Delete(false);
                            i--;
                            //this.uGrid1.Rows[i].Hidden = true;
                            //if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                            //{
                            //    this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                            //}
                        }
                    }
                    if (CheckDWFlag() == false)
                    {
                        AutoTopFlag();
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 배포업체 그리드 행삭제 버튼 이벤트


        private void uButtonDeleteRow2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!m_strAdmitStatus.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid2.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow3_Click(object sender, EventArgs e)
        {
            try
            {
                if (!m_strAdmitStatus.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid3.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid3.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid3.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDeleteRow4_Click(object sender, EventArgs e)
        {
            try
            {
                if (!m_strAdmitStatus.Equals("AR"))
                {
                    for (int i = 0; i < this.uGrid4.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGrid4.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGrid4.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        // 자동 행삭제와 수정시 rowselector 이미지를 변하게 하기 위한 이벤트

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
                if (e.Cell.Column.Key == "FileName")
                {
                    if (e.Cell.Value.ToString().Contains(":\\"))
                    {
                        FileInfo fileDoc = new FileInfo(e.Cell.Value.ToString());
                        e.Cell.Row.Cells["FileTitle"].Value = fileDoc.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 자동 행삭제와 수정시 rowselector 이미지를 변하게 하기 위한 이벤트

        private void uGrid3_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid3, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid4_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid4, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion


        //공장코드 콤보박스 변경시 공정, 대분류코드 콤보박스 이벤트
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                WinComboEditor wComboLType = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                DataTable dtLType = new DataTable();

                String strPlantCode = this.uComboPlant.Value.ToString();

                this.uComboProcess.Items.Clear();
                this.uComboLarge.Items.Clear();

                strPlantCode = this.uComboPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                    QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                    brwChannel.mfCredentials(clsLType);

                    dtLType = clsLType.mfReadISODocLTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
                wCombo.mfSetComboEditor(this.uComboLarge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                       , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                       , Infragistics.Win.HAlign.Left, "", "", "선택", "LTypeCode", "LTypeName", dtLType);







            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                this.uComboProcess.Appearance.BackColor = Color.PowderBlue;
                this.uComboLarge.Appearance.BackColor = Color.PowderBlue;
            }
        }


        //대분류코드 콤보박스 변경 이벤트
        private void uComboLarge_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();
                //중분류 변경
                DataTable dtMType = new DataTable();

                String strPlantCode = this.uComboPlant.Value.ToString();
                String strLTypeCode = this.uComboLarge.Value.ToString();

                this.uComboMiddle.Items.Clear();
                this.uTextChipsize.Text = "";
                this.uTextPackage.Text = "";
                this.uTextPadsize.Text = "";

                if (strLTypeCode != "")
                {
                    //중분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                    QRPISO.BL.ISODOC.ISODocMType clsMType = new QRPISO.BL.ISODOC.ISODocMType();
                    brwChannel.mfCredentials(clsMType);

                    dtMType = clsMType.mfReadISODocMTypeCombo(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboMiddle, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "MTypeCode", "MTypeName", dtMType);

                //표준번호 TextBox 변경
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH clsCheck = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(clsCheck);

                DataTable dtCheck = clsCheck.mfReadLTypeUse(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));

                if (dtCheck.Rows.Count > 0)
                {
                    if (this.uComboLarge.ReadOnly == false)
                    {
                        this.uTextStandardDocNo.Text = "";
                        this.uTextStandardDocNo.ReadOnly = false;
                        this.uTextStandardDocNo.Appearance.BackColor = Color.PowderBlue;
                        this.uTextVersionNum.Text = "";
                        this.uTextVersionNum.ReadOnly = false;
                        this.uTextVersionNum.Appearance.BackColor = Color.PowderBlue;
                    }
                }
                else
                {
                    this.uTextStandardDocNo.Text = "";
                    this.uTextStandardDocNo.ReadOnly = true;
                    this.uTextStandardDocNo.Appearance.BackColor = Color.Gainsboro;
                    this.uTextVersionNum.Text = "";
                    this.uTextVersionNum.ReadOnly = true;
                    this.uTextVersionNum.Appearance.BackColor = Color.Gainsboro;
                }

                //대분류가 임시표준일 경우 날짜 변경
                if (this.uComboLarge.Value.ToString().Equals("ECN"))
                {
                    this.uDateApplyFromDate.Value = DateTime.Now;
                    this.uDateApplyToDate.Value = DateTime.Now.AddMonths(1);
                    this.uDateApplyFromDate.Visible = true;
                    this.uDateApplyToDate.Visible = true;
                    this.ultraLabel2.Visible = true;
                    this.uLabelApplyDate.Visible = true;
                }
                else
                {
                    this.uDateApplyFromDate.Value = DateTime.Now;
                    this.uDateApplyToDate.Value = DateTime.Now;
                    this.uDateApplyFromDate.Visible = false;
                    this.uDateApplyToDate.Visible = false;
                    this.ultraLabel2.Visible = false;
                    this.uLabelApplyDate.Visible = false;
                }

                //고객사표준일경우 ChipSize, PadSize, Package 상태 변경, 작성자명 활성화
                if (this.uComboLarge.Value.ToString().Equals("Cust_Spec"))
                {

                    this.uTextPadsize.Visible = true;
                    this.uTextPackage.Visible = true;
                    this.uTextChipsize.Visible = true;
                    this.uLabelPackage.Visible = true;
                    this.uLabelPadsize.Visible = true;
                    this.uLabelChipSize.Visible = true;
                    this.uTextMakeUserName.Appearance.BackColor = Color.White;
                    this.uTextMakeUserName.ReadOnly = false;
                }
                else
                {
                    this.uTextPadsize.Visible = false;
                    this.uTextPackage.Visible = false;
                    this.uTextChipsize.Visible = false;
                    this.uLabelChipSize.Visible = false;
                    this.uLabelPadsize.Visible = false;
                    this.uLabelPackage.Visible = false;
                    this.uTextMakeUserName.Appearance.BackColor = Color.Gainsboro;
                    this.uTextMakeUserName.ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                this.uComboMiddle.Appearance.BackColor = Color.PowderBlue;
            }
        }

        private void uComboMiddle_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtSType = new DataTable();

                String strPlantCode = this.uComboPlant.Value.ToString();
                String strLTypeCode = this.uComboLarge.Value.ToString();
                String strMTypeCode = this.uComboMiddle.Value.ToString();

                this.uComboSmall.Items.Clear();

                if (strMTypeCode != "")
                {
                    //소분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
                    QRPISO.BL.ISODOC.ISODocSType clsSType = new QRPISO.BL.ISODOC.ISODocSType();
                    brwChannel.mfCredentials(clsSType);

                    dtSType = clsSType.mfReadISODocSTypeCombo(strPlantCode, strLTypeCode, strMTypeCode, m_resSys.GetString("SYS_LANG"));

                    if (dtSType.Rows.Count > 0)
                    {
                        this.uComboSmall.Appearance.BackColor = Color.PowderBlue;
                    }
                    else
                    {
                        this.uComboSmall.Appearance.BackColor = Color.White;
                    }
                }
                wCombo.mfSetComboEditor(this.uComboSmall, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "STypeCode", "STypeName", dtSType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid4_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (!m_strAdmitStatus.Equals("AR"))
                {
                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 frmAUser = new frmPOP0011();
                    frmAUser.PlantCode = uComboPlant.Value.ToString();
                    frmAUser.ShowDialog();

                    this.uGrid4.ActiveRow.Cells["AgreeUserID"].Value = frmAUser.UserID;
                    this.uGrid4.ActiveRow.Cells["AgreeUserName"].Value = frmAUser.UserName;

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }


        private void uTextApproveUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (this.uTextApproveUserID.ReadOnly == false)
                {
                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 frmAgreeUserID = new frmPOP0011();
                    frmAgreeUserID.PlantCode = uComboPlant.Value.ToString();
                    frmAgreeUserID.ShowDialog();
                    this.uTextApproveUserID.Text = frmAgreeUserID.UserID;
                    this.uTextApproveUserName.Text = frmAgreeUserID.UserName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISO0003C_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfLoadGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid2_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (!m_strAdmitStatus.Equals("AR"))
                {
                    frmPOP0020 frmVendor = new frmPOP0020();
                    frmVendor.PlantCode = this.uComboPlant.Value.ToString();
                    frmVendor.ShowDialog();

                    this.uGrid2.ActiveRow.Cells["VendorCode"].Value = frmVendor.VendorCode;
                    this.uGrid2.ActiveRow.Cells["VendorName"].Value = frmVendor.VendorName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uGrid2_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
        }

        private void uGrid1_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                if (e.Cell.Column.Key == "DWFlag")
                {
                    if (Convert.ToBoolean(e.Cell.Value) == true)
                    {
                        e.Cell.Value = false;
                    }
                    else
                    {
                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                            {
                                this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                                e.Cell.Value = true;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid1_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (!m_strAdmitStatus.Equals("AR"))
                {
                    System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "All files (*.*)|*.*";
                    openFile.FilterIndex = 1;
                    openFile.RestoreDirectory = true;

                    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        string strImageFile = openFile.FileName;
                        e.Cell.Value = strImageFile;

                        QRPGlobal grdImg = new QRPGlobal();
                        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                        AutoTopFlag();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextCreateUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextCreateUserID.ReadOnly == false)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uTextCreateUserID.Text == "")
                        {
                            this.uTextCreateUserName.Text = "";
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            WinMessageBox msg = new WinMessageBox();

                            // 공장 선택 확인
                            if (this.uComboPlant.Value.ToString() == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000266",
                                                Infragistics.Win.HAlign.Right);

                                this.uComboPlant.DropDown();
                            }
                            else
                            {
                                String strPlantCode = this.uComboPlant.Value.ToString();
                                String strWriteID = this.uTextCreateUserID.Text;

                                // UserName 검색 함수 호출
                                DataTable dtUsr = GetUsrInfo(strPlantCode, strWriteID);

                                if (dtUsr.Rows.Count == 0)
                                {
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000621",
                                                Infragistics.Win.HAlign.Right);

                                    this.uTextCreateUserID.Text = "";
                                    this.uTextCreateUserName.Text = "";
                                    this.uTextMakeUserName.Text = "";
                                    this.uTextMakeDeptName.Text = "";
                                }
                                else
                                {
                                    this.uTextCreateUserName.Text = dtUsr.Rows[0]["UserName"].ToString();
                                    this.uTextMakeUserName.Text = dtUsr.Rows[0]["UserName"].ToString();
                                    this.uTextMakeDeptName.Text = dtUsr.Rows[0]["DeptName"].ToString();
                                }
                            }
                        }
                    }

                    if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
                    {
                        if (this.uTextCreateUserID.Text.Length <= 1 || this.uTextCreateUserID.SelectedText == this.uTextCreateUserID.Text)
                        {
                            this.uTextCreateUserName.Text = "";
                            this.uTextCreateUserID.Text = "";
                            this.uTextMakeDeptName.Text = "";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }
        /// <summary>
        /// 사용자 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private DataTable GetUsrInfo(String strPlantCode, String strUserID)
        {
            DataTable dtRtnInfo = new DataTable();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                dtRtnInfo = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                return dtRtnInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtnInfo;
            }
            finally
            { }
        }

        // 작성후 엔터키로 검색
        private void uTextApproveUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextApproveUserID.ReadOnly == false)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (this.uTextApproveUserID.Text == "")
                        {
                            this.uTextApproveUserName.Text = "";
                        }
                        else
                        {
                            // SystemInfor ResourceSet
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                            WinMessageBox msg = new WinMessageBox();

                            // 공장 선택 확인
                            if (this.uComboPlant.Value.ToString() == "")
                            {
                                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000266",
                                                Infragistics.Win.HAlign.Right);

                                this.uComboPlant.DropDown();
                            }
                            else
                            {
                                String strPlantCode = this.uComboPlant.Value.ToString();
                                String strWriteID = this.uTextApproveUserID.Text;

                                // UserName 검색 함수 호출
                                String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                                if (strRtnUserName == "")
                                {
                                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000962", "M000621",
                                                Infragistics.Win.HAlign.Right);

                                    this.uTextApproveUserID.Text = "";
                                    this.uTextApproveUserName.Text = "";
                                }
                                else
                                {
                                    this.uTextApproveUserName.Text = strRtnUserName;
                                }
                            }
                        }
                    }

                    if (e.KeyCode == Keys.Back)
                    {
                        if (this.uTextApproveUserID.Text.Length <= 1 || this.uTextApproveUserID.SelectedText == this.uTextApproveUserID.Text)
                        {
                            this.uTextApproveUserName.Text = "";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 업체 선택시 담당자 선택 콤보 그리드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("VendorCode"))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPBrowser brwChannel = new QRPBrowser();

                    //VendorP 검색 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    DataTable dtVendorP = new DataTable();

                    dtVendorP = clsVendor.mfReadVendorPCombo(this.uGrid2.ActiveRow.Cells["VendorCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    WinGrid wGrid = new WinGrid();

                    e.Cell.Row.Cells["PersonName"].Column.Layout.ValueLists.Clear();
                    String strDropDownValue = "Seq,PersonName,DeptName,Hp,EMail,EtcDesc";
                    String strDropDownText = "순번,담당자,부서,전화번호,E-Mail,비고";

                    wGrid.mfSetGridCellValueGridList(this.uGrid2, 0, e.Cell.Row.Index, "PersonName", Infragistics.Win.ValueListDisplayStyle.DisplayText
                                                        , strDropDownValue, strDropDownText, "Seq", "PersonName", dtVendorP);
                    if (dtVendorP.Rows.Count == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                           , "M001264", "M000367", "M001249", Infragistics.Win.HAlign.Right);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보 선택시 나머지 값들 삽입
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid2_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                String strKey = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                String strValue = e.Cell.ValueList.GetText(e.Cell.ValueList.SelectedItemIndex);

                String strVendorCode = e.Cell.Row.Cells["VendorCode"].Value.ToString();
                int intSeq = Convert.ToInt32(strKey);

                DataTable dtVendorP = clsVendor.mfReadVendorPForGrid(strVendorCode, intSeq);

                e.Cell.Row.Cells["PersonName"].Value = dtVendorP.Rows[0]["PersonName"].ToString();
                e.Cell.Row.Cells["DeptName"].Value = dtVendorP.Rows[0]["DeptName"].ToString();
                e.Cell.Row.Cells["Hp"].Value = dtVendorP.Rows[0]["Hp"].ToString();
                e.Cell.Row.Cells["Email"].Value = dtVendorP.Rows[0]["Email"].ToString();
                e.Cell.Row.Cells["EtcDesc"].Value = dtVendorP.Rows[0]["EtcDesc"].ToString();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmISO0003C_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 작성자 검색 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextCreateUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uTextCreateUserID.ReadOnly == false)
                {
                    WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    if (uComboPlant.Value.ToString().Equals(string.Empty) || uComboPlant.SelectedIndex.Equals(0))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000266",
                                        Infragistics.Win.HAlign.Right);
                        return;
                    }
                    frmPOP0011 frmUsr = new frmPOP0011();
                    frmUsr.PlantCode = uComboPlant.Value.ToString();
                    frmUsr.ShowDialog();

                    this.uTextCreateUserID.Text = frmUsr.UserID;
                    this.uTextCreateUserName.Text = frmUsr.UserName;
                    this.uTextMakeUserName.Text = frmUsr.UserName;
                    this.uTextMakeDeptName.Text = frmUsr.DeptName;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170); //170
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridHeader.Height = 720;

                    for (int i = 0; i < uGridHeader.Rows.Count; i++)
                    {
                        uGridHeader.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        /// <summary>
        /// 공장코드 변경시 대분류 콤보박스 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocLType), "ISODocLType");
                QRPISO.BL.ISODOC.ISODocLType clsLType = new QRPISO.BL.ISODOC.ISODocLType();
                brwChannel.mfCredentials(clsLType);

                this.uComboSearchLarge.Items.Clear();
                this.uComboSearchMiddle.Items.Clear();
                this.uComboSearchSmall.Items.Clear();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                DataTable dtLtype = clsLType.mfReadISODocLTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchLarge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                                            , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                                                            , "LTypeCode", "LTypeName", dtLtype);

                this.uComboSearchPlant.Appearance.BackColor = Color.White;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        /// <summary>
        /// 대분류 콤보 변경시 중분류 콤보 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchLarge_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtMType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();

                this.uComboSearchMiddle.Items.Clear();

                if (strLTypeCode != "")
                {
                    //중분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocMType), "ISODocMType");
                    QRPISO.BL.ISODOC.ISODocMType clsMType = new QRPISO.BL.ISODOC.ISODocMType();
                    brwChannel.mfCredentials(clsMType);

                    dtMType = clsMType.mfReadISODocMTypeCombo(strPlantCode, strLTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchMiddle, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "MTypeCode", "MTypeName", dtMType);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        /// <summary>
        /// 중부류 콤보 변경시 소분류 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchMiddle_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                DataTable dtSType = new DataTable();

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strLTypeCode = this.uComboSearchLarge.Value.ToString();
                String strMTypeCode = this.uComboSearchMiddle.Value.ToString();

                this.uComboSearchSmall.Items.Clear();

                if (strMTypeCode != "")
                {
                    //소분류 콤보박스
                    //BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocSType), "ISODocSType");
                    QRPISO.BL.ISODOC.ISODocSType clsSType = new QRPISO.BL.ISODOC.ISODocSType();
                    brwChannel.mfCredentials(clsSType);

                    dtSType = clsSType.mfReadISODocSTypeCombo(strPlantCode, strLTypeCode, strMTypeCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchSmall, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                        , Infragistics.Win.HAlign.Left, "", "", "선택", "STypeCode", "STypeName", dtSType);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        /// <summary>
        /// Header그리드 더블클릭시 상세정보 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridHeader_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                InitValue();
                e.Row.Fixed = true;
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                ///BL 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH clsDocH = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(clsDocH);

                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                QRPISO.BL.ISODOC.ISODocDept clsDept = new QRPISO.BL.ISODOC.ISODocDept();
                brwChannel.mfCredentials(clsDept);

                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                QRPISO.BL.ISODOC.ISODocAgreeUser clsAgu = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                brwChannel.mfCredentials(clsAgu);

                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                QRPISO.BL.ISODOC.ISODocFile clsFile = new QRPISO.BL.ISODOC.ISODocFile();
                brwChannel.mfCredentials(clsFile);

                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                QRPISO.BL.ISODOC.ISODocVendor clsVendor = new QRPISO.BL.ISODOC.ISODocVendor();
                brwChannel.mfCredentials(clsVendor);
                //Header 검색
                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strStdNumber = e.Row.Cells["StdNumber"].Value.ToString();
                String strVersionNum = e.Row.Cells["VersionNum"].Value.ToString();
                m_strVersionNum = e.Row.Cells["VersionNum"].Value.ToString();


                DataTable dtHeader = clsDocH.mfReadISODocHForReWrite(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                if (dtHeader.Rows.Count > 0)
                {
                    m_strAdmitStatus = dtHeader.Rows[0]["AdmitStatusCode"].ToString();
                    if (m_strAdmitStatus.Equals("AR"))
                    {
                        SetDisWritable();
                    }
                    this.uGroupBoxContentsArea.Expanded = true;

                    this.uComboLarge.Value = dtHeader.Rows[0]["LTypeCode"].ToString();
                    this.uComboMiddle.Value = dtHeader.Rows[0]["MTypeCode"].ToString();
                    this.uComboSmall.Value = dtHeader.Rows[0]["STypeCode"].ToString();
                    this.uTextStandardDocNo.Text = dtHeader.Rows[0]["StdNumber"].ToString();
                    this.uTextVersionNum.Text = dtHeader.Rows[0]["VersionNum"].ToString();
                    this.uTextTitle.Text = dtHeader.Rows[0]["DocTitle"].ToString();
                    this.uComboPlant.Value = dtHeader.Rows[0]["PlantCode"].ToString();
                    this.uTextDocState.Text = dtHeader.Rows[0]["AdmitStatus"].ToString();
                    this.uTextCreateUserID.Text = dtHeader.Rows[0]["WriteID"].ToString();
                    this.uTextCreateUserName.Text = dtHeader.Rows[0]["WriteName"].ToString();
                    this.uTextApproveUserID.Text = dtHeader.Rows[0]["AdmitID"].ToString();
                    this.uTextApproveUserName.Text = dtHeader.Rows[0]["AdmitName"].ToString();
                    this.uTextEtc.Text = dtHeader.Rows[0]["DocDesc"].ToString();
                    this.uTextGRWComment.Text = dtHeader.Rows[0]["GRWComment"].ToString();
                    this.uTextDocKeyword.Text = dtHeader.Rows[0]["DocKeyword"].ToString();
                    this.uTextReturnReason.Text = dtHeader.Rows[0]["ReturnReason"].ToString();
                    this.uDateCreateDate.Value = dtHeader.Rows[0]["WriteDate"].ToString();
                    this.uDateApplyFromDate.Value = dtHeader.Rows[0]["ApplyDateFrom"].ToString();
                    this.uDateApplyToDate.Value = dtHeader.Rows[0]["ApplyDateTo"].ToString();
                    this.uTextMakeUserName.Text = dtHeader.Rows[0]["MakeUserName"].ToString();
                    this.uTextMakeDeptName.Text = dtHeader.Rows[0]["MakeDeptName"].ToString();
                    this.uTextPackage.Text = dtHeader.Rows[0]["Package"].ToString();
                    this.uTextPadsize.Text = dtHeader.Rows[0]["PadSize"].ToString();
                    this.uTextChipsize.Text = dtHeader.Rows[0]["ChipSize"].ToString();

                    //승인요청시에는 수정하지 못하도록 설정
                    if (!m_strAdmitStatus.Equals("AR"))
                    {
                        SetWritable();
                        GridWritable();
                    }
                    else
                    {
                        GridDiswritable();
                        SetDisWritable();
                    }
                    //작성중인 문서의 PK수정불가 처리
                    if (m_strAdmitStatus.Equals("WR") || m_strAdmitStatus.Equals("RE") || m_strAdmitStatus.Equals("AR"))
                    {
                        this.uComboLarge.ReadOnly = true;
                        this.uComboLarge.Appearance.BackColor = Color.Gainsboro;
                        this.uComboMiddle.ReadOnly = true;
                        this.uComboMiddle.Appearance.BackColor = Color.Gainsboro;
                        this.uComboSmall.ReadOnly = true;
                        this.uComboSmall.Appearance.BackColor = Color.Gainsboro;
                        this.uTextStandardDocNo.ReadOnly = true;
                        this.uTextStandardDocNo.Appearance.BackColor = Color.Gainsboro;
                        this.uTextVersionNum.ReadOnly = true;
                        this.uTextVersionNum.Appearance.BackColor = Color.Gainsboro;
                        this.uComboPlant.ReadOnly = true;
                        this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                    }

                    //첨부파일 검색
                    DataTable dtFile = clsFile.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));

                    if (dtFile.Rows.Count > 0)
                    {
                        uGrid1.DataSource = dtFile;
                        uGrid1.DataBind();

                        for (int i = 0; i < dtFile.Rows.Count; i++)
                        {
                            if (dtFile.Rows[i]["DWFlag"].ToString() == "T")
                            {
                                this.uGrid1.Rows[i].Cells["DWFlag"].Value = true;
                            }
                            else
                            {
                                this.uGrid1.Rows[i].Cells["DWFlag"].Value = false;
                            }
                        }

                    }
                    //배포업체
                    DataTable dtVendor = clsVendor.mfReadISODocVendor(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    if (dtVendor.Rows.Count > 0)
                    {
                        uGrid2.DataSource = dtVendor;
                        uGrid2.DataBind();
                    }
                    //적용범위
                    DataTable dtDept = clsDept.mfReadISODocDept(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    if (dtDept.Rows.Count > 0)
                    {
                        uGrid3.DataSource = dtDept;
                        uGrid3.DataBind();
                    }
                    //결제선
                    DataTable dtAgrUser = clsAgu.mfReadISODocAgreeUser(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                    {
                        uGrid4.DataSource = dtAgrUser;
                        uGrid4.DataBind();
                    }

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    InitValue();
                    this.uGroupBoxContentsArea.Expanded = false;
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        #region 파일 다운로드
        /// <summary>
        /// 버튼클릭시 첨부파일 다운로드 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid1.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\")
                            || this.uGrid1.Rows[i].Cells["FileName"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001147",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0006");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        String FolderName = this.uTextStandardDocNo.Text + "-" + this.uTextVersionNum.Text;

                        for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (this.uGrid1.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                                {
                                    arrFile.Add(this.uGrid1.Rows[i].Cells["FileName"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + FolderName,
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        if (arrFile.Count != 0)
                        {
                            fileAtt.ShowDialog();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 셀 더블클릭시 파일 다운로드, 자동실행 처리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.Cell.Column.ToString() == "FileName")
                {
                    if (e.Cell.Row.Cells["FileName"].Value.ToString() == "" || e.Cell.Row.Cells["FileName"].Value.ToString().Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000356",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (e.Cell.Row.Cells["FileName"].Value.ToString().Equals(string.Empty))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000356",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), "D0006");

                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();

                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        arrFile.Add(e.Cell.Value.ToString());
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (this.uTextStandardDocNo.Text + "-" + this.uTextVersionNum.Text),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());

                        fileAtt.ShowDialog();
                        System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion
        /// <summary>
        /// 다운로드 체크를 위해 그리드가 수정불가이어도 체크박스 체크는 가능하도록
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid1_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("Check"))
                {
                    if (e.Cell.Row.Cells["Check"].Value.Equals(false))
                        e.Cell.Row.Cells["Check"].Value = true;
                    else if (e.Cell.Row.Cells["Check"].Value.Equals(true))
                        e.Cell.Row.Cells["Check"].Value = false;
                }
            }
            catch
            { }
            finally
            { }
        }
        //uGrid1에 전체 DWFlag를 확인한다(hidden하여도 DWFlag는 지워지지않는다)
        public bool CheckDWFlag()
        {
            bool checkDwf = false;

            for (int i = 0; i < this.uGrid1.Rows.Count; i++)
            {
                if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["DWFlag"].Value) == true)
                {
                    checkDwf = true;
                }
            }
            return checkDwf;
        }
        //삭제시 항상 가장위에 파일에 DWFlag를 표시한다
        public void AutoTopFlag()
        {
            for (int i = 0; i < this.uGrid1.Rows.Count; i++)
            {
                if (i == 0 && this.uGrid1.Rows[i].Hidden == false)
                {
                    this.uGrid1.Rows[i].Cells["DWFlag"].Value = true;
                }
                else if (i > 0 && this.uGrid1.Rows[i].Hidden == false && this.uGrid1.Rows[i - 1].Hidden == true)
                {
                    this.uGrid1.Rows[i].Cells["DWFlag"].Value = true;
                }
            }
        }
    }
}