﻿namespace QRPISO.UI
{
    partial class frmISO0009D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton("Find");
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0009D));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonMatDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGridMatDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGridEquipDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextQTY = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uLabelStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquipHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextStdNumberS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPacakge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonApproveDelete = new Infragistics.Win.Misc.UltraButton();
            this.uGridApproveDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uCheckCompleteFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQTY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumberS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridApproveDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridMatDetail);
            this.ultraTabPageControl1.Controls.Add(this.uButtonMatDelete);
            this.ultraTabPageControl1.Controls.Add(this.uButtonDelete);
            this.ultraTabPageControl1.Controls.Add(this.uGridEquipDetail);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(886, 562);
            // 
            // uButtonMatDelete
            // 
            this.uButtonMatDelete.Location = new System.Drawing.Point(15, 292);
            this.uButtonMatDelete.Name = "uButtonMatDelete";
            this.uButtonMatDelete.Size = new System.Drawing.Size(75, 28);
            this.uButtonMatDelete.TabIndex = 99;
            this.uButtonMatDelete.Text = "行删除";
            this.uButtonMatDelete.Click += new System.EventHandler(this.uButtonMatDelete_Click);
            // 
            // uGridMatDetail
            // 
            this.uGridMatDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            appearance68.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMatDetail.DisplayLayout.Appearance = appearance68;
            this.uGridMatDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMatDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance69.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatDetail.DisplayLayout.GroupByBox.Appearance = appearance69;
            appearance70.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance70;
            this.uGridMatDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance71.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance71.BackColor2 = System.Drawing.SystemColors.Control;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance71.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMatDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance71;
            this.uGridMatDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMatDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance72.BackColor = System.Drawing.SystemColors.Window;
            appearance72.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMatDetail.DisplayLayout.Override.ActiveCellAppearance = appearance72;
            appearance73.BackColor = System.Drawing.SystemColors.Highlight;
            appearance73.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMatDetail.DisplayLayout.Override.ActiveRowAppearance = appearance73;
            this.uGridMatDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMatDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMatDetail.DisplayLayout.Override.CardAreaAppearance = appearance74;
            appearance75.BorderColor = System.Drawing.Color.Silver;
            appearance75.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMatDetail.DisplayLayout.Override.CellAppearance = appearance75;
            this.uGridMatDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMatDetail.DisplayLayout.Override.CellPadding = 0;
            appearance76.BackColor = System.Drawing.SystemColors.Control;
            appearance76.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance76.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance76.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMatDetail.DisplayLayout.Override.GroupByRowAppearance = appearance76;
            appearance77.TextHAlignAsString = "Left";
            this.uGridMatDetail.DisplayLayout.Override.HeaderAppearance = appearance77;
            this.uGridMatDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMatDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance78.BackColor = System.Drawing.SystemColors.Window;
            appearance78.BorderColor = System.Drawing.Color.Silver;
            this.uGridMatDetail.DisplayLayout.Override.RowAppearance = appearance78;
            this.uGridMatDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance79.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMatDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance79;
            this.uGridMatDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMatDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMatDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMatDetail.Location = new System.Drawing.Point(2, 326);
            this.uGridMatDetail.Name = "uGridMatDetail";
            this.uGridMatDetail.Size = new System.Drawing.Size(869, 233);
            this.uGridMatDetail.TabIndex = 8;
            this.uGridMatDetail.Text = "原辅材料";
            this.uGridMatDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMatDetail_AfterCellUpdate);
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(10, 3);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(75, 28);
            this.uButtonDelete.TabIndex = 6;
            this.uButtonDelete.Text = "行删除";
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uGridEquipDetail
            // 
            this.uGridEquipDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipDetail.DisplayLayout.Appearance = appearance47;
            this.uGridEquipDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.Appearance = appearance48;
            appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance49;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance50.BackColor2 = System.Drawing.SystemColors.Control;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance50.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance50;
            this.uGridEquipDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipDetail.DisplayLayout.Override.ActiveCellAppearance = appearance51;
            appearance52.BackColor = System.Drawing.SystemColors.Highlight;
            appearance52.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipDetail.DisplayLayout.Override.ActiveRowAppearance = appearance52;
            this.uGridEquipDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDetail.DisplayLayout.Override.CardAreaAppearance = appearance53;
            appearance54.BorderColor = System.Drawing.Color.Silver;
            appearance54.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipDetail.DisplayLayout.Override.CellAppearance = appearance54;
            this.uGridEquipDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipDetail.DisplayLayout.Override.CellPadding = 0;
            appearance55.BackColor = System.Drawing.SystemColors.Control;
            appearance55.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance55.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance55.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance55.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipDetail.DisplayLayout.Override.GroupByRowAppearance = appearance55;
            appearance56.TextHAlignAsString = "Left";
            this.uGridEquipDetail.DisplayLayout.Override.HeaderAppearance = appearance56;
            this.uGridEquipDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            appearance57.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipDetail.DisplayLayout.Override.RowAppearance = appearance57;
            this.uGridEquipDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance58;
            this.uGridEquipDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipDetail.Location = new System.Drawing.Point(10, 37);
            this.uGridEquipDetail.Name = "uGridEquipDetail";
            this.uGridEquipDetail.Size = new System.Drawing.Size(869, 249);
            this.uGridEquipDetail.TabIndex = 3;
            this.uGridEquipDetail.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.uGridPRCDetail_Error);
            this.uGridEquipDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipDetail_AfterCellUpdate);
            this.uGridEquipDetail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridEquipDetail_KeyDown);
            this.uGridEquipDetail.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPRCDetail_AfterCellListCloseUp);
            this.uGridEquipDetail.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridPRCDetail_BeforeCellListDropDown);
            this.uGridEquipDetail.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipDetail_CellChange);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(917, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(917, 715);
            this.uGroupBoxContentsArea.TabIndex = 8;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckCompleteFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextQTY);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraLabel8);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCustomerName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCustomerCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCustomer);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStdNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTab);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(911, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextQTY
            // 
            this.uTextQTY.Location = new System.Drawing.Point(97, 68);
            this.uTextQTY.Name = "uTextQTY";
            this.uTextQTY.Size = new System.Drawing.Size(175, 21);
            this.uTextQTY.TabIndex = 88;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Location = new System.Drawing.Point(8, 68);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(86, 20);
            this.ultraLabel8.TabIndex = 87;
            this.ultraLabel8.Text = "ultraLabel1";
            // 
            // uTextCustomerName
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance13;
            this.uTextCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Location = new System.Drawing.Point(704, 13);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(86, 21);
            this.uTextCustomerName.TabIndex = 86;
            // 
            // uTextCustomerCode
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCustomerCode.Appearance = appearance14;
            this.uTextCustomerCode.BackColor = System.Drawing.Color.PowderBlue;
            appearance15.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance15.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance15;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Key = "Find";
            this.uTextCustomerCode.ButtonsRight.Add(editorButton1);
            this.uTextCustomerCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.uTextCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCustomerCode.Location = new System.Drawing.Point(616, 13);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.Size = new System.Drawing.Size(86, 21);
            this.uTextCustomerCode.TabIndex = 85;
            this.uTextCustomerCode.ValueChanged += new System.EventHandler(this.uTextCustomerCode_ValueChanged);
            this.uTextCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCustomerCode_KeyDown);
            this.uTextCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCustomerCode_EditorButtonClick);
            // 
            // uLabelCustomer
            // 
            this.uLabelCustomer.Location = new System.Drawing.Point(525, 13);
            this.uLabelCustomer.Name = "uLabelCustomer";
            this.uLabelCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelCustomer.TabIndex = 84;
            this.uLabelCustomer.Text = "ultraLabel1";
            // 
            // uComboPackage
            // 
            this.uComboPackage.Location = new System.Drawing.Point(616, 40);
            this.uComboPackage.Name = "uComboPackage";
            this.uComboPackage.Size = new System.Drawing.Size(173, 21);
            this.uComboPackage.TabIndex = 83;
            this.uComboPackage.Text = "ultraComboEditor2";
            this.uComboPackage.ValueChanged += new System.EventHandler(this.uComboPackage_ValueChanged);
            this.uComboPackage.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboPackage_BeforeDropDown);
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(374, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(110, 21);
            this.uComboPlant.TabIndex = 82;
            this.uComboPlant.Text = "ultraComboEditor1";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(525, 40);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(86, 20);
            this.uLabelPackage.TabIndex = 81;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(285, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(86, 20);
            this.uLabelPlant.TabIndex = 80;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(373, 68);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(535, 21);
            this.uTextEtcDesc.TabIndex = 78;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(284, 68);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(86, 20);
            this.uLabelEtcDesc.TabIndex = 77;
            this.uLabelEtcDesc.Text = "ultraLabel1";
            // 
            // uDateWriteDate
            // 
            this.uDateWriteDate.DateTime = new System.DateTime(2015, 5, 5, 0, 0, 0, 0);
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(374, 40);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(86, 21);
            this.uDateWriteDate.TabIndex = 76;
            this.uDateWriteDate.Value = new System.DateTime(2015, 5, 5, 0, 0, 0, 0);
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(285, 40);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(86, 20);
            this.uLabelCreateDate.TabIndex = 75;
            this.uLabelCreateDate.Text = "ultraLabel1";
            // 
            // uTextWriteName
            // 
            appearance62.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance62;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(186, 40);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(86, 21);
            this.uTextWriteName.TabIndex = 74;
            // 
            // uTextWriteID
            // 
            appearance63.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteID.Appearance = appearance63;
            this.uTextWriteID.BackColor = System.Drawing.Color.PowderBlue;
            appearance64.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance64.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance64;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteID.ButtonsRight.Add(editorButton2);
            this.uTextWriteID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteID.Location = new System.Drawing.Point(99, 40);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.Size = new System.Drawing.Size(86, 21);
            this.uTextWriteID.TabIndex = 73;
            this.uTextWriteID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteID_KeyDown);
            this.uTextWriteID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(10, 40);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(86, 20);
            this.uLabelCreateUser.TabIndex = 72;
            this.uLabelCreateUser.Text = "ultraLabel1";
            // 
            // uTextStdNumber
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance18;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(99, 12);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(129, 21);
            this.uTextStdNumber.TabIndex = 71;
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Location = new System.Drawing.Point(10, 100);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(890, 588);
            this.uTab.TabIndex = 58;
            ultraTab1.Key = "Detail";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "设备材料信息";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "部门审批";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(886, 562);
            // 
            // uLabelStandardNo
            // 
            this.uLabelStandardNo.Location = new System.Drawing.Point(10, 12);
            this.uLabelStandardNo.Name = "uLabelStandardNo";
            this.uLabelStandardNo.Size = new System.Drawing.Size(86, 20);
            this.uLabelStandardNo.TabIndex = 35;
            this.uLabelStandardNo.Text = "ultraLabel1";
            // 
            // uGridEquipHeader
            // 
            this.uGridEquipHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipHeader.DisplayLayout.Appearance = appearance20;
            this.uGridEquipHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipHeader.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGridEquipHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGridEquipHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipHeader.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipHeader.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGridEquipHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipHeader.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            appearance27.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipHeader.DisplayLayout.Override.CellAppearance = appearance27;
            this.uGridEquipHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipHeader.DisplayLayout.Override.CellPadding = 0;
            appearance28.BackColor = System.Drawing.SystemColors.Control;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipHeader.DisplayLayout.Override.GroupByRowAppearance = appearance28;
            appearance29.TextHAlignAsString = "Left";
            this.uGridEquipHeader.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGridEquipHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipHeader.DisplayLayout.Override.RowAppearance = appearance30;
            this.uGridEquipHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance31;
            this.uGridEquipHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipHeader.Location = new System.Drawing.Point(0, 80);
            this.uGridEquipHeader.Name = "uGridEquipHeader";
            this.uGridEquipHeader.Size = new System.Drawing.Size(909, 760);
            this.uGridEquipHeader.TabIndex = 7;
            this.uGridEquipHeader.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridPRCHeader_DoubleClickRow);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance32;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextStdNumberS);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel9);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchCustomerCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPacakge);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(917, 40);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // uTextStdNumberS
            // 
            this.uTextStdNumberS.Location = new System.Drawing.Point(844, 12);
            this.uTextStdNumberS.Name = "uTextStdNumberS";
            this.uTextStdNumberS.Size = new System.Drawing.Size(116, 21);
            this.uTextStdNumberS.TabIndex = 91;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(752, 12);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(86, 20);
            this.ultraLabel9.TabIndex = 90;
            this.ultraLabel9.Text = "StandardNo";
            // 
            // uTextSearchCustomerName
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Appearance = appearance33;
            this.uTextSearchCustomerName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchCustomerName.Location = new System.Drawing.Point(422, 12);
            this.uTextSearchCustomerName.Name = "uTextSearchCustomerName";
            this.uTextSearchCustomerName.ReadOnly = true;
            this.uTextSearchCustomerName.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchCustomerName.TabIndex = 89;
            // 
            // uTextSearchCustomerCode
            // 
            appearance34.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance34;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchCustomerCode.ButtonsRight.Add(editorButton3);
            this.uTextSearchCustomerCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchCustomerCode.Location = new System.Drawing.Point(334, 12);
            this.uTextSearchCustomerCode.Name = "uTextSearchCustomerCode";
            this.uTextSearchCustomerCode.Size = new System.Drawing.Size(86, 21);
            this.uTextSearchCustomerCode.TabIndex = 88;
            this.uTextSearchCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSearchCustomerCode_KeyDown);
            this.uTextSearchCustomerCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchCustomerCode_EditorButtonClick);
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(243, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchCustomer.TabIndex = 87;
            this.uLabelSearchCustomer.Text = "ultraLabel2";
            // 
            // uComboSearchPacakge
            // 
            this.uComboSearchPacakge.Location = new System.Drawing.Point(614, 12);
            this.uComboSearchPacakge.Name = "uComboSearchPacakge";
            this.uComboSearchPacakge.Size = new System.Drawing.Size(123, 21);
            this.uComboSearchPacakge.TabIndex = 16;
            this.uComboSearchPacakge.Text = "ultraComboEditor2";
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(525, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPackage.TabIndex = 15;
            this.uLabelSearchPackage.Text = "ultraLabel2";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(99, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(129, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(10, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(86, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(917, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uButtonApproveDelete);
            this.ultraTabPageControl2.Controls.Add(this.uGridApproveDetail);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(886, 562);
            // 
            // uButtonApproveDelete
            // 
            this.uButtonApproveDelete.Location = new System.Drawing.Point(5, 12);
            this.uButtonApproveDelete.Name = "uButtonApproveDelete";
            this.uButtonApproveDelete.Size = new System.Drawing.Size(75, 31);
            this.uButtonApproveDelete.TabIndex = 111;
            this.uButtonApproveDelete.Text = "行删除";
            this.uButtonApproveDelete.Click += new System.EventHandler(this.ultraButton1_Click);
            // 
            // uGridApproveDetail
            // 
            this.uGridApproveDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridApproveDetail.DisplayLayout.Appearance = appearance1;
            this.uGridApproveDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridApproveDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridApproveDetail.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridApproveDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridApproveDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridApproveDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridApproveDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridApproveDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridApproveDetail.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridApproveDetail.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.uGridApproveDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridApproveDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridApproveDetail.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridApproveDetail.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGridApproveDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridApproveDetail.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridApproveDetail.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.uGridApproveDetail.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.uGridApproveDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridApproveDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridApproveDetail.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridApproveDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridApproveDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridApproveDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridApproveDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridApproveDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridApproveDetail.Location = new System.Drawing.Point(-4, 46);
            this.uGridApproveDetail.Name = "uGridApproveDetail";
            this.uGridApproveDetail.Size = new System.Drawing.Size(869, 513);
            this.uGridApproveDetail.TabIndex = 110;
            this.uGridApproveDetail.Text = "原辅材料";
            this.uGridApproveDetail.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridApproveDetail_AfterCellUpdate_1);
            this.uGridApproveDetail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridApproveDetail_KeyDown);
            this.uGridApproveDetail.AfterCellListCloseUp += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridApproveDetail_AfterCellListCloseUp);
            // 
            // uCheckCompleteFlag
            // 
            this.uCheckCompleteFlag.Enabled = false;
            this.uCheckCompleteFlag.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCompleteFlag.Location = new System.Drawing.Point(821, 41);
            this.uCheckCompleteFlag.Name = "uCheckCompleteFlag";
            this.uCheckCompleteFlag.Size = new System.Drawing.Size(76, 20);
            this.uCheckCompleteFlag.TabIndex = 99;
            this.uCheckCompleteFlag.Text = "审批完成";
            this.uCheckCompleteFlag.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // frmISO0009D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 850);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridEquipHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0009D";
            this.Text = "frmISO0009D";
            this.Load += new System.EventHandler(this.frmISO0009D_Load);
            this.Activated += new System.EventHandler(this.frmISO0009D_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISO0009D_FormClosing);
            this.Resize += new System.EventHandler(this.frmISO0009D_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMatDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextQTY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumberS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridApproveDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCompleteFlag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipHeader;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPacakge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextQTY;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumberS;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraButton uButtonMatDelete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMatDetail;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipDetail;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.Misc.UltraButton uButtonApproveDelete;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridApproveDetail;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCompleteFlag;
    }
}