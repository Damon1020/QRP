﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서관리                                          */
/* 프로그램ID   : frmISOZ0001L.cs                                       */
/* 프로그램명   : 표준문서 배포/회수 관리                               */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPISO.UI
{
    public partial class frmISOZ0001L : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();
        private string PlantCode { get; set; }
        public frmISOZ0001L()
        {
            InitializeComponent();
        }

        private void frmISOZ0001L_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISOZ0001L_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 배포/회수 조회", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Contorol 초기화
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            initValue();
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);


        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "배포/회수 이력", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void initValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                PlantCode = string.Empty;
                this.uTextCollectUnusual.Text = "";
                this.uTextCollectUserID.Text = "";
                this.uTextCollectUserID.MaxLength = 20;
                this.uTextCollectUserName.Text = "";
                this.uTextDistributeUnusual.Text = "";
                this.uTextDistributeUserID.Text = "";
                this.uTextDistributeUserID.MaxLength = 20;
                this.uTextDistributeUserName.Text = "";
                this.uCheckCollect.Checked = false;
                this.uCheckDistribute.Checked = false;
                this.uNumDistributeAmount.Value = 0;
                this.uNumCollectAmount.Value = 0;
                //this.uTextSearchDistributeCompanyID.Text = "";
                //this.uTextSearchDistributeCompanyName.Text = "";
                //this.uTextSearchStandardNum.Text = "";
                //this.uTextSearchTitle.Text = "";
                //this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE"); // "";
                this.uDateCollectDate.Value = DateTime.Now;
                this.uDateDistributeDate.Value = DateTime.Now;
                //this.uDateSearchDistributeRequestFromDate.Value = DateTime.Now.AddDays(-7);
                //this.uDateSearchDistributeRequestToDate.Value = DateTime.Now;
                //숫자 입력 박스에 1000단위 콤마
                this.uNumCollectAmount.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uNumCollectAmount.MaskInput = "nnn,nnn,nnn";
                this.uNumDistributeAmount.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                this.uNumDistributeAmount.MaskInput = "nnn,nnn,nnn";

                if (this.uGrid2.Rows.Count > 0)
                {
                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                }

                //공정 히든
                this.uComboSearchProcess.Visible = false;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNum, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDistributeCompany, "배포업체", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDistributeRequestDate, "배포요청일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelDistributeDate, "배포일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDistributeAmount, "배포부수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDistributeUser, "배포담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDistributeUnusual, "배포 특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.ulabelCollectDate, "회수일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCollectAmount, "회수부수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCollectUser, "회수담당자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCollectUnusual, "회수 특이사항", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                // Search Process ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo("", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Collect 상세 텍스트 초기화
        /// </summary>
        private void ClearText()
        {
            try
            {
                this.uTextCollectUserID.Text = "";
                this.uTextCollectUserName.Text = "";
                this.uTextDistributeUserID.Text = "";
                this.uTextDistributeUserName.Text = "";

                this.uDateCollectDate.Value = DateTime.Now;
                this.uDateDistributeDate.Value = DateTime.Now;

                this.uNumDistributeAmount.Value = 0;
                this.uNumCollectAmount.Value = 0;

                this.uCheckCollect.Checked = false;
                this.uCheckDistribute.Checked = false;

                this.uTextDistributeUnusual.Text = "";
                this.uTextCollectUnusual.Text = "";

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqNo", "배포관리번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqSeq", "배포순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdNumber", "표준번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "AdmitVersionNum", "개정번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "VersionNum", "이전개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistDeptCode", "배포부서코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistDeptName", "배포부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistVendorCode", "배포업체코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistVendorName", "배포업체", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqDate", "배포요청일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                //wGrid.mfSetGridColumn(this.uGrid1, 0, "배포일", "배포일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistNumber", "배포부수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistNum", "배포", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "HoldNum", "대기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "CollectNum", "회수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 배포/회수 이력 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGrid2, 0, "DistCollectType", "구분코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "WriteDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Number", "부수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "WriteUserID", "담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "WriteUserName", "담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "특이사항", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                initValue();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                QRPISO.BL.ISODOC.ISODocDistReqD ReqD;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistReqD), "ISODocDistReqD");
                ReqD = new QRPISO.BL.ISODOC.ISODocDistReqD();
                brwChannel.mfCredentials(ReqD);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strStdNumber = this.uTextSearchStandardNum.Text;
                String strProcessCode = this.uComboSearchProcess.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;
                String strDistVendorCode = this.uTextSearchDistributeCompanyID.Text;
                String strDistReqDateFrom = Convert.ToDateTime(this.uDateSearchDistributeRequestFromDate.Value).ToString("yyyy-MM-dd");
                String strDistReqDateTo = Convert.ToDateTime(this.uDateSearchDistributeRequestToDate.Value).ToString("yyyy-MM-dd");

                DataTable dt = ReqD.mfReadISODocDistReqDCol(strPlantCode, strStdNumber, strDocTitle,strProcessCode, strDistVendorCode
                                                            , strDistReqDateFrom, strDistReqDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }

                initValue();
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uCheckCollect.Checked == true)
                {
                    if (this.uTextCollectUserID.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001037", "M001266",
                                                   Infragistics.Win.HAlign.Right);

                        this.uTextCollectUserID.Focus();
                        return;
                    }
                }
                else if (this.uCheckDistribute.Checked == true)
                {
                    if (this.uTextDistributeUserID.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001037", "M000442",
                                                   Infragistics.Win.HAlign.Right);

                        this.uTextDistributeUserID.Focus();
                        return;
                    }
                }

                DataTable dtISODocDistCollect = new DataTable();
                DataRow row;

                QRPBrowser brwChannel = new QRPBrowser();
                QRPISO.BL.ISODOC.ISODocDistCollect DCollect = new QRPISO.BL.ISODOC.ISODocDistCollect();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistCollect), "ISODocDistCollect");
                brwChannel.mfCredentials(DCollect);

                System.Windows.Forms.DialogResult result1;

                dtISODocDistCollect = DCollect.mfSetDataInfo();
                row = dtISODocDistCollect.NewRow();

                row["PlantCode"] = this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString();
                row["DistReqNo"] = this.uGrid1.ActiveRow.Cells["DistReqNo"].Value.ToString();
                row["DistReqSeq"] = this.uGrid1.ActiveRow.Cells["DistReqSeq"].Value.ToString();
                if (this.uCheckCollect.Checked == false && this.uCheckDistribute.Checked == false)
                {
                    result1 = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001037", "M000441",
                                                   Infragistics.Win.HAlign.Right);

                    return;
                }
                

                else if (this.uCheckCollect.Checked == true)
                {
                    row["DistCollectType"] = '2';
                    row["WriteDate"] = this.uDateCollectDate.Value.ToString();
                    row["WriteUserID"] = this.uTextCollectUserID.Text;
                    row["Number"] = this.uNumCollectAmount.Value;
                    row["EtcDesc"] = this.uTextCollectUnusual.Text;
                }
                else if (this.uCheckDistribute.Checked == true)
                {
                    row["DistCollectType"] = '1';
                    row["WriteDate"] = this.uDateDistributeDate.Value.ToString();
                    row["WriteUserID"] = this.uTextDistributeUserID.Text;
                    row["Number"] = this.uNumDistributeAmount.Value;
                    row["EtcDesc"] = this.uTextDistributeUnusual.Text;
                }

                dtISODocDistCollect.Rows.Add(row);

                //배포부수와 회수부수 검사
                int colNum = Convert.ToInt32(this.uNumCollectAmount.Value); //회수부수 체크
                int MaxDistNum = Convert.ToInt32(this.uGrid1.ActiveRow.Cells["DistNumber"].Value.ToString());//배포예정부수
                int distNum = Convert.ToInt32(this.uNumDistributeAmount.Value); //배포부수 체크


                int checkDistNumber = 0;
                int checkColNumber = 0;
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (this.uGrid2.Rows[i].Cells["DistCollectType"].Value.ToString() == "회수") 
                    {
                        checkColNumber = checkColNumber + Convert.ToInt32(this.uGrid2.Rows[i].Cells["Number"].Value.ToString());
                    }
                    else
                    {
                        checkDistNumber = checkDistNumber + Convert.ToInt32(this.uGrid2.Rows[i].Cells["Number"].Value.ToString());
                    }
                }

                if (MaxDistNum < checkDistNumber + distNum)
                {
                   msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001037", "M000444",
                                                   Infragistics.Win.HAlign.Right);
                   this.uNumDistributeAmount.Focus();
                   return;
                }
                else if(MaxDistNum < checkColNumber + colNum)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001037", "M001268",
                                                   Infragistics.Win.HAlign.Right);
                    this.uNumCollectAmount.Focus();
                    return;
                }
                else if(checkColNumber + colNum > checkDistNumber)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001037", "M001267",
                                                   Infragistics.Win.HAlign.Right);
                    this.uNumCollectAmount.Focus();
                    return;
                }


                if (dtISODocDistCollect.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string rtMSG = DCollect.mfSaveISODocDistCollectOnly(dtISODocDistCollect, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        //decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        System.Windows.Forms.DialogResult result;
                        if (ErrRtn.ErrNum == 0)
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001037", "M000930",
                                                   Infragistics.Win.HAlign.Right);

                            //ClearText();
                            {
                                m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                                m_ProgressPopup = new QRPProgressBar();
                                Thread threadPop = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                brwChannel = new QRPBrowser();
                                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistCollect), "ISODocDistCollect");
                                DCollect = new QRPISO.BL.ISODOC.ISODocDistCollect();
                                brwChannel.mfCredentials(DCollect);

                                String strPlantCode = uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString();
                                String strDistReqNo = uGrid1.ActiveRow.Cells["DistReqNo"].Value.ToString();
                                Int32 intDistReqSeq = Convert.ToInt32(uGrid1.ActiveRow.Cells["DistReqSeq"].Value.ToString());

                                DataTable dt = DCollect.mfReadISODocDistCollect(strPlantCode, strDistReqNo, intDistReqSeq, m_resSys.GetString("SYS_LANG"));

                                this.uGrid2.DataSource = dt;
                                this.uGrid2.DataBind();

                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                if (this.uCheckCollect.Checked == true)
                                {
                                    //uNumCollectAmount
                                    this.uGrid1.Rows[0].Cells["CollectNum"].Value = Convert.ToString(Convert.ToInt32(this.uGrid1.Rows[0].Cells["CollectNum"].Value) 
                                                                                                                                                + Convert.ToInt32(uNumCollectAmount.Value));
                                }
                                else
                                {
                                    this.uGrid1.Rows[0].Cells["DistNum"].Value = Convert.ToString(Convert.ToInt32(this.uGrid1.Rows[0].Cells["DistNum"].Value)
                                                                                                                                                + Convert.ToInt32(uNumDistributeAmount.Value));
                                    this.uGrid1.Rows[0].Cells["HoldNum"].Value = Convert.ToString(Convert.ToInt32(this.uGrid1.Rows[0].Cells["HoldNum"].Value)
                                                                                                                                                - Convert.ToInt32(uNumDistributeAmount.Value));
                                }
                                ClearText();
                            }

                        }

                        else if (ErrRtn.ErrNum == -6)
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                               "M001135", "M001037", "M001153",
                                                                               Infragistics.Win.HAlign.Right);

                        }

                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 725;

                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        this.uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                this.uComboSearchProcess.Items.Clear();

                strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                           , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                           , Infragistics.Win.HAlign.Center, "", "", "선택", "ProcessCode", "ProcessName", dtProcess);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uTextDistributeUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M001204",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }
                
                frmPOP0011 frmAUser = new frmPOP0011();
                frmAUser.PlantCode = PlantCode;
                frmAUser.ShowDialog();

                this.uTextDistributeUserID.Text = frmAUser.UserID;
                this.uTextDistributeUserName.Text = frmAUser.UserName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }



        private void uTextCollectUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            try
            {
                if (PlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000962", "M001204",
                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmCUser = new frmPOP0011();
                frmCUser.PlantCode = PlantCode;
                frmCUser.ShowDialog();

                this.uTextCollectUserID.Text = frmCUser.UserID;
                this.uTextCollectUserName.Text = frmCUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {


            }

        }

        private void uCheckDistribute_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckDistribute.Checked == true)
                {
                    this.uCheckCollect.CheckedValue = false;
                }
                this.uNumCollectAmount.Value = 0;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uCheckCollect_CheckedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uCheckCollect.Checked == true)
                {
                    this.uCheckDistribute.CheckedValue = false;
                }
                this.uNumDistributeAmount.Value = 0;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 더블클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {

            try
            {
                initValue();
                e.Row.Fixed = true;
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                QRPISO.BL.ISODOC.ISODocDistCollect DCollect;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistCollect), "ISODocDistCollect");
                DCollect = new QRPISO.BL.ISODOC.ISODocDistCollect();
                brwChannel.mfCredentials(DCollect);

                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strDistReqNo = e.Row.Cells["DistReqNo"].Value.ToString();
                Int32 intDistReqSeq = Convert.ToInt32(e.Row.Cells["DistReqSeq"].Value.ToString());
                PlantCode = strPlantCode;
                DataTable dt = DCollect.mfReadISODocDistCollect(strPlantCode, strDistReqNo, intDistReqSeq, m_resSys.GetString("SYS_LANG"));

                this.uGrid2.DataSource = dt;
                this.uGrid2.DataBind();
                if (dt.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                WinMessageBox msg = new WinMessageBox();
                
                this.uGroupBoxContentsArea.Expanded = true;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {

            }
        }

        private void uNumDistributeAmount_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uNumCollectAmount_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        //private void SearchHeader()
        //{
        //    try
        //    {
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


        //        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
        //        Thread threadPop = m_ProgressPopup.mfStartThread();
        //        m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
        //        this.MdiParent.Cursor = Cursors.WaitCursor;
        //        QRPISO.BL.ISODOC.ISODocDistReqD ReqD;

        //        QRPBrowser brwChannel = new QRPBrowser();
        //        brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistReqD), "ISODocDistReqD");
        //        ReqD = new QRPISO.BL.ISODOC.ISODocDistReqD();
        //        brwChannel.mfCredentials(ReqD);

        //        String strPlantCode = this.uComboSearchPlant.Value.ToString();
        //        String strStdNumber = this.uTextSearchStandardNum.Text;
        //        String strProcessCode = this.uComboSearchProcess.Value.ToString();
        //        String strDocTitle = this.uTextSearchTitle.Text;
        //        String strDistVendorCode = this.uTextSearchDistributeCompanyID.Text;
        //        String strDistReqDateFrom = this.uDateSearchDistributeRequestFromDate.Value.ToString();
        //        String strDistReqDateTo = this.uDateSearchDistributeRequestToDate.Value.ToString();


        //        DataTable dt = ReqD.mfReadISODocDistReqDCol(strPlantCode, strStdNumber, strProcessCode, strDocTitle, strDistVendorCode
        //                                                    , strDistReqDateFrom, strDistReqDateTo, m_resSys.GetString("SYS_LANG"));

        //        this.uGrid1.DataSource = dt;
        //        this.uGrid1.DataBind();

        //        this.MdiParent.Cursor = Cursors.Default;
        //        m_ProgressPopup.mfCloseProgressPopup(this);


        //        DialogResult DResult = new DialogResult();
        //        WinMessageBox msg = new WinMessageBox();
        //        if (dt.Rows.Count == 0)
        //        {
        //            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        //                , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}


        /// <summary>
        /// 사용자 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        private String GetUserInfo(String strPlantCode, String strUserID)
        {
            String strRtnUserName = "";
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                if (dtUser.Rows.Count > 0)
                {
                    strRtnUserName = dtUser.Rows[0]["UserName"].ToString();
                }
                return strRtnUserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return strRtnUserName;
            }
            finally
            {
            }
        }

        private void uTextDistributeUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextDistributeUserID.Text == "")
                    {
                        this.uTextDistributeUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();


                        String strPlantCode = this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString();
                        String strWriteID = this.uTextDistributeUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextDistributeUserID.Text = "";
                            this.uTextDistributeUserName.Text = "";
                        }
                        else
                        {
                            this.uTextDistributeUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextCollectUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.uTextCollectUserID.Text == "")
                    {
                        this.uTextCollectUserName.Text = "";
                    }
                    else
                    {
                        // SystemInfor ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                        WinMessageBox msg = new WinMessageBox();


                        String strPlantCode = this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString();
                        String strWriteID = this.uTextCollectUserID.Text;

                        // UserName 검색 함수 호출
                        String strRtnUserName = GetUserInfo(strPlantCode, strWriteID);

                        if (strRtnUserName == "")
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000962", "M000621",
                                        Infragistics.Win.HAlign.Right);

                            this.uTextCollectUserID.Text = "";
                            this.uTextCollectUserName.Text = "";
                        }
                        else
                        {
                            this.uTextCollectUserName.Text = strRtnUserName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmISOZ0001L_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void frmISOZ0001L_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchDistributeCompanyID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                frmPOP0004 frmVendor = new frmPOP0004();
                frmVendor.ShowDialog();

                this.uTextSearchDistributeCompanyID.Text = frmVendor.CustomerCode;
                this.uTextSearchDistributeCompanyName.Text = frmVendor.CustomerName;
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #region 배포부수 1000단위 자릿수 표시처리
        private void uGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["DistNumber"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["DistNumber"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["DistNum"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["DistNum"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["HoldNum"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["HoldNum"].MaskInput = "nnn,nnn,nnn";
                e.Layout.Bands[0].Columns["CollectNum"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["CollectNum"].MaskInput = "nnn,nnn,nnn";
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uGrid2_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["Number"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["Number"].MaskInput = "nnn,nnn,nnn";
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
        #endregion
    }
}
