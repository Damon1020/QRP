﻿namespace QRPISO.UI
{
    partial class frmISOZ0004D
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISOZ0004D));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchEquipProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchRevisionNum = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabeSearchDrevisionNum = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProduct = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProduct = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uTree = new Infragistics.Win.UltraWinTree.UltraTree();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRevisionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTree)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchRevisionNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabeSearchDrevisionNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProduct);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProduct);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchEquipProcessGroup
            // 
            this.uComboSearchEquipProcessGroup.Location = new System.Drawing.Point(604, 36);
            this.uComboSearchEquipProcessGroup.Name = "uComboSearchEquipProcessGroup";
            this.uComboSearchEquipProcessGroup.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchEquipProcessGroup.TabIndex = 11;
            this.uComboSearchEquipProcessGroup.Text = "ultraComboEditor1";
            // 
            // uLabelSearchEquipProcessGroup
            // 
            this.uLabelSearchEquipProcessGroup.Location = new System.Drawing.Point(500, 36);
            this.uLabelSearchEquipProcessGroup.Name = "uLabelSearchEquipProcessGroup";
            this.uLabelSearchEquipProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipProcessGroup.TabIndex = 10;
            this.uLabelSearchEquipProcessGroup.Text = "ultraLabel1";
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(360, 36);
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchStation.TabIndex = 9;
            this.uComboSearchStation.Text = "ultraComboEditor1";
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(256, 36);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 8;
            this.uLabelSearchStation.Text = "ultraLabel1";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchArea.TabIndex = 7;
            this.uComboSearchArea.Text = "ultraComboEditor1";
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchArea.TabIndex = 6;
            this.uLabelSearchArea.Text = "ultraLabel1";
            // 
            // uComboSearchRevisionNum
            // 
            this.uComboSearchRevisionNum.Location = new System.Drawing.Point(604, 12);
            this.uComboSearchRevisionNum.Name = "uComboSearchRevisionNum";
            this.uComboSearchRevisionNum.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchRevisionNum.TabIndex = 5;
            this.uComboSearchRevisionNum.Text = "ultraComboEditor1";
            // 
            // uLabeSearchDrevisionNum
            // 
            this.uLabeSearchDrevisionNum.Location = new System.Drawing.Point(500, 12);
            this.uLabeSearchDrevisionNum.Name = "uLabeSearchDrevisionNum";
            this.uLabeSearchDrevisionNum.Size = new System.Drawing.Size(100, 20);
            this.uLabeSearchDrevisionNum.TabIndex = 4;
            this.uLabeSearchDrevisionNum.Text = "ultraLabel1";
            // 
            // uComboSearchProduct
            // 
            this.uComboSearchProduct.Location = new System.Drawing.Point(360, 12);
            this.uComboSearchProduct.Name = "uComboSearchProduct";
            this.uComboSearchProduct.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchProduct.TabIndex = 3;
            this.uComboSearchProduct.Text = "ultraComboEditor1";
            // 
            // uLabelSearchProduct
            // 
            this.uLabelSearchProduct.Location = new System.Drawing.Point(256, 12);
            this.uLabelSearchProduct.Name = "uLabelSearchProduct";
            this.uLabelSearchProduct.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProduct.TabIndex = 2;
            this.uLabelSearchProduct.Text = "ultraLabel1";
            // 
            // uComboSearchCustomer
            // 
            this.uComboSearchCustomer.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchCustomer.Name = "uComboSearchCustomer";
            this.uComboSearchCustomer.Size = new System.Drawing.Size(130, 21);
            this.uComboSearchCustomer.TabIndex = 1;
            this.uComboSearchCustomer.Text = "ultraComboEditor1";
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 0;
            this.uLabelSearchCustomer.Text = "ultraLabel1";
            // 
            // uTree
            // 
            this.uTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTree.Location = new System.Drawing.Point(0, 100);
            this.uTree.Name = "uTree";
            this.uTree.Size = new System.Drawing.Size(1070, 720);
            this.uTree.TabIndex = 2;
            // 
            // frmISOZ0004D
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uTree);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISOZ0004D";
            this.Load += new System.EventHandler(this.frmISOZ0004D_Load);
            this.Activated += new System.EventHandler(this.frmISOZ0004D_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchRevisionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTree)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProduct;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProduct;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchRevisionNum;
        private Infragistics.Win.Misc.UltraLabel uLabeSearchDrevisionNum;
        private Infragistics.Win.UltraWinTree.UltraTree uTree;
    }
}