﻿/*----------------------------------------------------------------------*/
/* 시스템명     : ISO관리                                               */
/* 모듈(분류)명 : 표준문서관리                                          */
/* 프로그램ID   : frmISOZ0001D.cs                                       */
/* 프로그램명   : 표준문서 배포조회                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-19                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.IO;
using System.Collections;

namespace QRPISO.UI
{
    public partial class frmISOZ0001D : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmISOZ0001D()
        {
            InitializeComponent();
        }

        private void frmISOZ0001D_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmISOZ0001D_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("표준문서 배포조회", m_resSys.GetString("SYS_FONTNAME"), 12);

            // Contorol 초기화
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitButton();
            InitValue();
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        #region 컨트롤 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화

        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBox2, GroupBoxType.LIST, "배포업체", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBox2.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox2.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //적용범위 
                wGroupBox.mfSetGroupBox(this.uGroupBox4, GroupBoxType.LIST, "적용범위", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupBox4.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox4.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //합의 담당자

                wGroupBox.mfSetGroupBox(this.uGroupBox5, GroupBoxType.LIST, "결재선", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupBox5.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBox5.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitValue()
        {
            try
            {
                this.uComboSearchProcess.Visible = false;
                this.uTextProcess.Visible = false;

                this.uTextSearchDistributeCompanyID.MaxLength = 20;
                this.uTextSearchStandardNum.MaxLength = 100;
                

                this.uTextApproveDate.Text = "";
                this.uTextApproveUserID.Text = "";
                this.uTextApproveUserName.Text = "";
                this.uTextChangeItem.Text = "";
                this.uTextCreateDate.Text = "";
                this.uTextCreateUserID.Text = "";
                this.uTextCreateUserName.Text = "";
                this.uTextDocKeyword.Text = "";
                this.uTextDocState.Text = "";
                this.uTextEtc.Text = "";
                this.uTextFrom.Text = "";
                this.uTextLarge.Text = "";
                this.uTextMiddle.Text = "";
                this.uTextPlant.Text = "";
                this.uTextRevDiscardDate.Text = "";
                this.uTextRevDiscardDiv.Text = "";
                this.uTextRevDiscardReason.Text = "";
                this.uTextRevDiscardUserID.Text = "";
                this.uTextRevDiscardUserName.Text = "";
                this.uTextRevisionNum.Text = "";
                this.uTextApplyFromDate.Text = "";
                this.uTextApplyToDate.Text = "";
                this.uTextCompleteDate.Text = "";
                //this.uTextSearchDistributeCompanyID.Text = "";
                //this.uTextSearchDistributeCompanyName.Text = "";
                //this.uTextSearchStandardNum.Text = "";
                //this.uTextSearchTitle.Text = "";
                this.uTextSmall.Text = "";
                this.uTextStdDocNum.Text = "";
                this.uTextTitle.Text = "";
                this.uTextTo.Text = "";
                this.uTextGRWComment.Text = "";


                if (this.uGrid2.Rows.Count > 0)
                {
                    while (this.uGrid2.Rows.Count > 0)
                    {
                        this.uGrid2.Rows[0].Delete(false);
                    }
                }

                if (this.uGrid3.Rows.Count > 0)
                {
                    while (this.uGrid3.Rows.Count > 0)
                    {
                        this.uGrid3.Rows[0].Delete(false);
                    }
                }
                if (this.uGrid4.Rows.Count > 0)
                {
                    while (this.uGrid4.Rows.Count > 0)
                    {
                        this.uGrid4.Rows[0].Delete(false);
                    }
                }

                if (this.uGrid5.Rows.Count > 0)
                {
                    while (this.uGrid5.Rows.Count > 0)
                    {
                        this.uGrid5.Rows[0].Delete(false);
                    }
                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStandardNum, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDistributeCompany, "배포업체", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchDistributeRequestDate, "배포요청일", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStdDocNum, "표준문서번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLarge, "대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMiddle, "중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSmall, "소분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "기안자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateDate, "기안일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevDiscardDiv, "개정/폐기구분", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevDiscardUser, "개정/폐기자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevDiscardDate, "개정/폐기일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevDiscardReason, "개정/폐기사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChangeItem, "변경항목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelFrom, "변경 전", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTo, "변경 후", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApproveUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApproveDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDocState, "문서상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelTitle, "제목", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDocKeyword, "문서Keyword", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelApplyDate, "적용일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCompleteDate, "만료일자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelMake, "작성자 / 부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelChipSize, "ChipsSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelPadSize, "PadSize", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelGRWComment, "결제Comment", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                //    , "PlantCode", "PlantName", dtPlant);

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                // Search Process ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                DataTable dtProcess = clsProcess.mfReadProcessForCombo("", m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqNo", "배포관리번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "StdNumber", "표준번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "AdmitVersionNum", "개정번호", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 50
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "AdmitStatus", "문서상태", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ProcessName", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "VersionNum", "이전개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DocTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqDate", "배포요청일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "AdmitDate", "배포일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistDeptCode", "배포부서코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistDeptName", "배포부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistNumber", "배포부수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistVendorCode", "배포업체코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistVendorName", "배포업체", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "ReqDesc", "요청사유", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //추가 Hidden Grid

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DistReqSeq", "배포번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 첨부파일 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "FileTitle", "제목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "DWFlag", "DW여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 60, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 배포업체 그리드

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid3, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                //wGrid.mfSetGridColumn(this.uGrid3, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "VendorCode", "업체코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "VendorName", "업체명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "PersonName", "담당자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "Hp", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "###-####-####", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "Email", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid3, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //적용범위 그리드

                //일반설정
                wGrid.mfInitGeneralGrid(this.uGrid4, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컴럼추가
                //wGrid.mfSetGridColumn(this.uGrid4, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGrid4, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "ProcessGroup", "공정그룹", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid4, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcessGroup);

                DataTable dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid4, 0, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcessGroup);

                //합의담당자 그리드

                //일반설정
                wGrid.mfInitGeneralGrid(this.uGrid5, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컴럼추가
                //wGrid.mfSetGridColumn(this.uGrid5, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 30, false, false, 0
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGrid5, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid5, 0, "AgreeUserID", "담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid5, 0, "AgreeUserName", "담당자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid5, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid3.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid3.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid4.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid4.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid5.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid5.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;
                QRPISO.BL.ISODOC.ISODocDistReqD ReqD;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDistReqD), "ISODocDistReqD");
                ReqD = new QRPISO.BL.ISODOC.ISODocDistReqD();
                brwChannel.mfCredentials(ReqD);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strStdNumber = this.uTextSearchStandardNum.Text;
                String strProcessCode = this.uComboSearchProcess.Value.ToString();
                String strDocTitle = this.uTextSearchTitle.Text;
                String strDistVendorCode = this.uTextSearchDistributeCompanyID.Text;
                String strDistReqDateFrom = Convert.ToDateTime(this.uDateSearchDistributeRequestFromDate.Value).ToString("yyyy-MM-dd");
                String strDistReqDateTo = Convert.ToDateTime(this.uDateSearchDistributeRequestToDate.Value).ToString("yyyy-MM-dd");


                DataTable dt = ReqD.mfReadISODocDistReqDMgn(strPlantCode, strStdNumber, strDocTitle, strProcessCode, strDistVendorCode
                                                            , strDistReqDateFrom, strDistReqDateTo, m_resSys.GetString("SYS_LANG"));

                this.uGrid1.DataSource = dt;
                this.uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
                InitValue();

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 720;

                    for(int i=0; i<this.uGrid1.Rows.Count; i++)
                    {
                        this.uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //공장코드 변경시 공정 콤보 변화
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                this.uComboSearchProcess.Items.Clear();

                strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                           , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100
                           , Infragistics.Win.HAlign.Center, "", "", "선택", "ProcessCode", "ProcessName", dtProcess);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //배포업체 검색 에디트버튼 클릭
        private void uTextSearchDistributeCompanyID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //uTextSearchDistributeCompanyID
                frmPOP0004 frmVendor = new frmPOP0004();
                frmVendor.ShowDialog();
                this.uTextSearchDistributeCompanyID.Text = frmVendor.CustomerCode;
                this.uTextSearchDistributeCompanyName.Text = frmVendor.CustomerName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드를 더블클릭 했을때 표준문서 헤더 검색
        private void uGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                //InitValue();

                e.Row.Fixed = true;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPopup = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;



                //함수호출
                QRPBrowser brwChannel = new QRPBrowser();

                //Header
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocH), "ISODocH");
                QRPISO.BL.ISODOC.ISODocH header = new QRPISO.BL.ISODOC.ISODocH();
                brwChannel.mfCredentials(header);
                //AgreeUser
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocAgreeUser), "ISODocAgreeUser");
                QRPISO.BL.ISODOC.ISODocAgreeUser agreeUser = new QRPISO.BL.ISODOC.ISODocAgreeUser();
                brwChannel.mfCredentials(agreeUser);
                //Dept
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocDept), "ISODocDept");
                QRPISO.BL.ISODOC.ISODocDept dept = new QRPISO.BL.ISODOC.ISODocDept();
                brwChannel.mfCredentials(dept);
                //Vendor
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocVendor), "ISODocVendor");
                QRPISO.BL.ISODOC.ISODocVendor vendor = new QRPISO.BL.ISODOC.ISODocVendor();
                brwChannel.mfCredentials(vendor);
                //File
                brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISODOC.ISODocFile), "ISODocFile");
                QRPISO.BL.ISODOC.ISODocFile file = new QRPISO.BL.ISODOC.ISODocFile();
                brwChannel.mfCredentials(file);


                //헤더용
                String strVersionNum = this.uGrid1.ActiveCell.Row.Cells["VersionNum"].Value.ToString();
                //공통인수
                String strPlantCode = this.uGrid1.ActiveCell.Row.Cells["PlantCode"].Value.ToString();
                String strStdNumber = this.uGrid1.ActiveRow.Cells["StdNumber"].Value.ToString();



                //header
                DataTable dtHeader = header.mfReadISODocHForDistReq(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));


                this.uTextStdDocNum.Text = dtHeader.Rows[0]["StdNumber"].ToString();
                this.uTextPlant.Text = dtHeader.Rows[0]["PlantName"].ToString();
                this.uTextRevisionNum.Text = dtHeader.Rows[0]["VersionNum"].ToString();
                this.uTextLarge.Text = dtHeader.Rows[0]["LTypeName"].ToString();
                this.uTextMiddle.Text = dtHeader.Rows[0]["MTypeName"].ToString();
                this.uTextSmall.Text = dtHeader.Rows[0]["STypeName"].ToString();
                this.uTextCreateUserID.Text = dtHeader.Rows[0]["WriteID"].ToString();
                this.uTextCreateUserName.Text = dtHeader.Rows[0]["WriteName"].ToString();
                this.uTextCreateDate.Text = dtHeader.Rows[0]["WriteDate"].ToString();
                this.uTextRevDiscardDiv.Text = dtHeader.Rows[0]["RevDisuseType"].ToString();
                this.uTextRevDiscardUserID.Text = dtHeader.Rows[0]["RevDisuseID"].ToString();
                this.uTextRevDiscardUserName.Text = dtHeader.Rows[0]["RevDisuseName"].ToString();
                this.uTextRevDiscardDate.Text = dtHeader.Rows[0]["RevDisuseDate"].ToString();
                this.uTextRevDiscardReason.Text = dtHeader.Rows[0]["RevDisuseReason"].ToString();
                this.uTextChangeItem.Text = dtHeader.Rows[0]["ChangeItem"].ToString();
                this.uTextFrom.Text = dtHeader.Rows[0]["FromDesc"].ToString();
                this.uTextTo.Text = dtHeader.Rows[0]["ToDesc"].ToString();
                this.uTextApproveUserID.Text = dtHeader.Rows[0]["AdmitID"].ToString();
                this.uTextApproveUserName.Text = dtHeader.Rows[0]["AdmitName"].ToString();
                this.uTextDocState.Text = dtHeader.Rows[0]["AdmitStatus"].ToString();
                this.uTextTitle.Text = dtHeader.Rows[0]["DocTitle"].ToString();
                this.uTextDocKeyword.Text = dtHeader.Rows[0]["DocKeyword"].ToString();
                this.uTextApplyFromDate.Text = dtHeader.Rows[0]["ApplyDateFrom"].ToString();
                this.uTextApplyToDate.Text = dtHeader.Rows[0]["ApplyDateTo"].ToString();
                this.uTextCompleteDate.Text = dtHeader.Rows[0]["CompleteDate"].ToString();
                this.uTextEtc.Text = dtHeader.Rows[0]["DocDesc"].ToString();
                this.uTextGRWComment.Text = dtHeader.Rows[0]["GRWComment"].ToString();
                this.uTextApproveDate.Text = dtHeader.Rows[0]["AdmitDate"].ToString();
                this.uTextMakeUserName.Text = dtHeader.Rows[0]["MakeUserName"].ToString();
                this.uTextMakeDeptName.Text = dtHeader.Rows[0]["MakeDeptName"].ToString();
                this.uTextPackage.Text = dtHeader.Rows[0]["Package"].ToString();
                this.uTextChipSize.Text = dtHeader.Rows[0]["ChipSize"].ToString();
                this.uTextPadSize.Text = dtHeader.Rows[0]["PadSize"].ToString();

                WinGrid grd = new WinGrid();

                //AgreeUser Start//
                DataTable dtAgree = agreeUser.mfReadISODocAgreeUser(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid5.DataSource = dtAgree;
                this.uGrid5.DataBind();
                if (dtAgree.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid5, 0);
                //AGreeUser End//

                //DeptStart//
                DataTable dtDept = dept.mfReadISODocDept(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid4.DataSource = dtDept;
                this.uGrid4.DataBind();
                if (dtDept.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid4, 0);
                //Dept End//

                //Vendor Start//
                DataTable dtVendor = vendor.mfReadISODocVendor(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid3.DataSource = dtVendor;
                this.uGrid3.DataBind();
                if (dtVendor.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid3, 0);
                //Vendor End//

                //File Start//
                DataTable dtFile = file.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));
                this.uGrid2.DataSource = dtFile;
                this.uGrid2.DataBind();
                if (dtFile.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGrid2, 0);

                for (int i = 0; i < dtFile.Rows.Count; i++)
                {
                    if (dtFile.Rows[i]["DWFlag"].ToString() == "T")
                    {
                        this.uGrid2.Rows[i].Cells["DWFlag"].Value = true;
                    }
                    else
                    {
                        this.uGrid2.Rows[i].Cells["DWFlag"].Value = false;
                    }
                }
                //File End//


                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                //DialogResult DResult = new DialogResult();
                //WinMessageBox msg = new WinMessageBox();
                //if (dtAgree.Rows.Count == 0)
                //{
                //    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //       , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);

                //    this.uGroupBoxContentsArea.Expanded = false;
                //}

                this.uGroupBoxContentsArea.Expanded = true;
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {

                Int32 intCheck = 0;
                Int32 intCheckCount = 0;

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGrid2.Rows[i].Cells["Check"].Value.ToString()) == true)
                    {
                        if (this.uGrid2.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\")
                            || this.uGrid2.Rows[i].Cells["FileName"].Value.ToString() == "")
                        {
                            intCheck++;
                        }
                        intCheckCount++;
                    }
                }
                if (intCheck > 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001148",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else if (intCheckCount == 0)
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000962", "M001144",
                                Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                    saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                    saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                    saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                    saveFolder.Description = "Download Folder";

                    if (saveFolder.ShowDialog() == DialogResult.OK)
                    {
                        string strSaveFolder = saveFolder.SelectedPath + "\\";
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");


                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();



                        for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                            {
                                if (this.uGrid2.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                                {
                                    arrFile.Add(this.uGrid2.Rows[i].Cells["FileName"].Value.ToString());
                                }
                            }
                        }
                        fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                               dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (this.uTextStdDocNum.Text + "-" + this.uTextRevisionNum.Text),
                                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmISOZ0001D_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);

        }

        private void uGrid2_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.Cell.Column.ToString() == "FileName")
                {
                    if (e.Cell.ToString() == "" || e.Cell.ToString().Contains(":\\"))
                    {
                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000962", "M000356",
                                            Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else
                    {
                            QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                            //화일서버 연결정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "S02");

                            //설비이미지 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uGrid1.ActiveRow.Cells["PlantCode"].Value.ToString(), "D0006");

                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            string strExePath = Application.ExecutablePath;
                            int intPos = strExePath.LastIndexOf(@"\");
                            strExePath = strExePath.Substring(0, intPos + 1);

                            arrFile.Add(e.Cell.Value.ToString());
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\", 
                                                                                   dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + (this.uTextStdDocNum.Text + "-" + this.uTextRevisionNum.Text),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());

                            fileAtt.ShowDialog();
                            System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + e.Cell.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmISOZ0001D_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        //배포부수 1000단위 자릿수 표시
        private void uGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            try
            {
                e.Layout.Bands[0].Columns["DistNumber"].MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals;
                e.Layout.Bands[0].Columns["DistNumber"].MaskInput = "nnn,nnn,nnn";
            }
            catch (Exception ex)
            { }
            finally
            { }
        }
    }
}
