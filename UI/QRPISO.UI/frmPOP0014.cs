﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공정검사규격서 UI                                     */
/* 모듈(분류)명 : 공정검사규격서 POPUP                                  */
/* 프로그램ID   : frmPOP0014.cs                                         */
/* 프로그램명   : 공정검사규격서 POPUP                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;
using System.Threading;

namespace QRPISO.UI
{
    public partial class frmPOP0014 : Form
    {
        // 리소소 호출
        QRPGlobal SysRes = new QRPGlobal();

        // 규격서정보 속성
        private string strPlantCode;
        private string strStdNumber;
        private string strStdSeq;
        private string strPackage;
        private DataTable ReturnTable = new DataTable();

        public string PlantCode
        {
            get { return strPlantCode; }
            set { strPlantCode = value; }
        }

        public string StdNumber
        {
            get { return strStdNumber; }
            set { strStdNumber = value; }
        }

        public string StdSeq
        {
            get { return strStdSeq; }
            set { strStdSeq = value; }
        }

        public string Package
        {
            get { return strPackage; }
            set { strPackage = value; }
        }

        public DataTable dtRtn
        {
            get { return ReturnTable; }
            set { ReturnTable = value; }
        }

        public frmPOP0014()
        {
            InitializeComponent();
        }

        private void frmPOP0014_Load(object sender, EventArgs e)
        {
            strStdNumber = "";
            strStdSeq = "";

            //초기화 메소드 호출
            InitLabel();
            InitCombo();
            InitGrid();
            InitButton();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(this.uLabelCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화

        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장 콤보박스
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dt);

                // Package 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                // DB로부터 데이터 가져오는 Method
                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));

                // 검색조건 : Package 콤보박스                
                wCombo.mfSetComboEditor(this.uComboSearchPacakge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "Package", "ComboName", dtPackage);

                //검색조건 : 고객사
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택"
                    , "CustomerCode", "CustomerName", dtCustomer);
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드


                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "CustomerName", "고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 20, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridList, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");
                
                grd.mfSetGridColumn(this.uGridList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                grd.mfInitGeneralGrid(this.uGridSpecD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridSpecD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "####", "0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "ProcessCode1", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "ProcessSeq", "공정순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "####", "0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectGroupCode", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectTypeCode", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectItemCode", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectItemName", "검사항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "Stack", "Stack", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 0
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nn", "1");

                //wGrid.mfSetGridColumn(this.uGridPRCDetail, 0, "StackSeq", "Stack", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "StackSeq", "Stack", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "Generation", "세대", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "ProcessInspectFlag", "공정검사", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "ProductItemFlag", "생산Item", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "QualityItemFlag", "품질Item", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "LowerSpec", "규격하한", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnn.nnnnn", "0.0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "UpperSpec", "규격상한", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "-nnnnn.nnnnn", "0.0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "SpecRange", "범위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "SpecUnitCode", "규격단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "SampleSize", "SampleSize", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "ProcessInspectSS", "공정S/S", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnn.nnn", "0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "ProductItemSS", "생산ItemS/S", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "QualityItemSS", "품질ItemS/S", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "UnitCode", "단위명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectPeriod", "검사주기", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "nnn", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "PeriodUnitCode", "주기단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "CompareFlag", "비교여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "AQLFlag", "AQL여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "DataType", "데이터 유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 2
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "Method", "Method", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "SpecDesc", "규격설명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSpecD, 0, "MeasureToolCode", "측정기기", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                this.uGridSpecD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSpecD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 버튼초기화


        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 조회
        /// </summary>
        private void Search()
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            this.Cursor = Cursors.WaitCursor;

            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecH), "ProcessInspectSpecH");
            QRPISO.BL.ISOPRC.ProcessInspectSpecH clsHeader = new QRPISO.BL.ISOPRC.ProcessInspectSpecH();
            brwChannel.mfCredentials(clsHeader);

            string strPlantCode = this.uComboSearchPlant.Value.ToString();            
            string strPackage = this.uComboSearchPacakge.Value.ToString();
            string strCustomerCode = this.uComboCustomer.Value.ToString();

            DataTable dtHeader = clsHeader.mfReadISOProcessInspectSpecH(strPlantCode, strPackage, strCustomerCode, m_resSys.GetString("SYS_LANG"));

            this.uGridList.DataSource = dtHeader;
            this.uGridList.DataBind();

            this.Cursor = Cursors.Default;

            DialogResult DResult = new DialogResult();
            WinMessageBox msg = new WinMessageBox();
            if (dtHeader.Rows.Count == 0)
                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
        }

        // 검색버튼

        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        // 닫기버튼
        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // 확인버튼
        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                //strPlantCode = this.uGridList.ActiveRow.Cells["PlantCode"].Text.ToString();
                //strStdNumber = this.uGridList.ActiveRow.Cells["StdNumber"].Text.ToString();
                //this.Close();
                //dtRtn.Columns.Add("PlantCode", typeof(string));
                //dtRtn.Columns.Add("Seq", typeof(int));
                ReturnTable.Columns.Add("ProcessCode1", typeof(string));
                ReturnTable.Columns.Add("ProcessCode", typeof(string));
                ReturnTable.Columns.Add("ProcessSeq", typeof(string));
                ReturnTable.Columns.Add("InspectGroupCode", typeof(string));
                ReturnTable.Columns.Add("InspectTypeCode", typeof(string));
                ReturnTable.Columns.Add("InspectItemCode", typeof(string));
                ReturnTable.Columns.Add("StackSeq", typeof(string));
                ReturnTable.Columns.Add("Generation", typeof(string));
                ReturnTable.Columns.Add("ProcessInspectFlag", typeof(string));
                ReturnTable.Columns.Add("ProductItemFlag", typeof(string));
                ReturnTable.Columns.Add("QualityItemFlag", typeof(string));
                ReturnTable.Columns.Add("LowerSpec", typeof(string));
                ReturnTable.Columns.Add("UpperSpec", typeof(string));
                ReturnTable.Columns.Add("SpecRange", typeof(string));
                ReturnTable.Columns.Add("SpecUnitCode", typeof(string));
                ReturnTable.Columns.Add("SampleSize", typeof(string));
                ReturnTable.Columns.Add("ProcessInspectSS", typeof(string));
                ReturnTable.Columns.Add("ProductItemSS", typeof(string));
                ReturnTable.Columns.Add("QualityItemSS", typeof(string));
                ReturnTable.Columns.Add("UnitCode", typeof(string));
                ReturnTable.Columns.Add("InspectPeriod", typeof(string));
                ReturnTable.Columns.Add("PeriodUnitCode", typeof(string));
                ReturnTable.Columns.Add("CompareFlag", typeof(string));
                ReturnTable.Columns.Add("AQLFlag", typeof(string));
                ReturnTable.Columns.Add("DataType", typeof(string));
                ReturnTable.Columns.Add("EtcDesc", typeof(string));
                ReturnTable.Columns.Add("InspectCondition", typeof(string));
                ReturnTable.Columns.Add("Method", typeof(string));
                ReturnTable.Columns.Add("SpecDesc", typeof(string));
                ReturnTable.Columns.Add("MeasureToolCode", typeof(string));

                DataRow dr;

                for (int i = 0; i < this.uGridSpecD.Rows.Count; i++)
                {
                    if (this.uGridSpecD.Rows[i].Cells["Check"].Value.ToString().Equals("True"))
                    {
                        dr = ReturnTable.NewRow();

                        dr["ProcessCode1"] = this.uGridSpecD.Rows[i].Cells["ProcessCode1"].Value.ToString();
                        dr["ProcessCode"] = this.uGridSpecD.Rows[i].Cells["ProcessCode"].Value.ToString();
                        dr["ProcessSeq"] = this.uGridSpecD.Rows[i].Cells["ProcessSeq"].Value.ToString();
                        dr["InspectGroupCode"] = this.uGridSpecD.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                        dr["InspectTypeCode"] = this.uGridSpecD.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                        dr["InspectItemCode"] = this.uGridSpecD.Rows[i].Cells["InspectItemCode"].Value.ToString();
                        dr["StackSeq"] = this.uGridSpecD.Rows[i].Cells["StackSeq"].Value.ToString();
                        dr["Generation"] = this.uGridSpecD.Rows[i].Cells["Generation"].Value.ToString();
                        dr["ProcessInspectFlag"] = this.uGridSpecD.Rows[i].Cells["ProcessInspectFlag"].Value.ToString();
                        dr["ProductItemFlag"] = this.uGridSpecD.Rows[i].Cells["ProductItemFlag"].Value.ToString();
                        dr["QualityItemFlag"] = this.uGridSpecD.Rows[i].Cells["QualityItemFlag"].Value.ToString();
                        dr["LowerSpec"] = this.uGridSpecD.Rows[i].Cells["LowerSpec"].Value.ToString();
                        dr["UpperSpec"] = this.uGridSpecD.Rows[i].Cells["UpperSpec"].Value.ToString();
                        dr["SpecRange"] = this.uGridSpecD.Rows[i].Cells["SpecRange"].Value.ToString();
                        dr["SpecUnitCode"] = this.uGridSpecD.Rows[i].Cells["SpecUnitCode"].Value.ToString();
                        dr["SampleSize"] = this.uGridSpecD.Rows[i].Cells["SampleSize"].Value.ToString();
                        dr["ProcessInspectSS"] = this.uGridSpecD.Rows[i].Cells["ProcessInspectSS"].Value.ToString();
                        dr["ProductItemSS"] = this.uGridSpecD.Rows[i].Cells["ProductItemSS"].Value.ToString();
                        dr["QualityItemSS"] = this.uGridSpecD.Rows[i].Cells["QualityItemSS"].Value.ToString();
                        dr["UnitCode"] = this.uGridSpecD.Rows[i].Cells["UnitCode"].Value.ToString();
                        dr["InspectPeriod"] = this.uGridSpecD.Rows[i].Cells["InspectPeriod"].Value.ToString();
                        dr["PeriodUnitCode"] = this.uGridSpecD.Rows[i].Cells["PeriodUnitCode"].Value.ToString();
                        dr["CompareFlag"] = this.uGridSpecD.Rows[i].Cells["CompareFlag"].Value.ToString();
                        dr["AQLFlag"] = this.uGridSpecD.Rows[i].Cells["AQLFlag"].Value.ToString();
                        dr["DataType"] = this.uGridSpecD.Rows[i].Cells["DataType"].Value.ToString();
                        dr["EtcDesc"] = this.uGridSpecD.Rows[i].Cells["EtcDesc"].Value.ToString();
                        dr["InspectCondition"] = this.uGridSpecD.Rows[i].Cells["InspectCondition"].Value.ToString();
                        dr["Method"] = this.uGridSpecD.Rows[i].Cells["Method"].Value.ToString();
                        dr["SpecDesc"] = this.uGridSpecD.Rows[i].Cells["SpecDesc"].Value.ToString();
                        dr["MeasureToolCode"] = this.uGridSpecD.Rows[i].Cells["MeasureToolCode"].Value.ToString();

                        ReturnTable.Rows.Add(dr);
                    }
                }

                ////for (int i = 0; i < this.uGridSpecD.Rows.Count; i++)
                ////{
                ////    if (!Convert.ToBoolean(this.uGridSpecD.Rows[i].Cells["Check"].Value))
                ////    {
                ////        this.uGridSpecD.Rows[i].Delete(false);
                ////        i--;
                ////    }
                ////}
                ////((DataTable)this.uGridSpecD.DataSource).AcceptChanges();
                ////DataTable dtGrid = ((DataTable)this.uGridSpecD.DataSource);

                //dtRtn = dtGrid.Copy();

                dtRtn = ReturnTable.Copy();
                this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                ReturnTable.Dispose();
            }
        }

        private void uGridList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            WinMessageBox msg = new WinMessageBox();

            // ProgressPopup 생성
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            Thread threadPop = m_ProgressPopup.mfStartThread();
            m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
            //this.MdiParent.Cursor = Cursors.WaitCursor;
            Cursor = Cursors.WaitCursor;

            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfRegisterChannel(typeof(QRPISO.BL.ISOPRC.ProcessInspectSpecD), "ProcessInspectSpecD");
            QRPISO.BL.ISOPRC.ProcessInspectSpecD clsSepcD = new QRPISO.BL.ISOPRC.ProcessInspectSpecD();
            brwChannel.mfCredentials(clsSepcD);

            strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();

            strStdNumber = e.Row.Cells["StdNumber"].Value.ToString().Substring(0, 9);
            strStdSeq = e.Row.Cells["StdNumber"].Value.ToString().Substring(9, 4);

            DataTable dtRtn = clsSepcD.mfRedISOProcessInspectSpecDPop(strPlantCode, strStdNumber, StdSeq, m_resSys.GetString("SYS_LANG"));

            // 팝업창 Close
            //this.MdiParent.Cursor = Cursors.Default;
            Cursor = Cursors.Default;
            m_ProgressPopup.mfCloseProgressPopup(this);

            if (dtRtn.Rows.Count == 0)
            {
                msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    , "M001264", "M000203", "M000204", Infragistics.Win.HAlign.Right);

                this.uGridSpecD.DataSource = dtRtn;
                this.uGridSpecD.DataBind();

                return;
            }
            else
            {
                this.uGridSpecD.DataSource = dtRtn;
                this.uGridSpecD.DataBind();
            }
        }

        private void uGridSpecD_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("Check"))
                {
                    //string strvalue = e.Cell.Value.ToString();
                    if (e.Cell.Value.ToString().Equals("True"))
                    {
                        e.Cell.Value = "False";
                    }
                    else
                    {
                        e.Cell.Value = "True";
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
        }                  
    }
}
