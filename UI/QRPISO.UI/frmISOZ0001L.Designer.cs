﻿namespace QRPISO.UI
{
    partial class frmISOZ0001L
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISOZ0001L));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton5 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uDateSearchDistributeRequestToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchDistributeRequestFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDistributeRequestDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchDistributeCompanyName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchDistributeCompanyID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchDistributeCompany = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchStandardNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStandardNum = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextCollectUnusual = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCollectUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCollectUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCollectUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCollectUser = new Infragistics.Win.Misc.UltraLabel();
            this.uNumCollectAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelCollectAmount = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCollectDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ulabelCollectDate = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckCollect = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uTextDistributeUnusual = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDistributeUnusual = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDistributeUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDistributeUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDistributeUser = new Infragistics.Win.Misc.UltraLabel();
            this.uNumDistributeAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uLabelDistributeAmount = new Infragistics.Win.Misc.UltraLabel();
            this.uDateDistributeDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDistributeDate = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckDistribute = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCollectUnusual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCollectUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCollectUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumCollectAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCollectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCollect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeUnusual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumDistributeAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDistributeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDistribute)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchDistributeRequestToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchDistributeRequestFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDistributeRequestDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDistributeCompanyName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchDistributeCompanyID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDistributeCompany);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStandardNum);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 4;
            // 
            // uDateSearchDistributeRequestToDate
            // 
            this.uDateSearchDistributeRequestToDate.Location = new System.Drawing.Point(604, 36);
            this.uDateSearchDistributeRequestToDate.Name = "uDateSearchDistributeRequestToDate";
            this.uDateSearchDistributeRequestToDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchDistributeRequestToDate.TabIndex = 24;
            // 
            // ultraLabel2
            // 
            appearance39.TextHAlignAsString = "Center";
            appearance39.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance39;
            this.ultraLabel2.Location = new System.Drawing.Point(588, 36);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 20);
            this.ultraLabel2.TabIndex = 23;
            this.ultraLabel2.Text = "~";
            // 
            // uDateSearchDistributeRequestFromDate
            // 
            this.uDateSearchDistributeRequestFromDate.Location = new System.Drawing.Point(484, 36);
            this.uDateSearchDistributeRequestFromDate.Name = "uDateSearchDistributeRequestFromDate";
            this.uDateSearchDistributeRequestFromDate.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchDistributeRequestFromDate.TabIndex = 22;
            // 
            // uLabelSearchDistributeRequestDate
            // 
            this.uLabelSearchDistributeRequestDate.Location = new System.Drawing.Point(380, 36);
            this.uLabelSearchDistributeRequestDate.Name = "uLabelSearchDistributeRequestDate";
            this.uLabelSearchDistributeRequestDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDistributeRequestDate.TabIndex = 21;
            this.uLabelSearchDistributeRequestDate.Text = "ultraLabel6";
            // 
            // uTextSearchDistributeCompanyName
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDistributeCompanyName.Appearance = appearance16;
            this.uTextSearchDistributeCompanyName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchDistributeCompanyName.Location = new System.Drawing.Point(218, 36);
            this.uTextSearchDistributeCompanyName.Name = "uTextSearchDistributeCompanyName";
            this.uTextSearchDistributeCompanyName.ReadOnly = true;
            this.uTextSearchDistributeCompanyName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDistributeCompanyName.TabIndex = 20;
            // 
            // uTextSearchDistributeCompanyID
            // 
            appearance17.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance17.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance17;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchDistributeCompanyID.ButtonsRight.Add(editorButton1);
            this.uTextSearchDistributeCompanyID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextSearchDistributeCompanyID.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchDistributeCompanyID.Name = "uTextSearchDistributeCompanyID";
            this.uTextSearchDistributeCompanyID.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchDistributeCompanyID.TabIndex = 19;
            this.uTextSearchDistributeCompanyID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchDistributeCompanyID_EditorButtonClick);
            // 
            // uLabelSearchDistributeCompany
            // 
            this.uLabelSearchDistributeCompany.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchDistributeCompany.Name = "uLabelSearchDistributeCompany";
            this.uLabelSearchDistributeCompany.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDistributeCompany.TabIndex = 18;
            this.uLabelSearchDistributeCompany.Text = "ultraLabel1";
            // 
            // uTextSearchTitle
            // 
            this.uTextSearchTitle.Location = new System.Drawing.Point(644, 12);
            this.uTextSearchTitle.Name = "uTextSearchTitle";
            this.uTextSearchTitle.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchTitle.TabIndex = 17;
            // 
            // uTextSearchStandardNum
            // 
            this.uTextSearchStandardNum.Location = new System.Drawing.Point(380, 12);
            this.uTextSearchStandardNum.Name = "uTextSearchStandardNum";
            this.uTextSearchStandardNum.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchStandardNum.TabIndex = 16;
            // 
            // uLabelSearchTitle
            // 
            this.uLabelSearchTitle.Location = new System.Drawing.Point(540, 12);
            this.uLabelSearchTitle.Name = "uLabelSearchTitle";
            this.uLabelSearchTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTitle.TabIndex = 12;
            this.uLabelSearchTitle.Text = "ultraLabel1";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(908, 36);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchProcess.TabIndex = 11;
            this.uComboSearchProcess.Text = "ultraComboEditor6";
            // 
            // uLabelSearchStandardNum
            // 
            this.uLabelSearchStandardNum.Location = new System.Drawing.Point(276, 12);
            this.uLabelSearchStandardNum.Name = "uLabelSearchStandardNum";
            this.uLabelSearchStandardNum.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStandardNum.TabIndex = 8;
            this.uLabelSearchStandardNum.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance7;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance5;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance6.BackColor2 = System.Drawing.SystemColors.Control;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance6;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance15;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance9;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance8;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance14.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance13;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance11.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance11;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 100);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 748);
            this.uGrid1.TabIndex = 5;
            this.uGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGrid1_DoubleClickRow);
            this.uGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGrid1_InitializeLayout);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 6;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCollectUnusual);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCollectUnusual);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCollectUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCollectUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCollectUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uNumCollectAmount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCollectAmount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateCollectDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ulabelCollectDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckCollect);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDistributeUnusual);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDistributeUnusual);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDistributeUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDistributeUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDistributeUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uNumDistributeAmount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDistributeAmount);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateDistributeDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDistributeDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckDistribute);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uGrid2);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 156);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1048, 492);
            this.uGroupBox1.TabIndex = 42;
            this.uGroupBox1.Text = "ultraGroupBox1";
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance20;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            appearance27.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance27;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance28.BackColor = System.Drawing.SystemColors.Control;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance28;
            appearance29.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance30;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance31;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 28);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1024, 456);
            this.uGrid2.TabIndex = 0;
            this.uGrid2.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.uGrid2_InitializeLayout);
            // 
            // uTextCollectUnusual
            // 
            this.uTextCollectUnusual.Location = new System.Drawing.Point(136, 132);
            this.uTextCollectUnusual.Name = "uTextCollectUnusual";
            this.uTextCollectUnusual.Size = new System.Drawing.Size(734, 21);
            this.uTextCollectUnusual.TabIndex = 41;
            // 
            // uLabelCollectUnusual
            // 
            this.uLabelCollectUnusual.Location = new System.Drawing.Point(12, 132);
            this.uLabelCollectUnusual.Name = "uLabelCollectUnusual";
            this.uLabelCollectUnusual.Size = new System.Drawing.Size(120, 20);
            this.uLabelCollectUnusual.TabIndex = 40;
            this.uLabelCollectUnusual.Text = "ultraLabel1";
            // 
            // uTextCollectUserName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCollectUserName.Appearance = appearance4;
            this.uTextCollectUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCollectUserName.Location = new System.Drawing.Point(706, 108);
            this.uTextCollectUserName.Name = "uTextCollectUserName";
            this.uTextCollectUserName.ReadOnly = true;
            this.uTextCollectUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextCollectUserName.TabIndex = 39;
            // 
            // uTextCollectUserID
            // 
            appearance3.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance3;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCollectUserID.ButtonsRight.Add(editorButton2);
            this.uTextCollectUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCollectUserID.Location = new System.Drawing.Point(604, 108);
            this.uTextCollectUserID.Name = "uTextCollectUserID";
            this.uTextCollectUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCollectUserID.TabIndex = 38;
            this.uTextCollectUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCollectUserID_KeyDown);
            this.uTextCollectUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCollectUserID_EditorButtonClick);
            // 
            // uLabelCollectUser
            // 
            this.uLabelCollectUser.Location = new System.Drawing.Point(480, 108);
            this.uLabelCollectUser.Name = "uLabelCollectUser";
            this.uLabelCollectUser.Size = new System.Drawing.Size(120, 20);
            this.uLabelCollectUser.TabIndex = 37;
            this.uLabelCollectUser.Text = "ultraLabel1";
            // 
            // uNumCollectAmount
            // 
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton3.Text = "0";
            this.uNumCollectAmount.ButtonsLeft.Add(editorButton3);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumCollectAmount.ButtonsRight.Add(spinEditorButton1);
            this.uNumCollectAmount.Location = new System.Drawing.Point(372, 108);
            this.uNumCollectAmount.MaskInput = "nnnnnnnn";
            this.uNumCollectAmount.Name = "uNumCollectAmount";
            this.uNumCollectAmount.Size = new System.Drawing.Size(100, 21);
            this.uNumCollectAmount.TabIndex = 36;
            this.uNumCollectAmount.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumCollectAmount_EditorSpinButtonClick);
            // 
            // uLabelCollectAmount
            // 
            this.uLabelCollectAmount.Location = new System.Drawing.Point(246, 108);
            this.uLabelCollectAmount.Name = "uLabelCollectAmount";
            this.uLabelCollectAmount.Size = new System.Drawing.Size(120, 20);
            this.uLabelCollectAmount.TabIndex = 35;
            this.uLabelCollectAmount.Text = "ultraLabel1";
            // 
            // uDateCollectDate
            // 
            this.uDateCollectDate.Location = new System.Drawing.Point(138, 108);
            this.uDateCollectDate.Name = "uDateCollectDate";
            this.uDateCollectDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCollectDate.TabIndex = 34;
            // 
            // ulabelCollectDate
            // 
            this.ulabelCollectDate.Location = new System.Drawing.Point(10, 108);
            this.ulabelCollectDate.Name = "ulabelCollectDate";
            this.ulabelCollectDate.Size = new System.Drawing.Size(120, 20);
            this.ulabelCollectDate.TabIndex = 33;
            this.ulabelCollectDate.Text = "ultraLabel6";
            // 
            // uCheckCollect
            // 
            this.uCheckCollect.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckCollect.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckCollect.Location = new System.Drawing.Point(12, 84);
            this.uCheckCollect.Name = "uCheckCollect";
            this.uCheckCollect.Size = new System.Drawing.Size(48, 20);
            this.uCheckCollect.TabIndex = 32;
            this.uCheckCollect.Text = "회수";
            this.uCheckCollect.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckCollect.CheckedValueChanged += new System.EventHandler(this.uCheckCollect_CheckedValueChanged);
            // 
            // uTextDistributeUnusual
            // 
            this.uTextDistributeUnusual.Location = new System.Drawing.Point(136, 60);
            this.uTextDistributeUnusual.Name = "uTextDistributeUnusual";
            this.uTextDistributeUnusual.Size = new System.Drawing.Size(734, 21);
            this.uTextDistributeUnusual.TabIndex = 31;
            // 
            // uLabelDistributeUnusual
            // 
            this.uLabelDistributeUnusual.Location = new System.Drawing.Point(12, 60);
            this.uLabelDistributeUnusual.Name = "uLabelDistributeUnusual";
            this.uLabelDistributeUnusual.Size = new System.Drawing.Size(120, 20);
            this.uLabelDistributeUnusual.TabIndex = 30;
            this.uLabelDistributeUnusual.Text = "ultraLabel1";
            // 
            // uTextDistributeUserName
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDistributeUserName.Appearance = appearance18;
            this.uTextDistributeUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDistributeUserName.Location = new System.Drawing.Point(706, 36);
            this.uTextDistributeUserName.Name = "uTextDistributeUserName";
            this.uTextDistributeUserName.ReadOnly = true;
            this.uTextDistributeUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextDistributeUserName.TabIndex = 29;
            // 
            // uTextDistributeUserID
            // 
            appearance19.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            appearance19.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance19;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextDistributeUserID.ButtonsRight.Add(editorButton4);
            this.uTextDistributeUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextDistributeUserID.Location = new System.Drawing.Point(604, 36);
            this.uTextDistributeUserID.Name = "uTextDistributeUserID";
            this.uTextDistributeUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextDistributeUserID.TabIndex = 28;
            this.uTextDistributeUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextDistributeUserID_KeyDown);
            this.uTextDistributeUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextDistributeUserID_EditorButtonClick);
            // 
            // uLabelDistributeUser
            // 
            this.uLabelDistributeUser.Location = new System.Drawing.Point(480, 36);
            this.uLabelDistributeUser.Name = "uLabelDistributeUser";
            this.uLabelDistributeUser.Size = new System.Drawing.Size(120, 20);
            this.uLabelDistributeUser.TabIndex = 27;
            this.uLabelDistributeUser.Text = "ultraLabel1";
            // 
            // uNumDistributeAmount
            // 
            editorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton5.Text = "0";
            this.uNumDistributeAmount.ButtonsLeft.Add(editorButton5);
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumDistributeAmount.ButtonsRight.Add(spinEditorButton2);
            this.uNumDistributeAmount.Location = new System.Drawing.Point(372, 36);
            this.uNumDistributeAmount.MaskInput = "nnnnnnnn";
            this.uNumDistributeAmount.Name = "uNumDistributeAmount";
            this.uNumDistributeAmount.Size = new System.Drawing.Size(100, 21);
            this.uNumDistributeAmount.TabIndex = 26;
            this.uNumDistributeAmount.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumDistributeAmount_EditorSpinButtonClick);
            // 
            // uLabelDistributeAmount
            // 
            this.uLabelDistributeAmount.Location = new System.Drawing.Point(246, 36);
            this.uLabelDistributeAmount.Name = "uLabelDistributeAmount";
            this.uLabelDistributeAmount.Size = new System.Drawing.Size(120, 20);
            this.uLabelDistributeAmount.TabIndex = 25;
            this.uLabelDistributeAmount.Text = "ultraLabel1";
            // 
            // uDateDistributeDate
            // 
            this.uDateDistributeDate.Location = new System.Drawing.Point(138, 36);
            this.uDateDistributeDate.Name = "uDateDistributeDate";
            this.uDateDistributeDate.Size = new System.Drawing.Size(100, 21);
            this.uDateDistributeDate.TabIndex = 24;
            // 
            // uLabelDistributeDate
            // 
            this.uLabelDistributeDate.Location = new System.Drawing.Point(10, 36);
            this.uLabelDistributeDate.Name = "uLabelDistributeDate";
            this.uLabelDistributeDate.Size = new System.Drawing.Size(120, 20);
            this.uLabelDistributeDate.TabIndex = 23;
            this.uLabelDistributeDate.Text = "ultraLabel6";
            // 
            // uCheckDistribute
            // 
            this.uCheckDistribute.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckDistribute.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckDistribute.Location = new System.Drawing.Point(12, 12);
            this.uCheckDistribute.Name = "uCheckDistribute";
            this.uCheckDistribute.Size = new System.Drawing.Size(48, 20);
            this.uCheckDistribute.TabIndex = 0;
            this.uCheckDistribute.Text = "배포";
            this.uCheckDistribute.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.uCheckDistribute.CheckedValueChanged += new System.EventHandler(this.uCheckDistribute_CheckedValueChanged);
            // 
            // frmISOZ0001L
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISOZ0001L";
            this.Load += new System.EventHandler(this.frmISOZ0001L_Load);
            this.Activated += new System.EventHandler(this.frmISOZ0001L_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISOZ0001L_FormClosing);
            this.Resize += new System.EventHandler(this.frmISOZ0001L_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchDistributeRequestFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchDistributeCompanyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCollectUnusual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCollectUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCollectUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumCollectAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCollectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckCollect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeUnusual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDistributeUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumDistributeAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDistributeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDistribute)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchDistributeRequestToDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchDistributeRequestFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDistributeRequestDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDistributeCompanyName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchDistributeCompanyID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDistributeCompany;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStandardNum;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStandardNum;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDistributeDate;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckDistribute;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeUnusual;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeUnusual;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDistributeUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeUser;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumDistributeAmount;
        private Infragistics.Win.Misc.UltraLabel uLabelDistributeAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCollectUnusual;
        private Infragistics.Win.Misc.UltraLabel uLabelCollectUnusual;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCollectUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCollectUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCollectUser;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumCollectAmount;
        private Infragistics.Win.Misc.UltraLabel uLabelCollectAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCollectDate;
        private Infragistics.Win.Misc.UltraLabel ulabelCollectDate;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckCollect;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
    }
}