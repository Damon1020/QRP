﻿namespace QRPISO.UI
{
    partial class frmISO0003D_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmISO0003D_S));
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchProcess = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckVersion = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelVersionCheck = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLotNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDiscard = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDiscard = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchRegToDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchRegFromDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchRegDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchStandardNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchSmall = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMiddle = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchLarge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uDateApplyDateTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateApplyDateFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateCreateDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateRev_DisDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextGRWComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelGRWComment = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakeDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakeUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMake = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPadSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPadSize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChipSize = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChipSize = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboRev_DisSeparation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextRev_DisUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextApproveUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextReturnReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelReturnReason = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelDept = new Infragistics.Win.Misc.UltraButton();
            this.uTextApproveDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSmall = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMiddle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLarge = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextProcess = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApproveDate = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDelVendor = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDelAgreeUser = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDown = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelFile = new Infragistics.Win.Misc.UltraButton();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCompleteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelApplyDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocKeyword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocKeyword = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTitle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextApproveUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelApproveUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelTo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextFrom = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFrom = new Infragistics.Win.Misc.UltraLabel();
            this.uTextChangeItem = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelChangeItem = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRev_DisReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRev_DisReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRev_DisDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRev_DisUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRev_DisUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRev_DisSeparation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSmall = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMiddle = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLarge = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevisionNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStandardDocNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardDocNo = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDiscard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRev_DisDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRev_DisSeparation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).BeginInit();
            this.uGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).BeginInit();
            this.uGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardDocNo)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance35;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uCheckVersion);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelVersionCheck);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelLotNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDiscard);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDiscard);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel2);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchRegToDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchRegFromDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchRegDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchTitle);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchStandardNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStandardNo);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchSmall);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMiddle);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchLarge);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uLabelSearchProcess
            // 
            this.uLabelSearchProcess.Location = new System.Drawing.Point(640, 36);
            this.uLabelSearchProcess.Name = "uLabelSearchProcess";
            this.uLabelSearchProcess.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcess.TabIndex = 39;
            this.uLabelSearchProcess.Text = "ultraLabel1";
            // 
            // uCheckVersion
            // 
            this.uCheckVersion.Location = new System.Drawing.Point(967, 12);
            this.uCheckVersion.Name = "uCheckVersion";
            this.uCheckVersion.Size = new System.Drawing.Size(16, 20);
            this.uCheckVersion.TabIndex = 37;
            // 
            // uLabelVersionCheck
            // 
            this.uLabelVersionCheck.Location = new System.Drawing.Point(863, 12);
            this.uLabelVersionCheck.Name = "uLabelVersionCheck";
            this.uLabelVersionCheck.Size = new System.Drawing.Size(100, 20);
            this.uLabelVersionCheck.TabIndex = 36;
            this.uLabelVersionCheck.Text = "ultraLabel1";
            // 
            // uTextLotNo
            // 
            this.uTextLotNo.Location = new System.Drawing.Point(1044, 36);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(12, 21);
            this.uTextLotNo.TabIndex = 35;
            this.uTextLotNo.Visible = false;
            // 
            // uLabelLotNo
            // 
            this.uLabelLotNo.Location = new System.Drawing.Point(1028, 36);
            this.uLabelLotNo.Name = "uLabelLotNo";
            this.uLabelLotNo.Size = new System.Drawing.Size(12, 20);
            this.uLabelLotNo.TabIndex = 34;
            this.uLabelLotNo.Text = "ultraLabel1";
            this.uLabelLotNo.Visible = false;
            // 
            // uComboSearchDiscard
            // 
            this.uComboSearchDiscard.Location = new System.Drawing.Point(1016, 4);
            this.uComboSearchDiscard.Name = "uComboSearchDiscard";
            this.uComboSearchDiscard.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchDiscard.TabIndex = 33;
            this.uComboSearchDiscard.Text = "ultraComboEditor1";
            this.uComboSearchDiscard.Visible = false;
            // 
            // uLabelSearchDiscard
            // 
            this.uLabelSearchDiscard.Location = new System.Drawing.Point(1020, 4);
            this.uLabelSearchDiscard.Name = "uLabelSearchDiscard";
            this.uLabelSearchDiscard.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchDiscard.TabIndex = 32;
            this.uLabelSearchDiscard.Text = "ultraLabel1";
            this.uLabelSearchDiscard.Visible = false;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(1044, 12);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel2.TabIndex = 31;
            this.ultraLabel2.Text = "~";
            this.ultraLabel2.Visible = false;
            // 
            // uDateSearchRegToDate
            // 
            this.uDateSearchRegToDate.Location = new System.Drawing.Point(1040, 4);
            this.uDateSearchRegToDate.Name = "uDateSearchRegToDate";
            this.uDateSearchRegToDate.Size = new System.Drawing.Size(21, 21);
            this.uDateSearchRegToDate.TabIndex = 30;
            this.uDateSearchRegToDate.Visible = false;
            // 
            // uDateSearchRegFromDate
            // 
            this.uDateSearchRegFromDate.Location = new System.Drawing.Point(1036, 4);
            this.uDateSearchRegFromDate.Name = "uDateSearchRegFromDate";
            this.uDateSearchRegFromDate.Size = new System.Drawing.Size(20, 21);
            this.uDateSearchRegFromDate.TabIndex = 29;
            this.uDateSearchRegFromDate.Visible = false;
            // 
            // uLabelSearchRegDate
            // 
            this.uLabelSearchRegDate.Location = new System.Drawing.Point(1044, 8);
            this.uLabelSearchRegDate.Name = "uLabelSearchRegDate";
            this.uLabelSearchRegDate.Size = new System.Drawing.Size(19, 20);
            this.uLabelSearchRegDate.TabIndex = 14;
            this.uLabelSearchRegDate.Text = "ultraLabel1";
            this.uLabelSearchRegDate.Visible = false;
            // 
            // uTextSearchTitle
            // 
            this.uTextSearchTitle.Location = new System.Drawing.Point(328, 36);
            this.uTextSearchTitle.Name = "uTextSearchTitle";
            this.uTextSearchTitle.Size = new System.Drawing.Size(306, 21);
            this.uTextSearchTitle.TabIndex = 13;
            // 
            // uLabelSearchTitle
            // 
            this.uLabelSearchTitle.Location = new System.Drawing.Point(224, 36);
            this.uLabelSearchTitle.Name = "uLabelSearchTitle";
            this.uLabelSearchTitle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchTitle.TabIndex = 12;
            this.uLabelSearchTitle.Text = "ultraLabel1";
            // 
            // uTextSearchStandardNo
            // 
            this.uTextSearchStandardNo.Location = new System.Drawing.Point(116, 36);
            this.uTextSearchStandardNo.Name = "uTextSearchStandardNo";
            this.uTextSearchStandardNo.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchStandardNo.TabIndex = 9;
            // 
            // uLabelSearchStandardNo
            // 
            this.uLabelSearchStandardNo.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchStandardNo.Name = "uLabelSearchStandardNo";
            this.uLabelSearchStandardNo.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStandardNo.TabIndex = 8;
            this.uLabelSearchStandardNo.Text = "ultraLabel1";
            // 
            // uComboSearchSmall
            // 
            this.uComboSearchSmall.Location = new System.Drawing.Point(744, 12);
            this.uComboSearchSmall.Name = "uComboSearchSmall";
            this.uComboSearchSmall.Size = new System.Drawing.Size(114, 21);
            this.uComboSearchSmall.TabIndex = 7;
            this.uComboSearchSmall.Text = "ultraComboEditor1";
            // 
            // uLabelSearchSmall
            // 
            this.uLabelSearchSmall.Location = new System.Drawing.Point(640, 12);
            this.uLabelSearchSmall.Name = "uLabelSearchSmall";
            this.uLabelSearchSmall.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchSmall.TabIndex = 6;
            this.uLabelSearchSmall.Text = "ultraLabel1";
            // 
            // uComboSearchMiddle
            // 
            this.uComboSearchMiddle.Location = new System.Drawing.Point(540, 12);
            this.uComboSearchMiddle.Name = "uComboSearchMiddle";
            this.uComboSearchMiddle.Size = new System.Drawing.Size(95, 21);
            this.uComboSearchMiddle.TabIndex = 5;
            this.uComboSearchMiddle.Text = "ultraComboEditor1";
            this.uComboSearchMiddle.ValueChanged += new System.EventHandler(this.uComboSearchMiddle_ValueChanged);
            // 
            // uLabelSearchMiddle
            // 
            this.uLabelSearchMiddle.Location = new System.Drawing.Point(436, 12);
            this.uLabelSearchMiddle.Name = "uLabelSearchMiddle";
            this.uLabelSearchMiddle.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMiddle.TabIndex = 4;
            this.uLabelSearchMiddle.Text = "ultraLabel1";
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(744, 36);
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(114, 21);
            this.uComboSearchProcess.TabIndex = 11;
            this.uComboSearchProcess.Text = "ultraComboEditor1";
            // 
            // uComboSearchLarge
            // 
            this.uComboSearchLarge.Location = new System.Drawing.Point(328, 12);
            this.uComboSearchLarge.Name = "uComboSearchLarge";
            this.uComboSearchLarge.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchLarge.TabIndex = 3;
            this.uComboSearchLarge.Text = "ultraComboEditor1";
            this.uComboSearchLarge.ValueChanged += new System.EventHandler(this.uComboSearchLarge_ValueChanged);
            // 
            // uLabelSearchLarge
            // 
            this.uLabelSearchLarge.Location = new System.Drawing.Point(224, 12);
            this.uLabelSearchLarge.Name = "uLabelSearchLarge";
            this.uLabelSearchLarge.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchLarge.TabIndex = 2;
            this.uLabelSearchLarge.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(100, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid
            // 
            this.uGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid.DisplayLayout.Appearance = appearance8;
            this.uGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance65.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance65.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance65.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance65.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.GroupByBox.Appearance = appearance65;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance36;
            this.uGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance37.BackColor2 = System.Drawing.SystemColors.Control;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance37;
            this.uGrid.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid.DisplayLayout.Override.ActiveRowAppearance = appearance39;
            this.uGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.CardAreaAppearance = appearance40;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            appearance41.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid.DisplayLayout.Override.CellAppearance = appearance41;
            this.uGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid.DisplayLayout.Override.CellPadding = 0;
            appearance42.BackColor = System.Drawing.SystemColors.Control;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid.DisplayLayout.Override.GroupByRowAppearance = appearance42;
            appearance43.TextHAlignAsString = "Left";
            this.uGrid.DisplayLayout.Override.HeaderAppearance = appearance43;
            this.uGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            this.uGrid.DisplayLayout.Override.RowAppearance = appearance44;
            this.uGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance45;
            this.uGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid.Location = new System.Drawing.Point(0, 100);
            this.uGrid.Name = "uGrid";
            this.uGrid.Size = new System.Drawing.Size(1070, 720);
            this.uGrid.TabIndex = 3;
            this.uGrid.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 5;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateApplyDateTo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateApplyDateFrom);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateRev_DisDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextGRWComment);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelGRWComment);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitVersionNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakeDeptName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakeUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMake);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPadSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPadSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextChipSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelChipSize);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPackage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboRev_DisSeparation);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRev_DisUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApproveUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextReturnReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelReturnReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApproveDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSmall);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMiddle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLarge);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProcess);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelApproveDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateCompleteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraLabel3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelApplyDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDocKeyword);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDocKeyword);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTitle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTitle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextDocState);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDocState);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextApproveUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelApproveUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextTo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelTo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextFrom);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFrom);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextChangeItem);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelChangeItem);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRev_DisReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRev_DisReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRev_DisDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRev_DisUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRev_DisUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRev_DisSeparation);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSmall);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMiddle);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLarge);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevisionNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardDocNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardDocNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uDateApplyDateTo
            // 
            this.uDateApplyDateTo.Location = new System.Drawing.Point(288, 204);
            this.uDateApplyDateTo.MaskInput = "yyyy-mm-dd";
            this.uDateApplyDateTo.Name = "uDateApplyDateTo";
            this.uDateApplyDateTo.Size = new System.Drawing.Size(80, 21);
            this.uDateApplyDateTo.TabIndex = 120;
            // 
            // uDateApplyDateFrom
            // 
            this.uDateApplyDateFrom.Location = new System.Drawing.Point(168, 204);
            this.uDateApplyDateFrom.MaskInput = "yyyy-mm-dd";
            this.uDateApplyDateFrom.Name = "uDateApplyDateFrom";
            this.uDateApplyDateFrom.Size = new System.Drawing.Size(104, 21);
            this.uDateApplyDateFrom.TabIndex = 119;
            // 
            // uDateCreateDate
            // 
            this.uDateCreateDate.Location = new System.Drawing.Point(1020, 236);
            this.uDateCreateDate.MaskInput = "yyyy-mm-dd";
            this.uDateCreateDate.Name = "uDateCreateDate";
            this.uDateCreateDate.Size = new System.Drawing.Size(16, 21);
            this.uDateCreateDate.TabIndex = 118;
            this.uDateCreateDate.Visible = false;
            // 
            // uDateRev_DisDate
            // 
            this.uDateRev_DisDate.Location = new System.Drawing.Point(1044, 236);
            this.uDateRev_DisDate.MaskInput = "yyyy-mm-dd";
            this.uDateRev_DisDate.Name = "uDateRev_DisDate";
            this.uDateRev_DisDate.Size = new System.Drawing.Size(16, 21);
            this.uDateRev_DisDate.TabIndex = 117;
            this.uDateRev_DisDate.Visible = false;
            // 
            // uTextGRWComment
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRWComment.Appearance = appearance18;
            this.uTextGRWComment.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextGRWComment.Location = new System.Drawing.Point(532, 36);
            this.uTextGRWComment.Name = "uTextGRWComment";
            this.uTextGRWComment.ReadOnly = true;
            this.uTextGRWComment.Size = new System.Drawing.Size(464, 21);
            this.uTextGRWComment.TabIndex = 116;
            // 
            // uLabelGRWComment
            // 
            this.uLabelGRWComment.Location = new System.Drawing.Point(376, 36);
            this.uLabelGRWComment.Name = "uLabelGRWComment";
            this.uLabelGRWComment.Size = new System.Drawing.Size(150, 20);
            this.uLabelGRWComment.TabIndex = 115;
            this.uLabelGRWComment.Text = "ultraLabel1";
            // 
            // uTextAdmitVersionNum
            // 
            appearance13.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitVersionNum.Appearance = appearance13;
            this.uTextAdmitVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitVersionNum.Location = new System.Drawing.Point(168, 36);
            this.uTextAdmitVersionNum.Name = "uTextAdmitVersionNum";
            this.uTextAdmitVersionNum.ReadOnly = true;
            this.uTextAdmitVersionNum.Size = new System.Drawing.Size(200, 21);
            this.uTextAdmitVersionNum.TabIndex = 112;
            // 
            // uTextMakeDeptName
            // 
            appearance46.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeDeptName.Appearance = appearance46;
            this.uTextMakeDeptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeDeptName.Location = new System.Drawing.Point(736, 60);
            this.uTextMakeDeptName.Name = "uTextMakeDeptName";
            this.uTextMakeDeptName.ReadOnly = true;
            this.uTextMakeDeptName.Size = new System.Drawing.Size(100, 21);
            this.uTextMakeDeptName.TabIndex = 111;
            // 
            // uTextMakeUserName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeUserName.Appearance = appearance17;
            this.uTextMakeUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakeUserName.Location = new System.Drawing.Point(1052, 212);
            this.uTextMakeUserName.Name = "uTextMakeUserName";
            this.uTextMakeUserName.ReadOnly = true;
            this.uTextMakeUserName.Size = new System.Drawing.Size(8, 21);
            this.uTextMakeUserName.TabIndex = 110;
            this.uTextMakeUserName.Visible = false;
            // 
            // uLabelMake
            // 
            this.uLabelMake.Location = new System.Drawing.Point(1040, 212);
            this.uLabelMake.Name = "uLabelMake";
            this.uLabelMake.Size = new System.Drawing.Size(12, 20);
            this.uLabelMake.TabIndex = 109;
            this.uLabelMake.Text = "ultraLabel1";
            this.uLabelMake.Visible = false;
            // 
            // uTextPadSize
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPadSize.Appearance = appearance5;
            this.uTextPadSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPadSize.Location = new System.Drawing.Point(168, 180);
            this.uTextPadSize.Name = "uTextPadSize";
            this.uTextPadSize.ReadOnly = true;
            this.uTextPadSize.Size = new System.Drawing.Size(200, 21);
            this.uTextPadSize.TabIndex = 108;
            // 
            // uLabelPadSize
            // 
            this.uLabelPadSize.Location = new System.Drawing.Point(12, 180);
            this.uLabelPadSize.Name = "uLabelPadSize";
            this.uLabelPadSize.Size = new System.Drawing.Size(150, 20);
            this.uLabelPadSize.TabIndex = 107;
            this.uLabelPadSize.Text = "ultraLabel1";
            // 
            // uTextChipSize
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChipSize.Appearance = appearance14;
            this.uTextChipSize.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChipSize.Location = new System.Drawing.Point(168, 156);
            this.uTextChipSize.Name = "uTextChipSize";
            this.uTextChipSize.ReadOnly = true;
            this.uTextChipSize.Size = new System.Drawing.Size(200, 21);
            this.uTextChipSize.TabIndex = 106;
            // 
            // uLabelChipSize
            // 
            this.uLabelChipSize.Location = new System.Drawing.Point(12, 156);
            this.uLabelChipSize.Name = "uLabelChipSize";
            this.uLabelChipSize.Size = new System.Drawing.Size(150, 20);
            this.uLabelChipSize.TabIndex = 105;
            this.uLabelChipSize.Text = "ultraLabel1";
            // 
            // uTextPackage
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Appearance = appearance15;
            this.uTextPackage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackage.Location = new System.Drawing.Point(168, 132);
            this.uTextPackage.Name = "uTextPackage";
            this.uTextPackage.ReadOnly = true;
            this.uTextPackage.Size = new System.Drawing.Size(200, 21);
            this.uTextPackage.TabIndex = 104;
            // 
            // uLabelPackage
            // 
            this.uLabelPackage.Location = new System.Drawing.Point(12, 132);
            this.uLabelPackage.Name = "uLabelPackage";
            this.uLabelPackage.Size = new System.Drawing.Size(150, 20);
            this.uLabelPackage.TabIndex = 103;
            this.uLabelPackage.Text = "ultraLabel1";
            // 
            // uComboRev_DisSeparation
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboRev_DisSeparation.Appearance = appearance3;
            this.uComboRev_DisSeparation.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboRev_DisSeparation.Location = new System.Drawing.Point(168, 60);
            this.uComboRev_DisSeparation.Name = "uComboRev_DisSeparation";
            this.uComboRev_DisSeparation.Size = new System.Drawing.Size(200, 21);
            this.uComboRev_DisSeparation.TabIndex = 102;
            this.uComboRev_DisSeparation.Text = "ultraComboEditor1";
            // 
            // uTextRev_DisUserID
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRev_DisUserID.Appearance = appearance10;
            this.uTextRev_DisUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance58.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            editorButton1.Appearance = appearance58;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRev_DisUserID.ButtonsRight.Add(editorButton1);
            this.uTextRev_DisUserID.Location = new System.Drawing.Point(532, 60);
            this.uTextRev_DisUserID.Name = "uTextRev_DisUserID";
            this.uTextRev_DisUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextRev_DisUserID.TabIndex = 101;
            this.uTextRev_DisUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRev_DisUserID_KeyDown);
            this.uTextRev_DisUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRev_DisUserID_EditorButtonClick);
            // 
            // uTextApproveUserID
            // 
            appearance33.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextApproveUserID.Appearance = appearance33;
            this.uTextApproveUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextApproveUserID.Location = new System.Drawing.Point(1036, 0);
            this.uTextApproveUserID.Name = "uTextApproveUserID";
            this.uTextApproveUserID.Size = new System.Drawing.Size(11, 21);
            this.uTextApproveUserID.TabIndex = 100;
            this.uTextApproveUserID.Visible = false;
            this.uTextApproveUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextApproveUserID_KeyDown);
            this.uTextApproveUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextApproveUserID_EditorButtonClick);
            // 
            // uTextCreateUserID
            // 
            appearance12.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCreateUserID.Appearance = appearance12;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance53.Image = global::QRPISO.UI.Properties.Resources.btn_Zoom;
            editorButton2.Appearance = appearance53;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCreateUserID.ButtonsRight.Add(editorButton2);
            this.uTextCreateUserID.Location = new System.Drawing.Point(532, 60);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 99;
            this.uTextCreateUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCreateUserID_KeyDown);
            this.uTextCreateUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCreateUserID_EditorButtonClick);
            // 
            // uGroupBox4
            // 
            this.uGroupBox4.Controls.Add(this.uGrid4);
            this.uGroupBox4.Location = new System.Drawing.Point(40, 32);
            this.uGroupBox4.Name = "uGroupBox4";
            this.uGroupBox4.Size = new System.Drawing.Size(32, 28);
            this.uGroupBox4.TabIndex = 98;
            this.uGroupBox4.Visible = false;
            // 
            // uGrid4
            // 
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid4.DisplayLayout.Appearance = appearance75;
            this.uGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance76.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance76.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance76.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.GroupByBox.Appearance = appearance76;
            appearance77.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance77;
            this.uGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance78.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance78.BackColor2 = System.Drawing.SystemColors.Control;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance78;
            this.uGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            appearance79.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance79;
            appearance80.BackColor = System.Drawing.SystemColors.Highlight;
            appearance80.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance80;
            this.uGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.CardAreaAppearance = appearance81;
            appearance82.BorderColor = System.Drawing.Color.Silver;
            appearance82.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid4.DisplayLayout.Override.CellAppearance = appearance82;
            this.uGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance83.BackColor = System.Drawing.SystemColors.Control;
            appearance83.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance83.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance83.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance83;
            appearance84.TextHAlignAsString = "Left";
            this.uGrid4.DisplayLayout.Override.HeaderAppearance = appearance84;
            this.uGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.BorderColor = System.Drawing.Color.Silver;
            this.uGrid4.DisplayLayout.Override.RowAppearance = appearance85;
            this.uGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance86.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance86;
            this.uGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid4.Location = new System.Drawing.Point(8, 8);
            this.uGrid4.Name = "uGrid4";
            this.uGrid4.Size = new System.Drawing.Size(12, 12);
            this.uGrid4.TabIndex = 37;
            this.uGrid4.Text = "ultraGrid2";
            this.uGrid4.Visible = false;
            this.uGrid4.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid4_ClickCellButton);
            // 
            // uTextReturnReason
            // 
            appearance48.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnReason.Appearance = appearance48;
            this.uTextReturnReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextReturnReason.Location = new System.Drawing.Point(1052, 144);
            this.uTextReturnReason.Name = "uTextReturnReason";
            this.uTextReturnReason.ReadOnly = true;
            this.uTextReturnReason.Size = new System.Drawing.Size(8, 21);
            this.uTextReturnReason.TabIndex = 97;
            this.uTextReturnReason.Visible = false;
            // 
            // uLabelReturnReason
            // 
            this.uLabelReturnReason.Location = new System.Drawing.Point(1040, 144);
            this.uLabelReturnReason.Name = "uLabelReturnReason";
            this.uLabelReturnReason.Size = new System.Drawing.Size(12, 20);
            this.uLabelReturnReason.TabIndex = 96;
            this.uLabelReturnReason.Text = "ultraLabel1";
            this.uLabelReturnReason.Visible = false;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Controls.Add(this.uGrid3);
            this.uGroupBox3.Controls.Add(this.uButtonDelDept);
            this.uGroupBox3.Location = new System.Drawing.Point(536, 256);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(520, 170);
            this.uGroupBox3.TabIndex = 95;
            // 
            // uGrid3
            // 
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid3.DisplayLayout.Appearance = appearance87;
            this.uGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance88.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance88.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance88.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.GroupByBox.Appearance = appearance88;
            appearance89.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance89;
            this.uGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance90.BackColor2 = System.Drawing.SystemColors.Control;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance90;
            this.uGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance91;
            appearance92.BackColor = System.Drawing.SystemColors.Highlight;
            appearance92.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance92;
            this.uGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.CardAreaAppearance = appearance93;
            appearance94.BorderColor = System.Drawing.Color.Silver;
            appearance94.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid3.DisplayLayout.Override.CellAppearance = appearance94;
            this.uGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance95.BackColor = System.Drawing.SystemColors.Control;
            appearance95.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance95.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance95;
            appearance96.TextHAlignAsString = "Left";
            this.uGrid3.DisplayLayout.Override.HeaderAppearance = appearance96;
            this.uGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.Color.Silver;
            this.uGrid3.DisplayLayout.Override.RowAppearance = appearance97;
            this.uGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance98;
            this.uGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid3.Location = new System.Drawing.Point(12, 61);
            this.uGrid3.Name = "uGrid3";
            this.uGrid3.Size = new System.Drawing.Size(496, 98);
            this.uGrid3.TabIndex = 37;
            this.uGrid3.Text = "ultraGrid2";
            // 
            // uButtonDelDept
            // 
            this.uButtonDelDept.Location = new System.Drawing.Point(12, 28);
            this.uButtonDelDept.Name = "uButtonDelDept";
            this.uButtonDelDept.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelDept.TabIndex = 106;
            this.uButtonDelDept.Text = "ultraButton1";
            this.uButtonDelDept.Click += new System.EventHandler(this.uButtonDelDept_Click);
            // 
            // uTextApproveDate
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveDate.Appearance = appearance4;
            this.uTextApproveDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveDate.Location = new System.Drawing.Point(1044, 88);
            this.uTextApproveDate.Name = "uTextApproveDate";
            this.uTextApproveDate.ReadOnly = true;
            this.uTextApproveDate.Size = new System.Drawing.Size(11, 21);
            this.uTextApproveDate.TabIndex = 94;
            this.uTextApproveDate.Visible = false;
            // 
            // uTextSmall
            // 
            appearance60.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSmall.Appearance = appearance60;
            this.uTextSmall.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSmall.Location = new System.Drawing.Point(1044, 40);
            this.uTextSmall.Name = "uTextSmall";
            this.uTextSmall.ReadOnly = true;
            this.uTextSmall.Size = new System.Drawing.Size(12, 21);
            this.uTextSmall.TabIndex = 93;
            this.uTextSmall.Visible = false;
            // 
            // uTextMiddle
            // 
            appearance59.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMiddle.Appearance = appearance59;
            this.uTextMiddle.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMiddle.Location = new System.Drawing.Point(1048, 164);
            this.uTextMiddle.Name = "uTextMiddle";
            this.uTextMiddle.ReadOnly = true;
            this.uTextMiddle.Size = new System.Drawing.Size(12, 21);
            this.uTextMiddle.TabIndex = 89;
            this.uTextMiddle.Visible = false;
            // 
            // uTextLarge
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLarge.Appearance = appearance9;
            this.uTextLarge.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLarge.Location = new System.Drawing.Point(1048, 184);
            this.uTextLarge.Name = "uTextLarge";
            this.uTextLarge.ReadOnly = true;
            this.uTextLarge.Size = new System.Drawing.Size(12, 21);
            this.uTextLarge.TabIndex = 88;
            this.uTextLarge.Visible = false;
            // 
            // uTextProcess
            // 
            appearance57.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Appearance = appearance57;
            this.uTextProcess.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProcess.Location = new System.Drawing.Point(1032, 12);
            this.uTextProcess.Name = "uTextProcess";
            this.uTextProcess.ReadOnly = true;
            this.uTextProcess.Size = new System.Drawing.Size(30, 21);
            this.uTextProcess.TabIndex = 87;
            this.uTextProcess.Visible = false;
            // 
            // uTextPlant
            // 
            appearance61.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance61;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(1040, 116);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(8, 21);
            this.uTextPlant.TabIndex = 86;
            this.uTextPlant.Visible = false;
            // 
            // uLabelApproveDate
            // 
            this.uLabelApproveDate.Location = new System.Drawing.Point(1035, 88);
            this.uLabelApproveDate.Name = "uLabelApproveDate";
            this.uLabelApproveDate.Size = new System.Drawing.Size(11, 20);
            this.uLabelApproveDate.TabIndex = 74;
            this.uLabelApproveDate.Text = "ultraLabel1";
            this.uLabelApproveDate.Visible = false;
            // 
            // uGroupBox2
            // 
            this.uGroupBox2.Controls.Add(this.uGrid2);
            this.uGroupBox2.Controls.Add(this.uButtonDelVendor);
            this.uGroupBox2.Location = new System.Drawing.Point(12, 432);
            this.uGroupBox2.Name = "uGroupBox2";
            this.uGroupBox2.Size = new System.Drawing.Size(1044, 180);
            this.uGroupBox2.TabIndex = 72;
            // 
            // uGrid2
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance2;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance47;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance52;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance54.BackColor2 = System.Drawing.SystemColors.Control;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance54;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            appearance67.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance67;
            appearance68.BackColor = System.Drawing.SystemColors.Highlight;
            appearance68.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance68;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance69;
            appearance70.BorderColor = System.Drawing.Color.Silver;
            appearance70.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance70;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance71.BackColor = System.Drawing.SystemColors.Control;
            appearance71.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance71.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance71.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance71;
            appearance72.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance72;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance73;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance74.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance74;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 61);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1020, 98);
            this.uGrid2.TabIndex = 37;
            this.uGrid2.Text = "ultraGrid2";
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            this.uGrid2.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_CellListSelect);
            this.uGrid2.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_ClickCellButton);
            // 
            // uButtonDelVendor
            // 
            this.uButtonDelVendor.Location = new System.Drawing.Point(12, 28);
            this.uButtonDelVendor.Name = "uButtonDelVendor";
            this.uButtonDelVendor.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelVendor.TabIndex = 105;
            this.uButtonDelVendor.Text = "ultraButton1";
            this.uButtonDelVendor.Click += new System.EventHandler(this.uButtonDelVendor_Click);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uButtonDelAgreeUser);
            this.uGroupBox1.Controls.Add(this.uButtonDown);
            this.uGroupBox1.Controls.Add(this.uButtonDelFile);
            this.uGroupBox1.Controls.Add(this.uGrid1);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 256);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(520, 170);
            this.uGroupBox1.TabIndex = 71;
            // 
            // uButtonDelAgreeUser
            // 
            this.uButtonDelAgreeUser.Location = new System.Drawing.Point(244, 28);
            this.uButtonDelAgreeUser.Name = "uButtonDelAgreeUser";
            this.uButtonDelAgreeUser.Size = new System.Drawing.Size(12, 28);
            this.uButtonDelAgreeUser.TabIndex = 106;
            this.uButtonDelAgreeUser.Text = "ultraButton1";
            this.uButtonDelAgreeUser.Visible = false;
            // 
            // uButtonDown
            // 
            this.uButtonDown.Location = new System.Drawing.Point(420, 28);
            this.uButtonDown.Name = "uButtonDown";
            this.uButtonDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonDown.TabIndex = 105;
            this.uButtonDown.Text = "ultraButton1";
            this.uButtonDown.Click += new System.EventHandler(this.uButtonDown_Click);
            // 
            // uButtonDelFile
            // 
            this.uButtonDelFile.Location = new System.Drawing.Point(12, 28);
            this.uButtonDelFile.Name = "uButtonDelFile";
            this.uButtonDelFile.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelFile.TabIndex = 104;
            this.uButtonDelFile.Text = "ultraButton1";
            this.uButtonDelFile.Click += new System.EventHandler(this.uButtonDelFile_Click);
            // 
            // uGrid1
            // 
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            appearance20.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance20;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance21;
            appearance22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance22;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance23.BackColor2 = System.Drawing.SystemColors.Control;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance23;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance25.BackColor = System.Drawing.SystemColors.Highlight;
            appearance25.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance25;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance26;
            appearance27.BorderColor = System.Drawing.Color.Silver;
            appearance27.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance27;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance28.BackColor = System.Drawing.SystemColors.Control;
            appearance28.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance28.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance28;
            appearance29.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance29;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance30;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance31.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance31;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(12, 61);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(496, 98);
            this.uGrid1.TabIndex = 35;
            this.uGrid1.Text = "ultraGrid1";
            this.uGrid1.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_ClickCellButton);
            this.uGrid1.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_CellChange);
            this.uGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uTextEtc
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtc.Appearance = appearance34;
            this.uTextEtc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtc.Location = new System.Drawing.Point(532, 228);
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.ReadOnly = true;
            this.uTextEtc.Size = new System.Drawing.Size(516, 21);
            this.uTextEtc.TabIndex = 70;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(376, 228);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(150, 20);
            this.uLabelEtc.TabIndex = 69;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uDateCompleteDate
            // 
            this.uDateCompleteDate.Location = new System.Drawing.Point(1032, 64);
            this.uDateCompleteDate.Name = "uDateCompleteDate";
            this.uDateCompleteDate.Size = new System.Drawing.Size(28, 21);
            this.uDateCompleteDate.TabIndex = 68;
            this.uDateCompleteDate.Visible = false;
            // 
            // ultraLabel3
            // 
            this.ultraLabel3.Location = new System.Drawing.Point(272, 208);
            this.ultraLabel3.Name = "ultraLabel3";
            this.ultraLabel3.Size = new System.Drawing.Size(16, 8);
            this.ultraLabel3.TabIndex = 66;
            this.ultraLabel3.Text = "~";
            // 
            // uLabelApplyDate
            // 
            this.uLabelApplyDate.Location = new System.Drawing.Point(12, 204);
            this.uLabelApplyDate.Name = "uLabelApplyDate";
            this.uLabelApplyDate.Size = new System.Drawing.Size(150, 20);
            this.uLabelApplyDate.TabIndex = 63;
            this.uLabelApplyDate.Text = "ultraLabel1";
            // 
            // uTextDocKeyword
            // 
            appearance56.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocKeyword.Appearance = appearance56;
            this.uTextDocKeyword.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocKeyword.Location = new System.Drawing.Point(168, 108);
            this.uTextDocKeyword.Name = "uTextDocKeyword";
            this.uTextDocKeyword.ReadOnly = true;
            this.uTextDocKeyword.Size = new System.Drawing.Size(200, 21);
            this.uTextDocKeyword.TabIndex = 62;
            // 
            // uLabelDocKeyword
            // 
            this.uLabelDocKeyword.Location = new System.Drawing.Point(12, 108);
            this.uLabelDocKeyword.Name = "uLabelDocKeyword";
            this.uLabelDocKeyword.Size = new System.Drawing.Size(150, 20);
            this.uLabelDocKeyword.TabIndex = 61;
            this.uLabelDocKeyword.Text = "ultraLabel1";
            // 
            // uTextTitle
            // 
            appearance1.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTitle.Appearance = appearance1;
            this.uTextTitle.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTitle.Location = new System.Drawing.Point(532, 12);
            this.uTextTitle.Name = "uTextTitle";
            this.uTextTitle.ReadOnly = true;
            this.uTextTitle.Size = new System.Drawing.Size(464, 21);
            this.uTextTitle.TabIndex = 60;
            // 
            // uLabelTitle
            // 
            this.uLabelTitle.Location = new System.Drawing.Point(376, 12);
            this.uLabelTitle.Name = "uLabelTitle";
            this.uLabelTitle.Size = new System.Drawing.Size(150, 20);
            this.uLabelTitle.TabIndex = 59;
            this.uLabelTitle.Text = "ultraLabel1";
            // 
            // uTextDocState
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Appearance = appearance16;
            this.uTextDocState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDocState.Location = new System.Drawing.Point(168, 84);
            this.uTextDocState.Name = "uTextDocState";
            this.uTextDocState.ReadOnly = true;
            this.uTextDocState.Size = new System.Drawing.Size(200, 21);
            this.uTextDocState.TabIndex = 58;
            // 
            // uLabelDocState
            // 
            this.uLabelDocState.Location = new System.Drawing.Point(12, 84);
            this.uLabelDocState.Name = "uLabelDocState";
            this.uLabelDocState.Size = new System.Drawing.Size(150, 20);
            this.uLabelDocState.TabIndex = 57;
            this.uLabelDocState.Text = "ultraLabel1";
            // 
            // uTextApproveUserName
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Appearance = appearance6;
            this.uTextApproveUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextApproveUserName.Location = new System.Drawing.Point(1048, 0);
            this.uTextApproveUserName.Name = "uTextApproveUserName";
            this.uTextApproveUserName.ReadOnly = true;
            this.uTextApproveUserName.Size = new System.Drawing.Size(11, 21);
            this.uTextApproveUserName.TabIndex = 56;
            this.uTextApproveUserName.Visible = false;
            // 
            // uLabelApproveUser
            // 
            this.uLabelApproveUser.Location = new System.Drawing.Point(1028, 0);
            this.uLabelApproveUser.Name = "uLabelApproveUser";
            this.uLabelApproveUser.Size = new System.Drawing.Size(11, 20);
            this.uLabelApproveUser.TabIndex = 54;
            this.uLabelApproveUser.Text = "ultraLabel1";
            this.uLabelApproveUser.Visible = false;
            // 
            // uTextTo
            // 
            appearance99.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTo.Appearance = appearance99;
            this.uTextTo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTo.Location = new System.Drawing.Point(532, 180);
            this.uTextTo.Multiline = true;
            this.uTextTo.Name = "uTextTo";
            this.uTextTo.ReadOnly = true;
            this.uTextTo.Size = new System.Drawing.Size(516, 45);
            this.uTextTo.TabIndex = 53;
            // 
            // uLabelTo
            // 
            this.uLabelTo.Location = new System.Drawing.Point(376, 180);
            this.uLabelTo.Name = "uLabelTo";
            this.uLabelTo.Size = new System.Drawing.Size(150, 20);
            this.uLabelTo.TabIndex = 52;
            this.uLabelTo.Text = "ultraLabel1";
            // 
            // uTextFrom
            // 
            appearance49.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFrom.Appearance = appearance49;
            this.uTextFrom.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFrom.Location = new System.Drawing.Point(532, 132);
            this.uTextFrom.Multiline = true;
            this.uTextFrom.Name = "uTextFrom";
            this.uTextFrom.ReadOnly = true;
            this.uTextFrom.Size = new System.Drawing.Size(516, 45);
            this.uTextFrom.TabIndex = 51;
            // 
            // uLabelFrom
            // 
            this.uLabelFrom.Location = new System.Drawing.Point(376, 132);
            this.uLabelFrom.Name = "uLabelFrom";
            this.uLabelFrom.Size = new System.Drawing.Size(150, 20);
            this.uLabelFrom.TabIndex = 50;
            this.uLabelFrom.Text = "ultraLabel1";
            // 
            // uTextChangeItem
            // 
            appearance50.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeItem.Appearance = appearance50;
            this.uTextChangeItem.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChangeItem.Location = new System.Drawing.Point(532, 108);
            this.uTextChangeItem.Name = "uTextChangeItem";
            this.uTextChangeItem.ReadOnly = true;
            this.uTextChangeItem.Size = new System.Drawing.Size(516, 21);
            this.uTextChangeItem.TabIndex = 49;
            // 
            // uLabelChangeItem
            // 
            this.uLabelChangeItem.Location = new System.Drawing.Point(376, 108);
            this.uLabelChangeItem.Name = "uLabelChangeItem";
            this.uLabelChangeItem.Size = new System.Drawing.Size(150, 20);
            this.uLabelChangeItem.TabIndex = 48;
            this.uLabelChangeItem.Text = "ultraLabel1";
            // 
            // uTextRev_DisReason
            // 
            appearance51.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRev_DisReason.Appearance = appearance51;
            this.uTextRev_DisReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRev_DisReason.Location = new System.Drawing.Point(532, 84);
            this.uTextRev_DisReason.Name = "uTextRev_DisReason";
            this.uTextRev_DisReason.ReadOnly = true;
            this.uTextRev_DisReason.Size = new System.Drawing.Size(516, 21);
            this.uTextRev_DisReason.TabIndex = 47;
            // 
            // uLabelRev_DisReason
            // 
            this.uLabelRev_DisReason.Location = new System.Drawing.Point(376, 84);
            this.uLabelRev_DisReason.Name = "uLabelRev_DisReason";
            this.uLabelRev_DisReason.Size = new System.Drawing.Size(150, 20);
            this.uLabelRev_DisReason.TabIndex = 46;
            this.uLabelRev_DisReason.Text = "ultraLabel1";
            // 
            // uLabelRev_DisDate
            // 
            this.uLabelRev_DisDate.Location = new System.Drawing.Point(1036, 236);
            this.uLabelRev_DisDate.Name = "uLabelRev_DisDate";
            this.uLabelRev_DisDate.Size = new System.Drawing.Size(12, 20);
            this.uLabelRev_DisDate.TabIndex = 44;
            this.uLabelRev_DisDate.Text = "ultraLabel1";
            this.uLabelRev_DisDate.Visible = false;
            // 
            // uTextRev_DisUserName
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRev_DisUserName.Appearance = appearance32;
            this.uTextRev_DisUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRev_DisUserName.Location = new System.Drawing.Point(633, 60);
            this.uTextRev_DisUserName.Name = "uTextRev_DisUserName";
            this.uTextRev_DisUserName.ReadOnly = true;
            this.uTextRev_DisUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextRev_DisUserName.TabIndex = 43;
            // 
            // uLabelRev_DisUser
            // 
            this.uLabelRev_DisUser.Location = new System.Drawing.Point(1008, 0);
            this.uLabelRev_DisUser.Name = "uLabelRev_DisUser";
            this.uLabelRev_DisUser.Size = new System.Drawing.Size(12, 20);
            this.uLabelRev_DisUser.TabIndex = 41;
            this.uLabelRev_DisUser.Text = "ultraLabel1";
            this.uLabelRev_DisUser.Visible = false;
            // 
            // uLabelRev_DisSeparation
            // 
            this.uLabelRev_DisSeparation.Location = new System.Drawing.Point(12, 60);
            this.uLabelRev_DisSeparation.Name = "uLabelRev_DisSeparation";
            this.uLabelRev_DisSeparation.Size = new System.Drawing.Size(150, 20);
            this.uLabelRev_DisSeparation.TabIndex = 39;
            this.uLabelRev_DisSeparation.Text = "ultraLabel1";
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(1012, 236);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(12, 20);
            this.uLabelCreateDate.TabIndex = 36;
            this.uLabelCreateDate.Text = "ultraLabel1";
            this.uLabelCreateDate.Visible = false;
            // 
            // uTextCreateUserName
            // 
            appearance55.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Appearance = appearance55;
            this.uTextCreateUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Location = new System.Drawing.Point(633, 60);
            this.uTextCreateUserName.Name = "uTextCreateUserName";
            this.uTextCreateUserName.ReadOnly = true;
            this.uTextCreateUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserName.TabIndex = 35;
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(376, 60);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(150, 20);
            this.uLabelCreateUser.TabIndex = 33;
            this.uLabelCreateUser.Text = "ultraLabel1";
            // 
            // uLabelSmall
            // 
            this.uLabelSmall.Location = new System.Drawing.Point(1032, 40);
            this.uLabelSmall.Name = "uLabelSmall";
            this.uLabelSmall.Size = new System.Drawing.Size(12, 20);
            this.uLabelSmall.TabIndex = 31;
            this.uLabelSmall.Text = "ultraLabel1";
            this.uLabelSmall.Visible = false;
            // 
            // uLabelMiddle
            // 
            this.uLabelMiddle.Location = new System.Drawing.Point(1036, 164);
            this.uLabelMiddle.Name = "uLabelMiddle";
            this.uLabelMiddle.Size = new System.Drawing.Size(12, 20);
            this.uLabelMiddle.TabIndex = 29;
            this.uLabelMiddle.Text = "ultraLabel1";
            this.uLabelMiddle.Visible = false;
            // 
            // uLabelLarge
            // 
            this.uLabelLarge.Location = new System.Drawing.Point(1036, 184);
            this.uLabelLarge.Name = "uLabelLarge";
            this.uLabelLarge.Size = new System.Drawing.Size(12, 20);
            this.uLabelLarge.TabIndex = 27;
            this.uLabelLarge.Text = "ultraLabel1";
            this.uLabelLarge.Visible = false;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(1048, 116);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(9, 20);
            this.uLabelPlant.TabIndex = 23;
            this.uLabelPlant.Text = "ultraLabel1";
            this.uLabelPlant.Visible = false;
            // 
            // uTextRevisionNo
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionNo.Appearance = appearance19;
            this.uTextRevisionNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionNo.Location = new System.Drawing.Point(317, 36);
            this.uTextRevisionNo.Name = "uTextRevisionNo";
            this.uTextRevisionNo.ReadOnly = true;
            this.uTextRevisionNo.Size = new System.Drawing.Size(27, 21);
            this.uTextRevisionNo.TabIndex = 22;
            this.uTextRevisionNo.Visible = false;
            // 
            // uTextStandardDocNo
            // 
            appearance64.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardDocNo.Appearance = appearance64;
            this.uTextStandardDocNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardDocNo.Location = new System.Drawing.Point(168, 12);
            this.uTextStandardDocNo.Name = "uTextStandardDocNo";
            this.uTextStandardDocNo.ReadOnly = true;
            this.uTextStandardDocNo.Size = new System.Drawing.Size(200, 21);
            this.uTextStandardDocNo.TabIndex = 20;
            // 
            // uLabelStandardDocNo
            // 
            this.uLabelStandardDocNo.Location = new System.Drawing.Point(12, 12);
            this.uLabelStandardDocNo.Name = "uLabelStandardDocNo";
            this.uLabelStandardDocNo.Size = new System.Drawing.Size(150, 20);
            this.uLabelStandardDocNo.TabIndex = 19;
            this.uLabelStandardDocNo.Text = "ultraLabel1";
            // 
            // frmISO0003D_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmISO0003D_S";
            this.Load += new System.EventHandler(this.frmISO0003D_Load);
            this.Activated += new System.EventHandler(this.frmISO0003D_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmISO0003D_FormClosing);
            this.Resize += new System.EventHandler(this.frmISO0003D_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDiscard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchRegFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchStandardNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateApplyDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRev_DisDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextGRWComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakeUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPadSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChipSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboRev_DisSeparation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox4)).EndInit();
            this.uGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextReturnReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox2)).EndInit();
            this.uGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCompleteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocKeyword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextApproveUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChangeItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRev_DisUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardDocNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchRegToDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchRegFromDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchRegDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchTitle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStandardNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchSmall;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMiddle;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLarge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchLarge;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDiscard;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDiscard;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelApproveDate;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCompleteDate;
        private Infragistics.Win.Misc.UltraLabel ultraLabel3;
        private Infragistics.Win.Misc.UltraLabel uLabelApplyDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocKeyword;
        private Infragistics.Win.Misc.UltraLabel uLabelDocKeyword;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelTitle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocState;
        private Infragistics.Win.Misc.UltraLabel uLabelDocState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelApproveUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTo;
        private Infragistics.Win.Misc.UltraLabel uLabelTo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFrom;
        private Infragistics.Win.Misc.UltraLabel uLabelFrom;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChangeItem;
        private Infragistics.Win.Misc.UltraLabel uLabelChangeItem;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRev_DisReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRev_DisUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisUser;
        private Infragistics.Win.Misc.UltraLabel uLabelRev_DisSeparation;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.Misc.UltraLabel uLabelSmall;
        private Infragistics.Win.Misc.UltraLabel uLabelMiddle;
        private Infragistics.Win.Misc.UltraLabel uLabelLarge;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardDocNo;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardDocNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMiddle;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLarge;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSmall;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox4;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextReturnReason;
        private Infragistics.Win.Misc.UltraLabel uLabelReturnReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextApproveUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRev_DisUserID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboRev_DisSeparation;
        private Infragistics.Win.Misc.UltraButton uButtonDelVendor;
        private Infragistics.Win.Misc.UltraButton uButtonDelFile;
        private Infragistics.Win.Misc.UltraButton uButtonDelDept;
        private Infragistics.Win.Misc.UltraButton uButtonDelAgreeUser;
        private Infragistics.Win.Misc.UltraButton uButtonDown;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPadSize;
        private Infragistics.Win.Misc.UltraLabel uLabelPadSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChipSize;
        private Infragistics.Win.Misc.UltraLabel uLabelChipSize;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeDeptName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakeUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelMake;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckVersion;
        private Infragistics.Win.Misc.UltraLabel uLabelVersionCheck;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcess;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitVersionNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextGRWComment;
        private Infragistics.Win.Misc.UltraLabel uLabelGRWComment;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRev_DisDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateApplyDateTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateApplyDateFrom;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCreateDate;
    }
}