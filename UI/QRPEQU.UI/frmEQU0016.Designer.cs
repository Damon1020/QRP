﻿namespace QRPEQU.UI
{
    partial class frmEQU0016
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0016));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextDocCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboMaterial = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDocCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateTimeTo = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateTimeFrom = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxCancel = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextChgName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextChgID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCancelChgID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCancelDate = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCancel = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uGridCancel = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCancel)).BeginInit();
            this.uGroupBoxCancel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCancel)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextDocCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDocCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterial);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDate);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateTimeTo);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateTimeFrom);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabel1);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextDocCode
            // 
            this.uTextDocCode.Location = new System.Drawing.Point(944, 12);
            this.uTextDocCode.MaxLength = 20;
            this.uTextDocCode.Name = "uTextDocCode";
            this.uTextDocCode.Size = new System.Drawing.Size(104, 21);
            this.uTextDocCode.TabIndex = 5;
            // 
            // uComboMaterial
            // 
            this.uComboMaterial.Location = new System.Drawing.Point(692, 12);
            this.uComboMaterial.MaxLength = 50;
            this.uComboMaterial.Name = "uComboMaterial";
            this.uComboMaterial.Size = new System.Drawing.Size(140, 21);
            this.uComboMaterial.TabIndex = 4;
            this.uComboMaterial.ValueChanged += new System.EventHandler(this.uComboMaterial_ValueChanged);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(440, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchPlant.TabIndex = 4;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchDocCode
            // 
            this.uLabelSearchDocCode.Location = new System.Drawing.Point(840, 12);
            this.uLabelSearchDocCode.Name = "uLabelSearchDocCode";
            this.uLabelSearchDocCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDocCode.TabIndex = 3;
            // 
            // uLabelSearchMaterial
            // 
            this.uLabelSearchMaterial.Location = new System.Drawing.Point(580, 12);
            this.uLabelSearchMaterial.Name = "uLabelSearchMaterial";
            this.uLabelSearchMaterial.Size = new System.Drawing.Size(108, 20);
            this.uLabelSearchMaterial.TabIndex = 3;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(336, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 3;
            // 
            // uLabelSearchDate
            // 
            this.uLabelSearchDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDate.Name = "uLabelSearchDate";
            this.uLabelSearchDate.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDate.TabIndex = 2;
            // 
            // uDateTimeTo
            // 
            appearance19.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeTo.Appearance = appearance19;
            this.uDateTimeTo.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeTo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTimeTo.Location = new System.Drawing.Point(228, 12);
            this.uDateTimeTo.Name = "uDateTimeTo";
            this.uDateTimeTo.Size = new System.Drawing.Size(100, 21);
            this.uDateTimeTo.TabIndex = 1;
            // 
            // uDateTimeFrom
            // 
            appearance18.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeFrom.Appearance = appearance18;
            this.uDateTimeFrom.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateTimeFrom.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateTimeFrom.Location = new System.Drawing.Point(116, 12);
            this.uDateTimeFrom.Name = "uDateTimeFrom";
            this.uDateTimeFrom.Size = new System.Drawing.Size(100, 21);
            this.uDateTimeFrom.TabIndex = 1;
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(216, 20);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(12, 8);
            this.uLabel1.TabIndex = 0;
            this.uLabel1.Text = "~";
            // 
            // uGroupBoxCancel
            // 
            this.uGroupBoxCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxCancel.Controls.Add(this.uTextChgName);
            this.uGroupBoxCancel.Controls.Add(this.uTextChgID);
            this.uGroupBoxCancel.Controls.Add(this.uLabelCancelChgID);
            this.uGroupBoxCancel.Controls.Add(this.uLabelCancelDate);
            this.uGroupBoxCancel.Controls.Add(this.uDateCancel);
            this.uGroupBoxCancel.Location = new System.Drawing.Point(0, 80);
            this.uGroupBoxCancel.Name = "uGroupBoxCancel";
            this.uGroupBoxCancel.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxCancel.TabIndex = 2;
            // 
            // uTextChgName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChgName.Appearance = appearance2;
            this.uTextChgName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextChgName.Location = new System.Drawing.Point(560, 12);
            this.uTextChgName.MaxLength = 50;
            this.uTextChgName.Name = "uTextChgName";
            this.uTextChgName.ReadOnly = true;
            this.uTextChgName.Size = new System.Drawing.Size(100, 21);
            this.uTextChgName.TabIndex = 5;
            // 
            // uTextChgID
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextChgID.Appearance = appearance3;
            this.uTextChgID.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextChgID.ButtonsRight.Add(editorButton1);
            this.uTextChgID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextChgID.Location = new System.Drawing.Point(456, 12);
            this.uTextChgID.MaxLength = 20;
            this.uTextChgID.Name = "uTextChgID";
            this.uTextChgID.Size = new System.Drawing.Size(100, 21);
            this.uTextChgID.TabIndex = 5;
            this.uTextChgID.ValueChanged += new System.EventHandler(this.uTextChgID_ValueChanged);
            this.uTextChgID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextChgID_KeyDown);
            this.uTextChgID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextChgID_EditorButtonClick);
            // 
            // uLabelCancelChgID
            // 
            this.uLabelCancelChgID.Location = new System.Drawing.Point(336, 12);
            this.uLabelCancelChgID.Name = "uLabelCancelChgID";
            this.uLabelCancelChgID.Size = new System.Drawing.Size(116, 20);
            this.uLabelCancelChgID.TabIndex = 2;
            // 
            // uLabelCancelDate
            // 
            this.uLabelCancelDate.Location = new System.Drawing.Point(12, 12);
            this.uLabelCancelDate.Name = "uLabelCancelDate";
            this.uLabelCancelDate.Size = new System.Drawing.Size(108, 20);
            this.uLabelCancelDate.TabIndex = 2;
            // 
            // uDateCancel
            // 
            appearance17.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCancel.Appearance = appearance17;
            this.uDateCancel.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCancel.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCancel.Location = new System.Drawing.Point(124, 12);
            this.uDateCancel.Name = "uDateCancel";
            this.uDateCancel.Size = new System.Drawing.Size(100, 21);
            this.uDateCancel.TabIndex = 1;
            // 
            // uGridCancel
            // 
            this.uGridCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCancel.DisplayLayout.Appearance = appearance8;
            this.uGridCancel.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCancel.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCancel.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCancel.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.uGridCancel.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCancel.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.uGridCancel.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCancel.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCancel.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCancel.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridCancel.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCancel.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCancel.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCancel.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridCancel.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCancel.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCancel.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance15.TextHAlignAsString = "Left";
            this.uGridCancel.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridCancel.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCancel.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.uGridCancel.DisplayLayout.Override.RowAppearance = appearance14;
            this.uGridCancel.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCancel.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.uGridCancel.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCancel.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCancel.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCancel.Location = new System.Drawing.Point(0, 120);
            this.uGridCancel.Name = "uGridCancel";
            this.uGridCancel.Size = new System.Drawing.Size(1070, 700);
            this.uGridCancel.TabIndex = 3;
            this.uGridCancel.Text = "ultraGrid1";
            // 
            // frmEQU0016
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridCancel);
            this.Controls.Add(this.uGroupBoxCancel);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0016";
            this.Load += new System.EventHandler(this.frmEQU0016_Load);
            this.Activated += new System.EventHandler(this.frmEQU0016_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0016_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateTimeFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxCancel)).EndInit();
            this.uGroupBoxCancel.ResumeLayout(false);
            this.uGroupBoxCancel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextChgID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCancel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeTo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateTimeFrom;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMaterial;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDocCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterial;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxCancel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChgName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextChgID;
        private Infragistics.Win.Misc.UltraLabel uLabelCancelChgID;
        private Infragistics.Win.Misc.UltraLabel uLabelCancelDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCancel;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCancel;
    }
}