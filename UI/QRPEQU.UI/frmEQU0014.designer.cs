﻿namespace QRPEQU.UI
{
    partial class frmEQU0014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQU0014));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboItemGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelItemGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDocCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDocCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateToDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateFromDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelSearchDay = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboInventory = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInventory = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCarryInName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCarryInID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelConfirmID = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCarryInConfirm = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCarryInConfirm = new Infragistics.Win.Misc.UltraLabel();
            this.uGridCarryInConfirm = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboItemGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryInName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryInID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCarryInConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryInConfirm)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboItemGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelItemGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextDocCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDocCode);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateToDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateFromDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDay);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 3;
            // 
            // uComboItemGubun
            // 
            this.uComboItemGubun.Location = new System.Drawing.Point(704, 12);
            this.uComboItemGubun.MaxLength = 50;
            this.uComboItemGubun.Name = "uComboItemGubun";
            this.uComboItemGubun.Size = new System.Drawing.Size(120, 21);
            this.uComboItemGubun.TabIndex = 9;
            this.uComboItemGubun.Text = "ultraComboEditor1";
            this.uComboItemGubun.ValueChanged += new System.EventHandler(this.uComboItemGubun_ValueChanged);
            // 
            // uLabelItemGubun
            // 
            this.uLabelItemGubun.Location = new System.Drawing.Point(600, 12);
            this.uLabelItemGubun.Name = "uLabelItemGubun";
            this.uLabelItemGubun.Size = new System.Drawing.Size(100, 20);
            this.uLabelItemGubun.TabIndex = 10;
            this.uLabelItemGubun.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(456, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(352, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 8;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uTextDocCode
            // 
            this.uTextDocCode.Location = new System.Drawing.Point(940, 12);
            this.uTextDocCode.MaxLength = 20;
            this.uTextDocCode.Name = "uTextDocCode";
            this.uTextDocCode.Size = new System.Drawing.Size(100, 21);
            this.uTextDocCode.TabIndex = 4;
            // 
            // uLabelDocCode
            // 
            this.uLabelDocCode.Location = new System.Drawing.Point(836, 12);
            this.uLabelDocCode.Name = "uLabelDocCode";
            this.uLabelDocCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelDocCode.TabIndex = 6;
            this.uLabelDocCode.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(216, 16);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(15, 15);
            this.ultraLabel1.TabIndex = 3;
            this.ultraLabel1.Text = "~";
            // 
            // uDateToDay
            // 
            this.uDateToDay.Location = new System.Drawing.Point(232, 12);
            this.uDateToDay.Name = "uDateToDay";
            this.uDateToDay.Size = new System.Drawing.Size(100, 21);
            this.uDateToDay.TabIndex = 2;
            // 
            // uDateFromDay
            // 
            this.uDateFromDay.Location = new System.Drawing.Point(116, 12);
            this.uDateFromDay.Name = "uDateFromDay";
            this.uDateFromDay.Size = new System.Drawing.Size(100, 21);
            this.uDateFromDay.TabIndex = 1;
            // 
            // uLabelSearchDay
            // 
            this.uLabelSearchDay.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchDay.Name = "uLabelSearchDay";
            this.uLabelSearchDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDay.TabIndex = 0;
            this.uLabelSearchDay.Text = "ultraLabel1";
            // 
            // uGroupBox
            // 
            this.uGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox.Controls.Add(this.uComboInventory);
            this.uGroupBox.Controls.Add(this.uLabelInventory);
            this.uGroupBox.Controls.Add(this.uTextCarryInName);
            this.uGroupBox.Controls.Add(this.uTextCarryInID);
            this.uGroupBox.Controls.Add(this.uLabelConfirmID);
            this.uGroupBox.Controls.Add(this.uDateCarryInConfirm);
            this.uGroupBox.Controls.Add(this.uLabelCarryInConfirm);
            this.uGroupBox.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBox.TabIndex = 4;
            // 
            // uComboInventory
            // 
            this.uComboInventory.Location = new System.Drawing.Point(704, 10);
            this.uComboInventory.MaxLength = 50;
            this.uComboInventory.Name = "uComboInventory";
            this.uComboInventory.Size = new System.Drawing.Size(120, 21);
            this.uComboInventory.TabIndex = 13;
            // 
            // uLabelInventory
            // 
            this.uLabelInventory.Location = new System.Drawing.Point(600, 10);
            this.uLabelInventory.Name = "uLabelInventory";
            this.uLabelInventory.Size = new System.Drawing.Size(100, 20);
            this.uLabelInventory.TabIndex = 14;
            this.uLabelInventory.Text = "ultraLabel1";
            // 
            // uTextCarryInName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryInName.Appearance = appearance2;
            this.uTextCarryInName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCarryInName.Location = new System.Drawing.Point(476, 12);
            this.uTextCarryInName.MaxLength = 20;
            this.uTextCarryInName.Name = "uTextCarryInName";
            this.uTextCarryInName.ReadOnly = true;
            this.uTextCarryInName.Size = new System.Drawing.Size(100, 21);
            this.uTextCarryInName.TabIndex = 12;
            // 
            // uTextCarryInID
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCarryInID.Appearance = appearance3;
            this.uTextCarryInID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCarryInID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCarryInID.ButtonsRight.Add(editorButton1);
            this.uTextCarryInID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCarryInID.Location = new System.Drawing.Point(372, 12);
            this.uTextCarryInID.MaxLength = 20;
            this.uTextCarryInID.Name = "uTextCarryInID";
            this.uTextCarryInID.Size = new System.Drawing.Size(100, 19);
            this.uTextCarryInID.TabIndex = 6;
            this.uTextCarryInID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCarryInID_EditorButtonClick);
            this.uTextCarryInID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCarryInID_KeyDown);
            // 
            // uLabelConfirmID
            // 
            this.uLabelConfirmID.Location = new System.Drawing.Point(268, 12);
            this.uLabelConfirmID.Name = "uLabelConfirmID";
            this.uLabelConfirmID.Size = new System.Drawing.Size(100, 20);
            this.uLabelConfirmID.TabIndex = 2;
            this.uLabelConfirmID.Text = "ultraLabel2";
            // 
            // uDateCarryInConfirm
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCarryInConfirm.Appearance = appearance5;
            this.uDateCarryInConfirm.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCarryInConfirm.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uDateCarryInConfirm.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateCarryInConfirm.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCarryInConfirm.Location = new System.Drawing.Point(116, 12);
            this.uDateCarryInConfirm.Name = "uDateCarryInConfirm";
            this.uDateCarryInConfirm.Size = new System.Drawing.Size(100, 19);
            this.uDateCarryInConfirm.TabIndex = 5;
            // 
            // uLabelCarryInConfirm
            // 
            this.uLabelCarryInConfirm.Location = new System.Drawing.Point(12, 12);
            this.uLabelCarryInConfirm.Name = "uLabelCarryInConfirm";
            this.uLabelCarryInConfirm.Size = new System.Drawing.Size(100, 20);
            this.uLabelCarryInConfirm.TabIndex = 0;
            this.uLabelCarryInConfirm.Text = "ultraLabel2";
            // 
            // uGridCarryInConfirm
            // 
            this.uGridCarryInConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCarryInConfirm.DisplayLayout.Appearance = appearance6;
            this.uGridCarryInConfirm.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCarryInConfirm.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance7.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance7.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryInConfirm.DisplayLayout.GroupByBox.Appearance = appearance7;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryInConfirm.DisplayLayout.GroupByBox.BandLabelAppearance = appearance8;
            this.uGridCarryInConfirm.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance9.BackColor2 = System.Drawing.SystemColors.Control;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCarryInConfirm.DisplayLayout.GroupByBox.PromptAppearance = appearance9;
            this.uGridCarryInConfirm.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCarryInConfirm.DisplayLayout.MaxRowScrollRegions = 1;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCarryInConfirm.DisplayLayout.Override.ActiveCellAppearance = appearance10;
            appearance11.BackColor = System.Drawing.SystemColors.Highlight;
            appearance11.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCarryInConfirm.DisplayLayout.Override.ActiveRowAppearance = appearance11;
            this.uGridCarryInConfirm.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCarryInConfirm.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCarryInConfirm.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance13.BorderColor = System.Drawing.Color.Silver;
            appearance13.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCarryInConfirm.DisplayLayout.Override.CellAppearance = appearance13;
            this.uGridCarryInConfirm.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCarryInConfirm.DisplayLayout.Override.CellPadding = 0;
            appearance14.BackColor = System.Drawing.SystemColors.Control;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCarryInConfirm.DisplayLayout.Override.GroupByRowAppearance = appearance14;
            appearance15.TextHAlignAsString = "Left";
            this.uGridCarryInConfirm.DisplayLayout.Override.HeaderAppearance = appearance15;
            this.uGridCarryInConfirm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCarryInConfirm.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            this.uGridCarryInConfirm.DisplayLayout.Override.RowAppearance = appearance16;
            this.uGridCarryInConfirm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCarryInConfirm.DisplayLayout.Override.TemplateAddRowAppearance = appearance17;
            this.uGridCarryInConfirm.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCarryInConfirm.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCarryInConfirm.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCarryInConfirm.Location = new System.Drawing.Point(0, 120);
            this.uGridCarryInConfirm.Name = "uGridCarryInConfirm";
            this.uGridCarryInConfirm.Size = new System.Drawing.Size(1070, 704);
            this.uGridCarryInConfirm.TabIndex = 5;
            this.uGridCarryInConfirm.Text = "ultraGrid1";
            this.uGridCarryInConfirm.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryInConfirm_AfterCellUpdate);
            this.uGridCarryInConfirm.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridCarryInConfirm_CellChange);
            this.uGridCarryInConfirm.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGridCarryInConfirm_ClickCell);
            // 
            // frmEQU0014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridCarryInConfirm);
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQU0014";
            this.Load += new System.EventHandler(this.frmEQU0014_Load);
            this.Activated += new System.EventHandler(this.frmEQU0014_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQU0014_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboItemGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDocCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateToDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateFromDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryInName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCarryInID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCarryInConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCarryInConfirm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateToDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateFromDay;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDay;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryInName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCarryInID;
        private Infragistics.Win.Misc.UltraLabel uLabelConfirmID;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCarryInConfirm;
        private Infragistics.Win.Misc.UltraLabel uLabelCarryInConfirm;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCarryInConfirm;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDocCode;
        private Infragistics.Win.Misc.UltraLabel uLabelDocCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboItemGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelItemGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInventory;
        private Infragistics.Win.Misc.UltraLabel uLabelInventory;
    }
}