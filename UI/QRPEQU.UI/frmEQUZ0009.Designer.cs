﻿namespace QRPEQU.UI
{
    partial class frmEQUZ0009
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEQUZ0009));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateSearchToDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateSearchFromDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextTechnicianName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchProcGubun = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextTechnician = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchProcGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipMentGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchArea = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchYear = new Infragistics.Win.Misc.UltraLabel();
            this.uGridRelease = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridPMResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uGridRepair = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRequestName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRequestID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateRepairReqDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPMPlanDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepairGICode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRelease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).BeginInit();
            this.uGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairReqDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMPlanDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraLabel1);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchToDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uDateSearchFromDay);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnicianName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextTechnician);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcGubun);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUser);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipMentGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchYear);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 56);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(132, 21);
            this.uComboProcessGroup.TabIndex = 27;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uLabelSearchProcessGroup
            // 
            this.uLabelSearchProcessGroup.Location = new System.Drawing.Point(12, 56);
            this.uLabelSearchProcessGroup.Name = "uLabelSearchProcessGroup";
            this.uLabelSearchProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcessGroup.TabIndex = 26;
            this.uLabelSearchProcessGroup.Text = "ultraLabel2";
            // 
            // uComboEquipLargeType
            // 
            this.uComboEquipLargeType.Location = new System.Drawing.Point(448, 56);
            this.uComboEquipLargeType.MaxLength = 50;
            this.uComboEquipLargeType.Name = "uComboEquipLargeType";
            this.uComboEquipLargeType.Size = new System.Drawing.Size(132, 21);
            this.uComboEquipLargeType.TabIndex = 24;
            this.uComboEquipLargeType.ValueChanged += new System.EventHandler(this.uComboEquipLargeType_ValueChanged);
            // 
            // uLabelEquipLargeType
            // 
            this.uLabelEquipLargeType.Location = new System.Drawing.Point(344, 56);
            this.uLabelEquipLargeType.Name = "uLabelEquipLargeType";
            this.uLabelEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLargeType.TabIndex = 25;
            this.uLabelEquipLargeType.Text = "ultraLabel2";
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(732, 32);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchEquipLoc.TabIndex = 22;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(628, 32);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 23;
            this.uLabelSearchEquipLoc.Text = "ultraLabel1";
            // 
            // ultraLabel1
            // 
            appearance48.TextHAlignAsString = "Center";
            this.ultraLabel1.Appearance = appearance48;
            this.ultraLabel1.Location = new System.Drawing.Point(216, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(15, 15);
            this.ultraLabel1.TabIndex = 19;
            this.ultraLabel1.Text = "~";
            // 
            // uDateSearchToDay
            // 
            appearance50.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDay.Appearance = appearance50;
            this.uDateSearchToDay.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchToDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchToDay.Location = new System.Drawing.Point(232, 8);
            this.uDateSearchToDay.Name = "uDateSearchToDay";
            this.uDateSearchToDay.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchToDay.TabIndex = 2;
            // 
            // uDateSearchFromDay
            // 
            appearance49.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDay.Appearance = appearance49;
            this.uDateSearchFromDay.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateSearchFromDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateSearchFromDay.Location = new System.Drawing.Point(116, 8);
            this.uDateSearchFromDay.Name = "uDateSearchFromDay";
            this.uDateSearchFromDay.Size = new System.Drawing.Size(100, 21);
            this.uDateSearchFromDay.TabIndex = 1;
            // 
            // uTextTechnicianName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Appearance = appearance21;
            this.uTextTechnicianName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTechnicianName.Location = new System.Drawing.Point(220, 32);
            this.uTextTechnicianName.Name = "uTextTechnicianName";
            this.uTextTechnicianName.ReadOnly = true;
            this.uTextTechnicianName.Size = new System.Drawing.Size(100, 21);
            this.uTextTechnicianName.TabIndex = 16;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(732, 56);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchEquipGroup.TabIndex = 4;
            this.uComboSearchEquipGroup.Text = "ultraComboEditor2";
            // 
            // uComboSearchProcGubun
            // 
            this.uComboSearchProcGubun.Location = new System.Drawing.Point(1048, 4);
            this.uComboSearchProcGubun.MaxLength = 50;
            this.uComboSearchProcGubun.Name = "uComboSearchProcGubun";
            this.uComboSearchProcGubun.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchProcGubun.TabIndex = 7;
            this.uComboSearchProcGubun.Text = "ultraComboEditor2";
            this.uComboSearchProcGubun.Visible = false;
            this.uComboSearchProcGubun.ValueChanged += new System.EventHandler(this.uComboSearchProcGubun_ValueChanged);
            // 
            // uTextTechnician
            // 
            appearance22.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTechnician.Appearance = appearance22;
            this.uTextTechnician.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextTechnician.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance23.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance23.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance23;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextTechnician.ButtonsRight.Add(editorButton1);
            this.uTextTechnician.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextTechnician.Location = new System.Drawing.Point(116, 32);
            this.uTextTechnician.MaxLength = 20;
            this.uTextTechnician.Name = "uTextTechnician";
            this.uTextTechnician.Size = new System.Drawing.Size(100, 19);
            this.uTextTechnician.TabIndex = 8;
            this.uTextTechnician.ValueChanged += new System.EventHandler(this.uTextTechnician_ValueChanged);
            this.uTextTechnician.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextTechnician_KeyDown);
            this.uTextTechnician.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextTechnician_EditorButtonClick);
            // 
            // uLabelSearchProcGubun
            // 
            this.uLabelSearchProcGubun.Location = new System.Drawing.Point(1024, 4);
            this.uLabelSearchProcGubun.Name = "uLabelSearchProcGubun";
            this.uLabelSearchProcGubun.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchProcGubun.TabIndex = 10;
            this.uLabelSearchProcGubun.Text = "ultraLabel2";
            this.uLabelSearchProcGubun.Visible = false;
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(448, 32);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchStation.TabIndex = 6;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelSearchUser
            // 
            this.uLabelSearchUser.Location = new System.Drawing.Point(12, 32);
            this.uLabelSearchUser.Name = "uLabelSearchUser";
            this.uLabelSearchUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchUser.TabIndex = 14;
            this.uLabelSearchUser.Text = "ultraLabel3";
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(344, 32);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 8;
            this.uLabelSearchStation.Text = "ultraLabel1";
            // 
            // uComboSearchArea
            // 
            this.uComboSearchArea.Location = new System.Drawing.Point(1040, 4);
            this.uComboSearchArea.MaxLength = 50;
            this.uComboSearchArea.Name = "uComboSearchArea";
            this.uComboSearchArea.Size = new System.Drawing.Size(20, 21);
            this.uComboSearchArea.TabIndex = 5;
            this.uComboSearchArea.Text = "ultraComboEditor2";
            this.uComboSearchArea.Visible = false;
            this.uComboSearchArea.ValueChanged += new System.EventHandler(this.uComboSearchArea_ValueChanged);
            // 
            // uLabelSearchEquipMentGroup
            // 
            this.uLabelSearchEquipMentGroup.Location = new System.Drawing.Point(628, 56);
            this.uLabelSearchEquipMentGroup.Name = "uLabelSearchEquipMentGroup";
            this.uLabelSearchEquipMentGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipMentGroup.TabIndex = 12;
            this.uLabelSearchEquipMentGroup.Text = "ultraLabel2";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(448, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchPlant.TabIndex = 3;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchArea
            // 
            this.uLabelSearchArea.Location = new System.Drawing.Point(1032, 4);
            this.uLabelSearchArea.Name = "uLabelSearchArea";
            this.uLabelSearchArea.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchArea.TabIndex = 6;
            this.uLabelSearchArea.Text = "ultraLabel2";
            this.uLabelSearchArea.Visible = false;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(344, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uLabelSearchYear
            // 
            this.uLabelSearchYear.Location = new System.Drawing.Point(12, 8);
            this.uLabelSearchYear.Name = "uLabelSearchYear";
            this.uLabelSearchYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchYear.TabIndex = 2;
            this.uLabelSearchYear.Text = "ultraLabel1";
            // 
            // uGridRelease
            // 
            this.uGridRelease.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRelease.DisplayLayout.Appearance = appearance4;
            this.uGridRelease.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRelease.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRelease.DisplayLayout.GroupByBox.Appearance = appearance6;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRelease.DisplayLayout.GroupByBox.BandLabelAppearance = appearance7;
            this.uGridRelease.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance8.BackColor2 = System.Drawing.SystemColors.Control;
            appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRelease.DisplayLayout.GroupByBox.PromptAppearance = appearance8;
            this.uGridRelease.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRelease.DisplayLayout.MaxRowScrollRegions = 1;
            appearance9.BackColor = System.Drawing.SystemColors.Window;
            appearance9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRelease.DisplayLayout.Override.ActiveCellAppearance = appearance9;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRelease.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this.uGridRelease.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRelease.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRelease.DisplayLayout.Override.CardAreaAppearance = appearance11;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            appearance12.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRelease.DisplayLayout.Override.CellAppearance = appearance12;
            this.uGridRelease.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRelease.DisplayLayout.Override.CellPadding = 0;
            appearance13.BackColor = System.Drawing.SystemColors.Control;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRelease.DisplayLayout.Override.GroupByRowAppearance = appearance13;
            appearance14.TextHAlignAsString = "Left";
            this.uGridRelease.DisplayLayout.Override.HeaderAppearance = appearance14;
            this.uGridRelease.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRelease.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.Color.Silver;
            this.uGridRelease.DisplayLayout.Override.RowAppearance = appearance15;
            this.uGridRelease.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRelease.DisplayLayout.Override.TemplateAddRowAppearance = appearance16;
            this.uGridRelease.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRelease.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRelease.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRelease.Location = new System.Drawing.Point(0, 120);
            this.uGridRelease.Name = "uGridRelease";
            this.uGridRelease.Size = new System.Drawing.Size(1070, 700);
            this.uGridRelease.TabIndex = 7;
            this.uGridRelease.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridRelease_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 8;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox3
            // 
            this.uGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox3.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox3.Controls.Add(this.uGridPMResult);
            this.uGroupBox3.Controls.Add(this.uLabel8);
            this.uGroupBox3.Controls.Add(this.uGridRepair);
            this.uGroupBox3.Controls.Add(this.uLabel7);
            this.uGroupBox3.Location = new System.Drawing.Point(12, 120);
            this.uGroupBox3.Name = "uGroupBox3";
            this.uGroupBox3.Size = new System.Drawing.Size(1040, 520);
            this.uGroupBox3.TabIndex = 10;
            // 
            // uButtonDeleteRow
            // 
            appearance51.Image = global::QRPEQU.UI.Properties.Resources.btn_delTable;
            this.uButtonDeleteRow.Appearance = appearance51;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(124, 24);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 24;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridPMResult
            // 
            this.uGridPMResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPMResult.DisplayLayout.Appearance = appearance24;
            this.uGridPMResult.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPMResult.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResult.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGridPMResult.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPMResult.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.uGridPMResult.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPMResult.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPMResult.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPMResult.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.uGridPMResult.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPMResult.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPMResult.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridPMResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPMResult.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPMResult.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.uGridPMResult.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.uGridPMResult.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPMResult.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridPMResult.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridPMResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPMResult.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridPMResult.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPMResult.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPMResult.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPMResult.Location = new System.Drawing.Point(12, 288);
            this.uGridPMResult.Name = "uGridPMResult";
            this.uGridPMResult.Size = new System.Drawing.Size(1020, 216);
            this.uGridPMResult.TabIndex = 4;
            this.uGridPMResult.Text = "ultraGrid1";
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 288);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(100, 20);
            this.uLabel8.TabIndex = 3;
            this.uLabel8.Text = "ultraLabel2";
            // 
            // uGridRepair
            // 
            this.uGridRepair.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridRepair.DisplayLayout.Appearance = appearance36;
            this.uGridRepair.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridRepair.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepair.DisplayLayout.GroupByBox.Appearance = appearance37;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepair.DisplayLayout.GroupByBox.BandLabelAppearance = appearance38;
            this.uGridRepair.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance39.BackColor2 = System.Drawing.SystemColors.Control;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridRepair.DisplayLayout.GroupByBox.PromptAppearance = appearance39;
            this.uGridRepair.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridRepair.DisplayLayout.MaxRowScrollRegions = 1;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridRepair.DisplayLayout.Override.ActiveCellAppearance = appearance40;
            appearance41.BackColor = System.Drawing.SystemColors.Highlight;
            appearance41.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridRepair.DisplayLayout.Override.ActiveRowAppearance = appearance41;
            this.uGridRepair.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridRepair.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            this.uGridRepair.DisplayLayout.Override.CardAreaAppearance = appearance42;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridRepair.DisplayLayout.Override.CellAppearance = appearance43;
            this.uGridRepair.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridRepair.DisplayLayout.Override.CellPadding = 0;
            appearance44.BackColor = System.Drawing.SystemColors.Control;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridRepair.DisplayLayout.Override.GroupByRowAppearance = appearance44;
            appearance45.TextHAlignAsString = "Left";
            this.uGridRepair.DisplayLayout.Override.HeaderAppearance = appearance45;
            this.uGridRepair.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridRepair.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            this.uGridRepair.DisplayLayout.Override.RowAppearance = appearance46;
            this.uGridRepair.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridRepair.DisplayLayout.Override.TemplateAddRowAppearance = appearance47;
            this.uGridRepair.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridRepair.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridRepair.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridRepair.Location = new System.Drawing.Point(12, 32);
            this.uGridRepair.Name = "uGridRepair";
            this.uGridRepair.Size = new System.Drawing.Size(1020, 220);
            this.uGridRepair.TabIndex = 2;
            this.uGridRepair.Text = "ultraGrid1";
            this.uGridRepair.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridRepair_AfterCellUpdate);
            this.uGridRepair.CellListSelect += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridRepair_CellListSelect);
            this.uGridRepair.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridRepair_BeforeCellListDropDown);
            this.uGridRepair.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridRepair_AfterRowInsert);
            this.uGridRepair.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid_CellChange);
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 28);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(108, 20);
            this.uLabel7.TabIndex = 0;
            this.uLabel7.Text = "ultraLabel2";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox1.Controls.Add(this.uTextRequestName);
            this.uGroupBox1.Controls.Add(this.uTextRequestID);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.uDateRepairReqDate);
            this.uGroupBox1.Controls.Add(this.uLabelEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.uTextPMPlanDate);
            this.uGroupBox1.Controls.Add(this.uLabel4);
            this.uGroupBox1.Controls.Add(this.uTextEquipName);
            this.uGroupBox1.Controls.Add(this.uTextEquipCode);
            this.uGroupBox1.Controls.Add(this.uLabel2);
            this.uGroupBox1.Controls.Add(this.uTextRepairGICode);
            this.uGroupBox1.Controls.Add(this.uLabel1);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1040, 104);
            this.uGroupBox1.TabIndex = 8;
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(124, 76);
            this.uTextEtcDesc.MaxLength = 100;
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(528, 21);
            this.uTextEtcDesc.TabIndex = 11;
            // 
            // uTextRequestName
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRequestName.Appearance = appearance3;
            this.uTextRequestName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRequestName.Location = new System.Drawing.Point(552, 52);
            this.uTextRequestName.Name = "uTextRequestName";
            this.uTextRequestName.ReadOnly = true;
            this.uTextRequestName.Size = new System.Drawing.Size(100, 21);
            this.uTextRequestName.TabIndex = 25;
            // 
            // uTextRequestID
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRequestID.Appearance = appearance2;
            this.uTextRequestID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRequestID.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.Image = global::QRPEQU.UI.Properties.Resources.btn_Zoom;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance5;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextRequestID.ButtonsRight.Add(editorButton2);
            this.uTextRequestID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextRequestID.Location = new System.Drawing.Point(448, 52);
            this.uTextRequestID.MaxLength = 20;
            this.uTextRequestID.Name = "uTextRequestID";
            this.uTextRequestID.Size = new System.Drawing.Size(100, 19);
            this.uTextRequestID.TabIndex = 10;
            this.uTextRequestID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRequestID_KeyDown);
            this.uTextRequestID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextRequestID_EditorButtonClick);
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(336, 52);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(108, 20);
            this.uLabel6.TabIndex = 23;
            this.uLabel6.Text = "ultraLabel3";
            // 
            // uDateRepairReqDate
            // 
            this.uDateRepairReqDate.Location = new System.Drawing.Point(124, 52);
            this.uDateRepairReqDate.Name = "uDateRepairReqDate";
            this.uDateRepairReqDate.Size = new System.Drawing.Size(100, 21);
            this.uDateRepairReqDate.TabIndex = 9;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 76);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(108, 20);
            this.uLabelEtcDesc.TabIndex = 21;
            this.uLabelEtcDesc.Text = "ultraLabel2";
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(12, 52);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(108, 20);
            this.uLabel5.TabIndex = 21;
            this.uLabel5.Text = "ultraLabel2";
            // 
            // uTextPMPlanDate
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMPlanDate.Appearance = appearance17;
            this.uTextPMPlanDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPMPlanDate.Location = new System.Drawing.Point(864, 28);
            this.uTextPMPlanDate.Name = "uTextPMPlanDate";
            this.uTextPMPlanDate.ReadOnly = true;
            this.uTextPMPlanDate.Size = new System.Drawing.Size(100, 21);
            this.uTextPMPlanDate.TabIndex = 15;
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(760, 28);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(100, 20);
            this.uLabel4.TabIndex = 14;
            this.uLabel4.Text = "ultraLabel2";
            // 
            // uTextEquipName
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance19;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(552, 28);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(180, 21);
            this.uTextEquipName.TabIndex = 13;
            // 
            // uTextEquipCode
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance20;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(448, 28);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(100, 21);
            this.uTextEquipCode.TabIndex = 11;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(336, 28);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(108, 20);
            this.uLabel2.TabIndex = 10;
            this.uLabel2.Text = "ultraLabel2";
            // 
            // uTextRepairGICode
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGICode.Appearance = appearance18;
            this.uTextRepairGICode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRepairGICode.Location = new System.Drawing.Point(124, 28);
            this.uTextRepairGICode.Name = "uTextRepairGICode";
            this.uTextRepairGICode.ReadOnly = true;
            this.uTextRepairGICode.Size = new System.Drawing.Size(148, 21);
            this.uTextRepairGICode.TabIndex = 9;
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(12, 28);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(108, 20);
            this.uLabel1.TabIndex = 8;
            this.uLabel1.Text = "ultraLabel2";
            // 
            // frmEQUZ0009
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridRelease);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEQUZ0009";
            this.Load += new System.EventHandler(this.frmEQUZ0009_Load);
            this.Activated += new System.EventHandler(this.frmEQUZ0009_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEQUZ0009_FormClosing);
            this.Resize += new System.EventHandler(this.frmEQUZ0009_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchToDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateSearchFromDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnicianName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTechnician)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRelease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox3)).EndInit();
            this.uGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridPMResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridRepair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRequestID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateRepairReqDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPMPlanDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepairGICode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnicianName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTechnician;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUser;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipMentGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcGubun;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchYear;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchToDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateSearchFromDay;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRelease;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPMPlanDate;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepairGICode;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox3;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridRepair;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPMResult;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRequestName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRequestID;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateRepairReqDate;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcessGroup;
    }
}