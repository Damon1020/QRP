﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQUZ0031.cs                                         */
/* 프로그램명   : PM계획실적조회                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-02-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*              : ~~~~~ 추가 ()                                         */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;
using Microsoft.VisualBasic;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0031 : Form,IToolbar
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        public frmEQUZ0031()
        {
            InitializeComponent();
        }

        private void frmEQUZ0031_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            brwChannel.mfActiveToolBar(this.MdiParent, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmEQUZ0031_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitComboBox();
            InitGrid();
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀초기화
        /// </summary>
        private void InitTitle()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("PM계획대비실적조회", m_resSys.GetString("SYS_FONTNAME"), 12);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSearchDate, "기간", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipLargeType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipGroup, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUesr, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택"
                    , "PlantCode", "PlantName", dtPlant);

                //기본값 설정
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");


                #region Date

                //검색조건의 기간 콤보 설정
                ArrayList MonthKey = new ArrayList();
                ArrayList MonthValue = new ArrayList();
                ArrayList YearKey = new ArrayList();
                ArrayList YearValue = new ArrayList();
                int intCnt = 2011;

                string strYear = string.Empty;
                string strMonth = string.Empty;

                switch(m_resSys.GetString("SYS_LANG"))
                {
                    case "CHN" :
                        strYear = "年";
                        strMonth = "月";
                        break;
                    case "ENG" :
                        strYear = "Y";
                        strMonth = "M";
                        break;
                    default :
                        strYear = "년";
                        strMonth = "월";
                        break;
                }

                for (int i = 1; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        MonthKey.Add("0" + i);
                        MonthValue.Add(i.ToString() + strMonth);
                    }
                    else
                    {
                        MonthKey.Add(i);
                        MonthValue.Add(i.ToString() + strMonth);
                    }
                    if (intCnt < 2021)
                    {
                        YearKey.Add(intCnt);
                        YearValue.Add(intCnt.ToString() + strYear);
                        intCnt++;
                    }
                }

                wCombo.mfSetComboEditor(this.uComboSearchYear, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", YearKey, YearValue);

                wCombo.mfSetComboEditor(this.uComboSearchFromMonth, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", MonthKey, MonthValue);


                #endregion

                // Date ComboBox 기본값 설정
                DateTime DateNow = DateTime.Now;
                this.uComboSearchYear.Value = DateNow.Year.ToString();
                this.uComboSearchFromMonth.Value = DateNow.Month.ToString("D2");


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                grd.mfInitGeneralGrid(this.uGridSearchEquipInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                   false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                   Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                   Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridSearchEquipInfo, 0, "EquipCode", "설비", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSearchEquipInfo, 0, "Gubun", "구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #region 날짜컬럼

                for (int i = 0; i < 31; i++)
                {
                    string strKey = "";
                    string strValue = "";
                    if (i < 9)
                    {
                        strKey = "D0" + (i + 1).ToString();
                        strValue = (i + 1).ToString();
                    }
                    else
                    {
                        strKey = "D" + (i + 1).ToString();
                        strValue = (i + 1).ToString();
                    }
                    grd.mfSetGridColumn(this.uGridSearchEquipInfo, 0, strKey, strValue, false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, false, 100
                        , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                }
                #endregion

                this.uGridSearchEquipInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSearchEquipInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        #region ToolBar

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                ClearGrid();

                #region 필수입력확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                if (this.uComboSearchYear.Value == null
                    || this.uComboSearchYear.Value.ToString().Equals(string.Empty)
                    || !Information.IsNumeric(this.uComboSearchYear.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000208", Infragistics.Win.HAlign.Right);

                    this.uComboSearchYear.DropDown();
                    return;
                }

                if (this.uComboSearchFromMonth.Value == null
                   || this.uComboSearchFromMonth.Value.ToString().Equals(string.Empty)
                   || !Information.IsNumeric(this.uComboSearchFromMonth.Value))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001230", "M000201", Infragistics.Win.HAlign.Right);

                    this.uComboSearchFromMonth.DropDown();
                    return;
                }
                if (this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                               , "M001264", "M001230", "M000128", Infragistics.Win.HAlign.Right);

                    this.uComboSearchStation.DropDown();
                    return;
                }


                #endregion

                //검색조건 정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strYear = this.uComboSearchYear.Value.ToString();
                string strMonth = this.uComboSearchFromMonth.Value.ToString();
                //검색시작일
                string strPMPlanDate = strYear + "-" + strMonth;
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = this.uComboSearchEquipLargeType.Value.ToString();
                string strEquipGroupCode = this.uComboSearchEquipGroup.Value.ToString();

                string strUserID = "";
                if (!this.uTextUserID.Text.Equals(string.Empty) && !this.uTextUserName.Text.Equals(string.Empty))
                    strUserID = this.uTextUserID.Text;


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //설비점검수리조회 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUDIS.PMDIS), "PMDIS");
                QRPEQU.BL.EQUDIS.PMDIS clsPMDIS = new QRPEQU.BL.EQUDIS.PMDIS();
                brwChannel.mfCredentials(clsPMDIS);

                //PM계획대비실적조회매서드 실행
                DataTable dtPMPlan = clsPMDIS.mfReadPMPlanD_DISPlanResult(strPlantCode, 
                                                                            strYear, 
                                                                            strStationCode, 
                                                                            strEquipLocCode, 
                                                                            strProcessGroup, 
                                                                            strEquipLargeTypeCode, 
                                                                            strEquipGroupCode, 
                                                                            strUserID, 
                                                                            strPMPlanDate);


                string strLang = m_resSys.GetString("SYS_LANG");

                string strGubun = "";
                if (strLang.Equals("KOR"))
                    strGubun = "실적률";
                else if (strLang.Equals("CHN"))
                    strGubun = "实绩率";
                else if (strLang.Equals("ENG"))
                    strGubun = "실적률";
                

                //dtPMPlan 재구성
                if (dtPMPlan.Rows.Count > 0)
                {
                    DatatableArrange(dtPMPlan);

                    for (int i = 0; i < this.uGridSearchEquipInfo.Rows.Count; i++)
                    {
                        if (this.uGridSearchEquipInfo.Rows[i].Cells["EquipCode"].Text.Equals("Total"))
                        {
                            for(int j =0; j<this.uGridSearchEquipInfo.DisplayLayout.Bands[0].Columns.Count;j++)
                            {
                                this.uGridSearchEquipInfo.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                this.uGridSearchEquipInfo.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                            }
                        }
                        else
                        {
                            if (this.uGridSearchEquipInfo.Rows[i].Cells["Gubun"].Text.Equals(strGubun))
                            {
                                for (int j = 0; j < this.uGridSearchEquipInfo.DisplayLayout.Bands[0].Columns.Count; j++)
                                {
                                    this.uGridSearchEquipInfo.Rows[i].Cells[j].Appearance.BackColor = Color.MistyRose;
                                    this.uGridSearchEquipInfo.Rows[i].Cells[j].Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                }
                            }
                        }
                    }
                }



                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtPMPlan.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSearchEquipInfo.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridSearchEquipInfo);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {
        }

        #endregion

        #region Event

        #region Combo

        //공장 선택 시 Station 콤보박스 변화
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //InitData();

                //공장저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strPlant = this.uComboSearchPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboSearchPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboSearchPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {

                    //SystemResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    WinComboEditor wCom = new WinComboEditor();

                    //콤보 아이템 클리어
                    //this.uComboSearchArea.Items.Clear();
                    this.uComboSearchStation.Items.Clear();
                    //this.uComboSearchProcGubun.Items.Clear();

                    //언어저장
                    string strLang = m_resSys.GetString("SYS_LANG");


                    //----------Station 콤보 변화-----------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                    wCom.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", "선택",
                        "StationCode", "StationName", dtStation);



                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station 선택 시위치,설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //-----위치정보 콤보 변화--------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsLoc = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsLoc);

                    DataTable dtLoc = clsLoc.mfReadLocation_Combo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                    this.uComboSearchEquipLoc.Items.Clear();
                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                            true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                                            "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 위치변경시 설비대분류 설비중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                string strCode = this.uComboSearchEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비대분류 콤보선택시 설비 중분류 설비그룹이 변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비중분류 선택시 설비 그룹이변경됨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLargeType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),
                            this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipLargeType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        

       

        #endregion

        #region Text

        //정비사정보가 변경시 이름클리어
        private void uTextUserID_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextUserName.Text.Equals(string.Empty))
                    this.uTextUserName.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //정비사정보입력조회
        private void uTextUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //출고요청자ID, 공장 저장
                string strUserID = this.uTextUserID.Text;
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //키 데이터가 엔터키 고 내용이 있는 경우 조회한다.
                if (e.KeyData.Equals(Keys.Enter) && !strUserID.Equals(string.Empty))
                {
                    //공장정보조회
                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000274", "M000812", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    //유저정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //유저정보조회매서드 호출
                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                    //정보가 있을경우 등록 없을 경우 명초기화
                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextUserName.Text = dtUser.Rows[0]["UserName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                             , "M001264", "M001116", "M000883", Infragistics.Win.HAlign.Right);

                        if (!this.uTextUserName.Text.Equals(string.Empty))
                            this.uTextUserName.Clear();

                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //정비사텍스트 유저정보팝업창조회
        private void uTextUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //공장정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //공장정보조회
                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error,  500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000274", "M000272", Infragistics.Win.HAlign.Right);
                    return;
                }

                frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlantCode;
                frmUser.ShowDialog();

                this.uTextUserID.Text = frmUser.UserID;
                this.uTextUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #endregion

        #region Search

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 해당 년월에 해당하는 일수 구하기
        /// </summary>
        /// <param name="intYear">년도</param>
        /// <param name="intMonth">월</param>
        /// <returns></returns>
        private DataTable mfSearchDay(int intYear, int intMonth)
        {
            DataTable dtDay = new DataTable();
            try
            {
                dtDay.Columns.Add("Key", typeof(string));
                dtDay.Columns.Add("Value", typeof(string));

                int intMax = 0;

                //월이 4,6,9,11 인경우 날짜는 30일까지
                if (intMonth.Equals(2)
                    || intMonth.Equals(4)
                    || intMonth.Equals(6)
                    || intMonth.Equals(9)
                    || intMonth.Equals(11))
                {
                    //월이 2월인경우 날짜는 28일 혹은 29일까지 있다.
                    if (intMonth.Equals(2))
                    {
                        // 년도 / 4 하여 몫이 있으면 28일 
                        if (!(intYear % 4).Equals(0))
                        {
                            intMax = 28;
                        }
                        //없으면 29일
                        else
                        {
                            intMax = 29;
                        }
                    }
                    else
                    {
                        intMax = 30;
                    }
                }
                else
                {
                    intMax = 31;
                }
                DataRow drDay;
                for (int i = 0; i < intMax; i++)
                {
                    drDay = dtDay.NewRow();
                    if (i < 10)
                    {
                        drDay["Key"] = "0" + i.ToString();
                        drDay["Value"] = "0" + i.ToString() + "일";
                    }
                    else
                    {
                        drDay["Key"] = i;
                        drDay["Value"] = i.ToString() + "일";
                    }
                    dtDay.Rows.Add(drDay);
                }


                return dtDay;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDay;
            }
            finally
            { }
        }

        #endregion

        /// <summary>
        /// 그리드행삭제
        /// </summary>
        private void ClearGrid()
        {
            try
            {
                if (this.uGridSearchEquipInfo.Rows.Count > 0)
                {
                    this.uGridSearchEquipInfo.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridSearchEquipInfo.Rows.All);
                    this.uGridSearchEquipInfo.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// PM계획대비실적조회 화면에맞게 재구성
        /// </summary>
        private void DatatableArrange(DataTable dtPMPlan)
        {
            try
            {
                DataTable dtArrange = GirdColumnsSet();
                DataRow drArrange;
                int intcnt = 2; //dtArrange 줄번호
                string strDataRow = ""; //dtPMPlan 컬럼명을담을 변수
                string strDataRow2 = ""; //dtPMPlan 컬럼명을담을 변수
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG");

                string strGubun1 = "";
                string strGubun2 = "";
                string strGubun3 = "";
                if(strLang.Equals("KOR"))
                {
                    strGubun1 = "계획량";
                    strGubun2 = "실적량";
                    strGubun3 = "실적률";
                }
                else if(strLang.Equals("CHN"))
                {
                    strGubun1 = "计划量";
                    strGubun2 = "实绩量";
                    strGubun3 = "实绩率";
                }
                else if(strLang.Equals("ENG"))
                {
                    strGubun1 = "계획량";
                    strGubun2 = "실적량";
                    strGubun3 = "실적률";
                }
                

                for (int q = 0; q < dtPMPlan.Rows.Count; q++)
                {
                    
                    //한 설비당 3줄을 만든다.
                    drArrange = dtArrange.NewRow();
                    drArrange["EquipCode"] = dtPMPlan.Rows[q]["EquipCode"];
                    drArrange["Gubun"] = strGubun1;

                    dtArrange.Rows.Add(drArrange);

                    drArrange = dtArrange.NewRow();
                    drArrange["EquipCode"] = dtPMPlan.Rows[q]["EquipCode"];
                    drArrange["Gubun"] = strGubun2;

                    
                    dtArrange.Rows.Add(drArrange);

                    drArrange = dtArrange.NewRow();
                    drArrange["EquipCode"] = dtPMPlan.Rows[q]["EquipCode"];
                    drArrange["Gubun"] = strGubun3;

                    dtArrange.Rows.Add(drArrange);


                    for (int i = 1; i < 32; i++)
                    {
                        if (i < 10)
                        {
                            strDataRow = "D0" + i.ToString();
                            strDataRow2 = "R0" + i.ToString();
                        }
                        else
                        {
                            strDataRow = "D" + i.ToString();
                            strDataRow2 = "R" + i.ToString();

                        }

                        //계획량, 실적량, 실적률을 담는다.
                        dtArrange.Rows[intcnt - 2][strDataRow] = dtPMPlan.Rows[q][strDataRow];
                        dtArrange.Rows[intcnt - 1][strDataRow] = dtPMPlan.Rows[q][strDataRow2];
                        string strResult = (100 * (Convert.ToDouble(dtArrange.Rows[intcnt - 1][strDataRow]) / Convert.ToDouble(dtArrange.Rows[intcnt - 2][strDataRow]))).ToString("N2");
                        dtArrange.Rows[intcnt][strDataRow] = dtArrange.Rows[intcnt - 1][strDataRow].ToString() == "0" ? "0" : strResult;
                    }

                    //줄번호를 증가시킨다.
                    intcnt = intcnt + 3;
                }

                // 그리드에 바인드
                this.uGridSearchEquipInfo.DataSource = dtArrange;
                this.uGridSearchEquipInfo.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 컬럼설정
        /// </summary>
        /// <returns></returns>
        private DataTable GirdColumnsSet()
        {
            DataTable dtGridColumns = new DataTable();
            try
            {
                for (int i = 1; i <= 33; i++)
                {
                    if (i < 12)
                    {
                        if (i.Equals(1))
                            dtGridColumns.Columns.Add("EquipCode");
                        else if (i.Equals(2))
                            dtGridColumns.Columns.Add("Gubun");
                        else
                            dtGridColumns.Columns.Add("D0" + (i-2), typeof(string));
                    }
                    else
                        dtGridColumns.Columns.Add("D" + (i-2), typeof(string));
                }

                return dtGridColumns;
            }
            catch (Exception ex)
            {
                return dtGridColumns;
            }
            finally
            { }
        }
    }
}
