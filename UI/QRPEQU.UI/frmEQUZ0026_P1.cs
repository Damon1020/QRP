﻿
/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : frmEQUZ0026.cs                                        */
/* 프로그램명   : PM체크결과조회(상세조회팝업창)                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-01-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*              : ~~~~~ 추가 ()                                         */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPEQU.UI
{
    public partial class frmEQUZ0026_P1 : Form
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmEQUZ0026_P1()
        {
            InitializeComponent();
            InitLabel();
            InitGrid();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        public frmEQUZ0026_P1(DataTable dtPMList)
        {
            InitializeComponent();
            InitLabel();
            InitGrid();

            QRPCOM.QRPGLO.QRPBrowser brw = new QRPCOM.QRPGLO.QRPBrowser();
            brw.mfSetFormLanguage(this);

            InitData(dtPMList);


        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelDate, "기간", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelTaget, "", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelAll, "전체건수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelWait, "미완료 건수", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPer, "실시율", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelUserID, "정비사", m_resSys.GetString("SYS_FONTNAME"), true, false);

                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {   
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                grd.mfInitGeneralGrid(this.uGridPMList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                   false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                   Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.Default, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                   Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridPMList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMPeriodName", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 40, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMPlanDate", "점검일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMResultCode", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMResultName", "점검결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMResultValue", "수치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "PMWorkName", "PM입력자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMList, 0, "FilePath", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 데이터바인딩
        /// </summary>
        private void InitData(DataTable dtPMList)
        {
            try
            {
                if (dtPMList !=null && dtPMList.Rows.Count > 0)
                {
                    this.uTextDate.Text = dtPMList.Rows[0]["PMFromDate"].ToString();
                    this.uTextToDate.Text = dtPMList.Rows[0]["PMToDate"].ToString();
                    this.uTextTaget.Text = dtPMList.Rows[0]["PMTagetCode"].ToString();

                    //ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    string strLang = m_resSys.GetString("SYS_LANG");
                    string strLabelName = "";
                    //검색대상이 정비사인경우 
                    if (dtPMList.Rows[0]["PMTaget"].ToString().Equals("U"))
                    {
                        if (strLang.Equals("KOR"))
                            strLabelName = "정비사";
                        else if (strLang.Equals("CHN"))
                            strLabelName = "PM担当";
                        else if (strLang.Equals("ENG"))
                            strLabelName = "정비사";

                        this.uLabelTaget.Text = strLabelName;
                        this.uLabelUserID.Visible = false;
                        this.uTextWorkerID.Visible = false;
                    }
                    else     //검색대상이 설비인경우
                    {
                        if (strLang.Equals("KOR"))
                            strLabelName = "설비";
                        else if (strLang.Equals("CHN"))
                            strLabelName = "设备";
                        else if (strLang.Equals("ENG"))
                            strLabelName = "설비";

                        this.uLabelTaget.Text = strLabelName;

                    }

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                    QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                    brwChannel.mfCredentials(clsPMPlanD);

                    DataTable dtPM = clsPMPlanD.mfReadPMPlanD_Detail(dtPMList.Rows[0]["PlantCode"].ToString(),dtPMList.Rows[0]["PMTagetCode"].ToString(),
                                                                    dtPMList.Rows[0]["PMTaget"].ToString(),dtPMList.Rows[0]["PlanYear"].ToString(),
                                                                    dtPMList.Rows[0]["PMFromDate"].ToString(),dtPMList.Rows[0]["PMToDate"].ToString(),m_resSys.GetString("SYS_LANG"));
                    if (dtPM.Rows.Count > 0)
                    {
                        
                        this.uTextPMAll.Text = dtPM.Rows[0]["TotalCount"].ToString();
                        this.uTextPMWait.Text = (Convert.ToInt32(dtPM.Rows[0]["TotalCount"]) - Convert.ToInt32(dtPM.Rows[0]["WaitCount"])).ToString();
                        //this.uTextPer.Text = dtPM.Rows[0]["ResultRate"].ToString() + "%";
                        this.uTextPer.Text = (100 * (Convert.ToDouble(dtPM.Rows[0]["WaitCount"]) / Convert.ToDouble(this.uTextPMAll.Text))).ToString("N3") + "%";
                        this.uGridPMList.DataSource = dtPM;
                        this.uGridPMList.DataBind();

                        for (int i = 0; i < this.uGridPMList.Rows.Count; i++)
                        {
                            if (this.uGridPMList.Rows[i].Cells["PMResultCode"].Value.ToString().Equals(string.Empty)
                                && this.uGridPMList.Rows[i].Cells["PMResultValue"].Value.ToString().Equals(string.Empty))
                            {
                                this.uGridPMList.Rows[i].Appearance.BackColor = Color.Salmon;
                            }
                        }

                    }
                    //설비정보의 상세내용일 경우 해당설비의 정비사를 조회하여 텍스트에 뿌려줌
                    if (!dtPMList.Rows[0]["PMTaget"].ToString().Equals("U"))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                        QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                        brwChannel.mfCredentials(clsEquipWorker);

                        DataTable dtEquipWorker = clsEquipWorker.mfReadEquipWorker_Combo(dtPMList.Rows[0]["PlantCode"].ToString(), dtPMList.Rows[0]["PMTagetCode"].ToString(), m_resSys.GetString("SYS_LANG"));

                        if (dtEquipWorker.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtEquipWorker.Rows.Count; i++)
                            {
                                if (this.uTextWorkerID.Text.Equals(string.Empty))
                                    this.uTextWorkerID.Text = dtEquipWorker.Rows[i]["UserName"].ToString();
                                else
                                    this.uTextWorkerID.Text = this.uTextWorkerID.Text + "," + dtEquipWorker.Rows[i]["UserName"].ToString();
                            }
                        }
                        else
                        {
                            WinMessageBox msg = new WinMessageBox();
                            string strTextName = "";
                            if (strLang.Equals("KOR"))
                                strTextName = "정비사가 미등록된 설비입니다.";
                            else if (strLang.Equals("CHN"))
                                strTextName = msg.GetMessge_Text("M001484",strLang);
                            else if (strLang.Equals("ENG"))
                                strTextName = "정비사가 미등록된 설비입니다.";

                            this.uTextWorkerID.Text = strTextName;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uGridPMList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("FilePath"))
                {
                    //PM체크 등록 업로드한 첨부파일 다운로드
                    if (e.Cell.Column.Key.Equals("FilePath")) //경로수정필요
                    {
                        if (e.Cell.Row.Cells["FilePath"].Value.ToString().Contains(":\\") || e.Cell.Row.Cells["FilePath"].Value.ToString().Equals(string.Empty))
                            return;

                        System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                        saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                        saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                        saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                        saveFolder.Description = "Download Folder";

                        string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                        if (saveFolder.ShowDialog() == DialogResult.OK)
                        {
                            string strSaveFolder = saveFolder.SelectedPath + "\\";

                            //화일서버 연결정보 가져오기
                            QRPBrowser brwChannel = new QRPBrowser();
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            brwChannel.mfCredentials(clsSysAccess);
                            DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                            //설비점검정보 첨부파일 저장경로정보 가져오기
                            brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                            QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                            brwChannel.mfCredentials(clsSysFilePath);
                            DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0029");


                            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                            ArrayList arrFile = new ArrayList();

                            arrFile.Add(e.Cell.Row.Cells["FilePath"].Value.ToString());


                            //Upload정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                                   dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                                   dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();

                            // 파일 실행시키기
                            System.Diagnostics.Process.Start(strSaveFolder + e.Cell.Row.Cells["FilePath"].Value.ToString());
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}
