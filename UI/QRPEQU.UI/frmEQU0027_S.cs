﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : SparePart관리                                         */
/* 프로그램ID   : frmEQU0027_S.cs                                       */
/* 프로그램명   : SparePart재고현황                                     */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-11-11                                            */
/* 수정이력     : 1111-11-11 :                                          */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;



namespace QRPEQU.UI
{
    public partial class frmEQU0027_S : Form , IToolbar
    {
        //시스템리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmEQU0027_S()
        {
            InitializeComponent();
        }

        private void frmEQU0027_Activated(object sender, EventArgs e)
        {


            QRPBrowser ToolBar = new QRPBrowser();
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolBar.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
            
        }

        private void frmEQU0027_Load(object sender, EventArgs e)
        {
            SetToolAuth();
            InitTitle();
            InitLabel();
            InitCombo();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화

        /// <summary>
        /// 타이틀설정
        /// </summary>
        private void InitTitle()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                titleArea.mfSetLabelText("SparePart재고현황", m_resSys.GetString("SYS_FONTNAME"), 12);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블설정
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelSPInventory, "창고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquip, "설비", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSparePart, "SparePart", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스설정
        /// </summary>
        private void InitCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                //공장정보BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCom = new WinComboEditor();

                wCom.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 설정
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //SpreaPart재고현황 그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridSparePart, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                     Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                      Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                //SparePart재고현황 그리드 컬럼설정
                grd.mfSetGridColumn(this.uGridSparePart, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SPInventoryCode", "창고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SPInventoryName", "창고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SPChgStandby", "대기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SparePartCode", "SparePart코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SparePartName", "SparePart명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "Spec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "Maker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "Qty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                grd.mfSetGridColumn(this.uGridSparePart, 0, "SafeQty", "안전재고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "n,nnn,nnn,nnn", "0");

                // 그리드 편집불가상태
                this.uGridSparePart.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                this.uGridSparePart.DisplayLayout.Bands[0].Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region 툴바

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001264", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }

                //검색정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strSPInventoryCode = this.uComboSPInventory.Value.ToString();
                string strEquipCode = "";
                string strSparePartCode = "";
                
                if (!this.uTextEquipCode.Text.Equals(string.Empty) && !this.uTextEquipName.Text.Equals(string.Empty))
                {
                    strEquipCode = this.uTextEquipCode.Text;
                }

                if (!this.uTextSparePartCode.Text.Equals(string.Empty) && !this.uTextSparePartName.Text.Equals(string.Empty))
                {
                    strSparePartCode = this.uTextSparePartCode.Text;
                }


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");

                //SparePart재고정보 BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUSPA.SPStock), "SPStock");
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                brwChannel.mfCredentials(clsSPStock);

                //SparePart현재고현황 정보 매서드 호출
                DataTable dtSPStock = clsSPStock.mfReadSPStockState_PSTS(strPlantCode, strSPInventoryCode, strEquipCode, strSparePartCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 현재고현황을 바인드
                this.uGridSparePart.DataSource = dtSPStock;
                this.uGridSparePart.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtSPStock.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    foreach (Infragistics.Win.UltraWinGrid.UltraGridRow _uRow in this.uGridSparePart.Rows)
                    {
                        if (!(_uRow.Cells["SPInventoryCode"].Value.ToString().Equals(string.Empty)))
                        {
                            if(!(Convert.ToInt32(_uRow.Cells["Qty"].Value) > Convert.ToInt32(_uRow.Cells["SafeQty"].Value)))
                                _uRow.Appearance.BackColor = Color.Salmon;
                        }
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSparePart, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSparePart.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridSparePart);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {
        }

        public void mfPrint()
        {
        }

        #endregion

        #region Event..

        //공장선택에 따라 창고가 변한다.
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (!strPlantCode.Equals(string.Empty))
                {

                    this.uComboSPInventory.Items.Clear();

                    //SparePart창고정보BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                    QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                    brwChannel.mfCredentials(clsSPInventory);

                    //SparePart창고 콤보박스조회 매서드 호출
                    DataTable dtSparePart = clsSPInventory.mfReadMASSPInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    WinComboEditor wCom = new WinComboEditor();

                    //SparePart창고 콤보박스 변환
                    wCom.mfSetComboEditor(this.uComboSPInventory, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                        , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체", "SPInventoryCode", "SPInventoryName", dtSparePart);


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비정보 팝업창을 띄운다.
        private void uTextEquipCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보 저장
                string strPlant = this.uComboSearchPlant.Value.ToString();

                if (strPlant.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000962", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0005 frmEquip = new QRPCOM.UI.frmCOM0005();
                //공장정보를 보낸다.
                frmEquip.PlantCode = strPlant;
                frmEquip.ShowDialog();


                this.uTextEquipCode.Text = frmEquip.EquipCode;
                this.uTextEquipName.Text = frmEquip.EquipName;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //SparePart 팝업창을 띄운다.
        private void uTextSparePartCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보 저장
                string strPlant = this.uComboSearchPlant.Value.ToString();

                if (strPlant.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000962", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPCOM.UI.frmCOM0008 frmSP = new QRPCOM.UI.frmCOM0008();
                //공장정보를 보낸다.
                frmSP.PlantCode = strPlant;
                frmSP.ShowDialog();

                this.uTextSparePartCode.Text = frmSP.SparePartCode;
                this.uTextSparePartName.Text = frmSP.SparePartName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비코드를 입력 후 엔터키를 누르면 자동조회
        private void uTextEquipCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Delete || e.KeyData == Keys.Back)
                {
                    if (!this.uTextEquipName.Text.Equals(string.Empty))
                    {
                        this.uTextEquipName.Text = "";      //설비명
                        
                    }
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextEquipCode.Text.Equals(string.Empty))
                {
                    //공장정보가 없으면 메세지를 보여준다.
                    if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000266", Infragistics.Win.HAlign.Right);
                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //공장정보, 설비코드 저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strEquipCode = this.uTextEquipCode.Text;

                    QRPBrowser brwChannel = new QRPBrowser();
                    //설비정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);
                    //설비정보 조회 매서드호출
                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    //정보가 있는경우
                    if (dtEquip.Rows.Count > 0)
                    {
                        this.uTextEquipName.Text = dtEquip.Rows[0]["EquipCode"].ToString();             //설비코드
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001115", "M000902", Infragistics.Win.HAlign.Right);

                        if (!this.uTextEquipName.Text.Equals(string.Empty))
                        {
                            this.uTextEquipName.Text = "";      //설비명
                        }

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //SparePart코드를 입력 후 엔터키를 누르면 자동조회
        private void uTextSparePartCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Delete || e.KeyData == Keys.Back)
                {
                    if (!this.uTextSparePartName.Text.Equals(string.Empty))
                    {
                        this.uTextSparePartName.Text = "";      //설비명

                    }
                }

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (e.KeyData.Equals(Keys.Enter) && !this.uTextSparePartCode.Text.Equals(string.Empty))
                {
                    //공장정보가 없으면 메세지를 보여준다.
                    if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001264", "M000266", Infragistics.Win.HAlign.Right);
                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //공장정보, SparePart코드 저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strSparePart = this.uTextSparePartCode.Text;

                    QRPBrowser brwChannel = new QRPBrowser();
                    //SparePart정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
                    QRPMAS.BL.MASEQU.SparePart clsSparePart = new QRPMAS.BL.MASEQU.SparePart();
                    brwChannel.mfCredentials(clsSparePart);

                    //SparePart정보 조회 매서드호출
                    DataTable dtSparePart = clsSparePart.mfReadMASSparePartName(strPlantCode, strSparePart, m_resSys.GetString("SYS_LANG"));

                    //정보가 있는경우
                    if (dtSparePart.Rows.Count > 0)
                    {
                        this.uTextSparePartName.Text = dtSparePart.Rows[0]["SparePartName"].ToString();             //SparePart명
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001115", "M000887", Infragistics.Win.HAlign.Right);

                        if (!this.uTextSparePartName.Text.Equals(string.Empty))
                        {
                            this.uTextSparePartName.Text = "";      //SparePart명
                        }

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        private void frmEQU0027_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfSaveGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSparePartCode_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextSparePartName.Text.Equals(string.Empty))
                this.uTextSparePartName.Clear();
        }

        #endregion

        private void uGridSparePart_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            //try
            //{
            //    string strPlantCode = e.Row.GetCellValue("PlantCode").ToString();
            //    string strSparePartCode = e.Row.GetCellValue("SparePartCode").ToString();
            //    string strSparePartName = e.Row.GetCellValue("SparePartName").ToString();
            //}
            //catch (Exception ex)
            //{
            //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            //    frmErr.ShowDialog();
            //}
            //finally
            //{
            //}
        }





    }
}
